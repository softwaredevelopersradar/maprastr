﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;

//0206*
using System.Windows.Input;
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;
using Bitmap = System.Drawing.Bitmap;

namespace GrozaMap
{
    public partial class FormWay : Form
    {
/*
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);
*/

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        public string flname_W;
        public string flname_R;

        public double dchisloW;
        public long ichisloW;
        public double X_Coordl5;
        public double Y_Coordl5;

        public double DXX_W;
        public double DYY_W;

        // !!! Флаг окончаня маршрута (устанавливается по кнопке ЗАГРУЗИТЬ)
        public static uint flEndWay = 0;
        //Координаты 
        public static double[] mas_LW = new double[10000];
        public static double[] mas_XW = new double[10000];
        public static double[] mas_YW = new double[10000];

        public static uint iW=0;
        public static double LW=0;
        public static double X_StartW=0;
        public static double Y_StartW=0;
        public static double X1_W=0;
        public static double Y1_W=0;
        public static double X2_W=0;
        public static double Y2_W=0;

        // ......................................................................

        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // Конструктор *********************************************************** 

        public FormWay()
        {
            InitializeComponent();


            //iW = 0;

            dchisloW=0;
            ichisloW=0;
            X_Coordl5 = 0;
            Y_Coordl5 = 0;

            DXX_W=0;
            DYY_W=0;
            // ......................................................................


        } // Конструктор
        // ***********************************************************  Конструктор

        // *****************************************************************************************
        // Обработчик кнопки Button1: Начало обработки маршрута маршрута
        // *****************************************************************************************

        private void button1_Click(object sender, EventArgs e)
        {
/*
            // ......................................................................
            //MessageBox.Show("Выберите пункты с помощью левой кнопки  мыши и нажмите кнопку ЗАГРУЗИТЬ");
            // ......................................................................
            // Очистка

            if (GlobalVarLn.blWay_stat == false)
            {
                flEndWay = 1;
                GlobalVarLn.blWay_stat = true;

                Array.Clear(mas_LW, 0, 10000);
                Array.Clear(mas_XW, 0, 10000);
                Array.Clear(mas_YW, 0, 10000);

                iW = 0;
                X_StartW = 0;
                Y_StartW = 0;
                LW = 0;
            }
            else
            {
                MessageBox.Show("Маршрут уже запущен");
            }
            // ......................................................................
*/
            ;
        } // Button1
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button2:ЗАГРУЗИТЬ (Конец обработки маршрута маршрута)
        // *****************************************************************************************

        private void button2_Click(object sender, EventArgs e)
        {
/*
            // !!! Флаг окончаня маршрута (устанавливается по кнопке ЗАГРУЗИТЬ)
            flEndWay = 0;

            ichisloW = (long)((LW / 1000) * 10000000);
            dchisloW = ((double)ichisloW) / 10000000;
            textBox2.Text = Convert.ToString(dchisloW);   // L

            textBox1.Text = Convert.ToString(iW);         // N
*/
            ;
        } // Button2
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button3: очистить
        // *****************************************************************************************

        private void button3_Click(object sender, EventArgs e)
        {
            // .....................................................................................
            // Очистка dataGridView

            while (dataGridView1.Rows.Count != 0)
                dataGridView1.Rows.Remove(dataGridView1.Rows[dataGridView1.Rows.Count - 1]);

            dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatWay_stat; i++)
            {
                dataGridView1.Rows.Add("", "", "");
            }
            // .....................................................................................
            if (chbWay.Checked == false)
            {
                GlobalVarLn.blWay_stat = false;
                GlobalVarLn.flEndWay_stat = 0;
            }
            else
            {
                GlobalVarLn.blWay_stat = true;
                GlobalVarLn.flEndWay_stat = 1;
            }

            GlobalVarLn.list_way.Clear();

            GlobalVarLn.iW_stat = 0;
            GlobalVarLn.X_StartW_stat = 0;
            GlobalVarLn.Y_StartW_stat = 0;
            GlobalVarLn.LW_stat = 0;
            // .....................................................................................
            textBox2.Text = "";   // L
            textBox1.Text = "";   // N
            // .....................................................................................
            // Убрать с карты

            //0209
            //GlobalVarLn.axMapScreenGlobal.Repaint();
            MapForm.REDRAW_MAP();
            // .....................................................................................


        } // Button3
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button4: сохранить
        // *****************************************************************************************

        private void button4_Click(object sender, EventArgs e)
        {
            int i_tmp=0;

        // -----------------------------------------------------------------------------------------
        // Variant2


            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            //0220
/*
            System.Windows.Forms.DialogResult result;
            result = saveFileDialog1.ShowDialog();
            if (
                (result == System.Windows.Forms.DialogResult.OK) &&
                (saveFileDialog1.FileName.Length > 0)
                )
                GlobalVarLn.flname_W_stat = saveFileDialog1.FileName;

            else if (result == System.Windows.Forms.DialogResult.Cancel)
                return;
            else
            {
                MessageBox.Show("Can't save file!");
                return;
            }
*/

            String strLine = "";
            String strLine1 = "";
            String strLine2 = "";
            int lng = 0;
            int indStart = 0;
            int indStop = 0;
            int indEnd = 0;
            int iLength = 0;
            int iLength1 = 0;
            int TekPoz = 0;

            String time = DateTime.Now.ToString(@"dd/MM/yyyy HH:mm:ss");

            TekPoz = 0;
            strLine = time;
            lng = time.Length;
            indEnd = lng - 1;
            indStart = 0; 
            indStop = strLine.IndexOf(' ', TekPoz);  // probel
            iLength = indStop - indStart + 1;
            iLength1 = indEnd - indStop + 1;
            strLine1 = strLine.Remove(indStop, iLength1);
            strLine2 = strLine.Remove(indStart, iLength);
            if (strLine1.IndexOf(".") > -1) strLine1 = strLine1.Replace('.', '_');
            if (strLine2.IndexOf(":") > -1) strLine2 = strLine2.Replace(':', '_');

            if (strLine1.IndexOf("/") > -1) strLine1 = strLine1.Replace('/', '_');
            if (strLine1.IndexOf("\\") > -1) strLine1 = strLine1.Replace('\\', '_');


            if (Directory.Exists(strLine1) == false)
                Directory.CreateDirectory(Application.StartupPath + "\\Routs\\" + strLine1);
            GlobalVarLn.flname_W_stat = Application.StartupPath + "\\Routs\\" + strLine1 +"\\"+strLine2+ ".txt";
            MessageBox.Show("File is saved");
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            StreamWriter srFile;

            try
            {
                srFile = new StreamWriter(GlobalVarLn.flname_W_stat);
            }
            catch
            {
                MessageBox.Show("Can’t save file");
                return;
            }

            srFile.WriteLine("N =" + Convert.ToString(GlobalVarLn.iW_stat));

            GlobalVarLn.iW_stat = GlobalVarLn.iW_stat;

            //0209
            //for (i_tmp = 1; i_tmp <= GlobalVarLn.iW_stat; i_tmp++)
            for (i_tmp = 0; i_tmp < GlobalVarLn.list_way.Count; i_tmp++)

            {
                srFile.WriteLine("X" + Convert.ToString(i_tmp) + " =" + Convert.ToString((int)GlobalVarLn.list_way[i_tmp].X_m));
                srFile.WriteLine("Y" + Convert.ToString(i_tmp) + " =" + Convert.ToString((int)GlobalVarLn.list_way[i_tmp].Y_m));

            }

            srFile.Close();
        // -----------------------------------------------------------------------------------------




        } // Button4
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button5: чтение
        // *****************************************************************************************

        private void button5_Click(object sender, EventArgs e)
        {

            // TTT
            //timer1.Start();



            // ......................................................................
            String strLine = "";

            String strLine1 = "";
            //String strLine2 = "";

            double number1=0;
            int number2=0;

            char symb1 = 'N';
            char symb2 = 'X';
            char symb3 = 'Y';
            char symb4 = '=';

            int indStart=0;
            int indStop=0;
            int iLength=0;

            int IndZap=0;
            int TekPoz=0;

            double x1 = 0;
            double y1 = 0;
            double x2 = 0;
            double y2 = 0;
            double dx = 0;
            double dy = 0;
            double li = 0;
            int fi=0;

            //0209 
            double lat = 0;
            double lon = 0;



 // VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
 // Variant2

            //if (chbWay.Checked == false)
            //{
            //    MessageBox.Show("The route entry checkbox is not selected");
            //    return;
            //}
           

// Очистка ---------------------------------------------------------------------------
// .....................................................................................
            // Очистка dataGridView

            while (dataGridView1.Rows.Count != 0)
                dataGridView1.Rows.Remove(dataGridView1.Rows[dataGridView1.Rows.Count - 1]);

            dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatWay_stat; i++)
            {
                dataGridView1.Rows.Add("", "", "");
            }
// .....................................................................................
            GlobalVarLn.blWay_stat = true;
            GlobalVarLn.flEndWay_stat = 1;

            GlobalVarLn.list_way.Clear();

            GlobalVarLn.iW_stat = 0;
            GlobalVarLn.X_StartW_stat = 0;
            GlobalVarLn.Y_StartW_stat = 0;
            GlobalVarLn.LW_stat = 0;
// .....................................................................................
            textBox2.Text = "";   // L
            textBox1.Text = "";   // N
// .....................................................................................
            // Убрать с карты

            //0209
            //GlobalVarLn.axMapScreenGlobal.Repaint();
// .....................................................................................

// --------------------------------------------------------------------------- Очистка

// Чтение файла ---------------------------------------------------------
// ......................................................................

            System.Windows.Forms.DialogResult result;

            // 09_10_2018
            openFileDialog1.InitialDirectory = Application.StartupPath + "\\Routs\\";

            result = openFileDialog1.ShowDialog();
            if (
               //(openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK) &&
               (result == System.Windows.Forms.DialogResult.OK) &&
               (openFileDialog1.FileName.Length > 0)
              )
                GlobalVarLn.flname_R_stat = openFileDialog1.FileName;
            else if (result == System.Windows.Forms.DialogResult.Cancel)
                return;
            else
            {
                MessageBox.Show("Can't open file!");
                return;
            }

 // ......................................................................
            //StreamReader srFile1 = new StreamReader(GlobalVarLn.flname_R_stat);

            StreamReader srFile1;
            try
            {
                srFile1 = new StreamReader(GlobalVarLn.flname_R_stat);
            }
            catch
            {
                MessageBox.Show("Can’t open file");
                return;

            }
 // ......................................................................

 // ----------------------------------------------------------------------
 // Чтение
 // N =...
 // X1 =...
 // Y1 =...
 // X2 =...
 // Y2 =...
 // ...
           try
            {
                TekPoz = 0;
                IndZap = 0;

              //0209
              // 1-й List пустой
              Way objWay = new Way();
              //objWay.X_m = 0;
              //objWay.Y_m = 0;
              //objWay.L_m = 0;
              //GlobalVarLn.list_way.Add(objWay);
              
               // .......................................................
               // N =...
               // 1-я строка

               strLine = srFile1.ReadLine(); 

               if(strLine==null)
               {
                   MessageBox.Show("No information");
                return;
               }

               indStart = strLine.IndexOf(symb1, TekPoz); // N

               if(indStart==-1)
               {
                   MessageBox.Show("No information");
                return;
               }

               indStop = strLine.IndexOf(symb4, TekPoz);  //=

               if((indStop==-1)||(indStop<indStart))
               {
                   MessageBox.Show("No information");
                return;
               }

                iLength = indStop - indStart + 1;
                // Убираем 'N ='
                strLine1 = strLine.Remove(indStart, iLength);

               if(strLine1=="")
               {
                   MessageBox.Show("No information");
                return;
               }

                // Количество пунктов
                number2 = Convert.ToInt32(strLine1);
                GlobalVarLn.iW_stat = (uint)number2;
               // .......................................................
               // 1-я координата X
               // X1 =...

               //0209
               //IndZap=1;

               strLine = srFile1.ReadLine(); // 2-я строка
               if(strLine==null)
               {
                GlobalVarLn.iW_stat=0;
                MessageBox.Show("No information");
                return;
               }

               indStart = strLine.IndexOf(symb2, TekPoz); // X
               if(indStart==-1)
               {
                GlobalVarLn.iW_stat=0;
                MessageBox.Show("No information");
                return;
               }

               indStop = strLine.IndexOf(symb4, TekPoz);  // =
               if((indStop==-1)||(indStop<indStart))
               {
                GlobalVarLn.iW_stat=0;
                MessageBox.Show("No information");
                return;
               }

               iLength = indStop - indStart + 1;
               strLine1 = strLine.Remove(indStart, iLength);
               if(strLine1=="")
               {
                GlobalVarLn.iW_stat=0;
                MessageBox.Show("No information");
                return;
               }

               number1 = Convert.ToDouble(strLine1);

               objWay.X_m = number1;
               x1=number1;
               // .......................................................
               // 1-я координата Y
               // Y1 =...

               strLine = srFile1.ReadLine(); // 3-я строка
               if(strLine==null)
               {
                GlobalVarLn.iW_stat=0;
                MessageBox.Show("No information");
                return;
               }

               indStart = strLine.IndexOf(symb3, TekPoz); // Y
               if(indStart==-1)
               {
                GlobalVarLn.iW_stat=0;
                MessageBox.Show("No information");
                return;
               }

               indStop = strLine.IndexOf(symb4, TekPoz);  // =
               if((indStop==-1)||(indStop<indStart))
               {
                GlobalVarLn.iW_stat=0;
                MessageBox.Show("No information");
                return;
               }

               iLength = indStop - indStart + 1;
               strLine1 = strLine.Remove(indStart, iLength);
               if(strLine1=="")
               {
                GlobalVarLn.iW_stat=0;
                MessageBox.Show("No information");
                return;
               }

               number1 = Convert.ToDouble(strLine1);

               objWay.Y_m=number1;
               y1=number1;
               // ......................................................
               // Занести в List

               objWay.L_m = 0;
               GlobalVarLn.list_way.Add(objWay);
               // .......................................................
               // Занести в таблицу
               // way

               var moveX = GlobalVarLn.list_way[IndZap].X_m;
               var moveY = GlobalVarLn.list_way[IndZap].Y_m;

               //0209
               //Merkator
               var p5 = Mercator.ToLonLat(moveX, moveY);
               lat = p5.Y;
               lon = p5.X;

               //0209
               //MapCore.mapPlaneToGeo((int)axaxcMapScreen.MapHandle, ref moveX, ref moveY);
               //MapCore.mapGeoToRealGeo(ref moveX, ref moveY);
               //dataGridView1.Rows[(int)(IndZap - 1)].Cells[0].Value = moveX.ToString("F3"); // X
               //dataGridView1.Rows[(int)(IndZap - 1)].Cells[1].Value = moveY.ToString("F3"); // Y
               dataGridView1.Rows[(int)IndZap].Cells[0].Value = lat.ToString("F3"); 
               dataGridView1.Rows[(int)IndZap].Cells[1].Value = lon.ToString("F3"); 
               // .......................................................

               fi=0;
               strLine = srFile1.ReadLine(); // читаем далее (X2)
               if ((strLine == "")||(strLine==null))
                   fi=1;
               // .......................................................

               // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH
                while ((strLine != "")&&(strLine!=null))
                {
                    IndZap += 1;
                    // ...................................................................
                    // X

                    indStart = strLine.IndexOf(symb2, TekPoz); // X
                    if(indStart==-1)
                      fi=1;

                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    if((indStop==-1)||(indStop<indStart))
                      fi=1;

                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    if(strLine1=="")
                      fi=1;

                    number1 = Convert.ToDouble(strLine1);

                    if(fi==0)
                     objWay.X_m=number1;
                    else
                     objWay.X_m=0;

                    x2=number1;

                    fi=0;
                    strLine = srFile1.ReadLine(); // читаем далее (Y)
                    if ((strLine == "")||(strLine==null))
                     fi=1;
                    // ...................................................................
                    // Y

                    indStart = strLine.IndexOf(symb3, TekPoz); // Y
                    if(indStart==-1)
                      fi=1;

                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    if((indStop==-1)||(indStop<indStart))
                      fi=1;

                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    if(strLine1=="")
                      fi=1;

                    number1 = Convert.ToDouble(strLine1);

                    if(fi==0)
                     objWay.Y_m=number1;
                    else
                     objWay.Y_m = 0;

                    y2=number1;
                    // ...................................................................
                    // L

                    dx = x2 - x1;
                    dy = y2 - y1;

                    li = Math.Sqrt(dx*dx+dy*dy);

                    objWay.L_m = li;

                    GlobalVarLn.LW_stat += li;

                    x1 = x2;
                    y1 = y2;
                    // .......................................................
                    // way
                    // Занести в List

                    GlobalVarLn.list_way.Add(objWay);
                    // .......................................................

                    // ...................................................................
                    // Занести в таблицу
                    moveX = GlobalVarLn.list_way[IndZap].X_m;
                    moveY = GlobalVarLn.list_way[IndZap].Y_m;

                    //0209
                    //MapCore.mapPlaneToGeo((int)axaxcMapScreen.MapHandle, ref moveX, ref moveY);
                    //MapCore.mapGeoToRealGeo(ref moveX, ref moveY);
                    var p6 = Mercator.ToLonLat(moveX, moveY);
                    lat = p6.Y;
                    lon = p6.X;

                    //??????????????
                    //dataGridView1.Rows[(int)(IndZap - 1)].Cells[0].Value = moveX.ToString("F3"); // X
                    //dataGridView1.Rows[(int)(IndZap - 1)].Cells[1].Value = moveY.ToString("F3"); // Y
                    //dataGridView1.Rows[(int)(IndZap - 1)].Cells[2].Value = (int)GlobalVarLn.list_way[IndZap].L_m; // L
                    dataGridView1.Rows[(int)IndZap ].Cells[0].Value = lat.ToString("F3"); // X
                    dataGridView1.Rows[(int)IndZap].Cells[1].Value = lon.ToString("F3"); // Y
                    dataGridView1.Rows[(int)IndZap].Cells[2].Value = (int)GlobalVarLn.list_way[IndZap].L_m; // L

                    // ...................................................................

                    fi=0;
                    strLine = srFile1.ReadLine(); // читаем далее (X)
                    if ((strLine == "")||(strLine==null))
                     fi=1;

                    // ...................................................................


                } // WHILE
               // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH

            }
            catch
            {
            }
            // ----------------------------------------------------------------------
            srFile1.Close();
 // --------------------------------------------------------- Чтение файла

            textBox2.Text = Convert.ToString((int)GlobalVarLn.LW_stat);   // L
            textBox1.Text = Convert.ToString(GlobalVarLn.iW_stat);         // N

            //0209
            // Перерисовка
            //f_WayReDraw();
            MapForm.REDRAW_MAP();

            // VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV


        } // Button5 (Read from file)
        // *****************************************************************************************

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            ;
        }

        private void label2_Click(object sender, EventArgs e)
        {
            ;
        }

        // FUNCTIONS ********************************************************************************

        // ******************************************************************************************
        // Нарисовать метку по координатам (красный прямоугольник)
        //
        // Входные параметры(на карте):
        // X - X, m
        // Y - Y, m
        // ******************************************************************************************
        public void f_Map_Rect_XY2(

                                         double X,
                                         double Y
                                        )
        {
/*
            // -------------------------------------------------------------------------------------
            //objMapForm10.graph = objMapForm10.axaxcMapScreen1.CreateGraphics();
            Graphics graph = axaxcMapScreen.CreateGraphics();


            Pen penRed2 = new Pen(Color.Red, 2);
            Brush brushRed2 = new SolidBrush(Color.Red);

            // -------------------------------------------------------------------------------------

            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
            // -------------------------------------------------------------------------------------

            //if (objMapForm10.graph != null)
            if (graph != null)

            {
                graph.FillRectangle(brushRed2, (int)X - axaxcMapScreen.MapLeft, (int)Y - axaxcMapScreen.MapTop, 7, 7);

            }
            // -------------------------------------------------------------------------------------
*/


        } // P/P f_Map_Rect_XY1


        // *************************************************************************************

        // ******************************************************************************************
        // Обработка нажатия левой кнопки мыши при отрисовке маршрута
        //
        // Входные параметры:
        // X - X, m на местности
        // Y - Y, m
        // ******************************************************************************************
        public void f_Way(
                          double X,
                          double Y
                         )
        {


            //0209
/*
            // ......................................................................
            GlobalVarLn.iW_stat += 1; // Номер пункта

            // way
            Way objWay = new Way();
            // ......................................................................
            // way

            if (GlobalVarLn.iW_stat == 1)
            {
                // 1-й List пустой
                objWay.X_m = 0;
                objWay.Y_m = 0;
                objWay.L_m = 0;
                GlobalVarLn.list_way.Add(objWay);
            }
            // ......................................................................

            GlobalVarLn.X1_W_stat = GlobalVarLn.X_StartW_stat;
            GlobalVarLn.Y1_W_stat = GlobalVarLn.Y_StartW_stat;
            GlobalVarLn.X2_W_stat = GlobalVarLn.MapX1;
            GlobalVarLn.Y2_W_stat = GlobalVarLn.MapY1;
            GlobalVarLn.X_StartW_stat = GlobalVarLn.X2_W_stat;
            GlobalVarLn.Y_StartW_stat = GlobalVarLn.Y2_W_stat;

            objWay.X_m = GlobalVarLn.MapX1;
            objWay.Y_m = GlobalVarLn.MapY1;

            // ......................................................................
            // Отображение координат карты, полученных кликом мыши на карте
            // !!! MapX1,MapY1 -> реальные координаты на местности карты в м (Plane)

            ClassMap.f_Map_Rect_XYS_stat(
                          GlobalVarLn.MapX1,
                          GlobalVarLn.MapY1,
                          ""
                         );
            // ......................................................................
            // Добавить строку

            var moveX = GlobalVarLn.X_StartW_stat;
            var moveY = GlobalVarLn.Y_StartW_stat;

            MapCore.mapPlaneToGeo((int)axaxcMapScreen.MapHandle, ref moveX, ref moveY);
            MapCore.mapGeoToRealGeo(ref moveX, ref moveY);

            dataGridView1.Rows[(int)(GlobalVarLn.iW_stat - 1)].Cells[0].Value = moveX.ToString("F3"); // X
            dataGridView1.Rows[(int)(GlobalVarLn.iW_stat - 1)].Cells[1].Value = moveY.ToString("F3"); // Y
            // ......................................................................

            // IF2
            if (GlobalVarLn.iW_stat > 1)
            {
                // ......................................................................
                //m
                GlobalVarLn.DXX_W_stat = GlobalVarLn.X2_W_stat - GlobalVarLn.X1_W_stat;
                GlobalVarLn.DYY_W_stat = GlobalVarLn.Y2_W_stat - GlobalVarLn.Y1_W_stat;

                objWay.L_m = Math.Sqrt(GlobalVarLn.DXX_W_stat * GlobalVarLn.DXX_W_stat +
                                       GlobalVarLn.DYY_W_stat * GlobalVarLn.DYY_W_stat);

                // ......................................................................
                GlobalVarLn.LW_stat += objWay.L_m;
                // ......................................................................
                dataGridView1.Rows[(int)(GlobalVarLn.iW_stat - 1)].Cells[2].Value = (int)objWay.L_m; // L

                // ......................................................................
                // Соединить линией

                ClassMap.f_Map_Line_XY_stat(
                                      GlobalVarLn.X1_W_stat,
                                      GlobalVarLn.Y1_W_stat,
                                      GlobalVarLn.X2_W_stat,
                                      GlobalVarLn.Y2_W_stat
                                            );
                // ......................................................................
                textBox2.Text = Convert.ToString((int)GlobalVarLn.LW_stat);   // L
                textBox1.Text = Convert.ToString(GlobalVarLn.iW_stat);         // N
                // ......................................................................
                // Добавить в List

                GlobalVarLn.list_way.Add(objWay);
                // ......................................................................


            } // IF2

            // way
            else
            {
                objWay.L_m = 0;
                // ......................................................................
                // Добавить в List

                GlobalVarLn.list_way.Add(objWay);
                // ......................................................................
            }

 */


            // ......................................................................
            GlobalVarLn.iW_stat += 1; // Номер пункта

            Way objWay = new Way();
            // ......................................................................

            //if (GlobalVarLn.iW_stat == 1)
            //{
            //    // 1-й List пустой
            //    objWay.X_m = 0;
            //    objWay.Y_m = 0;
            //    objWay.L_m = 0;
            //    GlobalVarLn.list_way.Add(objWay);
            //}
            // ......................................................................

            GlobalVarLn.X1_W_stat = GlobalVarLn.X_StartW_stat;
            GlobalVarLn.Y1_W_stat = GlobalVarLn.Y_StartW_stat;
            GlobalVarLn.X2_W_stat = GlobalVarLn.X_Rastr;
            GlobalVarLn.Y2_W_stat = GlobalVarLn.Y_Rastr;
            GlobalVarLn.X_StartW_stat = GlobalVarLn.X2_W_stat;
            GlobalVarLn.Y_StartW_stat = GlobalVarLn.Y2_W_stat;

            objWay.X_m = GlobalVarLn.X_Rastr;
            objWay.Y_m = GlobalVarLn.Y_Rastr;
            // ......................................................................
            // Добавить строку

            var moveX = GlobalVarLn.LAT_Rastr;
            var moveY = GlobalVarLn.LONG_Rastr;
            dataGridView1.Rows[(int)(GlobalVarLn.iW_stat - 1)].Cells[0].Value = moveX.ToString("F3"); // X
            dataGridView1.Rows[(int)(GlobalVarLn.iW_stat - 1)].Cells[1].Value = moveY.ToString("F3"); // Y
            // ......................................................................

            // IF2
            if (GlobalVarLn.iW_stat > 1)
            {
                // ......................................................................
                //m
                GlobalVarLn.DXX_W_stat = GlobalVarLn.X2_W_stat - GlobalVarLn.X1_W_stat;
                GlobalVarLn.DYY_W_stat = GlobalVarLn.Y2_W_stat - GlobalVarLn.Y1_W_stat;

                objWay.L_m = Math.Sqrt(GlobalVarLn.DXX_W_stat * GlobalVarLn.DXX_W_stat +
                                       GlobalVarLn.DYY_W_stat * GlobalVarLn.DYY_W_stat);

                // ......................................................................
                GlobalVarLn.LW_stat += objWay.L_m;
                // ......................................................................
                dataGridView1.Rows[(int)(GlobalVarLn.iW_stat - 1)].Cells[2].Value = (int)objWay.L_m; // L
                // ......................................................................
                // ......................................................................
                textBox2.Text = Convert.ToString((int)GlobalVarLn.LW_stat);   // L
                textBox1.Text = Convert.ToString(GlobalVarLn.iW_stat);         // N
                // ......................................................................
                // Добавить в List

                GlobalVarLn.list_way.Add(objWay);
                // ......................................................................


            } // IF2

            // way
            else
            {
                objWay.L_m = 0;
                // ......................................................................
                // Добавить в List

                GlobalVarLn.list_way.Add(objWay);
                // ......................................................................
            }

            MapForm.REDRAW_MAP();

        } // P/P f_Way
        // *************************************************************************************

        // *************************************************************************************
        // Перерисовка маршрута
        // way
        // *************************************************************************************
        public void f_WayReDraw()
        {
          double x1 = 0;
          double y1 = 0;
          double x2 = 0;
          double y2 = 0;

            //0209

/*

          for (i = 1; i <= GlobalVarLn.iW_stat;i++ )
          {
           // ...............................................................................
           // 1-й элемент

              // IF1
              if (i == 1)
              {
                  ClassMap.f_Map_Rect_XYS_stat(
                                GlobalVarLn.list_way[i].X_m,
                                GlobalVarLn.list_way[i].Y_m,
                                ""
                               );
                  x1 = GlobalVarLn.list_way[i].X_m;
                  y1 = GlobalVarLn.list_way[i].Y_m;


              } // IF1
              // ...............................................................................
              //  НЕ 1-й элемент

              else
              {
                  ClassMap.f_Map_Rect_XYS_stat(
                                GlobalVarLn.list_way[i].X_m,
                                GlobalVarLn.list_way[i].Y_m,
                                ""
                               );
                  x2 = GlobalVarLn.list_way[i].X_m;
                  y2 = GlobalVarLn.list_way[i].Y_m;


                  ClassMap.f_Map_Line_XY_stat(
                                           x1,
                                           y1,
                                           x2,
                                           y2
                                              );
                  x1 = GlobalVarLn.list_way[i].X_m;
                  y1 = GlobalVarLn.list_way[i].Y_m;

              } // else
              // ...............................................................................


          } // FOR
*/

          //0209
          // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
          List<Mapsui.Geometries.Point> pointPel = new List<Mapsui.Geometries.Point>();
          double lat = 0;
          double lon = 0;

          for (int i = 0; i < GlobalVarLn.list_way.Count; i++)
          {
              var p = Mercator.ToLonLat(GlobalVarLn.list_way[i].X_m, GlobalVarLn.list_way[i].Y_m);
              lat = p.Y;
              lon = p.X;

              pointPel.Add(new Mapsui.Geometries.Point(lon, lat));

          } // FOR


          try
          {
              MapForm.RasterMapControl.AddPolyline(pointPel, Mapsui.Styles.Color.Red, 2);
          }
          catch
          {
              //MessageBox.Show(e.Message);
          }

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


        } // P/P f_WayReDraw
        // *************************************************************************************

        // *************************************************************************************
        // Закрыть форму

        private void FormWay_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

            //GlobalVarLn.fFWay_stat = 0;

            GlobalVarLn.fl_Open_objFormWay = 0;


        // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
        // Для маршрута (приостановить обработку, если идет)

            GlobalVarLn.blWay_stat = false;
        // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS

        }
        // *************************************************************************************
        // Активизировать форму

        private void FormWay_Activated(object sender, EventArgs e)
        {

            //GlobalVarLn.objFormWayG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFWay_stat = 1;

            GlobalVarLn.fl_Open_objFormWay = 1;

         // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
         // Для маршрута (возобновить обработку, если идет)

            if (chbWay.Checked == true)
            {
                GlobalVarLn.blWay_stat = true;
                GlobalVarLn.flEndWay_stat = 1;
            }
         // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS

            //ClassMap.f_RemoveFrm(8);

        }
        // *************************************************************************************
        // Загрузка формы

        private void FormWay_Load(object sender, EventArgs e)
        {
            
            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
            // Для маршрута
            // 1-я загрузка формы

            // .....................................................................................
            // Очистка dataGridView

            dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatWay_stat; i++)
            {
                dataGridView1.Rows.Add("", "", "");
            }
            // .....................................................................................
            if (chbWay.Checked == true)
            {
                GlobalVarLn.blWay_stat = true;
                GlobalVarLn.flEndWay_stat = 1;
            }

            GlobalVarLn.list_way.Clear();

            GlobalVarLn.iW_stat = 0;
            GlobalVarLn.X_StartW_stat = 0;
            GlobalVarLn.Y_StartW_stat = 0;
            GlobalVarLn.LW_stat = 0;
            // .....................................................................................

            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS


        }
        // *************************************************************************************
        // Изменение состояния cgbWay

        private void chbWay_CheckedChanged(object sender, EventArgs e)
        {

         // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
         // Для маршрута 

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            if (chbWay.Checked == false)
            {
                // .....................................................................................
                // Очистка dataGridView

                while (dataGridView1.Rows.Count != 0)
                    dataGridView1.Rows.Remove(dataGridView1.Rows[dataGridView1.Rows.Count - 1]);

                dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDatWay_stat; i++)
                {
                    dataGridView1.Rows.Add("", "", "");
                }
                // .....................................................................................
                GlobalVarLn.blWay_stat = false;
                GlobalVarLn.flEndWay_stat = 0;

                // way
                //Array.Clear(GlobalVarLn.mas_LW_stat, 0, 10000);
                //Array.Clear(GlobalVarLn.mas_XW_stat, 0, 10000);
                //Array.Clear(GlobalVarLn.mas_YW_stat, 0, 10000);
                GlobalVarLn.list_way.Clear();

                GlobalVarLn.iW_stat = 0;
                GlobalVarLn.X_StartW_stat = 0;
                GlobalVarLn.Y_StartW_stat = 0;
                GlobalVarLn.LW_stat = 0;
                // .....................................................................................
                textBox2.Text = "";   // L
                textBox1.Text = "";   // N
                // .....................................................................................
                // Убрать с карты

                //GlobalVarLn.axMapScreenGlobal.Repaint();
                // .....................................................................................

            }
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            else 
            {
                GlobalVarLn.blWay_stat = true;
                GlobalVarLn.flEndWay_stat = 1;

            }
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

         // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            // TTT
            //MapForm.REDRAW_MAP();


        } // Изменение состояния cgbWay
        // *************************************************************************************


        // ********************************************************************************* FUNCTIONS



    } // Class
} // namespace
