﻿namespace GrozaMap
{
    partial class MapForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MapForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.openMapToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.closeMapToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.openTheHeightMatrixToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ZoomIn = new System.Windows.Forms.ToolStripButton();
            this.ZoomOut = new System.Windows.Forms.ToolStripButton();
            this.ZoomInitial = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.bZoneSuppressAvia = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.MapElementHost = new System.Windows.Forms.Integration.ElementHost();
            this.panel7 = new System.Windows.Forms.Panel();
            this.lY = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.cmStripMap = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cmiGetCoord = new System.Windows.Forms.ToolStripMenuItem();
            this.принятьКакЦентрToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lPC = new System.Windows.Forms.Label();
            this.bConnectPC = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lLat = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lLon = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.lX = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.lH = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.lB = new System.Windows.Forms.Label();
            this.lIPaddr = new System.Windows.Forms.Label();
            this.tbIP = new System.Windows.Forms.TextBox();
            this.tbPort = new System.Windows.Forms.MaskedTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.toolStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel7.SuspendLayout();
            this.cmStripMap.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1,
            this.ZoomIn,
            this.ZoomOut,
            this.ZoomInitial,
            this.toolStripButton1,
            this.toolStripButton2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(982, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.TabStop = true;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openMapToolStripMenuItem1,
            this.closeMapToolStripMenuItem1,
            this.openTheHeightMatrixToolStripMenuItem});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(44, 22);
            this.toolStripDropDownButton1.Text = "Map";
            this.toolStripDropDownButton1.Click += new System.EventHandler(this.toolStripDropDownButton1_Click);
            // 
            // openMapToolStripMenuItem1
            // 
            this.openMapToolStripMenuItem1.Name = "openMapToolStripMenuItem1";
            this.openMapToolStripMenuItem1.Size = new System.Drawing.Size(198, 22);
            this.openMapToolStripMenuItem1.Text = "Open Map";
            this.openMapToolStripMenuItem1.Click += new System.EventHandler(this.openMapToolStripMenuItem1_Click);
            // 
            // closeMapToolStripMenuItem1
            // 
            this.closeMapToolStripMenuItem1.Name = "closeMapToolStripMenuItem1";
            this.closeMapToolStripMenuItem1.Size = new System.Drawing.Size(198, 22);
            this.closeMapToolStripMenuItem1.Text = "Close Map";
            this.closeMapToolStripMenuItem1.Click += new System.EventHandler(this.closeMapToolStripMenuItem1_Click);
            // 
            // openTheHeightMatrixToolStripMenuItem
            // 
            this.openTheHeightMatrixToolStripMenuItem.Name = "openTheHeightMatrixToolStripMenuItem";
            this.openTheHeightMatrixToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.openTheHeightMatrixToolStripMenuItem.Text = "Open the Height Matrix";
            this.openTheHeightMatrixToolStripMenuItem.Click += new System.EventHandler(this.openTheHeightMatrixToolStripMenuItem_Click);
            // 
            // ZoomIn
            // 
            this.ZoomIn.Image = ((System.Drawing.Image)(resources.GetObject("ZoomIn.Image")));
            this.ZoomIn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ZoomIn.Name = "ZoomIn";
            this.ZoomIn.Size = new System.Drawing.Size(99, 22);
            this.ZoomIn.Text = "Increase scale";
            this.ZoomIn.Click += new System.EventHandler(this.ZoomIn_Click);
            // 
            // ZoomOut
            // 
            this.ZoomOut.Image = ((System.Drawing.Image)(resources.GetObject("ZoomOut.Image")));
            this.ZoomOut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ZoomOut.Name = "ZoomOut";
            this.ZoomOut.Size = new System.Drawing.Size(103, 22);
            this.ZoomOut.Text = "Decrease scale";
            this.ZoomOut.Click += new System.EventHandler(this.ZoomOut_Click);
            // 
            // ZoomInitial
            // 
            this.ZoomInitial.Image = ((System.Drawing.Image)(resources.GetObject("ZoomInitial.Image")));
            this.ZoomInitial.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ZoomInitial.Name = "ZoomInitial";
            this.ZoomInitial.Size = new System.Drawing.Size(80, 22);
            this.ZoomInitial.Text = "Base scale";
            this.ZoomInitial.Click += new System.EventHandler(this.ZoomInitial_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.AutoSize = false;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(80, 22);
            this.toolStripButton1.Text = "GROZA-S1";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.AutoSize = false;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(80, 22);
            this.toolStripButton2.Text = "GROZA-S2";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button19);
            this.panel1.Controls.Add(this.button18);
            this.panel1.Controls.Add(this.button17);
            this.panel1.Controls.Add(this.button16);
            this.panel1.Controls.Add(this.bZoneSuppressAvia);
            this.panel1.Controls.Add(this.button15);
            this.panel1.Controls.Add(this.button14);
            this.panel1.Controls.Add(this.button13);
            this.panel1.Controls.Add(this.button12);
            this.panel1.Controls.Add(this.button11);
            this.panel1.Controls.Add(this.button10);
            this.panel1.Controls.Add(this.button9);
            this.panel1.Controls.Add(this.button8);
            this.panel1.Controls.Add(this.button7);
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(942, 25);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(40, 633);
            this.panel1.TabIndex = 5;
            // 
            // button2
            // 
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(-1, 743);
            this.button2.Margin = new System.Windows.Forms.Padding(0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(40, 36);
            this.button2.TabIndex = 27;
            this.toolTip1.SetToolTip(this.button2, "Расстояние между двумя пунктами");
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_2);
            // 
            // button19
            // 
            this.button19.Image = ((System.Drawing.Image)(resources.GetObject("button19.Image")));
            this.button19.Location = new System.Drawing.Point(0, 280);
            this.button19.Margin = new System.Windows.Forms.Padding(0);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(40, 36);
            this.button19.TabIndex = 26;
            this.toolTip1.SetToolTip(this.button19, "Azimuth");
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // button18
            // 
            this.button18.Image = ((System.Drawing.Image)(resources.GetObject("button18.Image")));
            this.button18.Location = new System.Drawing.Point(1, 525);
            this.button18.Margin = new System.Windows.Forms.Padding(0);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(40, 36);
            this.button18.TabIndex = 25;
            this.toolTip1.SetToolTip(this.button18, "Prohibited frequencies and frequencies of special attention\r\n");
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click);
            // 
            // button17
            // 
            this.button17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button17.Image = global::GrozaMap.Properties.Resources.SectorsRangesRP;
            this.button17.Location = new System.Drawing.Point(0, 490);
            this.button17.Margin = new System.Windows.Forms.Padding(0);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(40, 36);
            this.button17.TabIndex = 24;
            this.toolTip1.SetToolTip(this.button17, "Bands and sectors of jamming ");
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // button16
            // 
            this.button16.Image = global::GrozaMap.Properties.Resources.SectorsRangesRR;
            this.button16.Location = new System.Drawing.Point(0, 455);
            this.button16.Margin = new System.Windows.Forms.Padding(0);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(40, 36);
            this.button16.TabIndex = 23;
            this.toolTip1.SetToolTip(this.button16, "Bands and sectors of reconnaissance");
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // bZoneSuppressAvia
            // 
            this.bZoneSuppressAvia.Image = ((System.Drawing.Image)(resources.GetObject("bZoneSuppressAvia.Image")));
            this.bZoneSuppressAvia.Location = new System.Drawing.Point(1, 350);
            this.bZoneSuppressAvia.Name = "bZoneSuppressAvia";
            this.bZoneSuppressAvia.Size = new System.Drawing.Size(40, 36);
            this.bZoneSuppressAvia.TabIndex = 22;
            this.toolTip1.SetToolTip(this.bZoneSuppressAvia, "Control jamming zone");
            this.bZoneSuppressAvia.UseVisualStyleBackColor = true;
            this.bZoneSuppressAvia.Click += new System.EventHandler(this.bZoneSuppressAvia_Click);
            // 
            // button15
            // 
            this.button15.Image = ((System.Drawing.Image)(resources.GetObject("button15.Image")));
            this.button15.Location = new System.Drawing.Point(-1, 70);
            this.button15.Margin = new System.Windows.Forms.Padding(0);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(40, 36);
            this.button15.TabIndex = 14;
            this.toolTip1.SetToolTip(this.button15, "Enemy objectives");
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button14
            // 
            this.button14.Image = ((System.Drawing.Image)(resources.GetObject("button14.Image")));
            this.button14.Location = new System.Drawing.Point(3, 139);
            this.button14.Margin = new System.Windows.Forms.Padding(0);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(40, 36);
            this.button14.TabIndex = 13;
            this.toolTip1.SetToolTip(this.button14, "Enemy force front line");
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button13
            // 
            this.button13.Image = ((System.Drawing.Image)(resources.GetObject("button13.Image")));
            this.button13.Location = new System.Drawing.Point(0, 105);
            this.button13.Margin = new System.Windows.Forms.Padding(0);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(40, 36);
            this.button13.TabIndex = 12;
            this.toolTip1.SetToolTip(this.button13, "Friendly force front line");
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button12
            // 
            this.button12.Image = global::GrozaMap.Properties.Resources.ZO;
            this.button12.Location = new System.Drawing.Point(0, 175);
            this.button12.Margin = new System.Windows.Forms.Padding(0);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(40, 36);
            this.button12.TabIndex = 11;
            this.toolTip1.SetToolTip(this.button12, "Zone of responsibility");
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button11
            // 
            this.button11.Image = global::GrozaMap.Properties.Resources.SP;
            this.button11.Location = new System.Drawing.Point(-1, 0);
            this.button11.Margin = new System.Windows.Forms.Padding(0);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(40, 36);
            this.button11.TabIndex = 10;
            this.toolTip1.SetToolTip(this.button11, "Jammer station");
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button10
            // 
            this.button10.Image = ((System.Drawing.Image)(resources.GetObject("button10.Image")));
            this.button10.Location = new System.Drawing.Point(1, 315);
            this.button10.Margin = new System.Windows.Forms.Padding(0);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(40, 36);
            this.button10.TabIndex = 9;
            this.toolTip1.SetToolTip(this.button10, "Line of sight zone");
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button9
            // 
            this.button9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button9.Image = ((System.Drawing.Image)(resources.GetObject("button9.Image")));
            this.button9.Location = new System.Drawing.Point(-1, 35);
            this.button9.Margin = new System.Windows.Forms.Padding(0);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(40, 36);
            this.button9.TabIndex = 8;
            this.toolTip1.SetToolTip(this.button9, "Friendly force objects");
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button8
            // 
            this.button8.Image = ((System.Drawing.Image)(resources.GetObject("button8.Image")));
            this.button8.Location = new System.Drawing.Point(0, 420);
            this.button8.Margin = new System.Windows.Forms.Padding(0);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(40, 36);
            this.button8.TabIndex = 7;
            this.toolTip1.SetToolTip(this.button8, "Spoofing zone");
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button7
            // 
            this.button7.Image = ((System.Drawing.Image)(resources.GetObject("button7.Image")));
            this.button7.Location = new System.Drawing.Point(0, 385);
            this.button7.Margin = new System.Windows.Forms.Padding(0);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(40, 36);
            this.button7.TabIndex = 6;
            this.toolTip1.SetToolTip(this.button7, "Navigation jamming zone");
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.Image = ((System.Drawing.Image)(resources.GetObject("button6.Image")));
            this.button6.Location = new System.Drawing.Point(0, 245);
            this.button6.Margin = new System.Windows.Forms.Padding(0);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(40, 36);
            this.button6.TabIndex = 5;
            this.toolTip1.SetToolTip(this.button6, "Route");
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.Location = new System.Drawing.Point(0, 560);
            this.button5.Margin = new System.Windows.Forms.Padding(0);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(40, 36);
            this.button5.TabIndex = 4;
            this.toolTip1.SetToolTip(this.button5, "Table of reconnaissance");
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.Location = new System.Drawing.Point(1, 596);
            this.button4.Margin = new System.Windows.Forms.Padding(0);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(40, 36);
            this.button4.TabIndex = 3;
            this.toolTip1.SetToolTip(this.button4, "ScreenShot");
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button1
            // 
            this.button1.Image = global::GrozaMap.Properties.Resources.TRO;
            this.button1.Location = new System.Drawing.Point(0, 210);
            this.button1.Margin = new System.Windows.Forms.Padding(0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(40, 36);
            this.button1.TabIndex = 0;
            this.toolTip1.SetToolTip(this.button1, "Tactical and radio environment");
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.MapElementHost);
            this.panel2.Controls.Add(this.panel7);
            this.panel2.Location = new System.Drawing.Point(0, 25);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(942, 608);
            this.panel2.TabIndex = 8;
            // 
            // MapElementHost
            // 
            this.MapElementHost.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MapElementHost.Location = new System.Drawing.Point(0, 0);
            this.MapElementHost.Name = "MapElementHost";
            this.MapElementHost.Size = new System.Drawing.Size(942, 608);
            this.MapElementHost.TabIndex = 19;
            this.MapElementHost.Text = "elementHost1";
            this.MapElementHost.Child = null;
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel7.BackColor = System.Drawing.SystemColors.Control;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel7.Controls.Add(this.lY);
            this.panel7.Location = new System.Drawing.Point(632, 607);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(110, 26);
            this.panel7.TabIndex = 13;
            this.panel7.Visible = false;
            // 
            // lY
            // 
            this.lY.AutoSize = true;
            this.lY.Location = new System.Drawing.Point(4, 4);
            this.lY.Name = "lY";
            this.lY.Size = new System.Drawing.Size(0, 13);
            this.lY.TabIndex = 0;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "1.ico");
            // 
            // cmStripMap
            // 
            this.cmStripMap.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmiGetCoord,
            this.принятьКакЦентрToolStripMenuItem});
            this.cmStripMap.Name = "contextMenuStrip1";
            this.cmStripMap.Size = new System.Drawing.Size(178, 48);
            this.cmStripMap.Opening += new System.ComponentModel.CancelEventHandler(this.cmStripMap_Opening);
            // 
            // cmiGetCoord
            // 
            this.cmiGetCoord.Name = "cmiGetCoord";
            this.cmiGetCoord.Size = new System.Drawing.Size(177, 22);
            this.cmiGetCoord.Text = "Снять координаты";
            // 
            // принятьКакЦентрToolStripMenuItem
            // 
            this.принятьКакЦентрToolStripMenuItem.Name = "принятьКакЦентрToolStripMenuItem";
            this.принятьКакЦентрToolStripMenuItem.Size = new System.Drawing.Size(177, 22);
            this.принятьКакЦентрToolStripMenuItem.Text = "Принять как центр";
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel4.BackColor = System.Drawing.SystemColors.Control;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.lPC);
            this.panel4.Controls.Add(this.bConnectPC);
            this.panel4.Location = new System.Drawing.Point(1, 632);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(109, 26);
            this.panel4.TabIndex = 9;
            // 
            // lPC
            // 
            this.lPC.AutoSize = true;
            this.lPC.Location = new System.Drawing.Point(24, 6);
            this.lPC.Name = "lPC";
            this.lPC.Size = new System.Drawing.Size(21, 13);
            this.lPC.TabIndex = 85;
            this.lPC.Text = "PC";
            // 
            // bConnectPC
            // 
            this.bConnectPC.BackColor = System.Drawing.Color.Red;
            this.bConnectPC.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bConnectPC.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.bConnectPC.FlatAppearance.BorderSize = 0;
            this.bConnectPC.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.bConnectPC.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.bConnectPC.Location = new System.Drawing.Point(3, 1);
            this.bConnectPC.Name = "bConnectPC";
            this.bConnectPC.Size = new System.Drawing.Size(22, 22);
            this.bConnectPC.TabIndex = 84;
            this.bConnectPC.UseVisualStyleBackColor = false;
            this.bConnectPC.Click += new System.EventHandler(this.bConnectPC_Click);
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel5.BackColor = System.Drawing.SystemColors.Control;
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel5.Controls.Add(this.lLat);
            this.panel5.Location = new System.Drawing.Point(278, 632);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(144, 26);
            this.panel5.TabIndex = 10;
            // 
            // lLat
            // 
            this.lLat.AutoSize = true;
            this.lLat.Location = new System.Drawing.Point(5, 5);
            this.lLat.Name = "lLat";
            this.lLat.Size = new System.Drawing.Size(0, 13);
            this.lLat.TabIndex = 0;
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel6.BackColor = System.Drawing.SystemColors.Control;
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel6.Controls.Add(this.lLon);
            this.panel6.Location = new System.Drawing.Point(425, 632);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(151, 26);
            this.panel6.TabIndex = 11;
            // 
            // lLon
            // 
            this.lLon.AutoSize = true;
            this.lLon.Location = new System.Drawing.Point(6, 4);
            this.lLon.Name = "lLon";
            this.lLon.Size = new System.Drawing.Size(0, 13);
            this.lLon.TabIndex = 0;
            // 
            // panel8
            // 
            this.panel8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel8.BackColor = System.Drawing.SystemColors.Control;
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel8.Controls.Add(this.lX);
            this.panel8.Location = new System.Drawing.Point(725, 632);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(172, 26);
            this.panel8.TabIndex = 12;
            // 
            // lX
            // 
            this.lX.AutoSize = true;
            this.lX.Location = new System.Drawing.Point(4, 5);
            this.lX.Name = "lX";
            this.lX.Size = new System.Drawing.Size(0, 13);
            this.lX.TabIndex = 0;
            // 
            // panel9
            // 
            this.panel9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel9.BackColor = System.Drawing.SystemColors.Control;
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel9.Controls.Add(this.lH);
            this.panel9.Location = new System.Drawing.Point(577, 632);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(142, 26);
            this.panel9.TabIndex = 14;
            // 
            // lH
            // 
            this.lH.AutoSize = true;
            this.lH.Location = new System.Drawing.Point(4, 4);
            this.lH.Name = "lH";
            this.lH.Size = new System.Drawing.Size(0, 13);
            this.lH.TabIndex = 0;
            // 
            // panel10
            // 
            this.panel10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel10.BackColor = System.Drawing.SystemColors.Control;
            this.panel10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel10.Controls.Add(this.lB);
            this.panel10.Location = new System.Drawing.Point(878, 632);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(62, 26);
            this.panel10.TabIndex = 15;
            this.panel10.Visible = false;
            // 
            // lB
            // 
            this.lB.AutoSize = true;
            this.lB.Location = new System.Drawing.Point(4, 4);
            this.lB.Name = "lB";
            this.lB.Size = new System.Drawing.Size(0, 13);
            this.lB.TabIndex = 0;
            // 
            // lIPaddr
            // 
            this.lIPaddr.AutoSize = true;
            this.lIPaddr.Location = new System.Drawing.Point(3, 6);
            this.lIPaddr.Name = "lIPaddr";
            this.lIPaddr.Size = new System.Drawing.Size(57, 13);
            this.lIPaddr.TabIndex = 244;
            this.lIPaddr.Text = "IP address";
            // 
            // tbIP
            // 
            this.tbIP.Location = new System.Drawing.Point(61, 2);
            this.tbIP.MaxLength = 15;
            this.tbIP.Name = "tbIP";
            this.tbIP.Size = new System.Drawing.Size(87, 20);
            this.tbIP.TabIndex = 243;
            this.tbIP.Text = "127.0.0.1";
            // 
            // tbPort
            // 
            this.tbPort.Location = new System.Drawing.Point(33, 2);
            this.tbPort.Name = "tbPort";
            this.tbPort.Size = new System.Drawing.Size(37, 20);
            this.tbPort.TabIndex = 246;
            this.tbPort.Text = "9101";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label13.Location = new System.Drawing.Point(4, 6);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(26, 13);
            this.label13.TabIndex = 245;
            this.label13.Text = "Port";
            // 
            // panel11
            // 
            this.panel11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel11.BackColor = System.Drawing.SystemColors.Control;
            this.panel11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel11.Controls.Add(this.tbPort);
            this.panel11.Controls.Add(this.label1);
            this.panel11.Controls.Add(this.label13);
            this.panel11.Location = new System.Drawing.Point(203, 632);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(76, 26);
            this.panel11.TabIndex = 19;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 0;
            // 
            // panel12
            // 
            this.panel12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel12.BackColor = System.Drawing.SystemColors.Control;
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel12.Controls.Add(this.label2);
            this.panel12.Controls.Add(this.tbIP);
            this.panel12.Controls.Add(this.lIPaddr);
            this.panel12.Location = new System.Drawing.Point(49, 632);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(154, 26);
            this.panel12.TabIndex = 20;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 0;
            // 
            // timer1
            // 
            this.timer1.Interval = 300;
            // 
            // MapForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(982, 658);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.panel11);
            this.Controls.Add(this.panel8);
            this.Controls.Add(this.panel10);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel9);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(50, 50);
            this.Name = "MapForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = " Map";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MapForm_FormClosing);
            this.Load += new System.EventHandler(this.MapForm_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.cmStripMap.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton ZoomIn;
        private System.Windows.Forms.ToolStripButton ZoomOut;
        private System.Windows.Forms.ToolStripButton ZoomInitial;
        private System.Windows.Forms.Panel panel1;
        //private AxaxGisToolKit.AxaxOpenMapDialog axaxOpenMapDialog1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ImageList imageList1;
        //private AxaxGisToolKit.AxaxMapPoint axaxMapPoint1;
        private System.Windows.Forms.Button button11;
        //private AxaxGisToolKit.AxaxMapSelectDialog axaxMapSelDlg;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.ContextMenuStrip cmStripMap;
        private System.Windows.Forms.ToolStripMenuItem cmiGetCoord;
        private System.Windows.Forms.ToolStripMenuItem принятьКакЦентрToolStripMenuItem;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button bZoneSuppressAvia;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lPC;
        private System.Windows.Forms.Button bConnectPC;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label lLat;
        private System.Windows.Forms.Label lLon;
        private System.Windows.Forms.Label lY;
        private System.Windows.Forms.Label lX;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label lH;
        private System.Windows.Forms.Panel panel10;
        public System.Windows.Forms.Label lB;
        private System.Windows.Forms.MaskedTextBox tbPort;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lIPaddr;
        private System.Windows.Forms.TextBox tbIP;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button17;
        public System.Windows.Forms.SaveFileDialog saveFileDialog1;
        public System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem openMapToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem closeMapToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem openTheHeightMatrixToolStripMenuItem;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.Integration.ElementHost MapElementHost;
        public System.Windows.Forms.Timer timer1;
    }
}

