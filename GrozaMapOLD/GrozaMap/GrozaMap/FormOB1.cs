﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//using AxaxGisToolKit;
//using axGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;

//0206*
using System.Windows.Input;
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;
using Bitmap = System.Drawing.Bitmap;

namespace GrozaMap
{
    public partial class FormOB1 : Form
    {
/*
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);
*/

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // Конструктор *********************************************************** 

        public FormOB1()
        {
            InitializeComponent();



        } // Конструктор
        // ***********************************************************  Конструктор

        // ************************************************************************
        // Загрузка формы
        // ************************************************************************
        private void FormOB1_Load(object sender, EventArgs e)
        {

            if (GlobalVarLn.flEndTRO_stat != 1)
            {
                // -------------------------------------------------------------------
                // Очистка dataGridView1+ Установка 100 строк
                // 10_10_2018

                // Очистка dataGridView1
                while (dataGridView1.Rows.Count != 0)
                    dataGridView1.Rows.Remove(dataGridView1.Rows[dataGridView1.Rows.Count - 1]);

                for (int iyu = 0; iyu < GlobalVarLn.sizeDatOB1_stat; iyu++)
                {
                    dataGridView1.Rows.Add("", "", "", "");
                }
                // -------------------------------------------------------------------
                // .....................................................................................
                // Флаги

                GlobalVarLn.blOB1_stat = true;
                GlobalVarLn.flEndOB1_stat = 1;
                // .....................................................................................
                GlobalVarLn.iOB1_stat = 0;
                GlobalVarLn.X_OB1 = 0;
                GlobalVarLn.Y_OB1 = 0;
                GlobalVarLn.H_OB1 = 0;
                GlobalVarLn.list_OB1.Clear();
                GlobalVarLn.list1_OB1.Clear();
                // .....................................................................................
                GlobalVarLn.iZOB1 = 0;
                //pbOB1.Image = imageList1.Images[0];
                pbOB1.BackgroundImage = imageList1.Images[0];

            }

        } // Load_form
        // ************************************************************************

        // ************************************************************************
        // Очистка OB1
        // ************************************************************************
        private void bClear_Click(object sender, EventArgs e)
        {
            // ----------------------------------------------------------------------
            GlobalVarLn.blOB1_stat = true;
            GlobalVarLn.flEndOB1_stat = 1;
            // ----------------------------------------------------------------------
            // переменные

            GlobalVarLn.iOB1_stat = 0;
            GlobalVarLn.X_OB1 = 0;
            GlobalVarLn.Y_OB1 = 0;
            GlobalVarLn.H_OB1 = 0;
            // -------------------------------------------------------------------
            GlobalVarLn.list_OB1.Clear();
            GlobalVarLn.list1_OB1.Clear();
            // -------------------------------------------------------------------
            // -------------------------------------------------------------------
            // Очистка dataGridView1+ Установка 100 строк
            // 10_10_2018

            // Очистка dataGridView1
            while (dataGridView1.Rows.Count != 0)
                dataGridView1.Rows.Remove(dataGridView1.Rows[dataGridView1.Rows.Count - 1]);

            for (int iyu = 0; iyu < GlobalVarLn.sizeDatOB1_stat; iyu++)
            {
                dataGridView1.Rows.Add("", "", "", "");
            }
            // -------------------------------------------------------------------

            // -------------------------------------------------------------------
            // Убрать с карты

            //0209
            //GlobalVarLn.axMapScreenGlobal.Repaint();
            MapForm.REDRAW_MAP();
            // -------------------------------------------------------------------

        } // Clear

        // ************************************************************************

        // ************************************************************************
        // Обработчик кнопки : сохранить
        // ************************************************************************
        // 10_10_2018

        private void bAccept_Click(object sender, EventArgs e)
        {


/*          // OLD
 
            int i_tmp = 0;
            // -----------------------------------------------------------------------------------------
            String strFileName;
            strFileName = Application.StartupPath + "\\SaveInFiles\\ObjectsFriendly.txt";
            StreamWriter srFile;

            try
            {
                srFile = new StreamWriter(strFileName);
            }
            catch
            {
                MessageBox.Show("Can’t save file");
                return;
            }

            LF1 objLF = new LF1();

            // -----------------------------------------------------------------------------------------
            srFile.WriteLine("N =" + Convert.ToString(GlobalVarLn.iOB1_stat));

            for (i_tmp = 0; i_tmp < GlobalVarLn.iOB1_stat; i_tmp++)
            {

                //  Переписать содержимое таблицы в List
                //objLF.X_m = GlobalVarLn.list1_OB1[i_tmp].X_m;
                if ((dataGridView1.Rows[i_tmp].Cells[0].Value == "") ||
                    (dataGridView1.Rows[i_tmp].Cells[0].Value == null))
                {
                    MessageBox.Show("Incorrect data");
                    srFile.Close();
                    return;
                }
                try
                {
                    objLF.Lat = Convert.ToDouble(dataGridView1.Rows[i_tmp].Cells[0].Value);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    srFile.Close();
                    return;
                }

                //objLF.Y_m = GlobalVarLn.list1_OB1[i_tmp].Y_m;
                if ((dataGridView1.Rows[i_tmp].Cells[1].Value == "") ||
                    (dataGridView1.Rows[i_tmp].Cells[1].Value == null))
                {
                    MessageBox.Show("Incorrect data");
                    srFile.Close();
                    return;
                }
                try
                {
                    objLF.Long = Convert.ToDouble(dataGridView1.Rows[i_tmp].Cells[1].Value);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    srFile.Close();
                    return;
                }

                //objLF.H_m = Convert.ToDouble(dataGridView1.Rows[i_tmp].Cells[2].Value);
                if ((dataGridView1.Rows[i_tmp].Cells[2].Value == "") ||
                    (dataGridView1.Rows[i_tmp].Cells[2].Value == null))
                {
                    MessageBox.Show("Incorrect data");
                    srFile.Close();
                    return;
                }
                try
                {
                    objLF.H_m = Convert.ToDouble(dataGridView1.Rows[i_tmp].Cells[2].Value);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    srFile.Close();
                    return;
                }

                objLF.sType = Convert.ToString(dataGridView1.Rows[i_tmp].Cells[3].Value);
                objLF.indzn = GlobalVarLn.list1_OB1[i_tmp].indzn;

                var p = Mercator.FromLonLat(objLF.Long, objLF.Lat);

                objLF.X_m = p.X;
                objLF.Y_m = p.Y;

                GlobalVarLn.list1_OB1.RemoveAt(i_tmp);
                GlobalVarLn.list1_OB1.Insert(i_tmp, objLF);

                srFile.WriteLine("X =" + Convert.ToString((int)GlobalVarLn.list1_OB1[i_tmp].X_m));
                srFile.WriteLine("Y =" + Convert.ToString((int)GlobalVarLn.list1_OB1[i_tmp].Y_m));
                srFile.WriteLine("H =" + Convert.ToString((int)GlobalVarLn.list1_OB1[i_tmp].H_m));
                srFile.WriteLine("Type =" + Convert.ToString(GlobalVarLn.list1_OB1[i_tmp].sType));
                srFile.WriteLine("indzn =" + Convert.ToString(GlobalVarLn.list1_OB1[i_tmp].indzn));

            } // FOR
            // -------------------------------------------------------------------------------------

            srFile.Close();
            // ------------------------------------------------------------------------------------
            //axaxcMapScreen.Repaint();
            //f_OB1ReDraw();
            MapForm.REDRAW_MAP();
            // ------------------------------------------------------------------------------------
*/


            // 10_10_2018
            // ---------------------------------------------------------------------
            GlobalVarLn.blOB1_stat = true;
            GlobalVarLn.flEndOB1_stat = 1;
            GlobalVarLn.list1_OB1.Clear();
            // -----------------------------------------------------------------------------------------
            int i_tmp = 0;
            // -----------------------------------------------------------------------------------------
            String strFileName;
            strFileName = Application.StartupPath + "\\SaveInFiles\\ObjectsFriendly.txt";
            StreamWriter srFile;

            try
            {
                srFile = new StreamWriter(strFileName);
            }
            catch
            {
                MessageBox.Show("Can’t save file");
                return;
            }

            LF1 objLF = new LF1();
            // -----------------------------------------------------------------------------------------
            int ir = 0;
            int irf = 0;
            double lt = 0;
            double lng = 0;
            double freq = 0;
            String s = "";
            // -----------------------------------------------------------------------------------------

            // FOR >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            for (ir = 0; ir < dataGridView1.Rows.Count; ir++)
            {
                // IF**
                if (
                    ((dataGridView1.Rows[ir].Cells[0].Value != null) && (dataGridView1.Rows[ir].Cells[0].Value != "")) &&
                    ((dataGridView1.Rows[ir].Cells[1].Value != null) && (dataGridView1.Rows[ir].Cells[1].Value != ""))
                   )
                {
                    // -----------------------------------------------------------------
                    //  Latitude из таблицы (WGS84)

                    // IF1
                    if ((dataGridView1.Rows[ir].Cells[0].Value != "") &&
                        (dataGridView1.Rows[ir].Cells[0].Value != null))
                    {
                        s = Convert.ToString(dataGridView1.Rows[ir].Cells[0].Value);

                        try
                        {
                            lt = Convert.ToDouble(s);
                        }
                        catch (SystemException)
                        {
                            try
                            {
                                if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                                lt = Convert.ToDouble(s);
                            }
                            catch
                            {
                                MessageBox.Show("Incorrect data");
                                return;
                            }

                        } // catch

                    } // IF1
                    // -----------------------------------------------------------------
                    //  Longitude из таблицы (WGS84)

                    // IF2
                    if ((dataGridView1.Rows[ir].Cells[1].Value != "") &&
                        (dataGridView1.Rows[ir].Cells[1].Value != null))
                    {
                        s = Convert.ToString(dataGridView1.Rows[ir].Cells[1].Value);

                        try
                        {
                            lng = Convert.ToDouble(s);
                        }
                        catch (SystemException)
                        {
                            try
                            {
                                if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                                lng = Convert.ToDouble(s);
                            }
                            catch
                            {
                                MessageBox.Show("Incorrect data");
                                return;
                            }

                        } // catch

                    } // IF2
                    // -----------------------------------------------------------------
                    // Имя

                    s = Convert.ToString(dataGridView1.Rows[ir].Cells[3].Value);

                    // -----------------------------------------------------------------
                    // Получить координаты на карте

                    // grad->rad
                    //lt = (lt * Math.PI) / 180;
                    //lng = (lng * Math.PI) / 180;
                    // Подаем rad, получаем там же расстояние на карте в м
                    //mapGeoToPlane(GlobalVarLn.hmapl, ref lt, ref lng);

                    var x = lt; // Lat
                    var y = lng; //Long
                    // преобразование В меркатор
                    var p = Mercator.FromLonLat(y, x);
                    double xx = p.X;
                    double yy = p.Y;

                    double hhh = 0;
                    try
                    {
                        hhh = (double)MapForm.RasterMapControl.Dted.GetElevation(lng, lt);
                    }
                    catch
                    {
                        hhh = 0;
                    }

                    GlobalVarLn.X_OB1 = xx;
                    GlobalVarLn.Y_OB1 = yy;
                    // -----------------------------------------------------------------
                    // Заполнение структуры (X,Y,Name,Znak)

                    objLF.X_m = GlobalVarLn.X_OB1;
                    objLF.Y_m = GlobalVarLn.Y_OB1;
                    objLF.sType = s;

                    objLF.indzn = GlobalVarLn.iZOB1;

                    objLF.Lat = lt;
                    objLF.Long = lng;
                    // -----------------------------------------------------------------
                    // H

                    //GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.X_OB1, GlobalVarLn.Y_OB1);
                    //GlobalVarLn.H_OB1 = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                    GlobalVarLn.H_OB1 = hhh;

                    objLF.H_m = GlobalVarLn.H_OB1;
                    dataGridView1.Rows[ir].Cells[2].Value = GlobalVarLn.H_OB1;
                    // -----------------------------------------------------------------
                    // Добавить строку

                    GlobalVarLn.list1_OB1.Add(objLF);
                    // -----------------------------------------------------------------

                    irf += 1;

                } // IF**

            } // FOR
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> FOR

            // --------------------------------------------------------------------
            // Write in file

            srFile.WriteLine("N =" + Convert.ToString(GlobalVarLn.list1_OB1.Count));

            for (i_tmp = 0; i_tmp < GlobalVarLn.list1_OB1.Count; i_tmp++)
            {

                srFile.WriteLine("X =" + Convert.ToString((int)GlobalVarLn.list1_OB1[i_tmp].X_m));
                srFile.WriteLine("Y =" + Convert.ToString((int)GlobalVarLn.list1_OB1[i_tmp].Y_m));
                srFile.WriteLine("H =" + Convert.ToString((int)GlobalVarLn.list1_OB1[i_tmp].H_m));
                srFile.WriteLine("Type =" + Convert.ToString(GlobalVarLn.list1_OB1[i_tmp].sType));
                srFile.WriteLine("indzn =" + Convert.ToString(GlobalVarLn.list1_OB1[i_tmp].indzn));

            }
            // -------------------------------------------------------------------------------------

            srFile.Close();
            // ------------------------------------------------------------------------------------
            //axaxcMapScreen.Repaint();
            //f_OB1ReDraw();
            MapForm.REDRAW_MAP();
            // ------------------------------------------------------------------------------------



        } // Save to file
        // ************************************************************************

        // ************************************************************************
        // Обработчик кнопки : read from file
        // ************************************************************************
        //0209

        private void button1_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            String strLine = "";
            String strLine1 = "";

            double number1 = 0;
            int number2 = 0;

            char symb1 = 'N';
            char symb2 = 'X';
            char symb3 = 'Y';
            char symb4 = '=';
            char symb5 = 'H';
            char symb8 = 'T';
            char symb9 = 'i';

            int indStart = 0;
            int indStop = 0;
            int iLength = 0;

            int IndZap = 0;
            int TekPoz = 0;

            int fi = 0;

            // Очистка ---------------------------------------------------------------------------

            // ----------------------------------------------------------------------
            GlobalVarLn.blOB1_stat = true;
            GlobalVarLn.flEndOB1_stat = 1;
            // ----------------------------------------------------------------------
            // переменные

            GlobalVarLn.iOB1_stat = 0;
            GlobalVarLn.X_OB1 = 0;
            GlobalVarLn.Y_OB1 = 0;
            GlobalVarLn.H_OB1 = 0;
            // -------------------------------------------------------------------
            GlobalVarLn.list_OB1.Clear();
            GlobalVarLn.list1_OB1.Clear();
            // -------------------------------------------------------------------
            // Очистка dataGridView1

            while (dataGridView1.Rows.Count != 0)
                dataGridView1.Rows.Remove(dataGridView1.Rows[dataGridView1.Rows.Count - 1]);

            dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatOB1_stat; i++)
            {
                dataGridView1.Rows.Add("", "", "","");
            }
            // -------------------------------------------------------------------
            // Убрать с карты
            //0209

            //GlobalVarLn.axMapScreenGlobal.Repaint();
            // --------------------------------------------------------------------------- Очистка

            // Чтение файла ---------------------------------------------------------
            String strFileName;

            //strFileName = "OB1.txt";
            //0219
            strFileName = Application.StartupPath + "\\SaveInFiles\\ObjectsFriendly.txt";


            StreamReader srFile;
            try
            {
                srFile = new StreamReader(strFileName);
            }
            catch
            {
                MessageBox.Show("Can’t open file");
                return;

            }
            // -------------------------------------------------------------------------------------
            // Чтение
            // N =...
            // X =...
            // Y =...
            // H =...

            try
            {
                LF1 objLF = new LF1();

                TekPoz = 0;
                IndZap = 0;
                // .......................................................
                // N =...
                // 1-я строка

                strLine = srFile.ReadLine();

                if (strLine == null)
                {
                    MessageBox.Show("No information");
                    return;
                }

                indStart = strLine.IndexOf(symb1, TekPoz); // N

                if (indStart == -1)
                {
                    MessageBox.Show("No information");
                    return;
                }

                indStop = strLine.IndexOf(symb4, TekPoz);  //=

                if ((indStop == -1) || (indStop < indStart))
                {
                    MessageBox.Show("No information");
                    return;
                }

                iLength = indStop - indStart + 1;
                // Убираем 'N ='
                strLine1 = strLine.Remove(indStart, iLength);

                if (strLine1 == "")
                {
                    MessageBox.Show("No information");
                    return;
                }

                // Количество 
                number2 = Convert.ToInt32(strLine1);
                GlobalVarLn.iOB1_stat = (uint)number2;
                // .......................................................

                fi = 0;
                strLine = srFile.ReadLine(); // читаем далее (X1)
                if ((strLine == "") || (strLine == null))
                {
                    fi = 1;
                }
                // .......................................................

                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH
                while ((strLine != "") && (strLine != null))
                {
                    IndZap += 1;

                    // .......................................................
                    // X =...

                    indStart = strLine.IndexOf(symb2, TekPoz); // X
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    number1 = Convert.ToInt32(strLine1);

                    objLF.X_m = number1;
                    // .......................................................
                    // Y =...

                    strLine = srFile.ReadLine();

                    indStart = strLine.IndexOf(symb3, TekPoz); // Y
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    number1 = Convert.ToInt32(strLine1);

                    objLF.Y_m = number1;
                    // .......................................................
                    // H =...

                    strLine = srFile.ReadLine();

                    indStart = strLine.IndexOf(symb5, TekPoz); // H
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    number1 = Convert.ToInt32(strLine1);

                    objLF.H_m = number1;
                    // .......................................................
                    // Type =...

                    strLine = srFile.ReadLine();

                    indStart = strLine.IndexOf(symb8, TekPoz); // T
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);

                    objLF.sType = strLine1;
                    // .......................................................
                    // indzn =...

                    strLine = srFile.ReadLine();

                    indStart = strLine.IndexOf(symb9, TekPoz); // i
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    number1 = Convert.ToInt32(strLine1);

                    objLF.indzn = (int)number1;
                    // .......................................................
                    //0209
                    //Merkator

                    var p5 = Mercator.ToLonLat(objLF.X_m, objLF.Y_m);
                    objLF.Lat = p5.Y;
                    objLF.Long = p5.X;
                    // .......................................................
                    // Занести в List

                    GlobalVarLn.list1_OB1.Add(objLF);
                    // .......................................................
                    // Занести в таблицу
                    //0209

                    //var p = axaxcMapScreen.MapPlaneToRealGeo(objLF.X_m, objLF.Y_m);

                    dataGridView1.Rows[(int)(IndZap - 1)].Cells[0].Value = objLF.Lat.ToString("F3");
                    dataGridView1.Rows[(int)(IndZap - 1)].Cells[1].Value = objLF.Long.ToString("F3");  // Y
                    dataGridView1.Rows[(int)(IndZap - 1)].Cells[2].Value = objLF.H_m;  // H
                    dataGridView1.Rows[(int)(IndZap - 1)].Cells[3].Value = objLF.sType;  // Type
                    // ............................................................

                    fi = 0;
                    strLine = srFile.ReadLine(); // читаем далее (Xi)
                    if ((strLine == "") || (strLine == null))
                        fi = 1;
                    // ...................................................................

                } // WHILE
                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH


            }
            // -----------------------------------------------------------------------------------
            catch
            {

            }
            // -------------------------------------------------------------------------------------
            srFile.Close();
            // -------------------------------------------------------------------------------------

            //0209
            //f_OB1ReDraw();
            MapForm.REDRAW_MAP();

        } // Read from file
        // ************************************************************************

        // ************************************************************************
        // Удалить объект
        // ************************************************************************
        //0209

        private void button3_Click(object sender, EventArgs e)
        {
            // -----------------------------------------------------------------------------------------
            String strLine2 = "";
            String strLine3 = "";

            int it = 0;
            int index = 0;
            // -----------------------------------------------------------------------------------------
            // Если открыта таблица

            if (GlobalVarLn.iOB1_stat != 0)
            {

             index=dataGridView1.CurrentRow.Index;
             if (index >= GlobalVarLn.list1_OB1.Count)
             {
                 return;
             }


                 // Убрать с таблицы
                 dataGridView1.Rows.Remove(dataGridView1.Rows[index]);
                 // Del from list
                 GlobalVarLn.list1_OB1.Remove(GlobalVarLn.list1_OB1[index]);
                 GlobalVarLn.iOB1_stat -= 1;

                 // Убрать с карты
                 //GlobalVarLn.axMapScreenGlobal.Repaint();

            } // IF(GlobalVarLn.iSP_stat != 0)
            // -------------------------------------------------------------------------------------

            //f_OB1ReDraw();
            MapForm.REDRAW_MAP();

        } // Удалить
        // ************************************************************************


        // ФУНКЦИИ ********************************************************************************

        // ****************************************************************************************
        // Обработка нажатия левой кнопки мыши при отрисовке OB1
        //
        // Входные параметры:
        // X - X, m на местности
        // Y - Y, m
        // ****************************************************************************************
        public void f_OB1(
                          double X,
                          double Y
                         )
        {



/*
            // ......................................................................

            LF1 objLF = new LF1();
            // ......................................................................
            // !!! реальные координаты на местности карты в м (Plane)

            GlobalVarLn.X_OB1 = GlobalVarLn.MapX1;
            GlobalVarLn.Y_OB1 = GlobalVarLn.MapY1;
            objLF.X_m = GlobalVarLn.X_OB1;
            objLF.Y_m = GlobalVarLn.Y_OB1;
            objLF.indzn = GlobalVarLn.iZOB1;
            // ......................................................................
            // ......................................................................
            GlobalVarLn.blOB1_stat = true;
            GlobalVarLn.flEndOB1_stat = 1;
            // ......................................................................
            // H

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.MapX1, GlobalVarLn.MapY1);
            GlobalVarLn.H_OB1 = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            objLF.H_m = GlobalVarLn.H_OB1;
            // ......................................................................
            GlobalVarLn.iOB1_stat += 1;
            // ......................................................................
            // Добавить строку

            var p = axaxcMapScreen.MapPlaneToRealGeo(objLF.X_m, objLF.Y_m);

            dataGridView1.Rows[(int)(GlobalVarLn.iOB1_stat - 1)].Cells[0].Value = p.X.ToString("F3"); // X
            dataGridView1.Rows[(int)(GlobalVarLn.iOB1_stat - 1)].Cells[1].Value = p.Y.ToString("F3"); // Y
            dataGridView1.Rows[(int)(GlobalVarLn.iOB1_stat - 1)].Cells[2].Value = (int)GlobalVarLn.H_OB1; // H

            // ......................................................................
            // Добавить в List

            GlobalVarLn.list1_OB1.Add(objLF);
            // ---------------------------------------------------------------------------------
            // Перерисовать

            GlobalVarLn.axMapScreenGlobal.Repaint();

            f_OB1ReDraw();
            // ---------------------------------------------------------------------------------
*/


            // ......................................................................

            LF1 objLF = new LF1();
            // ......................................................................
            // !!! Merkator в м 

            GlobalVarLn.X_OB1 = GlobalVarLn.X_Rastr;
            GlobalVarLn.Y_OB1 = GlobalVarLn.Y_Rastr;
            objLF.X_m = GlobalVarLn.X_OB1;
            objLF.Y_m = GlobalVarLn.Y_OB1;

            GlobalVarLn.H_OB1 = GlobalVarLn.H_Rastr;
            objLF.H_m = GlobalVarLn.H_OB1;

            objLF.Lat = GlobalVarLn.LAT_Rastr;   //grad WGS84
            objLF.Long = GlobalVarLn.LONG_Rastr; //grad WGS84

            objLF.indzn = GlobalVarLn.iZOB1;
            // ......................................................................
            // ......................................................................
            GlobalVarLn.blOB1_stat = true;
            GlobalVarLn.flEndOB1_stat = 1;
            // ......................................................................
            GlobalVarLn.iOB1_stat += 1;
            // ......................................................................
            // Добавить строку

            //var p = axaxcMapScreen.MapPlaneToRealGeo(objLF.X_m, objLF.Y_m);

            dataGridView1.Rows[(int)(GlobalVarLn.iOB1_stat - 1)].Cells[0].Value = objLF.Lat.ToString("F3"); 
            dataGridView1.Rows[(int)(GlobalVarLn.iOB1_stat - 1)].Cells[1].Value = objLF.Long.ToString("F3");
            dataGridView1.Rows[(int)(GlobalVarLn.iOB1_stat - 1)].Cells[2].Value = (int)GlobalVarLn.H_OB1; // H

            // ......................................................................
            // Добавить в List

            GlobalVarLn.list1_OB1.Add(objLF);
            // ---------------------------------------------------------------------------------
            // Перерисовать

            MapForm.REDRAW_MAP();
            // ---------------------------------------------------------------------------------


        } // P/P f_OB1
        // *************************************************************************************

        // *************************************************************************************
        // Перерисовка OB1
        // *************************************************************************************
        //0209 !!!

        public void f_OB1ReDraw()
        {
            //ClassMap.f_OB1_stat();

            String s1 = "";
            int i = 0;

            IMapObject objectGrozaS1;
            MapObjectStyle _placeObjectStyleOwn;
            Mapsui.Geometries.Point pointOwn = new Mapsui.Geometries.Point();

            // -----------------------------------------------------------------
            for (i = 0; i < GlobalVarLn.list1_OB1.Count; i++)
            {
                try
                {

                    s1 = GlobalVarLn.list1_OB1[i].sType;
                    pointOwn.X = GlobalVarLn.list1_OB1[i].Long;
                    pointOwn.Y = GlobalVarLn.list1_OB1[i].Lat;

                    _placeObjectStyleOwn = MapForm.RasterMapControl.LoadObjectStyle(
                        (Bitmap)GlobalVarLn.objFormOB1G.imageList1.Images[GlobalVarLn.list1_OB1[i].indzn],

                        //0216
                        //scale: 0.8,
                        scale: 0.6,

                        objectOffset: new Offset(0, 0),
                        textOffset: new Offset(0, 15)
                        );
                    objectGrozaS1 = MapForm.RasterMapControl.AddMapObject(_placeObjectStyleOwn, s1, pointOwn);

                }
                catch (Exception ex)
                {
                    // ignored
                }

            } // FOR
            // -----------------------------------------------------------------


        } // P/P f_OB1ReDraw
        // *************************************************************************************

        // ******************************************************************************** ФУНКЦИИ


        // ****************************************************************************************
        // Закрыть форму
        // ****************************************************************************************
        private void FormOB1_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

            // приостановить обработку, если идет
            GlobalVarLn.blOB1_stat = false;

            //GlobalVarLn.fFOB1 = 0;
            GlobalVarLn.fl_Open_objFormOB1 = 0;


        } // Closing

        // ****************************************************************************************

        // *************************************************************************************
        // Активизировать форму
        // *************************************************************************************
        private void FormOB1_Activated(object sender, EventArgs e)
        {
            //GlobalVarLn.objFormOB1G.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFOB1 = 1;

            GlobalVarLn.blOB1_stat = true;
            GlobalVarLn.flEndOB1_stat = 1;

           // 0809_3
           //ClassMap.f_RemoveFrm(2);
            GlobalVarLn.fl_Open_objFormOB1 = 1;


        } // Activated

        // ****************************************************************************************
        // Значок
        // ****************************************************************************************
        private void buttonZOB1_Click(object sender, EventArgs e)
        {
            GlobalVarLn.iZOB1 += 1;
            if (GlobalVarLn.iZOB1 == imageList1.Images.Count)
                GlobalVarLn.iZOB1 = 0;
            pbOB1.BackgroundImage = imageList1.Images[GlobalVarLn.iZOB1];
            //pbOB1.Image = imageList1.Images[GlobalVarLn.iZOB1];

        }
        private void button4_Click(object sender, EventArgs e)
        {
            GlobalVarLn.iZOB1 -= 1;
            if (GlobalVarLn.iZOB1 < 0)
                GlobalVarLn.iZOB1 = imageList1.Images.Count-1;
            pbOB1.BackgroundImage = imageList1.Images[GlobalVarLn.iZOB1];
        } 


        private void tbNumSP_TextChanged(object sender, EventArgs e)
        {

        } // Значок

        private void button2_Click(object sender, EventArgs e)
        {
            ;

        }

        // ****************************************************************************************


    } // Class
} // Namespace
