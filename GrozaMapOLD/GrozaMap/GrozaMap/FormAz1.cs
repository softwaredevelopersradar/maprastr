﻿using System;
using System.Drawing;
//using AxaxGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;

//0206*
using System.Windows.Input;
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;
using Bitmap = System.Drawing.Bitmap;

using System.Linq;


namespace GrozaMap
{
    public partial class FormAz1 : Form
    {
/*
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);
*/

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        private double dchislo;
        private long ichislo;
        //private double LAMBDA;

        // .....................................................................
        // Координаты центра ЗПВ

        // Координаты центра ЗПВ на местности в м
        //private double XSP_comm;
        //private double YSP_comm;

        // DATUM
        private double dXdat_comm;
        private double dYdat_comm;
        private double dZdat_comm;

        private double dLat_comm;
        private double dLong_comm;

        // Эллипсоид Красовского, град
        private double LatKrG_comm;
        private double LongKrG_comm;
        // Эллипсоид Красовского, rad
        private double LatKrR_comm;
        private double LongKrR_comm;
        // Эллипсоид Красовского, град,мин,сек
        private int Lat_Grad_comm;
        private int Lat_Min_comm;
        private double Lat_Sec_comm;
        private int Long_Grad_comm;
        private int Long_Min_comm;
        private double Long_Sec_comm;
        // Гаусс-крюгер(СК42) м
        private double XSP42_comm;
        private double YSP42_comm;

        // Конструктор *********************************************************** 

        public FormAz1()
        {
            InitializeComponent();


            dchislo = 0;
            ichislo = 0;
            //LAMBDA = 300000;

            // .....................................................................
            // Координаты центра ЗПВ

            // Координаты центра ЗПВ на местности в м
            //XSP_comm = 0;
            //YSP_comm = 0;

            // DATUM
            // ГОСТ 51794_2008
            dXdat_comm = 25;
            dYdat_comm = -141;
            dZdat_comm = -80;

            dLat_comm = 0;
            dLong_comm = 0;

            // Эллипсоид Красовского, град
            LatKrG_comm = 0;
            LongKrG_comm = 0;
            // Эллипсоид Красовского, rad
            LatKrR_comm = 0;
            LongKrR_comm = 0;
            // Эллипсоид Красовского, град,мин,сек
            Lat_Grad_comm = 0;
            Lat_Min_comm = 0;
            Lat_Sec_comm = 0;
            Long_Grad_comm = 0;
            Long_Min_comm = 0;
            Long_Sec_comm = 0;
            // Гаусс-крюгер(СК42) м
            XSP42_comm = 0;
            YSP42_comm = 0;

            GlobalVarLn.ListJSChangedEvent += OnListJSChanged;

        } // Конструктор
        // ***********************************************************  Конструктор

        // ************************************************************************
        // Загрузка формы
        // ************************************************************************
        private void FormAz1_Load(object sender, EventArgs e)
        {
            // ----------------------------------------------------------------------
            gbRect.Visible = true;
            // ----------------------------------------------------------------------
            //chbXY.Checked = false;
            // ----------------------------------------------------------------------
            // Очистка dataGridView

            GlobalVarLn.objFormAz1G.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatOB_az1; i++)
            {
                GlobalVarLn.objFormAz1G.dataGridView1.Rows.Add("", "", "", "");
            }
            // ----------------------------------------------------------------------
            GlobalVarLn.blOB_az1 = true;
            GlobalVarLn.flEndOB_az1 = 1;
            GlobalVarLn.flCoordOB_az1 = 0;
            GlobalVarLn.XCenter_az1 = 0;
            GlobalVarLn.YCenter_az1 = 0;
            GlobalVarLn.HCenter_az1 = 0;

            UpdateDataGridView();


        } // Load_form

        private void OnListJSChanged(object sender, EventArgs e)
        {
            UpdateDataGridView();
        }

        private void UpdateDataGridView()
        {
            var stations = GlobalVarLn.listJS.ToList();


            for (var i = 0; i < stations.Count && i < dataGridView1.RowCount; ++i)
            {
                var row = dataGridView1.Rows[i];

                //0209
                //var pos = axaxcMapScreen.MapPlaneToRealGeo(
                //    stations[i].CurrentPosition.x,
                //    stations[i].CurrentPosition.y);
                //row.Cells[0].Value = stations[i].Name;
                //row.Cells[1].Value = pos.X.ToString("F3");
                //row.Cells[2].Value = pos.Y.ToString("F3");

                //0209
                var pos = Mercator.ToLonLat(stations[i].CurrentPosition.x, stations[i].CurrentPosition.y);
                row.Cells[0].Value = stations[i].Name;
                row.Cells[1].Value = pos.Y.ToString("F3");
                row.Cells[2].Value = pos.X.ToString("F3");


            }
        }
        // ************************************************************************

        // ************************************************************************
        // Очистка
        // ************************************************************************
        private void bClear_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------
            tbXRect.Text = "";
            tbYRect.Text = "";
            tbOwnHeight.Text = "";
            // -------------------------------------------------------------------
            chbXY.Checked = false;
            
            GlobalVarLn.blOB_az1 = true;
            GlobalVarLn.flEndOB_az1 = 1;
            GlobalVarLn.flCoordOB_az1 = 0;
            GlobalVarLn.XCenter_az1 = 0;
            GlobalVarLn.YCenter_az1 = 0;
            GlobalVarLn.HCenter_az1 = 0;

            dataGridView1.Rows[0].Cells[3].Value = "";
            dataGridView1.Rows[1].Cells[3].Value = "";

            // ----------------------------------------------------------------------
            // Убрать с карты

            //0209
            //GlobalVarLn.axMapScreenGlobal.Repaint();
            MapForm.REDRAW_MAP();

            // -------------------------------------------------------------------


        } // Clear
        // ************************************************************************

        // ************************************************************************
        // Обработчик ComboBox "cbChooseSC": Выбор СК
        // ************************************************************************
        private void cbChooseSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ChooseSystemCoord_az1(cbChooseSC.SelectedIndex);

        } // Выбор СК
        // ************************************************************************

        // ************************************************************************
        // Принять (посчитать азимут)
        // ************************************************************************
        private void button2_Click(object sender, EventArgs e)
        {
            f_OB_az1(0, 0);

            var objClassMap6 = new ClassMap();
            var stations = GlobalVarLn.listJS
                .Where(s => s.HasCurrentPosition)
                .ToList();
            

            for (var i = 0; i < stations.Count; i++)
            {
                var X_Coordl3_1 = stations[i].CurrentPosition.x;
                var Y_Coordl3_1 = stations[i].CurrentPosition.y;
                var X_Coordl3_2 = GlobalVarLn.XCenter_az1;
                var Y_Coordl3_2 = GlobalVarLn.YCenter_az1;

                //0209  !!!Y -вверх
                // Разность координат для расчета азимута
                // На карте X - вверх
                //var DXX3 = Y_Coordl3_2 - Y_Coordl3_1;
                //var DYY3 = X_Coordl3_2 - X_Coordl3_1;
                var DXX3 = X_Coordl3_2 - X_Coordl3_1;
                var DYY3 = Y_Coordl3_2 - Y_Coordl3_1;

                // grad
                var azz = objClassMap6.f_Def_Azimuth(DYY3, DXX3);


                ichislo = (long) (azz * 100);
                dchislo = ((double) ichislo) / 100;
                dataGridView1.Rows[i].Cells[3].Value = (int) azz;
            }

            //0209
            //GlobalVarLn.axMapScreenGlobal.Repaint();
            MapForm.REDRAW_MAP();

        }

        // NEW NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWN
        private void OtobrOB_az1()
        {
            double lat = 0;
            double lon=0;
            // -----------------------------------------------------------------------
            // Метры на местности

            //0209
            //var p = axaxcMapScreen.MapPlaneToRealGeo(GlobalVarLn.XCenter_az1, GlobalVarLn.YCenter_az1);
            var p = Mercator.ToLonLat(GlobalVarLn.XCenter_az1, GlobalVarLn.YCenter_az1);
            lat = p.Y;
            lon = p.X;
            tbXRect.Text = lat.ToString("F3");
            tbYRect.Text = lon.ToString("F3");
            // ----------------------------------------------------------------------

        } // OtobrOB_az1

        // NEW NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWN
  
        // ************************************************************************

        // NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEW
        public void f_OB_az1(double X, double Y)
        {

            double lat = 0;
            double lon = 0;
            double xtmp1_ed = 0;
            double ytmp1_ed = 0;

            // ......................................................................
            //var objClassMap3_ed = new ClassMap();
            // ......................................................................
            // !!! реальные координаты на местности карты в м (Plane)

            GlobalVarLn.XCenter_az1 = GlobalVarLn.XXXaz;
            GlobalVarLn.YCenter_az1 = GlobalVarLn.YYYaz;

            //0209
            //GlobalVarLn.objFormWayG.f_Map_Rect_XY2(GlobalVarLn.XCenter_az1, GlobalVarLn.YCenter_az1);

            GlobalVarLn.blOB_az1 = true;
            GlobalVarLn.flEndOB_az1 = 1;
            GlobalVarLn.flCoordOB_az1 = 1;
            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            var xtmp_ed = GlobalVarLn.XCenter_az1;
            var ytmp_ed = GlobalVarLn.YCenter_az1;

            //0209
            //mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);
            var p = Mercator.ToLonLat(xtmp_ed, ytmp_ed);
            lat = p.Y;
            lon = p.X;
            xtmp1_ed = lat;
            ytmp1_ed = lon;
            GlobalVarLn.Lat84_az1 = lat;
            GlobalVarLn.Long84_az1 = lon;


            //0209
            // rad(WGS84)->grad(WGS84)
            //var xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            //var ytmp1_ed = (ytmp_ed * 180) / Math.PI;
            // .......................................................................
            // CDel

//0209
/*
            // WGS84,grad
            LatKrG_comm = xtmp1_ed;
            LongKrG_comm = ytmp1_ed;
            // .......................................................................

            LatKrR_comm = (LatKrG_comm * Math.PI) / 180;
            LongKrR_comm = (LongKrG_comm * Math.PI) / 180;

            objClassMap3_ed.f_Grad_GMS
            (
                // Входные параметры (grad)
                LatKrG_comm,

                // Выходные параметры 
                ref Lat_Grad_comm,
                ref Lat_Min_comm,
                ref Lat_Sec_comm

            );

            // Долгота
            objClassMap3_ed.f_Grad_GMS
            (
                // Входные параметры (grad)
                LongKrG_comm,

                // Выходные параметры 
                ref Long_Grad_comm,
                ref Long_Min_comm,
                ref Long_Sec_comm

            );
*/

            OtobrOB_az1();

            // .......................................................................
            // H

            //0209
            //GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.XXXaz, GlobalVarLn.YYYaz);
            //GlobalVarLn.HCenter_az1 = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            GlobalVarLn.HCenter_az1 = GlobalVarLn.H_Rastr;
            tbOwnHeight.Text = Convert.ToString((int) GlobalVarLn.HCenter_az1);
            // .......................................................................

            //0209
            //axaxcMapScreen.Repaint();
            //MapForm.REDRAW_MAP();
        }

        // NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEW

        // *************************************************************************************

        // *************************************************************************************
        // Перерисовка Object
        // *************************************************************************************
        //0209

        public void f_OBReDraw_Az1()
        {

            //GlobalVarLn.objFormWayG.f_Map_Rect_XY2(GlobalVarLn.XCenter_az1, GlobalVarLn.YCenter_az1);

            IMapObject objectGrozaS1;
            MapObjectStyle _placeObjectStyleOwn;
            string strExePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            Mapsui.Geometries.Point pointOwn = new Mapsui.Geometries.Point();

            try
            {

                var p3 = Mercator.ToLonLat(GlobalVarLn.XCenter_az1, GlobalVarLn.YCenter_az1);
                pointOwn.X = p3.X;
                pointOwn.Y = p3.Y;

                _placeObjectStyleOwn = MapForm.RasterMapControl.LoadObjectStyle(
                    "SquareRed.png",
                    // +strExePath + "\\Images\\Jammer\\1.png",
                    //(Bitmap)GlobalVarLn.objFormSPG.imageList1.Images[0],
                    scale: 0.6,
                    objectOffset: new Offset(0, 0),
                    textOffset: new Offset(0, 15)
                    );
                objectGrozaS1 = MapForm.RasterMapControl.AddMapObject(_placeObjectStyleOwn, "", pointOwn);

            }
            catch (Exception ex)
            {
                // ignored
            }

        }

        // ****************************************************************************************
        // Закрыть форму
        // ****************************************************************************************
        private void FormAz1_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

            // приостановить обработку, если идет
            GlobalVarLn.blOB_az1 = false;

            GlobalVarLn.fl_Open_objFormAz1 = 0;

            //GlobalVarLn.fFAz1 = 0;


        } // Closing
        // ****************************************************************************************
        // Активизировать форму
        // *************************************************************************************
        private void FormAz1_Activated(object sender, EventArgs e)
        {
            //GlobalVarLn.objFormAz1G.WindowState = FormWindowState.Normal;

            GlobalVarLn.blOB_az1 = true;
            GlobalVarLn.flEndOB_az1 = 1;

            GlobalVarLn.fl_Open_objFormAz1 = 1;

            // otl33
            //GlobalVarLn.flEndSP_stat = GlobalVarLn.flEndSP_stat;
            //GlobalVarLn.LoadListJS();
            //GlobalVarLn.ListJSChangedEvent += OnListJSChanged;
            //UpdateDataGridView();

            //GlobalVarLn.fFAz1 = 1;
            //ClassMap.f_RemoveFrm(9);

        }

        private void label15_Click(object sender, EventArgs e)
        {
            //ClassMap.f_RemoveFrm(9);

        } // Activated


        // ****************************************************************************************





    } // Class
} // namespace
