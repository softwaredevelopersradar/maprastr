﻿using System;
using System.Drawing;
//using AxaxGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;


//0206*
using System.Windows.Input;
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;
using Bitmap = System.Drawing.Bitmap;

namespace GrozaMap
{
    public partial class FormSP : Form
    {
        //0206*
        //public RasterMapControl RasterMapControl1 { get; private set; }

/*
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);
*/

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        private double dchislo;
        private long ichislo;
        //private double LAMBDA;

        // .....................................................................
        // Координаты центра ЗПВ

        // Координаты центра ЗПВ на местности в м
        //private double XSP_comm;
        //private double YSP_comm;

        // DATUM
        private double dXdat_comm;
        private double dYdat_comm;
        private double dZdat_comm;

        private double dLat_comm;
        private double dLong_comm;

        // Эллипсоид Красовского, град
        private double LatKrG_comm;
        private double LongKrG_comm;
        // Эллипсоид Красовского, rad
        private double LatKrR_comm;
        private double LongKrR_comm;
        // Эллипсоид Красовского, град,мин,сек
        private int Lat_Grad_comm;
        private int Lat_Min_comm;
        private double Lat_Sec_comm;
        private int Long_Grad_comm;
        private int Long_Min_comm;
        private double Long_Sec_comm;
        // Гаусс-крюгер(СК42) м
        private double XSP42_comm;
        private double YSP42_comm;

        private RadioButton[] radioButtons;

        private SPView[] spViews;

        private int _checkedRadioButtonIndex;

        

        private int CheckedRadioButtonIndex 
        {
            get { return _checkedRadioButtonIndex; }
            set
            {
                if (value == _checkedRadioButtonIndex)
                {
                    return;
                }
                if (value < 0 || value >= radioButtons.Length)
                {
                    return;
                }
                _checkedRadioButtonIndex = value;
                radioButtons[value].Checked = true;
            }
        }

        private int CheckedStationIndex { get { return CheckedRadioButtonIndex / 2; } }

        private bool IsCurrentRadioButtonChecked 
        { 
            get
            {
                return CurrentRadioButton1.Checked || CurrentRadioButton2.Checked;
            }
        }

        public FormSP()
        {
            InitializeComponent();


            //0206*
            //RasterMapControl1 = new RasterMapControl();



            dchislo = 0;
            ichislo = 0;
            //LAMBDA = 300000;

            // .....................................................................
            // Координаты центра ЗПВ

            // Координаты центра ЗПВ на местности в м
            //XSP_comm = 0;
            //YSP_comm = 0;

            // DATUM
            // ГОСТ 51794_2008
            dXdat_comm = 25;
            dYdat_comm = -141;
            dZdat_comm = -80;

            dLat_comm = 0;
            dLong_comm = 0;

            // Эллипсоид Красовского, град
            LatKrG_comm = 0;
            LongKrG_comm = 0;
            // Эллипсоид Красовского, rad
            LatKrR_comm = 0;
            LongKrR_comm = 0;
            // Эллипсоид Красовского, град,мин,сек
            Lat_Grad_comm = 0;
            Lat_Min_comm = 0;
            Lat_Sec_comm = 0;
            Long_Grad_comm = 0;
            Long_Min_comm = 0;
            Long_Sec_comm = 0;
            // Гаусс-крюгер(СК42) м
            XSP42_comm = 0;
            YSP42_comm = 0;

            radioButtons = new[] { CurrentRadioButton1, PlannedRadioButton1, CurrentRadioButton2, PlannedRadioButton2 };
            spViews = new[] {spView1, spView2};

            // sob
            //GlobalVarLn.clientPCG.OnConfirmCoord +=clientPCG_OnConfirmCoord;



        } // Конструктор
        // ***********************************************************  Конструктор

// sob
/*
private void clientPCG_OnConfirmCoord(object sender, byte bAddress, byte bCodeError, double dLatitudeOwn, double dLongitudeOwn, double dLatitudeLinked, double dLongitudeLinked)
{
 	throw new NotImplementedException();
} 
*/

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// ACCEPT

        private void bGetFreq_Click(object sender, EventArgs e)
        {
            if (GlobalVarLn.flllsp == 0)
            {
                MessageBox.Show("No connection");
                return;
            }

            GlobalVarLn.clientPCG.SendCoord((byte)GlobalVarLn.AdressOwn);
        }

        public void AcceptGPS()
        {
            GetSPCoordinates(GlobalVarLn.lt1sp, GlobalVarLn.ln1sp);

            var index = 0;
            var position = new KoordThree(GlobalVarLn.XCenter_SP, GlobalVarLn.YCenter_SP, GlobalVarLn.hhsp);

            // Функция, определяет нажатие CurrentRadioButton1||CurrentRadioButton2
            GlobalVarLn.listJS[0].CurrentPosition = position;

            // GlobalVarLn.listJS[index].indzn = GlobalVarLn.iZSP;

            spViews[index].UpdateView(GlobalVarLn.listJS[index]);

            //0206
            //GlobalVarLn.axMapScreenGlobal.Repaint();
            //MapForm.REDRAW_MAP();
            // SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1SP1

            // SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP

            GetSPCoordinates(GlobalVarLn.lt2sp, GlobalVarLn.ln2sp);

            var index1 = 1;
            var position1 = new KoordThree(GlobalVarLn.XCenter_SP, GlobalVarLn.YCenter_SP, GlobalVarLn.hhsp);

            // Функция, определяет нажатие CurrentRadioButton1||CurrentRadioButton2
            GlobalVarLn.listJS[1].CurrentPosition = position1;


            spViews[index1].UpdateView(GlobalVarLn.listJS[index1]);

            //0206
            //GlobalVarLn.axMapScreenGlobal.Repaint();
            MapForm.REDRAW_MAP();

            // SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP2SP

            GlobalVarLn.flagGPS_SP = 0;
        }
        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        // ************************************************************************
        // Загрузка формы
        // ************************************************************************
        private void FormSP_Load(object sender, EventArgs e)
        {

            if (GlobalVarLn.flEndTRO_stat != 1)
            {

                // ----------------------------------------------------------------------
                //gbRect.Visible = true;
                //gbRect.Location = new Point(7, 30);

                gbRect42.Visible = false;
                gbRad.Visible = false;
                gbDegMin.Visible = false;
                gbDegMinSec.Visible = false;

                cbChooseSC.SelectedIndex = 0;
                // ----------------------------------------------------------------------
                GlobalVarLn.objFormSPG.chbXY.Checked = false;
                // ----------------------------------------------------------------------
                // Очистка dataGridView
                // .......................................................................
                GlobalVarLn.blSP_stat = true;
                GlobalVarLn.flEndSP_stat = 1;

                GlobalVarLn.XCenter_SP = 0;
                GlobalVarLn.YCenter_SP = 0;
                GlobalVarLn.HCenter_SP = 0;
                GlobalVarLn.flCoord_SP2 = 0;

                GlobalVarLn.iZSP = 0;
                //pbSP.Image = imageList1.Images[0];
                pbSP.BackgroundImage = imageList1.Images[0];


            }

            spView1.CurrentLabelClickEvent += spView1_CurrentLabelClickEvent;
            spView1.PlannedlabelClickEvent += spView1_PlannedlabelClickEvent;
            spView1.UpdateView(GlobalVarLn.listJS[0]);

            spView2.CurrentLabelClickEvent += spView2_CurrentLabelClickEvent;
            spView2.PlannedlabelClickEvent += spView2_PlannedlabelClickEvent;
            spView2.UpdateView(GlobalVarLn.listJS[1]);

            foreach (var spView in spViews)
            {
                spView.NameChangedEvent += SpViewNameChangedEvent;
                spView.TypeChangedEvent += SpViewTypeChangedEvent;
                spView.IpChangedEvent += SpViewIpChangedEvent;   
            }

            UpdateSPViews();

            //0206
            //axaxcMapScreen.Repaint();

        } // LOAD
        // ************************************************************************

        void SpViewNameChangedEvent(object sender, string name)
        {
            var index = sender == spView1 ? 0 : 1;
            GlobalVarLn.listJS[index].Name = name;

            //0206
            //axaxcMapScreen.Repaint();
        }

        void SpViewTypeChangedEvent(object sender, string type)
        {
            var index = sender == spView1 ? 0 : 1;
            GlobalVarLn.listJS[index].Type = type;
        }

        void SpViewIpChangedEvent(object sender, int ip)
        {
            var index = sender == spView1 ? 0 : 1;
            GlobalVarLn.listJS[index].IP = ip;
        }

        void spView1_CurrentLabelClickEvent(object sender, EventArgs e)
        {
            CurrentRadioButton1.Checked = true;
        } 

        void spView1_PlannedlabelClickEvent(object sender, EventArgs e)
        {
            PlannedRadioButton1.Checked = true;
        }

        void spView2_CurrentLabelClickEvent(object sender, EventArgs e)
        {
            CurrentRadioButton2.Checked = true;
        }

        void spView2_PlannedlabelClickEvent(object sender, EventArgs e)
        {
            PlannedRadioButton2.Checked = true;
        }

        // Загрузка формы
        // ************************************************************************

        // ************************************************************************
        // Очистка
        // ************************************************************************
        private void bClear_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------
            tbXRect.Text = "";
            tbYRect.Text = "";
            tbXRect42.Text = "";
            tbYRect42.Text = "";
            tbBRad.Text = "";
            tbLRad.Text = "";
            tbBMin1.Text = "";
            tbLMin1.Text = "";
            tbBDeg2.Text = "";
            tbBMin2.Text = "";
            tbBSec.Text = "";
            tbLDeg2.Text = "";
            tbLMin2.Text = "";
            tbLSec.Text = "";

            tbOwnHeight.Text = "";
            tbNumSP.Text = "";
            // -------------------------------------------------------------------
            GlobalVarLn.objFormSPG.chbXY.Checked = false;
            // ----------------------------------------------------------------------
            // переменные

            GlobalVarLn.blSP_stat = true;
            GlobalVarLn.flEndSP_stat = 1;
            GlobalVarLn.XCenter_SP = 0;
            GlobalVarLn.YCenter_SP = 0;
            GlobalVarLn.HCenter_SP = 0;
            GlobalVarLn.flCoord_SP2 = 0;
            // seg2

            GlobalVarLn.ClearListJS();
            UpdateSPViews();
            // -------------------------------------------------------------------
            // Убрать с карты

            //0206
            //GlobalVarLn.axMapScreenGlobal.Repaint();
            MapForm.REDRAW_MAP();

        } // Clear
        // ************************************************************************

        // ************************************************************************
        // Обработчик ComboBox "cbChooseSC": Выбор СК
        // ************************************************************************
        //0206  NO USE


        private void cbChooseSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            //0206
            //ChooseSystemCoord_Comm(cbChooseSC.SelectedIndex);
            ;

        } // Выбор СК

        //0206  NO USE 
 /* 
        private void ChooseSystemCoord_Comm(int iSystemCoord)
        {
            gbRect.Visible = false;
            gbRect42.Visible = false;
            gbRad.Visible = false;
            gbDegMin.Visible = false;
            gbDegMinSec.Visible = false;

            switch (iSystemCoord)
            {
                case 0: // Метры на местности

                    gbRect.Visible = true;
                    gbRect.Location = new Point(7, 30);

                    if (GlobalVarLn.flCoord_SP2 == 1)
                    {

                        ichislo = (long)(GlobalVarLn.XCenter_SP);
                        tbXRect.Text = Convert.ToString(ichislo);

                        ichislo = (long)(GlobalVarLn.YCenter_SP);
                        tbYRect.Text = Convert.ToString(ichislo);

                    } // IF

                    break;
                case 1: // Радианы (Красовский)

                    gbRad.Visible = true;
                    gbRad.Location = new Point(7, 30);

                    if (GlobalVarLn.flCoord_SP2 == 1)
                    {

                        ichislo = (long)(LatKrR_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbBRad.Text = Convert.ToString(dchislo);

                        ichislo = (long)(LongKrR_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbLRad.Text = Convert.ToString(dchislo);

                    } // IF

                    break;

                // CDel
                // !!! Здесь это WGS84
                case 2: // Градусы (Красовский)

                    gbDegMin.Visible = true;
                    gbDegMin.Location = new Point(7, 30);

                    if (GlobalVarLn.flCoord_SP2 == 1)
                    {

                        ichislo = (long)(LatKrG_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbBMin1.Text = Convert.ToString(dchislo);

                        ichislo = (long)(LongKrG_comm * 1000000);
                        dchislo = ((double)ichislo) / 1000000;
                        tbLMin1.Text = Convert.ToString(dchislo);

                    } // IF

                    break;

                // CDel
                // !!! Здесь это WGS84
                case 3: // Градусы,мин,сек (Красовский)

                    gbDegMinSec.Visible = true;
                    gbDegMinSec.Location = new Point(7, 30);

                    if (GlobalVarLn.flCoord_SP2 == 1)
                    {
                        tbBDeg2.Text = Convert.ToString(Lat_Grad_comm);
                        tbBMin2.Text = Convert.ToString(Lat_Min_comm);
                        ichislo = (long)(Lat_Sec_comm);
                        tbBSec.Text = Convert.ToString(ichislo);

                        tbLDeg2.Text = Convert.ToString(Long_Grad_comm);
                        tbLMin2.Text = Convert.ToString(Long_Min_comm);
                        ichislo = (long)(Long_Sec_comm);
                        tbLSec.Text = Convert.ToString(ichislo);

                    } // IF
                    break;

            } // SWITCH

        } 
*/
        // ************************************************************************

        // ************************************************************************
        private void OtobrSP_Comm()
        {

            //0206 NO USE
/*
            // -----------------------------------------------------------------------
            // Метры на местности

            ichislo = (long)(GlobalVarLn.XCenter_SP);
            tbXRect.Text = Convert.ToString(ichislo);

            ichislo = (long)(GlobalVarLn.YCenter_SP);
            tbYRect.Text = Convert.ToString(ichislo);
            // -----------------------------------------------------------------------
            // CDel
            // Метры 1942 года

            //ichislo = (long)(XSP42_comm);
            //tbXRect42.Text = Convert.ToString(ichislo);
            //ichislo = (long)(YSP42_comm);
            //tbYRect42.Text = Convert.ToString(ichislo);
            // -----------------------------------------------------------------------
            // Радианы (Красовский)
            // CDel
            // !!!Здесь это WGS84

            ichislo = (long)(LatKrR_comm * 1000000);
            dchislo = ((double)ichislo) / 1000000;
            tbBRad.Text = Convert.ToString(dchislo);   // X, карта

            ichislo = (long)(LongKrR_comm * 1000000);
            dchislo = ((double)ichislo) / 1000000;
            tbLRad.Text = Convert.ToString(dchislo);   // X, карта
            // -----------------------------------------------------------------------
            // Градусы (Красовский)
            // CDel
            // !!!Здесь это WGS84

            ichislo = (long)(LatKrG_comm * 1000000);
            dchislo = ((double)ichislo) / 1000000;
            tbBMin1.Text = Convert.ToString(dchislo);

            ichislo = (long)(LongKrG_comm * 1000000);
            dchislo = ((double)ichislo) / 1000000;
            tbLMin1.Text = Convert.ToString(dchislo);
            // -----------------------------------------------------------------------
            // Градусы,мин,сек (Красовский)
            // CDel
            // !!!Здесь это WGS84

            tbBDeg2.Text = Convert.ToString(Lat_Grad_comm);
            tbBMin2.Text = Convert.ToString(Lat_Min_comm);
            ichislo = (long)(Lat_Sec_comm);
            tbBSec.Text = Convert.ToString(ichislo);

            tbLDeg2.Text = Convert.ToString(Long_Grad_comm);
            tbLMin2.Text = Convert.ToString(Long_Min_comm);
            ichislo = (long)(Long_Sec_comm);
            tbLSec.Text = Convert.ToString(ichislo);
            // -----------------------------------------------------------------------
*/
        } // OtobrSP_comm
        // ************************************************************************

        // ************************************************************************

//0206
/*
        private void GetSPCoordinates(double lt,double lng)
        {
            // ......................................................................
            double xtmp_ed, ytmp_ed;
            double xtmp1_ed, ytmp1_ed;
            xtmp_ed = 0;
            ytmp_ed = 0;
            xtmp1_ed = 0;
            ytmp1_ed = 0;

            // ......................................................................
            ClassMap objClassMap3_ed = new ClassMap();
            // ......................................................................
            // !!! реальные координаты на местности карты в м (Plane)

            GlobalVarLn.XCenter_SP = GlobalVarLn.MapX1;
            GlobalVarLn.YCenter_SP = GlobalVarLn.MapY1;
            // ......................................................................
            // Ручной ввод

            if (chbXY.Checked == true)
            {

                if ((tbXRect.Text == "") || (tbYRect.Text == ""))
                {
                    MessageBox.Show("Invalid coordinates of the jammer station");
                    return;
                }

                GlobalVarLn.XCenter_SP = Convert.ToDouble(tbXRect.Text);
                GlobalVarLn.YCenter_SP = Convert.ToDouble(tbYRect.Text);
            }

            GlobalVarLn.blSP_stat = true;
            GlobalVarLn.flEndSP_stat = 1;
            GlobalVarLn.flCoord_SP2 = 1;

            // ......................................................................
            // sob

            if (GlobalVarLn.flagGPS_SP == 1)
            {
                // grad->rad
                lt = (lt * Math.PI) / 180;
                lng = (lng * Math.PI) / 180;

                // Подаем rad, получаем там же расстояние на карте в м
                mapGeoToPlane(GlobalVarLn.hmapl, ref lt, ref lng);

              GlobalVarLn.XCenter_SP = lt;
              GlobalVarLn.YCenter_SP = lng;

            }
            // ......................................................................

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.XCenter_SP, GlobalVarLn.YCenter_SP);
            GlobalVarLn.hhsp = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);



            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            xtmp_ed = GlobalVarLn.XCenter_SP;
            ytmp_ed = GlobalVarLn.YCenter_SP;

            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;
            // .......................................................................
            // CDel

            // WGS84,grad
            LatKrG_comm = xtmp1_ed;
            LongKrG_comm = ytmp1_ed;

            LatKrR_comm = (LatKrG_comm * Math.PI) / 180;
            LongKrR_comm = (LongKrG_comm * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS
            // CDel
            // !!! Здесь это WGS84

            // Широта
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LatKrG_comm,

                // Выходные параметры 
                ref Lat_Grad_comm,
                ref Lat_Min_comm,
                ref Lat_Sec_comm

              );

            // Долгота
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LongKrG_comm,

                // Выходные параметры 
                ref Long_Grad_comm,
                ref Long_Min_comm,
                ref Long_Sec_comm

              );

            OtobrSP_Comm();
            // .......................................................................
            // H

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.XCenter_SP, GlobalVarLn.YCenter_SP);
            GlobalVarLn.HCenter_SP = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
        }

*/

        //0206*
                private void GetSPCoordinates(double lt,double lng)
                {
                    // ......................................................................
                    double xtmp_ed, ytmp_ed;
                    double xtmp1_ed, ytmp1_ed;
                    xtmp_ed = 0;
                    ytmp_ed = 0;
                    xtmp1_ed = 0;
                    ytmp1_ed = 0;

                    // ......................................................................
                    ClassMap objClassMap3_ed = new ClassMap();
                    // ......................................................................
                    // !!! Merkator

                    //0210
                    var p = Mercator.FromLonLat(lng, lt);
                    GlobalVarLn.XCenter_SP = p.X;
                    GlobalVarLn.YCenter_SP = p.Y;

                    double hhh;

                        try
                        {
                            hhh = (double)MapForm.RasterMapControl.Dted.GetElevation(lng, lt);
                        }
                        catch
                        {
                            hhh = 0;
                        }




                    GlobalVarLn.HCenter_SP = hhh;
                    GlobalVarLn.hhsp = hhh;
                    // ......................................................................
                    // Ручной ввод

/*
                    if (chbXY.Checked == true)
                    {

                        if ((tbXRect.Text == "") || (tbYRect.Text == ""))
                        {
                            MessageBox.Show("Invalid coordinates of the jammer station");
                            return;
                        }

                        GlobalVarLn.XCenter_SP = Convert.ToDouble(tbXRect.Text);
                        GlobalVarLn.YCenter_SP = Convert.ToDouble(tbYRect.Text);
                    }
*/
                    GlobalVarLn.blSP_stat = true;
                    GlobalVarLn.flEndSP_stat = 1;
                    GlobalVarLn.flCoord_SP2 = 1;

                    // ......................................................................
                    // sob


/*
                    if (GlobalVarLn.flagGPS_SP == 1)
                    {
                        // grad->rad
                        lt = (lt * Math.PI) / 180;
                        lng = (lng * Math.PI) / 180;

                        // Подаем rad, получаем там же расстояние на карте в м
                        mapGeoToPlane(GlobalVarLn.hmapl, ref lt, ref lng);

                      GlobalVarLn.XCenter_SP = lt;
                      GlobalVarLn.YCenter_SP = lng;

                    }
                    // ......................................................................

                    GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.XCenter_SP, GlobalVarLn.YCenter_SP);
                    GlobalVarLn.hhsp = (int)GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);



                    // ......................................................................
                    // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
                    // !!! Выход функции(rad) идет на место входных переменных

                    xtmp_ed = GlobalVarLn.XCenter_SP;
                    ytmp_ed = GlobalVarLn.YCenter_SP;

                    mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

                    // rad(WGS84)->grad(WGS84)
                    xtmp1_ed = (xtmp_ed * 180) / Math.PI;
                    ytmp1_ed = (ytmp_ed * 180) / Math.PI;
                    // .......................................................................
                    // CDel

                    // WGS84,grad
                    LatKrG_comm = xtmp1_ed;
                    LongKrG_comm = ytmp1_ed;

                    LatKrR_comm = (LatKrG_comm * Math.PI) / 180;
                    LongKrR_comm = (LongKrG_comm * Math.PI) / 180;
                    // .......................................................................
                    // Эллипсоид Красовского, grad,min,sec
                    // dd.ddddd -> DD MM SS
                    // CDel
                    // !!! Здесь это WGS84

                    // Широта
                    objClassMap3_ed.f_Grad_GMS
                      (
                        // Входные параметры (grad)
                        LatKrG_comm,

                        // Выходные параметры 
                        ref Lat_Grad_comm,
                        ref Lat_Min_comm,
                        ref Lat_Sec_comm

                      );

                    // Долгота
                    objClassMap3_ed.f_Grad_GMS
                      (
                        // Входные параметры (grad)
                        LongKrG_comm,

                        // Выходные параметры 
                        ref Long_Grad_comm,
                        ref Long_Min_comm,
                        ref Long_Sec_comm

                      );

                    OtobrSP_Comm();
                    // .......................................................................
                    // H

                    GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.XCenter_SP, GlobalVarLn.YCenter_SP);
                    GlobalVarLn.HCenter_SP = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
*/

                    OtobrSP_Comm();

                    
                }


        // ************************************************************************

        // *****************************************************************************************
        // Сюда входим по клику мыши
        // *****************************************************************************************

        public void f_SP(double X, double Y)
        {
            //0218 ?????????
            //GetSPCoordinates(0,0);
            //tbOwnHeight.Text = Convert.ToString(GlobalVarLn.HCenter_SP);
            GlobalVarLn.blSP_stat = true;
            GlobalVarLn.flEndSP_stat = 1;
            GlobalVarLn.flCoord_SP2 = 1;

            var index = CheckedStationIndex;

            GlobalVarLn.XCenter_SP = GlobalVarLn.X_Rastr;
            GlobalVarLn.YCenter_SP = GlobalVarLn.Y_Rastr;
            GlobalVarLn.HCenter_SP = GlobalVarLn.H_Rastr;


            var position = new KoordThree(GlobalVarLn.XCenter_SP, GlobalVarLn.YCenter_SP, GlobalVarLn.HCenter_SP);
            if (IsCurrentRadioButtonChecked)
            {
                GlobalVarLn.listJS[index].CurrentPosition = position;
            }
            else
            {
                GlobalVarLn.listJS[index].PlannedPosition = position;                
            }
            GlobalVarLn.listJS[index].indzn = GlobalVarLn.iZSP;

            spViews[index].UpdateView(GlobalVarLn.listJS[index]);

            //0206
            //GlobalVarLn.axMapScreenGlobal.Repaint();

            //0206*
            MapForm.REDRAW_MAP();
            //f_SPReDraw();

        }

        //0218  Ручной ввод
        private void button4_Click(object sender, EventArgs e)
        {



            double h=0;
            double Lt=0;
            double Ln=0;
            String s = "";
            // ----------------------------------------------------------------------------
            GlobalVarLn.blSP_stat = true;
            GlobalVarLn.flEndSP_stat = 1;
            GlobalVarLn.flCoord_SP2 = 1;
            // ----------------------------------------------------------------------------
            //0/1 station1/2

            var index = CheckedStationIndex;
            // ----------------------------------------------------------------------------
            // Lat,Long

            //Current
            if (IsCurrentRadioButtonChecked)
            {
                //Lat
                s = spViews[index].CurrentLatTextBox.Text;
                try
                {
                    //if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                    Lt = Convert.ToDouble(s);
                }
                catch (SystemException)
                {
                    try
                    {
                        if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                        Lt = Convert.ToDouble(s);
                    }
                    catch
                    {
                        MessageBox.Show("Incorrect data");
                        return;
                    }
                }

                //Long
                s = spViews[index].CurrentLongTextBox.Text;
                try
                {
                    //if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                    Ln = Convert.ToDouble(s);
                }
                catch (SystemException)
                {
                    try
                    {
                        if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                        Ln = Convert.ToDouble(s);
                    }
                    catch
                    {
                        MessageBox.Show("Incorrect data");
                        return;
                    }
                }

            }//Current
            // ...........................................................................
            // Planned

            else
            {

                //Lat
                s = spViews[index].PlannedLatTextBox.Text;
                try
                {
                    //if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                    Lt = Convert.ToDouble(s);
                }
                catch (SystemException)
                {
                    try
                    {
                        if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                        Lt = Convert.ToDouble(s);
                    }
                    catch
                    {
                        MessageBox.Show("Incorrect data");
                        return;
                    }
                }

                //Long
                s = spViews[index].PlannedLongTextBox.Text;
                try
                {
                    //if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                    Ln = Convert.ToDouble(s);
                }
                catch (SystemException)
                {
                    try
                    {
                        if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                        Ln = Convert.ToDouble(s);
                    }
                    catch
                    {
                        MessageBox.Show("Incorrect data");
                        return;
                    }
                }

            } // Planned
            // ----------------------------------------------------------------------------
            // H

            try
            {
                var x = MapForm.RasterMapControl.Dted.GetElevation(Ln, Lt);
                if (x == null)
                    h = 0;
                else
                    h = (double)x;
            }
            catch
            {
                h = 0;
            }

            if (IsCurrentRadioButtonChecked)
            {
                spViews[index].CurrentHTextBox.Text=Convert.ToString(h);
            }
            else
            {
                spViews[index].PlannedHTextBox.Text = Convert.ToString(h);
            }
            // ----------------------------------------------------------------------------
            // X,Y

            // преобразование В меркатор
            var p = Mercator.FromLonLat(Ln, Lt);
            GlobalVarLn.XCenter_SP = p.X;
            GlobalVarLn.YCenter_SP = p.Y;
            GlobalVarLn.HCenter_SP = h;
            // ----------------------------------------------------------------------------

            var position = new KoordThree(GlobalVarLn.XCenter_SP, GlobalVarLn.YCenter_SP, GlobalVarLn.HCenter_SP);
            if (IsCurrentRadioButtonChecked)
            {
                GlobalVarLn.listJS[index].CurrentPosition = position;
            }
            else
            {
                GlobalVarLn.listJS[index].PlannedPosition = position;
            }
            GlobalVarLn.listJS[index].indzn = GlobalVarLn.iZSP;

            spViews[index].UpdateView(GlobalVarLn.listJS[index]);
            // ----------------------------------------------------------------------------

            MapForm.REDRAW_MAP();
            // ----------------------------------------------------------------------------



        } //Ручной ввод
        // *****************************************************************************************
        // Обработчик кнопки : сохранить
        // *****************************************************************************************
        private void bAccept_Click(object sender, EventArgs e)
        {
            if (GlobalVarLn.listJS.Count == 0)
            {
                return;
            }

            var formatter = new BinaryFormatter();
            try
            {
                //0219
                //using (var fs = new FileStream("SP.bin", FileMode.OpenOrCreate))
                using (var fs = new FileStream(Application.StartupPath + "\\SaveInFiles\\JS.bin", FileMode.OpenOrCreate))

                {
                    formatter.Serialize(fs, GlobalVarLn.listJS);
                }

            }
            catch (Exception exception)
            {
                MessageBox.Show("Can’t save file");
            }
        } // Save in file
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки : read from file
        // *****************************************************************************************
        private void button1_Click(object sender, EventArgs e)
        {
            GlobalVarLn.LoadListJS();
            UpdateSPViews();

            //0206
            //axaxcMapScreen.Repaint();


            // otl33
            GlobalVarLn.flEndSP_stat = 1;
            //GlobalVarLn.objFormSPG.f_SPReDraw();
            //0206*
            MapForm.REDRAW_MAP();
            //f_SPReDraw();

            GlobalVarLn.fclSP = 0;


        } // read from file
        // *****************************************************************************************

        public void UpdateSPViews()
        {
            spView1.UpdateView(GlobalVarLn.listJS[0]);
            spView2.UpdateView(GlobalVarLn.listJS[1]);
        }

        // *****************************************************************************************
        // Удалить СП
        // *****************************************************************************************
        private void button3_Click(object sender, EventArgs e)
        {
            var station = GlobalVarLn.listJS[CheckedStationIndex];

            station.HasCurrentPosition = false;
            station.HasPlannedPosition = false;
            
            UpdateSPViews();

            //0206
            //GlobalVarLn.axMapScreenGlobal.Repaint();

            //0206*
            MapForm.REDRAW_MAP();
            //f_SPReDraw();


        } 

        // *************************************************************************************
        // Перерисовка SP
        // *************************************************************************************

        public void f_SPReDraw()
        {

            //0206
/*
            foreach (var station in GlobalVarLn.listJS)
            {
                if (station.HasCurrentPosition)
                {
                    ClassMap.f_DrawSPXY1(
                        station.CurrentPosition.x,
                        station.CurrentPosition.y,
                        station.Name,
                        (Bitmap) imageList1.Images[station.indzn]);


                }
                if (station.HasPlannedPosition)
                {
                    ClassMap.f_DrawSPXYV1(
                        station.PlannedPosition.x,
                        station.PlannedPosition.y,
                        station.Name,
                        (Bitmap) imageList1V.Images[station.indzn]);

                }
            }
*/

        //0206*

       foreach (var station in GlobalVarLn.listJS)
       {

        IMapObject objectGrozaS1;
        MapObjectStyle _placeObjectStyleOwn;
        MapObjectStyle _placeObjectStyleLinked;
        //string strExePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        Mapsui.Geometries.Point pointOwn = new Mapsui.Geometries.Point();
        //pointOwn.X = GlobalVarLn.LONG_Rastr;
        //pointOwn.Y = GlobalVarLn.LAT_Rastr;
       // --------------------------------------------------------------------------------------
       // SP_main

       if (station.HasCurrentPosition)
       {

                    try
                    {

                        var p3 = Mercator.ToLonLat(station.CurrentPosition.x, station.CurrentPosition.y);
                        pointOwn.X = p3.X;
                        pointOwn.Y = p3.Y;

                        _placeObjectStyleOwn = MapForm.RasterMapControl.LoadObjectStyle(
                            //     strExePath + "\\Images\\Jammer\\1.png",
                            (Bitmap) imageList1.Images[station.indzn],
                            //scale: 0.8,
                            scale: 0.6,
                            objectOffset: new Offset(0, 0),
                            textOffset: new Offset(0, 15)
                            );
                        objectGrozaS1 = MapForm.RasterMapControl.AddMapObject(_placeObjectStyleOwn,station.Name, pointOwn);

                        //MapForm.RasterMapControl.AddPolyline();
                        //RasterMapControl.MaxRes = 234234;


                    }
                    catch (Exception ex)
                    {
                        // ignored
                    }


        } // SP_main
       // --------------------------------------------------------------------------------------
       // SP_sopr

       if (station.HasPlannedPosition)
       {
           try
           {
               var p5 = Mercator.ToLonLat(station.PlannedPosition.x, station.PlannedPosition.y);
               pointOwn.X = p5.X;
               pointOwn.Y = p5.Y;

               _placeObjectStyleLinked = MapForm.RasterMapControl.LoadObjectStyle(
                   //strExePath + "\\Images\\JammerPlanned\\1.bmp",
                   (Bitmap) imageList1V.Images[station.indzn],
                   //scale: 0.8,
                   scale: 0.6,
                   objectOffset: new Offset(0, 0),
                   textOffset: new Offset(0, 15)

                   );
               objectGrozaS1 = MapForm.RasterMapControl.AddMapObject(_placeObjectStyleLinked, station.Name, pointOwn);

           }
           catch (Exception ex)
           {
               // ignored
           }

       }
       // --------------------------------------------------------------------------------------
      } //Foreach


      } // P/P

        // *************************************************************************************

        // ****************************************************************************************
        // Закрыть форму
        // ****************************************************************************************
        private void FormSP_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

            // приостановить обработку, если идет
            GlobalVarLn.blSP_stat = false;

            GlobalVarLn.fl_Open_objFormSP = 0;


        } // Закрыть форму
        // ****************************************************************************************
        // Активизировать форму
        // *************************************************************************************
        private void FormSP_Activated(object sender, EventArgs e)
        {
            //GlobalVarLn.objFormSPG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFSP = 1;

            GlobalVarLn.blSP_stat = true;
            GlobalVarLn.flEndSP_stat = 1;

           // 0809_3
           //ClassMap.f_RemoveFrm(1);

            GlobalVarLn.fl_Open_objFormSP = 1;

        } // Активизировать форму
        // ****************************************************************************************

        private void lMiddleHeight_Click(object sender, EventArgs e)
        {

        }

        // ****************************************************************************************
        // Значок (Forward)
        // ****************************************************************************************
        private void buttonZSP_Click(object sender, EventArgs e)
        {
            ///GlobalVarLn.iZSP = (GlobalVarLn.iZSP + 1) % GlobalVarLn.NumbZSP;
            GlobalVarLn.iZSP = (GlobalVarLn.iZSP + 1) % imageList1.Images.Count;

            //pbSP.Image = imageList1.Images[GlobalVarLn.iZSP];
            pbSP.BackgroundImage = imageList1.Images[GlobalVarLn.iZSP];

            GlobalVarLn.listJS[CheckedStationIndex].indzn = GlobalVarLn.iZSP;
        }

        // ****************************************************************************************
        // Значок (Backward)
        // ****************************************************************************************

        private void button2_Click(object sender, EventArgs e)
        {
            //GlobalVarLn.iZSP = (GlobalVarLn.iZSP + 1) % imageList1.Images.Count;

            GlobalVarLn.iZSP -= 1;
            if (GlobalVarLn.iZSP < 0)
                GlobalVarLn.iZSP = imageList1.Images.Count-1;
            pbSP.BackgroundImage = imageList1.Images[GlobalVarLn.iZSP];
            GlobalVarLn.listJS[CheckedStationIndex].indzn = GlobalVarLn.iZSP;

        }
        // ****************************************************************************************





        private void label15_Click(object sender, EventArgs e)
        {
        }

        private void CurrentRadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            var radioButton = sender as RadioButton;
            if (radioButton.Checked == false)
            {
                return;
            }

            for (var i = 0; i < radioButtons.Length; ++i)
            {
                if (radioButtons[i] == radioButton)
                {
                    CheckedRadioButtonIndex = i;
                    return;
                }
            }
        }

        // 1509Otl
        private void FormSP_FormClosed(object sender, FormClosedEventArgs e)
        {
            //e.Cancel = true;
            //Hide();

        }



    }
}
