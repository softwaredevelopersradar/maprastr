﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;

//0206*
using System.Windows.Input;
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;
using Bitmap = System.Drawing.Bitmap;

namespace GrozaMap
{
    public partial class FormSPFB : Form
    {
        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        private double dchislo;
        private long ichislo;
        //private double LAMBDA;

        // .....................................................................

        // DATUM
        private double dXdat_comm;
        private double dYdat_comm;
        private double dZdat_comm;

        private double dLat_comm;
        private double dLong_comm;

        // Эллипсоид Красовского, град
        private double LatKrG_comm;
        private double LongKrG_comm;
        // Эллипсоид Красовского, rad
        private double LatKrR_comm;
        private double LongKrR_comm;
        // Эллипсоид Красовского, град,мин,сек
        private int Lat_Grad_comm;
        private int Lat_Min_comm;
        private double Lat_Sec_comm;
        private int Long_Grad_comm;
        private int Long_Min_comm;
        private double Long_Sec_comm;
        // Гаусс-крюгер(СК42) м
        private double XSP42_comm;
        private double YSP42_comm;
        // Конструктор *********************************************************** 

        public FormSPFB()
        {
            InitializeComponent();


            dchislo = 0;
            ichislo = 0;

            // .....................................................................
            // Координаты центра ЗПВ

            // DATUM
            // ГОСТ 51794_2008
            dXdat_comm = 25;
            dYdat_comm = -141;
            dZdat_comm = -80;

            dLat_comm = 0;
            dLong_comm = 0;

            // Эллипсоид Красовского, град
            LatKrG_comm = 0;
            LongKrG_comm = 0;
            // Эллипсоид Красовского, rad
            LatKrR_comm = 0;
            LongKrR_comm = 0;
            // Эллипсоид Красовского, град,мин,сек
            Lat_Grad_comm = 0;
            Lat_Min_comm = 0;
            Lat_Sec_comm = 0;
            Long_Grad_comm = 0;
            Long_Min_comm = 0;
            Long_Sec_comm = 0;
            // Гаусс-крюгер(СК42) м
            XSP42_comm = 0;
            YSP42_comm = 0;
        } // Конструктор
        // ***********************************************************  Конструктор

        // ************************************************************************
        // Загрузка формы
        // ************************************************************************
        private void FormSPFB_Load(object sender, EventArgs e)
        {

            GlobalVarLn.iSP_f1 = 0;
            // ----------------------------------------------------------------------
            cbChooseSC.SelectedIndex = 0;
            GlobalVarLn.NumbSP_f1 = Convert.ToString(cbChooseSC.Items[cbChooseSC.SelectedIndex]);
            // ----------------------------------------------------------------------
            // Очистка dataGridView

            dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDat_f1; i++)
            {
                dataGridView1.Rows.Add("", "", "","");
            }
            // .......................................................................
            GlobalVarLn.flF_f1 = 1;
            GlobalVarLn.Az1_f1 = 0;
            GlobalVarLn.Az2_f1 = 0;
            GlobalVarLn.Az_f1 = 0;
            GlobalVarLn.flAz1_f1=0;
            GlobalVarLn.flAz2_f1 = 0;
            // ......................................................................
            GlobalVarLn.list1_f1.Clear();

            GlobalVarLn.ListJSChangedEvent += OnListJSChanged;
            MapForm.UpdateDropDownList1(cbChooseSC);
            // ----------------------------------------------------------------------


        } // Load_form
          // ************************************************************************

        private void OnListJSChanged(object sender, EventArgs e)
        {
            MapForm.UpdateDropDownList1(cbChooseSC);
        }

        // ************************************************************************
        // Очистка
        // ************************************************************************
        private void bClear_Click(object sender, EventArgs e)
        {
            cbChooseSC.SelectedIndex = 0;
            GlobalVarLn.NumbSP_f1 = Convert.ToString(cbChooseSC.Items[cbChooseSC.SelectedIndex]);
            // ----------------------------------------------------------------------
            // Очистка dataGridView

            while (dataGridView1.Rows.Count != 0)
                dataGridView1.Rows.Remove(dataGridView1.Rows[dataGridView1.Rows.Count - 1]);

            dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDat_f1; i++)
            {
                dataGridView1.Rows.Add("", "", "", "");
            }
            // .......................................................................
            GlobalVarLn.flF_f1 = 1;
            GlobalVarLn.Az1_f1 = 0;
            GlobalVarLn.Az2_f1 = 0;
            GlobalVarLn.Az_f1 = 0;
            GlobalVarLn.flAz1_f1 = 0;
            GlobalVarLn.flAz2_f1 = 0;
            // ......................................................................
            GlobalVarLn.list1_f1.Clear();
            // ----------------------------------------------------------------------

        } // Clear
        // ************************************************************************

        // ************************************************************************
        // Обработчик ComboBox "cbChooseSC": Выбор SP
        // ************************************************************************
        private void cbChooseSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            GlobalVarLn.NumbSP_f1 =Convert.ToString(cbChooseSC.Items[cbChooseSC.SelectedIndex]);

        } // SelectIndex
        // ************************************************************************

        // ************************************************************************
        // Save in file
        // ************************************************************************
        private void bAccept_Click(object sender, EventArgs e)
        {
            int i_tmp = 0;
            int i_tmp1 = 0;
            String s1 = "";
            // -----------------------------------------------------------------------------------------
            String strFileName;

            //strFileName = "FBSPRR.txt";
            //0219
            strFileName = Application.StartupPath + "\\SaveInFiles\\BandsSectReconn.txt";

            StreamReader srFile1;
            GlobalVarLn.NumbSP_f1 = cbChooseSC.Text;

            FBSP objFBSP = new FBSP();
            objFBSP.mfb = new FB[GlobalVarLn.sizeDiap_f1 + 10];

            GlobalVarLn.list1_f1.Clear();
            GlobalVarLn.iSP_f1 = 0;
            // ***********************************************************************************************
            // файл был

            try
            {
                // ----------------------------------------------------------------------------------------
                srFile1 = new StreamReader(strFileName);
                srFile1.Close();
                ReadFileFBSP_f1(0); // Читаем старый файл
                GlobalVarLn.iSP_f1 = (uint)GlobalVarLn.list1_f1.Count;

                for (i_tmp = 0; i_tmp < GlobalVarLn.list1_f1.Count; i_tmp++)
                {
                    // Убираем СП с таким же номером в старом файле
                    if (String.Compare(GlobalVarLn.list1_f1[i_tmp].NumbSP, GlobalVarLn.NumbSP_f1) == 0)
                    {
                        GlobalVarLn.list1_f1.RemoveAt(i_tmp);
                        GlobalVarLn.iSP_f1 -= 1;
                    }
                }
               // ----------------------------------------------------------------------------------------
                objFBSP.NumbSP = GlobalVarLn.NumbSP_f1;
                i_tmp = 0;

                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILE
                while (
                        (dataGridView1.Rows[(int)(i_tmp)].Cells[0].Value != "")&&
                        (dataGridView1.Rows[(int)(i_tmp)].Cells[0].Value != null)
                      )
                {
                    // ...........................................................................................
                    // Fmin

                    s1 = Convert.ToString(dataGridView1.Rows[(int)(i_tmp)].Cells[0].Value);
                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        objFBSP.mfb[i_tmp].Fmin = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            objFBSP.mfb[i_tmp].Fmin = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            MessageBox.Show("Incorrect data");
                            return;
                        }

                    }

                    // seg
                    ichislo = (long)(objFBSP.mfb[i_tmp].Fmin * 10);
                    dchislo = ((double)ichislo) / 10;
                    objFBSP.mfb[i_tmp].Fmin = dchislo;

                    // ...........................................................................................
                    // Fmax

                    //objFBSP.mfb[i_tmp].Fmax = Convert.ToDouble(dataGridView1.Rows[(int)(i_tmp)].Cells[1].Value);

                    s1 = Convert.ToString(dataGridView1.Rows[(int)(i_tmp)].Cells[1].Value);

                    if ((s1 == "") || (s1 == null))
                    {
                        MessageBox.Show("Incorrect data");
                        srFile1.Close();
                        return;
                    }

                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        objFBSP.mfb[i_tmp].Fmax = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            objFBSP.mfb[i_tmp].Fmax = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            MessageBox.Show("Incorrect data");
                            return;
                        }

                    }

                    // seg
                    ichislo = (long)(objFBSP.mfb[i_tmp].Fmax * 10);
                    dchislo = ((double)ichislo) / 10;
                    objFBSP.mfb[i_tmp].Fmax = dchislo;

                    // ...........................................................................................
                    // Bmin

                    s1 = Convert.ToString(dataGridView1.Rows[(int)(i_tmp)].Cells[2].Value);

                    if ((s1 == "") || (s1 == null))
                    {
                        MessageBox.Show("Incorrect data");
                        srFile1.Close();
                        return;
                    }

                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        objFBSP.mfb[i_tmp].Bmin = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            objFBSP.mfb[i_tmp].Bmin = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            MessageBox.Show("Incorrect data");
                            return;
                        }

                    }

                    // seg
                    ichislo = (long)(objFBSP.mfb[i_tmp].Bmin * 10);
                    dchislo = ((double)ichislo) / 10;
                    objFBSP.mfb[i_tmp].Bmin = dchislo;

                    // ...........................................................................................
                    // Bmax

                    s1 = Convert.ToString(dataGridView1.Rows[(int)(i_tmp)].Cells[3].Value);

                    if ((s1 == "") || (s1 == null))
                    {
                        MessageBox.Show("Incorrect data");
                        srFile1.Close();
                        return;
                    }

                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        objFBSP.mfb[i_tmp].Bmax = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            objFBSP.mfb[i_tmp].Bmax = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            MessageBox.Show("Incorrect data");
                            return;
                        }

                    }

                    // seg
                    ichislo = (long)(objFBSP.mfb[i_tmp].Bmax * 10);
                    dchislo = ((double)ichislo) / 10;
                    objFBSP.mfb[i_tmp].Bmax = dchislo;

                    // ...........................................................................................
                    // seg

                    if (
                       (objFBSP.mfb[i_tmp].Fmin > objFBSP.mfb[i_tmp].Fmax) 
                       )
                    {
                        MessageBox.Show("Incorrect data");
                        srFile1.Close();
                        return;
                    }

                    if (
                       (objFBSP.mfb[i_tmp].Fmin < 100) ||
                        (objFBSP.mfb[i_tmp].Fmin > 6000) ||
                        (objFBSP.mfb[i_tmp].Fmax < 100) ||
                        (objFBSP.mfb[i_tmp].Fmax > 6000)
                       )
                    {
                        MessageBox.Show("Parameter 'frequency' out of range 100MHz-6000 MHz");
                        srFile1.Close();
                        return;
                    }

                    if (
                       (objFBSP.mfb[i_tmp].Bmin < 0) ||
                        (objFBSP.mfb[i_tmp].Bmin > 360) ||
                        (objFBSP.mfb[i_tmp].Bmax < 0) ||
                        (objFBSP.mfb[i_tmp].Bmax > 360)
                       )
                    {
                        MessageBox.Show("Parameter 'azimuth' out of range 0deg-360deg");
                        srFile1.Close();
                        return;
                    }
                    // ...........................................................................................

                    i_tmp += 1;
                    if (i_tmp > GlobalVarLn.sizeDiap_f1)
                    {
                        MessageBox.Show("Invalid number of frequency bands");
                        break;
                    }
                } // WHILE
                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILE

                objFBSP.NumbDiap = i_tmp;
                // ----------------------------------------------------------------------------------------
                GlobalVarLn.list1_f1.Add(objFBSP);
                GlobalVarLn.iSP_f1 += 1;
                // ----------------------------------------------------------------------------------------
                StreamWriter srFile = new StreamWriter(strFileName);
                // ----------------------------------------------------------------------------------------
                // Запись в файл

                for (i_tmp = 0; i_tmp < GlobalVarLn.list1_f1.Count; i_tmp++)
                {
                    // SPi
                    srFile.WriteLine("Numb =" + Convert.ToString(GlobalVarLn.list1_f1[i_tmp].NumbSP));

                    for (i_tmp1 = 0; i_tmp1 < GlobalVarLn.list1_f1[i_tmp].NumbDiap; i_tmp1++)
                    {
                        srFile.WriteLine("Fmin ="+Convert.ToString(GlobalVarLn.list1_f1[i_tmp].mfb[i_tmp1].Fmin));
                        srFile.WriteLine("Fmax =" + Convert.ToString(GlobalVarLn.list1_f1[i_tmp].mfb[i_tmp1].Fmax));
                        srFile.WriteLine("Bmin =" + Convert.ToString(GlobalVarLn.list1_f1[i_tmp].mfb[i_tmp1].Bmin));
                        srFile.WriteLine("Bmax =" + Convert.ToString(GlobalVarLn.list1_f1[i_tmp].mfb[i_tmp1].Bmax));
                    }

                }

                // ----------------------------------------------------------------------------------------
                srFile.Close();
            }
            // ***********************************************************************************************
            // файла не было

            catch
            {
                // ...........................................................................................
                StreamWriter srFile;
                try
                {
                    srFile = new StreamWriter(strFileName);
                }
                catch
                {
                    MessageBox.Show("Can’t save file");
                    return;
                }

                objFBSP.NumbSP = GlobalVarLn.NumbSP_f1;
                i_tmp = 0;
                // ...........................................................................................

                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILE
                while (
                       (dataGridView1.Rows[(int)(i_tmp)].Cells[0].Value != "") &&
                       (dataGridView1.Rows[(int)(i_tmp)].Cells[0].Value != null)
                      )

                {
                    // ...........................................................................................
                    // Fmin

                    s1 = Convert.ToString(dataGridView1.Rows[(int)(i_tmp)].Cells[0].Value);

                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        objFBSP.mfb[i_tmp].Fmin = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            objFBSP.mfb[i_tmp].Fmin = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            MessageBox.Show("Incorrect data");
                            return;
                        }

                    }

                    // seg
                    ichislo = (long)(objFBSP.mfb[i_tmp].Fmin * 10);
                    dchislo = ((double)ichislo) / 10;
                    objFBSP.mfb[i_tmp].Fmin = dchislo;

                    // ...........................................................................................
                    // Fmax

                    s1 = Convert.ToString(dataGridView1.Rows[(int)(i_tmp)].Cells[1].Value);

                    if((s1=="")||(s1==null))
                    {
                        MessageBox.Show("Incorrect data");
                    srFile.Close();
                    return;
                    }

                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        objFBSP.mfb[i_tmp].Fmax = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            objFBSP.mfb[i_tmp].Fmax = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            MessageBox.Show("Incorrect data");
                            return;
                        }

                    }

                    // seg
                    ichislo = (long)(objFBSP.mfb[i_tmp].Fmax * 10);
                    dchislo = ((double)ichislo) / 10;
                    objFBSP.mfb[i_tmp].Fmax = dchislo;

                    // ...........................................................................................
                    // Bmin

                    s1 = Convert.ToString(dataGridView1.Rows[(int)(i_tmp)].Cells[2].Value);

                    if ((s1 == "") || (s1 == null))
                    {
                        MessageBox.Show("Incorrect data");
                        srFile.Close();
                        return;
                    }

                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        objFBSP.mfb[i_tmp].Bmin = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            objFBSP.mfb[i_tmp].Bmin = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            MessageBox.Show("Incorrect data");
                            return;
                        }

                    }

                    // seg
                    ichislo = (long)(objFBSP.mfb[i_tmp].Bmin * 10);
                    dchislo = ((double)ichislo) / 10;
                    objFBSP.mfb[i_tmp].Bmin = dchislo;

                    // ...........................................................................................
                    // Bmax

                    s1 = Convert.ToString(dataGridView1.Rows[(int)(i_tmp)].Cells[3].Value);

                    if ((s1 == "") || (s1 == null))
                    {
                        MessageBox.Show("Incorrect data");
                        srFile.Close();
                        return;
                    }

                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        objFBSP.mfb[i_tmp].Bmax = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            objFBSP.mfb[i_tmp].Bmax = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            MessageBox.Show("Incorrect data");
                            return;
                        }

                    }

                    // seg
                    ichislo = (long)(objFBSP.mfb[i_tmp].Bmax * 10);
                    dchislo = ((double)ichislo) / 10;
                    objFBSP.mfb[i_tmp].Bmax = dchislo;

                    // ...........................................................................................
                    // seg

                    if (
                       (objFBSP.mfb[i_tmp].Fmin > objFBSP.mfb[i_tmp].Fmax) 
                       )
                    {
                        MessageBox.Show("Incorrect data");
                        srFile.Close();
                        return;
                    }

                    if (
                       (objFBSP.mfb[i_tmp].Fmin < 100) ||
                        (objFBSP.mfb[i_tmp].Fmin > 6000) ||
                        (objFBSP.mfb[i_tmp].Fmax < 100) ||
                        (objFBSP.mfb[i_tmp].Fmax > 6000)
                       )
                    {
                        MessageBox.Show("Parameter 'frequency' out of range 100MHz-6000 MHz");
                        srFile.Close();
                        return;
                    }

                    if (
                       (objFBSP.mfb[i_tmp].Bmin < 0) ||
                        (objFBSP.mfb[i_tmp].Bmin > 360) ||
                        (objFBSP.mfb[i_tmp].Bmax < 0) ||
                        (objFBSP.mfb[i_tmp].Bmax > 360)
                       )
                    {
                        MessageBox.Show("Parameter 'azimuth' out of range 0deg-360deg");
                        srFile.Close();
                        return;
                    }
                    // ...........................................................................................

                    i_tmp += 1;
                    if (i_tmp > GlobalVarLn.sizeDiap_f1)
                    {
                        MessageBox.Show("Invalid number of frequency bands");
                        break;
                    }
                } // WHILE
                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILE

                objFBSP.NumbDiap = i_tmp;

                // SPi
                srFile.WriteLine("Numb =" + Convert.ToString(objFBSP.NumbSP));

                for (i_tmp = 0; i_tmp < objFBSP.NumbDiap; i_tmp++)
                {

                    srFile.WriteLine("Fmin =" + Convert.ToString(objFBSP.mfb[i_tmp].Fmin));
                    srFile.WriteLine("Fmax =" + Convert.ToString(objFBSP.mfb[i_tmp].Fmax));
                    srFile.WriteLine("Bmin =" + Convert.ToString(objFBSP.mfb[i_tmp].Bmin));
                    srFile.WriteLine("Bmax =" + Convert.ToString(objFBSP.mfb[i_tmp].Bmax));

                }

             srFile.Close();
            } // catch
            // ***********************************************************************************************

        } // Save in file
        // ************************************************************************

        // ************************************************************************
        // Read All
        // ************************************************************************
        private void button1_Click(object sender, EventArgs e)
        {

        } // ReadAll
        // ************************************************************************

        // ************************************************************************
        // Read Tek
        // ************************************************************************
        private void button2_Click_1(object sender, EventArgs e)
        {
            // ----------------------------------------------------------------------
            // Очистка dataGridView

            while (dataGridView1.Rows.Count != 0)
                dataGridView1.Rows.Remove(dataGridView1.Rows[dataGridView1.Rows.Count - 1]);

            dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDat_f1; i++)
            {
                dataGridView1.Rows.Add("", "", "", "");
            }
            // .......................................................................

            GlobalVarLn.flrd_f1 = 0;
            ReadFileFBSP_f1(1); // Читаем старый файл
            GlobalVarLn.list1_f1.Clear();

            if(GlobalVarLn.flrd_f1==0)
            {
                MessageBox.Show("No information");
                return;
            }

        } // Read tek
        // ************************************************************************


        // ФУНКЦИИ ****************************************************************


        // ****************************************************************************************
        // Обработка нажатия левой кнопки мыши при отрисовке OB1
        //
        // Входные параметры:
        // X - X, m на местности
        // Y - Y, m
        // ****************************************************************************************
        public void f_SPFB_f1()
        {

            if ((GlobalVarLn.flAz1_f1 == 0) && (chbXY.Checked==true))
            {
                GlobalVarLn.flAz1_f1 = 1;
                GlobalVarLn.Az1_f1 = GlobalVarLn.Az_f1;

                ichislo = (long)(GlobalVarLn.Az1_f1 * 100);
                dchislo = ((double)ichislo) / 100;
                dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[2].Value = (int)GlobalVarLn.Az1_f1; // seg

            }

            else if ((GlobalVarLn.flAz1_f1 == 1) && (GlobalVarLn.flAz2_f1==0) && (chbXY.Checked == true))
            {
                GlobalVarLn.flAz2_f1 = 1;
                GlobalVarLn.Az2_f1 = GlobalVarLn.Az_f1;

                ichislo = (long)(GlobalVarLn.Az2_f1 * 100);
                dchislo = ((double)ichislo) / 100;
                dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[3].Value = (int)GlobalVarLn.Az2_f1;// seg

                GlobalVarLn.flAz1_f1 = 0;
                GlobalVarLn.flAz2_f1 = 0;
                GlobalVarLn.Az1_f1 = 0;
                GlobalVarLn.Az2_f1 = 0;

                // Убрать с карты !!! Заменить в растровой
                //0219
                MapForm.REDRAW_MAP();
                //0219
                chbXY.Checked = false;
                //MapForm objMapForm=new MapForm();
                //objMapForm.lB.Text = "";


            }
        }
        // *************************************************************************************
        // Change Azimuth_enter from map
        // *************************************************************************************

        private void chbXY_CheckedChanged(object sender, EventArgs e)
        {
            //0221
            // !!! После ввода 1-го азимута отжали птичку
            if ((GlobalVarLn.flAz1_f1 == 1) && (chbXY.Checked == false))
            {
                GlobalVarLn.flAz1_f1 = 0;
                GlobalVarLn.flAz2_f1 = 0;
                GlobalVarLn.Az1_f1 = 0;
                GlobalVarLn.Az2_f1 = 0;
                // Убрать с карты !!! Заменить в растровой
                MapForm.REDRAW_MAP();
            }

        } // Change Azimuth_enter from map

        // *************************************************************************************
        // Перерисовка линии направления
        // way
        // *************************************************************************************
        //0219

        public void f_LuchReDraw(double x1, double y1, double x2, double y2)
        {

            List<Mapsui.Geometries.Point> pointPel = new List<Mapsui.Geometries.Point>();
            double lat = 0;
            double lon = 0;

            //for (int i = 0; i < GlobalVarLn.list_way.Count; i++)
            //{
                var p = Mercator.ToLonLat(x1, y1);
                lat = p.Y;
                lon = p.X;
                pointPel.Add(new Mapsui.Geometries.Point(lon, lat));
                var p1 = Mercator.ToLonLat(x2, y2);
                lat = p1.Y;
                lon = p1.X;
                pointPel.Add(new Mapsui.Geometries.Point(lon, lat));

            //} // FOR

            try
            {
                MapForm.RasterMapControl.AddPolyline(pointPel, Mapsui.Styles.Color.Red, 2);
            }
            catch
            {
                //MessageBox.Show(e.Message);
            }

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        } // P/P f_LuchReDraw
        // *************************************************************************************

        // ************************************************************************
        // функция чтения файла по FBСП 
        // ************************************************************************
        public void ReadFileFBSP_f1(int fl)
        {
            // ------------------------------------------------------------------------------------
            String strLine = "";
            String strLine1 = "";
            double number1 = 0;
            int number2 = 0;
            char symb1 = 'N';
            char symb2 = 'X';
            char symb3 = 'Y';
            char symb4 = '=';
            char symb5 = 'H';
            char symb6 = 'R';
            char symb7 = 'A';
            char symb8 = 'T';
            char symb9 = '{';
            char symb10 = '}';
            char symb11 = 'F';
            char symb12 = 'B';

            int indStart = 0;
            int indStop = 0;
            int iLength = 0;
            int IndZap = 0;
            int TekPoz = 0;
            int fi = 0;
            int if1 = 0;
            int indmas=0;

            // Чтение файла ---------------------------------------------------------
            String strFileName;

            //0219
            strFileName = Application.StartupPath + "\\SaveInFiles\\BandsSectReconn.txt";
            //strFileName = "FBSPRR.txt";

            StreamReader srFile;
            try
            {
                srFile = new StreamReader(strFileName);
            }
            catch
            {
                MessageBox.Show("Can't open file");
                return;

            }
            // -------------------------------------------------------------------------------------
            // Чтение
            // Numb =...

            // Fmin =...
            // Fmax =...
            // Bmin =...
            // Bmax =...
            // ...


            indmas = 0;

            try
            {
                FBSP objSP = new FBSP();
                objSP.mfb = new FB[GlobalVarLn.sizeDiap_f1 + 10];

                TekPoz = 0;
                IndZap = 0;
                // .......................................................
                // Numb =...
                // 1-я строка

                strLine = srFile.ReadLine();

                if ((strLine == null)&&(fl==0))
                {
                    srFile.Close();
                    return;
                }

                if (strLine == null)
                {
                    MessageBox.Show("No information");
                    srFile.Close();
                    return;
                }

                indStart = strLine.IndexOf(symb1, TekPoz); // N

                if (indStart == -1)
                {
                    MessageBox.Show("No information");
                    srFile.Close();
                    return;
                }

                indStop = strLine.IndexOf(symb4, TekPoz);  //=

                if ((indStop == -1) || (indStop < indStart))
                {
                    MessageBox.Show("No information");
                    srFile.Close();
                    return;
                }

                iLength = indStop - indStart + 1;
                // Убираем 'Numb ='
                strLine1 = strLine.Remove(indStart, iLength);

                if (strLine1 == "")
                {
                    MessageBox.Show("No information");
                    srFile.Close();
                    return;
                }

                objSP.NumbSP = strLine1;
                // .......................................................

                fi = 0;
                strLine = srFile.ReadLine(); // читаем далее (Fmin1=)
                if ((strLine == "") || (strLine == null))
                {
                    fi = 1;
                }
                // .......................................................

                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH
                while ((strLine != "") && (strLine != null))
                {

                    // .......................................................
                    // Fmin =...

                    indStart = strLine.IndexOf(symb11, TekPoz); // F
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    number1 = Convert.ToDouble(strLine1);

                    objSP.mfb[indmas].Fmin = number1;
                    // .......................................................
                    // Fmax =...

                    strLine = srFile.ReadLine();

                    indStart = strLine.IndexOf(symb11, TekPoz); // F
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    number1 = Convert.ToDouble(strLine1);

                    objSP.mfb[indmas].Fmax = number1;
                    // .......................................................
                    // Bmin =...

                    strLine = srFile.ReadLine();

                    indStart = strLine.IndexOf(symb12, TekPoz); // B
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    number1 = Convert.ToDouble(strLine1);

                    objSP.mfb[indmas].Bmin = number1;
                    // .......................................................
                    // Bmax =...

                    strLine = srFile.ReadLine();

                    indStart = strLine.IndexOf(symb12, TekPoz); // B
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    number1 = Convert.ToDouble(strLine1);

                    objSP.mfb[indmas].Bmax = number1;
                    // .......................................................
                    if ((fl == 1) &&
                        (String.Compare(objSP.NumbSP, GlobalVarLn.NumbSP_f1) == 0)
                       )
                    {
                        GlobalVarLn.flrd_f1 = 1;

                        dataGridView1.Rows[(int)(IndZap)].Cells[0].Value = objSP.mfb[indmas].Fmin;
                        dataGridView1.Rows[(int)(IndZap)].Cells[1].Value = objSP.mfb[indmas].Fmax;
                        dataGridView1.Rows[(int)(IndZap)].Cells[2].Value = objSP.mfb[indmas].Bmin;
                        dataGridView1.Rows[(int)(IndZap)].Cells[3].Value = objSP.mfb[indmas].Bmax;
                        IndZap += 1;

                    }
                    // .......................................................

                    fi = 0;
                    strLine = srFile.ReadLine(); // читаем далее (Numb/Fmin)
                    if ((strLine == "") || (strLine == null))
                        fi = 1;
                    // ...................................................................
                    if (strLine == null)
                    {
                        indmas += 1;
                        objSP.NumbDiap = indmas;
                        // Занести в List
                        GlobalVarLn.list1_f1.Add(objSP);
                        srFile.Close();
                        return;
                    }
                    // ...............................................
                    indStart = strLine.IndexOf(symb1, TekPoz); // N

                    if (indStart != -1)
                    {
                        indStop = strLine.IndexOf(symb4, TekPoz);  //=
                        iLength = indStop - indStart + 1;
                        iLength = indStop - indStart + 1;
                        // Убираем 'Numb ='
                        strLine1 = strLine.Remove(indStart, iLength);
                        indmas += 1;
                        objSP.NumbDiap = indmas;
                        // Занести в List
                        GlobalVarLn.list1_f1.Add(objSP); // предыдущий

                        objSP.NumbSP = strLine1;// новый номер
                        indmas = 0;
                        objSP.NumbDiap = 0;

                        fi = 0;
                        strLine = srFile.ReadLine(); // читаем далее (Fmini=)
                        if ((strLine == "") || (strLine == null))
                        {
                            fi = 1;
                        }

                    }
                    else
                    {
                        indmas += 1;
                    }



                } // WHILE
                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH

                // Занести в List
                GlobalVarLn.list1_f1.Add(objSP);

            }
            // -----------------------------------------------------------------------------------
            catch
            {
                srFile.Close();
                return;
            }
            // -------------------------------------------------------------------------------------
            srFile.Close();
            // -------------------------------------------------------------------------------------

        } // ReadfileFBSP_f1
        // ************************************************************************


        // **************************************************************** ФУНКЦИИ


        private void button2_Click(object sender, EventArgs e)
        {

        }

        // ****************************************************************************************
        // Закрыть форму
        // ****************************************************************************************
        private void FormSPFB_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

            // приостановить обработку, если идет
            GlobalVarLn.flF_f1 = 0;

            //GlobalVarLn.fFSPFB = 0;

            GlobalVarLn.fl_Open_objFormSPFB = 0;

        } // Closing
        // ****************************************************************************************
        // Активизировать форму
        // *************************************************************************************
        private void FormSPFB_Activated(object sender, EventArgs e)
        {
            //GlobalVarLn.objFormSPFBG.WindowState = FormWindowState.Normal;

            GlobalVarLn.flF_f1 = 1;

            //GlobalVarLn.fFSPFB = 1;
            //ClassMap.f_RemoveFrm(14);

            GlobalVarLn.fl_Open_objFormSPFB = 1;


        } // Activated


        // ****************************************************************************************


    } // Class
} // Namespace
