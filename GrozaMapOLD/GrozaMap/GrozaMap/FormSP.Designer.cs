﻿namespace GrozaMap
{
    partial class FormSP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gbDegMin = new System.Windows.Forms.GroupBox();
            this.tbLMin1 = new System.Windows.Forms.TextBox();
            this.tbBMin1 = new System.Windows.Forms.TextBox();
            this.lLDegMin = new System.Windows.Forms.Label();
            this.lBDegMin = new System.Windows.Forms.Label();
            this.tbOwnHeight = new System.Windows.Forms.TextBox();
            this.gbRect = new System.Windows.Forms.GroupBox();
            this.tbYRect = new System.Windows.Forms.TextBox();
            this.lYRect = new System.Windows.Forms.Label();
            this.tbXRect = new System.Windows.Forms.TextBox();
            this.lXRect = new System.Windows.Forms.Label();
            this.lBRad = new System.Windows.Forms.Label();
            this.bClear = new System.Windows.Forms.Button();
            this.bAccept = new System.Windows.Forms.Button();
            this.lMiddleHeight = new System.Windows.Forms.Label();
            this.tbBRad = new System.Windows.Forms.TextBox();
            this.tbLRad = new System.Windows.Forms.TextBox();
            this.gbRect42 = new System.Windows.Forms.GroupBox();
            this.tbYRect42 = new System.Windows.Forms.TextBox();
            this.lYRect42 = new System.Windows.Forms.Label();
            this.tbXRect42 = new System.Windows.Forms.TextBox();
            this.lXRect42 = new System.Windows.Forms.Label();
            this.lLRad = new System.Windows.Forms.Label();
            this.gbDegMinSec = new System.Windows.Forms.GroupBox();
            this.tbLSec = new System.Windows.Forms.TextBox();
            this.tbBSec = new System.Windows.Forms.TextBox();
            this.lMin4 = new System.Windows.Forms.Label();
            this.lMin3 = new System.Windows.Forms.Label();
            this.tbLMin2 = new System.Windows.Forms.TextBox();
            this.tbBMin2 = new System.Windows.Forms.TextBox();
            this.lSec2 = new System.Windows.Forms.Label();
            this.lSec1 = new System.Windows.Forms.Label();
            this.lDeg4 = new System.Windows.Forms.Label();
            this.lDeg3 = new System.Windows.Forms.Label();
            this.tbLDeg2 = new System.Windows.Forms.TextBox();
            this.lLDegMinSec = new System.Windows.Forms.Label();
            this.tbBDeg2 = new System.Windows.Forms.TextBox();
            this.lBDegMinSec = new System.Windows.Forms.Label();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.cbChooseSC = new System.Windows.Forms.ComboBox();
            this.pCoordPoint = new System.Windows.Forms.Panel();
            this.lChooseSC = new System.Windows.Forms.Label();
            this.gbRad = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.chbXY = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tbNumSP = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.pbSP = new System.Windows.Forms.PictureBox();
            this.buttonZSP = new System.Windows.Forms.Button();
            this.imageList1V = new System.Windows.Forms.ImageList(this.components);
            this.CurrentRadioButton1 = new System.Windows.Forms.RadioButton();
            this.GroupBoxPanel = new System.Windows.Forms.Panel();
            this.CurrentRadioButton2 = new System.Windows.Forms.RadioButton();
            this.PlannedRadioButton1 = new System.Windows.Forms.RadioButton();
            this.PlannedRadioButton2 = new System.Windows.Forms.RadioButton();
            this.bGetFreq = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.spView2 = new GrozaMap.SPView();
            this.spView1 = new GrozaMap.SPView();
            this.button4 = new System.Windows.Forms.Button();
            this.gbDegMin.SuspendLayout();
            this.gbRect.SuspendLayout();
            this.gbRect42.SuspendLayout();
            this.gbDegMinSec.SuspendLayout();
            this.pCoordPoint.SuspendLayout();
            this.gbRad.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSP)).BeginInit();
            this.GroupBoxPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbDegMin
            // 
            this.gbDegMin.Controls.Add(this.tbLMin1);
            this.gbDegMin.Controls.Add(this.tbBMin1);
            this.gbDegMin.Controls.Add(this.lLDegMin);
            this.gbDegMin.Controls.Add(this.lBDegMin);
            this.gbDegMin.Location = new System.Drawing.Point(3, 37);
            this.gbDegMin.Name = "gbDegMin";
            this.gbDegMin.Size = new System.Drawing.Size(150, 63);
            this.gbDegMin.TabIndex = 29;
            this.gbDegMin.TabStop = false;
            this.gbDegMin.Visible = false;
            // 
            // tbLMin1
            // 
            this.tbLMin1.Location = new System.Drawing.Point(65, 38);
            this.tbLMin1.MaxLength = 8;
            this.tbLMin1.Name = "tbLMin1";
            this.tbLMin1.Size = new System.Drawing.Size(74, 20);
            this.tbLMin1.TabIndex = 15;
            // 
            // tbBMin1
            // 
            this.tbBMin1.Location = new System.Drawing.Point(65, 15);
            this.tbBMin1.MaxLength = 8;
            this.tbBMin1.Name = "tbBMin1";
            this.tbBMin1.Size = new System.Drawing.Size(74, 20);
            this.tbBMin1.TabIndex = 13;
            // 
            // lLDegMin
            // 
            this.lLDegMin.AutoSize = true;
            this.lLDegMin.Location = new System.Drawing.Point(4, 41);
            this.lLDegMin.Name = "lLDegMin";
            this.lLDegMin.Size = new System.Drawing.Size(64, 13);
            this.lLDegMin.TabIndex = 12;
            this.lLDegMin.Text = "L.................";
            // 
            // lBDegMin
            // 
            this.lBDegMin.AutoSize = true;
            this.lBDegMin.Location = new System.Drawing.Point(4, 18);
            this.lBDegMin.Name = "lBDegMin";
            this.lBDegMin.Size = new System.Drawing.Size(59, 13);
            this.lBDegMin.TabIndex = 10;
            this.lBDegMin.Text = "B...............";
            // 
            // tbOwnHeight
            // 
            this.tbOwnHeight.BackColor = System.Drawing.SystemColors.Menu;
            this.tbOwnHeight.Location = new System.Drawing.Point(379, 510);
            this.tbOwnHeight.Name = "tbOwnHeight";
            this.tbOwnHeight.Size = new System.Drawing.Size(64, 20);
            this.tbOwnHeight.TabIndex = 152;
            this.tbOwnHeight.Visible = false;
            // 
            // gbRect
            // 
            this.gbRect.Controls.Add(this.tbYRect);
            this.gbRect.Controls.Add(this.lYRect);
            this.gbRect.Controls.Add(this.tbXRect);
            this.gbRect.Controls.Add(this.lXRect);
            this.gbRect.Location = new System.Drawing.Point(7, 30);
            this.gbRect.Name = "gbRect";
            this.gbRect.Size = new System.Drawing.Size(160, 63);
            this.gbRect.TabIndex = 17;
            this.gbRect.TabStop = false;
            this.gbRect.Visible = false;
            // 
            // tbYRect
            // 
            this.tbYRect.Location = new System.Drawing.Point(78, 36);
            this.tbYRect.MaxLength = 7;
            this.tbYRect.Name = "tbYRect";
            this.tbYRect.Size = new System.Drawing.Size(75, 20);
            this.tbYRect.TabIndex = 5;
            // 
            // lYRect
            // 
            this.lYRect.AutoSize = true;
            this.lYRect.Location = new System.Drawing.Point(4, 42);
            this.lYRect.Name = "lYRect";
            this.lYRect.Size = new System.Drawing.Size(85, 13);
            this.lYRect.TabIndex = 6;
            this.lYRect.Text = "Y, м...................";
            // 
            // tbXRect
            // 
            this.tbXRect.Location = new System.Drawing.Point(78, 13);
            this.tbXRect.MaxLength = 7;
            this.tbXRect.Name = "tbXRect";
            this.tbXRect.Size = new System.Drawing.Size(75, 20);
            this.tbXRect.TabIndex = 4;
            // 
            // lXRect
            // 
            this.lXRect.AutoSize = true;
            this.lXRect.Location = new System.Drawing.Point(4, 20);
            this.lXRect.Name = "lXRect";
            this.lXRect.Size = new System.Drawing.Size(85, 13);
            this.lXRect.TabIndex = 3;
            this.lXRect.Text = "X, м...................";
            // 
            // lBRad
            // 
            this.lBRad.AutoSize = true;
            this.lBRad.Location = new System.Drawing.Point(4, 18);
            this.lBRad.Name = "lBRad";
            this.lBRad.Size = new System.Drawing.Size(95, 13);
            this.lBRad.TabIndex = 3;
            this.lBRad.Text = "B, рад...................";
            // 
            // bClear
            // 
            this.bClear.Location = new System.Drawing.Point(176, 2);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(42, 23);
            this.bClear.TabIndex = 146;
            this.bClear.Text = "Clear";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // bAccept
            // 
            this.bAccept.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bAccept.Location = new System.Drawing.Point(79, 2);
            this.bAccept.Name = "bAccept";
            this.bAccept.Size = new System.Drawing.Size(53, 23);
            this.bAccept.TabIndex = 145;
            this.bAccept.Text = "Save";
            this.bAccept.UseVisualStyleBackColor = true;
            this.bAccept.Click += new System.EventHandler(this.bAccept_Click);
            // 
            // lMiddleHeight
            // 
            this.lMiddleHeight.AutoSize = true;
            this.lMiddleHeight.Location = new System.Drawing.Point(203, 517);
            this.lMiddleHeight.Name = "lMiddleHeight";
            this.lMiddleHeight.Size = new System.Drawing.Size(98, 13);
            this.lMiddleHeight.TabIndex = 143;
            this.lMiddleHeight.Text = "Высота, м.............";
            this.lMiddleHeight.Visible = false;
            this.lMiddleHeight.Click += new System.EventHandler(this.lMiddleHeight_Click);
            // 
            // tbBRad
            // 
            this.tbBRad.Location = new System.Drawing.Point(78, 13);
            this.tbBRad.MaxLength = 10;
            this.tbBRad.Name = "tbBRad";
            this.tbBRad.Size = new System.Drawing.Size(75, 20);
            this.tbBRad.TabIndex = 4;
            // 
            // tbLRad
            // 
            this.tbLRad.Location = new System.Drawing.Point(78, 36);
            this.tbLRad.MaxLength = 10;
            this.tbLRad.Name = "tbLRad";
            this.tbLRad.Size = new System.Drawing.Size(75, 20);
            this.tbLRad.TabIndex = 5;
            // 
            // gbRect42
            // 
            this.gbRect42.Controls.Add(this.tbYRect42);
            this.gbRect42.Controls.Add(this.lYRect42);
            this.gbRect42.Controls.Add(this.tbXRect42);
            this.gbRect42.Controls.Add(this.lXRect42);
            this.gbRect42.Location = new System.Drawing.Point(3, 37);
            this.gbRect42.Name = "gbRect42";
            this.gbRect42.Size = new System.Drawing.Size(160, 63);
            this.gbRect42.TabIndex = 27;
            this.gbRect42.TabStop = false;
            this.gbRect42.Visible = false;
            // 
            // tbYRect42
            // 
            this.tbYRect42.Location = new System.Drawing.Point(78, 36);
            this.tbYRect42.MaxLength = 7;
            this.tbYRect42.Name = "tbYRect42";
            this.tbYRect42.Size = new System.Drawing.Size(75, 20);
            this.tbYRect42.TabIndex = 5;
            // 
            // lYRect42
            // 
            this.lYRect42.AutoSize = true;
            this.lYRect42.Location = new System.Drawing.Point(4, 42);
            this.lYRect42.Name = "lYRect42";
            this.lYRect42.Size = new System.Drawing.Size(85, 13);
            this.lYRect42.TabIndex = 6;
            this.lYRect42.Text = "Y, м...................";
            // 
            // tbXRect42
            // 
            this.tbXRect42.Location = new System.Drawing.Point(78, 13);
            this.tbXRect42.MaxLength = 7;
            this.tbXRect42.Name = "tbXRect42";
            this.tbXRect42.Size = new System.Drawing.Size(75, 20);
            this.tbXRect42.TabIndex = 4;
            // 
            // lXRect42
            // 
            this.lXRect42.AutoSize = true;
            this.lXRect42.Location = new System.Drawing.Point(4, 20);
            this.lXRect42.Name = "lXRect42";
            this.lXRect42.Size = new System.Drawing.Size(85, 13);
            this.lXRect42.TabIndex = 3;
            this.lXRect42.Text = "X, м...................";
            // 
            // lLRad
            // 
            this.lLRad.AutoSize = true;
            this.lLRad.Location = new System.Drawing.Point(4, 41);
            this.lLRad.Name = "lLRad";
            this.lLRad.Size = new System.Drawing.Size(94, 13);
            this.lLRad.TabIndex = 6;
            this.lLRad.Text = "L, рад...................";
            // 
            // gbDegMinSec
            // 
            this.gbDegMinSec.Controls.Add(this.tbLSec);
            this.gbDegMinSec.Controls.Add(this.tbBSec);
            this.gbDegMinSec.Controls.Add(this.lMin4);
            this.gbDegMinSec.Controls.Add(this.lMin3);
            this.gbDegMinSec.Controls.Add(this.tbLMin2);
            this.gbDegMinSec.Controls.Add(this.tbBMin2);
            this.gbDegMinSec.Controls.Add(this.lSec2);
            this.gbDegMinSec.Controls.Add(this.lSec1);
            this.gbDegMinSec.Controls.Add(this.lDeg4);
            this.gbDegMinSec.Controls.Add(this.lDeg3);
            this.gbDegMinSec.Controls.Add(this.tbLDeg2);
            this.gbDegMinSec.Controls.Add(this.lLDegMinSec);
            this.gbDegMinSec.Controls.Add(this.tbBDeg2);
            this.gbDegMinSec.Controls.Add(this.lBDegMinSec);
            this.gbDegMinSec.Location = new System.Drawing.Point(3, 34);
            this.gbDegMinSec.Name = "gbDegMinSec";
            this.gbDegMinSec.Size = new System.Drawing.Size(158, 59);
            this.gbDegMinSec.TabIndex = 30;
            this.gbDegMinSec.TabStop = false;
            this.gbDegMinSec.Visible = false;
            // 
            // tbLSec
            // 
            this.tbLSec.Location = new System.Drawing.Point(119, 34);
            this.tbLSec.MaxLength = 3;
            this.tbLSec.Name = "tbLSec";
            this.tbLSec.Size = new System.Drawing.Size(25, 20);
            this.tbLSec.TabIndex = 23;
            // 
            // tbBSec
            // 
            this.tbBSec.Location = new System.Drawing.Point(119, 13);
            this.tbBSec.MaxLength = 3;
            this.tbBSec.Name = "tbBSec";
            this.tbBSec.Size = new System.Drawing.Size(25, 20);
            this.tbBSec.TabIndex = 18;
            // 
            // lMin4
            // 
            this.lMin4.AutoSize = true;
            this.lMin4.Location = new System.Drawing.Point(110, 36);
            this.lMin4.Name = "lMin4";
            this.lMin4.Size = new System.Drawing.Size(9, 13);
            this.lMin4.TabIndex = 27;
            this.lMin4.Text = "\'";
            // 
            // lMin3
            // 
            this.lMin3.AutoSize = true;
            this.lMin3.Location = new System.Drawing.Point(110, 14);
            this.lMin3.Name = "lMin3";
            this.lMin3.Size = new System.Drawing.Size(9, 13);
            this.lMin3.TabIndex = 26;
            this.lMin3.Text = "\'";
            // 
            // tbLMin2
            // 
            this.tbLMin2.Location = new System.Drawing.Point(84, 34);
            this.tbLMin2.MaxLength = 2;
            this.tbLMin2.Name = "tbLMin2";
            this.tbLMin2.Size = new System.Drawing.Size(25, 20);
            this.tbLMin2.TabIndex = 22;
            // 
            // tbBMin2
            // 
            this.tbBMin2.Location = new System.Drawing.Point(84, 13);
            this.tbBMin2.MaxLength = 2;
            this.tbBMin2.Name = "tbBMin2";
            this.tbBMin2.Size = new System.Drawing.Size(25, 20);
            this.tbBMin2.TabIndex = 16;
            // 
            // lSec2
            // 
            this.lSec2.AutoSize = true;
            this.lSec2.Location = new System.Drawing.Point(145, 35);
            this.lSec2.Name = "lSec2";
            this.lSec2.Size = new System.Drawing.Size(9, 13);
            this.lSec2.TabIndex = 25;
            this.lSec2.Text = "\'";
            // 
            // lSec1
            // 
            this.lSec1.AutoSize = true;
            this.lSec1.Location = new System.Drawing.Point(145, 15);
            this.lSec1.Name = "lSec1";
            this.lSec1.Size = new System.Drawing.Size(9, 13);
            this.lSec1.TabIndex = 24;
            this.lSec1.Text = "\'";
            // 
            // lDeg4
            // 
            this.lDeg4.AutoSize = true;
            this.lDeg4.Location = new System.Drawing.Point(69, 36);
            this.lDeg4.Name = "lDeg4";
            this.lDeg4.Size = new System.Drawing.Size(13, 13);
            this.lDeg4.TabIndex = 21;
            this.lDeg4.Text = "^";
            // 
            // lDeg3
            // 
            this.lDeg3.AutoSize = true;
            this.lDeg3.Location = new System.Drawing.Point(69, 14);
            this.lDeg3.Name = "lDeg3";
            this.lDeg3.Size = new System.Drawing.Size(13, 13);
            this.lDeg3.TabIndex = 20;
            this.lDeg3.Text = "^";
            // 
            // tbLDeg2
            // 
            this.tbLDeg2.Location = new System.Drawing.Point(43, 32);
            this.tbLDeg2.MaxLength = 2;
            this.tbLDeg2.Name = "tbLDeg2";
            this.tbLDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbLDeg2.TabIndex = 19;
            // 
            // lLDegMinSec
            // 
            this.lLDegMinSec.AutoSize = true;
            this.lLDegMinSec.Location = new System.Drawing.Point(4, 41);
            this.lLDegMinSec.Name = "lLDegMinSec";
            this.lLDegMinSec.Size = new System.Drawing.Size(64, 13);
            this.lLDegMinSec.TabIndex = 17;
            this.lLDegMinSec.Text = "L.................";
            // 
            // tbBDeg2
            // 
            this.tbBDeg2.Location = new System.Drawing.Point(43, 15);
            this.tbBDeg2.MaxLength = 2;
            this.tbBDeg2.Name = "tbBDeg2";
            this.tbBDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbBDeg2.TabIndex = 15;
            // 
            // lBDegMinSec
            // 
            this.lBDegMinSec.AutoSize = true;
            this.lBDegMinSec.Location = new System.Drawing.Point(4, 18);
            this.lBDegMinSec.Name = "lBDegMinSec";
            this.lBDegMinSec.Size = new System.Drawing.Size(59, 13);
            this.lBDegMinSec.TabIndex = 14;
            this.lBDegMinSec.Text = "B...............";
            // 
            // cbChooseSC
            // 
            this.cbChooseSC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChooseSC.DropDownWidth = 150;
            this.cbChooseSC.FormattingEnabled = true;
            this.cbChooseSC.Items.AddRange(new object[] {
            "Метры на мест-ти",
            "Радианы",
            "Градусы",
            "Град, мин, сек"});
            this.cbChooseSC.Location = new System.Drawing.Point(45, 4);
            this.cbChooseSC.Name = "cbChooseSC";
            this.cbChooseSC.Size = new System.Drawing.Size(123, 21);
            this.cbChooseSC.TabIndex = 11;
            this.cbChooseSC.Visible = false;
            this.cbChooseSC.SelectedIndexChanged += new System.EventHandler(this.cbChooseSC_SelectedIndexChanged);
            // 
            // pCoordPoint
            // 
            this.pCoordPoint.Controls.Add(this.gbDegMinSec);
            this.pCoordPoint.Controls.Add(this.gbRect42);
            this.pCoordPoint.Controls.Add(this.gbDegMin);
            this.pCoordPoint.Controls.Add(this.gbRect);
            this.pCoordPoint.Controls.Add(this.cbChooseSC);
            this.pCoordPoint.Controls.Add(this.lChooseSC);
            this.pCoordPoint.Controls.Add(this.gbRad);
            this.pCoordPoint.Location = new System.Drawing.Point(12, 497);
            this.pCoordPoint.Name = "pCoordPoint";
            this.pCoordPoint.Size = new System.Drawing.Size(173, 103);
            this.pCoordPoint.TabIndex = 136;
            this.pCoordPoint.Visible = false;
            // 
            // lChooseSC
            // 
            this.lChooseSC.AutoSize = true;
            this.lChooseSC.Location = new System.Drawing.Point(8, 13);
            this.lChooseSC.Name = "lChooseSC";
            this.lChooseSC.Size = new System.Drawing.Size(36, 13);
            this.lChooseSC.TabIndex = 12;
            this.lChooseSC.Text = "СК.....";
            this.lChooseSC.Visible = false;
            // 
            // gbRad
            // 
            this.gbRad.Controls.Add(this.tbLRad);
            this.gbRad.Controls.Add(this.lLRad);
            this.gbRad.Controls.Add(this.tbBRad);
            this.gbRad.Controls.Add(this.lBRad);
            this.gbRad.Location = new System.Drawing.Point(1, 37);
            this.gbRad.Name = "gbRad";
            this.gbRad.Size = new System.Drawing.Size(160, 63);
            this.gbRad.TabIndex = 28;
            this.gbRad.TabStop = false;
            this.gbRad.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(136, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(39, 23);
            this.button1.TabIndex = 200;
            this.button1.Text = "Load";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // chbXY
            // 
            this.chbXY.AutoSize = true;
            this.chbXY.Location = new System.Drawing.Point(206, 501);
            this.chbXY.Name = "chbXY";
            this.chbXY.Size = new System.Drawing.Size(15, 14);
            this.chbXY.TabIndex = 201;
            this.chbXY.UseVisualStyleBackColor = true;
            this.chbXY.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(227, 497);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(133, 13);
            this.label15.TabIndex = 202;
            this.label15.Text = "Ручной выбор координат";
            this.label15.Visible = false;
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // tbNumSP
            // 
            this.tbNumSP.Location = new System.Drawing.Point(279, 554);
            this.tbNumSP.MaxLength = 10;
            this.tbNumSP.Name = "tbNumSP";
            this.tbNumSP.Size = new System.Drawing.Size(75, 20);
            this.tbNumSP.TabIndex = 204;
            this.tbNumSP.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(410, 561);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 205;
            this.label1.Text = "№СП";
            this.label1.Visible = false;
            // 
            // button3
            // 
            this.button3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button3.Location = new System.Drawing.Point(4, 27);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(61, 23);
            this.button3.TabIndex = 206;
            this.button3.Text = "Delete JS\r\n";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(52, 40);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // pbSP
            // 
            this.pbSP.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbSP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbSP.Location = new System.Drawing.Point(224, 5);
            this.pbSP.Name = "pbSP";
            this.pbSP.Size = new System.Drawing.Size(46, 42);
            this.pbSP.TabIndex = 213;
            this.pbSP.TabStop = false;
            // 
            // buttonZSP
            // 
            this.buttonZSP.Location = new System.Drawing.Point(274, 1);
            this.buttonZSP.Name = "buttonZSP";
            this.buttonZSP.Size = new System.Drawing.Size(71, 23);
            this.buttonZSP.TabIndex = 214;
            this.buttonZSP.Text = "Forward";
            this.buttonZSP.UseVisualStyleBackColor = true;
            this.buttonZSP.Click += new System.EventHandler(this.buttonZSP_Click);
            // 
            // imageList1V
            // 
            this.imageList1V.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1V.ImageSize = new System.Drawing.Size(52, 40);
            this.imageList1V.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // CurrentRadioButton1
            // 
            this.CurrentRadioButton1.AutoSize = true;
            this.CurrentRadioButton1.Checked = true;
            this.CurrentRadioButton1.Location = new System.Drawing.Point(3, 56);
            this.CurrentRadioButton1.Name = "CurrentRadioButton1";
            this.CurrentRadioButton1.Size = new System.Drawing.Size(14, 13);
            this.CurrentRadioButton1.TabIndex = 215;
            this.CurrentRadioButton1.TabStop = true;
            this.CurrentRadioButton1.UseVisualStyleBackColor = true;
            this.CurrentRadioButton1.CheckedChanged += new System.EventHandler(this.CurrentRadioButton1_CheckedChanged);
            // 
            // GroupBoxPanel
            // 
            this.GroupBoxPanel.Controls.Add(this.CurrentRadioButton2);
            this.GroupBoxPanel.Controls.Add(this.PlannedRadioButton1);
            this.GroupBoxPanel.Controls.Add(this.PlannedRadioButton2);
            this.GroupBoxPanel.Controls.Add(this.CurrentRadioButton1);
            this.GroupBoxPanel.Location = new System.Drawing.Point(3, 52);
            this.GroupBoxPanel.Name = "GroupBoxPanel";
            this.GroupBoxPanel.Size = new System.Drawing.Size(20, 210);
            this.GroupBoxPanel.TabIndex = 216;
            // 
            // CurrentRadioButton2
            // 
            this.CurrentRadioButton2.AutoSize = true;
            this.CurrentRadioButton2.Location = new System.Drawing.Point(3, 163);
            this.CurrentRadioButton2.Name = "CurrentRadioButton2";
            this.CurrentRadioButton2.Size = new System.Drawing.Size(14, 13);
            this.CurrentRadioButton2.TabIndex = 218;
            this.CurrentRadioButton2.UseVisualStyleBackColor = true;
            this.CurrentRadioButton2.CheckedChanged += new System.EventHandler(this.CurrentRadioButton1_CheckedChanged);
            // 
            // PlannedRadioButton1
            // 
            this.PlannedRadioButton1.AutoSize = true;
            this.PlannedRadioButton1.Location = new System.Drawing.Point(3, 81);
            this.PlannedRadioButton1.Name = "PlannedRadioButton1";
            this.PlannedRadioButton1.Size = new System.Drawing.Size(14, 13);
            this.PlannedRadioButton1.TabIndex = 217;
            this.PlannedRadioButton1.UseVisualStyleBackColor = true;
            this.PlannedRadioButton1.CheckedChanged += new System.EventHandler(this.CurrentRadioButton1_CheckedChanged);
            // 
            // PlannedRadioButton2
            // 
            this.PlannedRadioButton2.AutoSize = true;
            this.PlannedRadioButton2.Location = new System.Drawing.Point(3, 190);
            this.PlannedRadioButton2.Name = "PlannedRadioButton2";
            this.PlannedRadioButton2.Size = new System.Drawing.Size(14, 13);
            this.PlannedRadioButton2.TabIndex = 216;
            this.PlannedRadioButton2.UseVisualStyleBackColor = true;
            this.PlannedRadioButton2.CheckedChanged += new System.EventHandler(this.CurrentRadioButton1_CheckedChanged);
            // 
            // bGetFreq
            // 
            this.bGetFreq.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.bGetFreq.Location = new System.Drawing.Point(98, 27);
            this.bGetFreq.Name = "bGetFreq";
            this.bGetFreq.Size = new System.Drawing.Size(107, 23);
            this.bGetFreq.TabIndex = 242;
            this.bGetFreq.Text = "Accept coordinates";
            this.bGetFreq.UseVisualStyleBackColor = true;
            this.bGetFreq.Click += new System.EventHandler(this.bGetFreq_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(273, 26);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(71, 23);
            this.button2.TabIndex = 243;
            this.button2.Text = "Backward";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // spView2
            // 
            this.spView2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.spView2.CurrentHeight = 0D;
            this.spView2.CurrentLatitude = 0D;
            this.spView2.CurrentLongitude = 0D;
            this.spView2.IP = 35;
            this.spView2.Location = new System.Drawing.Point(26, 159);
            this.spView2.Name = "spView2";
            this.spView2.NameSP = "112";
            this.spView2.PlannedHeight = 0D;
            this.spView2.PlannedLatitude = 0D;
            this.spView2.PlannedLongitude = 0D;
            this.spView2.Size = new System.Drawing.Size(319, 102);
            this.spView2.TabIndex = 218;
            this.spView2.TypeSP = "Groza-S";
            // 
            // spView1
            // 
            this.spView1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.spView1.CurrentHeight = 0D;
            this.spView1.CurrentLatitude = 0D;
            this.spView1.CurrentLongitude = 0D;
            this.spView1.IP = 33;
            this.spView1.Location = new System.Drawing.Point(27, 53);
            this.spView1.Name = "spView1";
            this.spView1.NameSP = "111";
            this.spView1.PlannedHeight = 0D;
            this.spView1.PlannedLatitude = 0D;
            this.spView1.PlannedLongitude = 0D;
            this.spView1.Size = new System.Drawing.Size(319, 102);
            this.spView1.TabIndex = 217;
            this.spView1.TypeSP = "Groza-S";
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button4.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button4.Location = new System.Drawing.Point(3, 1);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(61, 23);
            this.button4.TabIndex = 244;
            this.button4.Text = "Enter JS";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // FormSP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 266);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.bGetFreq);
            this.Controls.Add(this.spView2);
            this.Controls.Add(this.spView1);
            this.Controls.Add(this.GroupBoxPanel);
            this.Controls.Add(this.buttonZSP);
            this.Controls.Add(this.pbSP);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbNumSP);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.chbXY);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tbOwnHeight);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.bAccept);
            this.Controls.Add(this.lMiddleHeight);
            this.Controls.Add(this.pCoordPoint);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(800, 100);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSP";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Jammer stations (JS)";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.FormSP_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSP_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormSP_FormClosed);
            this.Load += new System.EventHandler(this.FormSP_Load);
            this.gbDegMin.ResumeLayout(false);
            this.gbDegMin.PerformLayout();
            this.gbRect.ResumeLayout(false);
            this.gbRect.PerformLayout();
            this.gbRect42.ResumeLayout(false);
            this.gbRect42.PerformLayout();
            this.gbDegMinSec.ResumeLayout(false);
            this.gbDegMinSec.PerformLayout();
            this.pCoordPoint.ResumeLayout(false);
            this.pCoordPoint.PerformLayout();
            this.gbRad.ResumeLayout(false);
            this.gbRad.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbSP)).EndInit();
            this.GroupBoxPanel.ResumeLayout(false);
            this.GroupBoxPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox tbLMin1;
        public System.Windows.Forms.TextBox tbBMin1;
        private System.Windows.Forms.Label lLDegMin;
        private System.Windows.Forms.Label lBDegMin;
        public System.Windows.Forms.TextBox tbYRect;
        private System.Windows.Forms.Label lYRect;
        public System.Windows.Forms.TextBox tbXRect;
        private System.Windows.Forms.Label lXRect;
        private System.Windows.Forms.Label lBRad;
        private System.Windows.Forms.Button bClear;
        private System.Windows.Forms.Button bAccept;
        private System.Windows.Forms.Label lMiddleHeight;
        public System.Windows.Forms.TextBox tbBRad;
        public System.Windows.Forms.TextBox tbLRad;
        public System.Windows.Forms.TextBox tbYRect42;
        private System.Windows.Forms.Label lYRect42;
        public System.Windows.Forms.TextBox tbXRect42;
        private System.Windows.Forms.Label lXRect42;
        private System.Windows.Forms.Label lLRad;
        public System.Windows.Forms.TextBox tbLSec;
        public System.Windows.Forms.TextBox tbBSec;
        private System.Windows.Forms.Label lMin4;
        private System.Windows.Forms.Label lMin3;
        public System.Windows.Forms.TextBox tbLMin2;
        public System.Windows.Forms.TextBox tbBMin2;
        private System.Windows.Forms.Label lSec2;
        private System.Windows.Forms.Label lSec1;
        private System.Windows.Forms.Label lDeg4;
        private System.Windows.Forms.Label lDeg3;
        public System.Windows.Forms.TextBox tbLDeg2;
        private System.Windows.Forms.Label lLDegMinSec;
        public System.Windows.Forms.TextBox tbBDeg2;
        private System.Windows.Forms.Label lBDegMinSec;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Panel pCoordPoint;
        private System.Windows.Forms.Label lChooseSC;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label15;
        public System.Windows.Forms.GroupBox gbRect;
        public System.Windows.Forms.GroupBox gbRect42;
        public System.Windows.Forms.GroupBox gbRad;
        public System.Windows.Forms.GroupBox gbDegMin;
        public System.Windows.Forms.GroupBox gbDegMinSec;
        public System.Windows.Forms.ComboBox cbChooseSC;
        public System.Windows.Forms.CheckBox chbXY;
        public System.Windows.Forms.TextBox tbOwnHeight;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button3;
        public System.Windows.Forms.TextBox tbNumSP;
        private System.Windows.Forms.Button buttonZSP;
        public System.Windows.Forms.PictureBox pbSP;
        private System.Windows.Forms.RadioButton CurrentRadioButton1;
        private System.Windows.Forms.Panel GroupBoxPanel;
        private System.Windows.Forms.RadioButton PlannedRadioButton2;
        private SPView spView1;
        private SPView spView2;
        private System.Windows.Forms.RadioButton CurrentRadioButton2;
        private System.Windows.Forms.RadioButton PlannedRadioButton1;
        public System.Windows.Forms.ImageList imageList1;
        public System.Windows.Forms.ImageList imageList1V;
        private System.Windows.Forms.Button bGetFreq;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
    }
}