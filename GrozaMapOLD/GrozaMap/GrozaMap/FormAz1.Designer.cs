﻿namespace GrozaMap
{
    partial class FormAz1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button2 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.chbXY = new System.Windows.Forms.CheckBox();
            this.bClear = new System.Windows.Forms.Button();
            this.lMiddleHeight = new System.Windows.Forms.Label();
            this.lXRect = new System.Windows.Forms.Label();
            this.tbXRect = new System.Windows.Forms.TextBox();
            this.lYRect = new System.Windows.Forms.Label();
            this.tbYRect = new System.Windows.Forms.TextBox();
            this.gbRect = new System.Windows.Forms.GroupBox();
            this.tbOwnHeight = new System.Windows.Forms.TextBox();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.gbRect.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.dataGridView1.Location = new System.Drawing.Point(167, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(306, 145);
            this.dataGridView1.TabIndex = 212;
            // 
            // button2
            // 
            this.button2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button2.Location = new System.Drawing.Point(1, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(74, 23);
            this.button2.TabIndex = 216;
            this.button2.Text = "Accept";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(36, 130);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 13);
            this.label15.TabIndex = 215;
            this.label15.Text = "Show on map";
            this.label15.Visible = false;
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // chbXY
            // 
            this.chbXY.AutoSize = true;
            this.chbXY.Location = new System.Drawing.Point(4, 129);
            this.chbXY.Name = "chbXY";
            this.chbXY.Size = new System.Drawing.Size(15, 14);
            this.chbXY.TabIndex = 214;
            this.chbXY.UseVisualStyleBackColor = true;
            this.chbXY.Visible = false;
            // 
            // bClear
            // 
            this.bClear.Location = new System.Drawing.Point(80, 4);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(75, 23);
            this.bClear.TabIndex = 210;
            this.bClear.Text = "Clear";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // lMiddleHeight
            // 
            this.lMiddleHeight.AutoSize = true;
            this.lMiddleHeight.Location = new System.Drawing.Point(-1, 37);
            this.lMiddleHeight.Name = "lMiddleHeight";
            this.lMiddleHeight.Size = new System.Drawing.Size(74, 13);
            this.lMiddleHeight.TabIndex = 208;
            this.lMiddleHeight.Text = "Altitude, m......";
            // 
            // lXRect
            // 
            this.lXRect.AutoSize = true;
            this.lXRect.Location = new System.Drawing.Point(4, 20);
            this.lXRect.Name = "lXRect";
            this.lXRect.Size = new System.Drawing.Size(118, 13);
            this.lXRect.TabIndex = 3;
            this.lXRect.Text = "Lat,deg.........................";
            // 
            // tbXRect
            // 
            this.tbXRect.Location = new System.Drawing.Point(78, 13);
            this.tbXRect.MaxLength = 7;
            this.tbXRect.Name = "tbXRect";
            this.tbXRect.Size = new System.Drawing.Size(75, 20);
            this.tbXRect.TabIndex = 4;
            // 
            // lYRect
            // 
            this.lYRect.AutoSize = true;
            this.lYRect.Location = new System.Drawing.Point(4, 39);
            this.lYRect.Name = "lYRect";
            this.lYRect.Size = new System.Drawing.Size(142, 13);
            this.lYRect.TabIndex = 6;
            this.lYRect.Text = "Long,deg..............................";
            // 
            // tbYRect
            // 
            this.tbYRect.Location = new System.Drawing.Point(78, 36);
            this.tbYRect.MaxLength = 7;
            this.tbYRect.Name = "tbYRect";
            this.tbYRect.Size = new System.Drawing.Size(75, 20);
            this.tbYRect.TabIndex = 5;
            // 
            // gbRect
            // 
            this.gbRect.Controls.Add(this.tbYRect);
            this.gbRect.Controls.Add(this.lYRect);
            this.gbRect.Controls.Add(this.tbXRect);
            this.gbRect.Controls.Add(this.lXRect);
            this.gbRect.Location = new System.Drawing.Point(1, 60);
            this.gbRect.Name = "gbRect";
            this.gbRect.Size = new System.Drawing.Size(163, 62);
            this.gbRect.TabIndex = 17;
            this.gbRect.TabStop = false;
            this.gbRect.Visible = false;
            // 
            // tbOwnHeight
            // 
            this.tbOwnHeight.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbOwnHeight.Location = new System.Drawing.Point(86, 35);
            this.tbOwnHeight.Name = "tbOwnHeight";
            this.tbOwnHeight.Size = new System.Drawing.Size(64, 20);
            this.tbOwnHeight.TabIndex = 211;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Jammer station";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column1.Width = 50;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Lat,deg";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column2.Width = 65;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Long,deg";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column3.Width = 65;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Azimuth,deg";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column4.Width = 65;
            // 
            // FormAz1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(478, 152);
            this.Controls.Add(this.gbRect);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.chbXY);
            this.Controls.Add(this.tbOwnHeight);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.lMiddleHeight);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(800, 100);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormAz1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Azimuth";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.FormAz1_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormAz1_FormClosing);
            this.Load += new System.EventHandler(this.FormAz1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.gbRect.ResumeLayout(false);
            this.gbRect.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label15;
        public System.Windows.Forms.CheckBox chbXY;
        private System.Windows.Forms.Button bClear;
        private System.Windows.Forms.Label lMiddleHeight;
        private System.Windows.Forms.Label lXRect;
        public System.Windows.Forms.TextBox tbXRect;
        private System.Windows.Forms.Label lYRect;
        public System.Windows.Forms.TextBox tbYRect;
        public System.Windows.Forms.GroupBox gbRect;
        public System.Windows.Forms.TextBox tbOwnHeight;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
    }
}