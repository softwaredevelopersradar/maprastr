﻿using System;
//using AxaxGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.IO;

//0206*
using System.Windows.Input;
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;
using Bitmap = System.Drawing.Bitmap;

namespace GrozaMap
{
    public partial class FormOB2 : Form
    {
/*
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);
*/

        public FormOB2()
        {
            InitializeComponent();

        } // Конструктор
        // ***********************************************************  Конструктор

        // ************************************************************************
        // Загрузка формы
        // ************************************************************************
        private void FormOB2_Load(object sender, EventArgs e)
        {

            if (GlobalVarLn.flEndTRO_stat != 1)
            {
                // .....................................................................................
                // Очистка dataGridView
                // 10_10_2018

                // Очистка dataGridView1
                while (dataGridView1.Rows.Count != 0)
                    dataGridView1.Rows.Remove(dataGridView1.Rows[dataGridView1.Rows.Count - 1]);

                dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDatOB2_stat; i++)
                {
                    dataGridView1.Rows.Add("", "", "", "", ""); 
                }
                // -------------------------------------------------------------------

                // .....................................................................................
                // Флаги

                GlobalVarLn.blOB2_stat = true;
                GlobalVarLn.flEndOB2_stat = 1;
                // .....................................................................................
                GlobalVarLn.X_OB2 = 0;
                GlobalVarLn.Y_OB2 = 0;
                GlobalVarLn.H_OB2 = 0;
                GlobalVarLn.list_OB2.Clear();
                GlobalVarLn.list1_OB2.Clear();
                // .....................................................................................
                GlobalVarLn.iZOB2 = 0;

                //..GlobalVarLn.objFormOB2G.pbOB2.Image = imageList1.Images[0];
                GlobalVarLn.objFormOB2G.pbOB2.BackgroundImage = imageList1.Images[0];

            }

                UpdateDataGridView();


        } // form_load
        // ************************************************************************

        // ************************************************************************
        // Очистка OB2
        // ************************************************************************
        private void bClear_Click(object sender, EventArgs e)
        {
            // ----------------------------------------------------------------------
            GlobalVarLn.blOB2_stat = true;
            GlobalVarLn.flEndOB2_stat = 1;
            // ----------------------------------------------------------------------
            // переменные

            GlobalVarLn.X_OB2 = 0;
            GlobalVarLn.Y_OB2 = 0;
            GlobalVarLn.H_OB2 = 0;
            // -------------------------------------------------------------------
            GlobalVarLn.list_OB2.Clear();
            GlobalVarLn.list1_OB2.Clear();
            // -------------------------------------------------------------------
            // .....................................................................................
            // Очистка dataGridView1+ Установка 100 строк
            // 10_10_2018

            // Очистка dataGridView1
            while (dataGridView1.Rows.Count != 0)
                dataGridView1.Rows.Remove(dataGridView1.Rows[dataGridView1.Rows.Count - 1]);

            for (int i = 0; i < GlobalVarLn.sizeDatOB2_stat; i++)
            {
                dataGridView1.Rows.Add("", "", "", "", "");
            }
            // -------------------------------------------------------------------

            // -------------------------------------------------------------------
            // Убрать с карты

            //0209
            //GlobalVarLn.axMapScreenGlobal.Repaint();
            MapForm.REDRAW_MAP();
            // -------------------------------------------------------------------

        } // Clear

        // ************************************************************************

        // ************************************************************************
        // Обработчик кнопки : сохранить
        // ************************************************************************
        // 10_10_2018

        private void bAccept_Click(object sender, EventArgs e)
        {
            // OLD ***************************************************************
            // 10_10_2018
/*
            UpdateListOB2();
            GlobalVarLn.SaveListOB2();
            //axaxcMapScreen.Repaint();
            MapForm.REDRAW_MAP();
 */ 
            // *************************************************************** OLD

            // NEW ***************************************************************
            // 10_10_2018

            // ---------------------------------------------------------------------
            GlobalVarLn.blOB2_stat = true;
            GlobalVarLn.flEndOB2_stat = 1;
            GlobalVarLn.list1_OB2.Clear();
            // ---------------------------------------------------------------------
            int ir = 0;
            int irf = 0;

            LF1 objLF = new LF1();
            double lt = 0;
            double lng = 0;
            double freq = 0;
            String s = "";
            // ---------------------------------------------------------------------

            // FOR >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            for (ir = 0; ir < dataGridView1.Rows.Count; ir++)
            {
                // IF**
                if (
                    ((dataGridView1.Rows[ir].Cells[0].Value != null) && (dataGridView1.Rows[ir].Cells[0].Value != "")) &&
                    ((dataGridView1.Rows[ir].Cells[1].Value != null) && (dataGridView1.Rows[ir].Cells[1].Value != ""))
                   )
                {
                    // -----------------------------------------------------------------
                    //  Latitude из таблицы (WGS84)

                    // IF1
                    if ((dataGridView1.Rows[ir].Cells[0].Value != "") &&
                        (dataGridView1.Rows[ir].Cells[0].Value != null))
                    {
                        s = Convert.ToString(dataGridView1.Rows[ir].Cells[0].Value);

                        try
                        {
                            lt = Convert.ToDouble(s);
                        }
                        catch (SystemException)
                        {
                            try
                            {
                                if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                                lt = Convert.ToDouble(s);
                            }
                            catch
                            {
                                MessageBox.Show("Incorrect data");
                                return;
                            }

                        } // catch

                    } // IF1
                    // -----------------------------------------------------------------
                    //  Longitude из таблицы (WGS84)

                    // IF2
                    if ((dataGridView1.Rows[ir].Cells[1].Value != "") &&
                        (dataGridView1.Rows[ir].Cells[1].Value != null))
                    {
                        s = Convert.ToString(dataGridView1.Rows[ir].Cells[1].Value);

                        try
                        {
                            lng = Convert.ToDouble(s);
                        }
                        catch (SystemException)
                        {
                            try
                            {
                                if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                                lng = Convert.ToDouble(s);
                            }
                            catch
                            {
                                MessageBox.Show("Incorrect data");
                                return;
                            }

                        } // catch

                    } // IF2
                    // -----------------------------------------------------------------
                    // Имя

                    s = Convert.ToString(dataGridView1.Rows[ir].Cells[3].Value);

                    // -----------------------------------------------------------------
                    // Получить координаты на карте

                    // grad->rad
                    //lt = (lt * Math.PI) / 180;
                    //lng = (lng * Math.PI) / 180;
                    // Подаем rad, получаем там же расстояние на карте в м
                    //mapGeoToPlane(GlobalVarLn.hmapl, ref lt, ref lng);

                    var x = lt; // Lat
                    var y = lng; //Long
                    // преобразование В меркатор
                    var p = Mercator.FromLonLat(y, x);
                    double xx = p.X;
                    double yy = p.Y;

                    double hhh = 0;
                    try
                    {
                        hhh = (double)MapForm.RasterMapControl.Dted.GetElevation(lng, lt);
                    }
                    catch
                    {
                        hhh = 0;
                    }

                    GlobalVarLn.X_OB2 = xx;
                    GlobalVarLn.Y_OB2 = yy;
                    // -----------------------------------------------------------------
                    // Заполнение структуры (X,Y,Name,Znak)

                    objLF.X_m = GlobalVarLn.X_OB2;
                    objLF.Y_m = GlobalVarLn.Y_OB2;
                    objLF.sType = s;

                    objLF.indzn = GlobalVarLn.iZOB2;

                    objLF.Lat = lt;
                    objLF.Long = lng;
                    // -----------------------------------------------------------------
                    // H

                    //GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.X_OB2, GlobalVarLn.Y_OB2);
                    //GlobalVarLn.H_OB2 = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                    GlobalVarLn.H_OB2 = hhh;

                    objLF.H_m = GlobalVarLn.H_OB2;

                    dataGridView1.Rows[ir].Cells[2].Value = GlobalVarLn.H_OB2;
                    // -----------------------------------------------------------------
                    // F из таблицы

                    // IF3
                    if ((dataGridView1.Rows[ir].Cells[4].Value != "") &&
                        (dataGridView1.Rows[ir].Cells[4].Value != null))
                    {
                        s = Convert.ToString(dataGridView1.Rows[ir].Cells[4].Value);

                        try
                        {
                            freq = Convert.ToDouble(s);
                        }
                        catch (SystemException)
                        {
                            try
                            {
                                if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                                freq = Convert.ToDouble(s);
                            }
                            catch
                            {
                                MessageBox.Show("Incorrect data");
                                return;
                            }

                        } // catch

                    } // IF3

                    objLF.FrequencyMhz = freq;
                    // -----------------------------------------------------------------
                    // Добавить строку

                    GlobalVarLn.list1_OB2.Add(objLF);
                    // -----------------------------------------------------------------

                    irf += 1;

                } // IF**

            } // FOR
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> FOR

            // -----------------------------------------------------------------
            if (irf != 0)
            {
                GlobalVarLn.blOB2_stat = true;
                GlobalVarLn.flEndOB2_stat = 1;
            }
            // -----------------------------------------------------------------
            //UpdateListOB2();
            GlobalVarLn.SaveListOB2();
            //axaxcMapScreen.Repaint();
            MapForm.REDRAW_MAP();

            // -----------------------------------------------------------------

            // *************************************************************** NEW



//""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

            //0209 NO USE
/*
            int i_tmp = 0;
            // -----------------------------------------------------------------------------------------
            String strFileName;
            strFileName = "OB2.txt";
            StreamWriter srFile;

            //StreamWriter srFile = new StreamWriter(strFileName);
            try
            {
                srFile = new StreamWriter(strFileName);
            }
            catch
            {
                MessageBox.Show("Can’t save file");
                return;
            }


            // -----------------------------------------------------------------------------------------
            srFile.WriteLine("N =" + Convert.ToString(GlobalVarLn.iOB2_stat));

            for (i_tmp = 0; i_tmp < GlobalVarLn.iOB2_stat; i_tmp++)
            {

                srFile.WriteLine("X =" + Convert.ToString((int)GlobalVarLn.list1_OB2[i_tmp].X_m));
                srFile.WriteLine("Y =" + Convert.ToString((int)GlobalVarLn.list1_OB2[i_tmp].Y_m));
                srFile.WriteLine("H =" + Convert.ToString((int)GlobalVarLn.list1_OB2[i_tmp].H_m));
                srFile.WriteLine("Type =" + Convert.ToString(GlobalVarLn.list1_OB2[i_tmp].sType));
                srFile.WriteLine("indzn =" + Convert.ToString(GlobalVarLn.list1_OB2[i_tmp].indzn));

            }
            // -------------------------------------------------------------------------------------

            srFile.Close();
            // ------------------------------------------------------------------------------------

*/

//""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

        } // Save in file
        // ************************************************************************

        // ************************************************************************
        // Update List
        // ************************************************************************

        private void UpdateListOB2()
        {

            //0209
            double xx = 0;
            double yy = 0;

            for (var i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                String s1 = "";

                var ob = GlobalVarLn.list1_OB2[i];
                var row = dataGridView1.Rows[i];

                var x = Convert.ToDouble((string) row.Cells[0].Value); // Lat
                var y = Convert.ToDouble((string) row.Cells[1].Value); //Long

                //0209
                //axaxcMapScreen.MapRealToPlaneGeo(ref x, ref y);
                // преобразование В меркатор
                var p = Mercator.FromLonLat(y, x);
                xx = p.X;
                yy = p.Y;

                var h = (double) row.Cells[2].Value;
                var name = (string) row.Cells[3].Value;

               // 1209
                s1 = Convert.ToString(row.Cells[4].Value);
                double frequency = 0;

/*
                try
                {

                    if (s1.IndexOf(",") > -1)
                        s1 = s1.Replace(',', '.');
                    frequency = Convert.ToDouble(s1);

                }
                catch (SystemException)

                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    else s1 = s1.Replace('.', ',');
                    frequency = Convert.ToDouble(s1);
                }
*/
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    frequency = Convert.ToDouble(s1);
                }
                catch (SystemException)
                {
                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        frequency = Convert.ToDouble(s1);
                    }
                    catch
                    {
                        MessageBox.Show("Incorrect data");
                        return;
                    }
                }

                var iconIndex = ob.indzn;

                GlobalVarLn.list1_OB2[i] = new LF1
                {
                    FrequencyMhz = frequency,
                    indzn = iconIndex,
                    sType = name,

                    //0209
                    X_m = xx,
                    Y_m = yy,

                    //0209
                    Lat=x,
                    Long=y,

                    H_m = h
                };
            }

        } // Update List
        // ************************************************************************

        // Обработчик кнопки : read from file
        // ************************************************************************
        private void button1_Click(object sender, EventArgs e)
        {
            GlobalVarLn.LoadListOB2();
            UpdateDataGridView();

            //0209
            GlobalVarLn.flEndOB2_stat = 1;
            //axaxcMapScreen.Repaint();
            MapForm.REDRAW_MAP();
        }

        // ************************************************************************
        // Удалить объект
        // ************************************************************************
        //0209

        private void button3_Click(object sender, EventArgs e)
        {
            // -----------------------------------------------------------------------------------------
            String strLine2 = "";
            String strLine3 = "";

            int it = 0;
            int index = 0;
            // -----------------------------------------------------------------------------------------
            // Если открыта таблица

            if (GlobalVarLn.iOB2_stat != 0)
            {
                index = dataGridView1.CurrentRow.Index;
                if (index >= GlobalVarLn.list1_OB2.Count)
                {
                    return;
                }

                // Убрать с таблицы
                dataGridView1.Rows.Remove(dataGridView1.Rows[index]);
                // Del from list
                GlobalVarLn.list1_OB2.Remove(GlobalVarLn.list1_OB2[index]);

                //0209
                // Убрать с карты
                //GlobalVarLn.axMapScreenGlobal.Repaint();

            } // IF(GlobalVarLn.iSP_stat != 0)
            // -------------------------------------------------------------------------------------

            //0209
            //f_OB2ReDraw();
            MapForm.REDRAW_MAP();

        } // Delete
        // ************************************************************************

        // ФУНКЦИИ ********************************************************************************

        // ****************************************************************************************
        // Обработка нажатия левой кнопки мыши при отрисовке OB1
        //
        // Входные параметры:
        // X - X, m на местности
        // Y - Y, m
        // ****************************************************************************************
        public void f_OB2(
                          double X,
                          double Y
                         )
        {

/*
            GlobalVarLn.X_OB2 = GlobalVarLn.MapX1;
            GlobalVarLn.Y_OB2 = GlobalVarLn.MapY1;

            var objLF = new LF1
            {
                X_m = GlobalVarLn.X_OB2,
                Y_m = GlobalVarLn.Y_OB2,
                indzn = GlobalVarLn.iZOB2,
                sType = "enemy"
            };



            GlobalVarLn.blOB2_stat = true;
            GlobalVarLn.flEndOB2_stat = 1;
            GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.MapX1, GlobalVarLn.MapY1);
            GlobalVarLn.H_OB2 = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            objLF.H_m = GlobalVarLn.H_OB2;
            // Добавить строку

            GlobalVarLn.list1_OB2.Add(objLF);
            UpdateDataGridView();

            GlobalVarLn.axMapScreenGlobal.Repaint();

            f_OB2ReDraw();
            // ---------------------------------------------------------------------------------
*/

            //0209
            // ......................................................................

            LF1 objLF = new LF1();
            // ......................................................................
            // !!! Merkator в м 

            GlobalVarLn.X_OB2 = GlobalVarLn.X_Rastr;
            GlobalVarLn.Y_OB2 = GlobalVarLn.Y_Rastr;
            objLF.X_m = GlobalVarLn.X_OB2;
            objLF.Y_m = GlobalVarLn.Y_OB2;

            GlobalVarLn.H_OB2 = GlobalVarLn.H_Rastr;
            objLF.H_m = GlobalVarLn.H_OB2;

            objLF.Lat = GlobalVarLn.LAT_Rastr;   //grad WGS84
            objLF.Long = GlobalVarLn.LONG_Rastr; //grad WGS84

            objLF.indzn = GlobalVarLn.iZOB2;
            objLF.sType = "enemy";
            // ......................................................................
            // ......................................................................
            GlobalVarLn.blOB2_stat = true;
            GlobalVarLn.flEndOB2_stat = 1;
            // ......................................................................
            //GlobalVarLn.iOB2_stat += 1;

            // Добавить строку

            GlobalVarLn.list1_OB2.Add(objLF);
            UpdateDataGridView();
            // ---------------------------------------------------------------------------------
            // Перерисовать

            MapForm.REDRAW_MAP();
            // ---------------------------------------------------------------------------------

        } // f_OB2

        // ***********************************************************************************
        // UpdateDataGridView
        // ***********************************************************************************

        private void UpdateDataGridView()
        {
             // 10_10_2018
/*
            // updating rows count
            if (dataGridView1.Rows.Count < GlobalVarLn.list1_OB2.Count)
            {
                dataGridView1.Rows.Add(GlobalVarLn.list1_OB2.Count - dataGridView1.Rows.Count);
            }
            while (dataGridView1.Rows.Count > GlobalVarLn.list1_OB2.Count)
            {
                dataGridView1.Rows.RemoveAt(dataGridView1.Rows.Count - 1);
            }
*/

            // .....................................................................................
            // Очистка dataGridView1+ Установка 100 строк
            // 10_10_2018

            // Очистка dataGridView1
            while (dataGridView1.Rows.Count != 0)
                dataGridView1.Rows.Remove(dataGridView1.Rows[dataGridView1.Rows.Count - 1]);

            for (int i = 0; i < GlobalVarLn.sizeDatOB2_stat; i++)
            {
                dataGridView1.Rows.Add("", "", "", "", "");
            }
            // .....................................................................................


            for (var i = 0; i < GlobalVarLn.list1_OB2.Count; ++i)
            {
                var ob = GlobalVarLn.list1_OB2[i];
                var row = dataGridView1.Rows[i];

                //var p = axaxcMapScreen.MapPlaneToRealGeo(ob.X_m, ob.Y_m);

                row.Cells[0].Value = ob.Lat.ToString("F3");
                row.Cells[1].Value = ob.Long.ToString("F3");
                row.Cells[2].Value = ob.H_m;
                row.Cells[3].Value = ob.sType;
                row.Cells[4].Value = ob.FrequencyMhz.ToString("F3");
            }

        } // UpdateDataGridView
        // ***********************************************************************************

        // *************************************************************************************
        // Перерисовка OB2
        // *************************************************************************************
        //0209 !!!

        public void f_OB2ReDraw()
        {
            //ClassMap.f_OB2_stat();

            String s1 = "";
            int i = 0;

            IMapObject objectGrozaS1;
            MapObjectStyle _placeObjectStyleOwn;
            Mapsui.Geometries.Point pointOwn = new Mapsui.Geometries.Point();

            // -----------------------------------------------------------------
            for (i = 0; i < GlobalVarLn.list1_OB2.Count; i++)
            {
                try
                {
                    s1 = GlobalVarLn.list1_OB2[i].sType;
                    pointOwn.X = GlobalVarLn.list1_OB2[i].Long;
                    pointOwn.Y = GlobalVarLn.list1_OB2[i].Lat;

                    _placeObjectStyleOwn = MapForm.RasterMapControl.LoadObjectStyle(
                        (Bitmap)GlobalVarLn.objFormOB2G.imageList1.Images[GlobalVarLn.list1_OB2[i].indzn],
                        //scale: 0.8,
                        scale: 0.6,
                        objectOffset: new Offset(0, 0),
                        textOffset: new Offset(0, 15)
                        );
                    objectGrozaS1 = MapForm.RasterMapControl.AddMapObject(_placeObjectStyleOwn, s1, pointOwn);

                }
                catch (Exception ex)
                {
                    // ignored
                }

            } // FOR
            // -----------------------------------------------------------------


        } // P/P f_OB2ReDraw
        // *************************************************************************************

        // ******************************************************************************** ФУНКЦИИ


        // ****************************************************************************************
        // Закрыть форму
        // ****************************************************************************************
        private void FormOB2_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

            // приостановить обработку, если идет
            GlobalVarLn.blOB2_stat = false;

            //GlobalVarLn.fFOB2 = 0;
            GlobalVarLn.fl_Open_objFormOB2 = 0;


        } // Closing
        // ****************************************************************************************
        // Активизировать форму
        // ****************************************************************************************

        private void FormOB2_Activated(object sender, EventArgs e)
        {
            //GlobalVarLn.objFormOB2G.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFOB2 = 1;

            GlobalVarLn.blOB2_stat = true;
            GlobalVarLn.flEndOB2_stat = 1;

            // NNNN
            UpdateDataGridView();

            // 0809_3
            //ClassMap.f_RemoveFrm(3);
            GlobalVarLn.fl_Open_objFormOB2 = 1;


        } // Activated

        // ****************************************************************************************
        // Значок
        // ****************************************************************************************
        private void buttonZOB1_Click(object sender, EventArgs e)
        {
            GlobalVarLn.iZOB2 += 1;
            if (GlobalVarLn.iZOB2 == imageList1.Images.Count)
                GlobalVarLn.iZOB2 = 0;
            //pbOB2.Image = imageList1.Images[GlobalVarLn.iZOB2];
            pbOB2.BackgroundImage = imageList1.Images[GlobalVarLn.iZOB2];


        }

        private void button2_Click(object sender, EventArgs e)
        {
            GlobalVarLn.iZOB2 -= 1;
            if (GlobalVarLn.iZOB2 < 0)
                GlobalVarLn.iZOB2 = imageList1.Images.Count - 1;
            pbOB2.BackgroundImage = imageList1.Images[GlobalVarLn.iZOB2];

        } // Значок


    } // Class
} // Namespace
