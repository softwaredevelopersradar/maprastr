﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrozaMap
{
    class iniRW
    {
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileSection(string section, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);


        public static void write_map_inf(int mapLeft, int mapTop, int scale, string path)
        {
            WritePrivateProfileString("Position", "MapLeft", mapLeft.ToString(), Application.StartupPath + "\\Setting.ini");
            WritePrivateProfileString("Position", "MapTop", mapTop.ToString(), Application.StartupPath + "\\Setting.ini");
            WritePrivateProfileString("Position", "Scale", scale.ToString(), Application.StartupPath + "\\Setting.ini");
            WritePrivateProfileString("Map", "Path", path, Application.StartupPath + "\\Setting.ini");
        }

        public static void write_map_inf(int mapLeft, int mapTop, int scale)
        {
            WritePrivateProfileString("Position", "MapLeft", mapLeft.ToString(), Application.StartupPath + "\\Setting.ini");
            WritePrivateProfileString("Position", "MapTop", mapTop.ToString(), Application.StartupPath + "\\Setting.ini");
            WritePrivateProfileString("Position", "Scale", scale.ToString(), Application.StartupPath + "\\Setting.ini");
        }


        public static string get_IPaddress()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Connect", "IPaddress", "0", temp, 255, Application.StartupPath + "\\Setting.ini");
            return temp.ToString();
        }

        public static int get_Port()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Connect", "Port", "0", temp, 255, Application.StartupPath + "\\Setting.ini");
            return int.Parse(temp.ToString());
        }
        public static int get_AdressOwn()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Common", "AdressOwn", "0", temp, 255, Application.StartupPath + "\\Setting.ini");
            return int.Parse(temp.ToString());
        }
        public static int get_AdressLinked()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Common", "AdressLinked", "0", temp, 255, Application.StartupPath + "\\Setting.ini");
            return int.Parse(temp.ToString());
        }


        public static string get_map_path()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Map", "Path", "", temp, 255, Application.StartupPath + "\\Setting.ini");
            return temp.ToString();
        }
        public static string get_matrix_path()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Map", "PathMatrix", "", temp, 255, Application.StartupPath + "\\Setting.ini");
            return temp.ToString();
        }
        //0219
        public static void set_map_path(string path)
        {
            WritePrivateProfileString("Map", "Path", path, Application.StartupPath + "\\Setting.ini");
        }
        public static void set_matrix_path(string path)
        {
            WritePrivateProfileString("Map", "PathMatrix", path, Application.StartupPath + "\\Setting.ini");
        }



        //0209
        public static void write_map_scl(int scale)
        {
            WritePrivateProfileString("Position", "Scale", scale.ToString(), Application.StartupPath + "\\Setting.ini");
        }
        public static int get_scl()
        {
            StringBuilder temp = new StringBuilder(255);
            GetPrivateProfileString("Position", "Scale", "0", temp, 255, Application.StartupPath + "\\Setting.ini");
            return int.Parse(temp.ToString());
        }






    }
}
