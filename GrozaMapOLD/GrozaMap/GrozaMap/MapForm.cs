﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
using System.Windows.Input;

//0206*
using System.IO;
using System.Windows.Input;
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;


//using USR_DLL;
using PC_DLL;
using System.Reflection;
//using Contract;

using ClassLibraryPeleng;

namespace GrozaMap
{


    public partial class MapForm : Form
    {

        //0206*
        public static RasterMapControl RasterMapControl { get; private set; }


        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();


        // sob
        public delegate void UpdateTableReconFWSEventHandler(TReconFWS[] tReconFWS);
        public event UpdateTableReconFWSEventHandler UpdateTableReconFWS;

        private bool MapIsOpenned
        {
            get { return RasterMapControl.IsMapLoaded; }
        }
        private bool HeightMatrixIsOpenned = false;

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        // Lena

        //Form1 form1;
        //Form2 ObjCommPowerAvail;
       // FormAirPlane ObjFormAirPlane;
        //FormPeleng objFormPeleng;
        //FormS objFormS;
        public FormWay objFormWay;
        //public ZonePowerAvail objZonePowerAvail;
        public FormSP objFormSP;
        public FormLF objFormLF;
        public FormLF2 objFormLF2;
        public FormZO objFormZO;
        public FormTRO objFormTRO;
        public FormOB1 objFormOB1;
        public FormOB2 objFormOB2;
        public FormSuppression objFormSuppression;
        public FormSPFB objFormSPFB;
        public FormSPFB1 objFormSPFB1;
        public FormSPFB2 objFormSPFB2;
        public FormAz1 objFormAz1;
        //public FormAz objFormAz;
        public FormLineSightRange objFormLineSightRange;
        public FormSupSpuf objFormSupSpuf;
        public FormTab objFormTab;
        // seg1
        public FormSpuf objFormSpuf;
        public FormScreen objFormScreen;

        public static int hmapl1;


        public static bool blAirObject = false;
        
        string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        
        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        int iCounter = 0;

        // create client
        public ClientPC clientPC;// = new ClientPC(1);
        

        // КОНСТРУКТОР *********************************************************************
        public MapForm()
        {
            InitializeComponent();

            //0206*
            RasterMapControl = new RasterMapControl();
            MapElementHost.Child = RasterMapControl;
            RasterMapControl.MouseEnter += RasterMapControl_MouseEnter;
            RasterMapControl.MapMouseMoveEvent += OnMapMapMouseMove;
            RasterMapControl.MapClickEvent += OnMapClick;

            objFormWay = new FormWay();
            objFormSP = new FormSP();
            objFormLF = new FormLF();
            objFormLF2 = new FormLF2();
            objFormZO = new FormZO();
            objFormTRO = new FormTRO();
            objFormOB1 = new FormOB1();
            objFormOB2 = new FormOB2();
            objFormSuppression = new FormSuppression();
            objFormSPFB = new FormSPFB();
            objFormSPFB1 = new FormSPFB1();
            objFormSPFB2 = new FormSPFB2(this);
            objFormAz1 = new FormAz1();
            objFormLineSightRange = new FormLineSightRange();
            objFormSupSpuf = new FormSupSpuf();
            objFormTab = new FormTab(this);
            // seg1
            objFormSpuf = new FormSpuf();
            objFormScreen = new FormScreen(this);


            if (System.IO.File.Exists(Application.StartupPath + "\\Setting.ini"))
            {
                string sIPAddress = iniRW.get_IPaddress();
                tbIP.Text = sIPAddress;

                int iPort = iniRW.get_Port();
                tbPort.Text = iPort.ToString();

                GlobalVarLn.AdressOwn = iniRW.get_AdressOwn();
                GlobalVarLn.AdressLinked = iniRW.get_AdressLinked();

            }
            else
            {
                MessageBox.Show("Can’t open file INI! \n", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


            // MAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAP
            //0206*

            string pathMap = "";
            string pathMatrix = "";


            if (System.IO.File.Exists(Application.StartupPath + "\\Setting.ini"))
            {
                //0219
                //pathMap = Application.StartupPath + iniRW.get_map_path();
                //pathMatrix = Application.StartupPath + iniRW.get_matrix_path();
                pathMap = iniRW.get_map_path();
                pathMatrix =iniRW.get_matrix_path();

                try
                {
                    RasterMapControl.OpenMap(pathMap);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format("Can't load map! {0}", ex.Message));
                }

                try
                {
                    RasterMapControl.OpenDted(pathMatrix);
                    HeightMatrixIsOpenned = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(string.Format("Can't load the height matrix! {0}", ex.Message));
                    HeightMatrixIsOpenned = false;
                }

            }
            else
            {
                MessageBox.Show("Can’t open file INI! \n", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


            RasterMapControl.Resolution = (double)iniRW.get_scl();
            // MAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAPMAP


            //0206
            //MouseWheel += MapForm_MouseWheel;


            // .................................................................................................
/*
            /// WCF
            //создаем сервис
            var service = new ServiceImplementation();
            //подписываемся на событие HelloReceived
            service.AirPlaneReceived += Service_AirPlaneReceived;
            //стартуем сервер
            var svh = new ServiceHost(service);
            svh.AddServiceEndpoint(typeof(Contract.IService), new NetTcpBinding(), "net.tcp://localhost:8000");
            svh.Open();
 */ 
            // .................................................................................................


            //axaxMapSelDlg.cMapView = axaxcMapScreen1.C_CONTAINER;


        }

        // *******************
        //0206* ?????
        void RasterMapControl_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            RasterMapControl.InnerMapControl.Focus();
        }
        // ********************

        // ********************************************************************* КОНСТРУКТОР



// ***************************************************************************************************************
// sob
// GPS
// Обработчик события

        void clientPC_OnConfirmCoord(object sender, byte bAddress, byte bCodeError, double dLatitudeOwn, double dLongitudeOwn, double dLatitudeLinked, double dLongitudeLinked)
        {

            GlobalVarLn.lt1sp = dLatitudeOwn;
            GlobalVarLn.ln1sp = dLongitudeOwn;
            GlobalVarLn.lt2sp = dLatitudeLinked;
            GlobalVarLn.ln2sp = dLongitudeLinked;

            // Otladka

/*
            double x1 = 2382464;
            double y1 = 5498821;
            mapPlaneToGeo(GlobalVarLn.hmapl, ref x1, ref y1);
            GlobalVarLn.lt1sp = x1 * 180 / Math.PI; // rad->grad
            GlobalVarLn.ln1sp = y1 * 180 / Math.PI;

            double x3 = 2240316;
            double y4 = 5551638;
            mapPlaneToGeo(GlobalVarLn.hmapl, ref x3, ref y4);
            GlobalVarLn.lt2sp = x3 * 180 / Math.PI; // rad->grad
            GlobalVarLn.ln2sp = y4 * 180 / Math.PI;
 */

            GlobalVarLn.flagGPS_SP = 1;
            GlobalVarLn.objFormSPG.BeginInvoke(
                (MethodInvoker) (() => GlobalVarLn.objFormSPG.AcceptGPS()));
        }
// ***************************************************************************************************************

// ****************************************************************************************************
// Обновление листа СП
// ****************************************************************************************************

        public static void UpdateDropDownList(ComboBox dropDownList)
        {
            var stations = GlobalVarLn.listJS;

            var selectedIndex = dropDownList.SelectedIndex;

            dropDownList.Items.Clear();
            dropDownList.Items.Add("X,Y");
            foreach (var station in stations)
            {
                dropDownList.Items.Add(station.Name);
            }
            dropDownList.SelectedIndex = dropDownList.Items.Count > selectedIndex ? selectedIndex : 0;
        }

        public static void UpdateDropDownList1(ComboBox dropDownList)
        {
            var stations = GlobalVarLn.listJS;

            var selectedIndex = dropDownList.SelectedIndex;

            dropDownList.Items.Clear();
            //dropDownList.Items.Add("X,Y");
            foreach (var station in stations)
            {
                dropDownList.Items.Add(station.Name);
            }
            dropDownList.SelectedIndex = dropDownList.Items.Count > selectedIndex ? selectedIndex : 0;
        }
// ****************************************************************************************************


        //Обработка колёсика

        //0206
/*
        void MapForm_MouseWheel(object sender, MouseEventArgs e)
        {
            if (MapIsOpenned == true)
            {

                Rectangle rp = axaxcMapScreen1.Bounds;
                Rectangle rp2 = panel2.Bounds;

                if ((e.X >= rp.Left && e.X <= rp.Right && e.Y >= rp.Top && e.Y <= rp.Bottom) && (e.X >= rp2.Left && e.X <= rp2.Right && e.Y >= rp2.Top && e.Y <= rp2.Bottom))
                {
                    if (e.Delta != 0)
                    {
                        if (e.Delta > 0)
                        {
                            MapCore.ZoomInFunc(ref axaxcMapScreen1);
                        }
                        if (e.Delta < 0)
                        {
                            MapCore.ZoomOutFunc(ref axaxcMapScreen1);
                        }

                    }
                }
            }
        }
*/


        // OpenMap **********************************************************************************************        
        //открыть карту

        //0206*
                private async void openMapToolStripMenuItem1_Click(object sender, EventArgs e)
                {
                    String path = "";

                    // -----------------------------------------------------------------
                    if (MapIsOpenned == false)
                    {

                      DialogResult result;
                      result=openFileDialog1.ShowDialog();

                        if (result == DialogResult.OK)
                        {
                            try
                            {
                                await RasterMapControl.OpenMap(openFileDialog1.FileName);

                                //0219
                                // Сохранить путь
                                path = openFileDialog1.FileName;
                                iniRW.set_map_path(path);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(string.Format("Can't load map! {0}", ex.Message));
                            }
                        } //Ok

                       else if (result == DialogResult.Cancel)
                       {
                         return;

                        } // Dialog==NO

                        else
                        {
                            MessageBox.Show("Can't load map!");
                        }

                    } // MapIsOpenned == false
                    // -----------------------------------------------------------------

                    else 
                        MessageBox.Show("The map is already open");
                    // -----------------------------------------------------------------


             } //P/P
        // ********************************************************************************************** OpenMap

        // CloseMap ****************************************************************************************
        //закрыть карту
        //0206

        private void closeMapToolStripMenuItem1_Click(object sender, EventArgs e)
        {

        //0206*
            if (MapIsOpenned == true)
            {
                RasterMapControl.CloseMap();
            }
            if (HeightMatrixIsOpenned == true)
            {
                RasterMapControl.CloseDted();
                HeightMatrixIsOpenned = false;
            }

            iniRW.write_map_scl((int)RasterMapControl.Resolution);


        } // CloseMap
        // **************************************************************************************** CloseMap

        // Open matrixH **********************************************************************************
        //открыть матрицу высот

        private void openTheHeightMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {

            String path;

            //0206*
            // -----------------------------------------------------------------
            if (HeightMatrixIsOpenned == false)
            {
                DialogResult result;
                result=openFileDialog1.ShowDialog();

                if (result == DialogResult.OK)
                {
                    try
                    {
                        RasterMapControl.OpenDted(openFileDialog1.FileName);
                        HeightMatrixIsOpenned = true;

                        //0219
                        // Сохранить путь
                        path = openFileDialog1.FileName;
                        iniRW.set_matrix_path(path);

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(string.Format("Can't load the height matrix! {0}", ex.Message));
                        HeightMatrixIsOpenned = false;
                    }

                } // Dialog==OK

                else if (result == DialogResult.Cancel)
                {
                    HeightMatrixIsOpenned = false;
                    return;

                } // Dialog==NO

                else
                {
                    MessageBox.Show("Can't load the height matrix!");
                    HeightMatrixIsOpenned = false;
                }

            } // HeightMatrixIsOpenned == false
            // -----------------------------------------------------------------

            else
                MessageBox.Show("The height matrix is already open");
            // -----------------------------------------------------------------

        } // P/P
        // ********************************************************************************** Open matrixH


        //0206 No use
/*
        //закрыть матрицу высот
        private void closeTheHeightMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MapCore.closeMatrixFunc(ref HeightMatrixIsOpenned, ref axaxcMapScreen1);
        }
*/


       // Масштаб *****************************************************************************
       //0209

        //увеличить масштаб
        private void ZoomIn_Click(object sender, EventArgs e)
        {
            RasterMapControl.Resolution = RasterMapControl.Resolution - 10000;

            if (RasterMapControl.Resolution <= RasterMapControl.MinResolution)
                RasterMapControl.Resolution = RasterMapControl.MinResolution;
            // 16_10_2018
            else if (RasterMapControl.Resolution == RasterMapControl.MaxResolution)
                RasterMapControl.Resolution = 73942;

        }

        //уменьшить масштаб
        private void ZoomOut_Click(object sender, EventArgs e)
        {
            RasterMapControl.Resolution = RasterMapControl.Resolution+10000;

            if (RasterMapControl.Resolution >= RasterMapControl.MaxResolution)
                RasterMapControl.Resolution = RasterMapControl.MaxResolution;
            // 16_10_2018
            else if (RasterMapControl.Resolution == RasterMapControl.MinResolution)
                RasterMapControl.Resolution = 13942;

        }

        //исходный масштаб
        private void ZoomInitial_Click(object sender, EventArgs e)
        {


            RasterMapControl.Resolution = 43942;

        }

        // ***************************************************************************** Масштаб

        //Форма по кнопке 1
        //public CheckPointForm checkPointForm;
        //private void button1_Click(object sender, EventArgs e)
        //{
/*
            if (checkPointForm == null || checkPointForm.IsDisposed)
            {
                checkPointForm = new CheckPointForm(this, axaxcMapScreen1);
                checkPointForm.Show();

            }
 */
        //}

        //Клик правой кнопкой мыши
        private double righteX;
        private double righteY;

        //Клик левой кнопкой мыши и премещение
        private bool waspressleft = false;
        private double startlefteX;
        private double startlefteY;
        private double movelefteX;
        private double movelefteY;

// MOUSE_DOWN *************************************************************************************************
//Обработка мыши на карте

        //0206*
        private void OnMapClick(object sender, MapMouseClickEventArgs e)
        {
/*
            // преобразование В меркатор
            var p = Mercator.FromLonLat(e.Location.Longitude, e.Location.Latitude);

            // преобразование из меркатора в wgs84
            var p2 = Mercator.ToLonLat(p.X, p.Y);

            if (RasterMapControl.LastClickedObject != null)
            {
                // if user clicked on a object let's remove it
                RasterMapControl.RemoveObject(RasterMapControl.LastClickedObject);
                return;
            }
            if (RasterMapControl.HoveredObject != null)
            {
                return;
            }
            if (e.ClickedButton.ChangedButton == MouseButton.Left)
            {
                ApplyInstrument(e.Location.ToPoint());
            }
*/


            // ---------------------------------------------------------------------------------
            // NEW


            if (e.Location.Latitude!=null)
             GlobalVarLn.LAT_Rastr = e.Location.Latitude;

            if (e.Location.Longitude != null)
                GlobalVarLn.LONG_Rastr = e.Location.Longitude;

            if (e.Location.Altitude != null)
                GlobalVarLn.H_Rastr =(double)e.Location.Altitude;

            if ((e.Location.Latitude != null) && (e.Location.Longitude != null))
            {
                // преобразование В меркатор
                var p = Mercator.FromLonLat(e.Location.Longitude, e.Location.Latitude);
                GlobalVarLn.X_Rastr = p.X;
                GlobalVarLn.Y_Rastr = p.Y;
            }
            // ---------------------------------------------------------------------------------
            // OLD

            GlobalVarLn.MousePress = true;
            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
            // Для маршрута

            if (GlobalVarLn.fl_Open_objFormWay == 1)
            {
                objFormWay.f_Way(
                               GlobalVarLn.X_Rastr,
                               GlobalVarLn.Y_Rastr
                              );
            }
            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS


            // -----------------------------------------------------------------------------------
            // SP

            if (GlobalVarLn.fl_Open_objFormSP == 1)
            {

                GlobalVarLn.fclSP = 0;

                objFormSP.f_SP(
                               GlobalVarLn.X_Rastr,
                               GlobalVarLn.Y_Rastr
                              );
            }

            // -----------------------------------------------------------------------------------
            // LF1
            if (GlobalVarLn.fl_Open_objFormLF == 1)
            {
                objFormLF.f_LF1(
                               GlobalVarLn.X_Rastr,
                               GlobalVarLn.Y_Rastr
                              );
            }
            // -----------------------------------------------------------------------------------
            // LF2
            if (GlobalVarLn.fl_Open_objFormLF2 == 1)
            {
                objFormLF2.f_LF2(
                               GlobalVarLn.X_Rastr,
                               GlobalVarLn.Y_Rastr
                              );
            }
            // -----------------------------------------------------------------------------------
            // ZO
            if (GlobalVarLn.fl_Open_objFormZO == 1)
            {
                objFormZO.f_ZO(
                               GlobalVarLn.X_Rastr,
                               GlobalVarLn.Y_Rastr
                              );
            }
            // ---------------------------------------------------------------------------------
            // OB1
            if (GlobalVarLn.fl_Open_objFormOB1 == 1)
            {
                objFormOB1.f_OB1(
                               GlobalVarLn.X_Rastr,
                               GlobalVarLn.Y_Rastr
                              );
            }
            // ---------------------------------------------------------------------------------
            // OB2
            if (GlobalVarLn.fl_Open_objFormOB2 == 1)
            {
                objFormOB2.f_OB2(
                               GlobalVarLn.X_Rastr,
                               GlobalVarLn.Y_Rastr
                              );
            }
            // ---------------------------------------------------------------------------------
            // Az1(Object)

            if (GlobalVarLn.blOB_az1 == true)
            {

                GlobalVarLn.XXXaz = GlobalVarLn.X_Rastr;
                GlobalVarLn.YYYaz = GlobalVarLn.Y_Rastr;

            }
            // ---------------------------------------------------------------------------------
            // SPFBRR
            //0219

            if (GlobalVarLn.fl_Open_objFormSPFB == 1)
            {
                objFormSPFB.f_SPFB_f1();
            }
            // ---------------------------------------------------------------------------------
            // SPFBPR
            //0219

            if (GlobalVarLn.fl_Open_objFormSPFB1 == 1)
            {
                objFormSPFB1.f_SPFB_f2();
            }
            // ---------------------------------------------------------------------------------



            // ---------------------------------------------------------------------------------


        } // P/P


/*
        //0206
        private void axaxcMapScreen1_OnMapMouseDown(object sender, AxaxGisToolKit.IaxMapScreenEvents_OnMapMouseDownEvent e)
        {


            // 0510
            GlobalVarLn.MousePress = true;
            

            // Lena *******************************************************************************

            // Сняли координаты -> GlobalVarLn.MapX1,...(m на местности)
            ClassMap.f_XYMap(e.x, e.y);

            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
            // Для маршрута

            if (GlobalVarLn.fl_Open_objFormWay == 1)

            {
                objFormWay.f_Way(
                               GlobalVarLn.MapX1,
                               GlobalVarLn.MapY1
                              );
            }
            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS

            // -----------------------------------------------------------------------------------
            // SP

                if (GlobalVarLn.fl_Open_objFormSP == 1)
                {

                    GlobalVarLn.fclSP = 0;

                    objFormSP.f_SP(
                                   GlobalVarLn.MapX1,
                                   GlobalVarLn.MapY1
                                  );
                }

                // -----------------------------------------------------------------------------------
                // LF1
                if (GlobalVarLn.fl_Open_objFormLF == 1)

                {
                    objFormLF.f_LF1(
                                   GlobalVarLn.MapX1,
                                   GlobalVarLn.MapY1
                                  );
                }
                // -----------------------------------------------------------------------------------
                // LF2
                if (GlobalVarLn.fl_Open_objFormLF2 == 1)
                {
                    objFormLF2.f_LF2(
                                   GlobalVarLn.MapX1,
                                   GlobalVarLn.MapY1
                                  );
                }
                // -----------------------------------------------------------------------------------
                // ZO
                if (GlobalVarLn.fl_Open_objFormZO == 1)

                {
                    objFormZO.f_ZO(
                                   GlobalVarLn.MapX1,
                                   GlobalVarLn.MapY1
                                  );
                }
  
                // ---------------------------------------------------------------------------------
                // OB1
                if (GlobalVarLn.fl_Open_objFormOB1 == 1)

                {
                    objFormOB1.f_OB1(
                                   GlobalVarLn.MapX1,
                                   GlobalVarLn.MapY1
                                  );
                }
                // ---------------------------------------------------------------------------------
                // OB2
                if (GlobalVarLn.fl_Open_objFormOB2 == 1)

                {
                    objFormOB2.f_OB2(
                                   GlobalVarLn.MapX1,
                                   GlobalVarLn.MapY1
                                  );
                }
            // ---------------------------------------------------------------------------------
            // Az1(Object)

            if (GlobalVarLn.blOB_az1 == true)
            {

                GlobalVarLn.XXXaz = GlobalVarLn.MapX1;
                GlobalVarLn.YYYaz = GlobalVarLn.MapY1;

            }
            // ---------------------------------------------------------------------------------
            // SPFBRR

            if (GlobalVarLn.fl_Open_objFormSPFB == 1)

            {
                objFormSPFB.f_SPFB_f1();
            }
            // ---------------------------------------------------------------------------------
            // SPFBPR

            if (GlobalVarLn.fl_Open_objFormSPFB1 == 1)

            {
                objFormSPFB1.f_SPFB_f2();
            }
            // ---------------------------------------------------------------------------------
            // SCREEN
            // SCR 1-я точка

            if ((GlobalVarLn.flScrM1 == 1) && (GlobalVarLn.flScrM2 == 0))
            {
                GlobalVarLn.flScrM2 = 1;

                GlobalVarLn.X1Scr = GlobalVarLn.MapX1;
                GlobalVarLn.Y1Scr = GlobalVarLn.MapY1;

                //0206
                //GlobalVarLn.location1 = new Point(Control.MousePosition.X, Control.MousePosition.Y);

            }
            // ---------------------------------------------------------------------------------


            // ******************************************************************************* Lena


            if (e.button == 0)
            {
                waspressleft = true;
                startlefteX = e.x;
                startlefteY = e.y;

                MapCore.mapPlaneToPicture((int)axaxcMapScreen1.MapHandle, ref startlefteX, ref startlefteY);

            }
            if (e.button == 1)
            {
                righteX = e.x;
                righteY = e.y;
            }

        } // P/P
        // ************************************************************************************************* MOUSE_DOWN
*/


       // MOUSE_MOVE *************************************************************************************************
       // MouseMove

        //0206*
        private void OnMapMapMouseMove(object sender, Location location)
        {

/*
            var altitudeSuffix = location.Altitude != null
                ? string.Format("{0} m", location.Altitude)
                : "";

            PositionLabel.Text = string.Format("{0:F3}  {1:F3}  {2}\r\nMGRS: {3}",
                location.Latitude, location.Longitude, altitudeSuffix, location.ToMgrs().ToLongString());

            if (_addPointsToLine)
            {
                var point = location.ToPoint();
                _linePoints.Add(point);
            }
*/


            // преобразование В меркатор
           // var p = Mercator.FromLonLat(e.Location.Longitude, e.Location.Latitude);
            // преобразование из меркатора в wgs84
            //var p2 = Mercator.ToLonLat(p.X, p.Y);

            double moveX = 0;
            double moveY = 0;

            if ((location.Latitude != null) && (location.Longitude != null))
            {
            //moveX = location.Latitude;
            //moveY = location.Longitude;
            //lLat.Text = string.Format("Latitude = {0}", moveX.ToString("0.000"));
            //lLon.Text = string.Format("Longitude = {0}", moveY.ToString("0.000"));
            }

            Peleng peleng = new Peleng("");
            int iDegreeLat=0;
            int iMinuteLat=0;
            double dSecondLat = 0;
            int iDegreeLong = 0;
            int iMinuteLong = 0;
            double dSecondLong = 0;
            int altitudeSuffix;

            if ((location.Latitude != null) && (location.Longitude != null))
            {
            // преобразование В меркатор
            var p = Mercator.FromLonLat(location.Longitude, location.Latitude);
            moveX = p.X;
            moveY = p.Y;
            //lX.Text = string.Format("X, m = {0}", Convert.ToString((int)moveX));
            //lY.Text = string.Format("Y, m = {0}", Convert.ToString((int)moveY));

            peleng.f_Grad_GMS(location.Latitude, ref iDegreeLat, ref iMinuteLat, ref dSecondLat);
            lLat.Text = string.Format("Latitude: {0}", iDegreeLat.ToString("00") + '\xB0' + " " + iMinuteLat.ToString("00") + "' " + dSecondLat.ToString("00") + "''" + " N");
            peleng.f_Grad_GMS(location.Longitude, ref iDegreeLong, ref iMinuteLong, ref dSecondLong);
            lLon.Text = string.Format("    Longitude: {0}", iDegreeLong.ToString("00") + '\xB0' + " " + iMinuteLong.ToString("00") + "' " + dSecondLong.ToString("00") + "''" + "  E");

             //0204
            //var ppp1 = location.ToMgrs();




            }

            if (location.Altitude!=null)
            {
            //moveX = (int)location.Altitude;
            //lH.Text = string.Format("H, m = {0}", Convert.ToString((int)moveX));

            //0211
            altitudeSuffix = (int)location.Altitude;
            lH.Text = string.Format("    Altitude: {0}", altitudeSuffix);
            lX.Text = string.Format("    MGRS: {0}", location.ToMgrs().ToLongString());

            }

            // SPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFB
            //0219

            String strLine2 = "";
            String strLine3 = "";

            int iaz = 0;
            double X_Coordl3_1 = 0;
            double Y_Coordl3_1 = 0;
            double X_Coordl3_2 = 0;
            double Y_Coordl3_2 = 0;
            double DXX3 = 0;
            double DYY3 = 0;
            double azz = 0;
            long ichislo = 0;
            double dchislo = 0;

            ClassMap objClassMap10 = new ClassMap();

            if ((GlobalVarLn.objFormSPFBG.chbXY.Checked == true) &&
                (GlobalVarLn.flF_f1 == 1)

            )
            {
                strLine2 = Convert.ToString(
                    GlobalVarLn.objFormSPFBG.cbChooseSC.Items[
                        GlobalVarLn.objFormSPFBG.cbChooseSC.SelectedIndex]); // Numb

                // if2
                if (!string.IsNullOrEmpty(strLine2))
                {
                    var stations = GlobalVarLn.GetListJSWithCurrentPosition();

                    for (iaz = 0; iaz < stations.Count; iaz++)
                    {

                        X_Coordl3_1 = stations[iaz].CurrentPosition.x;
                        Y_Coordl3_1 = stations[iaz].CurrentPosition.y;
                        X_Coordl3_2 = moveX;
                        Y_Coordl3_2 = moveY;

                        strLine3 = stations[iaz].Name;

                        // if1
                        if (String.Compare(strLine2, strLine3) == 0)
                        {
                            // Разность координат для расчета азимута
                            // На карте X - вверх
                            //0219 !!!
                            //DXX3 = Y_Coordl3_2 - Y_Coordl3_1;
                            //DYY3 = X_Coordl3_2 - X_Coordl3_1;
                            DXX3 = X_Coordl3_2 - X_Coordl3_1;
                            DYY3 = Y_Coordl3_2 - Y_Coordl3_1;

                            // grad
                            azz = objClassMap10.f_Def_Azimuth(
                                DYY3,
                                DXX3
                            );

                            GlobalVarLn.Az_f1 = azz;

                            ichislo = (long)(azz * 100);
                            dchislo = ((double)ichislo) / 100;
                            lB.Text = string.Format("B,grad  = {0}", Convert.ToString((int)azz));

                            //0219
                            // Убрать с карты
                            MapForm.REDRAW_MAP();
                            GlobalVarLn.objFormSPFBG.f_LuchReDraw(X_Coordl3_1, Y_Coordl3_1, X_Coordl3_2,
                                Y_Coordl3_2);

                        } // if1

                    } // FOR

                }
            }

            // seg
            if ((GlobalVarLn.objFormSPFBG.chbXY.Checked == false) && (GlobalVarLn.flF_f1 == 1))
            {
                lB.Text = "";
            }
            // SPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFB

            // SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1
            //0219


            if ((GlobalVarLn.objFormSPFB1G.chbXY.Checked == true) &&
                (GlobalVarLn.flF_f2 == 1)

            )
            {
                strLine2 = Convert.ToString(
                    GlobalVarLn.objFormSPFB1G.cbChooseSC.Items[
                        GlobalVarLn.objFormSPFB1G.cbChooseSC.SelectedIndex]); // Numb

                // if2
                if (!string.IsNullOrEmpty(strLine2))
                {
                    var stations = GlobalVarLn.GetListJSWithCurrentPosition();

                    for (iaz = 0; iaz < stations.Count; iaz++)
                    {

                        X_Coordl3_1 = stations[iaz].CurrentPosition.x;
                        Y_Coordl3_1 = stations[iaz].CurrentPosition.y;
                        X_Coordl3_2 = moveX;
                        Y_Coordl3_2 = moveY;

                        strLine3 = stations[iaz].Name;

                        // if1
                        if (String.Compare(strLine2, strLine3) == 0)
                        {
                            // Разность координат для расчета азимута
                            // На карте X - вверх
                            //0219 !!!
                            //DXX3 = Y_Coordl3_2 - Y_Coordl3_1;
                            //DYY3 = X_Coordl3_2 - X_Coordl3_1;
                            DXX3 = X_Coordl3_2 - X_Coordl3_1;
                            DYY3 = Y_Coordl3_2 - Y_Coordl3_1;

                            // grad
                            azz = objClassMap10.f_Def_Azimuth(
                                DYY3,
                                DXX3
                            );

                            GlobalVarLn.Az_f2 = azz;

                            ichislo = (long)(azz * 100);
                            dchislo = ((double)ichislo) / 100;
                            lB.Text = string.Format("B,grad  = {0}", Convert.ToString((int)azz)); // seg

                            // Убрать с карты
                            //GlobalVarLn.axMapScreenGlobal.Repaint();
                            //GlobalVarLn.objFormAzG.f_Map_Line_XY1(X_Coordl3_1, Y_Coordl3_1, X_Coordl3_2,
                            //    Y_Coordl3_2);
                            MapForm.REDRAW_MAP();
                            GlobalVarLn.objFormSPFBG.f_LuchReDraw(X_Coordl3_1, Y_Coordl3_1, X_Coordl3_2,
                                Y_Coordl3_2);


                        } // if1

                    } // FOR

                }
            }

            // seg
            if ((GlobalVarLn.objFormSPFB1G.chbXY.Checked == false) && (GlobalVarLn.flF_f2 == 1))
            {
                lB.Text = "";
            }

            // SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1

        } // P/P

/*
        private void axaxcMapScreen1_OnMapMouseMove(object sender, AxaxGisToolKit.IaxMapScreenEvents_OnMapMouseMoveEvent e)
        {


            //0206

            if ((waspressleft == true)&&
                (GlobalVarLn.flScrM1 == 0)
                )
            {
                movelefteX = e.x;
                movelefteY = e.y;

                MapCore.mapPlaneToPicture((int)axaxcMapScreen1.MapHandle, ref movelefteX, ref movelefteY);

                //0206
                //this.Cursor = Cursors.SizeAll;

                axaxcMapScreen1.MapLeft -= (int)(movelefteX - startlefteX);
                axaxcMapScreen1.MapTop -= (int)(movelefteY - startlefteY);

            }

            // llllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll
            // Lena
            if (MapIsOpenned == true)
            {
                double moveX1 = 0;
                double moveY1 = 0;
                double moveHH = 0;

                //Пересчет
                double moveX = e.x;
                double moveY = e.y;

                lX.Text = string.Format("X, m = {0}", Convert.ToString((int)moveX));
                lY.Text = string.Format("Y, m = {0}", Convert.ToString((int)moveY));

                moveX1 = moveX;
                moveY1 = moveY;


                // SCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCR
                // SCR MouseMove

                if ((GlobalVarLn.flScrM2 == 1) && (GlobalVarLn.flScrM3 == 0))
                {
                    // Сняли координаты -> GlobalVarLn.MapX1,...(m на местности)
                    ClassMap.f_XYMap(e.x, e.y);

                    GlobalVarLn.X2Scr = moveX1;
                    GlobalVarLn.Y2Scr = moveY1;

                    GlobalVarLn.axMapScreenGlobal.Repaint();
                    ClassMap.f_Screen_stat();
                }
                // SCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCRSCR


                // .......................................................................
                // H

                GlobalVarLn.axMapPointGlobalAdd.SetPoint(moveX1, moveY1);
                moveHH = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);

                lH.Text = string.Format("H, m = {0}", Convert.ToString((int)moveHH));
                // ......................................................................

                MapCore.mapPlaneToGeo((int)axaxcMapScreen1.MapHandle, ref moveX, ref moveY);
                MapCore.mapGeoToRealGeo(ref moveX, ref moveY);

                lLat.Text = string.Format("Latitude = {0}", moveX.ToString("0.000"));
                lLon.Text = string.Format("Longitude = {0}", moveY.ToString("0.000"));

                String strLine2="";
                String strLine3 = "";

                int iaz = 0;
                double X_Coordl3_1 = 0;
                double Y_Coordl3_1 = 0;
                double X_Coordl3_2 = 0;
                double Y_Coordl3_2 = 0;
                double DXX3 = 0;
                double DYY3 = 0;
                double azz = 0;
                long ichislo = 0;
                double dchislo = 0;

                ClassMap objClassMap10 = new ClassMap();

                if ((GlobalVarLn.objFormSPFBG.chbXY.Checked == true) &&
                    (GlobalVarLn.flF_f1 == 1)

                )
                {
                    strLine2 = Convert.ToString(
                        GlobalVarLn.objFormSPFBG.cbChooseSC.Items[
                            GlobalVarLn.objFormSPFBG.cbChooseSC.SelectedIndex]); // Numb

                    // if2
                    if (!string.IsNullOrEmpty(strLine2))
                    {
                        var stations = GlobalVarLn.GetListJSWithCurrentPosition();

                        for (iaz = 0; iaz < stations.Count; iaz++)
                        {

                            X_Coordl3_1 = stations[iaz].CurrentPosition.x;
                            Y_Coordl3_1 = stations[iaz].CurrentPosition.y;
                            X_Coordl3_2 = (int) moveX1;
                            Y_Coordl3_2 = (int) moveY1;

                            strLine3 = stations[iaz].Name;

                            // if1
                            if (String.Compare(strLine2, strLine3) == 0)
                            {
                                // Разность координат для расчета азимута
                                // На карте X - вверх
                                DXX3 = Y_Coordl3_2 - Y_Coordl3_1;
                                DYY3 = X_Coordl3_2 - X_Coordl3_1;

                                // grad
                                azz = objClassMap10.f_Def_Azimuth(
                                    DYY3,
                                    DXX3
                                );

                                GlobalVarLn.Az_f1 = azz;

                                ichislo = (long) (azz * 100);
                                dchislo = ((double) ichislo) / 100;
                                lB.Text = string.Format("B,grad  = {0}", Convert.ToString((int) azz));


                                // Убрать с карты
                                GlobalVarLn.axMapScreenGlobal.Repaint();
                                GlobalVarLn.objFormAzG.f_Map_Line_XY1(X_Coordl3_1, Y_Coordl3_1, X_Coordl3_2,
                                    Y_Coordl3_2);

                            } // if1

                        } // FOR

                    }
                }

                // seg
                if ((GlobalVarLn.objFormSPFBG.chbXY.Checked == false) && (GlobalVarLn.flF_f1 == 1))
                {
                    lB.Text = "";
                }

                // SPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFB

                // SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1
                if ((GlobalVarLn.objFormSPFB1G.chbXY.Checked == true) &&
                    (GlobalVarLn.flF_f2 == 1)

                )
                {
                    strLine2 = Convert.ToString(
                        GlobalVarLn.objFormSPFB1G.cbChooseSC.Items[
                            GlobalVarLn.objFormSPFB1G.cbChooseSC.SelectedIndex]); // Numb

                    // if2
                    if (!string.IsNullOrEmpty(strLine2))
                    {
                        var stations = GlobalVarLn.GetListJSWithCurrentPosition();

                        for (iaz = 0; iaz < stations.Count; iaz++)
                        {

                            X_Coordl3_1 = stations[iaz].CurrentPosition.x;
                            Y_Coordl3_1 = stations[iaz].CurrentPosition.y;
                            X_Coordl3_2 = (int) moveX1;
                            Y_Coordl3_2 = (int) moveY1;

                            strLine3 = stations[iaz].Name;

                            // if1
                            if (String.Compare(strLine2, strLine3) == 0)
                            {
                                // Разность координат для расчета азимута
                                // На карте X - вверх
                                DXX3 = Y_Coordl3_2 - Y_Coordl3_1;
                                DYY3 = X_Coordl3_2 - X_Coordl3_1;

                                // grad
                                azz = objClassMap10.f_Def_Azimuth(
                                    DYY3,
                                    DXX3
                                );

                                GlobalVarLn.Az_f2 = azz;

                                ichislo = (long) (azz * 100);
                                dchislo = ((double) ichislo) / 100;
                                lB.Text = string.Format("B,grad  = {0}", Convert.ToString((int) azz)); // seg

                                // Убрать с карты
                                GlobalVarLn.axMapScreenGlobal.Repaint();
                                GlobalVarLn.objFormAzG.f_Map_Line_XY1(X_Coordl3_1, Y_Coordl3_1, X_Coordl3_2,
                                    Y_Coordl3_2);

                            } // if1



                        } // FOR

                    }
                }

                // seg
                if ((GlobalVarLn.objFormSPFB1G.chbXY.Checked == false) && (GlobalVarLn.flF_f2 == 1))
                {
                    //statusStrip1.Items[5].Text = "";
                    lB.Text = "";
                }

                // SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1


            }
            // llllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll


            // Перерисовка зон **********************************************************************
            // 0510


            if (
                (GlobalVarLn.MousePress == true)&&
                (GlobalVarLn.flScrM2==1)
               )

            {
                // -----------------------------------------------------------------------------------
                // ZPV
                if (
                    (GlobalVarLn.fl_LineSightRange == 1)
                    )
                {
                    ClassMap.f_DrawSPXY(
                                  GlobalVarLn.XCenter_ZPV,  // m на местности
                                  GlobalVarLn.YCenter_ZPV,
                                      ""
                                 );
                    ClassMap.DrawLSR();
                }
                // -----------------------------------------------------------------------------------
                // Зона обнаружения СП
                if (
                    (GlobalVarLn.fl_ZOSP == 1)
                   )
                {
                    //0206
                    //ClassMap.DrawPolygon(GlobalVarLn.listDetectionZone, Color.HotPink);
                }
                // -----------------------------------------------------------------------------------
                // Зона подавления
                if (
                    (GlobalVarLn.flCoordSP_sup == 1)
                   )
                {
                    ClassMap.f_Map_Redraw_Zon_Suppression();
                }
                // -------------------------------------------------------------------------------------
                // Зона подавления навигации
                if (
                    (GlobalVarLn.fl_supnav == 1)
                   )
                {
                    // SP
                    ClassMap.f_DrawSPXY(
                        GlobalVarLn.XCenter_supnav, // m на местности
                        GlobalVarLn.YCenter_supnav,
                        ""
                    );

                    //0206
                    //ClassMap.DrawPolygon(GlobalVarLn.listNavigationJammingZone, Color.Blue);
                }
                // -----------------------------------------------------------------------------------
                // Зона спуфинга
                if (
                    (GlobalVarLn.fl_spf == 1)
                   )
                {
                    // SP
                    ClassMap.f_DrawSPXY(
                                  GlobalVarLn.XCenter_spf,  // m на местности
                                  GlobalVarLn.YCenter_spf,
                                      ""
                                 );
                    if (GlobalVarLn.flS_spf == 1)

                        //0206
                        //ClassMap.DrawPolygon(GlobalVarLn.listSpoofingZone, Color.Green);

                        if (GlobalVarLn.flJ_spf == 1) ;

                        //0206
                        //ClassMap.DrawPolygon(GlobalVarLn.listSpoofingJamZone, Color.Blue);
                }
                // -----------------------------------------------------------------------------------
            } // Mouse Press==true
            // ********************************************************************** Перерисовка зон


        }
 */
        // *********************************************************************************** MOUSE_MOVE

       // MOUSE_UP **************************************************************************************
       // MouseUp 

/*
        private void axaxcMapScreen1_OnMapMouseUp(object sender, AxaxGisToolKit.IaxMapScreenEvents_OnMapMouseUpEvent e)
        {

            // ------------------------------------------------------------------------------------
            if (e.button == 0)
            {
                waspressleft = false;
                //MapCore.layersMapFunc(ref axaxcMapScreen1, true);

                // 0510
                GlobalVarLn.MousePress = false;



                // 0510
                if(
                    (GlobalVarLn.fl_LineSightRange == 1)||
                    (GlobalVarLn.fl_ZOSP == 1)||
                    (GlobalVarLn.flCoordSP_sup == 1)||
                    (GlobalVarLn.fl_supnav == 1)||
                    (GlobalVarLn.fl_spf == 1) 
                   )
                // Убрать с карты
                GlobalVarLn.axMapScreenGlobal.Repaint();

                // Перерисовка зон **********************************************************************
                // 0510

                // -----------------------------------------------------------------------------------
                // ZPV
                if (
                    (GlobalVarLn.fl_LineSightRange == 1) &&
                    (GlobalVarLn.MousePress == false)
                    )
                {
                    ClassMap.f_DrawSPXY(
                                  GlobalVarLn.XCenter_ZPV,  // m на местности
                                  GlobalVarLn.YCenter_ZPV,
                                      ""
                                 );
                    ClassMap.DrawLSR_Rastr();
                }
                // -----------------------------------------------------------------------------------
                // Зона обнаружения СП
                if (
                    (GlobalVarLn.fl_ZOSP == 1) &&
                    (GlobalVarLn.MousePress == false)
                   )
                {
                    //0206
                    //ClassMap.DrawPolygon(GlobalVarLn.listDetectionZone, Color.HotPink);
                }
                // -----------------------------------------------------------------------------------
                // Зона подавления
                if (
                    (GlobalVarLn.flCoordSP_sup == 1) &&
                    (GlobalVarLn.MousePress == false)
                   )
                {
                    ClassMap.f_Map_Redraw_Zon_Suppression();
                }
                // -------------------------------------------------------------------------------------
                // Зона подавления навигации
                if (
                    (GlobalVarLn.fl_supnav == 1) &&
                    (GlobalVarLn.MousePress == false)
                   )
                {
                    // SP
                    ClassMap.f_DrawSPXY(
                        GlobalVarLn.XCenter_supnav, // m на местности
                        GlobalVarLn.YCenter_supnav,
                        ""
                    );

                    //0206
                    //ClassMap.DrawPolygon(GlobalVarLn.listNavigationJammingZone, Color.Blue);
                }
                // -----------------------------------------------------------------------------------
                // Зона спуфинга
                if (
                    (GlobalVarLn.fl_spf == 1) &&
                    (GlobalVarLn.MousePress == false)
                   )
                {
                    // SP
                    ClassMap.f_DrawSPXY(
                                  GlobalVarLn.XCenter_spf,  // m на местности
                                  GlobalVarLn.YCenter_spf,
                                      ""
                                 );
                    if (GlobalVarLn.flS_spf == 1) ;
                        //0206
                        //ClassMap.DrawPolygon(GlobalVarLn.listSpoofingZone, Color.Green);

                    if (GlobalVarLn.flJ_spf == 1) ;
                        //0206
                        //ClassMap.DrawPolygon(GlobalVarLn.listSpoofingJamZone, Color.Blue);
                }
                // -----------------------------------------------------------------------------------

                // ********************************************************************** Перерисовка зон

            } // e.button=0
            // ------------------------------------------------------------------------------------


            // --------------------------------------------------------------------------------------------------
            // SCR  последняя точка

            if ((GlobalVarLn.flScrM2 == 1) && (GlobalVarLn.flScrM3 == 0))
            {
                //0206
                //GlobalVarLn.location2 = new Point(Control.MousePosition.X, Control.MousePosition.Y);


                GlobalVarLn.flScrM3 = 1;
                GlobalVarLn.X2Scr = GlobalVarLn.MapX1;
                GlobalVarLn.Y2Scr = GlobalVarLn.MapY1;
                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                this.TopMost = true;
                this.Invalidate();
                GlobalVarLn.objFormScreenG.TopMost = false;
                GlobalVarLn.objFormScreenG.Invalidate();
                GlobalVarLn.objFormScreenG.Close();
                GlobalVarLn.objFormScreenG.Invalidate();
                GlobalVarLn.objFormScreenG.Hide();
                GlobalVarLn.objFormScreenG.Invalidate();
                this.Show();
                this.Activate();
                this.Invalidate();

                GlobalVarLn.objFormScreenG.pbScreen.Image = null;
                //0206
                //GlobalVarLn.objFormScreenG.pbScreen.Image = GlobalVarLn.objFormScreenG.ImageFromScreen(axaxcMapScreen1);
                //0206
                //GlobalVarLn.bmpScr = new Bitmap(GlobalVarLn.objFormScreenG.pbScreen.Image);

                this.TopMost = false;
                this.Invalidate();
                GlobalVarLn.objFormScreenG.TopMost = true;
                GlobalVarLn.objFormScreenG.Invalidate();
                GlobalVarLn.objFormScreenG.Activate();
                GlobalVarLn.objFormScreenG.Invalidate();
                GlobalVarLn.objFormScreenG.Show();
                GlobalVarLn.objFormScreenG.Invalidate();


                // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            }
            // --------------------------------------------------------------------------------------------------



        } // MouseUp
 */
      // ************************************************************************************** MOUSE_UP

        //Флаги перерисовки
        bool paintRadioSources = true;
        bool paintStations = true;
        bool paintArcPoints = true;
        bool paintPeleng = true;
        bool infopaint = true;


// ПЕРЕРИСОВКА КАРТЫ ***********************************************************************************************

/*
        //Перисовка карты
        private void axaxcMapScreen1_OnMapPaint(object sender, AxaxGisToolKit.IaxMapScreenEvents_OnMapPaintEvent e)
        {


            // LENA **********************************************************************************
            // Lena (перерисовка)


            if (GlobalVarLn.fllTab == 1)
            {
                for (int iitab = 0; iitab < GlobalVarLn.list_air_tab.Count; iitab++)
                {
                    //ClassMap.f_DrawAirPlane(GlobalVarLn.list_air_tab[iitab].Lat, GlobalVarLn.list_air_tab[iitab].Long, 0);
                    ClassMap.f_DrawAirPlane_Tab(GlobalVarLn.list_air_tab[iitab].Lat, GlobalVarLn.list_air_tab[iitab].Long, 0, GlobalVarLn.list_air_tab[iitab].Num);

                }
            }
            // ------------------------------------------------------------------------------------
            // Az

            if (GlobalVarLn.flDrawAzz == 1)
            {

                GlobalVarLn.objFormAzG.f_ReDrawAzz();
            }

            // ------------------------------------------------------------------------------------
            // Координаты (Form1)

            if (GlobalVarLn.fl_DrawCoord == 1)
            {

                ClassMap.f_Map_Rect_LatLong_stat(
                                     GlobalVarLn.Lat_CRD,
                                     GlobalVarLn.Long_CRD
                                     );
            }
            // ------------------------------------------------------------------------------------
            // Пеленг

            if (GlobalVarLn.fl_Peleng_stat == 1)
            {
                ClassMap.f_ReDrawPeleng();
            }
            // -----------------------------------------------------------------------------------
            // S

            if (GlobalVarLn.fl_S1_stat == 1)
            {
                ClassMap.f_ReDrawS_stat();
            }
            // -----------------------------------------------------------------------------------
            // Маршрут

            if (GlobalVarLn.flEndWay_stat == 1)
            {
                objFormWay.f_WayReDraw();

            }
            // -----------------------------------------------------------------------------------
            // Самолеты

            if (GlobalVarLn.fl_AirPlaneVisible == 1)
            {

                // Перерисовка самолетов
                ClassMap.f_ReDrawAirPlane1();
            }
            // -------------------------------------------------------------------------------------
            // Зона энергодоступности

            if (GlobalVarLn.fl_ZonePowerAvail == 1)
            {
                // SP
                //ClassMap.f_Map_Pol_XY_stat(
                //              GlobalVarLn.XCenter_ed,  // m
                //              GlobalVarLn.YCenter_ed,
                //              1,
                //              ""
                 //            );

                // SP
                ClassMap.f_DrawSPXY(
                              GlobalVarLn.XCenter_ed,  // m
                              GlobalVarLn.YCenter_ed,
                                  ""
                             );

                // Перерисовка зоны
                ClassMap.f_Map_El_XY_ed(
                                        GlobalVarLn.tpOwnCoordRect,
                                        GlobalVarLn.liRadiusZone_ed
                                        );
            }
            // -------------------------------------------------------------------------------------
            // Энергодоступность по УС

            if (GlobalVarLn.fl_CommPowerAvail == 1)
            {
                ClassMap.f_Map_Redraw_CommPowerAvail();
            }
            // -------------------------------------------------------------------------------------
            // ЗПВ

            // 0510
            if (
                (GlobalVarLn.fl_LineSightRange == 1)&&
                (GlobalVarLn.MousePress==false)
                )
            {
                // Центр ЗПВ
                //ClassMap.f_Map_Pol_XY_stat(
                //              GlobalVarLn.XCenter_ZPV,  // m на местности
                //              GlobalVarLn.YCenter_ZPV,
                //              1,
                //              ""
                //             );
                // SP
                ClassMap.f_DrawSPXY(
                              GlobalVarLn.XCenter_ZPV,  // m на местности
                              GlobalVarLn.YCenter_ZPV,
                                  ""
                             );


                ClassMap.DrawLSR();
            }
            // -------------------------------------------------------------------------------------
            // SP

            // otl33
            if (GlobalVarLn.flEndSP_stat == 1)
            //if ((GlobalVarLn.flEndSP_stat == 1) && (GlobalVarLn.fclSP == 0))

            {
                objFormSP.f_SPReDraw();

            }
            // -----------------------------------------------------------------------------------
            // LF1

            if (
                (GlobalVarLn.flEndLF1_stat == 1)
                )
            {
                objFormLF.f_LF1ReDraw();

            }
            // -----------------------------------------------------------------------------------
            // LF2

            if (
                (GlobalVarLn.flEndLF2_stat == 1)
                )
            {
                objFormLF2.f_LF2ReDraw();

            }
            // -----------------------------------------------------------------------------------
            // ZO

            if (
                (GlobalVarLn.flEndZO_stat == 1)
                )
            {
                objFormZO.f_ZOReDraw();
            }
            // -----------------------------------------------------------------------------------
            // TRO

            if (
                (GlobalVarLn.flEndTRO_stat == 1)
                )
            {
               // objFormTRO.f_TROReDraw();
                objFormSP.f_SPReDraw();
                objFormLF.f_LF1ReDraw();
                objFormLF2.f_LF2ReDraw();
                objFormZO.f_ZOReDraw();
                objFormOB1.f_OB1ReDraw();
                objFormOB2.f_OB2ReDraw();

            }
            // -----------------------------------------------------------------------------------
            // OB1

            if (
                (GlobalVarLn.flEndOB1_stat == 1)
                )
            {
                objFormOB1.f_OB1ReDraw();
            }
            // -----------------------------------------------------------------------------------
            // OB2

            if (
                (GlobalVarLn.flEndOB2_stat == 1)
                )
            {
                objFormOB2.f_OB2ReDraw();
            }
            // -----------------------------------------------------------------------------------
            // Зона подавления

            //if (GlobalVarLn.fl_Suppression == 1)
            // 0510
            if (
                (GlobalVarLn.flCoordSP_sup == 1)&&
                (GlobalVarLn.MousePress==false)
               )

            {
                ClassMap.f_Map_Redraw_Zon_Suppression();
            }
            // -------------------------------------------------------------------------------------
            // Az1

            if (GlobalVarLn.flEndOB_az1 == 1)
            {
                objFormAz1.f_OBReDraw_Az1();

            }
            // -----------------------------------------------------------------------------------
            // Зона обнаружения СП

            // 0510
            if (
                (GlobalVarLn.fl_ZOSP == 1)&&
                (GlobalVarLn.MousePress==false)
               )


            {
                ;
                //0206
                //ClassMap.DrawPolygon(GlobalVarLn.listDetectionZone, Color.HotPink);
            }
            // -----------------------------------------------------------------------------------
            // Зона подавления навигации

            // 0510
            if (
                (GlobalVarLn.fl_supnav == 1)&&
                (GlobalVarLn.MousePress==false)
               )

            {
                // SP
                ClassMap.f_DrawSPXY(
                    GlobalVarLn.XCenter_supnav, // m на местности
                    GlobalVarLn.YCenter_supnav,
                    ""
                );

                //0206
                //ClassMap.DrawPolygon(GlobalVarLn.listNavigationJammingZone, Color.Blue);
            }
            // -----------------------------------------------------------------------------------
            // Зона спуфинга
            // seg1

            // 0510
            if (
                (GlobalVarLn.fl_spf == 1)&&
                (GlobalVarLn.MousePress==false)
               )

            {
                // SP
                ClassMap.f_DrawSPXY(
                              GlobalVarLn.XCenter_spf,  // m на местности
                              GlobalVarLn.YCenter_spf,
                                  ""
                             );

                if (GlobalVarLn.flS_spf == 1) ;
                      //0206
                      //ClassMap.DrawPolygon(GlobalVarLn.listSpoofingZone, Color.Green);

                if (GlobalVarLn.flJ_spf == 1) ;
                  //0206
                  //ClassMap.DrawPolygon(GlobalVarLn.listSpoofingJamZone, Color.Blue);
                 

            }
            // -----------------------------------------------------------------------------------
            // SCR перерисовка (пока ничего)


            if ((GlobalVarLn.flScrM2 == 1) && (GlobalVarLn.flScrM3 == 0))
            //if ((GlobalVarLn.flScrM2 == 1) && (GlobalVarLn.flScrM3 == 1))

            {
                //ClassMap.f_Screen_stat();
            }

            // -----------------------------------------------------------------------------------


            // ********************************************************************************** LENA



        } // Перерисовка карты

*/


        // *********************************************************************************************** ПЕРЕРИСОВКА КАРТЫ


        //Радиоисточники
        //private List<MapCore.RadioSource> radioSources = new List<MapCore.RadioSource>();

        //Станции
        //private List<MapCore.Station> stations = new List<MapCore.Station>();

        //Точки кривой
        //private List<PointF> ArcPoints = new List<PointF>();

        // Снять координаты
        //private void toolStripMenuItem1_Click(object sender, EventArgs e)
        //{
        //}

        // добавить ИРИ
        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
        }

        // удалить ИРИ
        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
        }

        // добавить точку для дуги
        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
        }

        //нарисовать дугу
        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
        }

        //очистить точки для дуги
        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
        }

        //Добавить станцию
        private void toolStripMenuItem7_Click(object sender, EventArgs e)
        {
        }

        //Удалить станцию
        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
        }

        //Отрисовать пеленг
        private void toolStripMenuItem9_Click(object sender, EventArgs e)
        {
        }

        //Отобразить/скрыть текст
        private void toolStripMenuItem10_Click(object sender, EventArgs e)
        {
        }


        // LENA ************************************************************************************
        // Lena

        // *****************************************************************************************
        // Загрузка головной формы
        // *****************************************************************************************

        private void MapForm_Load(object sender, EventArgs e)
        {
           // Forms

            GlobalVarLn.objFormSPG = objFormSP;
            GlobalVarLn.objFormLFG = objFormLF;
            GlobalVarLn.objFormLF2G = objFormLF2;
            GlobalVarLn.objFormZOG = objFormZO;
            GlobalVarLn.objFormOB1G = objFormOB1;
            GlobalVarLn.objFormOB2G = objFormOB2;
            GlobalVarLn.objFormSuppressionG = objFormSuppression;
            GlobalVarLn.objFormSPFBG = objFormSPFB;
            GlobalVarLn.objFormSPFB1G = objFormSPFB1;
            GlobalVarLn.objFormSPFB2G = objFormSPFB2;
            GlobalVarLn.objFormAz1G = objFormAz1;
            GlobalVarLn.objFormLineSightRangeG = objFormLineSightRange;
            GlobalVarLn.objFormWayG = objFormWay;
            GlobalVarLn.objFormSupSpufG = objFormSupSpuf;
            GlobalVarLn.objFormSpufG = objFormSpuf;
            GlobalVarLn.objFormScreenG = objFormScreen;
            GlobalVarLn.objFormTROG = objFormTRO;
            GlobalVarLn.objFormTabG = objFormTab;

           // .................................................................
           // 0206
/*
            if (HeightMatrixIsOpenned != true) 
            {
                openFileDialog1.FileName = ".mtw";
                openFileDialog1.Filter = "Matrix file|*.mtw|All files|*.*";
                openFileDialog1.InitialDirectory = Application.StartupPath;
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    mapOpenMtrForMap((int)axaxcMapScreen1.MapHandle, openFileDialog1.FileName, 0);
                    HeightMatrixIsOpenned = true;
                    axaxcMapScreen1.Repaint();
                }
                else
                {
                    HeightMatrixIsOpenned = false;
                }
            }
*/
            // .................................................................

            try
            {
                //0206
                //bmas = iniRW.GetMapLayersSettings();
                //SetViewSelectLayers();
            }
            catch (Exception) { }
// *********************************************************************************************
//0206***

  GlobalVarLn.LoadListJS();
  //GlobalVarLn.objFormSPG.f_SPReDraw();
  GlobalVarLn.flEndSP_stat = 1;
            
// *********************************************************************************************
// Значки
            String nn;
            String nn1;
            String s;
            long iz;
// ..............................................................................................
 // OB1
            GlobalVarLn.objFormOB1G.imageList1.Images.Clear();
            try
            {
                s = Application.StartupPath + "\\Images\\OwnObject\\";
                DirectoryInfo di = new DirectoryInfo(@s);

                iz = 0;
                foreach (var fi in di.GetFiles("*.png"))
                {

                    nn = fi.Name;
                    nn1 = fi.DirectoryName + "\\" + fi.Name;
                    GlobalVarLn.objFormOB1G.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi_1 in di.GetFiles("*.bmp"))
                {

                    nn = fi_1.Name;
                    nn1 = fi_1.DirectoryName + "\\" + fi_1.Name;
                    GlobalVarLn.objFormOB1G.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi_1 in di.GetFiles("*.jpeg"))
                {

                    nn = fi_1.Name;
                    nn1 = fi_1.DirectoryName + "\\" + fi_1.Name;
                    GlobalVarLn.objFormOB1G.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }


                if (iz == 0) MessageBox.Show("No OwnObject signs");
            }
            catch
            {
                MessageBox.Show("No memory");
            }
// ..............................................................................................
 // OB2
            GlobalVarLn.objFormOB2G.imageList1.Images.Clear();
            try
            {
                s = Application.StartupPath + "\\Images\\EnemyObject\\";
                DirectoryInfo di1 = new DirectoryInfo(@s);

                iz = 0;
                foreach (var fi1 in di1.GetFiles("*.png"))
                {

                    nn = fi1.Name;
                    nn1 = fi1.DirectoryName + "\\" + fi1.Name;
                    GlobalVarLn.objFormOB2G.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi1_1 in di1.GetFiles("*.bmp"))
                {

                    nn = fi1_1.Name;
                    nn1 = fi1_1.DirectoryName + "\\" + fi1_1.Name;
                    GlobalVarLn.objFormOB2G.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi1_1 in di1.GetFiles("*.jpeg"))
                {

                    nn = fi1_1.Name;
                    nn1 = fi1_1.DirectoryName + "\\" + fi1_1.Name;
                    GlobalVarLn.objFormOB2G.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }

                if (iz == 0) MessageBox.Show("No EnemyObject signs");
            }
            catch
            {
                MessageBox.Show("No memory");
            }
//...............................................................................................
// SP
            GlobalVarLn.objFormSPG.imageList1.Images.Clear();
            try
            {
                s = Application.StartupPath + "\\Images\\Jammer\\";
                DirectoryInfo di2 = new DirectoryInfo(@s);

                iz = 0;
                foreach (var fi2 in di2.GetFiles("*.png"))
                {

                    nn = fi2.Name;
                    nn1 = fi2.DirectoryName + "\\" + fi2.Name;
                    GlobalVarLn.objFormSPG.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi2_1 in di2.GetFiles("*.bmp"))
                {

                    nn = fi2_1.Name;
                    nn1 = fi2_1.DirectoryName + "\\" + fi2_1.Name;
                    GlobalVarLn.objFormSPG.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi2_1 in di2.GetFiles("*.jpeg"))
                {

                    nn = fi2_1.Name;
                    nn1 = fi2_1.DirectoryName + "\\" + fi2_1.Name;
                    GlobalVarLn.objFormSPG.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }

                if (iz == 0) MessageBox.Show("No Jammer signs");
            }
            catch
            {
                MessageBox.Show("No memory");
            }
//...............................................................................................
// SPV
            GlobalVarLn.objFormSPG.imageList1V.Images.Clear();
            try
            {
                s = Application.StartupPath + "\\Images\\JammerPlanned\\";
                DirectoryInfo di3 = new DirectoryInfo(@s);

                iz = 0;
                foreach (var fi3 in di3.GetFiles("*.png"))
                {

                    nn = fi3.Name;
                    nn1 = fi3.DirectoryName + "\\" + fi3.Name;
                    GlobalVarLn.objFormSPG.imageList1V.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi3_1 in di3.GetFiles("*.bmp"))
                {

                    nn = fi3_1.Name;
                    nn1 = fi3_1.DirectoryName + "\\" + fi3_1.Name;
                    GlobalVarLn.objFormSPG.imageList1V.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }
                foreach (var fi3_1 in di3.GetFiles("*.jpeg"))
                {

                    nn = fi3_1.Name;
                    nn1 = fi3_1.DirectoryName + "\\" + fi3_1.Name;
                    GlobalVarLn.objFormSPG.imageList1V.Images.Add(Image.FromFile(nn1));
                    iz += 1;

                }

                if (iz == 0) MessageBox.Show("No Jammer(planned) signs");
            }
            catch
            {
                MessageBox.Show("No memory");
            }
//...............................................................................................


            // *********************************************************************************************


            // .....................................................................................
            // Очистка dataGridView1+ Установка 100 строк
            // 10_10_2018

            // Очистка dataGridView1
            while (objFormOB2.dataGridView1.Rows.Count != 0)
                objFormOB2.dataGridView1.Rows.Remove(objFormOB2.dataGridView1.Rows[objFormOB2.dataGridView1.Rows.Count - 1]);

            for (int i = 0; i < GlobalVarLn.sizeDatOB2_stat; i++)
            {
                objFormOB2.dataGridView1.Rows.Add("", "", "", "", "");
            }
            // -------------------------------------------------------------------
            // Очистка dataGridView1+ Установка 100 строк
            // 10_10_2018

            // Очистка dataGridView1
            while (objFormOB1.dataGridView1.Rows.Count != 0)
                objFormOB1.dataGridView1.Rows.Remove(objFormOB1.dataGridView1.Rows[objFormOB1.dataGridView1.Rows.Count - 1]);

            for (int iyu = 0; iyu < GlobalVarLn.sizeDatOB1_stat; iyu++)
            {
                objFormOB1.dataGridView1.Rows.Add("", "", "", "");
            }
            // -------------------------------------------------------------------


            //0206*
            GlobalVarLn.objFormSPG.f_SPReDraw();



        } // Загрузка головной формы
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button2: открыть окно отображения координат
        // *****************************************************************************************
        private void button2_Click(object sender, EventArgs e)
        {


        } // Button2

        // *****************************************************************************************
        // Обработчик кнопки Button3: открыть окно отображения пеленга
        // *****************************************************************************************
      

        // *****************************************************************************************
        //// Обработчик кнопки Button4: расстояние между двумя пунктами
        // Screen
        // *****************************************************************************************
        private void button4_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            //0219

            GlobalVarLn.fl_Open_objFormScreen = 1;
            //RasterMapControl.SaveCurrentMapView("file.png");
            //MessageBox.Show("File is saved");


            String strLine = "";
            String strLine1 = "";
            String strLine2 = "";
            String path = "";

            int lng = 0;
            int indStart = 0;
            int indStop = 0;
            int indEnd = 0;
            int iLength = 0;
            int iLength1 = 0;
            int TekPoz = 0;

            String time = DateTime.Now.ToString(@"dd/MM/yyyy HH:mm:ss");

            TekPoz = 0;
            strLine = time;
            lng = time.Length;
            indEnd = lng - 1;
            indStart = 0;
            indStop = strLine.IndexOf(' ', TekPoz);  // probel
            iLength = indStop - indStart + 1;
            iLength1 = indEnd - indStop + 1;
            strLine1 = strLine.Remove(indStop, iLength1);
            strLine2 = strLine.Remove(indStart, iLength);
            if (strLine1.IndexOf(".") > -1) strLine1 = strLine1.Replace('.', '_');
            if (strLine1.IndexOf("/") > -1) strLine1 = strLine1.Replace('/', '_');
            if (strLine1.IndexOf("\\") > -1) strLine1 = strLine1.Replace('\\', '_');
            if (strLine2.IndexOf(":") > -1) strLine2 = strLine2.Replace(':', '_');

            if (Directory.Exists(strLine1) == false)
                Directory.CreateDirectory(Application.StartupPath + "\\MapImages\\" + strLine1);
            path = Application.StartupPath + "\\MapImages\\" + strLine1 + "\\" + strLine2 + ".png";
            RasterMapControl.SaveCurrentMapView(path);
            MessageBox.Show("File is saved");

            // -------------------------------------------------------------------------------------

/*
            if (objFormScreen == null || objFormScreen.IsDisposed)
            {
                GlobalVarLn.flScr = 0;
                GlobalVarLn.flScrM1 = 0;
                GlobalVarLn.flScrM2 = 0;
                GlobalVarLn.flScrM3 = 0;

                //objFormScreen = new FormScreen(ref axaxcMapScreen1);
                objFormScreen = new FormScreen(this, ref axaxcMapScreen1);

                objFormScreen.Show();
            }
            else
            {
                objFormScreen.Show();
            }
 */ 

        } // Button4

        // ************************************************************************************ LENA
        // Обработчик кнопки Button5: Tab
        // *****************************************************************************************
        private void button5_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormTab = 1;
            ClassMap.F_Close_Form(17);
            // -------------------------------------------------------------------------------------

            if (objFormTab == null || objFormTab.IsDisposed)
            {
                // .....................................................................................
                // Очистка dataGridView

                objFormTab.dgvTab.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeTab; i++)
                {
                    objFormTab.dgvTab.Rows.Add("", "", "", "", "", "");
                }
                // .....................................................................................
                GlobalVarLn.fllTab = 0;
                GlobalVarLn.list_air_tab.Clear();

                objFormTab = new FormTab(this);
                objFormTab.Show();
            }
            else
            {
                objFormTab.Show();
            }


        } // Button5
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button6: Маршрут
        // *****************************************************************************************

        private void button6_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormWay = 1;
            ClassMap.F_Close_Form(8);
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormWayG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFWay_stat = 1;
            //ClassMap.f_RemoveFrm(8);

            // -------------------------------------------------------------------------------------

            if (objFormWay == null || objFormWay.IsDisposed)
            {
                objFormWay = new FormWay();
                objFormWay.Show();

            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
            // Для маршрута
            // 1-я загрузка формы

            // .....................................................................................
            // Очистка dataGridView

                objFormWay.dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDatWay_stat; i++)
                {
                    objFormWay.dataGridView1.Rows.Add("", "", "");
                }
           // .....................................................................................
                if (objFormWay.chbWay.Checked == true)
                {
                    GlobalVarLn.blWay_stat = true;
                    GlobalVarLn.flEndWay_stat = 1;
                }

                // way
                //Array.Clear(GlobalVarLn.mas_LW_stat, 0, 10000);
                //Array.Clear(GlobalVarLn.mas_XW_stat, 0, 10000);
                //Array.Clear(GlobalVarLn.mas_YW_stat, 0, 10000);
                GlobalVarLn.list_way.Clear();

                GlobalVarLn.iW_stat = 0;
                GlobalVarLn.X_StartW_stat = 0;
                GlobalVarLn.Y_StartW_stat = 0;
                GlobalVarLn.LW_stat = 0;
            // .....................................................................................

            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS

            }
           // -------------------------------------------------------------------------------------
            else
            {
                objFormWay.Show();

                // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
                // Для маршрута
                // НЕ 1-я загрузка формы

                if (objFormWay.chbWay.Checked == true)
                {
                    GlobalVarLn.blWay_stat = true;
                    GlobalVarLn.flEndWay_stat = 1;
                }
                // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS

            }
            // -------------------------------------------------------------------------------------


        } // Button6
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button7: Расчет энергодоступности по зонам 
        // *****************************************************************************************

        private void button7_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSupSpuf = 1;
            ClassMap.F_Close_Form(12);
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormSupSpufG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFSupSpuf = 1;
            //ClassMap.f_RemoveFrm(12);

            // Зона подавления навигации
            if (objFormSupSpuf == null || objFormSupSpuf.IsDisposed)
            {
                // ----------------------------------------------------------------------
                objFormSupSpuf = new FormSupSpuf();
                objFormSupSpuf.Show();

                GlobalVarLn.objFormSupSpufG.cbCenterLSR.SelectedIndex = 0;
                GlobalVarLn.objFormSupSpufG.comboBox2.SelectedIndex = 1;

                GlobalVarLn.objFormSupSpufG.tbOwnHeight.Text = "";
                GlobalVarLn.objFormSupSpufG.tbDistSightRange.Text = "";

                GlobalVarLn.NumbSP_supnav = "";
                // ----------------------------------------------------------------------
                GlobalVarLn.objFormSupSpufG.gbRect.Visible = true;

                //0206
                //GlobalVarLn.objFormSupSpufG.gbRect.Location = new Point(7, 30);
                // ----------------------------------------------------------------------
                // Переменные

                GlobalVarLn.fl_supnav = 0;
                GlobalVarLn.flCoord_supnav = 0; // =1-> Выбрали центр ЗПВ
                // ----------------------------------------------------------------------
                if (GlobalVarLn.listNavigationJammingZone.Count != 0)
                    GlobalVarLn.listNavigationJammingZone.Clear();
                // ----------------------------------------------------------------------


            }
            else
            {
                objFormSupSpuf.Show();
            }


        } // Button7
        // *****************************************************************************************

        // *****************************************************************************************
        //// Обработчик кнопки Button8: Расчет энергодоступности по зонам для УС
        // Spoofing Zone
        // *****************************************************************************************

        private void button8_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSpuf = 1;
            ClassMap.F_Close_Form(13);
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormSpufG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFSpuf = 1;
            //ClassMap.f_RemoveFrm(13);


            if (objFormSpuf == null || objFormSpuf.IsDisposed)
            {

                // ----------------------------------------------------------------------
                objFormSpuf = new FormSpuf();
                objFormSpuf.Show();
                GlobalVarLn.objFormSpufG.cbCenterLSR.SelectedIndex = 0;
                GlobalVarLn.objFormSpufG.comboBox2.SelectedIndex = 1;

                GlobalVarLn.objFormSpufG.tbOwnHeight.Text = "";
                GlobalVarLn.objFormSpufG.tbR1.Text = "";
                GlobalVarLn.objFormSpufG.tbR2.Text = "";

                GlobalVarLn.NumbSP_spf = "";
                // ----------------------------------------------------------------------
                GlobalVarLn.objFormSpufG.gbRect.Visible = true;

                //0206
                //GlobalVarLn.objFormSpufG.gbRect.Location = new Point(7, 30);
                // ----------------------------------------------------------------------
                // Переменные

                GlobalVarLn.fl_spf = 0;
                GlobalVarLn.flCoord_spf = 0; // =1-> Выбрали центр ЗПВ
                // ----------------------------------------------------------------------
                if (GlobalVarLn.listSpoofingZone.Count != 0)
                    GlobalVarLn.listSpoofingZone.Clear();
                if (GlobalVarLn.listSpoofingJamZone.Count != 0)
                    GlobalVarLn.listSpoofingJamZone.Clear();
                // ----------------------------------------------------------------------
                GlobalVarLn.flS_spf = 0;
                GlobalVarLn.flJ_spf = 0;
                GlobalVarLn.objFormSpufG.chbS.Checked = false;
                GlobalVarLn.objFormSpufG.chbJ.Checked = false;
                // ----------------------------------------------------------------------

            }
            else
            {
                objFormSpuf.Show();
            }



        } // Button8
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button9: Самолеты
        // *****************************************************************************************

        //private void button9_Click(object sender, EventArgs e)
        //{

/*
            if (ObjFormAirPlane == null || ObjFormAirPlane.IsDisposed)
            {
                ObjFormAirPlane = new FormAirPlane(ref axaxcMapScreen1);
                ObjFormAirPlane.Show();
            }
            else
            {
                ObjFormAirPlane.Show();
            }
*/

        //} // Button9
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button10: ЗПВ
        // *****************************************************************************************

        private void button10_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormLineSightRange = 1;
            ClassMap.F_Close_Form(10);
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormLineSightRangeG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFLineSightRange = 1;
            //ClassMap.f_RemoveFrm(10);

            if (objFormLineSightRange == null || objFormLineSightRange.IsDisposed)
            {
                GlobalVarLn.objFormLineSightRangeG.WindowState = FormWindowState.Normal;

                // ----------------------------------------------------------------------
                objFormLineSightRange = new FormLineSightRange();
                objFormLineSightRange.Show();
                // ---------------------------------------------------------------------
                GlobalVarLn.objFormLineSightRangeG.cbCenterLSR.SelectedIndex = 0;
                GlobalVarLn.NumbSP_lsr = "";
                // ----------------------------------------------------------------------
                GlobalVarLn.objFormLineSightRangeG.gbRect.Visible = true;

                //0206
                //GlobalVarLn.objFormLineSightRangeG.gbRect.Location = new Point(7, 30);


                GlobalVarLn.objFormLineSightRangeG.gbRect42.Visible = false;
                GlobalVarLn.objFormLineSightRangeG.gbRad.Visible = false;
                GlobalVarLn.objFormLineSightRangeG.gbDegMin.Visible = false;
                GlobalVarLn.objFormLineSightRangeG.gbDegMinSec.Visible = false;
                GlobalVarLn.objFormLineSightRangeG.cbChooseSC.SelectedIndex = 0;
                GlobalVarLn.objFormLineSightRangeG.cbHeightOwnObject.SelectedIndex = 0;
                GlobalVarLn.objFormLineSightRangeG.cbHeightOpponObject.SelectedIndex = 0;
                // ----------------------------------------------------------------------
                GlobalVarLn.objFormLineSightRangeG.chbXY.Checked = false;
                // ----------------------------------------------------------------------
                // Переменные

                GlobalVarLn.fl_LineSightRange = 0;
                GlobalVarLn.flCoordZPV = 0; // =1-> Выбрали центр ЗПВ
                // ----------------------------------------------------------------------
                if (GlobalVarLn.listPointDSR.Count != 0)
                    GlobalVarLn.listPointDSR.Clear();
                // ----------------------------------------------------------------------
            }
            else
            {
                objFormLineSightRange.Show();
            }

            //FormLineSightRange ObjFormLineSightRange = new FormLineSightRange(ref axaxcMapScreen1);
            //ObjFormLineSightRange.Show();

        } // Button10

        // *****************************************************************************************
        // Обработчик кнопки Button11:SP
        // *****************************************************************************************

        private void button11_Click(object sender, EventArgs e)
        {

            //GlobalVarLn.objFormSPG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFSP = 1;
            //ClassMap.f_RemoveFrm(1);

            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSP = 1;
            ClassMap.F_Close_Form(1);

            // 0510_1
            GlobalVarLn.blSP_stat = true;
            GlobalVarLn.flEndSP_stat = 1;
            // -------------------------------------------------------------------------------------


// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// Ручная отладка удаления самолетов

            // -------------------------------------------------------------------------------------

            if (objFormSP == null || objFormSP.IsDisposed)
            {
                objFormSP = new FormSP();
                objFormSP.Show();

                if (GlobalVarLn.flEndTRO_stat != 1)
                {
                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                    // 1-я загрузка формы

                    // .....................................................................................
                    // Очистка dataGridView
                    // .....................................................................................
                    GlobalVarLn.blSP_stat = true;
                    GlobalVarLn.flEndSP_stat = 1;
                    GlobalVarLn.XCenter_SP = 0;
                    GlobalVarLn.YCenter_SP = 0;
                    GlobalVarLn.HCenter_SP = 0;
                    GlobalVarLn.flCoord_SP2 = 0;

                    // seg2
                    GlobalVarLn.ClearListJS();

                    GlobalVarLn.iZSP = 0;
                    //GlobalVarLn.objFormSPG.pbSP.Image = imageList1.Images[0];
                    GlobalVarLn.objFormSPG.pbSP.BackgroundImage = imageList1.Images[0];


                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                }
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormSP.Show();

            }
            // -------------------------------------------------------------------------------------

            // SPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSP

        } // Button11 : SP
        // *****************************************************************************************

        // *****************************************************************************************
        // LF
        // *****************************************************************************************

        private void button13_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormLF = 1;
            ClassMap.F_Close_Form(4);

            // 0510_1
            GlobalVarLn.blLF1_stat = true;
            GlobalVarLn.flEndLF1_stat = 1;
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormLFG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFLF1 = 1;
            //ClassMap.f_RemoveFrm(4);

            if (objFormLF == null || objFormLF.IsDisposed)
            {

                objFormLF = new FormLF();
                objFormLF.Show();

                if (GlobalVarLn.flEndTRO_stat != 1)
                {

                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                    // 1-я загрузка формы

                    // .....................................................................................
                    // Очистка dataGridView

                    objFormLF.dataGridView1.ClearSelection();
                    for (int i = 0; i < GlobalVarLn.sizeDatLF1_stat; i++)
                    {
                        objFormLF.dataGridView1.Rows.Add("", "", "");
                    }
                    // .....................................................................................
                    // Флаги

                    GlobalVarLn.blLF1_stat = true;
                    GlobalVarLn.flEndLF1_stat = 1;
                    // .....................................................................................

                    GlobalVarLn.iLF1_stat = 0;
                    GlobalVarLn.X_LF1 = 0;
                    GlobalVarLn.Y_LF1 = 0;
                    GlobalVarLn.H_LF1 = 0;
                    GlobalVarLn.list_LF1.Clear();
                    // .....................................................................................

                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                }

            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormLF.Show();

            }

        } // LF
        // *****************************************************************************************

        // *****************************************************************************************
        // LF2
        // *****************************************************************************************
        private void button14_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormLF2 = 1;
            ClassMap.F_Close_Form(5);

            // 0510_1
            GlobalVarLn.blLF2_stat = true;
            GlobalVarLn.flEndLF2_stat = 1;
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormLF2G.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFLF2 = 1;
            //ClassMap.f_RemoveFrm(5);

            if (objFormLF2 == null || objFormLF2.IsDisposed)
            {

                objFormLF2 = new FormLF2();
                objFormLF2.Show();

                if (GlobalVarLn.flEndTRO_stat != 1)
                {
                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                    // 1-я загрузка формы

                    // .....................................................................................
                    // Очистка dataGridView

                    objFormLF2.dataGridView1.ClearSelection();
                    for (int i = 0; i < GlobalVarLn.sizeDatLF2_stat; i++)
                    {
                        objFormLF2.dataGridView1.Rows.Add("", "", "");
                    }
                    // .....................................................................................
                    // Флаги

                    GlobalVarLn.blLF2_stat = true;
                    GlobalVarLn.flEndLF2_stat = 1;
                    // .....................................................................................

                    GlobalVarLn.iLF2_stat = 0;
                    GlobalVarLn.X_LF2 = 0;
                    GlobalVarLn.Y_LF2 = 0;
                    GlobalVarLn.H_LF2 = 0;
                    GlobalVarLn.list_LF2.Clear();
                    // .....................................................................................

                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                }
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormLF2.Show();
            }

        } // LF2
        // *****************************************************************************************

        // *****************************************************************************************
        // ZO
        // *****************************************************************************************
        private void button12_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormZO = 1;
            ClassMap.F_Close_Form(6);

            // 0510_1
            GlobalVarLn.blZO_stat = true;
            GlobalVarLn.flEndZO_stat = 1;
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormZOG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFZO = 1;
            //ClassMap.f_RemoveFrm(6);

            if (objFormZO == null || objFormZO.IsDisposed)
            {

                objFormZO = new FormZO();
                objFormZO.Show();

                if (GlobalVarLn.flEndTRO_stat != 1)
                {

                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                    // 1-я загрузка формы

                    // .....................................................................................
                    // Очистка dataGridView

                    objFormZO.dataGridView1.ClearSelection();
                    for (int i = 0; i < GlobalVarLn.sizeDatZO_stat; i++)
                    {
                        objFormZO.dataGridView1.Rows.Add("", "", "");
                    }
                    // .....................................................................................
                    // Флаги

                    GlobalVarLn.blZO_stat = true;
                    GlobalVarLn.flEndZO_stat = 1;
                    // .....................................................................................

                    GlobalVarLn.iZO_stat = 0;
                    GlobalVarLn.X_ZO = 0;
                    GlobalVarLn.Y_ZO = 0;
                    GlobalVarLn.H_ZO = 0;
                    GlobalVarLn.list_ZO.Clear();
                    // .....................................................................................

                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                }

            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormZO.Show();
            }

        }
        // *****************************************************************************************

        // *****************************************************************************************
        // TRO
        // *****************************************************************************************
        private void button1_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormTRO = 1;
            ClassMap.F_Close_Form(7);

            // 0510_1
            GlobalVarLn.blTRO_stat = true;
            GlobalVarLn.flEndTRO_stat = 1;
            GlobalVarLn.blSP_stat = true;
            GlobalVarLn.flEndSP_stat = 1;
            GlobalVarLn.blLF1_stat = true;
            GlobalVarLn.flEndLF1_stat = 1;
            GlobalVarLn.blLF2_stat = true;
            GlobalVarLn.flEndLF2_stat = 1;
            GlobalVarLn.blZO_stat = true;
            GlobalVarLn.flEndZO_stat = 1;
            GlobalVarLn.blOB1_stat = true;
            GlobalVarLn.flEndOB1_stat = 1;
            GlobalVarLn.blOB2_stat = true;
            GlobalVarLn.flEndOB2_stat = 1;
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormTROG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFTRO = 1;
            //ClassMap.f_RemoveFrm(7);

            if (objFormTRO == null || objFormTRO.IsDisposed)
            {

                objFormTRO = new FormTRO();
                objFormTRO.Show();
                // .....................................................................................
                // Очистка TRO

                GlobalVarLn.blTRO_stat = true;
                GlobalVarLn.flEndTRO_stat = 1;
                // .....................................................................................
                // Очистка SP

                GlobalVarLn.blSP_stat = true;
                GlobalVarLn.flEndSP_stat = 1;
                GlobalVarLn.XCenter_SP = 0;
                GlobalVarLn.YCenter_SP = 0;
                GlobalVarLn.HCenter_SP = 0;
                GlobalVarLn.ClearListJS();
                // .....................................................................................
                // Очистка LF1

                objFormLF.dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDatLF1_stat; i++)
                {
                    objFormLF.dataGridView1.Rows.Add("", "", "");
                }
                GlobalVarLn.blLF1_stat = true;
                GlobalVarLn.flEndLF1_stat = 1;
                GlobalVarLn.iLF1_stat = 0;
                GlobalVarLn.X_LF1 = 0;
                GlobalVarLn.Y_LF1 = 0;
                GlobalVarLn.H_LF1 = 0;
                GlobalVarLn.list_LF1.Clear();
                // .....................................................................................
                // Очистка LF2

                objFormLF2.dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDatLF2_stat; i++)
                {
                    objFormLF2.dataGridView1.Rows.Add("", "", "");
                }
                GlobalVarLn.blLF2_stat = true;
                GlobalVarLn.flEndLF2_stat = 1;
                GlobalVarLn.iLF2_stat = 0;
                GlobalVarLn.X_LF2 = 0;
                GlobalVarLn.Y_LF2 = 0;
                GlobalVarLn.H_LF2 = 0;
                GlobalVarLn.list_LF2.Clear();
                // .....................................................................................
                // Очистка ZO

                objFormZO.dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDatZO_stat; i++)
                {
                    objFormZO.dataGridView1.Rows.Add("", "", "");
                }
                GlobalVarLn.blZO_stat = true;
                GlobalVarLn.flEndZO_stat = 1;
                GlobalVarLn.iZO_stat = 0;
                GlobalVarLn.X_ZO = 0;
                GlobalVarLn.Y_ZO = 0;
                GlobalVarLn.H_ZO = 0;
                GlobalVarLn.list_ZO.Clear();
                // .....................................................................................
                // OB1

                GlobalVarLn.blOB1_stat = true;
                GlobalVarLn.flEndOB1_stat = 1;
                GlobalVarLn.iOB1_stat = 0;
                GlobalVarLn.X_OB1 = 0;
                GlobalVarLn.Y_OB1 = 0;
                GlobalVarLn.H_OB1 = 0;
                GlobalVarLn.list_OB1.Clear();
                GlobalVarLn.list1_OB1.Clear();
                GlobalVarLn.objFormOB1G.dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDatOB1_stat; i++)
                {
                    GlobalVarLn.objFormOB1G.dataGridView1.Rows.Add("", "", "", "");
                }
                // -------------------------------------------------------------------
                // OB2

                GlobalVarLn.blOB2_stat = true;
                GlobalVarLn.flEndOB2_stat = 1;
                GlobalVarLn.X_OB2 = 0;
                GlobalVarLn.Y_OB2 = 0;
                GlobalVarLn.H_OB2 = 0;
                GlobalVarLn.list_OB2.Clear();
                GlobalVarLn.list1_OB2.Clear();
                GlobalVarLn.objFormOB2G.dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDatOB2_stat; i++)
                {
                    GlobalVarLn.objFormOB2G.dataGridView1.Rows.Add("", "", "", "");
                }
                // -------------------------------------------------------------------


            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormTRO.Show();
            }

        } // TRO
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button9: OB1
        // *****************************************************************************************

        private void button9_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormOB1 = 1;
            ClassMap.F_Close_Form(2);

            // 0510_1
            GlobalVarLn.blOB1_stat = true;
            GlobalVarLn.flEndOB1_stat = 1;
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormOB1G.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFOB1 = 1;
            //ClassMap.f_RemoveFrm(2);

            /*
                        if (ObjFormAirPlane == null || ObjFormAirPlane.IsDisposed)
                        {
                            ObjFormAirPlane = new FormAirPlane(ref axaxcMapScreen1);
                            ObjFormAirPlane.Show();
                        }
                        else
                        {
                            ObjFormAirPlane.Show();
                        }
            */

            if (objFormOB1 == null || objFormOB1.IsDisposed)
            {

                objFormOB1 = new FormOB1();
                objFormOB1.Show();

                if (GlobalVarLn.flEndTRO_stat != 1)
                {
                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                    // 1-я загрузка формы

                    // -------------------------------------------------------------------
                    // Очистка dataGridView1+ Установка 100 строк
                    // 10_10_2018

                    // Очистка dataGridView1
                    while (objFormOB1.dataGridView1.Rows.Count != 0)
                        objFormOB1.dataGridView1.Rows.Remove(objFormOB1.dataGridView1.Rows[objFormOB1.dataGridView1.Rows.Count - 1]);

                    for (int iyu = 0; iyu < GlobalVarLn.sizeDatOB1_stat; iyu++)
                    {
                        objFormOB1.dataGridView1.Rows.Add("", "", "", "");
                    }
                    // -------------------------------------------------------------------
                    // .....................................................................................
                    // Флаги

                    GlobalVarLn.blOB1_stat = true;
                    GlobalVarLn.flEndOB1_stat = 1;
                    // .....................................................................................

                    GlobalVarLn.iOB1_stat = 0;
                    GlobalVarLn.X_OB1 = 0;
                    GlobalVarLn.Y_OB1 = 0;
                    GlobalVarLn.H_OB1 = 0;
                    GlobalVarLn.list_OB1.Clear();
                    GlobalVarLn.list1_OB1.Clear();
                    GlobalVarLn.iZOB1 = 0;
                    GlobalVarLn.objFormOB1G.pbOB1.BackgroundImage = imageList1.Images[0];
                    //GlobalVarLn.objFormOB1G.pbOB1.Image = imageList1.Images[0];
                    // .....................................................................................

                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                }
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormOB1.Show();
            }

        } // Button9:OB1
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки Button15: OB2
        // *****************************************************************************************
        private void button15_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormOB2 = 1;
            ClassMap.F_Close_Form(3);

            // 0510_1
            GlobalVarLn.blOB2_stat = true;
            GlobalVarLn.flEndOB2_stat = 1;
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormOB2G.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFOB2 = 1;
            //ClassMap.f_RemoveFrm(3);

            if (objFormOB2 == null || objFormOB2.IsDisposed)
            {

                objFormOB2 = new FormOB2();
                objFormOB2.Show();

                if (GlobalVarLn.flEndTRO_stat != 1)
                {
                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                    // 1-я загрузка формы

                    // .....................................................................................
                    // Очистка dataGridView1+ Установка 100 строк
                    // 10_10_2018

                    // Очистка dataGridView1
                    while (objFormOB2.dataGridView1.Rows.Count != 0)
                        objFormOB2.dataGridView1.Rows.Remove(objFormOB2.dataGridView1.Rows[objFormOB2.dataGridView1.Rows.Count - 1]);

                    for (int i = 0; i < GlobalVarLn.sizeDatOB2_stat; i++)
                    {
                        objFormOB2.dataGridView1.Rows.Add("", "", "", "", "");
                    }
                    // .....................................................................................
                    // Флаги

                    GlobalVarLn.blOB2_stat = true;
                    GlobalVarLn.flEndOB2_stat = 1;
                    // .....................................................................................

                    GlobalVarLn.X_OB2 = 0;
                    GlobalVarLn.Y_OB2 = 0;
                    GlobalVarLn.H_OB2 = 0;
                    GlobalVarLn.list_OB2.Clear();
                    GlobalVarLn.list1_OB2.Clear();
                    GlobalVarLn.iZOB2 = 0;
                    //GlobalVarLn.objFormOB2G.pbOB2.Image = imageList1.Images[0];
                    GlobalVarLn.objFormOB2G.pbOB2.BackgroundImage = imageList1.Images[0];
                    // .....................................................................................

                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                }
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormOB2.Show();
            }
        } // OB2
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки : Зона подавления
        // *****************************************************************************************
        private void bZoneSuppressAvia_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSuppression = 1;
            ClassMap.F_Close_Form(11);
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormSuppressionG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFSuppr = 1;
            //ClassMap.f_RemoveFrm(11);


            if (objFormSuppression == null || objFormSuppression.IsDisposed)
            {
                objFormSuppression = new FormSuppression();
                objFormSuppression.Show();

                // ----------------------------------------------------------------------
                GlobalVarLn.objFormSuppressionG.cbOwnObject.SelectedIndex = 0;
                GlobalVarLn.NumbSP_sup = "";
                // ----------------------------------------------------------------------
                GlobalVarLn.objFormSuppressionG.gbOwnRect.Visible = true;

                //0206
                //GlobalVarLn.objFormSuppressionG.gbOwnRect.Location = new Point(8, 26);


                GlobalVarLn.objFormSuppressionG.gbOwnRect42.Visible = false;
                GlobalVarLn.objFormSuppressionG.gbOwnRad.Visible = false;
                GlobalVarLn.objFormSuppressionG.gbOwnDegMin.Visible = false;
                GlobalVarLn.objFormSuppressionG.gbOwnDegMinSec.Visible = false;
                GlobalVarLn.objFormSuppressionG.cbChooseSC.SelectedIndex = 0;
                // ----------------------------------------------------------------------
                GlobalVarLn.objFormSuppressionG.gbPt1Rect.Visible = true;

                //0206
                //GlobalVarLn.objFormSuppressionG.gbPt1Rect.Location = new Point(6, 11);


                GlobalVarLn.objFormSuppressionG.gbPt1Rect42.Visible = false;
                GlobalVarLn.objFormSuppressionG.gbPt1Rad.Visible = false;
                GlobalVarLn.objFormSuppressionG.gbPt1DegMin.Visible = false;
                GlobalVarLn.objFormSuppressionG.gbPt1DegMinSec.Visible = false;
                GlobalVarLn.objFormSuppressionG.cbCommChooseSC.SelectedIndex = 0;
                // ----------------------------------------------------------------------
                // Средство РП

                GlobalVarLn.objFormSuppressionG.cbHeightOwnObject.SelectedIndex = 0;
                // ----------------------------------------------------------------------
                // Object РП

                GlobalVarLn.objFormSuppressionG.cbPt1HeightOwnObject.SelectedIndex = 0;
                // ----------------------------------------------------------------------
                // Поляризация сигнала

                GlobalVarLn.objFormSuppressionG.cbPolarOpponent.SelectedIndex = 0;
                // ----------------------------------------------------------------------
                GlobalVarLn.objFormSuppressionG.chbXY.Checked = false;
                // ----------------------------------------------------------------------
                GlobalVarLn.objFormSuppressionG.chbXY1.Checked = false;
                // ----------------------------------------------------------------------
                // Переменные

                GlobalVarLn.fl_Suppression = 0; // Отрисовка зоны
                GlobalVarLn.flCoordSP_sup = 0; // =1-> Выбрали СП
                GlobalVarLn.flCoordOP = 0;
                GlobalVarLn.flA_sup = 0;
                // ----------------------------------------------------------------------
                if (GlobalVarLn.listControlJammingZone.Count != 0)
                    GlobalVarLn.listControlJammingZone.Clear();
                // ----------------------------------------------------------------------

            }
            else
            {
                objFormSuppression.Show();
            }

        }
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки :Диапазоны частот и секторов радиоразведки
        // *****************************************************************************************
        private void button16_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSPFB = 1;
            ClassMap.F_Close_Form(14);
            // 0510_1
            GlobalVarLn.flF_f1 = 1;

            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormSPFBG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFSPFB = 1;
            //ClassMap.f_RemoveFrm(14);


            if (objFormSPFB == null || objFormSPFB.IsDisposed)
            {

                objFormSPFB = new FormSPFB();
                objFormSPFB.Show();

                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                    // 1-я загрузка формы

                // ----------------------------------------------------------------------
                GlobalVarLn.objFormSPFBG.cbChooseSC.SelectedIndex = 0;
                GlobalVarLn.NumbSP_f1 = Convert.ToString(GlobalVarLn.objFormSPFBG.cbChooseSC.Items[GlobalVarLn.objFormSPFBG.cbChooseSC.SelectedIndex]);
                // ----------------------------------------------------------------------
                // Очистка dataGridView

                GlobalVarLn.objFormSPFBG.dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDat_f1; i++)
                {
                    GlobalVarLn.objFormSPFBG.dataGridView1.Rows.Add("", "", "", "");
                }
                // .......................................................................
                GlobalVarLn.flF_f1 = 1;
                GlobalVarLn.Az1_f1 = 0;
                GlobalVarLn.Az2_f1 = 0;
                GlobalVarLn.Az_f1 = 0;
                GlobalVarLn.flAz1_f1 = 0;
                GlobalVarLn.flAz2_f1 = 0;
                // ......................................................................
                GlobalVarLn.list1_f1.Clear();
                // ----------------------------------------------------------------------

                    // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl


            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormSPFB.Show();
            }

        }
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки :Диапазоны частот и секторов РП
        // *****************************************************************************************
        private void button17_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSPFB1 = 1;
            ClassMap.F_Close_Form(15);
            // 0510_1
            GlobalVarLn.flF_f2 = 1;
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormSPFB1G.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFSPFB1 = 1;
            //ClassMap.f_RemoveFrm(15);

            if (objFormSPFB1 == null || objFormSPFB1.IsDisposed)
            {

                objFormSPFB1 = new FormSPFB1();
                objFormSPFB1.Show();

                // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                // 1-я загрузка формы

                GlobalVarLn.iSP_f2 = 0;
                // ----------------------------------------------------------------------
                GlobalVarLn.objFormSPFB1G.cbChooseSC.SelectedIndex = 0;
                GlobalVarLn.NumbSP_f2 = Convert.ToString(GlobalVarLn.objFormSPFB1G.cbChooseSC.Items[GlobalVarLn.objFormSPFB1G.cbChooseSC.SelectedIndex]);
                // ----------------------------------------------------------------------
                // Очистка dataGridView

                GlobalVarLn.objFormSPFB1G.dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDat_f2; i++)
                {
                    GlobalVarLn.objFormSPFB1G.dataGridView1.Rows.Add("", "", "", "");
                }
                // .......................................................................
                GlobalVarLn.flF_f2 = 1;
                GlobalVarLn.Az1_f2 = 0;
                GlobalVarLn.Az2_f2 = 0;
                GlobalVarLn.Az_f2 = 0;
                GlobalVarLn.flAz1_f2 = 0;
                GlobalVarLn.flAz2_f2 = 0;
                // ......................................................................
                GlobalVarLn.list1_f2.Clear();
                // ----------------------------------------------------------------------

                // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl


            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormSPFB1.Show();
            }

        }
        // *****************************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки : запрещенные частоты
        // *****************************************************************************************
        private void button18_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSPFB2 = 1;
            ClassMap.F_Close_Form(16);

            // 0510_1
            GlobalVarLn.flF_f3 = 1;
            GlobalVarLn.flF1_f3 = 1;
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormSPFB2G.WindowState = FormWindowState.Normal;

            if (objFormSPFB2 == null || objFormSPFB2.IsDisposed)
            {

                objFormSPFB2 = new FormSPFB2(this);
                objFormSPFB2.Show();

                // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                // 1-я загрузка формы

                GlobalVarLn.iSP_f3 = 0;
                // ----------------------------------------------------------------------
                GlobalVarLn.objFormSPFB2G.cbChooseSC.SelectedIndex = 0;
                GlobalVarLn.NumbSP_f3 = Convert.ToString(GlobalVarLn.objFormSPFB2G.cbChooseSC.Items[GlobalVarLn.objFormSPFB2G.cbChooseSC.SelectedIndex]);
                // ----------------------------------------------------------------------
                // Очистка dataGridView

                GlobalVarLn.objFormSPFB2G.dgvFreqForbid.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDat_f3; i++)
                {
                    GlobalVarLn.objFormSPFB2G.dgvFreqForbid.Rows.Add("", "");
                }
                GlobalVarLn.objFormSPFB2G.dgvFreqSpec.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDat_f3; i++)
                {
                    GlobalVarLn.objFormSPFB2G.dgvFreqSpec.Rows.Add("", "");
                }

                // .......................................................................
                GlobalVarLn.flF_f3 = 1;
                GlobalVarLn.flF1_f3 = 1;
                // ......................................................................
                GlobalVarLn.list1_f3.Clear();
                GlobalVarLn.list2_f3.Clear();
                // ----------------------------------------------------------------------

                // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl


            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormSPFB2.Show();
            }
        }
        // *****************************************************************************************


        // *****************************************************************************************
        // Обработчик кнопки : Azimut1
        // *****************************************************************************************
        private void button19_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormAz1 = 1;
            ClassMap.F_Close_Form(9);

            // 0510_1
            GlobalVarLn.blOB_az1 = true;
            GlobalVarLn.flEndOB_az1 = 1;
            // -------------------------------------------------------------------------------------

            //GlobalVarLn.objFormAz1G.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFAz1 = 1;
            //ClassMap.f_RemoveFrm(9);

            if (objFormAz1 == null || objFormAz1.IsDisposed)
            {

                objFormAz1 = new FormAz1();
                objFormAz1.Show();

                // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl
                // 1-я загрузка формы

                // ----------------------------------------------------------------------
                //GlobalVarLn.objFormAz1G.cbChooseSC.SelectedIndex = 0;
                // ----------------------------------------------------------------------
                // Очистка dataGridView

                GlobalVarLn.objFormAz1G.dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDatOB_az1; i++)
                {
                    GlobalVarLn.objFormAz1G.dataGridView1.Rows.Add("","","","");
                }
                // .......................................................................
                GlobalVarLn.blOB_az1 = true;
                GlobalVarLn.flEndOB_az1 = 1;
                GlobalVarLn.flCoordOB_az1 = 0;
                GlobalVarLn.XCenter_az1 = 0;
                GlobalVarLn.YCenter_az1 = 0;
                GlobalVarLn.HCenter_az1 = 0;
                // ----------------------------------------------------------------------
                // clclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclclcl

            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormAz1.Show();
            }

        }
        // *****************************************************************************************





        // *****************************************************************************************
        // Слои
        // *****************************************************************************************

        private void составКартыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //0206
            //if (axaxMapSelDlg.Execute(axaxcMapScreen1.ViewSelect, false) == true)
            //    axaxcMapScreen1.Invalidate();

        } // Слои
        // *****************************************************************************************


        private void cmStripMap_Opening(object sender, CancelEventArgs e)
        {
            ;
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            ;
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

// ********************************************************************************************
// CONNECTION
// sob
       
        private void bConnectPC_Click(object sender, EventArgs e)
        {
            bool blConnect = false;

            //0206
            if (bConnectPC.BackColor == System.Drawing.Color.Red)
            {
                blConnect = true;
                GlobalVarLn.flllsp = 1;
            
            }



            InitConnectionPC(blConnect);
        }

        //1002  INIT CONNECTION
        
        private void InitConnectionPC(bool blConnect)
        {
            String s1;
            int nn;

            if (blConnect)
            {
                if (clientPC != null)
                    clientPC = null;

                // sob
                //clientPC = new ClientPC(3);
                clientPC = new ClientPC((byte)GlobalVarLn.AdressOwn);

                // GPS
                // sob
                clientPC.OnConfirmCoord += clientPC_OnConfirmCoord;
                GlobalVarLn.clientPCG = clientPC;

                // connect
                clientPC.OnConnect += new ClientPC.ConnectEventHandler(ShowConnectPC);

                // disconnect 
                clientPC.OnDisconnect += new ClientPC.ConnectEventHandler(ShowDisconnectPC);

                // sob
                clientPC.OnConfirmReconFWS += new ClientPC.ConfirmReconFWSEventHandler(clientPC_OnConfirmReconFWS);

                s1 = tbIP.Text;
                nn = Convert.ToInt32(tbPort.Text);
                //clientPC.Connect("127.0.0.1", 9101);
                clientPC.Connect(s1, nn);

            }
            else
            {
                clientPC.Disconnect();

                clientPC = null;
                GlobalVarLn.flllsp = 0;

            }
        }

        private void clientPC_OnConfirmReconFWS(object sender, byte bAddress, byte bCodeError, TReconFWS[] tReconFWS)
        {
            if (UpdateTableReconFWS != null)
                UpdateTableReconFWS(tReconFWS);
        }

        /// <summary>
        /// если подключено -- зеленая лампочка
        /// </summary>
        /// <param name="sender"></param>
        private void ShowConnectPC(object sender)
        {
            if (bConnectPC.InvokeRequired)
            {
                //0206
                bConnectPC.Invoke((MethodInvoker)(delegate()
                {
                    bConnectPC.BackColor = System.Drawing.Color.Green;

                }));
 
            }
            else
            {
                ;
                //0206
                bConnectPC.BackColor = System.Drawing.Color.Green;
            }
        }

        /// <summary>
        /// если не подключено -- краскная лампочка
        /// </summary>
        /// <param name="sender"></param>
        private void ShowDisconnectPC(object sender)
        {
            if (bConnectPC.InvokeRequired)
            {
                ;
                //0206
/*
                bConnectPC.Invoke((MethodInvoker)(delegate()
                {
                    bConnectPC.BackColor = Color.Red;
                }));
 */
            }
            else
            {
                //0206
                bConnectPC.BackColor = System.Drawing.Color.Red;
            }
        }

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        private void button2_Click_1(object sender, EventArgs e)
        {

            ;
        }


// ********************************************************************************************

        // CLOSE_MapForm ****************************************************************
        private void MapForm_FormClosing(object sender, FormClosingEventArgs e)
        {

            //0206*
            if (MapIsOpenned == true)
            {
                RasterMapControl.CloseMap();
            }
            if (HeightMatrixIsOpenned == true)
            {
                RasterMapControl.CloseDted();
                HeightMatrixIsOpenned = false;
            }

            iniRW.write_map_scl((int)RasterMapControl.Resolution);



        } // P/P
        // **************************************************************** CLOSE_MapForm

        //0206
/*
        private void MapForm_MouseDown(object sender, MouseEventArgs e)
        {
            ;
        }
*/
        private void button2_Click_2(object sender, EventArgs e)
        {


            //double lt = GlobalVarLn.listJS[0].CurrentPosition.x;
            //double ln = GlobalVarLn.listJS[0].CurrentPosition.y;
            // Расстояние в м на карте -> пикселы на изображении
            //mapPlaneToPicture(GlobalVarLn.hmapl, ref lt, ref ln);
            //axaxcMapScreen1.MapLeft = (int)(lt - axaxcMapScreen1.Width/2);
            //axaxcMapScreen1.MapTop = (int)(ln - axaxcMapScreen1.Height / 2);
            //axaxcMapScreen1.Repaint();

        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {

/*
            double lt = 0;
            double ln = 0;
            lt = GlobalVarLn.listJS[0].CurrentPosition.x;
            ln = GlobalVarLn.listJS[0].CurrentPosition.y;


            if (GlobalVarLn.listJS[0].HasCurrentPosition == false)
            {
                MessageBox.Show("No coordinates");
                return;
            }

            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref lt, ref ln);

            axaxcMapScreen1.MapLeft = (int)(lt - axaxcMapScreen1.Width / 2);
            axaxcMapScreen1.MapTop = (int)(ln - axaxcMapScreen1.Height / 2);

            axaxcMapScreen1.Repaint();
*/


            try
            {
                double lat;
                double lon;
                Mapsui.Geometries.Point g=new Mapsui.Geometries.Point();
                var p = Mercator.ToLonLat(GlobalVarLn.listJS[0].CurrentPosition.x, GlobalVarLn.listJS[0].CurrentPosition.y);
                lat = p.Y;
                lon = p.X;
                g.X = lon;
                g.Y = lat;
                RasterMapControl.NavigateTo(g);
            }
            catch { }

        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {


/*
            double lt = 0;
            double ln = 0;
            lt = GlobalVarLn.listJS[1].CurrentPosition.x;
            ln = GlobalVarLn.listJS[1].CurrentPosition.y;
            if (GlobalVarLn.listJS[1].HasCurrentPosition == false)
            {
                MessageBox.Show("No coordinates");
                return;
            }
            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref lt, ref ln);
            axaxcMapScreen1.MapLeft = (int)(lt - axaxcMapScreen1.Width / 2);
            axaxcMapScreen1.MapTop = (int)(ln - axaxcMapScreen1.Height / 2);
            axaxcMapScreen1.Repaint();
*/

            try
            {
                double lat;
                double lon;
                Mapsui.Geometries.Point g = new Mapsui.Geometries.Point();
                var p = Mercator.ToLonLat(GlobalVarLn.listJS[1].CurrentPosition.x, GlobalVarLn.listJS[1].CurrentPosition.y);
                lat = p.Y;
                lon = p.X;
                g.X = lon;
                g.Y = lat;
                RasterMapControl.NavigateTo(g);
            }
            catch { }

        }

        private void toolStripDropDownButton1_Click(object sender, EventArgs e)
        {
            ;
        }

        //FormLayersSelection FLS;
        private void mapCompositionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //0206
/*
            if (FLS == null || FLS.IsDisposed)
            {
                FLS = new FormLayersSelection(ref axaxcMapScreen1);
                FLS.Show();
            }
            else
                FLS.Show();
*/
        }


        // ************************************************************************************ LENA

 
        // REDRAW_MAP ******************************************************************************
        //0206*
        public static void REDRAW_MAP()
        {
            // _-----------------------------------------------------------------------------------
            // Удалить все
            //0207

            MapForm.RasterMapControl.RemoveAllObjects();
            // _-----------------------------------------------------------------------------------

            // SP

            // otl33
            if (GlobalVarLn.flEndSP_stat == 1)
            {
                GlobalVarLn.objFormSPG.f_SPReDraw();
            }
            // -------------------------------------------------------------------------------------
            // OB1

            if (
                (GlobalVarLn.flEndOB1_stat == 1)
                )
            {
                GlobalVarLn.objFormOB1G.f_OB1ReDraw();
            }
            // -----------------------------------------------------------------------------------
            // OB2

            if (
                (GlobalVarLn.flEndOB2_stat == 1)
                )
            {
                GlobalVarLn.objFormOB2G.f_OB2ReDraw();
            }
            // -----------------------------------------------------------------------------------
            // LF1

            if (
                (GlobalVarLn.flEndLF1_stat == 1)
                )
            {
                GlobalVarLn.objFormLFG.f_LF1ReDraw();

            }
            // -----------------------------------------------------------------------------------
            // LF2

            if (
                (GlobalVarLn.flEndLF2_stat == 1)
                )
            {
                GlobalVarLn.objFormLF2G.f_LF2ReDraw();

            }
            // -----------------------------------------------------------------------------------
            // ZO

            if (
                (GlobalVarLn.flEndZO_stat == 1)
                )
            {
                GlobalVarLn.objFormZOG.f_ZOReDraw();
            }
            // -------------------------------------------------------------------------------------
            // TRO

            if (
                (GlobalVarLn.flEndTRO_stat == 1)
                )
            {
                GlobalVarLn.objFormSPG.f_SPReDraw();
                GlobalVarLn.objFormLFG.f_LF1ReDraw();
                GlobalVarLn.objFormLF2G.f_LF2ReDraw();
                GlobalVarLn.objFormZOG.f_ZOReDraw();
                GlobalVarLn.objFormOB1G.f_OB1ReDraw();
                GlobalVarLn.objFormOB2G.f_OB2ReDraw();

            }
            // -------------------------------------------------------------------------------------
            // Маршрут

            if (GlobalVarLn.flEndWay_stat == 1)
            {
                GlobalVarLn.objFormWayG.f_WayReDraw();

            }
            // -------------------------------------------------------------------------------------
            // Az1

            if (GlobalVarLn.flEndOB_az1 == 1)
            {
                GlobalVarLn.objFormAz1G.f_OBReDraw_Az1();
            }
            // -------------------------------------------------------------------------------------
            // ЗПВ

            if (
                //0209
                (GlobalVarLn.fl_LineSightRange == 1) 
                //(GlobalVarLn.MousePress == false)
                )
            {
                // SP
                //0209
                ClassMap.f_DrawSPRastr(
                              GlobalVarLn.XCenter_ZPV,  // Mercator
                              GlobalVarLn.YCenter_ZPV,
                              ""
                             );

                //0209
                //ClassMap.DrawLSR();
                ClassMap.DrawLSR_Rastr();

                //0210
                ClassMap.DrawPolygon_Rastr1(GlobalVarLn.listDetectionZone);


            }
            // -------------------------------------------------------------------------------------
            //0210
            //Tab

            if (GlobalVarLn.fllTab == 1)
            {
                for (int iitab = 0; iitab < GlobalVarLn.list_air_tab.Count; iitab++)
                {
                    ClassMap.f_DrawAirPlane_Tab_Rastr(GlobalVarLn.list_air_tab[iitab].Lat, GlobalVarLn.list_air_tab[iitab].Long, 0, GlobalVarLn.list_air_tab[iitab].Num);
                }
            }

            // -------------------------------------------------------------------------------------
            // Зона подавления радиолиний управления
            //0210

            if (
                (GlobalVarLn.flCoordSP_sup == 1) 
                //(GlobalVarLn.MousePress == false)
               )
            {
                ClassMap.f_Map_Redraw_Zon_Suppression_Rastr();
            }
            // -------------------------------------------------------------------------------------
            // Зона подавления навигации

            if (
                (GlobalVarLn.fl_supnav == 1) 
                //(GlobalVarLn.MousePress == false)
               )
            {
                // SP
                //0210
                ClassMap.f_DrawSPRastr(
                                  GlobalVarLn.XCenter_supnav,  // Mercator
                                  GlobalVarLn.YCenter_supnav,
                              ""
                             );

                //0206
                ClassMap.DrawPolygon_Rastr5(GlobalVarLn.listNavigationJammingZone);
            }
            // -----------------------------------------------------------------------------------
            // Зона подавления навигации

            if (
                (GlobalVarLn.fl_supnav == 1)
                //(GlobalVarLn.MousePress == false)
               )
            {
                // SP
                //0210
                ClassMap.f_DrawSPRastr(
                                  GlobalVarLn.XCenter_supnav,  // Mercator
                                  GlobalVarLn.YCenter_supnav,
                              ""
                             );

                //0206
                ClassMap.DrawPolygon_Rastr5(GlobalVarLn.listNavigationJammingZone);
            }
            // -------------------------------------------------------------------------------------
            // Зона спуфинга
            // seg1

            // 0510
            if (
                (GlobalVarLn.fl_spf == 1) 
                //(GlobalVarLn.MousePress == false)
               )
            {
                // SP
                ClassMap.f_DrawSPRastr(
                              GlobalVarLn.XCenter_spf,  // m на местности
                              GlobalVarLn.YCenter_spf,
                                  ""
                             );

                if (GlobalVarLn.flS_spf == 1) ;
                //0206
                ClassMap.DrawPolygon_Rastr6(GlobalVarLn.listSpoofingZone);

                if (GlobalVarLn.flJ_spf == 1) ;
                //0206
                ClassMap.DrawPolygon_Rastr5(GlobalVarLn.listSpoofingJamZone);


            }

            // -------------------------------------------------------------------------------------
            // -------------------------------------------------------------------------------------
            // -------------------------------------------------------------------------------------





        }  // REDRAW_MAP
        // ****************************************************************************** REDRAW_MAP


    } // Class
} // namespace
