﻿namespace GrozaMap
{
    partial class FormSpuf
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.tbR2 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.bAccept = new System.Windows.Forms.Button();
            this.lCenterLSR = new System.Windows.Forms.Label();
            this.cbCenterLSR = new System.Windows.Forms.ComboBox();
            this.tbLRad = new System.Windows.Forms.TextBox();
            this.lLRad = new System.Windows.Forms.Label();
            this.tbBRad = new System.Windows.Forms.TextBox();
            this.lBRad = new System.Windows.Forms.Label();
            this.cbChooseSC = new System.Windows.Forms.ComboBox();
            this.lChooseSC = new System.Windows.Forms.Label();
            this.tbYRect = new System.Windows.Forms.TextBox();
            this.lYRect = new System.Windows.Forms.Label();
            this.tbXRect = new System.Windows.Forms.TextBox();
            this.lXRect = new System.Windows.Forms.Label();
            this.gbRect = new System.Windows.Forms.GroupBox();
            this.tbR1 = new System.Windows.Forms.TextBox();
            this.lOwnHeight = new System.Windows.Forms.Label();
            this.tbOwnHeight = new System.Windows.Forms.TextBox();
            this.pCoordPoint = new System.Windows.Forms.Panel();
            this.gbRad = new System.Windows.Forms.GroupBox();
            this.lDistSightRange = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tbOpponentAntenna = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tbHAnt = new System.Windows.Forms.TextBox();
            this.chbS = new System.Windows.Forms.CheckBox();
            this.chbJ = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.gbRect.SuspendLayout();
            this.pCoordPoint.SuspendLayout();
            this.gbRad.SuspendLayout();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(201, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(151, 13);
            this.label4.TabIndex = 204;
            this.label4.Text = "Radius of the jamming zone, m";
            // 
            // tbR2
            // 
            this.tbR2.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbR2.Location = new System.Drawing.Point(350, 136);
            this.tbR2.Name = "tbR2";
            this.tbR2.Size = new System.Drawing.Size(61, 20);
            this.tbR2.TabIndex = 203;
            this.tbR2.TextChanged += new System.EventHandler(this.tbR2_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox2.Location = new System.Drawing.Point(352, 4);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(56, 20);
            this.textBox2.TabIndex = 202;
            this.textBox2.Text = "8";
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "1,5",
            "7"});
            this.comboBox2.Location = new System.Drawing.Point(352, 27);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(56, 21);
            this.comboBox2.TabIndex = 201;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(205, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 13);
            this.label3.TabIndex = 200;
            this.label3.Text = "Gain.....................................";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(204, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 13);
            this.label2.TabIndex = 199;
            this.label2.Text = "Power, W (0.1-15) ...............";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(204, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(150, 13);
            this.label1.TabIndex = 198;
            this.label1.Text = "Spoofing coefficient, dB (7-15)";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox1.Location = new System.Drawing.Point(352, 54);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(56, 20);
            this.textBox1.TabIndex = 197;
            this.textBox1.Text = "10";
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button1.Location = new System.Drawing.Point(190, 160);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(102, 23);
            this.button1.TabIndex = 196;
            this.button1.Text = "Center of the zone";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // bClear
            // 
            this.bClear.Location = new System.Drawing.Point(365, 160);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(42, 23);
            this.bClear.TabIndex = 195;
            this.bClear.Text = "Clear";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // bAccept
            // 
            this.bAccept.Location = new System.Drawing.Point(301, 160);
            this.bAccept.Name = "bAccept";
            this.bAccept.Size = new System.Drawing.Size(54, 23);
            this.bAccept.TabIndex = 194;
            this.bAccept.Text = "Accept";
            this.bAccept.UseVisualStyleBackColor = true;
            this.bAccept.Click += new System.EventHandler(this.bAccept_Click);
            // 
            // lCenterLSR
            // 
            this.lCenterLSR.AutoSize = true;
            this.lCenterLSR.Location = new System.Drawing.Point(2, 8);
            this.lCenterLSR.Name = "lCenterLSR";
            this.lCenterLSR.Size = new System.Drawing.Size(94, 13);
            this.lCenterLSR.TabIndex = 188;
            this.lCenterLSR.Text = "Center of the zone";
            // 
            // cbCenterLSR
            // 
            this.cbCenterLSR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCenterLSR.FormattingEnabled = true;
            this.cbCenterLSR.Items.AddRange(new object[] {
            "X,Y",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""});
            this.cbCenterLSR.Location = new System.Drawing.Point(114, 3);
            this.cbCenterLSR.Name = "cbCenterLSR";
            this.cbCenterLSR.Size = new System.Drawing.Size(65, 21);
            this.cbCenterLSR.TabIndex = 187;
            this.cbCenterLSR.SelectedIndexChanged += new System.EventHandler(this.cbCenterLSR_SelectedIndexChanged);
            // 
            // tbLRad
            // 
            this.tbLRad.Location = new System.Drawing.Point(78, 36);
            this.tbLRad.MaxLength = 10;
            this.tbLRad.Name = "tbLRad";
            this.tbLRad.Size = new System.Drawing.Size(75, 20);
            this.tbLRad.TabIndex = 5;
            // 
            // lLRad
            // 
            this.lLRad.AutoSize = true;
            this.lLRad.Location = new System.Drawing.Point(4, 41);
            this.lLRad.Name = "lLRad";
            this.lLRad.Size = new System.Drawing.Size(94, 13);
            this.lLRad.TabIndex = 6;
            this.lLRad.Text = "L, рад...................";
            // 
            // tbBRad
            // 
            this.tbBRad.Location = new System.Drawing.Point(78, 13);
            this.tbBRad.MaxLength = 10;
            this.tbBRad.Name = "tbBRad";
            this.tbBRad.Size = new System.Drawing.Size(75, 20);
            this.tbBRad.TabIndex = 4;
            // 
            // lBRad
            // 
            this.lBRad.AutoSize = true;
            this.lBRad.Location = new System.Drawing.Point(4, 18);
            this.lBRad.Name = "lBRad";
            this.lBRad.Size = new System.Drawing.Size(95, 13);
            this.lBRad.TabIndex = 3;
            this.lBRad.Text = "B, рад...................";
            // 
            // cbChooseSC
            // 
            this.cbChooseSC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChooseSC.DropDownWidth = 150;
            this.cbChooseSC.FormattingEnabled = true;
            this.cbChooseSC.Items.AddRange(new object[] {
            "Метры на мест-ти",
            "Радианы",
            "Градусы",
            "Град, мин, сек"});
            this.cbChooseSC.Location = new System.Drawing.Point(133, 314);
            this.cbChooseSC.Name = "cbChooseSC";
            this.cbChooseSC.Size = new System.Drawing.Size(123, 21);
            this.cbChooseSC.TabIndex = 11;
            this.cbChooseSC.Visible = false;
            // 
            // lChooseSC
            // 
            this.lChooseSC.AutoSize = true;
            this.lChooseSC.Location = new System.Drawing.Point(71, 335);
            this.lChooseSC.Name = "lChooseSC";
            this.lChooseSC.Size = new System.Drawing.Size(36, 13);
            this.lChooseSC.TabIndex = 12;
            this.lChooseSC.Text = "СК.....";
            this.lChooseSC.Visible = false;
            // 
            // tbYRect
            // 
            this.tbYRect.BackColor = System.Drawing.SystemColors.Window;
            this.tbYRect.Location = new System.Drawing.Point(78, 36);
            this.tbYRect.MaxLength = 7;
            this.tbYRect.Name = "tbYRect";
            this.tbYRect.ReadOnly = true;
            this.tbYRect.Size = new System.Drawing.Size(75, 20);
            this.tbYRect.TabIndex = 5;
            // 
            // lYRect
            // 
            this.lYRect.AutoSize = true;
            this.lYRect.Location = new System.Drawing.Point(4, 42);
            this.lYRect.Name = "lYRect";
            this.lYRect.Size = new System.Drawing.Size(73, 13);
            this.lYRect.TabIndex = 6;
            this.lYRect.Text = "Long, deg......";
            // 
            // tbXRect
            // 
            this.tbXRect.BackColor = System.Drawing.SystemColors.Window;
            this.tbXRect.Location = new System.Drawing.Point(78, 13);
            this.tbXRect.MaxLength = 7;
            this.tbXRect.Name = "tbXRect";
            this.tbXRect.ReadOnly = true;
            this.tbXRect.Size = new System.Drawing.Size(75, 20);
            this.tbXRect.TabIndex = 4;
            // 
            // lXRect
            // 
            this.lXRect.AutoSize = true;
            this.lXRect.Location = new System.Drawing.Point(4, 20);
            this.lXRect.Name = "lXRect";
            this.lXRect.Size = new System.Drawing.Size(73, 13);
            this.lXRect.TabIndex = 3;
            this.lXRect.Text = "Lat, deg.........";
            // 
            // gbRect
            // 
            this.gbRect.Controls.Add(this.tbYRect);
            this.gbRect.Controls.Add(this.lYRect);
            this.gbRect.Controls.Add(this.tbXRect);
            this.gbRect.Controls.Add(this.lXRect);
            this.gbRect.Location = new System.Drawing.Point(7, 30);
            this.gbRect.Name = "gbRect";
            this.gbRect.Size = new System.Drawing.Size(160, 63);
            this.gbRect.TabIndex = 17;
            this.gbRect.TabStop = false;
            this.gbRect.Visible = false;
            // 
            // tbR1
            // 
            this.tbR1.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbR1.Location = new System.Drawing.Point(148, 136);
            this.tbR1.Name = "tbR1";
            this.tbR1.Size = new System.Drawing.Size(54, 20);
            this.tbR1.TabIndex = 192;
            // 
            // lOwnHeight
            // 
            this.lOwnHeight.AutoSize = true;
            this.lOwnHeight.Location = new System.Drawing.Point(10, 10);
            this.lOwnHeight.Name = "lOwnHeight";
            this.lOwnHeight.Size = new System.Drawing.Size(77, 13);
            this.lOwnHeight.TabIndex = 191;
            this.lOwnHeight.Text = "H, m................";
            // 
            // tbOwnHeight
            // 
            this.tbOwnHeight.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbOwnHeight.Location = new System.Drawing.Point(89, 9);
            this.tbOwnHeight.Name = "tbOwnHeight";
            this.tbOwnHeight.ReadOnly = true;
            this.tbOwnHeight.Size = new System.Drawing.Size(72, 20);
            this.tbOwnHeight.TabIndex = 190;
            // 
            // pCoordPoint
            // 
            this.pCoordPoint.Controls.Add(this.gbRect);
            this.pCoordPoint.Controls.Add(this.gbRad);
            this.pCoordPoint.Controls.Add(this.tbOwnHeight);
            this.pCoordPoint.Controls.Add(this.lOwnHeight);
            this.pCoordPoint.Location = new System.Drawing.Point(3, 29);
            this.pCoordPoint.Name = "pCoordPoint";
            this.pCoordPoint.Size = new System.Drawing.Size(173, 103);
            this.pCoordPoint.TabIndex = 189;
            // 
            // gbRad
            // 
            this.gbRad.Controls.Add(this.tbLRad);
            this.gbRad.Controls.Add(this.lLRad);
            this.gbRad.Controls.Add(this.tbBRad);
            this.gbRad.Controls.Add(this.lBRad);
            this.gbRad.Location = new System.Drawing.Point(1, 37);
            this.gbRad.Name = "gbRad";
            this.gbRad.Size = new System.Drawing.Size(160, 63);
            this.gbRad.TabIndex = 28;
            this.gbRad.TabStop = false;
            this.gbRad.Visible = false;
            // 
            // lDistSightRange
            // 
            this.lDistSightRange.AutoSize = true;
            this.lDistSightRange.Location = new System.Drawing.Point(-3, 140);
            this.lDistSightRange.Name = "lDistSightRange";
            this.lDistSightRange.Size = new System.Drawing.Size(153, 13);
            this.lDistSightRange.TabIndex = 193;
            this.lDistSightRange.Text = "Radius of the spoofing zone, m";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(209, 107);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(135, 13);
            this.label11.TabIndex = 208;
            this.label11.Text = "Antenna height, m..............";
            // 
            // tbOpponentAntenna
            // 
            this.tbOpponentAntenna.Location = new System.Drawing.Point(351, 104);
            this.tbOpponentAntenna.Name = "tbOpponentAntenna";
            this.tbOpponentAntenna.Size = new System.Drawing.Size(56, 20);
            this.tbOpponentAntenna.TabIndex = 207;
            this.tbOpponentAntenna.Text = "2";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(208, 81);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(138, 13);
            this.label17.TabIndex = 206;
            this.label17.Text = "Antenna (JS) height, m........";
            // 
            // tbHAnt
            // 
            this.tbHAnt.Location = new System.Drawing.Point(352, 78);
            this.tbHAnt.Name = "tbHAnt";
            this.tbHAnt.Size = new System.Drawing.Size(56, 20);
            this.tbHAnt.TabIndex = 205;
            this.tbHAnt.Text = "8";
            // 
            // chbS
            // 
            this.chbS.AutoSize = true;
            this.chbS.Location = new System.Drawing.Point(82, 382);
            this.chbS.Name = "chbS";
            this.chbS.Size = new System.Drawing.Size(15, 14);
            this.chbS.TabIndex = 209;
            this.chbS.UseVisualStyleBackColor = true;
            this.chbS.Visible = false;
            this.chbS.CheckedChanged += new System.EventHandler(this.chbS_CheckedChanged);
            // 
            // chbJ
            // 
            this.chbJ.AutoSize = true;
            this.chbJ.Location = new System.Drawing.Point(82, 402);
            this.chbJ.Name = "chbJ";
            this.chbJ.Size = new System.Drawing.Size(15, 14);
            this.chbJ.TabIndex = 210;
            this.chbJ.UseVisualStyleBackColor = true;
            this.chbJ.Visible = false;
            this.chbJ.CheckedChanged += new System.EventHandler(this.chbJ_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(111, 382);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 13);
            this.label5.TabIndex = 211;
            this.label5.Text = "On/Off spoofing";
            this.label5.Visible = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(111, 402);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 13);
            this.label6.TabIndex = 212;
            this.label6.Text = "On/Off jamming";
            this.label6.Visible = false;
            // 
            // FormSpuf
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(413, 189);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.chbJ);
            this.Controls.Add(this.chbS);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.tbOpponentAntenna);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.tbHAnt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lChooseSC);
            this.Controls.Add(this.cbChooseSC);
            this.Controls.Add(this.tbR2);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.bAccept);
            this.Controls.Add(this.lCenterLSR);
            this.Controls.Add(this.cbCenterLSR);
            this.Controls.Add(this.tbR1);
            this.Controls.Add(this.pCoordPoint);
            this.Controls.Add(this.lDistSightRange);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(900, 100);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSpuf";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Spoofing zone";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.FormSpuf_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSpuf_FormClosing);
            this.Load += new System.EventHandler(this.FormSpuf_Load);
            this.gbRect.ResumeLayout(false);
            this.gbRect.PerformLayout();
            this.pCoordPoint.ResumeLayout(false);
            this.pCoordPoint.PerformLayout();
            this.gbRad.ResumeLayout(false);
            this.gbRad.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox tbR2;
        private System.Windows.Forms.TextBox textBox2;
        public System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button bClear;
        private System.Windows.Forms.Button bAccept;
        private System.Windows.Forms.Label lCenterLSR;
        public System.Windows.Forms.ComboBox cbCenterLSR;
        public System.Windows.Forms.TextBox tbLRad;
        private System.Windows.Forms.Label lLRad;
        public System.Windows.Forms.TextBox tbBRad;
        private System.Windows.Forms.Label lBRad;
        public System.Windows.Forms.ComboBox cbChooseSC;
        private System.Windows.Forms.Label lChooseSC;
        public System.Windows.Forms.TextBox tbYRect;
        private System.Windows.Forms.Label lYRect;
        public System.Windows.Forms.TextBox tbXRect;
        private System.Windows.Forms.Label lXRect;
        public System.Windows.Forms.GroupBox gbRect;
        public System.Windows.Forms.TextBox tbR1;
        private System.Windows.Forms.Label lOwnHeight;
        public System.Windows.Forms.TextBox tbOwnHeight;
        private System.Windows.Forms.Panel pCoordPoint;
        public System.Windows.Forms.GroupBox gbRad;
        private System.Windows.Forms.Label lDistSightRange;
        private System.Windows.Forms.Label label11;
        public System.Windows.Forms.TextBox tbOpponentAntenna;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.TextBox tbHAnt;
        public System.Windows.Forms.CheckBox chbS;
        public System.Windows.Forms.CheckBox chbJ;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}