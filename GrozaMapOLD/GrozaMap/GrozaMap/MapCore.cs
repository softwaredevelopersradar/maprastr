﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

//using AxaxGisToolKit;
//using axGisToolKit;

using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Input;


namespace GrozaMap
{
    public static class MapCore
    {

/*
        [DllImport("gisacces.dll")]
        public static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern Int32 mapOpenMtrForMap(int hmap, String mtrname, Int32 mode);
        [DllImport("gisacces.dll")]
        static extern void mapCloseMtr(int hmap, Int32 number);
*/

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);



        //private static axMapPoint axMapPoint1 = new axMapPoint();
        //private static axMapPoint coordForHeight = new axMapPoint();

        public struct RadioSource
        {
            public int id;

            public double geoX;
            public double geoY;

            public double planeX;
            public double planeY;

            public double pictureX;
            public double pictureY;

            public double[] latitude;
            public double[] longitude;

            public bool isinfopaint;

            public RadioSource(double planeX, double planeY)
            {
                id = 0;

                geoX = 0;
                geoY = 0;

                this.planeX = planeX;
                this.planeY = planeY;

                pictureX = 0;
                pictureY = 0;

                latitude = null;
                longitude = null;

                isinfopaint = false;
            }

            public void RadioSourceSetPlane(double planeX, double planeY)
            {
                this.planeX = planeX;
                this.planeY = planeY;
            }

            public void RadioSourceSetPicture(double pictureX, double pictureY)
            {
                this.pictureX = pictureX;
                this.pictureY = pictureY;
            }

            public void RadioSourceSetLatLon(double[] latitude, double[] longitude)
            {
                this.latitude = latitude;
                this.longitude = longitude;
            }

            public void RadioSourceSetBoolInfo(bool isinfopaint)
            {
                this.isinfopaint = isinfopaint;
            }

        };

        public struct Station
        {
            public int id;

            public double geoX;
            public double geoY;

            public double planeX;
            public double planeY;

            public double pictureX;
            public double pictureY;

            public Station(double planeX, double planeY)
            {
                id = 0;

                geoX = 0;
                geoY = 0;

                this.planeX = planeX;
                this.planeY = planeY;

                pictureX = 0;
                pictureY = 0;
            }

            public void StationSetPlane(double planeX, double planeY)
            {
                this.planeX = planeX;
                this.planeY = planeY;
            }

            public void StationSetPicture(double pictureX, double pictureY)
            {
                this.pictureX = pictureX;
                this.pictureY = pictureY;
            }


        };

        public struct ArcPoint
        {

        };

        public struct Coordinates
        {
            public double X;
            public double Y;

            public Coordinates(double x, double y)
            {
                X = x;
                Y = y;
            }
        }


        public static void mapGeoToRealGeo(ref double geoX, ref double geoY)
        {
            geoX = geoX * 180 / Math.PI;
            geoY = geoY * 180 / Math.PI;
        }

        public static void realGeoToMapGeo(ref double geoX, ref double geoY)
        {
            geoX = geoX / 180 * Math.PI;
            geoY = geoY / 180 * Math.PI;
        }

/*
        public static Coordinates MapPlaneToRealGeo(this AxaxcMapScreen map, double x, double y)
        {
            var tempX = x;
            var tempY = y;
            mapPlaneToGeo(map.MapHandle, ref tempX, ref tempY);
            mapGeoToRealGeo(ref tempX, ref tempY);
            return new Coordinates(tempX, tempY);
        }

        public static Coordinates MapRealToPlaneGeo(this AxaxcMapScreen map, double x, double y)
        {
            var tempX = x;
            var tempY = y;
            realGeoToMapGeo(ref tempX, ref tempY);
            mapGeoToPlane(map.MapHandle, ref tempX, ref tempY);
            return new Coordinates(tempX, tempY);
        }

        public static void MapPlaneToRealGeo(this AxaxcMapScreen map, ref double x, ref double y)
        {
            mapPlaneToGeo(map.MapHandle, ref x, ref y);
            mapGeoToRealGeo(ref x, ref y);
        }

        public static void MapRealToPlaneGeo(this AxaxcMapScreen map, ref double x, ref double y)
        {
            realGeoToMapGeo(ref x, ref y);
            mapGeoToPlane(map.MapHandle, ref x, ref y);
        }
*/
/*
        //открыть карту
        public static void openMapFunc(ref bool MapIsOpenned, ref AxaxcMapScreen axaxcMapScreen1, ref  AxaxOpenMapDialog axaxOpenMapDialog1, string PathandNameINI)
        {

            axaxOpenMapDialog1.Filter = "Map file|*.map|All files|*.*";
            if (axaxOpenMapDialog1.Execute)
                MapIsOpenned = axaxcMapScreen1.MapOpen(axaxOpenMapDialog1.FileName, true);
            int hmap = (int)axaxcMapScreen1.MapHandle;
            Graphics graph = axaxcMapScreen1.CreateGraphics();
            axaxcMapScreen1.ViewScale = axaxcMapScreen1.MapScale;
            StringBuilder temp = new StringBuilder(255);
            coordForHeight.cMapView = axMapPoint1.cMapView = axaxcMapScreen1.C_CONTAINER;
            coordForHeight.PlaceInp = axMapPoint1.PlaceInp = TxPPLACE.PP_PLANE;
            coordForHeight.PlaceOut = axMapPoint1.PlaceOut = TxPPLACE.PP_PLANE;

            GetPrivateProfileString("Position", "Scale", "1250000", temp, 255, PathandNameINI);
            int sc = int.Parse(temp.ToString());

            if (sc > 1250000) axaxcMapScreen1.ViewScale = 1250000;
            else axaxcMapScreen1.ViewScale = sc;

            GetPrivateProfileString("Position", "MapLeft", "0", temp, 255, PathandNameINI);
            axaxcMapScreen1.MapLeft = int.Parse(temp.ToString());
            GetPrivateProfileString("Position", "MapTop", "0", temp, 255, PathandNameINI);
            axaxcMapScreen1.MapTop = int.Parse(temp.ToString());

            axaxcMapScreen1.Selecting = true;

            axaxcMapScreen1.Repaint();
 
        }
 */




        // otl55
        // Меню
/*
        public static void openMapFunc(ref bool MapIsOpenned, ref AxaxcMapScreen axaxcMapScreen1, ref  OpenFileDialog openFileDialog1, string PathandNameINI)
        {

            openFileDialog1.FileName = ".map";
            openFileDialog1.Filter = "Map file|*.map|All files|*.*";
            openFileDialog1.InitialDirectory = Application.StartupPath;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {

                MapIsOpenned = axaxcMapScreen1.MapOpen(openFileDialog1.FileName, true);
                int hmap = (int)axaxcMapScreen1.MapHandle;
                Graphics graph = axaxcMapScreen1.CreateGraphics();
                axaxcMapScreen1.ViewScale = axaxcMapScreen1.MapScale;
                StringBuilder temp = new StringBuilder(255);
                coordForHeight.cMapView = axMapPoint1.cMapView = axaxcMapScreen1.C_CONTAINER;
                coordForHeight.PlaceInp = axMapPoint1.PlaceInp = TxPPLACE.PP_PLANE;
                coordForHeight.PlaceOut = axMapPoint1.PlaceOut = TxPPLACE.PP_PLANE;


                GetPrivateProfileString("Position", "Scale", "1250000", temp, 255, PathandNameINI);
                axaxcMapScreen1.ViewScale = int.Parse(temp.ToString());
                GetPrivateProfileString("Position", "MapLeft", "0", temp, 255, PathandNameINI);
                axaxcMapScreen1.MapLeft = int.Parse(temp.ToString());
                GetPrivateProfileString("Position", "MapTop", "0", temp, 255, PathandNameINI);
                axaxcMapScreen1.MapTop = int.Parse(temp.ToString());

                axaxcMapScreen1.Selecting = true;

                axaxcMapScreen1.Repaint();
            }
            else
            {
                MapIsOpenned = false;
            }
        }
 */ 

/*
        // otl55
        // Загрузка
        public static void openMapFunc(ref bool MapIsOpenned, ref AxaxcMapScreen axaxcMapScreen1, string PathandNameMAP, string PathandNameINI)
        {

            MapIsOpenned = axaxcMapScreen1.MapOpen(PathandNameMAP, true);
            int hmap = (int)axaxcMapScreen1.MapHandle;
            Graphics graph = axaxcMapScreen1.CreateGraphics();
            axaxcMapScreen1.ViewScale = axaxcMapScreen1.MapScale;
            StringBuilder temp = new StringBuilder(255);
            coordForHeight.cMapView = axMapPoint1.cMapView = axaxcMapScreen1.C_CONTAINER;
            coordForHeight.PlaceInp = axMapPoint1.PlaceInp = TxPPLACE.PP_PLANE;
            coordForHeight.PlaceOut = axMapPoint1.PlaceOut = TxPPLACE.PP_PLANE;

            GetPrivateProfileString("Position", "Scale", "1250000", temp, 255, PathandNameINI);
            axaxcMapScreen1.ViewScale = int.Parse(temp.ToString());
            GetPrivateProfileString("Position", "MapLeft", "0", temp, 255, PathandNameINI);
            axaxcMapScreen1.MapLeft = int.Parse(temp.ToString());
            GetPrivateProfileString("Position", "MapTop", "0", temp, 255, PathandNameINI);
            axaxcMapScreen1.MapTop = int.Parse(temp.ToString());

            axaxcMapScreen1.Selecting = true;

            axaxcMapScreen1.Repaint();
  
        }
*/
  
/*  
        public static void openMapFuncLn(ref bool MapIsOpenned, ref AxaxcMapScreen axaxcMapScreen1, string PathandNameMAP, string PathandNameINI)
        {

            MapIsOpenned = axaxcMapScreen1.MapOpen(PathandNameMAP, true);

            int hmap = (int)axaxcMapScreen1.MapHandle;
            Graphics graph = axaxcMapScreen1.CreateGraphics();
            axaxcMapScreen1.ViewScale = axaxcMapScreen1.MapScale;
            StringBuilder temp = new StringBuilder(255);
            coordForHeight.cMapView = axMapPoint1.cMapView = axaxcMapScreen1.C_CONTAINER;
            coordForHeight.PlaceInp = axMapPoint1.PlaceInp = TxPPLACE.PP_PLANE;
            coordForHeight.PlaceOut = axMapPoint1.PlaceOut = TxPPLACE.PP_PLANE;

            GetPrivateProfileString("Position", "Scale", "1250000", temp, 255, PathandNameINI);
            axaxcMapScreen1.ViewScale = int.Parse(temp.ToString());
            //int sc = int.Parse(temp.ToString());

            //if (sc > 1250000) axaxcMapScreen1.ViewScale = 1250000;
            //else axaxcMapScreen1.ViewScale = sc;

            GetPrivateProfileString("Position", "MapLeft", "0", temp, 255, PathandNameINI);
            axaxcMapScreen1.MapLeft = int.Parse(temp.ToString());
            GetPrivateProfileString("Position", "MapTop", "0", temp, 255, PathandNameINI);
            axaxcMapScreen1.MapTop = int.Parse(temp.ToString());

            axaxcMapScreen1.Selecting = true;

            axaxcMapScreen1.Repaint();
 
        }
*/


/*
        //другие варианты открытия карты
        public static void openMapFunc(ref bool MapIsOpenned, ref AxaxcMapScreen axaxcMapScreen1, string FilePathandName)
        {

            MapIsOpenned = axaxcMapScreen1.MapOpen(FilePathandName, true);

            int hmap = (int)axaxcMapScreen1.MapHandle;
            Graphics graph = axaxcMapScreen1.CreateGraphics();
            axaxcMapScreen1.ViewScale = axaxcMapScreen1.MapScale;
            StringBuilder temp = new StringBuilder(255);
            coordForHeight.cMapView = axMapPoint1.cMapView = axaxcMapScreen1.C_CONTAINER;
            coordForHeight.PlaceInp = axMapPoint1.PlaceInp = TxPPLACE.PP_PLANE;
            coordForHeight.PlaceOut = axMapPoint1.PlaceOut = TxPPLACE.PP_PLANE;

            axaxcMapScreen1.Selecting = true;

            axaxcMapScreen1.Repaint();
  
        }
*/
/*
        public static void openMapFunc(ref bool MapIsOpenned, ref AxaxcMapScreen axaxcMapScreen1, ref  AxaxOpenMapDialog axaxOpenMapDialog1)
        {
            axaxOpenMapDialog1.Filter = "Map file|*.map|All files|*.*";
            if (axaxOpenMapDialog1.Execute)
                MapIsOpenned = axaxcMapScreen1.MapOpen(axaxOpenMapDialog1.FileName, true);

            int hmap = (int)axaxcMapScreen1.MapHandle;
            Graphics graph = axaxcMapScreen1.CreateGraphics();
            axaxcMapScreen1.ViewScale = axaxcMapScreen1.MapScale;
            StringBuilder temp = new StringBuilder(255);
            coordForHeight.cMapView = axMapPoint1.cMapView = axaxcMapScreen1.C_CONTAINER;
            coordForHeight.PlaceInp = axMapPoint1.PlaceInp = TxPPLACE.PP_PLANE;
            coordForHeight.PlaceOut = axMapPoint1.PlaceOut = TxPPLACE.PP_PLANE;

            axaxcMapScreen1.Selecting = true;

            axaxcMapScreen1.Repaint();
  
        }
*/
/*
        public static void openMapFunc(ref bool MapIsOpenned, ref AxaxcMapScreen axaxcMapScreen1, ref  OpenFileDialog openFileDialog1)
        {

            openFileDialog1.FileName = ".map";
            openFileDialog1.Filter = "Map file|*.map|All files|*.*";
            openFileDialog1.InitialDirectory = Application.StartupPath;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                MapIsOpenned = axaxcMapScreen1.MapOpen(openFileDialog1.FileName, true);
                int hmap = (int)axaxcMapScreen1.MapHandle;
                Graphics graph = axaxcMapScreen1.CreateGraphics();
                axaxcMapScreen1.ViewScale = axaxcMapScreen1.MapScale;
                StringBuilder temp = new StringBuilder(255);

                coordForHeight.cMapView = axMapPoint1.cMapView = axaxcMapScreen1.C_CONTAINER;
                coordForHeight.PlaceInp = axMapPoint1.PlaceInp = TxPPLACE.PP_PLANE;
                coordForHeight.PlaceOut = axMapPoint1.PlaceOut = TxPPLACE.PP_PLANE;

                axaxcMapScreen1.Selecting = true;

                axaxcMapScreen1.Repaint();
            }
            else
            {
                MapIsOpenned = false;
            }
  
        }

*/

/*
        //закрыть карту
        public static void closeMapFunc(ref bool MapIsOpenned, ref AxaxcMapScreen axaxcMapScreen1, string PathandNameINI)
        {

            WritePrivateProfileString("Map", "Path", "", PathandNameINI);
            WritePrivateProfileString("Position", "Scale", axaxcMapScreen1.ViewScale.ToString(), PathandNameINI);
            WritePrivateProfileString("Position", "MapLeft", axaxcMapScreen1.MapLeft.ToString(), PathandNameINI);
            WritePrivateProfileString("Position", "MapTop", axaxcMapScreen1.MapTop.ToString(), PathandNameINI);

            axaxcMapScreen1.MapClose();
            MapIsOpenned = false;
 
        }
*/
/*
        //открыть матрицу высот
        public static void openMatrixFunc(ref bool HeightMatrixIsOpenned, ref AxaxcMapScreen axaxcMapScreen1, ref OpenFileDialog openFileDialog1)
        {

            openFileDialog1.FileName = ".mtw";
            openFileDialog1.Filter = "Matrix file|*.mtw|All files|*.*";
            openFileDialog1.InitialDirectory = Application.StartupPath;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                mapOpenMtrForMap((int)axaxcMapScreen1.MapHandle, openFileDialog1.FileName, 0);
                HeightMatrixIsOpenned = true;
                axaxcMapScreen1.Repaint();
            }
            else
            {
                HeightMatrixIsOpenned = false;
            }

        }
 */
/*
        public static void openMatrixFunc(ref bool HeightMatrixIsOpenned, ref AxaxcMapScreen axaxcMapScreen1, string PathandNameMTW)
        {

            try
            {
                mapOpenMtrForMap((int)axaxcMapScreen1.MapHandle, PathandNameMTW, 0);
                HeightMatrixIsOpenned = true;
                axaxcMapScreen1.Repaint();
            }
            catch
            {
                HeightMatrixIsOpenned = false;
            }

        }
*/
/*
        //закрыть матрицу высот
        public static void closeMatrixFunc(ref bool HeightMatrixIsOpenned, ref AxaxcMapScreen axaxcMapScreen1)
        {
            mapCloseMtr((int)axaxcMapScreen1.MapHandle, 0);
            HeightMatrixIsOpenned = false;
            axaxcMapScreen1.Repaint();
        }
*/

/*
        //убрать лишние слои
        public static void uselessLayersOff(ref AxaxcMapScreen axaxcMapScreen1)
        
        {
             
            axaxcMapScreen1.Selecting = true;
            axaxcMapScreen1.ViewSelect.Layers_set(2, false); //непонятно
            axaxcMapScreen1.Repaint();
        }
*/
/*
        //Убрать/добавить слои
        public static void layersMapFunc(ref AxaxcMapScreen axaxcMapScreen1, bool Value)
        {
              axaxcMapScreen1.ViewSelect.Layers_set(16, Value); //хрень убрать
              axaxcMapScreen1.ViewSelect.Layers_set(3, false); //высотность рельефа оставить
               axaxcMapScreen1.Repaint();
        }
*/

/*
        //увеличить масштаб
        public static void ZoomInFunc(ref AxaxcMapScreen axaxcMapScreen1)
        {

            int sc = (int)(axaxcMapScreen1.ViewScale / 2);
            if (sc <= 2000)
                axaxcMapScreen1.ViewScale = 2000;
            else
                axaxcMapScreen1.ViewScale = sc;

        }
*/
/*
        //уменьшить масштаб
        public static void ZoomOutFunc(ref AxaxcMapScreen axaxcMapScreen1)
        {

            int sc = (int)(axaxcMapScreen1.ViewScale * 2);
            if (sc >= 20000000)
                axaxcMapScreen1.ViewScale = 20000000;
            else 
            axaxcMapScreen1.ViewScale = sc;
 
        }
*/
/*
        //исходный масштаб
        public static void ZoomInitialFunc(ref AxaxcMapScreen axaxcMapScreen1)
        {
            axaxcMapScreen1.ViewScale = 1000000;

        }
*/
/*
        //отрисовка и сохранение треугольника-ИРИ
        public static void DrawAndSaveTriangle(ref RadioSource radioSource, ref AxaxcMapScreen axaxcMapScreen1)
        {

            Graphics graph = axaxcMapScreen1.CreateGraphics();

            double geoX = radioSource.planeX;
            double geoY = radioSource.planeY;
            //Plane -> Geo
            mapPlaneToGeo((int)axaxcMapScreen1.MapHandle, ref geoX, ref geoY);
            //Geo -> RealGeo
            geoX = geoX * 180 / Math.PI;
            geoY = geoY * 180 / Math.PI;

            radioSource.geoX = geoX;
            radioSource.geoY = geoY;


            double x = radioSource.planeX;
            double y = radioSource.planeY;

            mapPlaneToPicture((int)axaxcMapScreen1.MapHandle, ref x, ref y);

            radioSource.pictureX = x;
            radioSource.pictureY = y;

            Point[] toDraw = new Point[3];
            toDraw[0].X = (int)x - (axaxcMapScreen1.MapLeft + 7);
            toDraw[0].Y = (int)y - (axaxcMapScreen1.MapTop - 7);
            toDraw[1].X = (int)x - axaxcMapScreen1.MapLeft;
            toDraw[1].Y = (int)y - (axaxcMapScreen1.MapTop + 7);
            toDraw[2].X = (int)x - (axaxcMapScreen1.MapLeft - 7);
            toDraw[2].Y = (int)y - (axaxcMapScreen1.MapTop - 7);
            Brush brush3 = new SolidBrush(Color.Red);
            graph.FillPolygon(brush3, toDraw);
 
        }
*/
/*
        //отрисовка и сохранение треугольника-ИРИ
        public static void DrawAndSaveTriangle(ref RadioSource radioSource, ref AxaxcMapScreen axaxcMapScreen1, Color color)
        {

            Graphics graph = axaxcMapScreen1.CreateGraphics();

            double x = radioSource.planeX;
            double y = radioSource.planeY;

            mapPlaneToPicture((int)axaxcMapScreen1.MapHandle, ref x, ref y);

            radioSource.pictureX = x;
            radioSource.pictureY = y;

            Point[] toDraw = new Point[3];
            toDraw[0].X = (int)x - (axaxcMapScreen1.MapLeft + 7);
            toDraw[0].Y = (int)y - (axaxcMapScreen1.MapTop - 7);
            toDraw[1].X = (int)x - axaxcMapScreen1.MapLeft;
            toDraw[1].Y = (int)y - (axaxcMapScreen1.MapTop + 7);
            toDraw[2].X = (int)x - (axaxcMapScreen1.MapLeft - 7);
            toDraw[2].Y = (int)y - (axaxcMapScreen1.MapTop - 7);
            Brush brush3 = new SolidBrush(color);
            graph.FillPolygon(brush3, toDraw);
 
        }
*/
/*
        //удаление треугольника-ИРИ
        public static void DeleteTriangle(double planeX, double planeY, ref List<RadioSource> stations, ref AxaxcMapScreen axaxcMapScreen1)
        {

            double x = planeX;
            double y = planeY;

            mapPlaneToPicture((int)axaxcMapScreen1.MapHandle, ref x, ref y);

            int index = -1;

            for (int i = 0; i < stations.Count; i++)
            {
                if ((stations[i].pictureX < x + 10 && stations[i].pictureX > x - 10) && (stations[i].pictureY < y + 10 && stations[i].pictureY > y - 10))
                {
                    index = i;
                    break;
                }
            }

            if (index != -1)
                stations.RemoveAt(index);

            axaxcMapScreen1.Repaint();
 
        }
*/
/*
        //Получение треугольника-ИРИ
        public static RadioSource GetRadioSource(double planeX, double planeY, ref List<RadioSource> stations, ref AxaxcMapScreen axaxcMapScreen1)
        {

            double x = planeX;
            double y = planeY;

            mapPlaneToPicture((int)axaxcMapScreen1.MapHandle, ref x, ref y);

            int index = -1;

            for (int i = 0; i < stations.Count; i++)
            {
                if ((stations[i].pictureX < x + 10 && stations[i].pictureX > x - 10) && (stations[i].pictureY < y + 10 && stations[i].pictureY > y - 10))
                {
                    index = i;
                    break;
                }
            }

            if (index != -1)
                return stations[index];
            else return new RadioSource();
 
        }
*/
/*
        //Получение индекса треугольника-ИРИ
        public static int GetRadioSourceIndex(double planeX, double planeY, ref List<RadioSource> stations, ref AxaxcMapScreen axaxcMapScreen1)
        {

            double x = planeX;
            double y = planeY;

            mapPlaneToPicture((int)axaxcMapScreen1.MapHandle, ref x, ref y);

            int index = -1;

            for (int i = 0; i < stations.Count; i++)
            {
                if ((stations[i].pictureX < x + 10 && stations[i].pictureX > x - 10) && (stations[i].pictureY < y + 10 && stations[i].pictureY > y - 10))
                {
                    index = i;
                    break;
                }
            }

            if (index != -1)
                return index;
            else return -1;
 
        }
*/
/*
        //отрисовка дуги
        public static void DrawArc(PointF[] Arcs, ref AxaxcMapScreen axaxcMapScreen1)
        {

            Graphics graph = axaxcMapScreen1.CreateGraphics();

            for (int i = 0; i < Arcs.Count(); i++)
            {
                double tempX = Arcs[i].X;
                double tempY = Arcs[i].Y;
                mapPlaneToPicture((int)axaxcMapScreen1.MapHandle, ref tempX, ref tempY);
                Arcs[i].X = (float)tempX - axaxcMapScreen1.MapLeft;
                Arcs[i].Y = (float)tempY - axaxcMapScreen1.MapTop;
            }

            Pen pen = new Pen(Color.Violet);
            pen.Width = 5;
            graph.DrawCurve(pen, Arcs);

        }
*/
/*
        //отрисовка пеленга
        public static void DrawPeleng(double[] latitude, double[] longitude, ref AxaxcMapScreen axaxcMapScreen1)
        {

            double[] lat = new double[latitude.Count()];
            latitude.CopyTo(lat, 0);
            double[] lon = new double[longitude.Count()];
            longitude.CopyTo(lon, 0);


            int count = Math.Min(latitude.Count(), longitude.Count());

            PointF[] Arcs = new PointF[count];

            Graphics graph = axaxcMapScreen1.CreateGraphics();

            for (int i = 0; i < count; i++)
            {
                //RealGeo -> Geo
                lat[i] = lat[i] * Math.PI / 180.0;
                lon[i] = lon[i] * Math.PI / 180.0;
                //Geo -> Plane
                mapGeoToPlane((int)axaxcMapScreen1.MapHandle, ref lat[i], ref lon[i]);
                //Plane -> Picture
                mapPlaneToPicture((int)axaxcMapScreen1.MapHandle, ref lat[i], ref lon[i]);
                Arcs[i].X = (float)lat[i] - axaxcMapScreen1.MapLeft;
                Arcs[i].Y = (float)lon[i] - axaxcMapScreen1.MapTop;
            }

            Pen pen = new Pen(Color.Red);
            pen.Width = 3;
            graph.DrawCurve(pen, Arcs);

        }
*/

        //извлечение массивов широты-долготы
        public static void One3NumArrtoTwo1NumArrs(double[] One3DimArr, ref double[] latitude, ref double[] longitude)
        {
/*
            int k = One3DimArr.Count() / 3;
            for (int i = 0; i < k; i++)
            {
                latitude[i] = One3DimArr[3 * i + 1];
                longitude[i] = One3DimArr[3 * i + 2];
            }
 */ 
        }

/*
        //отрисовка и сохранение картинки-станции
        public static void DrawAndSaveStation(ref Station stantion, ref AxaxcMapScreen axaxcMapScreen1, ImageList imageList)
        {

            Graphics graph = axaxcMapScreen1.CreateGraphics();

            double x = stantion.planeX;
            double y = stantion.planeY;

            mapPlaneToPicture((int)axaxcMapScreen1.MapHandle, ref x, ref y);

            stantion.pictureX = x;
            stantion.pictureY = y;


            Bitmap bmp = new Bitmap("1.bmp");
            Rectangle rec = new Rectangle((int)x - axaxcMapScreen1.MapLeft - bmp.Width / 2, (int)y - axaxcMapScreen1.MapTop - bmp.Height / 2, bmp.Width, bmp.Height);
            graph.DrawImage(bmp, rec);

        }
*/
/*
        //удаление картинки-станции
        public static void DeleteStation(double planeX, double planeY, ref List<Station> stations, ref AxaxcMapScreen axaxcMapScreen1)
        {

            double x = planeX;
            double y = planeY;

            mapPlaneToPicture((int)axaxcMapScreen1.MapHandle, ref x, ref y);

            int index = -1;

            for (int i = 0; i < stations.Count; i++)
            {
                if ((stations[i].pictureX < x + 10 && stations[i].pictureX > x - 10) && (stations[i].pictureY < y + 10 && stations[i].pictureY > y - 10))
                {
                    index = i;
                    break;
                }
            }

            if (index != -1)
                stations.RemoveAt(index);

            axaxcMapScreen1.Repaint();
            //axaxcMapScreen1.Refresh();
 
        }
*/
/*
        //показать/удалить информацию
        public static void SetInfoPaint(double planeX, double planeY, ref List<RadioSource> radioSources, ref AxaxcMapScreen axaxcMapScreen1)
        {

            double x = planeX;
            double y = planeY;

            mapPlaneToPicture((int)axaxcMapScreen1.MapHandle, ref x, ref y);

            int index = -1;

            for (int i = 0; i < radioSources.Count; i++)
            {
                if ((radioSources[i].pictureX < x + 10 && radioSources[i].pictureX > x - 10) && (radioSources[i].pictureY < y + 10 && radioSources[i].pictureY > y - 10))
                {
                    index = i;
                    break;
                }
            }

            if (index != -1)
            {
                RadioSource temp = radioSources[index];
                temp.RadioSourceSetBoolInfo(!temp.isinfopaint);
                radioSources[index] = temp;
            }
            axaxcMapScreen1.Repaint();
  
        }
*/

/*
        //отрисовка информации
        public static void InfoPaint(RadioSource radioSource, ref AxaxcMapScreen axaxcMapScreen1)
        {

            Graphics graph = axaxcMapScreen1.CreateGraphics();

            double x = radioSource.planeX;
            double y = radioSource.planeY;

            mapPlaneToPicture((int)axaxcMapScreen1.MapHandle, ref x, ref y);

            Brush brush = new SolidBrush(Color.Red);

            Font font = new Font(FontFamily.GenericSansSerif, 12.0F, FontStyle.Regular, GraphicsUnit.Pixel);

            graph.DrawString("X=", font, brush, (int)radioSource.pictureX - axaxcMapScreen1.MapLeft + 5, (int)radioSource.pictureY - axaxcMapScreen1.MapTop - 12);

            graph.DrawString("Y=",font, brush, (int)radioSource.pictureX - axaxcMapScreen1.MapLeft + 5, (int)radioSource.pictureY - axaxcMapScreen1.MapTop - 2);
  
  
        }
*/
    }
}
