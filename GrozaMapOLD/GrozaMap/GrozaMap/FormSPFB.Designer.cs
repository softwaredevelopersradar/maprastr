﻿namespace GrozaMap
{
    partial class FormSPFB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.bAccept = new System.Windows.Forms.Button();
            this.lChooseSC = new System.Windows.Forms.Label();
            this.cbChooseSC = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.chbXY = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5});
            this.dataGridView1.Location = new System.Drawing.Point(0, 32);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(346, 198);
            this.dataGridView1.TabIndex = 212;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Fmin,MHz";
            this.Column2.Name = "Column2";
            this.Column2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column2.Width = 70;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Fmax,MHz";
            this.Column3.Name = "Column3";
            this.Column3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column3.Width = 70;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Az.min,deg";
            this.Column4.Name = "Column4";
            this.Column4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column4.Width = 70;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Az.max,deg";
            this.Column5.Name = "Column5";
            this.Column5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Column5.Width = 70;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(45, 315);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(83, 23);
            this.button1.TabIndex = 213;
            this.button1.Text = "Читать все";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // bClear
            // 
            this.bClear.Location = new System.Drawing.Point(115, 3);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(58, 23);
            this.bClear.TabIndex = 210;
            this.bClear.Text = "Clear";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // bAccept
            // 
            this.bAccept.Location = new System.Drawing.Point(1, 2);
            this.bAccept.Name = "bAccept";
            this.bAccept.Size = new System.Drawing.Size(55, 23);
            this.bAccept.TabIndex = 209;
            this.bAccept.Text = "Save";
            this.bAccept.UseVisualStyleBackColor = true;
            this.bAccept.Click += new System.EventHandler(this.bAccept_Click);
            // 
            // lChooseSC
            // 
            this.lChooseSC.AutoSize = true;
            this.lChooseSC.Location = new System.Drawing.Point(194, 9);
            this.lChooseSC.Name = "lChooseSC";
            this.lChooseSC.Size = new System.Drawing.Size(77, 13);
            this.lChooseSC.TabIndex = 12;
            this.lChooseSC.Text = "Jammer station";
            // 
            // cbChooseSC
            // 
            this.cbChooseSC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChooseSC.DropDownWidth = 150;
            this.cbChooseSC.FormattingEnabled = true;
            this.cbChooseSC.Items.AddRange(new object[] {
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""});
            this.cbChooseSC.Location = new System.Drawing.Point(273, 4);
            this.cbChooseSC.Name = "cbChooseSC";
            this.cbChooseSC.Size = new System.Drawing.Size(73, 21);
            this.cbChooseSC.TabIndex = 11;
            this.cbChooseSC.SelectedIndexChanged += new System.EventHandler(this.cbChooseSC_SelectedIndexChanged);
            // 
            // button3
            // 
            this.button3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button3.Location = new System.Drawing.Point(144, 315);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(85, 23);
            this.button3.TabIndex = 221;
            this.button3.Text = "Отправить";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(58, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(55, 23);
            this.button2.TabIndex = 222;
            this.button2.Text = "Load";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // chbXY
            // 
            this.chbXY.AutoSize = true;
            this.chbXY.Location = new System.Drawing.Point(89, 237);
            this.chbXY.Name = "chbXY";
            this.chbXY.Size = new System.Drawing.Size(15, 14);
            this.chbXY.TabIndex = 223;
            this.chbXY.UseVisualStyleBackColor = true;
            this.chbXY.CheckedChanged += new System.EventHandler(this.chbXY_CheckedChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(6, 237);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 13);
            this.label15.TabIndex = 224;
            this.label15.Text = "Input from map";
            // 
            // FormSPFB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(351, 256);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.chbXY);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.lChooseSC);
            this.Controls.Add(this.cbChooseSC);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.bAccept);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(900, 100);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSPFB";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Bands and sectors of reconnaissance ( not more than 10)";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.FormSPFB_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSPFB_FormClosing);
            this.Load += new System.EventHandler(this.FormSPFB_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        public System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button bClear;
        private System.Windows.Forms.Button bAccept;
        private System.Windows.Forms.Label lChooseSC;
        public System.Windows.Forms.ComboBox cbChooseSC;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        public System.Windows.Forms.CheckBox chbXY;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
    }
}