﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//using AxaxGisToolKit;
//using axGisToolKit;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.IO;

//0206*
using System.Windows.Input;
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;
using Bitmap = System.Drawing.Bitmap;


namespace GrozaMap
{
    public partial class FormLF : Form
    {
/*
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeoWGS84(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToPicture(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPictureToPlane(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        public static extern int mapPlaneToGeo(int hmap, ref double Bx, ref double Ly);
        [DllImport("gisacces.dll")]
        static extern int mapGeoToPlane(int hmap, ref double Bx, ref double Ly);
*/

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // Конструктор *********************************************************** 

        public FormLF()
        {
            InitializeComponent();



        } // Конструктор
        // ***********************************************************  Конструктор

        // ************************************************************************
        // Загрузка формы
       // ************************************************************************
        private void FormLF_Load(object sender, EventArgs e)
        {

            if (GlobalVarLn.flEndTRO_stat != 1)
            {

                // .....................................................................................
                // Очистка dataGridView

                dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDatLF1_stat; i++)
                {
                    dataGridView1.Rows.Add("", "", "");
                }

                // .....................................................................................
                // Флаги

                GlobalVarLn.blLF1_stat = true;
                GlobalVarLn.flEndLF1_stat = 1;
                // .....................................................................................
                GlobalVarLn.iLF1_stat = 0;
                GlobalVarLn.X_LF1 = 0;
                GlobalVarLn.Y_LF1 = 0;
                GlobalVarLn.H_LF1 = 0;
                GlobalVarLn.list_LF1.Clear();
                // .....................................................................................
            }


        } // Form_Load
        // ************************************************************************

        // ************************************************************************
        // Очистка LF1
        // ************************************************************************
        private void bClear_Click(object sender, EventArgs e)
        {
           // ----------------------------------------------------------------------
             GlobalVarLn.blLF1_stat = true;
             GlobalVarLn.flEndLF1_stat = 1;
            // ----------------------------------------------------------------------
            // переменные

            GlobalVarLn.iLF1_stat = 0;
            GlobalVarLn.X_LF1 = 0;
            GlobalVarLn.Y_LF1 = 0;
            GlobalVarLn.H_LF1 = 0;
            // -------------------------------------------------------------------
            GlobalVarLn.list_LF1.Clear();
            // -------------------------------------------------------------------
            // Очистка dataGridView1

            while (dataGridView1.Rows.Count != 0)
                dataGridView1.Rows.Remove(dataGridView1.Rows[dataGridView1.Rows.Count - 1]);

            dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatLF1_stat; i++)
            {
                dataGridView1.Rows.Add("", "", "");
            }
            // -------------------------------------------------------------------
            // Убрать с карты

            //0209
            //GlobalVarLn.axMapScreenGlobal.Repaint();
            MapForm.REDRAW_MAP();
            // -------------------------------------------------------------------

        } // Clear LF1
        // ************************************************************************

        // ************************************************************************
        // Обработчик кнопки : сохранить
        // ************************************************************************
        private void bAccept_Click(object sender, EventArgs e)
        {
            int i_tmp = 0;
            // -----------------------------------------------------------------------------------------
            String strFileName;

            //strFileName = "LF1.txt";
            //0219
            strFileName = Application.StartupPath + "\\SaveInFiles\\LineFriendly.txt";

            StreamWriter srFile;

            //StreamWriter srFile = new StreamWriter(strFileName);
            try
            {
                srFile = new StreamWriter(strFileName);
            }
            catch
            {
                MessageBox.Show("Can’t save file");
                return;
            }

            // -----------------------------------------------------------------------------------------
            srFile.WriteLine("N =" + Convert.ToString(GlobalVarLn.iLF1_stat));

            //0209
            //for (i_tmp = 0; i_tmp < GlobalVarLn.iLF1_stat; i_tmp++)
            for (i_tmp = 0; i_tmp < GlobalVarLn.list_LF1.Count; i_tmp++)

            {
                srFile.WriteLine("X =" + Convert.ToString((int)GlobalVarLn.list_LF1[i_tmp].X_m));
                srFile.WriteLine("Y =" + Convert.ToString((int)GlobalVarLn.list_LF1[i_tmp].Y_m));
                srFile.WriteLine("H =" + Convert.ToString((int)GlobalVarLn.list_LF1[i_tmp].H_m));
            }

            // -----------------------------------------------------------------------------------------

            srFile.Close();
            // -----------------------------------------------------------------------------------------

        } // Save to file
        // ************************************************************************

        // *****************************************************************************************
        // Обработчик кнопки : read from file
        // *****************************************************************************************
        private void button1_Click(object sender, EventArgs e)
        {
            // -----------------------------------------------------------------------------------------
            String strLine = "";
            String strLine1 = "";

            double number1 = 0;
            int number2 = 0;

            char symb1 = 'N';
            char symb2 = 'X';
            char symb3 = 'Y';
            char symb4 = '=';
            char symb5 = 'H';

            int indStart = 0;
            int indStop = 0;
            int iLength = 0;

            int IndZap = 0;
            int TekPoz = 0;

            int fi = 0;

            //0209
            double lat = 0;
            double lon = 0;
            // Очистка ---------------------------------------------------------------------------

            // ----------------------------------------------------------------------
            GlobalVarLn.blLF1_stat = true;
            GlobalVarLn.flEndLF1_stat = 1;
            // ----------------------------------------------------------------------
            // переменные

            GlobalVarLn.iLF1_stat = 0;
            GlobalVarLn.X_LF1 = 0;
            GlobalVarLn.Y_LF1 = 0;
            GlobalVarLn.H_LF1 = 0;
            // -------------------------------------------------------------------
            GlobalVarLn.list_LF1.Clear();
            // -------------------------------------------------------------------
            // Очистка dataGridView1

            while (dataGridView1.Rows.Count != 0)
                dataGridView1.Rows.Remove(dataGridView1.Rows[dataGridView1.Rows.Count - 1]);

            dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatLF1_stat; i++)
            {
                dataGridView1.Rows.Add("", "", "");
            }
            // -------------------------------------------------------------------
            // Убрать с карты

            //0209
            //GlobalVarLn.axMapScreenGlobal.Repaint();
            // --------------------------------------------------------------------------- Очистка

            // Чтение файла ---------------------------------------------------------
            String strFileName;

            //strFileName = "LF1.txt";
            //0219
            strFileName = Application.StartupPath + "\\SaveInFiles\\LineFriendly.txt";

            StreamReader srFile;
            try
            {
                srFile = new StreamReader(strFileName);
            }
            catch
            {
                MessageBox.Show("Can’t open file");
                return;

            }
            // -------------------------------------------------------------------------------------
            // Чтение
            // N =...
            // X =...
            // Y =...
            // H =...

            try
            {
                LF objLF = new LF();

                TekPoz = 0;
                IndZap = 0;
                // .......................................................
                // N =...
                // 1-я строка

                strLine = srFile.ReadLine();

                if (strLine == null)
                {
                    MessageBox.Show("No information");
                    return;
                }

                indStart = strLine.IndexOf(symb1, TekPoz); // N

                if (indStart == -1)
                {
                    MessageBox.Show("No information");
                    return;
                }

                indStop = strLine.IndexOf(symb4, TekPoz);  //=

                if ((indStop == -1) || (indStop < indStart))
                {
                    MessageBox.Show("No information");
                    return;
                }

                iLength = indStop - indStart + 1;
                // Убираем 'NSP ='
                strLine1 = strLine.Remove(indStart, iLength);

                if (strLine1 == "")
                {
                    MessageBox.Show("No information");
                    return;
                }

                // Количество 
                number2 = Convert.ToInt32(strLine1);
                GlobalVarLn.iLF1_stat = (uint)number2;
                // .......................................................

                fi = 0;
                strLine = srFile.ReadLine(); // читаем далее (X1)
                if ((strLine == "") || (strLine == null))
                {
                    fi = 1;
                }
                // .......................................................

                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH
                while ((strLine != "") && (strLine != null))
                {
                    IndZap += 1;

                    // .......................................................
                    // X =...

                    indStart = strLine.IndexOf(symb2, TekPoz); // X
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    number1 = Convert.ToDouble(strLine1);

                    objLF.X_m = number1;
                    // .......................................................
                    // Y =...

                    strLine = srFile.ReadLine();

                    indStart = strLine.IndexOf(symb3, TekPoz); // Y
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    number1 = Convert.ToDouble(strLine1);

                    objLF.Y_m = number1;
                    // .......................................................
                    // H =...

                    strLine = srFile.ReadLine();

                    indStart = strLine.IndexOf(symb5, TekPoz); // H
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    number1 = Convert.ToInt32(strLine1);

                    objLF.H_m = number1;
                    // .......................................................
                    //0209
                    //Merkator

                    var p5 = Mercator.ToLonLat(objLF.X_m, objLF.Y_m);
                    lat = p5.Y;
                    lon = p5.X;
                    // .......................................................
                    // Занести в List

                    GlobalVarLn.list_LF1.Add(objLF);

                    // .......................................................
                    // Занести в таблицу
                    //0209

                    //var p = axaxcMapScreen.MapPlaneToRealGeo(objLF.X_m, objLF.Y_m);
                    //axaxcMapScreen.MapPlaneToRealGeo(ref objLF.X_m, ref objLF.Y_m);

                    dataGridView1.Rows[(int)(IndZap - 1)].Cells[0].Value = lat.ToString("F3");
                    dataGridView1.Rows[(int)(IndZap - 1)].Cells[1].Value = lon.ToString("F3");  
                    dataGridView1.Rows[(int)(IndZap - 1)].Cells[2].Value = objLF.H_m;  // H
                    // ............................................................

                    fi = 0;
                    strLine = srFile.ReadLine(); // читаем далее (Xi)
                    if ((strLine == "") || (strLine == null))
                        fi = 1;
                    // ...................................................................

                } // WHILE
                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH


            }
            // -----------------------------------------------------------------------------------
            catch
            {

            }
            // -------------------------------------------------------------------------------------
            srFile.Close();
            // -------------------------------------------------------------------------------------

            //0209
            //f_LF1ReDraw();
            MapForm.REDRAW_MAP();


        } // read from file
        // *****************************************************************************************

        // ФУНКЦИИ ********************************************************************************

        // ****************************************************************************************
        // Обработка нажатия левой кнопки мыши при отрисовке LF1
        //
        // Входные параметры:
        // X - X, m на местности
        // Y - Y, m
        // ****************************************************************************************
        public void f_LF1(
                          double X,
                          double Y
                         )
        {
/*
            // ......................................................................

            LF objLF = new LF();
            // ......................................................................
            // !!! реальные координаты на местности карты в м (Plane)

            GlobalVarLn.X_LF1 = GlobalVarLn.MapX1;
            GlobalVarLn.Y_LF1 = GlobalVarLn.MapY1;
            objLF.X_m = GlobalVarLn.X_LF1;
            objLF.Y_m = GlobalVarLn.Y_LF1;
            // ......................................................................
            // ......................................................................
            GlobalVarLn.blLF1_stat = true;
            GlobalVarLn.flEndLF1_stat = 1;
            // ......................................................................
            // H

            GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.MapX1, GlobalVarLn.MapY1);
            GlobalVarLn.H_LF1 = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
            objLF.H_m = GlobalVarLn.H_LF1;
            // ......................................................................
            GlobalVarLn.iLF1_stat += 1;
            // ......................................................................
            // Добавить строку

            var p = axaxcMapScreen.MapPlaneToRealGeo(GlobalVarLn.X_LF1, GlobalVarLn.Y_LF1);

            dataGridView1.Rows[(int)(GlobalVarLn.iLF1_stat - 1)].Cells[0].Value = p.X.ToString("F3"); // X
            dataGridView1.Rows[(int)(GlobalVarLn.iLF1_stat - 1)].Cells[1].Value = p.Y.ToString("F3"); // Y
            dataGridView1.Rows[(int)(GlobalVarLn.iLF1_stat - 1)].Cells[2].Value = (int)GlobalVarLn.H_LF1; // H
            // ......................................................................
            // Добавить в List

            GlobalVarLn.list_LF1.Add(objLF);
            // ---------------------------------------------------------------------------------
            // Перерисовать

            GlobalVarLn.axMapScreenGlobal.Repaint();

            f_LF1ReDraw();
           // -------------------------------------------------------------------
*/

            //0209
            // ......................................................................
            double lat = 0;
            double lon = 0;

            LF objLF = new LF();
            // ......................................................................
            // !!! Merkator в м 

            GlobalVarLn.X_LF1 = GlobalVarLn.X_Rastr;
            GlobalVarLn.Y_LF1 = GlobalVarLn.Y_Rastr;
            objLF.X_m = GlobalVarLn.X_LF1;
            objLF.Y_m = GlobalVarLn.Y_LF1;

            GlobalVarLn.H_LF1 = GlobalVarLn.H_Rastr;
            objLF.H_m = GlobalVarLn.H_LF1;

            var p = Mercator.ToLonLat(GlobalVarLn.X_LF1, GlobalVarLn.Y_LF1);
            lat = p.Y;
            lon = p.X;

            // ......................................................................
            GlobalVarLn.blLF1_stat = true;
            GlobalVarLn.flEndLF1_stat = 1;
            // ......................................................................
            GlobalVarLn.iLF1_stat += 1;
            // ......................................................................
            // Добавить строку

            //var p = axaxcMapScreen.MapPlaneToRealGeo(GlobalVarLn.X_LF1, GlobalVarLn.Y_LF1);

            dataGridView1.Rows[(int)(GlobalVarLn.iLF1_stat - 1)].Cells[0].Value = lat.ToString("F3"); 
            dataGridView1.Rows[(int)(GlobalVarLn.iLF1_stat - 1)].Cells[1].Value = lon.ToString("F3"); 
            dataGridView1.Rows[(int)(GlobalVarLn.iLF1_stat - 1)].Cells[2].Value = (int)GlobalVarLn.H_LF1; // H
            // ......................................................................
            // Добавить в List

            GlobalVarLn.list_LF1.Add(objLF);
            // ---------------------------------------------------------------------------------
            // Перерисовать

            //GlobalVarLn.axMapScreenGlobal.Repaint();
            //f_LF1ReDraw();
            MapForm.REDRAW_MAP();
            // -------------------------------------------------------------------


        } // P/P f_LF1
        // *************************************************************************************

        // *************************************************************************************
        // Перерисовка LF1
        // *************************************************************************************
        //0209

        public void f_LF1ReDraw()
        {
            //ClassMap.f_LF1_stat();

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            List<Mapsui.Geometries.Point> pointPel = new List<Mapsui.Geometries.Point>();
           double lat=0;
           double lon=0;

            for (int i = 0; i < GlobalVarLn.list_LF1.Count; i++)
            {
               var p = Mercator.ToLonLat(GlobalVarLn.list_LF1[i].X_m, GlobalVarLn.list_LF1[i].Y_m);
               lat=p.Y;
               lon=p.X;
             
                pointPel.Add(new Mapsui.Geometries.Point(lon, lat));

            } // FOR


            try
            {
               MapForm.RasterMapControl.AddPolyline(pointPel, Mapsui.Styles.Color.Blue, 2);
            }
            catch
            {
             //MessageBox.Show(e.Message);
            }

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


        } // P/P f_LFReDraw
        // *************************************************************************************

        // ******************************************************************************** ФУНКЦИИ

        // ****************************************************************************************
        // Закрыть форму
        // ****************************************************************************************
        private void FormLF_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

            // приостановить обработку, если идет
            GlobalVarLn.blLF1_stat = false;

            //GlobalVarLn.fFLF1 = 0;
            GlobalVarLn.fl_Open_objFormLF = 0;


        }
        // ****************************************************************************************
        // Активизировать форму
        // *************************************************************************************
        private void FormLF_Activated(object sender, EventArgs e)
        {

            //GlobalVarLn.objFormLFG.WindowState = FormWindowState.Normal;
            //GlobalVarLn.fFLF1 = 1;

          GlobalVarLn.blLF1_stat = true;
          GlobalVarLn.flEndLF1_stat = 1;

          GlobalVarLn.fl_Open_objFormLF = 1;

              // 0809_3
              //ClassMap.f_RemoveFrm(4);

        }
        // *************************************************************************************

        private void button6_Click(object sender, EventArgs e)
        {

        }

        // *************************************************************************************

    } // class
} // namespace
