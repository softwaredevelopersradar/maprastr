﻿


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.IO;


//0206*
using System.Windows.Input;
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;
using Bitmap = System.Drawing.Bitmap;
using Point = System.Drawing.Point;



namespace GrozaMap
{

    public class ClassMap
    {


        // VARS *******************************************************************************

        // STATIC -----------------------------------------------------------------------------
/*
        public static double MapX1 = 0;
        public static double MapY1 = 0;
        public static int hmapl = 0;

        // Самолеты
        public static int fl_AirPlane = 0;
        public static double Lat_air;
        public static double Long_air;
        public static double Angle_air;
*/
        // ----------------------------------------------------------------------------STATIC

        //public static double LAMBDA=300000;

        // Координаты -------------------------------------------------------------------------
        //private double Pi;

        // Число угловых секунд в радиане
        private double ro;

        // Эллипсоид Красовского
        private double aP;  // Большая полуось
        private double a1P; // Сжатие
        private double e2P; // Квадрат эксцентриситета

        // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
        private double aW;  // Большая полуось
        private double a1W; // Сжатие
        private double e2W; // Квадрат эксцентриситета

        // Вспомогательные значения для преобразования эллипсоидов
        private double a;
        private double da;
        private double e2;
        private double de2;

        // Угловые элементы трансформирования, в секундах
        private double wx;
        private double wy;
        private double wz;

        // Дифференциальное различие масштабов
        private double ms;

        // Флаг НУ
        //private int fl_first1_Coord;
        private int fl_first2_Coord;
        //private int fl_first3_Coord;
        //private int fl_first5_Coord;
        //private int fl_first6_Coord;
        //private int fl_first7_Coord;
        //private int fl_first8_Coord;
        // ------------------------------------------------------------------------Координаты

        // Пеленг PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP

        // Флаг НУ
        private int fl_first_Pel;

        // Вспомогательные флаги
        //private int fl1_Pel;

        // Индекс фиктивной точки (1,2, ...)
        private uint IndFP_Pel;
        // Индекс i-й фиктивной точки в массивах(0,...)
        private uint IndMas_Pel;

        // Расстояние по поверхности земли от точки стояния пеленгатора до текущей 
        // фиктивной точки 
        //private double Mi_Pel;
        // Расстояние по прямой от точки стояния пеленгатора до пересечения прямой
        // (проходящей через текущую фиктивную точку и начало координат)и плоскости,
        // касательной в точке стояния пеленгатора
        //private double Di_Pel;

        // Радиус Земли (шар)
        private double REarth_Pel;
        // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 
        private double REarthP_Pel;
        // Радиус Земли  в (i-1)-й фиктивной точке с учетом текущей широты 
        //double REarthF_Pel;

        // Координаты фиктивной точки в СК пеленгатора (СКП)
        //private double Xi_Pel;
        //private double Yi_Pel;
        //private double Zi_Pel;

        // Координаты фиктивной точки в СК1 (Поворот СКП на угол=широте пеленгатора)
        //private double Xi0_Pel;
        //private double Yi0_Pel;
        //private double Zi0_Pel;

        // Координаты фиктивной точки в опорной геоцентрической СК
        // (Поворот СК1 на угол=долготе пеленгатора)
        private double XiG_Pel;
        private double YiG_Pel;
        private double ZiG_Pel;

        // Угловые координаты фиктивной точки в опорной геоцентрической СК
        private double Ri_Pel;
        //private double LATi_Pel;
        //private double LONGi_Pel;

        // Координаты точки пересечения прямой (проходящей через текущую фиктивную
        // точку и начало координат) и плоскости касательной в точке стояния 
        // пеленгатора в опорной геоцентрической СК
        //private double XiG1_Pel;
        //private double YiG1_Pel;
        //private double ZiG1_Pel;
        //private double Ri1_Pel;

        // Центральный угол дуги Mi
        //private double Alpha;

        // Широта и долгота стояния пеленгатора
        private double LatP_Pel_tmp;
        private double LongP_Pel_tmp;

        // Количество фиктивных точек в плоскости пеленгования пеленгатора
        private uint NumbFikt_Pel_tmp;

        // Max дальность отображения пеленга
        private double Mmax_Pel_tmp;

        // Пеленг
        private double Theta_Pel_tmp;

        // Координаты пеленгатора в опорной геоцентрической СК
        private double XP_Pel_tmp;
        private double YP_Pel_tmp;
        private double ZP_Pel_tmp;

        // Счетчик
        //private int sch_Pel;

        // ----------------------------------------------------------------
        // !!! VAR2

        private double Lat_Pel_N;  //Lat(n)
        private double Lat_Pel_N1;  //Lat(n-1)
        private double Long_Pel_N;  //Long(n)
        private double Long_Pel_N1;  //Long(n-1)
        private double V_Pel;

        private int fl_var2;
        private int fl1_var2;
        private int fl2_var2;

        // ----------------------------------------------------------------

        // PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP Пеленг



        // ******************************************************************************* VARS


        // Конструктор *************************************************************************

        public ClassMap()
        {

            // Координаты ---------------------------------------------------------------------
            //Pi = 3.14159265358979;

            // Число угловых секунд в радиане
            ro = 206264.8062;

            // Эллипсоид Красовского
            aP = 6378245;  // Большая полуось
            a1P = 0; // Сжатие
            e2P = 0; // Квадрат эксцентриситета

            // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
            aW = 6378137;  // Большая полуось
            a1W = 0; // Сжатие
            e2W = 0; // Квадрат эксцентриситета

            // Вспомогательные значения для преобразования эллипсоидов
            a = 0;
            da = 0;
            e2 = 0;
            de2 = 0;

            // Линейные элементы трансформирования, в метрах
            // ГОСТ 51794_2001
            //dx = 23.92;
            //dy = -141.27;
            //dz = -80.9;
            //wx = 0;
            //wy = 0.35;
            //wz = 0.82;
            //ms = -0.000012;

            // ГОСТ 51794_2008
            //dx = 25;
            //dy = -141;
            //dz = -80;
            //wx=0; 
            //wy = -0.35;
            //wz = -0.66;
            //ms = 0;
            //dx = 28;
            //dy = -130;
            //dz = -95;
            wx = 0;
            wy = 0;
            wz = 0;
            ms = 0;

            //dx = 28;
            //dy = -130;
            //dz = -95;

            // Флаг НУ
            //fl_first1_Coord = 0;
            fl_first2_Coord = 0;
            //fl_first3_Coord = 0;
            //fl_first5_Coord = 0;
            //fl_first6_Coord = 0;
            //fl_first7_Coord = 0;
            //fl_first8_Coord = 0;
            // ---------------------------------------------------------------------Координаты


            // Пеленг PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP
            // Флаг НУ
            fl_first_Pel = 0;

            // Вспомогательные флаги
            //fl1_Pel = 0;

            // Индекс фиктивной точки (1,2, ...)
            IndFP_Pel = 1;
            // Индекс i-й фиктивной точки в массивах(0,...)
            IndMas_Pel = 0;

            // Расстояние по поверхности земли от точки стояния пеленгатора до текущей 
            // фиктивной точки 
            //Mi_Pel = 0;
            // Расстояние по прямой от точки стояния пеленгатора до пересечения прямой
            // (проходящей через текущую фиктивную точку и начало координат)и плоскости,
            // касательной в точке стояния пеленгатора
            //Di_Pel = 0;

            // Радиус Земли (шар)
            REarth_Pel = 6378245;
            // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 
            REarthP_Pel = 0;
            // Радиус Земли  в (i-1)-й фиктивной точке с учетом текущей широты 
            //REarthF_Pel=0;

            // Координаты фиктивной точки в СК пеленгатора
            //Xi_Pel = 0;
            //Yi_Pel = 0;
            //Zi_Pel = 0;

            // Координаты фиктивной точки в СК1 (Поворот СКП на угол=широте пеленгатора)
            //Xi0_Pel = 0;
            //Yi0_Pel = 0;
            //Zi0_Pel = 0;

            // Координаты фиктивной точки в опорной геоцентрической СК
            // (Поворот СК1 на угол=долготе пеленгатора)
            XiG_Pel = 0;
            YiG_Pel = 0;
            ZiG_Pel = 0;

            // Угловые координаты фиктивной точки в опорной геоцентрической СК
            Ri_Pel = 0;
            //LATi_Pel = 0;
            //LONGi_Pel = 0;

            // Координаты точки пересечения прямой (проходящей через текущую фиктивную
            // точку и начало координат) и плоскости касательной в точке стояния 
            // пеленгатора в опорной геоцентрической СК
            //XiG1_Pel = 0;
            //YiG1_Pel = 0;
            //ZiG1_Pel = 0;
            //Ri1_Pel = 0;

            // Центральный угол дуги Mi
            //Alpha = 0;

            // Широта и долгота стояния пеленгатора
            LatP_Pel_tmp = 0;
            LongP_Pel_tmp = 0;

            // Количество фиктивных точек в плоскости пеленгования пеленгатора
            NumbFikt_Pel_tmp = 0;

            // Max дальность отображения пеленга
            Mmax_Pel_tmp = 0;

            // Пеленг
            Theta_Pel_tmp = 0;

            // Счетчик
            //sch_Pel = 0;

            // Координаты пеленгатора в опорной геоцентрической СК
            XP_Pel_tmp = 0;
            YP_Pel_tmp = 0;
            ZP_Pel_tmp = 0;

            // ----------------------------------------------------------------
            // !!! VAR2

            Lat_Pel_N = 0;  //Lat(n)
            Lat_Pel_N1 = 0;  //Lat(n-1)
            Long_Pel_N = 0;  //Long(n)
            Long_Pel_N1 = 0;  //Long(n-1)
            V_Pel = 0;

            fl_var2 = 0;
            fl1_var2 = 0;
            fl2_var2 = 0;

            // ----------------------------------------------------------------

            // PelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelPelP Пеленг


        } // Конструктор
        // ************************************************************************* Конструктор

        // *************************************************************************************
        // Module
        // *************************************************************************************
        public double module(double a)
        {
            if (a >= 0) return (a);
            else return (-a);

        } // P/P module
        // *************************************************************************************

        // *************************************************************************************
        //  Считывание координат карты по клику мыши на карте
        // *************************************************************************************

        public static void f_XYMap(double x, double y)
        {
            GlobalVarLn.MapX1 = x;
            GlobalVarLn.MapY1 = y;

        } // f_XYMap
        // *************************************************************************************


        // ***********************************************************************
        // Преобразование dd.ddddd (grad) -> DD MM SS 
        // (дробное значение градусов -> в градусы(целое значение), минуты(целое значение),
        // секунды(дробное значение))

        // Входные параметры:
        // Grad_Dbl_Coord - градусы в дробном виде

        // Выходные параметры:
        // Grad_I_Coord - градусы (целое)
        // Min_Coord - минуты (целое)
        // Sec_Coord - секунды (дробное)
        // ***********************************************************************
        public void f_Grad_GMS
            (

                // Входные параметры (grad)
                double Grad_Dbl_Coord,

                // Выходные параметры 
                ref int Grad_I_Coord,
                ref int Min_Coord,
                ref double Sec_Coord

            )
        {

            Grad_I_Coord = (int)(Grad_Dbl_Coord);

            Min_Coord = (int)((module(Grad_Dbl_Coord) - module(Grad_I_Coord)) * 60);

            Sec_Coord = ((module(Grad_Dbl_Coord) - module(Grad_I_Coord)) * 60 - Min_Coord) * 60;

        } // Функция f_Grad_GMS
        //************************************************************************

        // ***********************************************************************
        // Расчет приращения по долготе при преобразованиях координат WGS84 (эллипсоид)
        // <-> эллипсоид Красовского(Пулково-42)   (преобразования Молоденского) в
        //  угл.сек

        // Входные параметры:
        // Lat_Coord_8442 - широта (град)
        // Long_Coord_8442 - долгота (град)
        // H_Coord_8442 - высота (км)
        // dX_Coord,dY_Coord,dZ_Coord - !!! это д.б.глобальные переменные, в которых
        // хранится DATUM (м) 

        // Выходные параметры:
        // dLong_Coord - приращение по долготе, !!! угловые секунды
        // ***********************************************************************
        public void f_dLong
            (

                // Входные параметры (внутри функции переводятся в рад и м)
                double Lat_Coord_8442,   // широта   (grad)
                double Long_Coord_8442,  // долгота  (grad)
                double H_Coord_8442,     // высота   (km)

                // DATUM
                double dX_Coord,
                double dY_Coord,
                double dZ_Coord,

                // Выходные параметры 
                ref double dLong_Coord   // приращение по долготе, угл.сек

            )
        {

            double N;
            double Lat_Coord_8442_tmp;
            double Long_Coord_8442_tmp;
            double H_Coord_8442_tmp;


            // Initial conditions ****************************************************
            // Initial conditions

            if (fl_first2_Coord == 0)
            {

                // Эллипсоид Красовского
                a1P = 1 / 298.3;         // Сжатие
                e2P = 2 * a1P - a1P * a1P; // Квадрат эксцентриситета

                // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
                a1W = 1 / 298.257223563; // Сжатие
                e2W = 2 * a1W - a1W * a1W;  // Квадрат эксцентриситета

                // Вспомогательные значения для преобразования эллипсоидов
                a = (aP + aW) / 2;
                e2 = (e2P + e2W) / 2;
                da = aW - aP;
                de2 = e2W - e2P;
                // ...............................................................

                fl_first2_Coord = 1;
                // ...............................................................

            }
            // **************************************************** Initial conditions

            // ...................................................................
            // Перевод в радианы и м

            H_Coord_8442_tmp = H_Coord_8442 * 1000; // km->m

            // grad->rad
            Lat_Coord_8442_tmp = (Lat_Coord_8442 * Math.PI) / 180;
            Long_Coord_8442_tmp = (Long_Coord_8442 * Math.PI) / 180;
            // ...................................................................

            // Радиус кривизны первого вертикала
            N = a / Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp));

            dLong_Coord = ro / ((N + H_Coord_8442_tmp) * Math.Cos(Lat_Coord_8442_tmp)) *
                         (-dX_Coord * Math.Sin(Long_Coord_8442_tmp) + dY_Coord * Math.Cos(Long_Coord_8442_tmp)) +
                        Math.Tan(Lat_Coord_8442_tmp) * (1 - e2) * (wx * Math.Cos(Long_Coord_8442_tmp) + wy * Math.Sin(Long_Coord_8442_tmp)) - wz;


        } // Функция f_dLong
        //************************************************************************

        // ***********************************************************************
        // Расчет приращения по широте при преобразованиях координат WGS84 (эллипсоид)
        // <-> эллипсоид Красовского(Пулково-42)   (преобразования Молоденского) в
        //  угл.сек

        // Входные параметры:
        // Lat_Coord_8442 - широта (град)
        // Long_Coord_8442 - долгота (град)
        // H_Coord_8442 - высота (км)
        // dX_Coord,dY_Coord,dZ_Coord - !!! это д.б.глобальные переменные, в которых
        // хранится DATUM (м) 

        // Выходные параметры:
        // dLat_Coord - приращение по широте, !!! угловые секунды
        // ***********************************************************************
        public void f_dLat
            (

                // Входные параметры (внутри функции переводятся в рад и м)
                double Lat_Coord_8442,   // широта (grad)
                double Long_Coord_8442,  // долгота (grad)
                double H_Coord_8442,     // высота (km)

                // DATUM
                double dX_Coord,
                double dY_Coord,
                double dZ_Coord,

                // Выходные параметры 
                ref double dLat_Coord    // приращение по широте, угл.сек

            )
        {

            double N, M;
            double Lat_Coord_8442_tmp;
            double Long_Coord_8442_tmp;
            double H_Coord_8442_tmp;

            // Initial conditions ****************************************************
            // Initial conditions

            if (fl_first2_Coord == 0)
            {
                // ...................................................................

                // Эллипсоид Красовского
                a1P = 1 / 298.3;                 // Сжатие
                //e2P = 2 * a1P - a1P*a1P;       // Квадрат эксцентриситета
                e2P = 2 * a1P - Math.Pow(a1P, 2); // Квадрат эксцентриситета


                // Эллипсоид WGS84 (GRS80, эти два эллипсоида сходны по большинству параметров)
                a1W = 1 / 298.257223563; // Сжатие
                e2W = 2 * a1W - a1W * a1W;  // Квадрат эксцентриситета

                // Вспомогательные значения для преобразования эллипсоидов
                a = (aP + aW) / 2;
                e2 = (e2P + e2W) / 2;
                da = aW - aP;
                de2 = e2W - e2P;
                // ...............................................................

                fl_first2_Coord = 1;
                // ...............................................................

            }
            // **************************************************** Initial conditions

            // ...................................................................
            // Перевод в радианы и м

            H_Coord_8442_tmp = H_Coord_8442 * 1000; // km->m

            // grad->rad
            Lat_Coord_8442_tmp = (Lat_Coord_8442 * Math.PI) / 180;
            Long_Coord_8442_tmp = (Long_Coord_8442 * Math.PI) / 180;
            // ...................................................................

            // Радиус кривизны первого вертикала
            N = a / Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp));

            // Радиус кривизны меридиана
            M = a * (1 - e2) *
                (1 / (Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp)) *
                    Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp)) *
                    Math.Sqrt(1 - e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp))));

            dLat_Coord = (ro / (M + H_Coord_8442_tmp)) *
                          ((N / a) * e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Cos(Lat_Coord_8442_tmp) * da +
                          ((N * N) / (a * a) + 1) * N * Math.Sin(Lat_Coord_8442_tmp) * Math.Cos(Lat_Coord_8442_tmp) * (de2 / 2) -
                          dX_Coord * Math.Cos(Long_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp) -
                          dY_Coord * Math.Sin(Long_Coord_8442_tmp) * Math.Sin(Lat_Coord_8442_tmp) +
                          dZ_Coord * Math.Cos(Lat_Coord_8442_tmp)) -
                          wx * Math.Sin(Long_Coord_8442_tmp) * (1 + e2 * Math.Cos(2 * Lat_Coord_8442_tmp)) +
                          wy * Math.Cos(Long_Coord_8442_tmp) * (1 + e2 * Math.Cos(2 * Lat_Coord_8442_tmp)) -
                          ro * ms * e2 * Math.Sin(Lat_Coord_8442_tmp) * Math.Cos(Lat_Coord_8442_tmp);


        } // Функция f_dLat
        //************************************************************************

        // ***********************************************************************
        // Перевод координат WGS84(эллипсоид) -> эллипсоид Красовского(Пулково-42)

        // Входные параметры:
        // Lat_Coord_8442 - широта (град)
        // Long_Coord_8442 - долгота (град)
        // H_Coord_8442 - высота (км)
        // Lat_Coord - приращение по широте, !!! угловые секунды
        // Long_Coord - приращение по долготе, !!! угловые секунды

        // Выходные параметры:
        // Lat_Coord_Vyx_8442 - широта (град)
        // Long_Coord_Vyx_8442 - долгота (град)

        // ***********************************************************************
        public void f_WGS84_SK42_Lat_Long
               (

                   // Входные параметры (grad,km)
                   double Lat_Coord_8442,   // широта
                   double Long_Coord_8442,  // долгота
                   double H_Coord_8442,     // высота
                   double dLat_Coord,       // приращение по долготе,угл.сек
                   double dLong_Coord,      // приращение по долготе,угл.сек

                   // Выходные параметры (grad)
                   ref double Lat_Coord_Vyx_8442,   // широта
                   ref double Long_Coord_Vyx_8442   // долгота

               )
        {

            // grad
            //Lat_Coord_Vyx_8442 = (Lat_Coord_8442 * 180) / Math.PI - dLat_Coord / 3600; 
            //Long_Coord_Vyx_8442 = (Long_Coord_8442 * 180) / Math.PI - dLong_Coord / 3600;
            Lat_Coord_Vyx_8442 = Lat_Coord_8442 - dLat_Coord / 3600;
            Long_Coord_Vyx_8442 = Long_Coord_8442 - dLong_Coord / 3600;

        } // Функция f_WGS84_SK42_Lat_Long
        //************************************************************************

        // ***********************************************************************
        // Преобразование геодезических координат (широта, долгота, высота) 
        // эллипсоида Красовского (СК42) в плоские прямоугольные координаты в
        // проекции Гаусса-Крюгера

        // Входные параметры:
        // Lat_Coord_Kr - широта (град)
        // Long_Coord_Kr - долгота (град)

        // Выходные параметры:
        // X_Coord_Kr, Y_Coord_Kr - плоские прямоугольные координаты (км)
        // ***********************************************************************
        public void f_SK42_Krug
            (

                // Входные параметры (!!! град)
                double Lat_Coord_Kr,   // широта
                double Long_Coord_Kr,  // долгота
            //double H_Coord_Kr,   // высота (m)

                // Выходные параметры (км)
                ref double X_Coord_Kr,
                ref double Y_Coord_Kr

            )
        {

            int No_tmp;
            double Lo_tmp, Bo_tmp;

            // Вспомогательные величины
            double Xa_tmp, Xb_tmp, Xc_tmp, Xd_tmp;
            double Ya_tmp, Yb_tmp, Yc_tmp;
            double sin2_B, sin4_B, sin6_B;

            // No ********************************************************************
            // Номер шестиградусной зоны в проекци Гаусса-Крюгера (1,2,...)
            // !!! Long - в град

            No_tmp = (int)(6 + Long_Coord_Kr) / 6;

            // ******************************************************************** No

            // Lo ********************************************************************
            // Расстояние от определяемой точки до осевого меридиана зоны, рад

            Lo_tmp = (Long_Coord_Kr - (3 + 6 * (No_tmp - 1))) / 57.29577951;

            // ******************************************************************** Lo

            // Bo ********************************************************************
            // Переводим широту в рад

            Bo_tmp = (Lat_Coord_Kr * Math.PI) / 180;


            sin2_B = Math.Pow(Math.Sin(Bo_tmp), 2);
            sin4_B = Math.Pow(Math.Sin(Bo_tmp), 4);
            sin6_B = Math.Pow(Math.Sin(Bo_tmp), 6);

            // ******************************************************************** Bo

            // X *********************************************************************

            Xa_tmp = Math.Pow(Lo_tmp, 2) *
                     (109500 - 574700 * sin2_B + 863700 * sin4_B -
                      398600 * sin6_B);

            Xb_tmp = Math.Pow(Lo_tmp, 2) *
                     (278194 - 830174 * sin2_B + 572434 * sin4_B -
                      16010 * sin6_B + Xa_tmp);

            Xc_tmp = Math.Pow(Lo_tmp, 2) *
                    (672483.4 - 811219.9 * sin2_B + 5420 * sin4_B -
                     10.6 * sin6_B + Xb_tmp);

            Xd_tmp = Math.Pow(Lo_tmp, 2) *
                    (1594561.25 + 5336.535 * sin2_B + 26.79 * sin4_B +
                    0.149 * sin6_B + Xc_tmp);

            X_Coord_Kr = 6367558.4968 * Bo_tmp -
                         Math.Sin(Bo_tmp * 2) *
                         (16002.89 + 66.9607 * sin2_B + 0.3515 * sin4_B - Xd_tmp);

            // ********************************************************************* X

            // Y *********************************************************************

            Ya_tmp = Math.Pow(Lo_tmp, 2) *
                    (79690 - 866190 * sin2_B + 1730360 * sin4_B -
                     945460 * sin6_B);

            Yb_tmp = Math.Pow(Lo_tmp, 2) *
                    (270806 - 1523417 * sin2_B + 1327645 * sin4_B -
                     21701 * sin6_B + Ya_tmp);

            Yc_tmp = Math.Pow(Lo_tmp, 2) *
                     (1070204.16 - 2136826.66 * sin2_B + 17.98 * sin4_B -
                      11.99 * sin6_B + Yb_tmp);

            Y_Coord_Kr = (5 + 10 * No_tmp) * 100000 +
                         Lo_tmp * Math.Cos(Bo_tmp) *
                         (6378245 + 21346.1415 * sin2_B + 107.159 * sin4_B +
                          0.5977 * sin6_B + Yc_tmp);

            // ********************************************************************* Y

            // .......................................................................
            // перевод в км

            X_Coord_Kr = X_Coord_Kr / 1000;
            Y_Coord_Kr = Y_Coord_Kr / 1000;
            // .......................................................................


        } // Функция f_SK42_Krug
        //************************************************************************

        // ***********************************************************************
        // Отрисовка траектории N фиктивных точек по пеленгу
        //
        // Входные параметры: 
        // Theta_Pel(град) - пеленг,
        // Mmax_Pel(км) - максимальная дальность отображения пеленга,
        // NumberFikt_Pel - количество фиктивных точек
        // LatP_Pel,LongP_Pel (град) - широта и долгота стояния пеленгатора 

        //
        // Выходные параметры: 
        // массив mas_Pel(R(км),Latitude(град),Longitude(град)) ->
        // дальность, широта, долгота
        // массив mas_Pel_XYZ (км) -> координаты в опорной геоцентрической СК
        // XP_Pel,YP_Pel,ZP_Pel (км) -координаты пеленгатора в опорной геоцентрической СК
        // XFN_Pel,YFN_Pel,ZFN_Pel (км) -координаты N-й фиктивной точки в опорной 
        // геоцентрической СК

        // Широта -90(юг)...+90(север)
        // Долгота -180(на запад от Гринвича)...+180(на восток от Гринвича)
        // Пеленг 0...360(по часовой стрелке)
        // ***********************************************************************
        public void f_Peleng
            (

                 // Пеленг
                 double Theta_Pel,
            // Max дальность отображения пеленга
                 double Mmax_Pel,
            // Количество фиктивных точек в плоскости пеленгования пеленгатора
                 uint NumbFikt_Pel,

                 // Широта и долгота стояния пеленгатора
                 double LatP_Pel,
                 double LongP_Pel,

                 // Координаты пеленгатора в опорной геоцентрической СК
                 ref double XP_Pel,
                 ref double YP_Pel,
                 ref double ZP_Pel,

                 // Координаты N-й фиктивной точки в опорной геоцентрической СК
                 ref double XFN_Pel,
                 ref double YFN_Pel,
                 ref double ZFN_Pel,

                 // Координаты фиктивных точек
                 ref double[] mas_Pel,
                 ref double[] mas_Pel_XYZ

                 // ForOtl
            // Time of model
            //double *pT,
            // Discrete of time
            //double *pdt,
            //double *pmas_Otl_Pel        


            )
        {

            // .........................................................................
            //double yyy;
            //int jjj;
            //int lngp_i, lngi_i, lngpi_i;
            //double lngp_d, lngi_d, lngpi_d;


            //jjj = 0;
            //lngp_i = 0;
            //lngp_d = 0;
            //lngi_i = 0;
            //lngi_d = 0;
            //lngpi_i = 0;
            //lngpi_d = 0;
            // .........................................................................


            // Initial conditions ****************************************************
            // Initial conditions

            if (fl_first_Pel == 0)
            {

                // ...............................................................
                // Радиус Земли  в точке стояния пеленгатора с учетом текущей широты 

                // m
                //REarthP_Pel = REarth_Pel*(1.-0.003352*sin(*pLatP_Pel)*sin(*pLatP_Pel));
                REarthP_Pel = REarth_Pel; // Шар
                // ...............................................................

                fl_first_Pel = 1;
                // ...............................................................
                // Широта и долгота стояния пеленгатора

                LatP_Pel_tmp = (LatP_Pel * Math.PI) / 180;   // grad->rad
                LongP_Pel_tmp = (LongP_Pel * Math.PI) / 180;
                // ...............................................................
                // Количество фиктивных точек в плоскости пеленгования пеленгатора

                NumbFikt_Pel_tmp = NumbFikt_Pel;
                // ...............................................................
                // Max дальность отображения пеленга

                Mmax_Pel_tmp = Mmax_Pel * 1000; // km-> m
                // ...............................................................
                // Координаты пеленгатора в опорной геоцентрической СК

                // m
                XP_Pel_tmp = REarthP_Pel * Math.Cos(LatP_Pel_tmp) * Math.Cos(LongP_Pel_tmp);
                YP_Pel_tmp = REarthP_Pel * Math.Cos(LatP_Pel_tmp) * Math.Sin(LongP_Pel_tmp);
                ZP_Pel_tmp = REarthP_Pel * Math.Sin(LatP_Pel_tmp);

                // km
                XP_Pel = XP_Pel_tmp / 1000;
                YP_Pel = YP_Pel_tmp / 1000;
                ZP_Pel = ZP_Pel_tmp / 1000;
                // ...............................................................
                // Пеленг

                // grad -> rad
                Theta_Pel_tmp = (Theta_Pel * Math.PI) / 180;
                // ...............................................................

                // ...............................................................
                // VAR2

                V_Pel = Mmax_Pel_tmp / NumbFikt_Pel;
                Lat_Pel_N1 = LatP_Pel_tmp;
                Long_Pel_N1 = LongP_Pel_tmp;
                fl_var2 = 0;
                fl1_var2 = 0;
                fl2_var2 = 0;
                // ...............................................................

            }
            // **************************************************** Initial conditions

            while (IndMas_Pel < (NumbFikt_Pel))
            {

/*
                
                        // VAR1 *******************************************************************
                        //yyy = Math.Acos((Math.Sin(LATi_Pel) -Math.Sin(LatP_Pel_tmp)*Math.Cos(Alpha))/
                        //(Math.Cos(LatP_Pel_tmp)*Math.Sin(Alpha)));
                        //yyy = Math.Acos(Math.Sin(LatP_Pel)/Math.Cos(LATi_Pel)); 

                        // Mi ********************************************************************
                        // Расстояние по поверхности земли от точки стояния пеленгатора до текущей 
                        // фиктивной точки (Mi-дуга болшьшого круга с центральным углом Mi/Rз радиан)

                        Mi_Pel = (((double)IndFP_Pel) * Mmax_Pel_tmp) / ((double)(NumbFikt_Pel_tmp));

                        // Центральный угол дуги Mi
                        Alpha = Mi_Pel / REarthP_Pel;
                        // ******************************************************************** Mi

                        // ***********************************************************************
                        if (Alpha >= Math.PI / 2)
                        {

                            jjj += 1;

                            // A2
                            yyy = Math.Acos(Math.Sin(LatP_Pel_tmp) / Math.Cos(LATi_Pel)); // Pel=45grad
                            //yyy = Math.Acos(Math.Sin(LATi_Pel) / Math.Cos(LatP_Pel)); // Pel=45grad
                            //yyy = Math.Asin((1 / Math.Tan(Math.PI / 2 - LatP_Pel)) /
                            //(1 / Math.Tan(Theta_Pel_tmp))); // Pel=45grad

                            NumbFikt_Pel_tmp = NumbFikt_Pel_tmp - IndFP_Pel;
                            IndFP_Pel = 1;
                            Mmax_Pel_tmp = Mmax_Pel_tmp - Mi_Pel;
                            LatP_Pel_tmp = LATi_Pel;
                            LongP_Pel_tmp = LONGi_Pel;
                            //Theta_Pel_tmp = Math.PI - Theta_Pel_tmp;
                            Mi_Pel = (((double)IndFP_Pel) * Mmax_Pel_tmp) / ((double)(NumbFikt_Pel_tmp));
                            Alpha = Mi_Pel / REarthP_Pel;

                            //Theta_Pel_tmp = yyy;

                            // Pel<90 and !=0
                            if ((Theta_Pel < Math.PI / 2) && (Theta_Pel != 0))
                                Theta_Pel_tmp = Math.PI - yyy;  // Pel=45grad

                            // Pel=0
                            else if (Theta_Pel == 0)
                            {
                                //if (jjj<2)
                                //    Theta_Pel_tmp = 0;
                                //else
                                //    Theta_Pel_tmp = Math.PI;

                                lngp_i = (int)(LongP_Pel * 1000);
                                lngp_d = ((double)lngp_i)/1000;
                                lngi_i = (int)(LONGi_Pel * 1000);
                                lngi_d = ((double)lngi_i) / 1000;
                                lngpi_i = (int)(Math.PI * 1000);
                                lngpi_d = ((double)lngpi_i) / 1000;


                               if(lngi_d == lngp_d )
                                  Theta_Pel_tmp = 0;
                               else if (lngi_d == (lngp_d+lngpi_d))
                                   Theta_Pel_tmp = Math.PI;
                               else
                                   Theta_Pel_tmp = 0;
                            }

                            // Pel=90
                            else if (Theta_Pel == Math.PI/2)
                            {
                                Theta_Pel_tmp = Math.PI / 2;
                            }

                            //Theta_Pel_tmp = Math.PI + yyy;   // Pel=315grad
                            //else
                            //Theta_Pel_tmp = yyy;    // Pel=135grad
                            //Theta_Pel_tmp = Math.PI / 2 + Theta_Pel_tmp; 


                        } // Alpha >= Math.PI / 2
                        // ***********************************************************************

                        // Di ********************************************************************
                        // Расстояние по прямой от точки стояния пеленгатора до пересечения прямой
                        // (проходящей через текущую фиктивную точку и начало координат)и плоскости,
                        // касательной в точке стояния пеленгатора

                        //Di_Pel = REarthP_Pel * Math.Tan(Alpha);
                        //Di_Pel = REarthP_Pel * module(Math.Tan(Mi_Pel / REarthP_Pel));
                        Di_Pel = REarthP_Pel * module(Math.Tan(Alpha));

                        // ******************************************************************** Di

                        // XiYiZi ****************************************************************
                        // Координаты точки пересечения прямой (проходящей через текущую фиктивную
                        // точку и начало координат) и плоскости касательной в точке стояния 
                        // пеленгатора в СК пеленгатора

                        Xi_Pel = REarthP_Pel;
                        Yi_Pel = Di_Pel * Math.Sin(Theta_Pel_tmp);
                        Zi_Pel = Di_Pel * Math.Cos(Theta_Pel_tmp);

                        // **************************************************************** XiYiZi

                        // Xi0Yi0Zi0 *************************************************************
                        // Координаты точки пересечения прямой (проходящей через текущую фиктивную
                        // точку и начало координат) и плоскости касательной в точке стояния 
                        // пеленгатора в СК1 (Поворот СКП на угол=широте пеленгатора)

                        Xi0_Pel = Xi_Pel * Math.Cos(LatP_Pel_tmp) - Zi_Pel * Math.Sin(LatP_Pel_tmp);
                        Yi0_Pel = Yi_Pel;
                        Zi0_Pel = Xi_Pel * Math.Sin(LatP_Pel_tmp) + Zi_Pel * Math.Cos(LatP_Pel_tmp);

                        // ************************************************************* Xi0Yi0Zi0

                        // XiG1YiG1ZiG1 **********************************************************
                        // Координаты точки пересечения прямой (проходящей через текущую фиктивную
                        // точку и начало координат) и плоскости касательной в точке стояния 
                        // пеленгатора в опорной геоцентрической СК (Поворот СК1 на угол=долготе 
                        // пеленгатора)

                        // !!!рабочий вариант
                        XiG1_Pel = Xi0_Pel * Math.Cos(LongP_Pel_tmp) - Yi0_Pel * Math.Sin(LongP_Pel_tmp);
                        YiG1_Pel = Xi0_Pel * Math.Sin(LongP_Pel_tmp) + Yi0_Pel * Math.Cos(LongP_Pel_tmp);

                        //XiG1_Pel = Xi0_Pel * Math.Cos(LongP_Pel_tmp) + Yi0_Pel * Math.Sin(LongP_Pel_tmp);
                        //YiG1_Pel = -Xi0_Pel * Math.Sin(LongP_Pel_tmp) + Yi0_Pel * Math.Cos(LongP_Pel_tmp);
                        ZiG1_Pel = Zi0_Pel;

                        // ********************************************************** XiG1YiG1ZiG1

                        // RiLATiLONGi ***********************************************************
                        // Угловые координаты фиктивной точки в опорной геоцентрической СК

                        Ri1_Pel = Math.Sqrt(XiG1_Pel * XiG1_Pel + YiG1_Pel * YiG1_Pel + ZiG1_Pel * ZiG1_Pel);

                        LATi_Pel = Math.Asin(ZiG1_Pel / Ri1_Pel);

                        Def_Longitude_180(
                        //Def_Longitude(

                                      XiG1_Pel,
                                      YiG1_Pel,
                                      Ri1_Pel,
                                      LATi_Pel,
                                      ref LONGi_Pel
                                     );


                        XiG_Pel = REarthP_Pel * Math.Cos(LATi_Pel) * Math.Cos(LONGi_Pel);
                        YiG_Pel = REarthP_Pel * Math.Cos(LATi_Pel) * Math.Sin(LONGi_Pel);
                        ZiG_Pel = REarthP_Pel * Math.Sin(LATi_Pel);
                        Ri_Pel = Math.Sqrt(XiG_Pel * XiG_Pel + YiG_Pel * YiG_Pel + ZiG_Pel * ZiG_Pel);

                        // *********************************************************** RiLATiLONGi

                        // ******************************************************************* VAR1
                
*/





                // VAR2 ******************************************************************
                // !!!VAR2

                // ......................................................................
                // LAT

                if ((fl_var2 == 0) && (fl2_var2 == 0))
                    Lat_Pel_N = Lat_Pel_N1 + (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;

                // Перешли через северный полюс
                else if ((fl_var2 == 1) && (fl2_var2 == 0) && (Math.Cos(Theta_Pel_tmp) >= 0))
                    Lat_Pel_N = Lat_Pel_N1 - (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;
                else if ((fl_var2 == 1) && (fl2_var2 == 0) && (Math.Cos(Theta_Pel_tmp) < 0))
                    Lat_Pel_N = Lat_Pel_N1 + (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;

                // Перешли через южный полюс 
                else if ((fl2_var2 == 1) && (fl_var2 == 0) && (Math.Cos(Theta_Pel_tmp) >= 0))
                    Lat_Pel_N = Lat_Pel_N1 + (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;
                else if ((fl2_var2 == 1) && (fl_var2 == 0) && (Math.Cos(Theta_Pel_tmp) < 0))
                    Lat_Pel_N = Lat_Pel_N1 - (V_Pel * Math.Cos(Theta_Pel_tmp)) / REarthP_Pel;


                // Перешли через северный полюс
                if (
                    (fl_var2 == 0) &&
                    (Lat_Pel_N > 0) &&
                    (Lat_Pel_N >= Math.PI / 2)
                   )
                {
                    Lat_Pel_N = Math.PI / 2;

                    fl_var2 = 1;
                    fl2_var2 = 0;
                    fl1_var2 = 0;

                }

                // Перешли через южный полюс
                if (
                    (fl2_var2 == 0) &&
                    (Lat_Pel_N < 0) &&
                    (Lat_Pel_N <= -Math.PI / 2)
                   )
                {
                    Lat_Pel_N = -Math.PI / 2;
                    //Theta_Pel = 0;

                    fl2_var2 = 1;
                    fl_var2 = 0;
                    fl1_var2 = 0;

                }

                // .......................................................................
                // LONG

                // Перешли через полюс
                if (
                    (fl1_var2 == 0) &&
                    ((fl_var2 == 1) || (fl2_var2 == 1))
                   )
                {
                    if (Long_Pel_N1 >= 0)
                        Long_Pel_N1 = Long_Pel_N1 + Math.PI;
                    else if (Long_Pel_N1 < 0)
                        Long_Pel_N1 = Long_Pel_N1 - Math.PI;

                    if (Long_Pel_N1 > Math.PI)
                        Long_Pel_N1 = -(2 * Math.PI - Long_Pel_N1);
                    else if (Long_Pel_N1 < -Math.PI)
                        Long_Pel_N1 = 2 * Math.PI + Long_Pel_N1;


                    fl1_var2 = 1;
                }


                // Обычный расчет
                Long_Pel_N = Long_Pel_N1 +
                            (V_Pel * Math.Sin(Theta_Pel_tmp)) / (REarthP_Pel * Math.Cos(Lat_Pel_N1));

                if (Long_Pel_N > Math.PI)
                    Long_Pel_N = -(2 * Math.PI - Long_Pel_N);
                else if (Long_Pel_N < -Math.PI)
                    Long_Pel_N = 2 * Math.PI + Long_Pel_N;
                // .......................................................................

                // ........................................................................
                XiG_Pel = REarthP_Pel * Math.Cos(Lat_Pel_N) * Math.Cos(Long_Pel_N);
                YiG_Pel = REarthP_Pel * Math.Cos(Lat_Pel_N) * Math.Sin(Long_Pel_N);
                ZiG_Pel = REarthP_Pel * Math.Sin(Lat_Pel_N);
                Ri_Pel = Math.Sqrt(XiG_Pel * XiG_Pel + YiG_Pel * YiG_Pel + ZiG_Pel * ZiG_Pel);
                // ........................................................................

                Lat_Pel_N1 = Lat_Pel_N;
                Long_Pel_N1 = Long_Pel_N;
                // ****************************************************************** VAR2


                // Массивы ***************************************************************
                // Занесение в массивы

                // Ri,LATi,LONGi (км, град)
                mas_Pel[IndMas_Pel * 3] = Ri_Pel / 1000;

                // VAR1
                //mas_Pel[IndMas_Pel * 3 + 1] = (LATi_Pel*180)/Math.PI;
                //mas_Pel[IndMas_Pel * 3 + 2] = (LONGi_Pel*180)/Math.PI;

                // VAR2
                mas_Pel[IndMas_Pel * 3 + 1] = (Lat_Pel_N * 180) / Math.PI;
                mas_Pel[IndMas_Pel * 3 + 2] = (Long_Pel_N * 180) / Math.PI;


                // XYZ в опорной геоцентрической СК (км)
                mas_Pel_XYZ[IndMas_Pel * 3] = XiG_Pel / 1000;
                mas_Pel_XYZ[IndMas_Pel * 3 + 1] = YiG_Pel / 1000;
                mas_Pel_XYZ[IndMas_Pel * 3 + 2] = ZiG_Pel / 1000;

                // *************************************************************** Массивы

                // Вывод для отладки ****************************************************

                //*(pmas_Otl_Pel+IndMas_Pel) = Di_Pel;

                // **************************************************** Вывод для отладки`

                // ***********************************************************************
                // Переход к следующей фиктивной точке

                IndFP_Pel += 1;
                IndMas_Pel += 1;
                // ***********************************************************************

            }; // WHILE

            // km
            XFN_Pel = XiG_Pel / 1000;
            YFN_Pel = YiG_Pel / 1000;
            ZFN_Pel = ZiG_Pel / 1000;

        } // Функция f_Peleng
        //************************************************************************

        // *************************************************************************
        // Расчет азимута ИРИ относительно СП
        // dY,dX - разность координат между ИРИ и СП на плоскости XOY (dX=Xири-Xсп...)
        // Условно считаем Y - направление на север

        // Возврат: азимут в градусах (0...360 по часовой стрелке)
        // *************************************************************************

        public double f_Def_Azimuth(
                                   double dY,
                                   double dX
                                      )
        {
            double Beta, Beta_tmp;

            Beta = 0;
            Beta_tmp = 0;
            // ------------------------------------------------------------------------
            if (dY != 0)
                Beta_tmp = Math.Atan(module(dX) / module(dY));
            // -------------------------------------------------------------------------
            if ((dX == 0) && (dY >= 0))
                Beta = 0;
            // -------------------------------------------------------------------------
            else if ((dX == 0) && (dY < 0))
                Beta = Math.PI;
            // ------------------------------------------------------------------------
            else if ((dY == 0) && (dX > 0))
                Beta = Math.PI / 2;
            // -------------------------------------------------------------------------
            else if ((dY == 0) && (dX < 0))
                Beta = 3 * Math.PI / 2;
            // -------------------------------------------------------------------------
            else if ((dY > 0) && (dX > 0))
                Beta = Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY > 0) && (dX < 0))
                Beta = 2 * Math.PI - Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY < 0) && (dX > 0))
                Beta = Math.PI - Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY < 0) && (dX < 0))
                Beta = Math.PI + Beta_tmp;
            // -------------------------------------------------------------------------
            // Перевод в градусы

            Beta = (Beta * 180) / Math.PI;
            // -------------------------------------------------------------------------

            return Beta;

        } // P/P f_Def_Azimuth
        // *************************************************************************

        // ***********************************************************************
        // Расчет долготы
        // [0...180] к востоку от Гринвича
        // ]0...-180[ к западу от Гринвича
        // Широта -90(юг)...+90(север)
        // ***********************************************************************

        public void Def_Longitude_180(
                            double X,
                            double Y,
                            double R,
                            double Lat, // rad
                            ref double Long

                                  )
        {
            double Long1;

            // -------------------------------------------------------------------
            if (module(R * Math.Cos(Lat)) > 1E-10)
                Long1 = Math.Asin(module(Y) / (R * Math.Cos(Lat)));
            else
                Long1 = Math.Asin(module(Y) / 1E-10);

            // -------------------------------------------------------------------
            if ((Y == 0) && (X > 0))
                Long = 0;
            // -------------------------------------------------------------------
            else if ((Y == 0) && (X < 0))
                Long = Math.PI;
            // -------------------------------------------------------------------
            else if ((Y > 0) && (X > 0))
                Long = Long1;
            // -------------------------------------------------------------------
            else if ((Y < 0) && (X > 0))
                Long = -Long1;
            // -------------------------------------------------------------------
            else if ((Y > 0) && (X < 0))
                Long = Math.PI - Long1;
            // ------------------------------------------------------------------
            else // X<0 Y<0
                Long = -(Math.PI - Long1);
            // -------------------------------------------------------------------

        } // Функция Def_Longitude_180
        // ***********************************************************************

        // ***********************************************************************
        // Расчет долготы
        // 0...360 к востоку от Гринвича
        // ***********************************************************************

        public void Def_Longitude(
                            double X,
                            double Y,
                            double R,
                            double Lat,
                            ref double Long

                                  )
        {
            double Long1;

            // -------------------------------------------------------------------
            if (module(R * Math.Cos(Lat)) > 1E-10)
                Long1 = Math.Asin(module(Y) / (R * Math.Cos(Lat)));
            else
                Long1 = Math.Asin(module(Y) / 1E-10);

            // -------------------------------------------------------------------
            if ((Y == 0) && (X > 0))
                Long = 0;
            // -------------------------------------------------------------------
            else if ((Y == 0) && (X < 0))
                Long = Math.PI;
            // -------------------------------------------------------------------
            else if ((Y > 0) && (X > 0))
                Long = Long1;
            // -------------------------------------------------------------------
            else if ((Y < 0) && (X > 0))
                Long = 2 * Math.PI - Long1;
            // -------------------------------------------------------------------
            else if ((Y > 0) && (X < 0))
                Long = Math.PI - Long1;
            // ------------------------------------------------------------------
            else // X<0 Y<0
                Long = Math.PI + Long1;
            // -------------------------------------------------------------------

        } // Функция Def_Longitude
        // ***********************************************************************

        // ***********************************************************************
        // Расчет сектора
        // Beta1 - азимут начальной точки (grad)
        // 0...360 по часовой стрелке
        // ***********************************************************************

        public double f_Sect(
                            double Beta1,
                            double Beta2
                                  )
        {
            double Sect;
            Sect = 0;
            // -------------------------------------------------------------------
            if (Beta1 == Beta2)
                Sect = 0;
            // -------------------------------------------------------------------
            else if (Beta1==0)
                Sect=Beta2;
            // -------------------------------------------------------------------
            else if (Beta2<Beta1)
                Sect=360-(Beta1-Beta2);
            // -------------------------------------------------------------------
            else // Beta2>Beta1
                Sect=Beta2-Beta1;
            // -------------------------------------------------------------------

            return Sect;

        } // Функция Def_Longitude
        // ***********************************************************************

        // ОТРИСОВКА DRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWD

        // ***********************************************************************
        // Самолет по долготе и широте (Geo,grad)
        // ***********************************************************************
        public static void f_DrawAirPlane(
                                  double Lat_air,
                                  double Long_air,
                                  double Angle_air
                                         )
        {
/*
            double LatDX, LongDY;

            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            Pen penRed = new Pen(Color.Red, 2);
            Brush brushRed = new SolidBrush(Color.Red);

            // -------------------------------------------------------------------------------------
            // grad->rad
            LatDX = (Lat_air * Math.PI) / 180;
            LongDY = (Long_air * Math.PI) / 180;

            // Подаем rad, получаем там же расстояние на карте в м
            mapGeoToPlane(GlobalVarLn.hmapl, ref LatDX, ref LongDY);

            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref LatDX, ref LongDY);
            // -------------------------------------------------------------------------------------
            // -------------------------------------------------------------------------------------
            // Самолет

            if (graph != null)
            {

                // Куда поместим рисунок (!!! Изображение д.б. не меньше прямоугольника)
                Rectangle rec = new Rectangle((int)LatDX - GlobalVarLn.axMapScreenGlobal.MapLeft - GlobalVarLn.width_air/2,
                                              (int)LongDY - GlobalVarLn.axMapScreenGlobal.MapTop - GlobalVarLn.height_air/2,
                                              GlobalVarLn.width_air,
                                              GlobalVarLn.height_air
                                             );

                Icon icon1 = new Icon("airplaneBlue.ico");

                //0206
               // Bitmap bmp = icon1.ToBitmap();
               // graph.DrawImage(f_rotateImage(bmp, Angle_air), rec);

            }
            // -------------------------------------------------------------------------------------
*/
        } // Функция f_DrawAirPlane
        // ***********************************************************************


        // ***********************************************************************
        // Самолет по долготе и широте (Geo,grad)
        // ***********************************************************************
        //0206

        public static void f_DrawAirPlane_Tab(
                                  double Lat_air,
                                  double Long_air,
                                  double Angle_air,
                                  int Num_air
                                         )
        {
/*
            double LatDX, LongDY;

            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            Pen penRed = new Pen(Color.Red, 2);
            Brush brushRed = new SolidBrush(Color.Red);

            // -------------------------------------------------------------------------------------
            // grad->rad
            LatDX = (Lat_air * Math.PI) / 180;
            LongDY = (Long_air * Math.PI) / 180;

            // Подаем rad, получаем там же расстояние на карте в м
            mapGeoToPlane(GlobalVarLn.hmapl, ref LatDX, ref LongDY);

            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref LatDX, ref LongDY);
            // -------------------------------------------------------------------------------------
            // Самолет

            if (graph != null)
            {

                // Куда поместим рисунок (!!! Изображение д.б. не меньше прямоугольника)
                Rectangle rec = new Rectangle((int)LatDX - GlobalVarLn.axMapScreenGlobal.MapLeft - GlobalVarLn.width_air / 2,
                                              (int)LongDY - GlobalVarLn.axMapScreenGlobal.MapTop - GlobalVarLn.height_air / 2,
                                              GlobalVarLn.width_air,
                                              GlobalVarLn.height_air
                                             );

                Icon icon1 = new Icon("airplaneBlue.ico");
                //0206
                Bitmap bmp = icon1.ToBitmap();
                graph.DrawImage(f_rotateImage(bmp, Angle_air), rec);

            }
            // -------------------------------------------------------------------------------------
            // Подпись

            //0206
            Brush brush1 = new SolidBrush(Color.LightBlue);
            Font font = new Font("Arial", 10);
            graph.DrawString(
                             Convert.ToString(Num_air),
                             font,
                             brush1,
                             (int)LatDX - GlobalVarLn.axMapScreenGlobal.MapLeft - GlobalVarLn.width_air / 4,
                             (int)LongDY - GlobalVarLn.axMapScreenGlobal.MapTop + 10
                             );

            // -------------------------------------------------------------------------------------
            */


        } // Функция f_DrawAirPlane
 
        // ***********************************************************************



        // ***********************************************************************
        // Самолет по XY (pix), Angle(grad), Num
        // ***********************************************************************
        public static void f_DrawAirPlaneXY(
                                  double Xp_air,
                                  double Yp_air,
                                  double Angle_air,
                                  int Num_air
                                         )
        {
/*
            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            Pen penRed = new Pen(Color.Red, 2);
            Brush brushRed = new SolidBrush(Color.Red);
            Font font = new Font("Arial", 10);

            // -------------------------------------------------------------------------------------
            // Самолет

            if (graph != null)
            {

                // Куда поместим рисунок (!!! Изображение д.б. не меньше прямоугольника)
                Rectangle rec = new Rectangle((int)Xp_air - GlobalVarLn.axMapScreenGlobal.MapLeft - GlobalVarLn.width_air / 2,
                                              (int)Yp_air - GlobalVarLn.axMapScreenGlobal.MapTop - GlobalVarLn.height_air / 2,
                                              GlobalVarLn.width_air,
                                              GlobalVarLn.height_air
                                             );

                if(Num_air==1)
                {
                 //Icon icon1 = new Icon("airplane106blue.ico");
                 Icon icon1 = new Icon("airplanePink.ico");
                 Bitmap bmp1 = icon1.ToBitmap();
                 graph.DrawImage(f_rotateImage(bmp1, Angle_air), rec);
                }
                else if(Num_air==2)
                {
                    Icon icon2 = new Icon("airplanePurple.ico");
                 Bitmap bmp2 = icon2.ToBitmap();
                 graph.DrawImage(f_rotateImage(bmp2, Angle_air), rec);
                }
                else if (Num_air == 3)
                {
                    //Icon icon3 = new Icon("airplane106.ico");
                    Icon icon3 = new Icon("airplaneYellow.ico");
                    Bitmap bmp3 = icon3.ToBitmap();
                    graph.DrawImage(f_rotateImage(bmp3, Angle_air), rec);
                }
                else if (Num_air == 4)
                {
                    Icon icon4 = new Icon("airplaneBlue.ico");
                    Bitmap bmp4 = icon4.ToBitmap();
                    graph.DrawImage(f_rotateImage(bmp4, Angle_air), rec);
                }
                else if (Num_air == 5)
                {
                    Icon icon5 = new Icon("airplaneRed.ico");
                    Bitmap bmp5 = icon5.ToBitmap();
                    graph.DrawImage(f_rotateImage(bmp5, Angle_air), rec);
                }

                else 
                {
                    Icon icon6 = new Icon("airplaneGreen.ico");
                    Bitmap bmp6 = icon6.ToBitmap();
                    graph.DrawImage(f_rotateImage(bmp6, Angle_air), rec);
                }


            }
            // -------------------------------------------------------------------------------------
            // Подпись

            graph.DrawString(
                             Convert.ToString(Num_air),
                             font,
                             brushRed,
                             (int)Xp_air - GlobalVarLn.axMapScreenGlobal.MapLeft + 3,
                             (int)Yp_air - GlobalVarLn.axMapScreenGlobal.MapTop +3
                             );
  
            // -------------------------------------------------------------------------------------

*/
        } // Функция f_DrawAirPlaneXY
        // ***********************************************************************

        // ***********************************************************************
        // Самолет по XY (pix), Angle(grad), Num
        // ***********************************************************************
        public static void f_DrawAirPlaneXY1(
                                  double Xp_air,
                                  double Yp_air,
                                  double Angle_air,
                                  String sNum_air
                                         )
        {
/*
            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            // -------------------------------------------------------------------------------------
            // Самолет

            if (graph != null)
            {

                // Куда поместим рисунок (!!! Изображение д.б. не меньше прямоугольника)
                Rectangle rec = new Rectangle((int)Xp_air - GlobalVarLn.axMapScreenGlobal.MapLeft - GlobalVarLn.width_air / 2,
                                              (int)Yp_air - GlobalVarLn.axMapScreenGlobal.MapTop - GlobalVarLn.height_air / 2,
                                              GlobalVarLn.width_air,
                                              GlobalVarLn.height_air
                                             );

                Icon icon1 = Properties.Resources.airplaneBlue;

                Bitmap bmp1 = icon1.ToBitmap();
                graph.DrawImage(f_rotateImage(bmp1, Angle_air), rec);

                // -------------------------------------------------------------------------------------
                // Подпись

                Font font2 = new Font(FontFamily.GenericSansSerif, 10.0F, FontStyle.Regular, GraphicsUnit.Pixel);
                Brush brushi = new SolidBrush(Color.White);
                Brush brushj = new SolidBrush(Color.Blue);
                Rectangle rec1 = new Rectangle((int)Xp_air - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                                  (int)Yp_air - GlobalVarLn.axMapScreenGlobal.MapTop + 10,
                                                  40,
                                                  13
                                                 );



                graph.FillRectangle(brushi, rec1);

                graph.DrawString(
                                 sNum_air,
                                 font2,
                                 brushj,
                                 (int)Xp_air - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                 (int)Yp_air - GlobalVarLn.axMapScreenGlobal.MapTop + 11
                                 );


                // -------------------------------------------------------------------------------------

            }
            // -------------------------------------------------------------------------------------
*/
        } // Функция f_DrawAirPlaneXY1
        // ***********************************************************************

        // ***********************************************************************
        // Поворот изображения
        // ***********************************************************************
/*
        public static Bitmap f_rotateImage(Bitmap input, double angle)
        {

            Bitmap result = new Bitmap(input.Width, input.Height);
            Graphics g = Graphics.FromImage(result);
            g.TranslateTransform((float)input.Width / 2, (float)input.Height / 2);
            g.RotateTransform((float)angle);
            g.TranslateTransform(-(float)input.Width / 2, -(float)input.Height / 2);
            g.DrawImage(input, new Point(0, 0));
            return result;

        } // f_rotateImage
 */ 
        // ***********************************************************************

        // ***********************************************************************
        // Добавить самолет
        // ff_first=0 -> 1-й раз в лист
        // ***********************************************************************
/*
        public static void f_AddAirPlane1(
                                  double Lat_air,   // grad
                                  double Long_air,
                                  //int Num_air
                                  String sNum_air,
                                  //AirPlane objAirPlane,
                                  int ff_first
                                  //ref List<AirPlane> list_air
                                         )
        {
            double LatDX = 0;
            double LongDY = 0;
            double LatDX_m = 0;
            double LongDY_m = 0;
            int f = 0;
            double X1 = 0;
            double X2 = 0;
            double Y1 = 0;
            double Y2 = 0;
            double dX = 0;
            double dY = 0;
            double Az = 0;

            int iij = 0;

            double dchislo1 = 0;
            long ichislo1 = 0;
            double dchislo2 = 0;
            long ichislo2 = 0;
            double dchislo3 = 0;
            long ichislo3 = 0;
            double dchislo4 = 0;
            long ichislo4 = 0;

            // -------------------------------------------------------------------------------------

            AirPlane objAirPlane = new AirPlane();

            // -------------------------------------------------------------------------------------
            // Широта,долгота в градусах

            objAirPlane.Lat = Lat_air;
            objAirPlane.Long = Long_air;
            // -------------------------------------------------------------------------------------
            // grad->rad
            LatDX = (Lat_air * Math.PI) / 180;
            LongDY = (Long_air * Math.PI) / 180;

            // Подаем rad, получаем там же расстояние на карте в м
            mapGeoToPlane(MapForm.hmapl1, ref LatDX, ref LongDY);

            LatDX_m = LatDX;
            LongDY_m = LongDY;

            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref LatDX, ref LongDY);
            // -------------------------------------------------------------------------------------
            // расстояние на карте в м

            objAirPlane.X_m = LatDX_m;
            objAirPlane.Y_m = LongDY_m;
            X2 = LatDX_m;
            Y2 = LongDY_m;
            // -------------------------------------------------------------------------------------
            objAirPlane.sNum = String.Copy(sNum_air);
            // -------------------------------------------------------------------------------------
            // 1-ое занесение в лист

            // IF1
            if (ff_first == 0)
            {
                objAirPlane.sh = 1;
                objAirPlane.fl_sh = 1;
                objAirPlane.Angle = 0;

                GlobalVarLn.list_air.Add(objAirPlane);
                GlobalVarLn.Number_air += 1;
                return;

            } // IF1:ff_first == 0
            // -------------------------------------------------------------------------------------
            // НЕ 1-ое занесение в лист

            // IF1
            else
            {
                // ...................................................................................
                // Есть ли самолет с таким же номером

                f = 0;
                for (int i = (GlobalVarLn.Number_air - 1); i >= 0; i--)
                {
                    // ...................................................................................
                    // Есть самолет с таким же номером

                    // IF2
                    if (
                        String.Compare(GlobalVarLn.list_air[i].sNum, objAirPlane.sNum) == 0
                       )
                    {
                        f = 1;

                        ichislo1 = (long)(GlobalVarLn.list_air[i].Lat * 100000);
                        dchislo1 = ((double)ichislo1) / 100000;
                        ichislo2 = (long)(objAirPlane.Lat * 100000);
                        dchislo2 = ((double)ichislo2) / 100000;

                        ichislo3 = (long)(GlobalVarLn.list_air[i].Long * 100000);
                        dchislo3 = ((double)ichislo3) / 100000;
                        ichislo4 = (long)(objAirPlane.Long * 100000);
                        dchislo4 = ((double)ichislo4) / 100000;

                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        // Координаты повторились

                        // IF3
                        if (
                            (dchislo1 == dchislo2) &&
                            (dchislo3 == dchislo4)
                           )
                            return;
                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        // координаты не повторились

                        // IF3
                        else
                        {
                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                            // счетчик вышел

                            // IF4
                            if (GlobalVarLn.list_air[i].sh >= GlobalVarLn.ndraw_air)
                            {

                                X1 = GlobalVarLn.list_air[i].X_m;
                                Y1 = GlobalVarLn.list_air[i].Y_m;
                                // На карте X - вверх
                                dY = X2 - X1;
                                dX = Y2 - Y1;
                                // grad
                                Az = f_Def_Azimuth1(dY, dX);
                                objAirPlane.Angle = Az;

                                objAirPlane.sh = 1;
                                objAirPlane.fl_sh = 2;

                                // FOR2: Убрать все предыдущие
                                for (iij = (GlobalVarLn.Number_air - 1); iij >= 0; iij--)
                                {

                                    if (String.Compare(GlobalVarLn.list_air[iij].sNum, objAirPlane.sNum) == 0)
                                    {
                                        GlobalVarLn.list_air.Remove(GlobalVarLn.list_air[iij]);
                                        GlobalVarLn.Number_air -= 1;
                                    }
                                } // FOR2

                                // Добавили новый
                                GlobalVarLn.list_air.Add(objAirPlane);
                                GlobalVarLn.Number_air += 1;

                                // Убрать с карты
                                GlobalVarLn.axMapScreenGlobal.Repaint();

                                return;

                            } // IF4: счетчик вышел
                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
                            // счетчик не вышел-> Добавить и вычислить азимут

                          // IF4
                            else
                            {
                                objAirPlane.sh = GlobalVarLn.list_air[i].sh + 1;
                                objAirPlane.fl_sh = 2;

                                X1 = GlobalVarLn.list_air[i].X_m;
                                Y1 = GlobalVarLn.list_air[i].Y_m;

                                // На карте X - вверх
                                dY = X2 - X1;
                                dX = Y2 - Y1;

                                // grad
                                Az = f_Def_Azimuth1(dY, dX);

                                objAirPlane.Angle = Az;

                                GlobalVarLn.list_air.Add(objAirPlane);

                                GlobalVarLn.Number_air += 1;
                                return;

                            } // IF4: счетчик не вышел
                            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,


                        } // IF3 координаты не повторились
                        // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                    } // IF2 (есть такой же самолет)
                    // ...................................................................................

                } // FOR
                // ...................................................................................

                // ...................................................................................
                // Такого самолета не нашлось при НЕ первом сеансе получения данных

                if (f == 0)
                {
                    objAirPlane.sh = 1;
                    objAirPlane.fl_sh = 1;
                    objAirPlane.Angle = 0;
                    GlobalVarLn.list_air.Add(objAirPlane);
                    GlobalVarLn.Number_air += 1;
                }
                // ...................................................................................

            } // IF1:НЕ 1-ое занесение в лист
            // -------------------------------------------------------------------------------------

        } // Функция f_AddAirPlane1
 */ 
        // ***********************************************************************

        // ***********************************************************************
        // Удалить все самолеты с этим номером
        // ***********************************************************************
        public static void f_DelAirPlane(
                                  String sNum_air
                                       )
        {
            int iij = 0;

            // FOR1: Убрать все с этим номером
            for (iij = (GlobalVarLn.Number_air - 1); iij >= 0; iij--)
            {

                if (String.Compare(GlobalVarLn.list_air[iij].sNum, sNum_air) == 0)
                {
                    GlobalVarLn.list_air.Remove(GlobalVarLn.list_air[iij]);
                    GlobalVarLn.Number_air -= 1;
                }
            } // FOR1

            if (GlobalVarLn.Number_air==0) // Самолетов не осталось
            {
                GlobalVarLn.fl_AirPlane = 0;
            }

            // Перерисовать карту
            //GlobalVarLn.axMapScreenGlobal.Repaint();


        } // Функция f_DelAirPlane
        // ***********************************************************************

        // ***********************************************************************
        // Удалить все самолеты с этим номером
        // ***********************************************************************
        public static void f_DelAirPlaneAll()
        {
            int iij = 0;

            if (GlobalVarLn.list_air.Count != 0)
            {
                // FOR1: Убрать все 
                for (iij = (GlobalVarLn.Number_air - 1); iij >= 0; iij--)
                {

                        GlobalVarLn.list_air.Remove(GlobalVarLn.list_air[iij]);
                        GlobalVarLn.Number_air -= 1;

                } // FOR1

                GlobalVarLn.Number_air = 0;
                GlobalVarLn.fl_AirPlane = 0;
                // Перерисовать карту
                //GlobalVarLn.axMapScreenGlobal.Repaint();

            } // IF GlobalVarLn.list_air.Count != 0


        } // Функция f_DelAirPlaneAll
        // ***********************************************************************

        // ***********************************************************************
        // Добавить самолет
        // ***********************************************************************
/*
        public static void f_AddAirPlane(
                                  double Lat_air,   // grad
                                  double Long_air,
                                  int Num_air
                                         )
        {
            double LatDX = 0;
            double LongDY = 0;
            double LatDX_m = 0;
            double LongDY_m = 0;
            int f = 0;
            double X1 = 0;
            double X2 = 0;
            double Y1 = 0;
            double Y2 = 0;
            double dX = 0;
            double dY = 0;
            double Az = 0;
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_AirPlane = 1;
            // -------------------------------------------------------------------------------------
            GlobalVarLn.Number_air += 1;
            // ------------------------------------------------------------------------------------
            GlobalVarLn.mass_stAirPlane[GlobalVarLn.Number_air - 1].Lat = Lat_air;
            GlobalVarLn.mass_stAirPlane[GlobalVarLn.Number_air - 1].Long = Long_air;
            // ------------------------------------------------------------------------------------

            // grad->rad
            LatDX = (Lat_air * Math.PI) / 180;
            LongDY = (Long_air * Math.PI) / 180;

            // Подаем rad, получаем там же расстояние на карте в м
            mapGeoToPlane(GlobalVarLn.hmapl, ref LatDX, ref LongDY);
            LatDX_m = LatDX;
            LongDY_m = LongDY;

            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref LatDX, ref LongDY);
            // -------------------------------------------------------------------------------------
            //GlobalVar.mass_stAirPlane[GlobalVar.Number_air - 1].X_p = LatDX;
            //GlobalVar.mass_stAirPlane[GlobalVar.Number_air - 1].Y_p = LongDY;
            GlobalVarLn.mass_stAirPlane[GlobalVarLn.Number_air - 1].X_m = LatDX_m;
            GlobalVarLn.mass_stAirPlane[GlobalVarLn.Number_air - 1].Y_m = LongDY_m;
            X2 = LatDX_m;
            Y2 = LongDY_m;
            GlobalVarLn.mass_stAirPlane[GlobalVarLn.Number_air - 1].Num = Num_air;
            // ------------------------------------------------------------------------------------
            f = 0;
            for (int i = (GlobalVarLn.Number_air - 2); i >= 0; i--)
            {
                if (GlobalVarLn.mass_stAirPlane[i].Num == GlobalVarLn.mass_stAirPlane[GlobalVarLn.Number_air - 1].Num)
                {
                    f = 1;
                    X1 = GlobalVarLn.mass_stAirPlane[i].X_m;
                    Y1 = GlobalVarLn.mass_stAirPlane[i].Y_m;
                    // На карте X - вверх
                    dY = X2 - X1;
                    dX = Y2 - Y1;

                    // grad
                    Az = f_Def_Azimuth1(dY, dX);
                    GlobalVarLn.mass_stAirPlane[GlobalVarLn.Number_air - 1].Angle = Az;
                    break;

                } // IF


            } // FOR

            if (f == 0)
                GlobalVarLn.mass_stAirPlane[GlobalVarLn.Number_air - 1].Angle = 0;


        } // Функция f_AddAirPlane
 */
        // ***********************************************************************

        // ***********************************************************************
        // Перерисовка самолетов
        // ***********************************************************************
/*
        public static void f_ReDrawAirPlane()
        {
            double LatDX = 0;
            double LongDY = 0;


            for (int i = 0; i < GlobalVarLn.Number_air; i++)
            {

                // grad->rad
                LatDX = (GlobalVarLn.mass_stAirPlane[i].Lat * Math.PI) / 180;
                LongDY = (GlobalVarLn.mass_stAirPlane[i].Long * Math.PI) / 180;

                // Подаем rad, получаем там же расстояние на карте в м
                mapGeoToPlane(GlobalVarLn.hmapl, ref LatDX, ref LongDY);

                // Расстояние в м на карте -> пикселы на изображении
                mapPlaneToPicture(GlobalVarLn.hmapl, ref LatDX, ref LongDY);

                f_DrawAirPlaneXY(
                                 //GlobalVar.mass_stAirPlane[i].X_p,
                                 //GlobalVar.mass_stAirPlane[i].Y_p,
                                 LatDX,
                                 LongDY,
                                 GlobalVarLn.mass_stAirPlane[i].Angle,
                                 GlobalVarLn.mass_stAirPlane[i].Num
                                 );

            } // FOR


        } // Функция f_ReDrawAirPlane
 */
        // ***********************************************************************

        // ***********************************************************************
        // Перерисовка самолетов
        // ***********************************************************************
/*
        public static void f_ReDrawAirPlane1()
        {
            double LatDX = 0;
            double LongDY = 0;

            for (int i = 0; i < GlobalVarLn.list_air.Count; i++)
            {

                // grad->rad
                LatDX = (GlobalVarLn.list_air[i].Lat * Math.PI) / 180;
                LongDY = (GlobalVarLn.list_air[i].Long * Math.PI) / 180;

                // Подаем rad, получаем там же расстояние на карте в м
                mapGeoToPlane(GlobalVarLn.hmapl, ref LatDX, ref LongDY);

                // Расстояние в м на карте -> пикселы на изображении
                mapPlaneToPicture(GlobalVarLn.hmapl, ref LatDX, ref LongDY);

                f_DrawAirPlaneXY1(
                                 LatDX,
                                 LongDY,
                                 GlobalVarLn.list_air[i].Angle,
                                 GlobalVarLn.list_air[i].sNum
                                 );

            } // FOR

        } // Функция f_ReDrawAirPlane1
 */
        // ***********************************************************************

        // *************************************************************************
        // Расчет азимута ИРИ относительно СП
        // dY,dX - разность координат между ИРИ и СП на плоскости XOY (dX=Xири-Xсп...)
        // Условно считаем Y - направление на север

        // Возврат: азимут в градусах (0...360 по часовой стрелке)
        // *************************************************************************

        public static double f_Def_Azimuth1(
                                   double dY,
                                   double dX
                                      )
        {
            double Beta, Beta_tmp;

            Beta = 0;
            Beta_tmp = 0;
            // ------------------------------------------------------------------------
            if (dY != 0) 
                Beta_tmp = Math.Atan(module1(dX) / module1(dY));

            // -------------------------------------------------------------------------
            if ((dX == 0) && (dY >= 0))
                Beta = 0;
            // -------------------------------------------------------------------------
            else if ((dX == 0) && (dY < 0))
                Beta = Math.PI;
            // ------------------------------------------------------------------------
            else if ((dY == 0) && (dX > 0))
                Beta = Math.PI / 2;
            // -------------------------------------------------------------------------
            else if ((dY == 0) && (dX < 0))
                Beta = 3 * Math.PI / 2;
            // -------------------------------------------------------------------------
            else if ((dY > 0) && (dX > 0))
                Beta = Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY > 0) && (dX < 0))
                Beta = 2 * Math.PI - Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY < 0) && (dX > 0))
                Beta = Math.PI - Beta_tmp;
            // -------------------------------------------------------------------------
            else if ((dY < 0) && (dX < 0))
                Beta = Math.PI + Beta_tmp;
            // -------------------------------------------------------------------------
            // Перевод в градусы

            Beta = (Beta * 180) / Math.PI;
            // -------------------------------------------------------------------------

            return Beta;

        } // P/P f_Def_Azimuth
        // *************************************************************************

        // *************************************************************************************
        // Module
        // *************************************************************************************
        public static double module1(double a)
        {
            if (a >= 0) return (a);
            else return (-a);

        } // P/P module
        // *************************************************************************************

        // ******************************************************************************************
        // Зона энергодоступности(м на местности)
        // ******************************************************************************************
        public static void f_Map_El_XY_ed(
                                         //Point tpCenterPoint, //0209
                                         long iRadiusZone
                                        )
        {
/*
            double dLeftX = 0;
            double dLeftY = 0;
            double dRightX = 0;
            double dRightY = 0;

            double XC = 0;
            double YC = 0;
            XC = tpCenterPoint.X;
            YC = tpCenterPoint.Y;

            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            Pen penRed1 = new Pen(Color.Red, 2);
            Brush brushRed1 = new SolidBrush(Color.Red);

            Pen pen1 = new Pen(Color.HotPink, 5.0f);
            HatchBrush brush1 = new HatchBrush(HatchStyle.Cross, Color.Linen, Color.Transparent);

            // -------------------------------------------------------------------------------------

            // -------------------------------------------------------------------------------------

            if (graph != null)
            {
                dLeftX = XC - iRadiusZone;
                dLeftY = YC - iRadiusZone;
                dRightX = XC + iRadiusZone;
                dRightY = YC + iRadiusZone;


                mapPlaneToPicture(GlobalVarLn.hmapl, ref XC, ref YC);
                mapPlaneToPicture(GlobalVarLn.hmapl, ref dLeftX, ref dLeftY);
                mapPlaneToPicture(GlobalVarLn.hmapl, ref dRightX, ref dRightY);

                graph.DrawEllipse(pen1, (int)dLeftX - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)dLeftY - GlobalVarLn.axMapScreenGlobal.MapTop,
                                        (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));
                graph.FillEllipse(brush1, (int)dLeftX - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)dLeftY - GlobalVarLn.axMapScreenGlobal.MapTop,
                                        (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));


            }
            // -------------------------------------------------------------------------------------
*/
        } // P/P f_Map_El_XY_ed
        // *************************************************************************************

        // ******************************************************************************************
        // Нарисовать метку по координатам (треугольник)
        //
        // Входные параметры:
        // X - X, m на местности
        // Y - Y, m
        // Color: 1-Red, 2-Blue
        // ******************************************************************************************
        public static void f_Map_Pol_XY_stat(
                                         double X,
                                         double Y,
                                         int Col,
                                         String s
                                        )
        {
/*
            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            Pen penRed1 = new Pen(Color.Red, 2);
            Brush brushRed1 = new SolidBrush(Color.Red);
            Brush brushBlue = new SolidBrush(Color.Blue);

            Font font = new Font("Arial", 7);
            Brush brushSnow = new SolidBrush(Color.Snow);

            // -------------------------------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении

            mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
            // -------------------------------------------------------------------------------------

            if (graph != null)
            {

                Point[] toDraw = new Point[3];
                toDraw[0].X = (int)X - (GlobalVarLn.axMapScreenGlobal.MapLeft + 7);
                toDraw[0].Y = (int)Y - (GlobalVarLn.axMapScreenGlobal.MapTop - 7);
                toDraw[1].X = (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft;
                toDraw[1].Y = (int)Y - (GlobalVarLn.axMapScreenGlobal.MapTop + 7);
                toDraw[2].X = (int)X - (GlobalVarLn.axMapScreenGlobal.MapLeft - 7);
                toDraw[2].Y = (int)Y - (GlobalVarLn.axMapScreenGlobal.MapTop - 7);

                if(Col==1)
                    graph.FillPolygon(brushRed1, toDraw);
                else
                    graph.FillPolygon(brushBlue, toDraw);

            }

            // -------------------------------------------------------------------------------------
            // С подписью

            if (s != "")
            {
                graph.FillRectangle(brushSnow,
                                   (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                   (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 10,
                                   20,
                                   10
                                   );

                // Подпись
                graph.DrawString(
                                 s,
                                 font,
                                 brushRed1,
                                 (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                 (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 10
                                 );
            }
            // -------------------------------------------------------------------------------------
*/
        } // P/P f_Map_Pol_XY_stat
        // *************************************************************************************

        // **********************************************************************
        // Перерисовка "Энергодоступность по УС"
        // **********************************************************************
        public static void f_Map_Redraw_CommPowerAvail()
        {

            if (GlobalVarLn.flCoordSP_comm == 1) // СП выбрана
            {
                ClassMap.f_Map_Pol_XY_stat(
                              GlobalVarLn.XCenter_comm,  // m
                              GlobalVarLn.YCenter_comm,
                              1,
                              ""
                                           );
            }

            if (GlobalVarLn.flCoordYS1_comm == 1) // YS1 выбран
            {
                ClassMap.f_Map_Pol_XY_stat(
                              GlobalVarLn.XPoint1_comm,  // m
                              GlobalVarLn.YPoint1_comm,
                              2,
                              "УС1"
                             );
            }

            if (GlobalVarLn.flCoordYS2_comm == 1) // YS2 выбран
            {
                ClassMap.f_Map_Pol_XY_stat(
                              GlobalVarLn.XPoint2_comm,  // m
                              GlobalVarLn.YPoint2_comm,
                              2,
                              "УС2"
                             );
            }

        } // P/P f_Map_Redraw_CommPowerAvail
        // **********************************************************************

        // ******************************************************************************************
        // Нарисовать метку по координатам (красный прямоугольник)
        //
        // Входные параметры:
        // Lat - широта, град (GEO)
        // Long - долгота, град (GEO)
        // ******************************************************************************************
        public static void f_Map_Rect_LatLong_stat(
                                         double Lat,
                                         double Long
                                        )
        {
/*
            double LatDX, LongDY;

            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            Pen penRed = new Pen(Color.Red, 2);
            Brush brushRed = new SolidBrush(Color.Red);

            // -------------------------------------------------------------------------------------

            // grad->rad
            LatDX = (Lat * Math.PI) / 180;
            LongDY = (Long * Math.PI) / 180;

            // Подаем rad, получаем там же расстояние на карте в м
            mapGeoToPlane(GlobalVarLn.hmapl, ref LatDX, ref LongDY);

            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref LatDX, ref LongDY);
            // -------------------------------------------------------------------------------------

            if (graph != null)
            {
                graph.FillRectangle(brushRed, (int)LatDX - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)LongDY -
                                    GlobalVarLn.axMapScreenGlobal.MapTop, 7, 7);

                // Надпись
                //TitleObject(typeStation, lat, lon);
            }

            // -------------------------------------------------------------------------------------

            //objMapForm1.graph.Dispose();

*/
        } // P/P f_Map_Rect_LatLong_stat
        // *************************************************************************************

        // *************************************************************************************
        // Нарисовать метку по координатам (красный треугольник)
        //
        // Входные параметры:
        // Lat - широта, град (GEO)
        // Long - долгота, град (GEO)
        // ************************************************************************************
        public static void f_Map_Pol_LatLong_stat(
                                         double Lat,
                                         double Long
                                        )
        {
/*
            double LatDX, LongDY;

            // --------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            Pen penRed1 = new Pen(Color.Red, 2);
            Brush brushRed1 = new SolidBrush(Color.Red);

            // --------------------------------------------------------------------------------
            // grad->rad
            LatDX = (Lat * Math.PI) / 180;
            LongDY = (Long * Math.PI) / 180;

            // Подаем rad, получаем там же расстояние на карте в м
            mapGeoToPlane(GlobalVarLn.hmapl, ref LatDX, ref LongDY);

            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref LatDX, ref LongDY);
            // ---------------------------------------------------------------------------------

            if (graph != null)
            {
                Point[] toDraw = new Point[3];
                toDraw[0].X = (int)LatDX - (GlobalVarLn.axMapScreenGlobal.MapLeft + 7);
                toDraw[0].Y = (int)LongDY - (GlobalVarLn.axMapScreenGlobal.MapTop - 7);
                toDraw[1].X = (int)LatDX - GlobalVarLn.axMapScreenGlobal.MapLeft;
                toDraw[1].Y = (int)LongDY - (GlobalVarLn.axMapScreenGlobal.MapTop + 7);
                toDraw[2].X = (int)LatDX - (GlobalVarLn.axMapScreenGlobal.MapLeft - 7);
                toDraw[2].Y = (int)LongDY - (GlobalVarLn.axMapScreenGlobal.MapTop - 7);
                graph.FillPolygon(brushRed1, toDraw);

            }
*/
            // -------------------------------------------------------------------
        } // P/P f_Map_Pol_LatLong_stat

        // ***********************************************************************

        // ***********************************************************************
        // Нарисовать метку по координатам (красный кружок для линии)
        //
        // Входные параметры:
        // Lat - широта, град (GEO)
        // Long - долгота, град (GEO)
        // ***********************************************************************
        public static void f_Map_El_LatLong_stat(
                                              double Lat,
                                              double Long
                                                )
        {
/*
            double LatDX, LongDY;
            // -------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            Pen penRed1 = new Pen(Color.Red, 2);
            Brush brushRed1 = new SolidBrush(Color.Red);
            // -------------------------------------------------------------------

            // grad->rad
            LatDX = (Lat * Math.PI) / 180;
            LongDY = (Long * Math.PI) / 180;

            // Подаем градусы, получаем там же расстояние на карте в км
            mapGeoToPlane(GlobalVarLn.hmapl, ref LatDX, ref LongDY);

            // Расстояние в км на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref LatDX, ref LongDY);
            // -----------------------------------------------------------------
            if (graph != null)
            {
                graph.FillEllipse(brushRed1, (int)LatDX - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)LongDY -
                                  GlobalVarLn.axMapScreenGlobal.MapTop, 5, 5);
            }
            // -------------------------------------------------------------------
*/
        } // P/P f_Map_El_LatLong_stat
        // ***********************************************************************

        // ***********************************************************************
        // Перерисовка пеленга
        // ***********************************************************************
        public static void f_ReDrawPeleng()
        {

            // ......................................................................
            f_Map_Pol_LatLong_stat(
                                 GlobalVarLn.LatP_Pel_stat,
                                 GlobalVarLn.LongP_Pel_stat
                                 );
            // ......................................................................

            while (GlobalVarLn.IndFikt_Pel_stat < (GlobalVarLn.NumbFikt_Pel_stat * 3))
            {
                // .......................................................................
                // Широта, долгота, grad

                f_Map_El_LatLong_stat(
                                     GlobalVarLn.mas_Pel[GlobalVarLn.IndFikt_Pel_stat + 1],
                                     GlobalVarLn.mas_Pel[GlobalVarLn.IndFikt_Pel_stat + 2]
                                     );

                GlobalVarLn.IndFikt_Pel_stat += 3;
                // .......................................................................

            }; // WHILE
            // .......................................................................
            GlobalVarLn.IndFikt_Pel_stat = 0;
            // ...................................................................

        } // Функция f_ReDrawPeleng
        // ***********************************************************************

        // **********************************************************************
        // Нарисовать метку по координатам (красный прямоугольник)
        //
        // Входные параметры:
        // X - X, m на местности
        // Y - Y, m
        // *********************************************************************
        public static void f_Map_Rect_XY_stat(

                                  double X,
                                  double Y
                                  )
        {
/*
            // -----------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            Pen penRed2 = new Pen(Color.Red, 2);
            Brush brushRed2 = new SolidBrush(Color.Red);
            // -----------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
            // -----------------------------------------------------------------

            if (graph != null)
            {
                graph.FillRectangle(brushRed2, (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)Y - 
                                    GlobalVarLn.axMapScreenGlobal.MapTop, 7, 7);
            }
            // -----------------------------------------------------------------
*/
        } // P/P f_Map_Rect_XY_stat
        // *********************************************************************

        // *********************************************************************
        // Нарисовать линию по координатам
        //
        // Входные параметры:
        // X1,X2 - X, m на местности
        // Y1,Y2 - Y, m
        // *********************************************************************
        public static void f_Map_Line_XY_stat(
                                  double X1,
                                  double Y1,
                                  double X2,
                                  double Y2
                                             )
        {
/*
            // -----------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();
            Pen penRed2 = new Pen(Color.Red, 2);
            Brush brushRed2 = new SolidBrush(Color.Red);
            // -----------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref X1, ref Y1);
            mapPlaneToPicture(GlobalVarLn.hmapl, ref X2, ref Y2);
            // -----------------------------------------------------------------
            if (graph != null)
            {
                Point point1 = new Point((int)X1 - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)Y1 - 
                                          GlobalVarLn.axMapScreenGlobal.MapTop);
                Point point2 = new Point((int)X2 - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)Y2 - 
                                          GlobalVarLn.axMapScreenGlobal.MapTop);
                graph.DrawLine(penRed2, point1, point2);
            }
            // -----------------------------------------------------------------
*/
        } // P/P f_Map_Line_XY_stat
        // *********************************************************************

        // ***********************************************************************
        // Перерисовка S
        // ***********************************************************************
        public static void f_ReDrawS_stat()
        {
            // ......................................................................
            if (GlobalVarLn.fl_S1_stat==1)
            {
            f_Map_Rect_XY_stat(
                          GlobalVarLn.X_Coordl1_stat,
                          GlobalVarLn.Y_Coordl1_stat
                         );
            }
            // ......................................................................


            // ......................................................................
            if (GlobalVarLn.fl_S2_stat == 1)
            {
                f_Map_Rect_XY_stat(
                              GlobalVarLn.X_Coordl2_stat,
                              GlobalVarLn.Y_Coordl2_stat
                             );
                f_Map_Line_XY_stat(
                              GlobalVarLn.X_Coordl1_stat,
                              GlobalVarLn.Y_Coordl1_stat,
                              GlobalVarLn.X_Coordl2_stat,
                              GlobalVarLn.Y_Coordl2_stat
                             );
            }
            // ......................................................................

        } // Функция f_ReDrawS_stat
        // ***********************************************************************

        // **********************************************************************
        // Нарисовать метку по координатам (красный прямоугольник) с надписью
        //
        // Входные параметры:
        // X - X, m на местности
        // Y - Y, m
        // *********************************************************************
        public static void f_Map_Rect_XYS_stat(

                                  double X,
                                  double Y,
                                  String s1
                                  )
        {
/*
            // -----------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();
            Brush brushRed2 = new SolidBrush(Color.Red);
            Font font2 = new Font(FontFamily.GenericSansSerif, 10.0F, FontStyle.Regular, GraphicsUnit.Pixel);
            Brush brushj = new SolidBrush(Color.Blue);
            Brush brushi = new SolidBrush(Color.White);

            // -----------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
            // -----------------------------------------------------------------

            if (graph != null)
            {
                graph.FillRectangle(brushRed2, (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)Y -
                                    GlobalVarLn.axMapScreenGlobal.MapTop, 5, 5);

               Rectangle rec1 = new Rectangle((int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 5,
                                               (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 7,
                                                  15,
                                                  13
                                                 );


                if (s1 != "")
                {
                    graph.FillRectangle(brushi, rec1);

                    graph.DrawString(
                                     s1,
                                     font2,
                                     brushj,
                                     (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 5,
                                     (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 8
                                     );
                }
            }
            // -----------------------------------------------------------------
*/
        } // P/P f_Map_Rect_XYS_stat
        // *********************************************************************

        // ************************************************************************
        // функция рисования ЗПВ
/*
        public static void DrawLSR()
        { 
            DrawPolygon(GlobalVarLn.listPointDSR, Color.Yellow);
        }

        public static void DrawPolygon(IReadOnlyList<Point> points, Color color)
        {
            //Point point_tmp=new Point();

            if (points.Count == 0)
            {
                return;
            }
            var listPointPictDSR = new Point[points.Count];

            // в цикле пересчитать координаты на местности
            // в координаты на экране
            for (var i = 0; i < points.Count; i++)
            {
                double addPointX = points[i].X;
                double addPointY = points[i].Y;

                // Расстояние в м на карте -> пикселы на изображении
                var j = mapPlaneToPicture(GlobalVarLn.hmapl, ref addPointX, ref addPointY);

                if (j >= 0)
                {
                    listPointPictDSR[i].X = (int) (addPointX - GlobalVarLn.axMapScreenGlobal.MapLeft);
                    listPointPictDSR[i].Y = (int) (addPointY - GlobalVarLn.axMapScreenGlobal.MapTop);
                }
            }

            // ---------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();
            Pen pen1 = new Pen(color, 2.0f);
            HatchBrush brush1 = new HatchBrush(HatchStyle.Cross, Color.Cornsilk, Color.Transparent);

            graph.DrawPolygon(pen1, listPointPictDSR);
            graph.FillClosedCurve(brush1, listPointPictDSR);
        }
*/
        // ************************************************************************

        // ***********************************************************************
        // SP по XY m
        // ***********************************************************************
        public static void f_DrawSPXY(
                                         double X,
                                         double Y,
                                         String s
                                         )
        {

/*
            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            // -------------------------------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении

            mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
            // -------------------------------------------------------------------------------------

            if (graph != null)
            {

                // Куда поместим рисунок (!!! Изображение д.б. не меньше прямоугольника)
                Rectangle rec = new Rectangle((int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - GlobalVarLn.width_SP / 2,
                                               (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop - GlobalVarLn.height_SP,
                                               GlobalVarLn.width_SP,
                                               GlobalVarLn.height_SP
                                              );



                Bitmap bmp = new Bitmap("1.bmp");


                graph.DrawImage(bmp, rec);
                //graph.DrawImage(pbmp, rec);

                // -------------------------------------------------------------------------------------
                // Подпись

                if (s != "")
                {
                    Font font2 = new Font(FontFamily.GenericSansSerif, 10.0F, FontStyle.Regular, GraphicsUnit.Pixel);
                    Brush brushi = new SolidBrush(Color.White);
                    Brush brushj = new SolidBrush(Color.Blue);
                    Rectangle rec1 = new Rectangle((int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                                      (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 1,
                        //40,
                                                      30,
                                                      13
                                                     );

                    graph.FillRectangle(brushi, rec1);

                    graph.DrawString(
                                     s,
                                     font2,
                                     brushj,
                                     (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                     (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 2
                                     );
                }
                // -------------------------------------------------------------------------------------

            } // graph!=0
*/

        } // Функция f_DrawSPXY
        // ***********************************************************************

       // ******************************************************************
        // SP по XY m
        // ***********************************************************************
        public static void f_DrawSPXY1(
                                         double X,
                                         double Y,
                                         String s,
                                         Bitmap pbmp
                                         )
        {
/*
            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            // -------------------------------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении

            mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
            // -------------------------------------------------------------------------------------

            if (graph != null)
            {

                // Куда поместим рисунок (!!! Изображение д.б. не меньше прямоугольника)
                Rectangle rec = new Rectangle((int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - GlobalVarLn.width_SP / 2,
                                               (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop - GlobalVarLn.height_SP,
                                               GlobalVarLn.width_SP,
                                               GlobalVarLn.height_SP
                                              );


                graph.DrawImage(pbmp, rec);

                // -------------------------------------------------------------------------------------
                // Подпись

                if (s != "")
                {
                    Font font2 = new Font(FontFamily.GenericSansSerif, 10.0F, FontStyle.Regular, GraphicsUnit.Pixel);
                    Brush brushi = new SolidBrush(Color.White);
                    Brush brushj = new SolidBrush(Color.Blue);
                    Rectangle rec1 = new Rectangle((int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                                      (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 1,
                        //40,
                                                      30,
                                                      13
                                                     );

                    //graph.FillRectangle(brushi, rec1);

                    graph.DrawString(
                                     s,
                                     font2,
                                     brushj,
                                     (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                     (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 2
                                     );
                }
                // -------------------------------------------------------------------------------------

            } // graph!=0

*/
        } // Функция f_DrawSPXY1
        // ***********************************************************************

        // ***********************************************************************
        // SP по XY m
        // ***********************************************************************
        public static void f_DrawSPXYV(
                                         double X,
                                         double Y,
                                         String s
                                         )
        {
/*
            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            // -------------------------------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении

            mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
            // -------------------------------------------------------------------------------------

            if (graph != null)
            {

                // Куда поместим рисунок (!!! Изображение д.б. не меньше прямоугольника)
                Rectangle rec = new Rectangle((int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - GlobalVarLn.width_SP / 2,
                                               (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop - GlobalVarLn.height_SP,
                                               GlobalVarLn.width_SP,
                                               GlobalVarLn.height_SP
                                              );

                Bitmap bmp = new Bitmap("5_1.bmp");
                graph.DrawImage(bmp, rec);

                // -------------------------------------------------------------------------------------
                // Подпись

                if (s != "")
                {
                    Font font2 = new Font(FontFamily.GenericSansSerif, 10.0F, FontStyle.Regular, GraphicsUnit.Pixel);
                    Brush brushi = new SolidBrush(Color.White);
                    Brush brushj = new SolidBrush(Color.Blue);
                    Rectangle rec1 = new Rectangle((int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                                      (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 1,
                        //40,
                                                      30,
                                                      13
                                                     );


                    //graph.FillRectangle(brushi, rec1);

                    graph.DrawString(
                                     s,
                                     font2,
                                     brushj,
                                     (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                     (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 2
                                     );
                }
                // -------------------------------------------------------------------------------------

            } // graph!=0

*/
        } // Функция f_DrawSPXYV
        // ***********************************************************************

        // ***********************************************************************
        // SP по XY m
        // ***********************************************************************
        public static void f_DrawSPXYV1(
                                         double X,
                                         double Y,
                                         String s,
                                         Bitmap pbmp
                                         )
        {
/*
            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            // -------------------------------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении

            mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
            // -------------------------------------------------------------------------------------

            if (graph != null)
            {

                // Куда поместим рисунок (!!! Изображение д.б. не меньше прямоугольника)
                Rectangle rec = new Rectangle((int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - GlobalVarLn.width_SP / 2,
                                               (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop - GlobalVarLn.height_SP,
                                               GlobalVarLn.width_SP,
                                               GlobalVarLn.height_SP
                                              );

                Bitmap bmp = new Bitmap("5_1.bmp");
                //graph.DrawImage(bmp, rec);
                graph.DrawImage(pbmp, rec);

                // -------------------------------------------------------------------------------------
                // Подпись

                if (s != "")
                {
                    Font font2 = new Font(FontFamily.GenericSansSerif, 10.0F, FontStyle.Regular, GraphicsUnit.Pixel);
                    Brush brushi = new SolidBrush(Color.White);
                    Brush brushj = new SolidBrush(Color.Blue);
                    Rectangle rec1 = new Rectangle((int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                                      (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 1,
                        //40,
                                                      30,
                                                      13
                                                     );


                    //graph.FillRectangle(brushi, rec1);

                    graph.DrawString(
                                     s,
                                     font2,
                                     brushj,
                                     (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                     (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 2
                                     );
                }
                // -------------------------------------------------------------------------------------

            } // graph!=0

*/
        } // Функция f_DrawSPXYV1
        // ***********************************************************************

        // *********************************************************************
        // Нарисовать линию фронта LF1 по координатам (m на местности)
        // *********************************************************************
        public static void f_LF1_stat()
        {
/*
            double x1 = 0;
            double x2 = 0;
            double y1 = 0;
            double y2 = 0;
            double x1_tmp = 0;
            double x2_tmp = 0;
            double y1_tmp = 0;
            double y2_tmp = 0;

            int i = 0;
            // -----------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();
            //Pen penRed2 = new Pen(Color.Red, 2);
            Pen penRed2 = new Pen(Color.Blue, 5);

            // -----------------------------------------------------------------

              for (i = 0; i < GlobalVarLn.iLF1_stat; i++)
              {

                  if (i >= 1)
                  {

                      x2 = GlobalVarLn.list_LF1[i].X_m;
                      y2 = GlobalVarLn.list_LF1[i].Y_m;
                      x1 = GlobalVarLn.list_LF1[i-1].X_m;
                      y1 = GlobalVarLn.list_LF1[i-1].Y_m;

                      x2_tmp = GlobalVarLn.list_LF1[i].X_m;
                      y2_tmp = GlobalVarLn.list_LF1[i].Y_m;
                      x1_tmp = GlobalVarLn.list_LF1[i-1].X_m;
                      y1_tmp = GlobalVarLn.list_LF1[i-1].Y_m;

                      // Расстояние в м на карте -> пикселы на изображении
                      mapPlaneToPicture(GlobalVarLn.hmapl, ref x1_tmp, ref y1_tmp);
                      mapPlaneToPicture(GlobalVarLn.hmapl, ref x2_tmp, ref y2_tmp);

                      // -----------------------------------------------------------------
                      if (graph != null)
                      {
                          Point point1 = new Point((int)x1_tmp - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)y1_tmp -
                                                    GlobalVarLn.axMapScreenGlobal.MapTop);
                          Point point2 = new Point((int)x2_tmp - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)y2_tmp -
                                                    GlobalVarLn.axMapScreenGlobal.MapTop);
                          graph.DrawLine(penRed2, point1, point2);
                      }
                      // -----------------------------------------------------------------

                  }


              } // FOR

*/
        } // P/P f_LF1_stat
        // *********************************************************************

        // *********************************************************************
        // Нарисовать линию фронта LF2 по координатам (m на местности)
        // *********************************************************************
        public static void f_LF2_stat()
        {
/*
            double x1 = 0;
            double x2 = 0;
            double y1 = 0;
            double y2 = 0;
            double x1_tmp = 0;
            double x2_tmp = 0;
            double y1_tmp = 0;
            double y2_tmp = 0;

            int i = 0;
            // -----------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();
            //Pen penRed2 = new Pen(Color.Blue, 2);
            Pen penRed2 = new Pen(Color.Red, 5);

            // -----------------------------------------------------------------

            for (i = 0; i < GlobalVarLn.iLF2_stat; i++)
            {

                if (i >= 1)
                {

                    x2 = GlobalVarLn.list_LF2[i].X_m;
                    y2 = GlobalVarLn.list_LF2[i].Y_m;
                    x1 = GlobalVarLn.list_LF2[i - 1].X_m;
                    y1 = GlobalVarLn.list_LF2[i - 1].Y_m;

                    x2_tmp = GlobalVarLn.list_LF2[i].X_m;
                    y2_tmp = GlobalVarLn.list_LF2[i].Y_m;
                    x1_tmp = GlobalVarLn.list_LF2[i - 1].X_m;
                    y1_tmp = GlobalVarLn.list_LF2[i - 1].Y_m;

                    // Расстояние в м на карте -> пикселы на изображении
                    mapPlaneToPicture(GlobalVarLn.hmapl, ref x1_tmp, ref y1_tmp);
                    mapPlaneToPicture(GlobalVarLn.hmapl, ref x2_tmp, ref y2_tmp);

                    // -----------------------------------------------------------------
                    if (graph != null)
                    {
                        Point point1 = new Point((int)x1_tmp - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)y1_tmp -
                                                  GlobalVarLn.axMapScreenGlobal.MapTop);
                        Point point2 = new Point((int)x2_tmp - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)y2_tmp -
                                                  GlobalVarLn.axMapScreenGlobal.MapTop);
                        graph.DrawLine(penRed2, point1, point2);
                    }
                    // -----------------------------------------------------------------

                }


            } // FOR
*/

        } // P/P f_LF2_stat
        // *********************************************************************

        // *********************************************************************
        // Нарисовать ZO по координатам (m на местности)
        // *********************************************************************
        public static void f_ZO_stat()
        {
/*
            double x1 = 0;
            double x2 = 0;
            double y1 = 0;
            double y2 = 0;

            double x1_tmp = 0;
            double x2_tmp = 0;
            double y1_tmp = 0;
            double y2_tmp = 0;

            double x3 = 0;
            double y3 = 0;

            int i = 0;
            // -----------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();
            //Pen penRed2 = new Pen(Color.Black, 2);
            Pen penRed2 = new Pen(Color.Black, 5);

            // -----------------------------------------------------------------

            for (i = 0; i < GlobalVarLn.iZO_stat; i++)
            {

                x3 = GlobalVarLn.list_ZO[i].X_m;
                y3 = GlobalVarLn.list_ZO[i].Y_m;

                // if1
                if (i >= 1)
                {
                    x2 = GlobalVarLn.list_ZO[i].X_m;
                    y2 = GlobalVarLn.list_ZO[i].Y_m;
                    x1 = GlobalVarLn.list_ZO[i - 1].X_m;
                    y1 = GlobalVarLn.list_ZO[i - 1].Y_m;

                    x2_tmp = GlobalVarLn.list_ZO[i].X_m;
                    y2_tmp = GlobalVarLn.list_ZO[i].Y_m;
                    x1_tmp = GlobalVarLn.list_ZO[i - 1].X_m;
                    y1_tmp = GlobalVarLn.list_ZO[i - 1].Y_m;

                    // Расстояние в м на карте -> пикселы на изображении
                    mapPlaneToPicture(GlobalVarLn.hmapl, ref x1_tmp, ref y1_tmp);
                    mapPlaneToPicture(GlobalVarLn.hmapl, ref x2_tmp, ref y2_tmp);

                    // -----------------------------------------------------------------
                    if (graph != null)
                    {
                        Point point1 = new Point((int)x1_tmp - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)y1_tmp -
                                                  GlobalVarLn.axMapScreenGlobal.MapTop);
                        Point point2 = new Point((int)x2_tmp - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)y2_tmp -
                                                  GlobalVarLn.axMapScreenGlobal.MapTop);
                        graph.DrawLine(penRed2, point1, point2);
                    }
                    // -----------------------------------------------------------------

                    // if2
                    if (i == (GlobalVarLn.iZO_stat - 1))
                    {
                        x1 = GlobalVarLn.list_ZO[i].X_m;
                        y1 = GlobalVarLn.list_ZO[i].Y_m;
                        x1_tmp = GlobalVarLn.list_ZO[i].X_m;
                        y1_tmp = GlobalVarLn.list_ZO[i].Y_m;
                        x2 = GlobalVarLn.list_ZO[0].X_m;
                        y2 = GlobalVarLn.list_ZO[0].Y_m;
                        x2_tmp = GlobalVarLn.list_ZO[0].X_m;
                        y2_tmp = GlobalVarLn.list_ZO[0].Y_m;
                        mapPlaneToPicture(GlobalVarLn.hmapl, ref x1_tmp, ref y1_tmp);
                        mapPlaneToPicture(GlobalVarLn.hmapl, ref x2_tmp, ref y2_tmp);
                        if (graph != null)
                        {
                            Point point1 = new Point((int)x1_tmp - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)y1_tmp -
                                                      GlobalVarLn.axMapScreenGlobal.MapTop);
                            Point point2 = new Point((int)x2_tmp - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)y2_tmp -
                                                      GlobalVarLn.axMapScreenGlobal.MapTop);
                            graph.DrawLine(penRed2, point1, point2);
                        }


                    } // if2

                } // if1

            } // FOR

*/
        } // P/P f_ZO_stat
        // *********************************************************************

        // **********************************************************************
        // Нарисовать метку по координатам (Black прямоугольник) с надписью
        //
        // Входные параметры:
        // X - X, m на местности
        // Y - Y, m
        // *********************************************************************
        public static void f_Map_Rect_XYS_ZO_stat(

                                  double X,
                                  double Y,
                                  String s1
                                  )
        {
/*
            // -----------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            //Brush brushRed2 = new SolidBrush(Color.Red);
            Brush brushRed2 = new SolidBrush(Color.Black);

            Font font2 = new Font(FontFamily.GenericSansSerif, 10.0F, FontStyle.Regular, GraphicsUnit.Pixel);
            Brush brushj = new SolidBrush(Color.Blue);
            Brush brushi = new SolidBrush(Color.White);

            // -----------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
            // -----------------------------------------------------------------

            if (graph != null)
            {
                graph.FillRectangle(brushRed2, (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)Y -
                                    GlobalVarLn.axMapScreenGlobal.MapTop, 7, 7);

                if (s1 != "")
                {
                    Rectangle rec1 = new Rectangle((int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 5,
                                                    (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 7,
                                                       15,
                                                       13
                                                      );

                    graph.FillRectangle(brushi, rec1);

                    graph.DrawString(
                                     s1,
                                     font2,
                                     brushj,
                                     (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 5,
                                     (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 8
                                     );
                }

            }
            // -----------------------------------------------------------------
*/
        } // P/P f_Map_Rect_XYS_stat
        // *********************************************************************

        // **********************************************************************
        // Нарисовать метку по координатам (Black прямоугольник) с надписью
        //
        // Входные параметры:
        // X - X, m на местности
        // Y - Y, m
        // clr=1-red, 2-Blue
        // *********************************************************************
        public static void f_Map_Rect_XYS_OB_stat(

                                  double X,
                                  double Y,
                                  int clr,
                                  String s1
                                  )
        {
/*
            // -----------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            //Brush brushRed2 = new SolidBrush(Color.Red);
            Brush brushRed2 = new SolidBrush(Color.Red);
            Brush brushRed3 = new SolidBrush(Color.Blue);


            Font font2 = new Font(FontFamily.GenericSansSerif, 10.0F, FontStyle.Regular, GraphicsUnit.Pixel);
            Brush brushj = new SolidBrush(Color.Blue);
            Brush brushi = new SolidBrush(Color.White);

            // -----------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
            // -----------------------------------------------------------------

            if (graph != null)
            {


                if (clr == 1)
                {
                    graph.FillRectangle(brushRed2, (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)Y -
                                        GlobalVarLn.axMapScreenGlobal.MapTop, 7, 7);
                }

                else
                {
                    graph.FillRectangle(brushRed3, (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)Y -
                                        GlobalVarLn.axMapScreenGlobal.MapTop, 7, 7);

                }

                Rectangle rec1 = new Rectangle((int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 5,
                                                (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 7,
                                                   15,
                                                   13
                                                  );

                graph.FillRectangle(brushi, rec1);

                graph.DrawString(
                                 s1,
                                 font2,
                                 brushj,
                                 (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 5,
                                 (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 8
                                 );

            }
            // -----------------------------------------------------------------
*/
        } // P/P f_Map_Rect_XYS_OB_stat
        // *********************************************************************

        // *********************************************************************
        // Нарисовать OB1 по координатам (m на местности)
        // *********************************************************************
        public static void f_OB1_stat()
        {
            double x3 = 0;
            double y3 = 0;
            String s1 = "";
            int i = 0;
            // -----------------------------------------------------------------
            for (i = 0; i < GlobalVarLn.iOB1_stat; i++)
            {

                x3 = GlobalVarLn.list1_OB1[i].X_m;
                y3 = GlobalVarLn.list1_OB1[i].Y_m;
                s1 = GlobalVarLn.list1_OB1[i].sType;

                f_DrawObjXY1(
                          x3,
                          y3,
                          s1,
                   (Bitmap)GlobalVarLn.objFormOB1G.imageList1.Images[GlobalVarLn.list1_OB1[i].indzn],
                         1
                         );

            } // FOR

        } // P/P f_OB1_stat
        // *********************************************************************

        // *********************************************************************
        // Нарисовать OB2 по координатам (m на местности)
        // *********************************************************************
        public static void f_OB2_stat()
        {
            double x3 = 0;
            double y3 = 0;
            int i = 0;
            String s1 = "";
            // -----------------------------------------------------------------
            for (i = 0; i < GlobalVarLn.iOB2_stat; i++)
            {

                x3 = GlobalVarLn.list1_OB2[i].X_m;
                y3 = GlobalVarLn.list1_OB2[i].Y_m;
                s1 = GlobalVarLn.list1_OB2[i].sType;

                f_DrawObjXY1(
                          x3,
                          y3,
                          s1,
                   (Bitmap)GlobalVarLn.objFormOB2G.imageList1.Images[GlobalVarLn.list1_OB2[i].indzn],
                          2
                         );


            } // FOR

        } // P/P f_OB2_stat
        // *********************************************************************

        // ******************************************************************************************
        // Зона подавления(м на местности)
        // ******************************************************************************************
/*
        public static void f_Map_Zon_Suppression(
                                         Point tpCenterPoint,
                                         int clr,
                                         long iRadiusZone
                                        )
        {


            double dLeftX = 0;
            double dLeftY = 0;
            double dRightX = 0;
            double dRightY = 0;

            double XC = 0;
            double YC = 0;
            XC = tpCenterPoint.X;
            YC = tpCenterPoint.Y;

            double x3 = 0;
            double y3 = 0;
            double x4 = 0;
            double y4 = 0;

            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            Pen penRed1 = new Pen(Color.Red, 2);
            Pen penRed2 = new Pen(Color.White, 2);

            Brush brushRed1 = new SolidBrush(Color.Red);
            Brush brushRed2 = new SolidBrush(Color.Blue);

            Pen pen1 = new Pen(Color.HotPink, 5.0f);
            HatchBrush brush1 = new HatchBrush(HatchStyle.Cross, Color.Linen, Color.Transparent);

            HatchBrush brush2 = new HatchBrush(HatchStyle.DiagonalCross, Color.Linen, Color.Transparent);


            Brush brushi = new SolidBrush(Color.White);

            // -------------------------------------------------------------------------------------

            if (graph != null)
            {

                dLeftX = XC - iRadiusZone;
                dLeftY = YC - iRadiusZone;
                dRightX = XC + iRadiusZone;
                dRightY = YC + iRadiusZone;
                mapPlaneToPicture(GlobalVarLn.hmapl, ref XC, ref YC);
                mapPlaneToPicture(GlobalVarLn.hmapl, ref dLeftX, ref dLeftY);
                mapPlaneToPicture(GlobalVarLn.hmapl, ref dRightX, ref dRightY);

                if (clr == 1)
                {

                    graph.DrawEllipse(pen1, (int) dLeftX - GlobalVarLn.axMapScreenGlobal.MapLeft,
                        (int) dLeftY - GlobalVarLn.axMapScreenGlobal.MapTop,
                        (int) (dRightX - dLeftX), (int) (dRightY - dLeftY));
                    graph.FillEllipse(brush1, (int) dLeftX - GlobalVarLn.axMapScreenGlobal.MapLeft,
                        (int) dLeftY - GlobalVarLn.axMapScreenGlobal.MapTop,
                        (int) (dRightX - dLeftX), (int) (dRightY - dLeftY));

                }

                else
                {
                    graph.DrawEllipse(penRed2, (int) dLeftX - GlobalVarLn.axMapScreenGlobal.MapLeft,
                        (int) dLeftY - GlobalVarLn.axMapScreenGlobal.MapTop,
                        (int) (dRightX - dLeftX), (int) (dRightY - dLeftY));

                    if(GlobalVarLn.flA_sup!=3)
                    {
                    graph.FillEllipse(brush2, (int)dLeftX - GlobalVarLn.axMapScreenGlobal.MapLeft,
                        (int)dLeftY - GlobalVarLn.axMapScreenGlobal.MapTop,
                        (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));
                    }
                }


            }
            // -------------------------------------------------------------------------------------

        } // P/P f_Zon_Suppression
 */
        // *************************************************************************************

        // ******************************************************************************************
        // Перерисовка зоны подавления(м на местности)
        // ******************************************************************************************
        public static void f_Map_Redraw_Zon_Suppression()
        {
/*
                DrawPolygon(GlobalVarLn.listControlJammingZone, Color.Red);



                if (GlobalVarLn.flA_sup == 2)
                {
                    f_Map_Zon_Suppression(
                                          GlobalVarLn.tpPointCentre1_sup,
                                          2,
                                          (long)GlobalVarLn.RZ1_sup
                                                  );

                }

                else if (GlobalVarLn.flA_sup == 3) // SP
                {
                    ClassMap.f_Map_Zon_Suppression(
                                      //GlobalVarLn.tpOwnCoordRect_sup,
                                      GlobalVarLn.tpPointCentre1_sup,          
                                                   2,
                                      (long)GlobalVarLn.RZ1_sup
                                                  );

                }
            }
*/




        } // P/P f_Map_Redraw_Zon_Suppression
        // *************************************************************************************

        // ***********************************************************************
        // Object (XY)
        // ***********************************************************************
        public static void f_DrawObjXY(
                                         double X,
                                         double Y,
                                         String s,
                                         int fl
                                         )
        {
/*
            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            // -------------------------------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении

            mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
            // -------------------------------------------------------------------------------------

            if (graph != null)
            {

                // Куда поместим рисунок (!!! Изображение д.б. не меньше прямоугольника)
                Rectangle rec = new Rectangle((int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - GlobalVarLn.width_SP / 2,
                                               //(int)Y - GlobalVarLn.axMapScreenGlobal.MapTop - GlobalVarLn.height_SP / 2,
                                               (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop - GlobalVarLn.height_SP,
                                               GlobalVarLn.width_SP,
                                               GlobalVarLn.height_SP
                                              );
                // -------------------------------------------------------------------------------------

                Bitmap bmp = new Bitmap("1.bmp");
                Bitmap bmp1 = new Bitmap("2.bmp");
                Bitmap bmp2 = new Bitmap("3.bmp");
                Bitmap bmp3 = new Bitmap("4.bmp");
                // -------------------------------------------------------------------------------------
                if(fl==1)
                  graph.DrawImage(bmp, rec);
                else if (fl == 2)
                  graph.DrawImage(bmp1, rec);
                else if (fl == 3)
                  graph.DrawImage(bmp2, rec);
                else if (fl == 4)
                    graph.DrawImage(bmp3, rec);

                // -------------------------------------------------------------------------------------
                // Подпись

                if ((s != "")&&(s!=null))
                {
                    Font font2 = new Font(FontFamily.GenericSansSerif, 10.0F, FontStyle.Regular, GraphicsUnit.Pixel);
                    Brush brushi = new SolidBrush(Color.White);
                    Brush brushj = new SolidBrush(Color.Blue);

                   Rectangle rec1 = new Rectangle(
                                                 //(int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 5,
                                                 (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                                  //(int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 7,
                                                  (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 1,
                                                   30,
                                                   13
                                                  );


                    graph.FillRectangle(brushi, rec1);

                    graph.DrawString(
                                     s,
                                     font2,
                                     brushj,
                                     //(int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 5,
                                     (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                     (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 2
                                     );


                }
                // -------------------------------------------------------------------------------------

            } // graph!=0

*/
        } // Функция f_DrawObjXY
        // ***********************************************************************

        // ***********************************************************************
        // Object (XY)
        // ***********************************************************************
        public static void f_DrawObjXY1(
                                         double X,
                                         double Y,
                                         String s,
                                         Bitmap pbmp,
                                         int fl
                                         )
        {
/*
            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            // -------------------------------------------------------------------------------------
            // Расстояние в м на карте -> пикселы на изображении

            mapPlaneToPicture(GlobalVarLn.hmapl, ref X, ref Y);
            // -------------------------------------------------------------------------------------

            if (graph != null)
            {

                // Куда поместим рисунок (!!! Изображение д.б. не меньше прямоугольника)
                Rectangle rec = new Rectangle((int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - GlobalVarLn.width_SP / 2,
                    //(int)Y - GlobalVarLn.axMapScreenGlobal.MapTop - GlobalVarLn.height_SP / 2,
                                               (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop - GlobalVarLn.height_SP,
                                               GlobalVarLn.width_SP,
                                               GlobalVarLn.height_SP
                                              );
                // -------------------------------------------------------------------------------------

                Bitmap bmp = new Bitmap("1.bmp");
                Bitmap bmp1 = new Bitmap("2.bmp");
                Bitmap bmp2 = new Bitmap("3.bmp");
                Bitmap bmp3 = new Bitmap("4.bmp");
                // -------------------------------------------------------------------------------------
                graph.DrawImage(pbmp, rec);


                // -------------------------------------------------------------------------------------
                // Подпись

                if ((s != "") && (s != null))
                {
                    Font font2 = new Font(FontFamily.GenericSansSerif, 10.0F, FontStyle.Regular, GraphicsUnit.Pixel);
                    Brush brushi = new SolidBrush(Color.White);
                    Brush brushj = new SolidBrush(Color.Blue);
                    Brush brushjj = new SolidBrush(Color.Red);


                    Rectangle rec1 = new Rectangle(
                        //(int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 5,
                                                  (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                        //(int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 7,
                                                   (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 1,
                                                    30,
                                                    13
                                                   );


                    //graph.FillRectangle(brushi, rec1);

                    if (fl == 1)
                    {
                        graph.DrawString(
                                         s,
                                         font2,
                                         brushj,
                            //(int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 5,
                                         (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                         (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 2
                                         );
                    }
                    else
                    {
                        graph.DrawString(
                                         s,
                                         font2,
                                         brushjj,
                            //(int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 5,
                                         (int)X - GlobalVarLn.axMapScreenGlobal.MapLeft - 10,
                                         (int)Y - GlobalVarLn.axMapScreenGlobal.MapTop + 2
                                         );

                    }


                }
                // -------------------------------------------------------------------------------------

            } // graph!=0

*/
        } // Функция f_DrawObjXY1
        // ***********************************************************************

        // ******************************************************************************************
        // Зона обнаружения СП(м на местности)
        // ******************************************************************************************
        public static void f_ZOSP(
                                         double XC,
                                         double YC,
                                         int iRadiusZone
                                        )
        {
/*
            double dLeftX = 0;
            double dLeftY = 0;
            double dRightX = 0;
            double dRightY = 0;

            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            Pen penRed1 = new Pen(Color.Blue, 5.0f);
            //Brush brushRed1 = new SolidBrush(Color.Red);
            //Pen pen1 = new Pen(Color.HotPink, 5.0f);
            //HatchBrush brush1 = new HatchBrush(HatchStyle.Cross, Color.Linen, Color.Transparent);

            // -------------------------------------------------------------------------------------

            if (graph != null)
            {

                dLeftX = XC - iRadiusZone;
                dLeftY = YC - iRadiusZone;
                dRightX = XC + iRadiusZone;
                dRightY = YC + iRadiusZone;

                mapPlaneToPicture(GlobalVarLn.hmapl, ref XC, ref YC);
                mapPlaneToPicture(GlobalVarLn.hmapl, ref dLeftX, ref dLeftY);
                mapPlaneToPicture(GlobalVarLn.hmapl, ref dRightX, ref dRightY);

                graph.DrawEllipse(penRed1, (int)dLeftX - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)dLeftY - GlobalVarLn.axMapScreenGlobal.MapTop,
                                        (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));
                //graph.FillEllipse(brush1, (int)dLeftX - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)dLeftY - GlobalVarLn.axMapScreenGlobal.MapTop,
                //                        (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));


            }
            // -------------------------------------------------------------------------------------
*/
        } // f_ZOSP
        // *************************************************************************************

        // ******************************************************************************************
        // Зона подавления навигации
        // ******************************************************************************************
        public static void f_Z_supnav(double XC, double YC, int iRadiusZone)
        {
/*
            double dLeftX = 0;
            double dLeftY = 0;
            double dRightX = 0;
            double dRightY = 0;

            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            Pen penRed1 = new Pen(Color.Blue, 5.0f);
            Brush brushRed1 = new SolidBrush(Color.Red);
            Pen pen1 = new Pen(Color.HotPink, 5.0f);
            HatchBrush brush1 = new HatchBrush(HatchStyle.Cross, Color.Linen, Color.Transparent);

            // -------------------------------------------------------------------------------------

            if (graph != null)
            {

                dLeftX = XC - iRadiusZone;
                dLeftY = YC - iRadiusZone;
                dRightX = XC + iRadiusZone;
                dRightY = YC + iRadiusZone;

                mapPlaneToPicture(GlobalVarLn.hmapl, ref XC, ref YC);
                mapPlaneToPicture(GlobalVarLn.hmapl, ref dLeftX, ref dLeftY);
                mapPlaneToPicture(GlobalVarLn.hmapl, ref dRightX, ref dRightY);

                graph.DrawEllipse(pen1, (int)dLeftX - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)dLeftY - GlobalVarLn.axMapScreenGlobal.MapTop,
                                        (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));
                graph.FillEllipse(brush1, (int)dLeftX - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)dLeftY - GlobalVarLn.axMapScreenGlobal.MapTop,
                                        (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));


            }
            // -------------------------------------------------------------------------------------
*/
        } // f_Z_supnav
        // *************************************************************************************

        // ******************************************************************************************
        // Зона спуфинга
        // ******************************************************************************************
        // seg1
        public static void f_Z_spf(
                                         double XC,
                                         double YC,
                                         int iRadiusZone1,
                                         int iRadiusZone2

                                        )
        {
/*
            double dLeftX = 0;
            double dLeftY = 0;
            double dRightX = 0;
            double dRightY = 0;
            double XC1 = 0;
            double YC1 = 0;

            XC1 = XC;
            YC1 = YC;
            // -------------------------------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

            Pen penRed1 = new Pen(Color.Blue, 5.0f);
            Brush brushRed1 = new SolidBrush(Color.Red);
            Pen pen1 = new Pen(Color.HotPink, 5.0f);
            HatchBrush brush1 = new HatchBrush(HatchStyle.Cross, Color.Linen, Color.Transparent);

            // -------------------------------------------------------------------------------------

            if (graph != null)
            {

                dLeftX = XC - iRadiusZone1;
                dLeftY = YC - iRadiusZone1;
                dRightX = XC + iRadiusZone1;
                dRightY = YC + iRadiusZone1;

                mapPlaneToPicture(GlobalVarLn.hmapl, ref XC, ref YC);
                mapPlaneToPicture(GlobalVarLn.hmapl, ref dLeftX, ref dLeftY);
                mapPlaneToPicture(GlobalVarLn.hmapl, ref dRightX, ref dRightY);

                graph.DrawEllipse(pen1, (int)dLeftX - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)dLeftY - GlobalVarLn.axMapScreenGlobal.MapTop,
                                        (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));
                graph.FillEllipse(brush1, (int)dLeftX - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)dLeftY - GlobalVarLn.axMapScreenGlobal.MapTop,
                                        (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));


                dLeftX = XC1 - iRadiusZone2;
                dLeftY = YC1 - iRadiusZone2;
                dRightX = XC1 + iRadiusZone2;
                dRightY = YC1 + iRadiusZone2;
                mapPlaneToPicture(GlobalVarLn.hmapl, ref XC1, ref YC1);
                mapPlaneToPicture(GlobalVarLn.hmapl, ref dLeftX, ref dLeftY);
                mapPlaneToPicture(GlobalVarLn.hmapl, ref dRightX, ref dRightY);


                graph.DrawEllipse(penRed1, (int)dLeftX - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)dLeftY - GlobalVarLn.axMapScreenGlobal.MapTop,
                                        (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));


            }
            // -------------------------------------------------------------------------------------
*/
        } // f_Z_spf
        // *************************************************************************************


        // *********************************************************************
        // Screen
        // *********************************************************************
        public static void f_Screen_stat()
        {
/*
            double x1 = 0;
            double y1 = 0;
            double x2 = 0;
            double y2 = 0;

            x1 = GlobalVarLn.X1Scr;
            y1 = GlobalVarLn.Y1Scr;
            x2 = GlobalVarLn.X2Scr;
            y2 = GlobalVarLn.Y2Scr;

            // -----------------------------------------------------------------
            Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();
            Pen penRed2 = new Pen(Color.White, 1);

            // -----------------------------------------------------------------
           // Расстояние в м на карте -> пикселы на изображении
           mapPlaneToPicture(GlobalVarLn.hmapl, ref x1, ref y1);
           mapPlaneToPicture(GlobalVarLn.hmapl, ref x2, ref y2);


        if(x2>=x1)  x2=x2-x1;
        else        x2=x1-x2;

        if(y2>=y1)  y2=y2-y1;
        else        y2=y1-y2;

         // -----------------------------------------------------------------
                    if (graph != null)
                    {
                        Point point1 = new Point((int)x1 - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)y1 -
                                                  GlobalVarLn.axMapScreenGlobal.MapTop);
                        //Point point2 = new Point((int)x2 - GlobalVarLn.axMapScreenGlobal.MapLeft, (int)y2 -
                        //                          GlobalVarLn.axMapScreenGlobal.MapTop);
                        graph.DrawRectangle(penRed2, point1.X, point1.Y, (int)x2,(int)y2);
                    }
        // -----------------------------------------------------------------
*/
        } // Screen
        // *********************************************************************


        // DRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWDRAWD ОТРИСОВКА


// *********************************************************************************
// Убрать все остальные формы, если одна стала активной

// То, что будет активно
// 1-SP
// 2-OB1
// 3-OB2
// 4-LF1
// 5-LF2
// 6-ZO
// 7_TRO
// *********************************************************************************
public static void f_RemoveFrm(int fl)
 {

         // SP *****************************************************************************

         if ((fl != 1) && (GlobalVarLn.fFSP == 1))

         {
             GlobalVarLn.objFormSPG.WindowState=FormWindowState.Minimized;
             GlobalVarLn.fFSP = 0;
         }
         // ***************************************************************************** SP

         // OB1 *****************************************************************************
         // Close OB1

         if ((fl != 2) && (GlobalVarLn.fFOB1 == 1))

         {

             GlobalVarLn.objFormOB1G.WindowState = FormWindowState.Minimized;
             GlobalVarLn.fFOB1 = 0;

         } // OB1
         // ***************************************************************************** OB1

         // OB2 *****************************************************************************
         // Close OB2

         if ((fl != 3) && (GlobalVarLn.fFOB2 == 1))

         {
             GlobalVarLn.objFormOB2G.WindowState = FormWindowState.Minimized;
             GlobalVarLn.fFOB2 = 0;

         } // OB2
         // ***************************************************************************** OB2

         // LF1 *****************************************************************************
         // Close LF1

         if ((fl != 4) && (GlobalVarLn.fFLF1 == 1))

         {
             GlobalVarLn.objFormLFG.WindowState = FormWindowState.Minimized;
             GlobalVarLn.fFLF1 = 0;

         } // LF1
         // ***************************************************************************** LF1

         // LF2 *****************************************************************************
         // Close LF2

         if ((fl != 5) && (GlobalVarLn.fFLF2 == 1))

         {
             GlobalVarLn.objFormLF2G.WindowState = FormWindowState.Minimized;
             GlobalVarLn.fFLF2 = 0;

         } // LF2
         // ***************************************************************************** LF2

         // ZO *****************************************************************************
         // Close ZO

         if ((fl != 6) && (GlobalVarLn.fFZO == 1))

         {
             GlobalVarLn.objFormZOG.WindowState = FormWindowState.Minimized;
             GlobalVarLn.fFZO = 0;

         } // ZO


         // Close TRO
        // 1509&&&
         if ((fl != 7) && (GlobalVarLn.fFTRO == 1))
         {
         }

         if ((fl != 8) && (GlobalVarLn.fFWay_stat == 1))
         {
             GlobalVarLn.objFormWayG.WindowState = FormWindowState.Minimized;
             GlobalVarLn.fFWay_stat = 0;
         }
         if ((fl != 9) && (GlobalVarLn.fFAz1 == 1))
         {
             GlobalVarLn.objFormAz1G.WindowState = FormWindowState.Minimized;
             GlobalVarLn.fFAz1 = 0;
         } 
         if ((fl != 10) && (GlobalVarLn.fFLineSightRange == 1))
         {
             GlobalVarLn.objFormLineSightRangeG.WindowState = FormWindowState.Minimized;
             GlobalVarLn.fFLineSightRange = 0;
         }
         if ((fl != 11) && (GlobalVarLn.fFSuppr == 1))
         {
             GlobalVarLn.objFormSuppressionG.WindowState = FormWindowState.Minimized;
             GlobalVarLn.fFSuppr = 0;

         }
         if ((fl != 12) && (GlobalVarLn.fFSupSpuf == 1))
         {
             GlobalVarLn.objFormSupSpufG.WindowState = FormWindowState.Minimized;
             GlobalVarLn.fFSupSpuf = 0;

         }
         if ((fl != 13) && (GlobalVarLn.fFSpuf == 1))
         {
             GlobalVarLn.objFormSpufG.WindowState = FormWindowState.Minimized;
             GlobalVarLn.fFSpuf = 0;

         }
         if ((fl != 14) && (GlobalVarLn.fFSPFB == 1))
         {
             GlobalVarLn.objFormSPFBG.WindowState = FormWindowState.Minimized;
             GlobalVarLn.fFSPFB = 0;

         }
         if ((fl != 15) && (GlobalVarLn.fFSPFB1 == 1))
         {
             GlobalVarLn.objFormSPFB1G.WindowState = FormWindowState.Minimized;
             GlobalVarLn.fFSPFB1 = 0;
         } 



 }
// *********************************************************************************

public static void f_RemoveFrm1()
{

/*
        // TRO *****************************************************************************
        // Close TRO

                     GlobalVarLn.blTRO_stat = false;
                     GlobalVarLn.flEndTRO_stat = 0;
                     // ....................................................................
                     // SP

                     GlobalVarLn.objFormSPG.tbXRect.Text = "";
                     GlobalVarLn.objFormSPG.tbYRect.Text = "";
                     GlobalVarLn.objFormSPG.tbXRect42.Text = "";
                     GlobalVarLn.objFormSPG.tbYRect42.Text = "";
                     GlobalVarLn.objFormSPG.tbBRad.Text = "";
                     GlobalVarLn.objFormSPG.tbLRad.Text = "";
                     GlobalVarLn.objFormSPG.tbBMin1.Text = "";
                     GlobalVarLn.objFormSPG.tbLMin1.Text = "";
                     GlobalVarLn.objFormSPG.tbBDeg2.Text = "";
                     GlobalVarLn.objFormSPG.tbBMin2.Text = "";
                     GlobalVarLn.objFormSPG.tbBSec.Text = "";
                     GlobalVarLn.objFormSPG.tbLDeg2.Text = "";
                     GlobalVarLn.objFormSPG.tbLMin2.Text = "";
                     GlobalVarLn.objFormSPG.tbLSec.Text = "";
                     GlobalVarLn.objFormSPG.tbOwnHeight.Text = "";
                     GlobalVarLn.objFormSPG.tbNumSP.Text = "";
                     GlobalVarLn.objFormSPG.chbXY.Checked = false;
                     GlobalVarLn.objFormSPG.gbRect.Visible = true;
                     GlobalVarLn.objFormSPG.gbRect.Location = new Point(7, 30);
                     GlobalVarLn.objFormSPG.gbRect42.Visible = false;
                     GlobalVarLn.objFormSPG.gbRad.Visible = false;
                     GlobalVarLn.objFormSPG.gbDegMin.Visible = false;
                     GlobalVarLn.objFormSPG.gbDegMinSec.Visible = false;
                     GlobalVarLn.objFormSPG.cbChooseSC.SelectedIndex = 0;
                     GlobalVarLn.objFormSPG.chbXY.Checked = false;
                     GlobalVarLn.blSP_stat = false;
                     GlobalVarLn.flEndSP_stat = 0;
                     GlobalVarLn.XCenter_SP = 0;
                     GlobalVarLn.YCenter_SP = 0;
                     GlobalVarLn.HCenter_SP = 0;

                     GlobalVarLn.ClearListJS();

                     // ....................................................................
                     // LF1

                     while (GlobalVarLn.objFormLFG.dataGridView1.Rows.Count != 0)
                         GlobalVarLn.objFormLFG.dataGridView1.Rows.Remove(GlobalVarLn.objFormLFG.dataGridView1.Rows[GlobalVarLn.objFormLFG.dataGridView1.Rows.Count - 1]);
                     GlobalVarLn.objFormLFG.dataGridView1.ClearSelection();
                     for (int i = 0; i < GlobalVarLn.sizeDatLF1_stat; i++)
                     {
                         GlobalVarLn.objFormLFG.dataGridView1.Rows.Add("", "", "");
                     }
                     GlobalVarLn.blLF1_stat = false;
                     GlobalVarLn.flEndLF1_stat = 0;
                     GlobalVarLn.iLF1_stat = 0;
                     GlobalVarLn.X_LF1 = 0;
                     GlobalVarLn.Y_LF1 = 0;
                     GlobalVarLn.H_LF1 = 0;
                     GlobalVarLn.list_LF1.Clear();
                     // ....................................................................
                     // LF2

                     while (GlobalVarLn.objFormLF2G.dataGridView1.Rows.Count != 0)
                         GlobalVarLn.objFormLF2G.dataGridView1.Rows.Remove(GlobalVarLn.objFormLF2G.dataGridView1.Rows[GlobalVarLn.objFormLF2G.dataGridView1.Rows.Count - 1]);
                     GlobalVarLn.objFormLF2G.dataGridView1.ClearSelection();
                     for (int i = 0; i < GlobalVarLn.sizeDatLF2_stat; i++)
                     {
                         GlobalVarLn.objFormLF2G.dataGridView1.Rows.Add("", "", "");
                     }
                     GlobalVarLn.blLF2_stat = false;
                     GlobalVarLn.flEndLF2_stat = 0;
                     GlobalVarLn.iLF2_stat = 0;
                     GlobalVarLn.X_LF2 = 0;
                     GlobalVarLn.Y_LF2 = 0;
                     GlobalVarLn.H_LF2 = 0;
                     GlobalVarLn.list_LF2.Clear();
                     // ....................................................................
                     // ZO

                     while (GlobalVarLn.objFormZOG.dataGridView1.Rows.Count != 0)
                         GlobalVarLn.objFormZOG.dataGridView1.Rows.Remove(GlobalVarLn.objFormZOG.dataGridView1.Rows[GlobalVarLn.objFormZOG.dataGridView1.Rows.Count - 1]);
                     GlobalVarLn.objFormZOG.dataGridView1.ClearSelection();
                     for (int i = 0; i < GlobalVarLn.sizeDatZO_stat; i++)
                     {
                         GlobalVarLn.objFormZOG.dataGridView1.Rows.Add("", "", "");
                     }
                     GlobalVarLn.blZO_stat = false;
                     GlobalVarLn.flEndZO_stat = 0;
                     GlobalVarLn.iZO_stat = 0;
                     GlobalVarLn.X_ZO = 0;
                     GlobalVarLn.Y_ZO = 0;
                     GlobalVarLn.H_ZO = 0;
                     GlobalVarLn.list_ZO.Clear();
                     // ....................................................................
                     // OB1

                     while (GlobalVarLn.objFormOB1G.dataGridView1.Rows.Count != 0)
                         GlobalVarLn.objFormOB1G.dataGridView1.Rows.Remove(GlobalVarLn.objFormOB1G.dataGridView1.Rows[GlobalVarLn.objFormOB1G.dataGridView1.Rows.Count - 1]);
                     GlobalVarLn.objFormOB1G.dataGridView1.ClearSelection();
                     for (int i = 0; i < GlobalVarLn.sizeDatOB1_stat; i++)
                     {
                         GlobalVarLn.objFormOB1G.dataGridView1.Rows.Add("", "", "", "");
                     }
                     GlobalVarLn.blOB1_stat = false;
                     GlobalVarLn.flEndOB1_stat = 0;
                     GlobalVarLn.iOB1_stat = 0;
                     GlobalVarLn.X_OB1 = 0;
                     GlobalVarLn.Y_OB1 = 0;
                     GlobalVarLn.H_OB1 = 0;
                     GlobalVarLn.list_OB1.Clear();
                     GlobalVarLn.list1_OB1.Clear();
                     // ....................................................................
                     // OB2

                     while (GlobalVarLn.objFormOB2G.dataGridView1.Rows.Count != 0)
                         GlobalVarLn.objFormOB2G.dataGridView1.Rows.Remove(GlobalVarLn.objFormOB2G.dataGridView1.Rows[GlobalVarLn.objFormOB2G.dataGridView1.Rows.Count - 1]);
                     GlobalVarLn.objFormOB2G.dataGridView1.ClearSelection();
                     for (int i = 0; i < GlobalVarLn.sizeDatOB2_stat; i++)
                     {
                         GlobalVarLn.objFormOB2G.dataGridView1.Rows.Add("", "", "", "");
                     }
                     GlobalVarLn.blOB2_stat = false;
                     GlobalVarLn.flEndOB2_stat = 0;
                     GlobalVarLn.X_OB2 = 0;
                     GlobalVarLn.Y_OB2 = 0;
                     GlobalVarLn.H_OB2 = 0;
                     GlobalVarLn.list_OB2.Clear();
                     GlobalVarLn.list1_OB2.Clear();
                     // ....................................................................
                     // Убрать с карты

                     GlobalVarLn.axMapScreenGlobal.Repaint();
                     // -------------------------------------------------------------------
                     GlobalVarLn.objFormTROG.Close();


        // ***************************************************************************** TRO
*/


}
// *********************************************************************************

// *********************************************************************************
// F_Close_Form
// *********************************************************************************

public static void F_Close_Form(int fl)
{
    // SP
    if ((fl != 1) && (GlobalVarLn.fl_Open_objFormSP == 1))
    {
        GlobalVarLn.objFormSPG.Hide();
        GlobalVarLn.fl_Open_objFormSP = 0;

        // 0510_1
        GlobalVarLn.blSP_stat = false;

    }

    // OB1
    if ((fl != 2) && (GlobalVarLn.fl_Open_objFormOB1 == 1))
    {
        GlobalVarLn.objFormOB1G.Hide();
        GlobalVarLn.fl_Open_objFormOB1 = 0;

        // 0510_1
        GlobalVarLn.blOB1_stat = false;
    }

    // OB2
    if ((fl != 3) && (GlobalVarLn.fl_Open_objFormOB2 == 1))
    {
        GlobalVarLn.objFormOB2G.Hide();
        GlobalVarLn.fl_Open_objFormOB2 = 0;

        // 0510_1
        GlobalVarLn.blOB2_stat = false;
    }

    // LF
    if ((fl != 4) && (GlobalVarLn.fl_Open_objFormLF == 1))
    {
        GlobalVarLn.objFormLFG.Hide();
        GlobalVarLn.fl_Open_objFormLF = 0;

        // 0510_1
        GlobalVarLn.blLF1_stat = false;
    }

    // LF2
    if ((fl != 5) && (GlobalVarLn.fl_Open_objFormLF2 == 1))
    {
        GlobalVarLn.objFormLF2G.Hide();
        GlobalVarLn.fl_Open_objFormLF2 = 0;

        // 0510_1
        GlobalVarLn.blLF2_stat = false;
    }

    // ZO
    if ((fl != 6) && (GlobalVarLn.fl_Open_objFormZO == 1))
    {
        GlobalVarLn.objFormZOG.Hide();
        GlobalVarLn.fl_Open_objFormZO = 0;

        // 0510_1
        GlobalVarLn.blZO_stat = false;
    }

    // TRO
    if ((fl != 7) && (GlobalVarLn.fl_Open_objFormTRO == 1))
    {
        GlobalVarLn.objFormTROG.Hide();
        GlobalVarLn.fl_Open_objFormTRO = 0;

        // 0510_1
        GlobalVarLn.blTRO_stat = false;
        GlobalVarLn.blSP_stat = false;
        GlobalVarLn.blLF1_stat = false;
        GlobalVarLn.blLF2_stat = false;
        GlobalVarLn.blZO_stat = false;
        GlobalVarLn.blOB1_stat = false;
        GlobalVarLn.blOB2_stat = false;

    }

    // Way
    if ((fl != 8) && (GlobalVarLn.fl_Open_objFormWay == 1))
    {
        GlobalVarLn.objFormWayG.Hide();
        GlobalVarLn.fl_Open_objFormWay = 0;
    }

    // Az1
    if ((fl != 9) && (GlobalVarLn.fl_Open_objFormAz1 == 1))
    {
        GlobalVarLn.objFormAz1G.Hide();
        GlobalVarLn.fl_Open_objFormAz1 = 0;

        // 0510_1
        GlobalVarLn.blOB_az1 = false;
    }

    // LineSightRange
    if ((fl != 10) && (GlobalVarLn.fl_Open_objFormLineSightRange == 1))
    {
        GlobalVarLn.objFormLineSightRangeG.Hide();
        GlobalVarLn.fl_Open_objFormLineSightRange = 0;
    }

    // Suppression
    if ((fl != 11) && (GlobalVarLn.fl_Open_objFormSuppression == 1))
    {
        GlobalVarLn.objFormSuppressionG.Hide();
        GlobalVarLn.fl_Open_objFormSuppression = 0;
    }

    // SupSpuf
    if ((fl != 12) && (GlobalVarLn.fl_Open_objFormSupSpuf == 1))
    {
        GlobalVarLn.objFormSupSpufG.Hide();
        GlobalVarLn.fl_Open_objFormSupSpuf = 0;
    }

    // Spuf
    if ((fl != 13) && (GlobalVarLn.fl_Open_objFormSpuf == 1))
    {
        GlobalVarLn.objFormSpufG.Hide();
        GlobalVarLn.fl_Open_objFormSpuf = 0;
    }

    // SPFB
    if ((fl != 14) && (GlobalVarLn.fl_Open_objFormSPFB == 1))
    {
        GlobalVarLn.objFormSPFBG.Hide();
        GlobalVarLn.fl_Open_objFormSPFB = 0;
        // 0510_1
        GlobalVarLn.flF_f1 = 0;
    }

    // SPFB1
    if ((fl != 15) && (GlobalVarLn.fl_Open_objFormSPFB1 == 1))
    {
        GlobalVarLn.objFormSPFB1G.Hide();
        GlobalVarLn.fl_Open_objFormSPFB1 = 0;
        // 0510_1
        GlobalVarLn.flF_f2 = 0;

    }

    // SPFB2
    if ((fl != 16) && (GlobalVarLn.fl_Open_objFormSPFB2 == 1))
    {
        GlobalVarLn.objFormSPFB2G.Hide();
        GlobalVarLn.fl_Open_objFormSPFB2 = 0;

        // 0510_1
        GlobalVarLn.flF_f3 = 0;
        GlobalVarLn.flF1_f3 = 0;
    }

    // Tab
    if ((fl != 17) && (GlobalVarLn.fl_Open_objFormTab == 1))
    {
        GlobalVarLn.objFormTabG.Hide();
        GlobalVarLn.fl_Open_objFormTab = 0;
    }

    // Screen
    if ((fl != 18) && (GlobalVarLn.fl_Open_objFormScreen == 1))
    {
        GlobalVarLn.objFormScreenG.Hide();
        GlobalVarLn.fl_Open_objFormScreen = 0;

        // 0310
        GlobalVarLn.objFormScreenG.pbScreen.Image = null;
        GlobalVarLn.flScrM1 = 0;
        GlobalVarLn.flScrM2 = 0;
        GlobalVarLn.flScrM3 = 0;
        GlobalVarLn.bmpScr = null;

    }

} // F_Close_Form
// *********************************************************************************


// RASTR RASTR RASTR RASTR RASTR RASTR RASTR RASTR RASTR RASTR RASTR RASTR RASTR RASTR 

// ***********************************************************************
// SP по XY m
// ***********************************************************************
public static void f_DrawSPRastr(
                                 double X,
                                 double Y,
                                 String s
                                 )
{

    IMapObject objectGrozaS1;
    MapObjectStyle _placeObjectStyleOwn;
    Mapsui.Geometries.Point pointOwn = new Mapsui.Geometries.Point();
    // --------------------------------------------------------------------------------------
    // SP_main

        try
        {
            var p3 = Mercator.ToLonLat(X,Y);
            pointOwn.X = p3.X;
            pointOwn.Y = p3.Y;

            _placeObjectStyleOwn = MapForm.RasterMapControl.LoadObjectStyle(
                "1.png",
                //(Bitmap)imageList1.Images[station.indzn],
                scale: 2,
                objectOffset: new Offset(0, 0),
                textOffset: new Offset(0, 15)
                );
            objectGrozaS1 = MapForm.RasterMapControl.AddMapObject(_placeObjectStyleOwn, s, pointOwn);

        }
        catch (Exception ex)
        {
            // ignored
        }

} // Функция f_DrawSPXY
        // ***********************************************************************

// ************************************************************************
// функция рисования ЗПВ
// ************************************************************************

public static void DrawLSR_Rastr()
{
    //DrawPolygon_Rastr(GlobalVarLn.listPointDSR, Color.Yellow);
    DrawPolygon_Rastr(GlobalVarLn.listPointDSR);

}

//public static void DrawPolygon_Rastr(IReadOnlyList<Point> points, Color color)
public static void DrawPolygon_Rastr(List<Point> points)

{

    if (points.Count == 0)
    {
        return;
    }

    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    List<Mapsui.Geometries.Point> pointPel = new List<Mapsui.Geometries.Point>();
    double lat = 0;
    double lon = 0;

    for (int i = 0; i < points.Count; i++)
    {
        var p = Mercator.ToLonLat(points[i].X, points[i].Y);
        lat = p.Y;
        lon = p.X;

        pointPel.Add(new Mapsui.Geometries.Point(lon, lat));

    } // FOR


    try
    {
        //Yellow
        MapForm.RasterMapControl.AddPolygon(pointPel, Mapsui.Styles.Color.FromArgb(100, 255, 255,(byte)0.25* 255));
    }
    catch
    {
        //MessageBox.Show(e.Message);
    }

    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>



} // DrawPolygon_Rastr

 // ************************************************************************
public static void DrawPolygon_Rastr1(List<Point> points)
{

    if (points.Count == 0)
    {
        return;
    }

    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    List<Mapsui.Geometries.Point> pointPel = new List<Mapsui.Geometries.Point>();
    double lat = 0;
    double lon = 0;

    for (int i = 0; i < points.Count; i++)
    {
        var p = Mercator.ToLonLat(points[i].X, points[i].Y);
        lat = p.Y;
        lon = p.X;

        pointPel.Add(new Mapsui.Geometries.Point(lon, lat));

    } // FOR


    try
    {
        //Yellow
        MapForm.RasterMapControl.AddPolygon(pointPel, Mapsui.Styles.Color.FromArgb(100, 255, 0, 255));
    }
    catch
    {
        //MessageBox.Show(e.Message);
    }

    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


} //DrawPolygon_Rastr1

// ************************************************************************

// ***********************************************************************
// Самолет по долготе и широте (Geo,grad)
// ***********************************************************************
//0210

public static void f_DrawAirPlane_Tab_Rastr(
                          double Lat_air,
                          double Long_air,
                          double Angle_air,
                          int Num_air
                                 )
{

    IMapObject objectGrozaS1;
    MapObjectStyle _placeObjectStyleOwn;
    Mapsui.Geometries.Point pointOwn = new Mapsui.Geometries.Point();
    // --------------------------------------------------------------------------------------
    // SP_main

    try
    {
        //var p3 = Mercator.ToLonLat(X, Y);
        pointOwn.X = Long_air;
        pointOwn.Y = Lat_air;

        _placeObjectStyleOwn = MapForm.RasterMapControl.LoadObjectStyle(
            "Source.ico",
            //(Bitmap)imageList1.Images[station.indzn],
            scale: 0.6,
            objectOffset: new Offset(0, 0),
            textOffset: new Offset(0, 15)
            );
        objectGrozaS1 = MapForm.RasterMapControl.AddMapObject(_placeObjectStyleOwn, Convert.ToString(Num_air), pointOwn);

    }
    catch (Exception ex)
    {
        // ignored
    }

} // Функция f_DrawAirPlane

        // ***********************************************************************

// ******************************************************************************************
// Перерисовка зоны подавления(м на местности)
// ******************************************************************************************
//0210

public static void f_Map_Redraw_Zon_Suppression_Rastr()
{


    if (GlobalVarLn.flCoordSP_sup == 1)
    {
        f_DrawSPRastr(
                   GlobalVarLn.XCenter_sup,  // m
                   GlobalVarLn.YCenter_sup,
                   ""
                   );
    }

    if (GlobalVarLn.flCoordOP == 1)
    {
        // OP
        ClassMap.f_Ob_Rastr(
                      GlobalVarLn.XPoint1_sup,  // m
                      GlobalVarLn.YPoint1_sup
                     );

    }

    if (GlobalVarLn.fl_Suppression == 1)
    {

        DrawPolygon_Rastr2(GlobalVarLn.listControlJammingZone);



        if (GlobalVarLn.flA_sup == 2)
        {
            f_Map_Zon_Suppression_Rastr(
                                  GlobalVarLn.tpPointCentre1_sup,
                                  2,
                                  (long)GlobalVarLn.RZ1_sup
                                          );

        }

        else if (GlobalVarLn.flA_sup == 3) // SP
        {
            ClassMap.f_Map_Zon_Suppression_Rastr(
                              GlobalVarLn.tpPointCentre1_sup,
                                           2,
                              (long)GlobalVarLn.RZ1_sup
                                          );

        }
    }



} // P/P f_Map_Redraw_Zon_Suppression_Rastr
        // *************************************************************************************



// ******************************************************************************************
// Нарисовать метку по координатам (треугольник)
//
// ******************************************************************************************
public static void f_Ob_Rastr(
                                 double X,
                                 double Y
                                )
{
    IMapObject objectGrozaS1;
    MapObjectStyle _placeObjectStyleOwn;
    Mapsui.Geometries.Point pointOwn = new Mapsui.Geometries.Point();

    try
    {

        var p3 = Mercator.ToLonLat(X, Y);
        pointOwn.X = p3.X;
        pointOwn.Y = p3.Y;

        _placeObjectStyleOwn = MapForm.RasterMapControl.LoadObjectStyle(
            "TriangleRed.png",
            scale: 0.6,
            objectOffset: new Offset(0, 0),
            textOffset: new Offset(0, 15)
            );
        objectGrozaS1 = MapForm.RasterMapControl.AddMapObject(_placeObjectStyleOwn, "", pointOwn);

    }
    catch (Exception ex)
    {
        // ignored
    }

} // P/P f_Map_Pol_XY_stat
// *************************************************************************************
public static void DrawPolygon_Rastr2(List<Point> points)
{

    if (points.Count == 0)
    {
        return;
    }

    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    List<Mapsui.Geometries.Point> pointPel = new List<Mapsui.Geometries.Point>();
    double lat = 0;
    double lon = 0;

    for (int i = 0; i < points.Count; i++)
    {
        var p = Mercator.ToLonLat(points[i].X, points[i].Y);
        lat = p.Y;
        lon = p.X;

        pointPel.Add(new Mapsui.Geometries.Point(lon, lat));

    } // FOR


    try
    {
        //Yellow
        MapForm.RasterMapControl.AddPolygon(pointPel, Mapsui.Styles.Color.FromArgb(100, 255, 0, 0));
    }
    catch
    {
        //MessageBox.Show(e.Message);
    }

    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


} //DrawPolygon_Rastr1

// ************************************************************************

// ******************************************************************************************
// Зона подавления(м на местности)
// ******************************************************************************************
public static void f_Map_Zon_Suppression_Rastr(
                                 Point tpCenterPoint,
                                 int clr,
                                 long iRadiusZone
                                )
{
/*
    double dLeftX = 0;
    double dLeftY = 0;
    double dRightX = 0;
    double dRightY = 0;

    double XC = 0;
    double YC = 0;
    XC = tpCenterPoint.X;
    YC = tpCenterPoint.Y;

    double x3 = 0;
    double y3 = 0;
    double x4 = 0;
    double y4 = 0;

    // -------------------------------------------------------------------------------------
    Graphics graph = GlobalVarLn.axMapScreenGlobal.CreateGraphics();

    Pen penRed1 = new Pen(Color.Red, 2);
    Pen penRed2 = new Pen(Color.White, 2);

    Brush brushRed1 = new SolidBrush(Color.Red);
    Brush brushRed2 = new SolidBrush(Color.Blue);

    Pen pen1 = new Pen(Color.HotPink, 5.0f);
    HatchBrush brush1 = new HatchBrush(HatchStyle.Cross, Color.Linen, Color.Transparent);

    HatchBrush brush2 = new HatchBrush(HatchStyle.DiagonalCross, Color.Linen, Color.Transparent);


    Brush brushi = new SolidBrush(Color.White);

    // -------------------------------------------------------------------------------------

    if (graph != null)
    {

        dLeftX = XC - iRadiusZone;
        dLeftY = YC - iRadiusZone;
        dRightX = XC + iRadiusZone;
        dRightY = YC + iRadiusZone;
        mapPlaneToPicture(GlobalVarLn.hmapl, ref XC, ref YC);
        mapPlaneToPicture(GlobalVarLn.hmapl, ref dLeftX, ref dLeftY);
        mapPlaneToPicture(GlobalVarLn.hmapl, ref dRightX, ref dRightY);

        if (clr == 1)
        {

            graph.DrawEllipse(pen1, (int)dLeftX - GlobalVarLn.axMapScreenGlobal.MapLeft,
                (int)dLeftY - GlobalVarLn.axMapScreenGlobal.MapTop,
                (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));
            graph.FillEllipse(brush1, (int)dLeftX - GlobalVarLn.axMapScreenGlobal.MapLeft,
                (int)dLeftY - GlobalVarLn.axMapScreenGlobal.MapTop,
                (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));

        }

        else
        {
            graph.DrawEllipse(penRed2, (int)dLeftX - GlobalVarLn.axMapScreenGlobal.MapLeft,
                (int)dLeftY - GlobalVarLn.axMapScreenGlobal.MapTop,
                (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));

            if (GlobalVarLn.flA_sup != 3)
            {
                graph.FillEllipse(brush2, (int)dLeftX - GlobalVarLn.axMapScreenGlobal.MapLeft,
                    (int)dLeftY - GlobalVarLn.axMapScreenGlobal.MapTop,
                    (int)(dRightX - dLeftX), (int)(dRightY - dLeftY));
            }
        }


    }
    // -------------------------------------------------------------------------------------
*/

    try
    {
        double xx=0;
        double yy=0;
        double lt = 0;
        double lng = 0;
        xx=tpCenterPoint.X;
        yy=tpCenterPoint.Y;

        var p = Mercator.ToLonLat(xx, yy);
        lt = p.Y;
        lng = p.X;

        Mapsui.Geometries.Point point = new Mapsui.Geometries.Point();
        point.Y = lt;
        point.X = lng;


        float fSectorLeft = 0;
        //float fSectorRight = 359;
        float fSectorRight = 360;


        var points = MapForm.RasterMapControl.CreateSectorPoints(
            point,
            fSectorLeft,
            fSectorRight,
            (float)iRadiusZone);

        MapForm.RasterMapControl.AddPolygon(points, Mapsui.Styles.Color.FromArgb(100, 255, 255, 255));
    }
    catch (Exception e)
    {
        //MessageBox.Show(e.Message);
    }



} // P/P f_Zon_Suppression
        // *************************************************************************************

public static void DrawPolygon_Rastr5(List<Point> points)
{

    if (points.Count == 0)
    {
        return;
    }

    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    List<Mapsui.Geometries.Point> pointPel = new List<Mapsui.Geometries.Point>();
    double lat = 0;
    double lon = 0;

    for (int i = 0; i < points.Count; i++)
    {
        var p = Mercator.ToLonLat(points[i].X, points[i].Y);
        lat = p.Y;
        lon = p.X;

        pointPel.Add(new Mapsui.Geometries.Point(lon, lat));

    } // FOR


    try
    {
        //Yellow
        MapForm.RasterMapControl.AddPolygon(pointPel, Mapsui.Styles.Color.FromArgb(100, 0, 0, 255));
    }
    catch
    {
        //MessageBox.Show(e.Message);
    }

    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>



} // DrawPolygon_Rastr



public static void DrawPolygon_Rastr6(List<Point> points)
{

    if (points.Count == 0)
    {
        return;
    }

    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    List<Mapsui.Geometries.Point> pointPel = new List<Mapsui.Geometries.Point>();
    double lat = 0;
    double lon = 0;

    for (int i = 0; i < points.Count; i++)
    {
        var p = Mercator.ToLonLat(points[i].X, points[i].Y);
        lat = p.Y;
        lon = p.X;

        pointPel.Add(new Mapsui.Geometries.Point(lon, lat));

    } // FOR


    try
    {
        //Yellow
        MapForm.RasterMapControl.AddPolygon(pointPel, Mapsui.Styles.Color.FromArgb(100, 0, 255, 0));
    }
    catch
    {
        //MessageBox.Show(e.Message);
    }

    // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>



} // DrawPolygon_Rastr

        // ************************************************************************


// RASTR RASTR RASTR RASTR RASTR RASTR RASTR RASTR RASTR RASTR RASTR RASTR RASTR RASTR 


    } // class ClassMap

} // namespace MapApplication
