﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.IO;
using PC_DLL;

namespace GrozaMap
{
    public partial class FormSPFB2 : Form
    {

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        private double dchislo;
        private long ichislo;
        //private double LAMBDA;
        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR  Переменные

        // Конструктор *********************************************************** 

        private MapForm mapForm;

        public FormSPFB2(MapForm mapF)
        {
            InitializeComponent();

            mapForm = mapF;

            dchislo = 0;
            ichislo = 0;
            //LAMBDA = 300000;

        } // Конструктор
        // ***********************************************************  Конструктор

        // Загрузка формы *********************************************************

        private void FormSPFB2_Load(object sender, EventArgs e)
        {
            ClassMapRastrLoadForm.f_Load_FormSPFB2();

            GlobalVarLn.ListJSChangedEvent += OnListJSChanged;

            MapForm.UpdateDropDownList1(cbChooseSC);

        }
        // ********************************************************* Загрузка формы

        // ChangeSP *****************************************************************
        // Обработчик события при изменении списка СП

        private void OnListJSChanged(object sender, EventArgs e)
        {
            // Обновление списка СП (без X,Y в списке)
            MapForm.UpdateDropDownList1(cbChooseSC);
        }
        // ***************************************************************** ChangeSP

        // Активизировать форму *****************************************************

        private void FormSPFB2_Activated(object sender, EventArgs e)
        {
            GlobalVarLn.flF_f3 = 1;
            GlobalVarLn.flF1_f3 = 1;

            GlobalVarLn.fl_Open_objFormSPFB2 = 1;

        }
        // ***************************************************** Активизировать форму

        // Закрыть форму ************************************************************

        private void FormSPFB2_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

            // приостановить обработку, если идет
            GlobalVarLn.flF_f3 = 0;
            GlobalVarLn.flF1_f3 = 0;

            GlobalVarLn.fl_Open_objFormSPFB2 = 0;

        }
        // ************************************************************ Закрыть форму

        // Очистка1 *****************************************************************

        private void bClear_Click(object sender, EventArgs e)
        {
            // --------------------------------------------------------------------
            ClassMapRastrClear.f_Clear_FormSPFB2_1();
            // --------------------------------------------------------------------

        }
        // ***************************************************************** Очистка1

        // Очистка2 *****************************************************************

        private void bClear1_Click(object sender, EventArgs e)
        {
            // --------------------------------------------------------------------
            ClassMapRastrClear.f_Clear_FormSPFB2_2();
            // --------------------------------------------------------------------

        }
        // ***************************************************************** Очистка2

        // ************************************************************************
        // Обработчик ComboBox "cbChooseSC": Выбор SP
        // ************************************************************************
        private void cbChooseSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            GlobalVarLn.NumbSP_f3 = Convert.ToString(cbChooseSC.Items[cbChooseSC.SelectedIndex]);

        } //Select_SP
        // ************************************************************************

        // ************************************************************************
        // Save in file
        // ************************************************************************
        private void bAccept_Click(object sender, EventArgs e)
        {
            int i_tmp = 0;
            int i_tmp1 = 0;
            String s1 = "";
            // -----------------------------------------------------------------------------------------
            String strFileName;

            //strFileName = "FSP.txt";
            //0219
            strFileName = Application.StartupPath + "\\SaveInFiles\\FreqProhibited.txt";

            StreamReader srFile1;
            GlobalVarLn.NumbSP_f3 = cbChooseSC.Text;

            FBSP objFBSP = new FBSP();
            objFBSP.mfb = new FB[GlobalVarLn.sizeDiap_f3+10];

            GlobalVarLn.list1_f3.Clear();
            GlobalVarLn.iSP_f3 = 0;
            // ***********************************************************************************************
            // файл был

            try
            {
                // ----------------------------------------------------------------------------------------
                srFile1 = new StreamReader(strFileName);
                srFile1.Close();
                ReadFileFBSP_f3(0); // Читаем старый файл
                GlobalVarLn.iSP_f3 = (uint)GlobalVarLn.list1_f3.Count;

                for (i_tmp = 0; i_tmp < GlobalVarLn.list1_f3.Count; i_tmp++)
                {
                    // Убираем СП с таким же номером в старом файле
                    if (String.Compare(GlobalVarLn.list1_f3[i_tmp].NumbSP, GlobalVarLn.NumbSP_f3) == 0)
                    {
                        GlobalVarLn.list1_f3.RemoveAt(i_tmp);
                        GlobalVarLn.iSP_f3 -= 1;
                    }
                }
                // ----------------------------------------------------------------------------------------
                objFBSP.NumbSP = GlobalVarLn.NumbSP_f3;
                i_tmp = 0;

                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILE
                while (
                        (dgvFreqForbid.Rows[(int)(i_tmp)].Cells[0].Value != "") &&
                        (dgvFreqForbid.Rows[(int)(i_tmp)].Cells[0].Value != null)
                      )
                {
                    // ...........................................................................................
                    // Fmin==F

                    s1 = Convert.ToString(dgvFreqForbid.Rows[(int)(i_tmp)].Cells[0].Value);

                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        objFBSP.mfb[i_tmp].Fmin = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            objFBSP.mfb[i_tmp].Fmin = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            MessageBox.Show("Incorrect data");
                            return;
                        }

                    }

                    // seg
                    ichislo = (long)(objFBSP.mfb[i_tmp].Fmin * 10);
                    dchislo = ((double)ichislo) / 10;
                    if (dchislo < 100) dchislo = 100;
                    objFBSP.mfb[i_tmp].Fmin = dchislo;

                    // ...........................................................................................
                    // Fmax

                    s1 = Convert.ToString(dgvFreqForbid.Rows[(int)(i_tmp)].Cells[1].Value);

                    if ((s1 == "") || (s1 == null))
                    {
                        MessageBox.Show("Incorrect data");
                        srFile1.Close();
                        return;
                    }

                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        objFBSP.mfb[i_tmp].Fmax = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            objFBSP.mfb[i_tmp].Fmax = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            MessageBox.Show("Incorrect data");
                            return;
                        }

                    }

                    // seg
                    ichislo = (long)(objFBSP.mfb[i_tmp].Fmax * 10);
                    dchislo = ((double)ichislo) / 10;
                    if (dchislo > 6000) dchislo = 6000;
                    objFBSP.mfb[i_tmp].Fmax = dchislo;

                    // ...........................................................................................
                    // seg

                    if (
                       (objFBSP.mfb[i_tmp].Fmin > objFBSP.mfb[i_tmp].Fmax) 
                       )
                    {
                        MessageBox.Show("Incorrect data");
                        srFile1.Close();
                        return;
                    }

                    if (
                       (objFBSP.mfb[i_tmp].Fmin < 100) ||
                        (objFBSP.mfb[i_tmp].Fmin > 6000) ||
                        (objFBSP.mfb[i_tmp].Fmax < 100) ||
                        (objFBSP.mfb[i_tmp].Fmax > 6000)
                       )
                    {
                        MessageBox.Show("Parameter 'frequency' out of range 100-6000 MHz");
                        srFile1.Close();
                        return;
                    }

                    // ...........................................................................................

                    i_tmp += 1;
                    if (i_tmp > GlobalVarLn.sizeDiap_f3)
                    {
                        MessageBox.Show("Invalid number of frequency bands");
                        break;
                    }
                } // WHILE
                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILE

                objFBSP.NumbDiap = i_tmp;
                // ----------------------------------------------------------------------------------------
                GlobalVarLn.list1_f3.Add(objFBSP);
                GlobalVarLn.iSP_f3 += 1;
                // ----------------------------------------------------------------------------------------
                StreamWriter srFile = new StreamWriter(strFileName);
                // ----------------------------------------------------------------------------------------
                // Запись в файл

                for (i_tmp = 0; i_tmp < GlobalVarLn.list1_f3.Count; i_tmp++)
                {
                    // SPi
                    srFile.WriteLine("Numb =" + Convert.ToString(GlobalVarLn.list1_f3[i_tmp].NumbSP));

                    for (i_tmp1 = 0; i_tmp1 < GlobalVarLn.list1_f3[i_tmp].NumbDiap; i_tmp1++)
                    {
                        srFile.WriteLine("Fmin =" + Convert.ToString(GlobalVarLn.list1_f3[i_tmp].mfb[i_tmp1].Fmin));
                        srFile.WriteLine("Fmax =" + Convert.ToString(GlobalVarLn.list1_f3[i_tmp].mfb[i_tmp1].Fmax));
                    }

                }

                // ----------------------------------------------------------------------------------------
                srFile.Close();
            }
            // ***********************************************************************************************
            // файла не было

            catch
            {
                // ...........................................................................................
                //StreamWriter srFile = new StreamWriter(strFileName);
                StreamWriter srFile;
                try
                {
                    srFile = new StreamWriter(strFileName);
                }
                catch
                {
                    MessageBox.Show("Can’t save file");
                    return;
                }


                objFBSP.NumbSP = GlobalVarLn.NumbSP_f3;
                i_tmp = 0;
                // ...........................................................................................

                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILE
                while (
                       (dgvFreqForbid.Rows[(int)(i_tmp)].Cells[0].Value != "") &&
                       (dgvFreqForbid.Rows[(int)(i_tmp)].Cells[0].Value != null)
                      )
                {
                    // ...........................................................................................
                    // Fmin==F

                    s1 = Convert.ToString(dgvFreqForbid.Rows[(int)(i_tmp)].Cells[0].Value);

                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        objFBSP.mfb[i_tmp].Fmin = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            objFBSP.mfb[i_tmp].Fmin = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            MessageBox.Show("Incorrect data");
                            return;
                        }

                    }

                    // seg
                    ichislo = (long)(objFBSP.mfb[i_tmp].Fmin * 10);
                    dchislo = ((double)ichislo) / 10;
                    if (dchislo < 100) dchislo = 100;
                    objFBSP.mfb[i_tmp].Fmin = dchislo;

                    // ...........................................................................................
                    // Fmax

                    s1 = Convert.ToString(dgvFreqForbid.Rows[(int)(i_tmp)].Cells[1].Value);

                    if ((s1 == "") || (s1 == null))
                    {
                        MessageBox.Show("Incorrect data");
                        srFile.Close();
                        return;
                    }

                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        objFBSP.mfb[i_tmp].Fmax = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            objFBSP.mfb[i_tmp].Fmax = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            MessageBox.Show("Incorrect data");
                            return;
                        }

                    }

                    // seg
                    ichislo = (long)(objFBSP.mfb[i_tmp].Fmax * 10);
                    dchislo = ((double)ichislo) / 10;
                    if (dchislo > 6000) dchislo = 6000;
                    objFBSP.mfb[i_tmp].Fmax = dchislo;

                    // ...........................................................................................
                    // seg

                    if (
                       (objFBSP.mfb[i_tmp].Fmin > objFBSP.mfb[i_tmp].Fmax) 
                       )
                    {
                        MessageBox.Show("Incorrect data");
                        srFile.Close();
                        return;
                    }

                    if (
                       (objFBSP.mfb[i_tmp].Fmin < 100) ||
                        (objFBSP.mfb[i_tmp].Fmin > 6000) ||
                        (objFBSP.mfb[i_tmp].Fmax < 100) ||
                        (objFBSP.mfb[i_tmp].Fmax > 6000)
                       )
                    {
                        MessageBox.Show("Parameter 'frequency' out of range 100-6000 MHz");
                        srFile.Close();
                        return;
                    }

                    // ...........................................................................................

                    i_tmp += 1;
                    if (i_tmp > GlobalVarLn.sizeDiap_f3)
                    {
                        MessageBox.Show("Invalid number of frequency bands");
                        break;
                    }
                } // WHILE
                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILE

                objFBSP.NumbDiap = i_tmp;

                // SPi
                srFile.WriteLine("Numb =" + Convert.ToString(objFBSP.NumbSP));

                for (i_tmp = 0; i_tmp < objFBSP.NumbDiap; i_tmp++)
                {

                    srFile.WriteLine("Fmin =" + Convert.ToString(objFBSP.mfb[i_tmp].Fmin));
                    srFile.WriteLine("Fmax =" + Convert.ToString(objFBSP.mfb[i_tmp].Fmax));

                }

                srFile.Close();
            } // catch
            // ***********************************************************************************************

        } // Save in file
        // ************************************************************************

        // ************************************************************************
        // Save in file1
        // ************************************************************************
        private void bAccept1_Click(object sender, EventArgs e)
        {
            int i_tmp = 0;
            int i_tmp1 = 0;
            String s1 = "";
            // -----------------------------------------------------------------------------------------
            String strFileName;

            //strFileName = "FSP_Special.txt";
            //0219
            strFileName = Application.StartupPath + "\\SaveInFiles\\FreqSpecial.txt";

            StreamReader srFile1;
            GlobalVarLn.NumbSP_f3 = cbChooseSC.Text;

            FBSP objFBSP = new FBSP();
            objFBSP.mfb = new FB[GlobalVarLn.sizeDiap_f3 + 10];

            GlobalVarLn.list2_f3.Clear();
            GlobalVarLn.iSP_f3 = 0;
            // ***********************************************************************************************
            // файл был

            try
            {
                // ----------------------------------------------------------------------------------------
                srFile1 = new StreamReader(strFileName);
                srFile1.Close();
                ReadFileFBSP1_f3(0); // Читаем старый файл
                GlobalVarLn.iSP_f3 = (uint)GlobalVarLn.list2_f3.Count;

                for (i_tmp = 0; i_tmp < GlobalVarLn.list2_f3.Count; i_tmp++)
                {
                    // Убираем СП с таким же номером в старом файле
                    if (String.Compare(GlobalVarLn.list2_f3[i_tmp].NumbSP, GlobalVarLn.NumbSP_f3) == 0)
                    {
                        GlobalVarLn.list2_f3.RemoveAt(i_tmp);
                        GlobalVarLn.iSP_f3 -= 1;
                    }
                }
                // ----------------------------------------------------------------------------------------
                objFBSP.NumbSP = GlobalVarLn.NumbSP_f3;
                i_tmp = 0;

                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILE
                while (
                        (dgvFreqSpec.Rows[(int)(i_tmp)].Cells[0].Value != "") &&
                        (dgvFreqSpec.Rows[(int)(i_tmp)].Cells[0].Value != null)
                      )
                {
                    // ...........................................................................................
                    // Fmin==F

                    s1 = Convert.ToString(dgvFreqSpec.Rows[(int)(i_tmp)].Cells[0].Value);

                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        objFBSP.mfb[i_tmp].Fmin = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            objFBSP.mfb[i_tmp].Fmin = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            MessageBox.Show("Incorrect data");
                            return;
                        }

                    }

                    // seg
                    ichislo = (long)(objFBSP.mfb[i_tmp].Fmin * 10);
                    dchislo = ((double)ichislo) / 10;
                    if (dchislo < 100) dchislo = 100;
                    objFBSP.mfb[i_tmp].Fmin = dchislo;

                    // ...........................................................................................
                    // Fmax

                    s1 = Convert.ToString(dgvFreqSpec.Rows[(int)(i_tmp)].Cells[1].Value);

                    if ((s1 == "") || (s1 == null))
                    {
                        MessageBox.Show("Incorrect data");
                        srFile1.Close();
                        return;
                    }

                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        objFBSP.mfb[i_tmp].Fmax = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            objFBSP.mfb[i_tmp].Fmax = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            MessageBox.Show("Incorrect data");
                            return;
                        }

                    }

                    // seg
                    ichislo = (long)(objFBSP.mfb[i_tmp].Fmax * 10);
                    dchislo = ((double)ichislo) / 10;
                    if (dchislo > 6000) dchislo = 6000;
                    objFBSP.mfb[i_tmp].Fmax = dchislo;

                    // ...........................................................................................
                    // seg

                    if (
                       (objFBSP.mfb[i_tmp].Fmin > objFBSP.mfb[i_tmp].Fmax)
                       )
                    {
                        MessageBox.Show("Incorrect data");
                        srFile1.Close();
                        return;
                    }

                    if (
                       (objFBSP.mfb[i_tmp].Fmin < 100) ||
                        (objFBSP.mfb[i_tmp].Fmin > 6000) ||
                        (objFBSP.mfb[i_tmp].Fmax < 100) ||
                        (objFBSP.mfb[i_tmp].Fmax > 6000)
                       )
                    {
                        MessageBox.Show("Parameter 'frequency' out of range 100-6000 MHz");
                        srFile1.Close();
                        return;
                    }

                    // ...........................................................................................

                    i_tmp += 1;
                    if (i_tmp > GlobalVarLn.sizeDiap_f3)
                    {
                        MessageBox.Show("Invalid number of frequency bands");
                        break;
                    }
                } // WHILE
                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILE

                objFBSP.NumbDiap = i_tmp;
                // ----------------------------------------------------------------------------------------
                GlobalVarLn.list2_f3.Add(objFBSP);
                GlobalVarLn.iSP_f3 += 1;
                // ----------------------------------------------------------------------------------------
                StreamWriter srFile = new StreamWriter(strFileName);
                // ----------------------------------------------------------------------------------------
                // Запись в файл

                for (i_tmp = 0; i_tmp < GlobalVarLn.list2_f3.Count; i_tmp++)
                {
                    // SPi
                    srFile.WriteLine("Numb =" + Convert.ToString(GlobalVarLn.list2_f3[i_tmp].NumbSP));

                    for (i_tmp1 = 0; i_tmp1 < GlobalVarLn.list2_f3[i_tmp].NumbDiap; i_tmp1++)
                    {
                        srFile.WriteLine("Fmin =" + Convert.ToString(GlobalVarLn.list2_f3[i_tmp].mfb[i_tmp1].Fmin));
                        srFile.WriteLine("Fmax =" + Convert.ToString(GlobalVarLn.list2_f3[i_tmp].mfb[i_tmp1].Fmax));
                    }

                }

                // ----------------------------------------------------------------------------------------
                srFile.Close();
            }
            // ***********************************************************************************************
            // файла не было

            catch
            {
                // ...........................................................................................
                //StreamWriter srFile = new StreamWriter(strFileName);
                StreamWriter srFile;
                try
                {
                    srFile = new StreamWriter(strFileName);
                }
                catch
                {
                    MessageBox.Show("Can’t save file");
                    return;
                }

                objFBSP.NumbSP = GlobalVarLn.NumbSP_f3;
                i_tmp = 0;
                // ...........................................................................................

                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILE
                while (
                       (dgvFreqSpec.Rows[(int)(i_tmp)].Cells[0].Value != "") &&
                       (dgvFreqSpec.Rows[(int)(i_tmp)].Cells[0].Value != null)
                      )
                {
                    // ...........................................................................................
                    // Fmin==F

                    s1 = Convert.ToString(dgvFreqSpec.Rows[(int)(i_tmp)].Cells[0].Value);

                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        objFBSP.mfb[i_tmp].Fmin = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            objFBSP.mfb[i_tmp].Fmin = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            MessageBox.Show("Incorrect data");
                            return;
                        }

                    }

                    // seg
                    ichislo = (long)(objFBSP.mfb[i_tmp].Fmin * 10);
                    dchislo = ((double)ichislo) / 10;
                    if (dchislo < 100) dchislo = 100;
                    objFBSP.mfb[i_tmp].Fmin = dchislo;

                    // ...........................................................................................
                    // Fmax

                    s1 = Convert.ToString(dgvFreqSpec.Rows[(int)(i_tmp)].Cells[1].Value);

                    if ((s1 == "") || (s1 == null))
                    {
                        MessageBox.Show("Incorrect data");
                        srFile.Close();
                        return;
                    }

                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        objFBSP.mfb[i_tmp].Fmax = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            objFBSP.mfb[i_tmp].Fmax = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            MessageBox.Show("Incorrect data");
                            return;
                        }

                    }

                    // seg
                    ichislo = (long)(objFBSP.mfb[i_tmp].Fmax * 10);
                    dchislo = ((double)ichislo) / 10;
                    if (dchislo > 6000) dchislo = 6000;
                    objFBSP.mfb[i_tmp].Fmax = dchislo;
                    // ...........................................................................................
                    // seg

                    if (
                       (objFBSP.mfb[i_tmp].Fmin > objFBSP.mfb[i_tmp].Fmax)
                       )
                    {
                        MessageBox.Show("Incorrect data");
                        srFile.Close();
                        return;
                    }

                    if (
                       (objFBSP.mfb[i_tmp].Fmin < 100) ||
                        (objFBSP.mfb[i_tmp].Fmin > 6000) ||
                        (objFBSP.mfb[i_tmp].Fmax < 100) ||
                        (objFBSP.mfb[i_tmp].Fmax > 6000)
                       )
                    {
                        MessageBox.Show("Parameter 'frequency' out of range 100-6000 MHz");
                        srFile.Close();
                        return;
                    }

                    // ...........................................................................................

                    i_tmp += 1;
                    if (i_tmp > GlobalVarLn.sizeDiap_f3)
                    {
                        MessageBox.Show("Invalid number of frequency bands");
                        break;
                    }
                } // WHILE
                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILE

                objFBSP.NumbDiap = i_tmp;

                // SPi
                srFile.WriteLine("Numb =" + Convert.ToString(objFBSP.NumbSP));

                for (i_tmp = 0; i_tmp < objFBSP.NumbDiap; i_tmp++)
                {

                    srFile.WriteLine("Fmin =" + Convert.ToString(objFBSP.mfb[i_tmp].Fmin));
                    srFile.WriteLine("Fmax =" + Convert.ToString(objFBSP.mfb[i_tmp].Fmax));

                }

                srFile.Close();
            } // catch
        // ***********************************************************************************************

        } // Save in file1
        // ************************************************************************

        // ************************************************************************
        // Read Tek
        // ************************************************************************
        private void button2_Click(object sender, EventArgs e)
        {
            // ----------------------------------------------------------------------
            // Очистка dataGridView

            while (dgvFreqForbid.Rows.Count != 0)
                dgvFreqForbid.Rows.Remove(dgvFreqForbid.Rows[dgvFreqForbid.Rows.Count - 1]);

            dgvFreqForbid.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDat_f3; i++)
            {
                dgvFreqForbid.Rows.Add("","");
            }
            // .......................................................................

            GlobalVarLn.flrd_f3 = 0;
            ReadFileFBSP_f3(1); // Читаем старый файл
            GlobalVarLn.list1_f3.Clear();

            if (GlobalVarLn.flrd_f3 == 0)
            {
                MessageBox.Show("No information");
                return;
            }

        } // Read_tek
        // ************************************************************************

        // ************************************************************************
        // Read Tek1
        // ************************************************************************
        private void bLoad1_Click(object sender, EventArgs e)
        {
            // ----------------------------------------------------------------------
            // Очистка dataGridView

            while (dgvFreqSpec.Rows.Count != 0)
                dgvFreqSpec.Rows.Remove(dgvFreqSpec.Rows[dgvFreqSpec.Rows.Count - 1]);

            dgvFreqSpec.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDat_f3; i++)
            {
                dgvFreqSpec.Rows.Add("", "");
            }
            // .......................................................................

            GlobalVarLn.flrd1_f3 = 0;
            ReadFileFBSP1_f3(1); // Читаем старый файл
            GlobalVarLn.list2_f3.Clear();

            if (GlobalVarLn.flrd1_f3 == 0)
            {
                MessageBox.Show("No information");
                return;
            }

        } // Read1
        // ************************************************************************

        // ************************************************************************
        // функция чтения файла по FСП 
        // ************************************************************************
        public void ReadFileFBSP_f3(int fl)
        {
            // ------------------------------------------------------------------------------------
            String strLine = "";
            String strLine1 = "";
            double number1 = 0;
            int number2 = 0;
            char symb1 = 'N';
            char symb2 = 'X';
            char symb3 = 'Y';
            char symb4 = '=';
            char symb5 = 'H';
            char symb6 = 'R';
            char symb7 = 'A';
            char symb8 = 'T';
            char symb9 = '{';
            char symb10 = '}';
            char symb11 = 'F';
            char symb12 = 'B';

            int indStart = 0;
            int indStop = 0;
            int iLength = 0;
            int IndZap = 0;
            int TekPoz = 0;
            int fi = 0;
            int if1 = 0;
            int indmas = 0;

            // Чтение файла ---------------------------------------------------------
            String strFileName;

            //strFileName = "FSP.txt";
            //0219
            strFileName = Application.StartupPath + "\\SaveInFiles\\FreqProhibited.txt";

            StreamReader srFile;
            try
            {
                srFile = new StreamReader(strFileName);
            }
            catch
            {
                MessageBox.Show("Can't open file");
                return;

            }
            // -------------------------------------------------------------------------------------
            // Чтение
            // Numb =...

            // Fmin =...
            // Fmax =...
            // Bmin =...
            // Bmax =...
            // ...

            indmas = 0;

            try
            {
                FBSP objSP = new FBSP();
                objSP.mfb = new FB[GlobalVarLn.sizeDiap_f3+10];

                TekPoz = 0;
                IndZap = 0;
                // .......................................................
                // Numb =...
                // 1-я строка

                strLine = srFile.ReadLine();

                if ((strLine == null) && (fl == 0))
                {
                    srFile.Close();
                    return;
                }

                if (strLine == null)
                {
                    MessageBox.Show("No information");
                    srFile.Close();
                    return;
                }

                indStart = strLine.IndexOf(symb1, TekPoz); // N

                if (indStart == -1)
                {
                    MessageBox.Show("No information");
                    srFile.Close();
                    return;
                }

                indStop = strLine.IndexOf(symb4, TekPoz);  //=

                if ((indStop == -1) || (indStop < indStart))
                {
                    MessageBox.Show("No information");
                    srFile.Close();
                    return;
                }

                iLength = indStop - indStart + 1;
                // Убираем 'Numb ='
                strLine1 = strLine.Remove(indStart, iLength);

                if (strLine1 == "")
                {
                    MessageBox.Show("No information");
                    srFile.Close();
                    return;
                }

                objSP.NumbSP = strLine1;
                // .......................................................

                fi = 0;
                strLine = srFile.ReadLine(); // читаем далее (Fmin1=)
                if ((strLine == "") || (strLine == null))
                {
                    fi = 1;
                }
                // .......................................................

                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH
                while ((strLine != "") && (strLine != null))
                {

                    // .......................................................
                    // Fmin =...

                    indStart = strLine.IndexOf(symb11, TekPoz); // F
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    number1 = Convert.ToDouble(strLine1);

                    objSP.mfb[indmas].Fmin = number1;
                    // .......................................................
                    // Fmax =...

                    strLine = srFile.ReadLine();

                    indStart = strLine.IndexOf(symb11, TekPoz); // F
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    number1 = Convert.ToDouble(strLine1);

                    objSP.mfb[indmas].Fmax = number1;

                    // .......................................................
                    // Bmin =...
/*
                    strLine = srFile.ReadLine();

                    indStart = strLine.IndexOf(symb12, TekPoz); // B
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    number1 = Convert.ToDouble(strLine1);

                    objSP.mfb[indmas].Bmin = number1;
 */
                    // .......................................................
                    // Bmax =...
/*
                    strLine = srFile.ReadLine();

                    indStart = strLine.IndexOf(symb12, TekPoz); // B
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    number1 = Convert.ToDouble(strLine1);

                    objSP.mfb[indmas].Bmax = number1;
*/
                    // .......................................................
                    if ((fl == 1) &&
                        (String.Compare(objSP.NumbSP, GlobalVarLn.NumbSP_f3) == 0)
                       )
                    {
                        GlobalVarLn.flrd_f3 = 1;

                        dgvFreqForbid.Rows[(int)(IndZap)].Cells[0].Value = objSP.mfb[indmas].Fmin;
                        dgvFreqForbid.Rows[(int)(IndZap)].Cells[1].Value = objSP.mfb[indmas].Fmax;
                        //dataGridView1.Rows[(int)(IndZap)].Cells[2].Value = objSP.mfb[indmas].Bmin;
                        //dataGridView1.Rows[(int)(IndZap)].Cells[3].Value = objSP.mfb[indmas].Bmax;
                        IndZap += 1;

                    }
                    // .......................................................

                    fi = 0;
                    strLine = srFile.ReadLine(); // читаем далее (Numb/Fmin)
                    if ((strLine == "") || (strLine == null))
                        fi = 1;
                    // ...................................................................
                    if (strLine == null)
                    {
                        indmas += 1;
                        objSP.NumbDiap = indmas;
                        // Занести в List
                        GlobalVarLn.list1_f3.Add(objSP);
                        srFile.Close();
                        return;
                    }
                    // ...............................................
                    indStart = strLine.IndexOf(symb1, TekPoz); // N

                    if (indStart != -1)
                    {
                        indStop = strLine.IndexOf(symb4, TekPoz);  //=
                        iLength = indStop - indStart + 1;
                        iLength = indStop - indStart + 1;
                        // Убираем 'Numb ='
                        strLine1 = strLine.Remove(indStart, iLength);
                        indmas += 1;
                        objSP.NumbDiap = indmas;
                        // Занести в List
                        GlobalVarLn.list1_f3.Add(objSP); // предыдущий

                        objSP.NumbSP = strLine1;// новый номер
                        indmas = 0;
                        objSP.NumbDiap = 0;

                        fi = 0;
                        strLine = srFile.ReadLine(); // читаем далее (Fmini=)
                        if ((strLine == "") || (strLine == null))
                        {
                            fi = 1;
                        }

                    }
                    else
                    {
                        indmas += 1;
                    }


                } // WHILE
                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH

                // Занести в List
                GlobalVarLn.list1_f3.Add(objSP);

            }
            // -----------------------------------------------------------------------------------
            catch
            {
                srFile.Close();
                return;
            }
            // -------------------------------------------------------------------------------------
            srFile.Close();
            // -------------------------------------------------------------------------------------

        } // ReadfileFBSP_f3
        // ************************************************************************

        // ************************************************************************
        // функция чтения файла по FСП 1
        // ************************************************************************
        public void ReadFileFBSP1_f3(int fl)
        {
            // ------------------------------------------------------------------------------------
            String strLine = "";
            String strLine1 = "";
            double number1 = 0;
            int number2 = 0;
            char symb1 = 'N';
            char symb2 = 'X';
            char symb3 = 'Y';
            char symb4 = '=';
            char symb5 = 'H';
            char symb6 = 'R';
            char symb7 = 'A';
            char symb8 = 'T';
            char symb9 = '{';
            char symb10 = '}';
            char symb11 = 'F';
            char symb12 = 'B';

            int indStart = 0;
            int indStop = 0;
            int iLength = 0;
            int IndZap = 0;
            int TekPoz = 0;
            int fi = 0;
            int if1 = 0;
            int indmas = 0;

            // Чтение файла ---------------------------------------------------------
            String strFileName;

            //strFileName = "FSP_Special.txt";
            //0219
            strFileName = Application.StartupPath + "\\SaveInFiles\\FreqSpecial.txt";

            StreamReader srFile;
            try
            {
                srFile = new StreamReader(strFileName);
            }
            catch
            {
                MessageBox.Show("Can't open file");
                return;

            }
            // -------------------------------------------------------------------------------------
            // Чтение
            // Numb =...

            // Fmin =...
            // Fmax =...
            // Bmin =...
            // Bmax =...
            // ...

            indmas = 0;

            try
            {
                FBSP objSP = new FBSP();
                objSP.mfb = new FB[GlobalVarLn.sizeDiap_f3 + 10];

                TekPoz = 0;
                IndZap = 0;
                // .......................................................
                // Numb =...
                // 1-я строка

                strLine = srFile.ReadLine();

                if ((strLine == null) && (fl == 0))
                {
                    srFile.Close();
                    return;
                }

                if (strLine == null)
                {
                    MessageBox.Show("No information");
                    srFile.Close();
                    return;
                }

                indStart = strLine.IndexOf(symb1, TekPoz); // N

                if (indStart == -1)
                {
                    MessageBox.Show("No information");
                    srFile.Close();
                    return;
                }

                indStop = strLine.IndexOf(symb4, TekPoz);  //=

                if ((indStop == -1) || (indStop < indStart))
                {
                    MessageBox.Show("No information");
                    srFile.Close();
                    return;
                }

                iLength = indStop - indStart + 1;
                // Убираем 'Numb ='
                strLine1 = strLine.Remove(indStart, iLength);

                if (strLine1 == "")
                {
                    MessageBox.Show("No information");
                    srFile.Close();
                    return;
                }

                objSP.NumbSP = strLine1;
                // .......................................................

                fi = 0;
                strLine = srFile.ReadLine(); // читаем далее (Fmin1=)
                if ((strLine == "") || (strLine == null))
                {
                    fi = 1;
                }
                // .......................................................

                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH
                while ((strLine != "") && (strLine != null))
                {

                    // .......................................................
                    // Fmin =...

                    indStart = strLine.IndexOf(symb11, TekPoz); // F
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    number1 = Convert.ToDouble(strLine1);

                    objSP.mfb[indmas].Fmin = number1;
                    // .......................................................
                    // Fmax =...

                    strLine = srFile.ReadLine();

                    indStart = strLine.IndexOf(symb11, TekPoz); // F
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    number1 = Convert.ToDouble(strLine1);

                    objSP.mfb[indmas].Fmax = number1;

                    // .......................................................
                    if ((fl == 1) &&
                        (String.Compare(objSP.NumbSP, GlobalVarLn.NumbSP_f3) == 0)
                       )
                    {
                        GlobalVarLn.flrd1_f3 = 1;

                        dgvFreqSpec.Rows[(int)(IndZap)].Cells[0].Value = objSP.mfb[indmas].Fmin;
                        dgvFreqSpec.Rows[(int)(IndZap)].Cells[1].Value = objSP.mfb[indmas].Fmax;
                        IndZap += 1;

                    }
                    // .......................................................

                    fi = 0;
                    strLine = srFile.ReadLine(); // читаем далее (Numb/Fmin)
                    if ((strLine == "") || (strLine == null))
                        fi = 1;
                    // ...................................................................
                    if (strLine == null)
                    {
                        indmas += 1;
                        objSP.NumbDiap = indmas;
                        // Занести в List
                        GlobalVarLn.list2_f3.Add(objSP);
                        srFile.Close();
                        return;
                    }
                    // ...............................................
                    indStart = strLine.IndexOf(symb1, TekPoz); // N

                    if (indStart != -1)
                    {
                        indStop = strLine.IndexOf(symb4, TekPoz);  //=
                        iLength = indStop - indStart + 1;
                        iLength = indStop - indStart + 1;
                        // Убираем 'Numb ='
                        strLine1 = strLine.Remove(indStart, iLength);
                        indmas += 1;
                        objSP.NumbDiap = indmas;
                        // Занести в List
                        GlobalVarLn.list2_f3.Add(objSP); // предыдущий

                        objSP.NumbSP = strLine1;// новый номер
                        indmas = 0;
                        objSP.NumbDiap = 0;

                        fi = 0;
                        strLine = srFile.ReadLine(); // читаем далее (Fmini=)
                        if ((strLine == "") || (strLine == null))
                        {
                            fi = 1;
                        }

                    }
                    else
                    {
                        indmas += 1;
                    }


                } // WHILE
                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH

                // Занести в List
                GlobalVarLn.list2_f3.Add(objSP);

            }
            // -----------------------------------------------------------------------------------
            catch
            {
                srFile.Close();
                return;
            }
            // -------------------------------------------------------------------------------------
            srFile.Close();
            // -------------------------------------------------------------------------------------

        } // ReadfileFBSP1_f3
        // ************************************************************************


        // **************************************************************** ФУНКЦИИ



// SEND **********************************************************************************************
        /// <summary>
        /// Количество записей в таблице
        /// </summary>
        /// <param name="dgv"></param>
        /// <returns></returns>
        public int AmountRecordsDGV(DataGridView dgv)
        {
            int amount = 0;

            for (int i = 0; i < dgv.RowCount; i++)
            {
                if (/*dgv.Rows[i].Cells[0].Value != null || */dgv.Rows[i].Cells[0].Value.ToString() != "")
                {
                    amount++;
                }
            }

            return amount;
        }
// ---------------------------------------------------------------------------------------------------
        /// <summary>
        /// Send Frequencies Forbidden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bSendFreqForbid_Click(object sender, EventArgs e)
        {
            double f1=0;
            double f2=0;


            int iAmount = AmountRecordsDGV(dgvFreqForbid);

            //TRangeSpec[] tRangeSpecTemp = new TRangeSpec[iAmount];
            TRangeSpec[] tRangeSpecTemp = new TRangeSpec[iAmount];

            for (int i = 0; i < iAmount; i++)
            {
                tRangeSpecTemp[i] = new TRangeSpec();


                f1=Convert.ToDouble(dgvFreqForbid.Rows[i].Cells[0].Value);
                f2=Convert.ToDouble(dgvFreqForbid.Rows[i].Cells[1].Value);

                ichislo = (long)(f1 * 10);
                dchislo = ((double)ichislo) / 10;
                if (dchislo < 100) f1 = 100;
                else if (dchislo > 6000) f1 = 6000;
                else f1 = dchislo;

                ichislo = (long)(f2 * 10);
                dchislo = ((double)ichislo) / 10;
                if (dchislo < 100) f2 = 100;
                else if (dchislo > 6000) f2 = 6000;
                else f2 = dchislo;


                //tRangeSpecTemp[i].iFregMin = (int)(Convert.ToDouble(dgvFreqForbid.Rows[i].Cells[0].Value)*1000);
                //tRangeSpecTemp[i].iFregMax = (int)(Convert.ToDouble(dgvFreqForbid.Rows[i].Cells[1].Value)*1000);
                tRangeSpecTemp[i].iFregMin = (int)(f1 * 1000);
                tRangeSpecTemp[i].iFregMax = (int)(f2 * 1000);


            }

            if (mapForm.clientPC != null)
                //mapForm.clientPC.SendRangeSpec(3, 0, tRangeSpecTemp);
                //mapForm.clientPC.SendRangeSpec((byte)GlobalVarLn.AdressOwn, 0, tRangeSpecTemp);
                mapForm.clientPC.SendRangeForbid((byte)GlobalVarLn.AdressOwn, 0, tRangeSpecTemp);


        } // SEND
// ********************************************************************************************** SEND

// SEND1 *********************************************************************************************

private void bSendFreqForbit1_Click(object sender, EventArgs e)
 {

     double f1 = 0;
     double f2 = 0;


     int iAmount = AmountRecordsDGV(dgvFreqSpec);

     // ???????????????
     TRangeSpec[] tRangeSpecTemp = new TRangeSpec[iAmount];

     for (int i = 0; i < iAmount; i++)
     {
         // ?????????????????
         tRangeSpecTemp[i] = new TRangeSpec();


         f1 = Convert.ToDouble(dgvFreqSpec.Rows[i].Cells[0].Value);
         f2 = Convert.ToDouble(dgvFreqSpec.Rows[i].Cells[1].Value);

         ichislo = (long)(f1 * 10);
         dchislo = ((double)ichislo) / 10;
         if (dchislo < 100) f1 = 100;
         else if (dchislo > 6000) f1 = 6000;
         else f1 = dchislo;

         ichislo = (long)(f2 * 10);
         dchislo = ((double)ichislo) / 10;
         if (dchislo < 100) f2 = 100;
         else if (dchislo > 6000) f2 = 6000;
         else f2 = dchislo;

         // ??????????????????????
         tRangeSpecTemp[i].iFregMin = (int)(f1 * 1000);
         tRangeSpecTemp[i].iFregMax = (int)(f2 * 1000);

     }

     // ???????????????
     if (mapForm.clientPC != null)
         mapForm.clientPC.SendRangeSpec((byte)GlobalVarLn.AdressOwn, 0, tRangeSpecTemp);

 }
        // ********************************************************************************************* SEND1

        // -----------------------------------------------------------------------------
        private void button1_Click(object sender, EventArgs e)
        {
            ;
        }
        // -----------------------------------------------------------------------------

    } // Class
} // NameSpace
