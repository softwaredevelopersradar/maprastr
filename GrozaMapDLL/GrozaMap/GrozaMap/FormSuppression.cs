﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Collections.Generic;

using System.IO;
using System.Collections.Generic;

using System.Windows.Input;
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;
using Bitmap = System.Drawing.Bitmap;
using Point = System.Drawing.Point;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.ServiceModel;
using System.Diagnostics;
using System.Threading;
using System.Globalization;

using MapRastr;  // DLL


namespace GrozaMap
{
    public partial class FormSuppression : Form
    {
        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        private long ichislo;

        // .....................................................................
        // Координаты СП,ys

        // Координаты СП на местности в м
        private double XSP_comm;
        private double YSP_comm;
        private double XYS1_comm;
        private double YYS1_comm;

        // ......................................................................
        // Основные параметры

        private double OwnHeight_comm;
        private double Point1Height_comm;
        private double PowerOwn_comm;
        private double CoeffOwn_comm;
        private double CoeffOwnPod_comm;

        private int i_HeightOwnObject_comm;
        private int i_Pt1HeightOwnObject_comm;

        // Высота средства подавления
        private double HeightAntennOwn_comm;
        private double HeightTotalOwn_comm;
        private double Pt1HeightTotalOwn_comm;
        private double Pt1HeightTotalOwn1_comm;

        // Для подавляемой линии
        private double PowerOpponent_comm;
        private double CoeffTransmitOpponent_comm;
        private double CoeffReceiverOpponent_comm;
        private double HeightTransmitOpponent_comm;
        private double HeightTransmitOpponent1_comm;
        private int i_PolarOpponent_comm;
        // ......................................................................
        // Зона
        private int iMiddleHeight_comm;
        private int iGamma;

        // ......................................................................

        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // Конструктор *********************************************************** 

        public FormSuppression()
        {
            InitializeComponent();

            ichislo = 0;

            //LAMBDA = 300000;

            // .....................................................................
            // Координаты СП

            // Координаты СП на местности в м
            XSP_comm = 0;
            YSP_comm = 0;
            XYS1_comm = 0;
            YYS1_comm = 0;

            // ......................................................................
            // Основные параметры

            OwnHeight_comm = 0;
            Point1Height_comm = 0;
            PowerOwn_comm = 0;
            CoeffOwn_comm = 0;
            CoeffOwnPod_comm = 0;

            i_HeightOwnObject_comm = 0;
            i_Pt1HeightOwnObject_comm = 0;

            // Высота средства подавления
            // ??????????????????????
            HeightAntennOwn_comm = 0;
            HeightTotalOwn_comm = 0;
            Pt1HeightTotalOwn_comm = 0;
            Pt1HeightTotalOwn1_comm = 0;

            // Для подавляемой линии
            PowerOpponent_comm = 0;
            CoeffTransmitOpponent_comm = 0;
            CoeffReceiverOpponent_comm = 0;
            HeightTransmitOpponent_comm = 0;
            HeightTransmitOpponent1_comm = 0;
            i_PolarOpponent_comm = 0;

            // ......................................................................
            // Зона
            iMiddleHeight_comm = 0;
            iGamma = 0;

        } // Конструктор
        // ***********************************************************  Конструктор

        // Загрузка формы *********************************************************

        private void FormSuppression_Load(object sender, EventArgs e)
        {
            // ----------------------------------------------------------------------
            ClassMapRastrLoadForm.f_Load_FormSuppression();
            // ----------------------------------------------------------------------
            // Изменение списка СП

            GlobalVarLn.ListJSChangedEvent += OnListJSChanged;
            // ----------------------------------------------------------------------

        }
        // ********************************************************* Загрузка формы

        // Изменение списка СП ****************************************************

        private void OnListJSChanged(object sender, EventArgs e)
        {
            MapForm.UpdateDropDownList(cbOwnObject);
        }
        // **************************************************** Изменение списка СП

        // Activated **************************************************************

        private void FormSuppression_Activated(object sender, EventArgs e)
        {
            GlobalVarLn.fl_Open_objFormSuppression = 1;

        }
        // ************************************************************** Activated

        // Закрыть форму **********************************************************

        private void FormSuppression_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

            GlobalVarLn.fl_Open_objFormSuppression = 0;

        }
        // ********************************************************** Закрыть форму

        // Очистка ****************************************************************

        private void bClear_Click(object sender, EventArgs e)
        {
            // --------------------------------------------------------------------
            ClassMapRastrClear.f_Clear_FormSuppression();
            // --------------------------------------------------------------------
            // Убрать с карты

            // CLASS
            ClassMapRastrReDrawAll.REDRAW_MAP_CLASS();
            // --------------------------------------------------------------------

        }
        // **************************************************************** Очистка

        // ************************************************************************
        // Обработчик ComboBox : Выбор СК SP
        // ************************************************************************
        private void cbChooseSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChooseSystemCoordSP_sup(cbChooseSC.SelectedIndex);

        }
        // ************************************************************************

        // ************************************************************************
        // Обработчик ComboBox : Выбор СК OP
        // ************************************************************************
        private void cbCommChooseSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            //ChooseSystemCoordOP_sup(cbCommChooseSC.SelectedIndex);

        }
        // ************************************************************************

        // ************************************************************************
        // Обработчик ComboBox "cbOwnObject": Выбор SP
        // ************************************************************************
        private void cbOwnObject_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbOwnObject.SelectedIndex == 0)
                GlobalVarLn.NumbSP_sup = "";
            else
                GlobalVarLn.NumbSP_sup = Convert.ToString(cbOwnObject.Items[cbOwnObject.SelectedIndex]);

        }
        // ************************************************************************

        // SP *********************************************************************
        // Выбор СП

        private void button1_Click(object sender, EventArgs e)
        {
            double xtmp_ed, ytmp_ed;

            var objClassMap3_ed = new ClassMap();
            // ......................................................................

            // Выбор координат COORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOOR

            // ----------------------------------------------------------------------
            // Мышь на карте

            if (cbOwnObject.SelectedIndex == 0)
            {
                GlobalVarLn.NumbSP_sup = "";
                // !!! реальные координаты на местности карты в м (Plane)

                XSP_comm = GlobalVarLn.X_Rastr;
                YSP_comm = GlobalVarLn.Y_Rastr;
                OwnHeight_comm = GlobalVarLn.H_Rastr;

            }
            // ----------------------------------------------------------------------
            // Выбор из списка СП

            else
            {
                GlobalVarLn.NumbSP_sup = Convert.ToString(cbOwnObject.Items[cbOwnObject.SelectedIndex]);
                var stations = GlobalVarLn.listJS;
                var station = stations[cbOwnObject.SelectedIndex - 1];

                XSP_comm = station.CurrentPosition.x;
                YSP_comm = station.CurrentPosition.y;
                OwnHeight_comm = station.CurrentPosition.h;

            }

            tbOwnHeight.Text = Convert.ToString(OwnHeight_comm);
            // ----------------------------------------------------------------------

            GlobalVarLn.XCenter_sup = XSP_comm;
            GlobalVarLn.YCenter_sup = YSP_comm;

            // SP
            // CLASS
            ClassMapRastrDraw.f_DrawImage(
                          GlobalVarLn.XCenter_sup,  // Mercator
                          GlobalVarLn.YCenter_sup,
                          -1,
                          -1,
                          Application.StartupPath + "\\ImagesHelp\\" + "1.png",
                          "",
                          // Масштаб изображения
                          2
                         );

            // ......................................................................
            GlobalVarLn.flCoordSP_sup = 1; // СП выбрана
            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            xtmp_ed = XSP_comm;
            ytmp_ed = YSP_comm;

            var p = Mercator.ToLonLat(GlobalVarLn.XCenter_sup, GlobalVarLn.YCenter_sup);
            GlobalVarLn.LatCenter_sup_84 = p.Y;
            GlobalVarLn.LongCenter_sup_84 = p.X;

/*
            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;
            // .......................................................................
            // CDel

            // WGS84,grad
            LatKrG_comm = xtmp1_ed;
            LongKrG_comm = ytmp1_ed;
            // .......................................................................
            LatKrR_comm = (LatKrG_comm * Math.PI) / 180;
            LongKrR_comm = (LongKrG_comm * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS
            // CDel
            // !!! Здесь это WGS84

            // Широта
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LatKrG_comm,

                // Выходные параметры 
                ref Lat_Grad_comm,
                ref Lat_Min_comm,
                ref Lat_Sec_comm

              );

            // Долгота
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LongKrG_comm,

                // Выходные параметры 
                ref Long_Grad_comm,
                ref Long_Min_comm,
                ref Long_Sec_comm

              );
*/
            OtobrSP_sup();
            // .......................................................................


        } // SP
        // ********************************************************************* SP

        // ************************************************************************
        // OP

        private void button2_Click(object sender, EventArgs e)
        {
            double xtmp_ed, ytmp_ed;
            // ......................................................................
            var objClassMap5_ed = new ClassMap();
            // ......................................................................
            // !!! реальные координаты на местности карты в м (Plane)

            XYS1_comm = GlobalVarLn.X_Rastr;
            YYS1_comm = GlobalVarLn.Y_Rastr;
            Point1Height_comm = GlobalVarLn.H_Rastr;
            tbPt1Height.Text = Convert.ToString(Point1Height_comm);

            // ......................................................................

            // ........................................................................
            // Треугольник(red) на карте

            GlobalVarLn.XPoint1_sup = XYS1_comm;
            GlobalVarLn.YPoint1_sup = YYS1_comm;

            // CLASS
            ClassMapRastrDraw.f_DrawImage(
                          GlobalVarLn.XPoint1_sup,  // Mercator
                          GlobalVarLn.YPoint1_sup,
                          -1,
                          -1,
                          Application.StartupPath + "\\ImagesHelp\\" + "TriangleRed.png",
                          //"TriangleRed.png",
                          "",
                          0.6
                         );

            GlobalVarLn.flCoordOP = 1; // OP выбран
            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            xtmp_ed = XYS1_comm;
            ytmp_ed = YYS1_comm;

            var p = Mercator.ToLonLat(XYS1_comm, YYS1_comm);
            GlobalVarLn.LatCenter_sup_OP_84 = p.Y;
            GlobalVarLn.LongCenter_sup_OP_84 = p.X;

/*
            mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);

            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;
            // .......................................................................
            // CDel

            // WGS84,grad
            LatKrG_YS1_comm = xtmp1_ed;
            LongKrG_YS1_comm = ytmp1_ed;
            // Широта
            objClassMap5_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LatKrG_YS1_comm,

                // Выходные параметры 
                ref Lat_Grad_YS1_comm,
                ref Lat_Min_YS1_comm,
                ref Lat_Sec_YS1_comm

              );

            // Долгота
            objClassMap5_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LongKrG_YS1_comm,

                // Выходные параметры 
                ref Long_Grad_YS1_comm,
                ref Long_Min_YS1_comm,
                ref Long_Sec_YS1_comm

              );
*/

            OtobrOP_sup();

            // CLASS
            ClassMapRastrReDrawAll.REDRAW_MAP_CLASS();
            // .......................................................................


        } // OP
        // ************************************************************************

        // Зоны подавления+неподавления ******************************************* 
        // Button "Принять"

        private void bAccept_Click(object sender, EventArgs e)
        {

            String s1 = "";

            // ------------------------------------------------------------------
            if (GlobalVarLn.flCoordSP_sup == 0)
            {
                MessageBox.Show("Jammer station is not selected");
                return;
            }

            if (GlobalVarLn.flCoordOP == 0)
            {
                MessageBox.Show("Object of jam is not selected");
                return;
            }
            // ------------------------------------------------------------------
            if (GlobalVarLn.listControlJammingZone.Count != 0)
                GlobalVarLn.listControlJammingZone.Clear();
            // ----------------------------------------------------------------------

            GlobalVarLn.fl_Suppression = 1;


            // Ввод параметров ********************************************************
            // !!! Координаты SP,OP уже расчитаны и введены по кнопке 'SP','OP'

            // ------------------------------------------------------------------
            // SP

            // Мощность 

            s1 = tbPowerOwn.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                PowerOwn_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    PowerOwn_comm = Convert.ToDouble(s1);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }

            }

            if ((PowerOwn_comm < 10) || (PowerOwn_comm > 300))
            {
                MessageBox.Show("Parameter 'power' out of range 10W - 300W ");
                return;
            }


            // Коэффициент усиления
            s1 = tbCoeffOwn.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                CoeffOwn_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    CoeffOwn_comm = Convert.ToDouble(s1);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }

            }
            if ((CoeffOwn_comm < 4) || (CoeffOwn_comm > 8))
            {
                MessageBox.Show("Parameter 'gain' out of range 4-8");
                return;
            }

            // Коэффициент подавления
            s1 = tbCoeffSupOwn.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                CoeffOwnPod_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    CoeffOwnPod_comm = Convert.ToDouble(s1);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }

            }

            if ((CoeffOwnPod_comm < 1) || (CoeffOwnPod_comm > 100))
            {
                MessageBox.Show("Parameter 'J/S ratio' out of range 1-100");
                return;
            }

            // Высота антенны
            //HeightAntennOwn_comm = Convert.ToDouble(tbHAnt.Text);
            s1 = tbHAnt.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                HeightAntennOwn_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    HeightAntennOwn_comm = Convert.ToDouble(s1);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }

            }

            // ComboBox (Индексы)
            i_HeightOwnObject_comm = cbHeightOwnObject.SelectedIndex; // Средство РП

            // ------------------------------------------------------------------
            // OP

            // Мощность 
            //PowerOpponent_comm = Convert.ToDouble(textBox2.Text);
            s1 = textBox2.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                PowerOpponent_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    PowerOpponent_comm = Convert.ToDouble(s1);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }

            }
            if ((PowerOpponent_comm < 1) || (PowerOpponent_comm > 100))
            {
                MessageBox.Show("Parameter 'power' out of range 1W - 100W");
                return;
            }

            // коэффициент усиления 
            s1 = textBox3.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                CoeffTransmitOpponent_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    CoeffTransmitOpponent_comm = Convert.ToDouble(s1);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }

            }
            if ((CoeffTransmitOpponent_comm < 1) || (CoeffTransmitOpponent_comm > 10))
            {
                MessageBox.Show("Parameter 'gain' out of range 1-10");
                return;
            }

            // коэффициент подавления ??????????????? не использ.
            s1 = textBox4.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                CoeffReceiverOpponent_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    CoeffReceiverOpponent_comm = Convert.ToDouble(s1);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }

            }

            // антенна 
            s1 = tbOpponentAntenna.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                HeightTransmitOpponent_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    HeightTransmitOpponent_comm = Convert.ToDouble(s1);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }

            }

            s1 = tbOpponentAntenna1.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                HeightTransmitOpponent1_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    HeightTransmitOpponent1_comm = Convert.ToDouble(s1);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }

            }

            // ComboBox (Индексы)
            i_Pt1HeightOwnObject_comm = cbPt1HeightOwnObject.SelectedIndex; // OP
            i_PolarOpponent_comm = cbPolarOpponent.SelectedIndex;       // Поляризация

            // ------------------------------------------------------------------

            // ******************************************************** Ввод параметров

            // SP *********************************************************************
            // Координаты на местности в 

            GlobalVarLn.tpOwnCoordRect_sup.X = (int) GlobalVarLn.XCenter_sup;
            GlobalVarLn.tpOwnCoordRect_sup.Y = (int) GlobalVarLn.YCenter_sup;

            if ((tbXRect.Text == "") || (tbYRect.Text == ""))
            {
                MessageBox.Show("Incorrect data");
                return;
            }
            // ........................................................................
            // Высота из карты

            GlobalVarLn.H_sup = OwnHeight_comm;
            // ********************************************************************* SP

            // OP *********************************************************************
            // Координаты на местности в м

            var x = Convert.ToDouble(tbPt1XRect.Text);
            var y = Convert.ToDouble(tbPt1YRect.Text);

            //0210
            GlobalVarLn.tpPoint1Rect_sup.X = (int) GlobalVarLn.XPoint1_sup;
            GlobalVarLn.tpPoint1Rect_sup.Y = (int) GlobalVarLn.YPoint1_sup;

            if ((tbPt1XRect.Text == "") || (tbPt1YRect.Text == ""))
            {
                MessageBox.Show("Incorrect data");
                return;
            }
            // ********************************************************************* OP

            // H средства подавления **************************************************
            // определить значение высоты средства подавления

            i_HeightOwnObject_comm = 0;
            switch (i_HeightOwnObject_comm)
            {
                // рельеф местности+высота антенны
                case 0:
                    HeightTotalOwn_comm = HeightAntennOwn_comm + OwnHeight_comm;
                    break;

                // высота антенны
                case 1:
                    HeightTotalOwn_comm = HeightAntennOwn_comm;
                    break;

                // задать самостоятельно
                case 2:
                    if (tbHeightOwnObject.Text == "")
                        HeightTotalOwn_comm = 0;
                    else
                    {
                        //HeightTotalOwn_comm = Convert.ToDouble(tbHeightOwnObject.Text);
                        s1 = tbHeightOwnObject.Text;
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            HeightTotalOwn_comm = Convert.ToDouble(s1);
                        }
                        catch (SystemException)
                        {
                            try
                            {
                                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                                HeightTotalOwn_comm = Convert.ToDouble(s1);
                            }
                            catch
                            {
                                MessageBox.Show("Incorrect data");
                                return;
                            }

                        }

                    } // else

                    break;

            } // Switch

            // отобразить значение высоты
            ichislo = (long)(HeightTotalOwn_comm);
            tbHeightOwnObject.Text = Convert.ToString(ichislo);

            // ************************************************** H средства подавления

            // H объекта подавления ***************************************************

            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
            // Средняя высота местности

           // DLL
           ClassMapRastr objClassMap1 = new ClassMapRastr();
           MyPoint objMyPoint1 = new MyPoint();
           objMyPoint1.X = (int)GlobalVarLn.tpOwnCoordRect_sup.X;
           objMyPoint1.Y = (int)GlobalVarLn.tpOwnCoordRect_sup.Y;
           iMiddleHeight_comm = objClassMap1.DefineMiddleHeight_Comm(
                                                                objMyPoint1,
                                                                MapForm.RasterMapControl,
                                                                30000, // R
                                                                100   // Step
                                                                    );
            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

            tbCoeffHE.Text = iMiddleHeight_comm.ToString();

            i_Pt1HeightOwnObject_comm = 0;
            switch (i_Pt1HeightOwnObject_comm)
            {
                // рельеф местности+высота антенны
                case 0:
                    Pt1HeightTotalOwn_comm = HeightTransmitOpponent_comm + Point1Height_comm;
                    Pt1HeightTotalOwn1_comm = iMiddleHeight_comm + HeightTransmitOpponent1_comm;
                    break;

                // высота антенны
                case 1:
                    Pt1HeightTotalOwn_comm = HeightTransmitOpponent_comm;
                    break;

                // задать самостоятельно
                case 2:
                    Pt1HeightTotalOwn_comm = 0;
                    break;

            } // Switch

            // отобразить значение высоты
            ichislo = (long)(Pt1HeightTotalOwn_comm);

            // *************************************************** H объекта подавления

            // Расчет зоны ************************************************************

            // ------------------------------------------------------------------------
            // DLL

            ClassMapRastr objClassMap2 = new ClassMapRastr();
            MyPoint objMyPointSP = new MyPoint();
            MyPoint objMyPointOP = new MyPoint();
            MyPoint objMyPointCenterNoSup = new MyPoint();

            objMyPointSP.X = GlobalVarLn.tpOwnCoordRect_sup.X;
            objMyPointSP.Y = GlobalVarLn.tpOwnCoordRect_sup.Y;
            objMyPointOP.X = GlobalVarLn.tpPoint1Rect_sup.X;
            objMyPointOP.Y = GlobalVarLn.tpPoint1Rect_sup.Y;

            iGamma = 1;

            int R_Zone_Sup_Tmp = 0;
            int R_Zone_NoSup_Tmp = 0;

            var PolZoneSuppression = objClassMap2.f_ZoneSuppression(

                                                  // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                                  MapForm.RasterMapControl,

                                                  // Координаты СП
                                                  objMyPointSP,
                                                  // Координаты ОП
                                                  objMyPointOP,

                                                  // Hantsp
                                                  (int)HeightAntennOwn_comm,
                                                  // Hrelsp
                                                  (int)OwnHeight_comm,
                                                  // Hsredn
                                                  iMiddleHeight_comm,
                                                  // Hantop
                                                  (int)HeightTransmitOpponent_comm,
                                                  // Hantop_prm
                                                  (int)HeightTransmitOpponent1_comm,

                                                  // P, Wt, SP
                                                  PowerOwn_comm,
                                                  // P, Wt, OP
                                                  PowerOpponent_comm,

                                                  // К-т усиления, СП
                                                  CoeffOwn_comm,
                                                  // =1 пока подаем
                                                  (double)iGamma,
                                                  // К-т усиления, ОП
                                                  CoeffTransmitOpponent_comm,
                                                  // К-т подавления
                                                  CoeffOwnPod_comm,

                                                  // шаг изменения угла в град при расчете полигона
                                                  GlobalVarLn.iStepAngleInput_ZPV,
                                                  // шаг в м при расчете ЗПВ
                                                  GlobalVarLn.iStepLengthInput_ZPV,

                                                  // R zone suppression
                                                  ref R_Zone_Sup_Tmp,
                                                  // R zone NOsuppression
                                                  ref R_Zone_NoSup_Tmp,
                                                  // =2 -> Centre=OP, =3 -> Centre=SP
                                                  ref GlobalVarLn.flA_sup,
                                                  // Координаты центра зоны неподавления
                                                  ref objMyPointCenterNoSup
                                                                    );

            // ????????????????
            GlobalVarLn.RZ_sup = (double)R_Zone_Sup_Tmp;
            GlobalVarLn.RZ1_sup = (double)R_Zone_NoSup_Tmp;

            tbRadiusZone.Text = Convert.ToString(R_Zone_Sup_Tmp);
            tbRadiusZone1.Text = Convert.ToString(R_Zone_NoSup_Tmp);

            Point objpol = new Point();
            for (int ipol = 0; ipol < PolZoneSuppression.Count; ipol++)
            {
                objpol.X = PolZoneSuppression[ipol].X;
                objpol.Y = PolZoneSuppression[ipol].Y;
                GlobalVarLn.listControlJammingZone.Add(objpol);

            }

            GlobalVarLn.tpPointCentre1_sup.X = objMyPointCenterNoSup.X;
            GlobalVarLn.tpPointCentre1_sup.Y = objMyPointCenterNoSup.Y;
            // ------------------------------------------------------------------------
            // Убрать с карты и отрисовать зоны подавления и неподавления

            // CLASS
            ClassMapRastrReDrawAll.REDRAW_MAP_CLASS();
            // ------------------------------------------------------------------------

        }
        // *******************************************  Зоны подавления+неподавления

        // NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEW
        private void ChooseSystemCoordSP_sup(int iSystemCoord)
        {
        } // ChooseSystemCoordOP_sup


        private void OtobrSP_sup()
        {
            tbXRect.Text = GlobalVarLn.LatCenter_sup_84.ToString("F3");
            tbYRect.Text = GlobalVarLn.LongCenter_sup_84.ToString("F3");

        } // OtobrSP_sup

        // NEW NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWN
        private void OtobrOP_sup()
        {

            tbPt1XRect.Text = GlobalVarLn.LatCenter_sup_OP_84.ToString("F3");
            tbPt1YRect.Text = GlobalVarLn.LongCenter_sup_OP_84.ToString("F3");


        } // OtobrOP_sup

        // NEW NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWN
          

        // ************************************************************************
        // !!! Не закончена

        private void DefLuch(Point tpPointOwn, Point tpPointOp)
        {


            double x1 = 0;
            double y1 = 0;
            double x2 = 0;
            double y2 = 0;
            double x3 = 0;
            double y3 = 0;
            double x4 = 0;
            double y4 = 0;
            double dx = 0;
            double dy = 0;
            double alf = 0;
            double bet = 0;
            double x0 = 0;
            double y0 = 0;
            double D = 0;
            double a = 0;

            GlobalVarLn.Luch_sup3.X = 0;
            GlobalVarLn.Luch_sup3.Y = 0;
            GlobalVarLn.Luch_sup4.X = 0;
            GlobalVarLn.Luch_sup4.Y = 0;

            // IF1
            if ((tpPointOwn.X != 0) & (tpPointOwn.Y != 0) & (tpPointOp.X != 0) & (tpPointOp.Y != 0))
            {
                x1 = (double)(tpPointOwn.X);
                y1 = (double)(tpPointOwn.Y);
                x2 = (double)(tpPointOp.X);
                y2 = (double)(tpPointOp.Y);
                // ----------------------------------------------------------------------------------
                if ((y1 == y2) && (x2 > x1))
                {
                    x3 = (x2 - x1) / 2;
                    x4 = (x2 - x1) / 2;
                    y3 = y1 - GlobalVarLn.s_sup;
                    y4 = y1 + GlobalVarLn.s_sup;

                }
                // ----------------------------------------------------------------------------------
                else if ((y1 == y2) && (x2 < x1))
                {
                    x3 = (x1 - x2) / 2;
                    x4 = (x1 - x2) / 2;
                    y3 = y1 - GlobalVarLn.s_sup;
                    y4 = y1 + GlobalVarLn.s_sup;

                }
                // ----------------------------------------------------------------------------------
                else if ((x1 == x2) && (y2 > y1))
                {
                    y3 = (y2 - y1) / 2;
                    y4 = (y2 - y1) / 2;
                    x3 = x1 + GlobalVarLn.s_sup;
                    x4 = x1 - GlobalVarLn.s_sup;

                }
                // ----------------------------------------------------------------------------------
                else if ((x1 == x2) && (y1 > y2))
                {
                    y3 = (y1 - y2) / 2;
                    y4 = (y1 - y2) / 2;
                    x3 = x1 + GlobalVarLn.s_sup;
                    x4 = x1 - GlobalVarLn.s_sup;

                }
                // ----------------------------------------------------------------------------------
                else if ((x2 > x1) && (y1 < y2))
                {
                    dx = x2 - x1;
                    dy = y2 - y1;
                    alf = Math.Atan(dx/dy);
                    x0=(x2-x1)/2;
                    y0=(y2-y1)/2;
                    D = Math.Sqrt(dx*dx+dy*dy);
                    a = Math.Sqrt((D / 2) * (D / 2) + GlobalVarLn.s_sup * GlobalVarLn.s_sup);
                    bet = Math.Atan(GlobalVarLn.s_sup/(D/2));
                    y3 = y1 + a * Math.Cos(alf+bet);
                    x3 = x1 + a * Math.Sin(alf+bet);
                    y4 = y1 + a * Math.Cos(alf - bet);
                    x4 = x1 - a * Math.Sin(alf-  bet);

                }
                // ----------------------------------------------------------------------------------
                // ----------------------------------------------------------------------------------
                // ----------------------------------------------------------------------------------
                // ----------------------------------------------------------------------------------
                // ----------------------------------------------------------------------------------




            } // IF1

            GlobalVarLn.Luch_sup3.X = (int)x3;
            GlobalVarLn.Luch_sup3.Y = (int)y3;
            GlobalVarLn.Luch_sup4.X = (int)x4;
            GlobalVarLn.Luch_sup4.Y = (int)y4;


        }
        // ************************************************************************

        // -----------------------------------------------------------------------------------------
        private void label20_Click(object sender, EventArgs e)
        {
            ;
        }

        private void tbRadiusZone1_TextChanged(object sender, EventArgs e)
        {

        }
        // -----------------------------------------------------------------------------------------


    } // Class
} // Namespace
