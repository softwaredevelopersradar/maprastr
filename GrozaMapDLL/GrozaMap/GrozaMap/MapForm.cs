﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
using System.Windows.Input;

using System.IO;
using System.Windows.Input;
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;

using System.Globalization;
using System.Threading.Tasks;

using PC_DLL;
using System.Reflection;

using ClassLibraryPeleng;
using GeoCalculator;
// ------------------------------------------------------------------

namespace GrozaMap
{

    public partial class MapForm : Form
    {

        // ------------------------------------------------------------------
        public static RasterMapControl RasterMapControl { get; private set; }
        // ------------------------------------------------------------------

        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();
        // ------------------------------------------------------------------
        // EVENTS

        // sob
        public delegate void UpdateTableReconFWSEventHandler(TReconFWS[] tReconFWS);
        public event UpdateTableReconFWSEventHandler UpdateTableReconFWS;
        // ------------------------------------------------------------------
        // ???????????????????
        // Ybrat

        private bool MapIsOpenned
        {
            get { return RasterMapControl.IsMapLoaded; }
        }
        private bool HeightMatrixIsOpenned = false;
        // ------------------------------------------------------------------

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 

        // ------------------------------------------------------------------
        // Создаем объекты форм (инициализируются при загрузке головной формы)
        // для обращения из других файлов

        public FormWay objFormWay;
        public FormSP objFormSP;
        public FormLF objFormLF;
        public FormLF2 objFormLF2;
        public FormZO objFormZO;
        public FormTRO objFormTRO;
        public FormOB1 objFormOB1;
        public FormOB2 objFormOB2;
        public FormSuppression objFormSuppression;
        public FormSPFB objFormSPFB;
        public FormSPFB1 objFormSPFB1;
        public FormSPFB2 objFormSPFB2;
        public FormAz1 objFormAz1;
        public FormLineSightRange objFormLineSightRange;
        public FormSupSpuf objFormSupSpuf;
        public FormTab objFormTab;
        public FormSpuf objFormSpuf;
        public FormScreen objFormScreen;
        // ------------------------------------------------------------------
        // ???????????????????? Убрать
        public static int hmapl1;
        // ------------------------------------------------------------------
        // ?????????????????? Убрать
        public static bool blAirObject = false;
        // ------------------------------------------------------------------

        string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        // ------------------------------------------------------------------
        // create client
        public ClientPC clientPC;// = new ClientPC(1);
        // ------------------------------------------------------------------

        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // КОНСТРУКТОР **********************************************************
        public MapForm()
        {
            // ------------------------------------------------------------------
            InitializeComponent();
            // ------------------------------------------------------------------

            // Там, где лежит карта
            RasterMapControl = new RasterMapControl();
            MapElementHost.Child = RasterMapControl;

            RasterMapControl.MouseEnter += RasterMapControl_MouseEnter;
            RasterMapControl.MapMouseMoveEvent += OnMapMapMouseMove;
            RasterMapControl.MapClickEvent += OnMapClick;
            // ------------------------------------------------------------------
            // Объекты форм (инициализируются при загрузке головной формы)
            // для обращения из других файлов

            objFormWay = new FormWay();
            objFormSP = new FormSP();
            objFormLF = new FormLF();
            objFormLF2 = new FormLF2();
            objFormZO = new FormZO();
            objFormTRO = new FormTRO();
            objFormOB1 = new FormOB1();
            objFormOB2 = new FormOB2();
            objFormSuppression = new FormSuppression();
            objFormSPFB = new FormSPFB();
            objFormSPFB1 = new FormSPFB1();
            objFormSPFB2 = new FormSPFB2(this);
            objFormAz1 = new FormAz1();
            objFormLineSightRange = new FormLineSightRange();
            objFormSupSpuf = new FormSupSpuf();
            objFormTab = new FormTab(this);
            // seg1
            objFormSpuf = new FormSpuf();
            objFormScreen = new FormScreen(this);
            // ------------------------------------------------------------------
            // IPAddress, Port, AdressOwn, AdressLinked from Setting.ini

            if (System.IO.File.Exists(Application.StartupPath + "\\Setting.ini"))
            {
                string sIPAddress = iniRW.get_IPaddress();
                tbIP.Text = sIPAddress;

                int iPort = iniRW.get_Port();
                tbPort.Text = iPort.ToString();

                GlobalVarLn.AdressOwn = iniRW.get_AdressOwn();
                GlobalVarLn.AdressLinked = iniRW.get_AdressLinked();

            }
            else
            {
                MessageBox.Show("Can’t open file INI! \n", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            // ------------------------------------------------------------------

            // MAP,MATRIX RESOLUTION *****************************************************************
            // CLASS

            string s = "";
            s=ClassMapRastrMenu.f_OpenMap(
                                        MapForm.RasterMapControl,
                                        ref GlobalVarLn.MapOpened
                                        );
            if(s!="")
                MessageBox.Show(s);

            s=ClassMapRastrMenu.f_OpenMatrix(
                                        MapForm.RasterMapControl,
                                        ref GlobalVarLn.MatrixOpened
                                            );
            if (s != "")
                MessageBox.Show(s);

            s=ClassMapRastrMenu.f_GetResolution(
                                        MapForm.RasterMapControl
                                               );
            if (s != "")
                MessageBox.Show(s);

            // ***************************************************************** MAP,MATRIX RESOLUTION

            // .......................................................................................
            /*
                        /// WCF
                        //создаем сервис
                        var service = new ServiceImplementation();
                        //подписываемся на событие HelloReceived
                        service.AirPlaneReceived += Service_AirPlaneReceived;
                        //стартуем сервер
                        var svh = new ServiceHost(service);
                        svh.AddServiceEndpoint(typeof(Contract.IService), new NetTcpBinding(), "net.tcp://localhost:8000");
                        svh.Open();
             */
            // ........................................................................................


        }

        // *******************
        //0206* ?????
        void RasterMapControl_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            RasterMapControl.InnerMapControl.Focus();
        }
        // ********************

        // ********************************************************************* КОНСТРУКТОР

        // GPS ******************************************************************************
        // Обработчик события

        void clientPC_OnConfirmCoord(object sender, byte bAddress, byte bCodeError, double dLatitudeOwn, double dLongitudeOwn, double dLatitudeLinked, double dLongitudeLinked)
        {
            GlobalVarLn.lt1sp = dLatitudeOwn;
            GlobalVarLn.ln1sp = dLongitudeOwn;
            GlobalVarLn.lt2sp = dLatitudeLinked;
            GlobalVarLn.ln2sp = dLongitudeLinked;

            // Otladka

/*
            double x1 = 2382464;
            double y1 = 5498821;
            mapPlaneToGeo(GlobalVarLn.hmapl, ref x1, ref y1);
            GlobalVarLn.lt1sp = x1 * 180 / Math.PI; // rad->grad
            GlobalVarLn.ln1sp = y1 * 180 / Math.PI;

            double x3 = 2240316;
            double y4 = 5551638;
            mapPlaneToGeo(GlobalVarLn.hmapl, ref x3, ref y4);
            GlobalVarLn.lt2sp = x3 * 180 / Math.PI; // rad->grad
            GlobalVarLn.ln2sp = y4 * 180 / Math.PI;
 */

            GlobalVarLn.flagGPS_SP = 1;
            GlobalVarLn.objFormSPG.BeginInvoke(
                (MethodInvoker) (() => GlobalVarLn.objFormSPG.AcceptGPS()));
        }
        // ****************************************************************************** GPS

        // Обновление листа СП **************************************************************

        public static void UpdateDropDownList(ComboBox dropDownList)
        {
            var stations = GlobalVarLn.listJS;

            var selectedIndex = dropDownList.SelectedIndex;

            dropDownList.Items.Clear();
            dropDownList.Items.Add("X,Y");
            foreach (var station in stations)
            {
                dropDownList.Items.Add(station.Name);
            }
            dropDownList.SelectedIndex = dropDownList.Items.Count > selectedIndex ? selectedIndex : 0;
        }

        public static void UpdateDropDownList1(ComboBox dropDownList)
        {
            var stations = GlobalVarLn.listJS;

            var selectedIndex = dropDownList.SelectedIndex;

            dropDownList.Items.Clear();
            //dropDownList.Items.Add("X,Y");
            foreach (var station in stations)
            {
                dropDownList.Items.Add(station.Name);
            }
            dropDownList.SelectedIndex = dropDownList.Items.Count > selectedIndex ? selectedIndex : 0;
        }
        // ************************************************************** Обновление листа СП

        // OpenMapMenu ********************************************************************************
        // Открытие карты из меню

        private async void openMapToolStripMenuItem1_Click(object sender, EventArgs e)
        {
        // --------------------------------------------------------------------------------------------
         // Путь

         String path = "";
         String s = "";

         DialogResult result;
         result = openFileDialog1.ShowDialog();

        if (result == DialogResult.OK)
         {
             path = openFileDialog1.FileName;
             s = ClassMapRastrMenu.f_OpenMapMenu(
                                                 // Путь к карте
                                                 path,
                                                 // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                                 MapForm.RasterMapControl,
                                                 ref GlobalVarLn.MapOpened
                                                 );

         } // Dialog=OK

        // В диалоге нажать 'Отмена/Cancel'
        else if (result == DialogResult.Cancel)
         {
           return;

         } // Dialog=Cancel

        else
        {
         s = "Can't load map!";
        }
         // --------------------------------------------------------------------------------------------
        if(s!="")
           MessageBox.Show(s);
         // --------------------------------------------------------------------------------------------

         } 
        // ******************************************************************************** OpenMapMenu

        // CloseMapMenu *******************************************************************************
        // Закрыть карту и матрицу высот -> пункт меню

        private void closeMapToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            string s = "";
            s=ClassMapRastrMenu.f_CloseMapMenu(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     MapForm.RasterMapControl,
                                     ref GlobalVarLn.MapOpened,
                                     ref GlobalVarLn.MatrixOpened
                                              );

            if (s != "")
                MessageBox.Show(s);

        }
        // ******************************************************************************* CloseMapMenu

        // OpenMatrixMenu *****************************************************************************
        // Открыть матрицу высот -> пункт меню

        private void openTheHeightMatrixToolStripMenuItem_Click(object sender, EventArgs e)
        {

            // --------------------------------------------------------------------------------------------
            // Путь

            String path = "";
            String s = "";

            DialogResult result;
            result = openFileDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                path = openFileDialog1.FileName;
                s = ClassMapRastrMenu.f_OpenMatrixMenu(
                                                      path,
                                                      // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                                      MapForm.RasterMapControl,
                                                      ref GlobalVarLn.MatrixOpened
                                                      );

            } // Dialog=OK

            // В диалоге нажать 'Отмена/Cancel'
            else if (result == DialogResult.Cancel)
            {
                return;

            } // Dialog=Cancel

            else
            {
                s = "Can't load the height matrix!";
            }
            // --------------------------------------------------------------------------------------------
            if (s != "")
                MessageBox.Show(s);
            // --------------------------------------------------------------------------------------------

        }
        // OpenMatrixMenu *****************************************************************************

        // Масштаб ************************************************************************************

        //увеличить масштаб
        private void ZoomIn_Click(object sender, EventArgs e)
        {

            ClassMapRastrMenu.f_ResolutionIncrease(
                                               // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                               MapForm.RasterMapControl
                                                  );
        }

        //уменьшить масштаб
        private void ZoomOut_Click(object sender, EventArgs e)
        {
            ClassMapRastrMenu.f_ResolutionDecrease(
                                               // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                               MapForm.RasterMapControl
                                                 );

        }

        // исходный масштаб
        private void ZoomInitial_Click(object sender, EventArgs e)
        {

            ClassMapRastrMenu.f_ResolutionBase(
                                               // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                               MapForm.RasterMapControl
                                              );
        }
       // ************************************************************************************ Масштаб




       // MOUSE_DOWN *********************************************************************************
       //Обработка мыши на карте

        private void OnMapClick(object sender, MapMouseClickEventArgs e)
        {
/*
            // преобразование В меркатор
            var p = Mercator.FromLonLat(e.Location.Longitude, e.Location.Latitude);
            // преобразование из меркатора в wgs84
            var p2 = Mercator.ToLonLat(p.X, p.Y);

            if (RasterMapControl.LastClickedObject != null)
            {
                // if user clicked on a object let's remove it
                RasterMapControl.RemoveObject(RasterMapControl.LastClickedObject);
                return;
            }
            if (RasterMapControl.HoveredObject != null)
            {
                return;
            }
            if (e.ClickedButton.ChangedButton == MouseButton.Left)
            {
                ApplyInstrument(e.Location.ToPoint());
            }
*/

            // ---------------------------------------------------------------------------------
            // NEW
            // 555

            // .................................................................................
            // Geographic

            if (GlobalVarLn.TypeMap == 1)
            {
                if (e.Location.Latitude != null)
                    GlobalVarLn.LAT_Rastr = e.Location.Latitude;

                if (e.Location.Longitude != null)
                    GlobalVarLn.LONG_Rastr = e.Location.Longitude;

                if (e.Location.Altitude != null)
                    GlobalVarLn.H_Rastr = (double)e.Location.Altitude;

                if ((e.Location.Latitude != null) && (e.Location.Longitude != null))
                {
                    // преобразование В меркатор
                    var p = Mercator.FromLonLat(e.Location.Longitude, e.Location.Latitude);
                    GlobalVarLn.X_Rastr = p.X;
                    GlobalVarLn.Y_Rastr = p.Y;
                }

            } // TypeMap==1
            // .................................................................................
            // Mercator

            else
            {
                if (e.Location.Longitude != null)
                    GlobalVarLn.X_Rastr = e.Location.Longitude;

                if (e.Location.Latitude != null)
                    GlobalVarLn.Y_Rastr = e.Location.Latitude;

                if (e.Location.Altitude != null)
                    GlobalVarLn.H_Rastr = (double)e.Location.Altitude;

                var p1 = Mercator.ToLonLat(GlobalVarLn.X_Rastr, GlobalVarLn.Y_Rastr);
                GlobalVarLn.LAT_Rastr = p1.Y;
                GlobalVarLn.LONG_Rastr = p1.X;

            } // Mercator
            // .................................................................................

            // ---------------------------------------------------------------------------------
            // OLD

            GlobalVarLn.MousePress = true;

            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
            // Для маршрута

            if (GlobalVarLn.fl_Open_objFormWay == 1)
            {
                objFormWay.f_Way(
                               GlobalVarLn.X_Rastr,
                               GlobalVarLn.Y_Rastr
                              );
            }
            // SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS


            // -----------------------------------------------------------------------------------
            // SP

            if (GlobalVarLn.fl_Open_objFormSP == 1)
            {

                GlobalVarLn.fclSP = 0;

                objFormSP.f_SP(
                               GlobalVarLn.X_Rastr,
                               GlobalVarLn.Y_Rastr
                              );
            }

            // -----------------------------------------------------------------------------------
            // LF1
            if (GlobalVarLn.fl_Open_objFormLF == 1)
            {
                objFormLF.f_LF1(
                               GlobalVarLn.X_Rastr,
                               GlobalVarLn.Y_Rastr
                              );
            }
            // -----------------------------------------------------------------------------------
            // LF2
            if (GlobalVarLn.fl_Open_objFormLF2 == 1)
            {
                objFormLF2.f_LF2(
                               GlobalVarLn.X_Rastr,
                               GlobalVarLn.Y_Rastr
                              );
            }
            // -----------------------------------------------------------------------------------
            // ZO
            if (GlobalVarLn.fl_Open_objFormZO == 1)
            {
                objFormZO.f_ZO(
                               GlobalVarLn.X_Rastr,
                               GlobalVarLn.Y_Rastr
                              );
            }
            // ---------------------------------------------------------------------------------
            // OB1
            if (GlobalVarLn.fl_Open_objFormOB1 == 1)
            {
                objFormOB1.f_OB1(
                               GlobalVarLn.X_Rastr,
                               GlobalVarLn.Y_Rastr
                              );
            }
            // ---------------------------------------------------------------------------------
            // OB2
            if (GlobalVarLn.fl_Open_objFormOB2 == 1)
            {
                objFormOB2.f_OB2(
                               GlobalVarLn.X_Rastr,
                               GlobalVarLn.Y_Rastr
                              );
            }
            // ---------------------------------------------------------------------------------
            // Az1(Object)

            if (GlobalVarLn.blOB_az1 == true)
            {

                GlobalVarLn.XXXaz = GlobalVarLn.X_Rastr;
                GlobalVarLn.YYYaz = GlobalVarLn.Y_Rastr;

            }
            // ---------------------------------------------------------------------------------
            // SPFBRR

            if (GlobalVarLn.fl_Open_objFormSPFB == 1)
            {
                objFormSPFB.f_SPFB_f1();
            }
            // ---------------------------------------------------------------------------------
            // SPFBPR

            if (GlobalVarLn.fl_Open_objFormSPFB1 == 1)
            {
                objFormSPFB1.f_SPFB_f2();
            }
            // ---------------------------------------------------------------------------------


        } // P/P
          // ********************************************************************************* MOUSE_DOWN

        // MOUSE_MOVE ***********************************************************************************
        // MouseMove

        private void OnMapMapMouseMove(object sender, Location location)
        {

/*
!!! Ne ybirat
 
            // преобразование В меркатор
            // var p = Mercator.FromLonLat(e.Location.Longitude, e.Location.Latitude);
            // преобразование из меркатора в wgs84
            //var p2 = Mercator.ToLonLat(p.X, p.Y);

            double moveX = 0;
            double moveY = 0;

            if ((location.Latitude != null) && (location.Longitude != null))
            {
            //moveX = location.Latitude;
            //moveY = location.Longitude;
            //lLat.Text = string.Format("Latitude = {0}", moveX.ToString("0.000"));
            //lLon.Text = string.Format("Longitude = {0}", moveY.ToString("0.000"));
            }

            Peleng peleng = new Peleng("");
            int iDegreeLat=0;
            int iMinuteLat=0;
            double dSecondLat = 0;
            int iDegreeLong = 0;
            int iMinuteLong = 0;
            double dSecondLong = 0;
            int altitudeSuffix;

            if ((location.Latitude != null) && (location.Longitude != null))
            {
            // преобразование В меркатор
            var p = Mercator.FromLonLat(location.Longitude, location.Latitude);
            moveX = p.X;
            moveY = p.Y;
            //lX.Text = string.Format("X, m = {0}", Convert.ToString((int)moveX));
            //lY.Text = string.Format("Y, m = {0}", Convert.ToString((int)moveY));

            peleng.f_Grad_GMS(location.Latitude, ref iDegreeLat, ref iMinuteLat, ref dSecondLat);
            lLat.Text = string.Format("Latitude: {0}", iDegreeLat.ToString("00") + '\xB0' + " " + iMinuteLat.ToString("00") + "' " + dSecondLat.ToString("00") + "''" + " N");
            peleng.f_Grad_GMS(location.Longitude, ref iDegreeLong, ref iMinuteLong, ref dSecondLong);
            lLon.Text = string.Format("    Longitude: {0}", iDegreeLong.ToString("00") + '\xB0' + " " + iMinuteLong.ToString("00") + "' " + dSecondLong.ToString("00") + "''" + "  E");

            //var ppp1 = location.ToMgrs();

            }

            if (location.Altitude!=null)
            {
            //moveX = (int)location.Altitude;
            //lH.Text = string.Format("H, m = {0}", Convert.ToString((int)moveX));

            altitudeSuffix = (int)location.Altitude;
            lH.Text = string.Format("    Altitude: {0}", altitudeSuffix);
            lX.Text = string.Format("    MGRS: {0}", location.ToMgrs().ToLongString());

            }
*/


// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// 555

            double moveX = 0;
            double moveY = 0;

            Peleng peleng = new Peleng("");
            int iDegreeLat = 0;
            int iMinuteLat = 0;
            double dSecondLat = 0;
            int iDegreeLong = 0;
            int iMinuteLong = 0;
            double dSecondLong = 0;
            int altitudeSuffix;
            double lt = 0;
            double lng = 0;

            // .............................................................................
            // H

            if (location.Altitude != null)
            {
                altitudeSuffix = (int)location.Altitude;
                lH.Text = string.Format("    Altitude: {0}", altitudeSuffix);
            }
            // .............................................................................
            //  Geographic

            if (GlobalVarLn.TypeMap == 1)
            {
                if ((location.Latitude != null) && (location.Longitude != null))
                {
                    // преобразование В меркатор
                    var p = Mercator.FromLonLat(location.Longitude, location.Latitude);
                    moveX = p.X;
                    moveY = p.Y;

                    peleng.f_Grad_GMS(location.Latitude, ref iDegreeLat, ref iMinuteLat, ref dSecondLat);
                    lLat.Text = string.Format("Latitude: {0}", iDegreeLat.ToString("00") + '\xB0' + " " + iMinuteLat.ToString("00") + "' " + dSecondLat.ToString("00") + "''" + " N");
                    peleng.f_Grad_GMS(location.Longitude, ref iDegreeLong, ref iMinuteLong, ref dSecondLong);
                    lLon.Text = string.Format("    Longitude: {0}", iDegreeLong.ToString("00") + '\xB0' + " " + iMinuteLong.ToString("00") + "' " + dSecondLong.ToString("00") + "''" + "  E");

                    lX.Text = string.Format("    MGRS: {0}", location.ToMgrs().ToLongString());

                }

            } // Geographic
            // .............................................................................
            // Mercator

            else
            {
                if (location.Longitude != null)
                    moveX = location.Longitude;
                if (location.Latitude != null)
                    moveY = location.Latitude;

                var p1 = Mercator.ToLonLat(moveX, moveY);
                lt = p1.Y;
                lng = p1.X;

                peleng.f_Grad_GMS(lt, ref iDegreeLat, ref iMinuteLat, ref dSecondLat);
                lLat.Text = string.Format("Latitude: {0}", iDegreeLat.ToString("00") + '\xB0' + " " + iMinuteLat.ToString("00") + "' " + dSecondLat.ToString("00") + "''" + " N");
                peleng.f_Grad_GMS(lng, ref iDegreeLong, ref iMinuteLong, ref dSecondLong);
                lLon.Text = string.Format("    Longitude: {0}", iDegreeLong.ToString("00") + '\xB0' + " " + iMinuteLong.ToString("00") + "' " + dSecondLong.ToString("00") + "''" + "  E");

                string ss = ClassGeoCalculator.f_WGS84_MGRS(lt, lng);
                lX.Text = "MGRS: " + ss;

            }
            // .............................................................................


// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


            // SPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFB

            String strLine2 = "";
            String strLine3 = "";

            int iaz = 0;
            double X_Coordl3_1 = 0;
            double Y_Coordl3_1 = 0;
            double X_Coordl3_2 = 0;
            double Y_Coordl3_2 = 0;
            double DXX3 = 0;
            double DYY3 = 0;
            double azz = 0;
            long ichislo = 0;
            double dchislo = 0;

            ClassMap objClassMap10 = new ClassMap();

            if ((GlobalVarLn.objFormSPFBG.chbXY.Checked == true) &&
                (GlobalVarLn.flF_f1 == 1)

            )
            {
                strLine2 = Convert.ToString(
                    GlobalVarLn.objFormSPFBG.cbChooseSC.Items[
                        GlobalVarLn.objFormSPFBG.cbChooseSC.SelectedIndex]); // Numb

                // if2
                if (!string.IsNullOrEmpty(strLine2))
                {
                    var stations = GlobalVarLn.GetListJSWithCurrentPosition();

                    for (iaz = 0; iaz < stations.Count; iaz++)
                    {

                        X_Coordl3_1 = stations[iaz].CurrentPosition.x;
                        Y_Coordl3_1 = stations[iaz].CurrentPosition.y;
                        X_Coordl3_2 = moveX;
                        Y_Coordl3_2 = moveY;

                        strLine3 = stations[iaz].Name;

                        // if1
                        if (String.Compare(strLine2, strLine3) == 0)
                        {
                            // Разность координат для расчета азимута
                            // На карте X - вверх
                            //0219 !!!
                            //DXX3 = Y_Coordl3_2 - Y_Coordl3_1;
                            //DYY3 = X_Coordl3_2 - X_Coordl3_1;
                            DXX3 = X_Coordl3_2 - X_Coordl3_1;
                            DYY3 = Y_Coordl3_2 - Y_Coordl3_1;

                            // grad
                            azz = objClassMap10.f_Def_Azimuth(
                                DYY3,
                                DXX3
                            );

                            GlobalVarLn.Az_f1 = azz;

                            ichislo = (long)(azz * 100);
                            dchislo = ((double)ichislo) / 100;
                            lB.Text = string.Format("B,grad  = {0}", Convert.ToString((int)azz));

                            // Убрать с карты
                            // WORK
                            //MapForm.REDRAW_MAP();
                            // CLASS
                            ClassMapRastrReDrawAll.REDRAW_MAP_CLASS();

                            GlobalVarLn.objFormSPFBG.f_LuchReDraw(X_Coordl3_1, Y_Coordl3_1, X_Coordl3_2,
                                Y_Coordl3_2);

                        } // if1

                    } // FOR

                }
            }

            // seg
            if ((GlobalVarLn.objFormSPFBG.chbXY.Checked == false) && (GlobalVarLn.flF_f1 == 1))
            {
                lB.Text = "";
            }
            // SPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFBSPFB

            // SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1

            if ((GlobalVarLn.objFormSPFB1G.chbXY.Checked == true) &&
                (GlobalVarLn.flF_f2 == 1)

            )
            {
                strLine2 = Convert.ToString(
                    GlobalVarLn.objFormSPFB1G.cbChooseSC.Items[
                        GlobalVarLn.objFormSPFB1G.cbChooseSC.SelectedIndex]); // Numb

                // if2
                if (!string.IsNullOrEmpty(strLine2))
                {
                    var stations = GlobalVarLn.GetListJSWithCurrentPosition();

                    for (iaz = 0; iaz < stations.Count; iaz++)
                    {

                        X_Coordl3_1 = stations[iaz].CurrentPosition.x;
                        Y_Coordl3_1 = stations[iaz].CurrentPosition.y;
                        X_Coordl3_2 = moveX;
                        Y_Coordl3_2 = moveY;

                        strLine3 = stations[iaz].Name;

                        // if1
                        if (String.Compare(strLine2, strLine3) == 0)
                        {
                            // Разность координат для расчета азимута
                            // На карте X - вверх
                            //0219 !!!
                            //DXX3 = Y_Coordl3_2 - Y_Coordl3_1;
                            //DYY3 = X_Coordl3_2 - X_Coordl3_1;
                            DXX3 = X_Coordl3_2 - X_Coordl3_1;
                            DYY3 = Y_Coordl3_2 - Y_Coordl3_1;

                            // grad
                            azz = objClassMap10.f_Def_Azimuth(
                                DYY3,
                                DXX3
                            );

                            GlobalVarLn.Az_f2 = azz;

                            ichislo = (long)(azz * 100);
                            dchislo = ((double)ichislo) / 100;
                            lB.Text = string.Format("B,grad  = {0}", Convert.ToString((int)azz)); // seg

                            // Убрать с карты
                            //GlobalVarLn.axMapScreenGlobal.Repaint();
                            //GlobalVarLn.objFormAzG.f_Map_Line_XY1(X_Coordl3_1, Y_Coordl3_1, X_Coordl3_2,
                            //    Y_Coordl3_2);

                            // WORK
                            //MapForm.REDRAW_MAP();
                            // CLASS
                            ClassMapRastrReDrawAll.REDRAW_MAP_CLASS();

                            GlobalVarLn.objFormSPFBG.f_LuchReDraw(X_Coordl3_1, Y_Coordl3_1, X_Coordl3_2,
                                Y_Coordl3_2);


                        } // if1

                    } // FOR

                }
            }

            // seg
            if ((GlobalVarLn.objFormSPFB1G.chbXY.Checked == false) && (GlobalVarLn.flF_f2 == 1))
            {
                lB.Text = "";
            }

            // SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1SPFB1

        } // P/P
        // *********************************************************************************** MOUSE_MOVE

        // MOUSE_UP *************************************************************************************
        // MouseUp 

        // ************************************************************************************* MOUSE_UP

        // Загрузка головной формы **********************************************************************

        private void MapForm_Load(object sender, EventArgs e)
        {
            // ------------------------------------------------------------------------------------------
            // Объекты форм для использования в других формах (Инициализируются)

            GlobalVarLn.objFormSPG = objFormSP;
            GlobalVarLn.objFormLFG = objFormLF;
            GlobalVarLn.objFormLF2G = objFormLF2;
            GlobalVarLn.objFormZOG = objFormZO;
            GlobalVarLn.objFormOB1G = objFormOB1;
            GlobalVarLn.objFormOB2G = objFormOB2;
            GlobalVarLn.objFormSuppressionG = objFormSuppression;
            GlobalVarLn.objFormSPFBG = objFormSPFB;
            GlobalVarLn.objFormSPFB1G = objFormSPFB1;
            GlobalVarLn.objFormSPFB2G = objFormSPFB2;
            GlobalVarLn.objFormAz1G = objFormAz1;
            GlobalVarLn.objFormLineSightRangeG = objFormLineSightRange;
            GlobalVarLn.objFormWayG = objFormWay;
            GlobalVarLn.objFormSupSpufG = objFormSupSpuf;
            GlobalVarLn.objFormSpufG = objFormSpuf;
            GlobalVarLn.objFormScreenG = objFormScreen;
            GlobalVarLn.objFormTROG = objFormTRO;
            GlobalVarLn.objFormTabG = objFormTab;
            // ------------------------------------------------------------------------------------------
            GlobalVarLn.LoadListJS();
            GlobalVarLn.flEndSP_stat = 1;
            // ------------------------------------------------------------------------------------------
            // Значки

            String nn;
            String nn1;
            String s;
            long iz;
            // ...........................................................................................
            // OB1

            GlobalVarLn.objFormOB1G.imageList1.Images.Clear();
            try
            {
                s = Application.StartupPath + "\\Images\\OwnObject\\";
                DirectoryInfo di = new DirectoryInfo(@s);

                iz = 0;
                foreach (var fi in di.GetFiles("*.png"))
                {
                    nn = fi.Name;
                    nn1 = fi.DirectoryName + "\\" + fi.Name;
                    GlobalVarLn.objFormOB1G.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;
                }
                foreach (var fi_1 in di.GetFiles("*.bmp"))
                {
                    nn = fi_1.Name;
                    nn1 = fi_1.DirectoryName + "\\" + fi_1.Name;
                    GlobalVarLn.objFormOB1G.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;
                }
                foreach (var fi_1 in di.GetFiles("*.jpeg"))
                {
                    nn = fi_1.Name;
                    nn1 = fi_1.DirectoryName + "\\" + fi_1.Name;
                    GlobalVarLn.objFormOB1G.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;
                }

                if (iz == 0) MessageBox.Show("No OwnObject signs");
            }
            catch
            {
                MessageBox.Show("No memory");
            }
            // ...........................................................................................
            // OB2

            GlobalVarLn.objFormOB2G.imageList1.Images.Clear();
            try
            {
                s = Application.StartupPath + "\\Images\\EnemyObject\\";
                DirectoryInfo di1 = new DirectoryInfo(@s);

                iz = 0;
                foreach (var fi1 in di1.GetFiles("*.png"))
                {
                    nn = fi1.Name;
                    nn1 = fi1.DirectoryName + "\\" + fi1.Name;
                    GlobalVarLn.objFormOB2G.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;
                }
                foreach (var fi1_1 in di1.GetFiles("*.bmp"))
                {
                    nn = fi1_1.Name;
                    nn1 = fi1_1.DirectoryName + "\\" + fi1_1.Name;
                    GlobalVarLn.objFormOB2G.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;
                }
                foreach (var fi1_1 in di1.GetFiles("*.jpeg"))
                {
                    nn = fi1_1.Name;
                    nn1 = fi1_1.DirectoryName + "\\" + fi1_1.Name;
                    GlobalVarLn.objFormOB2G.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;
                }

                if (iz == 0) MessageBox.Show("No EnemyObject signs");
            }
            catch
            {
                MessageBox.Show("No memory");
            }
            // ...........................................................................................
            // SP

            GlobalVarLn.objFormSPG.imageList1.Images.Clear();
            try
            {
                s = Application.StartupPath + "\\Images\\Jammer\\";
                DirectoryInfo di2 = new DirectoryInfo(@s);

                iz = 0;
                foreach (var fi2 in di2.GetFiles("*.png"))
                {
                    nn = fi2.Name;
                    nn1 = fi2.DirectoryName + "\\" + fi2.Name;
                    GlobalVarLn.objFormSPG.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;
                }
                foreach (var fi2_1 in di2.GetFiles("*.bmp"))
                {
                    nn = fi2_1.Name;
                    nn1 = fi2_1.DirectoryName + "\\" + fi2_1.Name;
                    GlobalVarLn.objFormSPG.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;
                }
                foreach (var fi2_1 in di2.GetFiles("*.jpeg"))
                {
                    nn = fi2_1.Name;
                    nn1 = fi2_1.DirectoryName + "\\" + fi2_1.Name;
                    GlobalVarLn.objFormSPG.imageList1.Images.Add(Image.FromFile(nn1));
                    iz += 1;
                }

                if (iz == 0) MessageBox.Show("No Jammer signs");
            }
            catch
            {
                MessageBox.Show("No memory");
            }
            // ...........................................................................................
            // SPV

            GlobalVarLn.objFormSPG.imageList1V.Images.Clear();
            try
            {
                s = Application.StartupPath + "\\Images\\JammerPlanned\\";
                DirectoryInfo di3 = new DirectoryInfo(@s);

                iz = 0;
                foreach (var fi3 in di3.GetFiles("*.png"))
                {
                    nn = fi3.Name;
                    nn1 = fi3.DirectoryName + "\\" + fi3.Name;
                    GlobalVarLn.objFormSPG.imageList1V.Images.Add(Image.FromFile(nn1));
                    iz += 1;
                }
                foreach (var fi3_1 in di3.GetFiles("*.bmp"))
                {
                    nn = fi3_1.Name;
                    nn1 = fi3_1.DirectoryName + "\\" + fi3_1.Name;
                    GlobalVarLn.objFormSPG.imageList1V.Images.Add(Image.FromFile(nn1));
                    iz += 1;
                }
                foreach (var fi3_1 in di3.GetFiles("*.jpeg"))
                {
                    nn = fi3_1.Name;
                    nn1 = fi3_1.DirectoryName + "\\" + fi3_1.Name;
                    GlobalVarLn.objFormSPG.imageList1V.Images.Add(Image.FromFile(nn1));
                    iz += 1;
                }

                if (iz == 0) MessageBox.Show("No Jammer(planned) signs");
            }
            catch
            {
                MessageBox.Show("No memory");
            }
            // ...........................................................................................

            // -------------------------------------------------------------------------------------------
            // Очистка dataGridView1+ Установка 100 строк

            // Очистка dataGridView1
            while (objFormOB2.dataGridView1.Rows.Count != 0)
                objFormOB2.dataGridView1.Rows.Remove(objFormOB2.dataGridView1.Rows[objFormOB2.dataGridView1.Rows.Count - 1]);

            for (int i = 0; i < GlobalVarLn.sizeDatOB2_stat; i++)
            {
                objFormOB2.dataGridView1.Rows.Add("", "", "", "", "");
            }
            // -------------------------------------------------------------------------------------------
            // Очистка dataGridView1+ Установка 100 строк

            // Очистка dataGridView1
            while (objFormOB1.dataGridView1.Rows.Count != 0)
                objFormOB1.dataGridView1.Rows.Remove(objFormOB1.dataGridView1.Rows[objFormOB1.dataGridView1.Rows.Count - 1]);

            for (int iyu = 0; iyu < GlobalVarLn.sizeDatOB1_stat; iyu++)
            {
                objFormOB1.dataGridView1.Rows.Add("", "", "", "");
            }
            // -------------------------------------------------------------------------------------------
            ClassMapRastrReDraw.f_ReDraw_SP(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     MapForm.RasterMapControl
                                           );
            // -------------------------------------------------------------------------------------------
            // 555
            // Тип карты (географический/Меркатор)

            GlobalVarLn.TypeMap = iniRW.get_TypeMap();
            // -------------------------------------------------------------------------------------------


/*
            CultureInfo current = System.Threading.Thread.CurrentThread.CurrentUICulture;
            if (current.TwoLetterISOLanguageName != "fr")
            {
                CultureInfo newCulture = CultureInfo.CreateSpecificCulture("en-US");
                System.Threading.Thread.CurrentThread.CurrentUICulture = newCulture;
                // Make current UI culture consistent with current culture.
                System.Threading.Thread.CurrentThread.CurrentCulture = newCulture;
            }
*/




        }
        // ********************************************************************** Загрузка головной формы

        // Обработчик кнопки Button4: save screen *******************************************************

        private void button4_Click(object sender, EventArgs e)
        {
            // -----------------------------------------------------------------------------------------

            String s = ClassMapRastrButton.f_SaveScreen(
                                               // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                               MapForm.RasterMapControl
                                                       );
            if(s!="")
                MessageBox.Show(s);
            // -------------------------------------------------------------------------------------

        }
        // ************************************************** Обработчик кнопки Button4: save screen

        // Обработчик кнопки Button5: Tab **********************************************************

        private void button5_Click(object sender, EventArgs e)
        {
         // ----------------------------------------------------------------------------------------
           GlobalVarLn.fl_Open_objFormTab = 1;

           // Закрыть все другие формы
           ClassMapRastrDopFunctions.F_Close_Form(17);
         // ----------------------------------------------------------------------------------------
            if (objFormTab == null || objFormTab.IsDisposed)
            {
                // ................................................................................
                // Очистка dataGridView

                objFormTab.dgvTab.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeTab; i++)
                {
                    objFormTab.dgvTab.Rows.Add("", "", "", "", "", "");
                }
                GlobalVarLn.fllTab = 0;
                GlobalVarLn.list_air_tab.Clear();
                // ................................................................................

                objFormTab = new FormTab(this);
                GlobalVarLn.objFormTabG = objFormTab;
                objFormTab.Show();
            }
         // ----------------------------------------------------------------------------------------
            else
            {
                objFormTab.Show();
            }
         // ----------------------------------------------------------------------------------------

        }
        // ********************************************************** Обработчик кнопки Button5: Tab

        // Обработчик кнопки Button6: Маршрут ******************************************************

        private void button6_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormWay = 1;
            ClassMapRastrDopFunctions.F_Close_Form(8);
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormWay == null || objFormWay.IsDisposed)
            {
                objFormWay = new FormWay();
                GlobalVarLn.objFormWayG = objFormWay;
                objFormWay.Show();

                ClassMapRastrClear.f_Clear_FormWay();
            }
            // -------------------------------------------------------------------------------------
            // НЕ 1-я загрузка формы

            else
            {
                objFormWay.Show();

                objFormWay.chbWay.Checked = true;
                if (objFormWay.chbWay.Checked == true)
                {
                    GlobalVarLn.blWay_stat = true;
                    GlobalVarLn.flEndWay_stat = 1;
                }

            }
            // -------------------------------------------------------------------------------------

        }
        // ****************************************************** Обработчик кнопки Button6: Маршрут

        // Обработчик кнопки Button7: Зона подавления навигации ************************************

        private void button7_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSupSpuf = 1;
            ClassMapRastrDopFunctions.F_Close_Form(12);
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormSupSpuf == null || objFormSupSpuf.IsDisposed)
            {
                objFormSupSpuf = new FormSupSpuf();
                GlobalVarLn.objFormSupSpufG = objFormSupSpuf;
                objFormSupSpuf.Show();

                ClassMapRastrClear.f_Clear_FormSupSpuf();
            }
            // -------------------------------------------------------------------------------------
            // не 1-я загрузка формы

            else
            {
                objFormSupSpuf.Show();
            }
            // -------------------------------------------------------------------------------------

        }
        // ************************************ Обработчик кнопки Button7: Зона подавления навигации

        // Обработчик кнопки Button8: Зона спуфинга ************************************************

        private void button8_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSpuf = 1;
            ClassMapRastrDopFunctions.F_Close_Form(13);
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormSpuf == null || objFormSpuf.IsDisposed)
            {
                objFormSpuf = new FormSpuf();
                GlobalVarLn.objFormSpufG = objFormSpuf;
                objFormSpuf.Show();

                ClassMapRastrClear.f_Clear_FormSpuf();
            }
            // -------------------------------------------------------------------------------------
            // не 1-я загрузка формы

            else
            {
                objFormSpuf.Show();
            }
            // -------------------------------------------------------------------------------------

        }
        // ************************************************ Обработчик кнопки Button8: Зона спуфинга

        // Обработчик кнопки Button10: ЗПВ *********************************************************

        private void button10_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormLineSightRange = 1;
            ClassMapRastrDopFunctions.F_Close_Form(10);
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormLineSightRange == null || objFormLineSightRange.IsDisposed)
            {
                //GlobalVarLn.objFormLineSightRangeG.WindowState = FormWindowState.Normal;

                objFormLineSightRange = new FormLineSightRange();
                GlobalVarLn.objFormLineSightRangeG = objFormLineSightRange;
                objFormLineSightRange.Show();

                ClassMapRastrClear.f_Clear_FormLineSightRange1();
                ClassMapRastrClear.f_Clear_FormLineSightRange2();
            }
            // -------------------------------------------------------------------------------------
            // не 1-я загрузка формы

            else
            {
                objFormLineSightRange.Show();
            }
            // -------------------------------------------------------------------------------------

        }
        // ********************************************************* Обработчик кнопки Button10: ЗПВ

        // Обработчик кнопки Button11:SP ***********************************************************

        private void button11_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSP = 1;
            ClassMapRastrDopFunctions.F_Close_Form(1);

            GlobalVarLn.blSP_stat = true;
            GlobalVarLn.flEndSP_stat = 1;
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormSP == null || objFormSP.IsDisposed)
            {
                objFormSP = new FormSP();
                GlobalVarLn.objFormSPG = objFormSP;
                objFormSP.Show();

                if (GlobalVarLn.flEndTRO_stat != 1)
                {
                    GlobalVarLn.blSP_stat = true;
                    GlobalVarLn.flEndSP_stat = 1;
                    GlobalVarLn.XCenter_SP = 0;
                    GlobalVarLn.YCenter_SP = 0;
                    GlobalVarLn.HCenter_SP = 0;
                    GlobalVarLn.flCoord_SP2 = 0;

                    GlobalVarLn.ClearListJS();

                    GlobalVarLn.iZSP = 0;
                    GlobalVarLn.objFormSPG.pbSP.BackgroundImage = imageList1.Images[0];
                }
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormSP.Show();
            }
            // -------------------------------------------------------------------------------------

        }
        // *********************************************************** Обработчик кнопки Button11:SP

        // Button13: LF ****************************************************************************

        private void button13_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormLF = 1;
            ClassMapRastrDopFunctions.F_Close_Form(4);

            GlobalVarLn.blLF1_stat = true;
            GlobalVarLn.flEndLF1_stat = 1;
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormLF == null || objFormLF.IsDisposed)
            {

                objFormLF = new FormLF();
                GlobalVarLn.objFormLFG = objFormLF;
                objFormLF.Show();

                ClassMapRastrClear.f_Clear_FormLF();
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormLF.Show();
            }
            // -------------------------------------------------------------------------------------

        }
        // **************************************************************************** Button13: LF

        // Button 14: LF2 **************************************************************************

        private void button14_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormLF2 = 1;
            ClassMapRastrDopFunctions.F_Close_Form(5);

            GlobalVarLn.blLF2_stat = true;
            GlobalVarLn.flEndLF2_stat = 1;
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormLF2 == null || objFormLF2.IsDisposed)
            {
                objFormLF2 = new FormLF2();
                GlobalVarLn.objFormLF2G = objFormLF2;
                objFormLF2.Show();

                ClassMapRastrClear.f_Clear_FormLF2();
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormLF2.Show();
            }

        }
        // ************************************************************************** Button 14: LF2

        // Button12: ZO ****************************************************************************

        private void button12_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormZO = 1;
            ClassMapRastrDopFunctions.F_Close_Form(6);

            GlobalVarLn.blZO_stat = true;
            GlobalVarLn.flEndZO_stat = 1;
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormZO == null || objFormZO.IsDisposed)
            {

                objFormZO = new FormZO();
                GlobalVarLn.objFormZOG = objFormZO;
                objFormZO.Show();

                ClassMapRastrClear.f_Clear_FormZO();
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormZO.Show();
            }
            // -------------------------------------------------------------------------------------

        }
        // **************************************************************************** Button12: ZO

        // Button1: TRO ****************************************************************************

        private void button1_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormTRO = 1;
            ClassMapRastrDopFunctions.F_Close_Form(7);

            GlobalVarLn.blTRO_stat = true;
            GlobalVarLn.flEndTRO_stat = 1;
            GlobalVarLn.blSP_stat = true;
            GlobalVarLn.flEndSP_stat = 1;
            GlobalVarLn.blLF1_stat = true;
            GlobalVarLn.flEndLF1_stat = 1;
            GlobalVarLn.blLF2_stat = true;
            GlobalVarLn.flEndLF2_stat = 1;
            GlobalVarLn.blZO_stat = true;
            GlobalVarLn.flEndZO_stat = 1;
            GlobalVarLn.blOB1_stat = true;
            GlobalVarLn.flEndOB1_stat = 1;
            GlobalVarLn.blOB2_stat = true;
            GlobalVarLn.flEndOB2_stat = 1;
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormTRO == null || objFormTRO.IsDisposed)
            {
                objFormTRO = new FormTRO();
                GlobalVarLn.objFormTROG = objFormTRO;
                objFormTRO.Show();

                ClassMapRastrClear.f_Clear_FormTRO();
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormTRO.Show();
            }

        }
        // **************************************************************************** Button1: TRO

        // Обработчик кнопки Button9: OB1 **********************************************************

        private void button9_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormOB1 = 1;
            ClassMapRastrDopFunctions.F_Close_Form(2);

            GlobalVarLn.blOB1_stat = true;
            GlobalVarLn.flEndOB1_stat = 1;
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormOB1 == null || objFormOB1.IsDisposed)
            {

                objFormOB1 = new FormOB1();
                GlobalVarLn.objFormOB1G = objFormOB1;
                objFormOB1.Show();

                ClassMapRastrClear.f_Clear_FormOB1();
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormOB1.Show();
            }
            // -------------------------------------------------------------------------------------

        }
        // ********************************************************** Обработчик кнопки Button9: OB1

        // Обработчик кнопки Button15: OB2 *********************************************************

        private void button15_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormOB2 = 1;
            ClassMapRastrDopFunctions.F_Close_Form(3);

            GlobalVarLn.blOB2_stat = true;
            GlobalVarLn.flEndOB2_stat = 1;
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormOB2 == null || objFormOB2.IsDisposed)
            {

                objFormOB2 = new FormOB2();
                GlobalVarLn.objFormOB2G = objFormOB2;
                objFormOB2.Show();

                ClassMapRastrClear.f_Clear_FormOB2();
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormOB2.Show();
            }
            // -------------------------------------------------------------------------------------
        }
        // ********************************************************* Обработчик кнопки Button15: OB2

        // Обработчик кнопки : Зона подавления радиолиний управления *******************************

        private void bZoneSuppressAvia_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSuppression = 1;
            ClassMapRastrDopFunctions.F_Close_Form(11);
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormSuppression == null || objFormSuppression.IsDisposed)
            {
                objFormSuppression = new FormSuppression();
                GlobalVarLn.objFormSuppressionG = objFormSuppression;
                objFormSuppression.Show();

                ClassMapRastrClear.f_Clear_FormSuppression();
            }
            else
            {
                objFormSuppression.Show();
            }

        }
        // ******************************* Обработчик кнопки : Зона подавления радиолиний управления

        // Button16 :Диапазоны частот и секторов радиоразведки *************************************

        private void button16_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSPFB = 1;
            ClassMapRastrDopFunctions.F_Close_Form(14);
            GlobalVarLn.flF_f1 = 1;
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormSPFB == null || objFormSPFB.IsDisposed)
            {
                objFormSPFB = new FormSPFB();
                GlobalVarLn.objFormSPFBG = objFormSPFB;
                objFormSPFB.Show();

                ClassMapRastrClear.f_Clear_FormSPFB();
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormSPFB.Show();
            }
            // -------------------------------------------------------------------------------------
        }
        // ************************************* Button16 :Диапазоны частот и секторов радиоразведки

        // Button17 :Диапазоны частот и секторов РП ************************************************

        private void button17_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSPFB1 = 1;
            ClassMapRastrDopFunctions.F_Close_Form(15);
            GlobalVarLn.flF_f2 = 1;
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormSPFB1 == null || objFormSPFB1.IsDisposed)
            {

                objFormSPFB1 = new FormSPFB1();
                GlobalVarLn.objFormSPFB1G = objFormSPFB1;
                objFormSPFB1.Show();

                ClassMapRastrClear.f_Clear_FormSPFB1();
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormSPFB1.Show();
            }
            // -------------------------------------------------------------------------------------
        }
        // ************************************************ Button17 :Диапазоны частот и секторов РП

        // Button18: запрещенные частоты ***********************************************************

        private void button18_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormSPFB2 = 1;
            ClassMapRastrDopFunctions.F_Close_Form(16);

            GlobalVarLn.flF_f3 = 1;
            GlobalVarLn.flF1_f3 = 1;
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormSPFB2 == null || objFormSPFB2.IsDisposed)
            {
                objFormSPFB2 = new FormSPFB2(this);
                GlobalVarLn.objFormSPFB2G = objFormSPFB2;
                objFormSPFB2.Show();

                ClassMapRastrClear.f_Clear_FormSPFB2_1();
                ClassMapRastrClear.f_Clear_FormSPFB2_2();
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormSPFB2.Show();
            }
        }
        // *********************************************************** Button18: запрещенные частоты

        // Обработчик кнопки : Azimut1 *************************************************************

        private void button19_Click(object sender, EventArgs e)
        {
            // -------------------------------------------------------------------------------------
            GlobalVarLn.fl_Open_objFormAz1 = 1;
            ClassMapRastrDopFunctions.F_Close_Form(9);

            GlobalVarLn.blOB_az1 = true;
            GlobalVarLn.flEndOB_az1 = 1;
            // -------------------------------------------------------------------------------------
            // 1-я загрузка формы

            if (objFormAz1 == null || objFormAz1.IsDisposed)
            {
                objFormAz1 = new FormAz1();
                GlobalVarLn.objFormAz1G = objFormAz1;
                objFormAz1.Show();

                ClassMapRastrClear.f_Clear_FormAz1();
            }
            // -------------------------------------------------------------------------------------
            else
            {
                objFormAz1.Show();
            }
            // -------------------------------------------------------------------------------------

        }
        // ************************************************************* Обработчик кнопки : Azimut1


        // CONNECTION ******************************************************************************

        // -----------------------------------------------------------------------------------------
        private void bConnectPC_Click(object sender, EventArgs e)
        {
            bool blConnect = false;

            if (bConnectPC.BackColor == System.Drawing.Color.Red)
            {
                blConnect = true;
                GlobalVarLn.flllsp = 1;
            
            }

            InitConnectionPC(blConnect);
        }
        // -----------------------------------------------------------------------------------------
        // INIT CONNECTION

        private void InitConnectionPC(bool blConnect)
        {
            String s1;
            int nn;

            if (blConnect)
            {
                if (clientPC != null)
                    clientPC = null;

                // sob
                //clientPC = new ClientPC(3);
                clientPC = new ClientPC((byte)GlobalVarLn.AdressOwn);

                // GPS
                // sob
                clientPC.OnConfirmCoord += clientPC_OnConfirmCoord;
                GlobalVarLn.clientPCG = clientPC;

                // connect
                clientPC.OnConnect += new ClientPC.ConnectEventHandler(ShowConnectPC);

                // disconnect 
                clientPC.OnDisconnect += new ClientPC.ConnectEventHandler(ShowDisconnectPC);

                // sob
                clientPC.OnConfirmReconFWS += new ClientPC.ConfirmReconFWSEventHandler(clientPC_OnConfirmReconFWS);

                s1 = tbIP.Text;
                nn = Convert.ToInt32(tbPort.Text);
                //clientPC.Connect("127.0.0.1", 9101);
                clientPC.Connect(s1, nn);

            }
            else
            {
                clientPC.Disconnect();

                clientPC = null;
                GlobalVarLn.flllsp = 0;

            }
        }
        // -----------------------------------------------------------------------------------------

        private void clientPC_OnConfirmReconFWS(object sender, byte bAddress, byte bCodeError, TReconFWS[] tReconFWS)
        {
            if (UpdateTableReconFWS != null)
                UpdateTableReconFWS(tReconFWS);
        }
        // -----------------------------------------------------------------------------------------
        /// <summary>
        /// если подключено -- зеленая лампочка
        /// </summary>
        /// <param name="sender"></param>
        /// 
        private void ShowConnectPC(object sender)
        {
            if (bConnectPC.InvokeRequired)
            {
                //0206
                bConnectPC.Invoke((MethodInvoker)(delegate()
                {
                    bConnectPC.BackColor = System.Drawing.Color.Green;

                }));
 
            }
            else
            {
                ;
                //0206
                bConnectPC.BackColor = System.Drawing.Color.Green;
            }
        }
        // -----------------------------------------------------------------------------------------
        /// <summary>
        /// если не подключено -- краскная лампочка
        /// </summary>
        /// <param name="sender"></param>
        /// 
        private void ShowDisconnectPC(object sender)
        {
            if (bConnectPC.InvokeRequired)
            {
                ;
                //0206
/*
                bConnectPC.Invoke((MethodInvoker)(delegate()
                {
                    bConnectPC.BackColor = Color.Red;
                }));
 */
            }
            else
            {
                //0206
                bConnectPC.BackColor = System.Drawing.Color.Red;
            }
        }
        // -----------------------------------------------------------------------------------------

        // ****************************************************************************** CONNECTION

        // CLOSE_MapForm ***************************************************************************
        private void MapForm_FormClosing(object sender, FormClosingEventArgs e)
        {

/*
            if (MapIsOpenned == true)
            {
                RasterMapControl.CloseMap();
            }
            if (HeightMatrixIsOpenned == true)
            {
                RasterMapControl.CloseDted();
                HeightMatrixIsOpenned = false;
            }
            iniRW.write_map_scl((int)RasterMapControl.Resolution);
*/

            string s = "";
            s = ClassMapRastrMenu.f_CloseMapMenu(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     MapForm.RasterMapControl,
                                     ref GlobalVarLn.MapOpened,
                                     ref GlobalVarLn.MatrixOpened
                                              );

            if (s != "")
                MessageBox.Show(s);


        }
        // *************************************************************************** CLOSE_MapForm

        // Центровка по СП1 ************************************************************************

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            string s = "";
            s = ClassMapRastrMenu.f_CenterMapTo(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     MapForm.RasterMapControl,
                                     GlobalVarLn.listJS[0].CurrentPosition.x,
                                     GlobalVarLn.listJS[0].CurrentPosition.y
                                              );
            if (s != "")
                MessageBox.Show(s);
        }
        // ************************************************************************ Центровка по СП1

        // Центровка по СП2 ************************************************************************

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            string s = "";
            s = ClassMapRastrMenu.f_CenterMapTo(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     MapForm.RasterMapControl,
                                     GlobalVarLn.listJS[1].CurrentPosition.x,
                                     GlobalVarLn.listJS[1].CurrentPosition.y
                                              );
            if (s != "")
                MessageBox.Show(s);
        }
        // ************************************************************************ Центровка по СП2

        // --------------------------------------------------------------------------------------------
        private void toolStripDropDownButton1_Click(object sender, EventArgs e)
        {
            ;
        }

        private void mapCompositionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ;
        }
        private void button2_Click_2(object sender, EventArgs e)
        {
            ;
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            ;
        }

        private void составКартыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ;
        } 

        private void cmStripMap_Opening(object sender, CancelEventArgs e)
        {
            ;
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            ;
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            ;
        }
        private void button2_Click_1(object sender, EventArgs e)
        {
            ;
        }
        // --------------------------------------------------------------------------------------------


    } // Class
} // namespace
