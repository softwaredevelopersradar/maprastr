﻿namespace GrozaMap
{
    partial class FormTab
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvTab = new System.Windows.Forms.DataGridView();
            this.bGetFreq = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.pTableSource = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTab)).BeginInit();
            this.pTableSource.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvTab
            // 
            this.dgvTab.AllowUserToAddRows = false;
            this.dgvTab.AllowUserToDeleteRows = false;
            this.dgvTab.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgvTab.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvTab.Location = new System.Drawing.Point(0, 0);
            this.dgvTab.Name = "dgvTab";
            this.dgvTab.Size = new System.Drawing.Size(640, 215);
            this.dgvTab.TabIndex = 236;
            // 
            // bGetFreq
            // 
            this.bGetFreq.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.bGetFreq.Location = new System.Drawing.Point(2, 223);
            this.bGetFreq.Name = "bGetFreq";
            this.bGetFreq.Size = new System.Drawing.Size(74, 23);
            this.bGetFreq.TabIndex = 241;
            this.bGetFreq.Text = "Accept";
            this.bGetFreq.UseVisualStyleBackColor = true;
            this.bGetFreq.Click += new System.EventHandler(this.bGetFreq_Click);
            // 
            // bClear
            // 
            this.bClear.ForeColor = System.Drawing.SystemColors.ControlText;
            this.bClear.Location = new System.Drawing.Point(82, 223);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(74, 23);
            this.bClear.TabIndex = 242;
            this.bClear.Text = "Clear";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // pTableSource
            // 
            this.pTableSource.Controls.Add(this.dgvTab);
            this.pTableSource.Location = new System.Drawing.Point(2, 3);
            this.pTableSource.Name = "pTableSource";
            this.pTableSource.Size = new System.Drawing.Size(640, 215);
            this.pTableSource.TabIndex = 243;
            // 
            // FormTab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 253);
            this.Controls.Add(this.pTableSource);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.bGetFreq);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Location = new System.Drawing.Point(200, 500);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormTab";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Table of reconnaissance";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.FormTab_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormTab_FormClosing);
            this.Load += new System.EventHandler(this.FormTab_Load);
            this.Shown += new System.EventHandler(this.FormTab_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTab)).EndInit();
            this.pTableSource.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.DataGridView dgvTab;
        private System.Windows.Forms.Button bGetFreq;
        private System.Windows.Forms.Button bClear;
        private System.Windows.Forms.Panel pTableSource;
    }
}