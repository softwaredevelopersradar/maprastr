﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using GrozaMap.Annotations;

namespace GrozaMap
{
    // Структуры ************************************************************
    // ---------------------------------------------------------------------
    // Структура rкоординат точки либо в м Меркатор (тогда Lat,Long=-1)/
    // либо в град (тогда X,Y=-1)

    public struct Point_XY_LatLong
    {
        // Merkator
        public double X;
        public double Y;

        public double Lat;
        public double Long;

    }
    // ---------------------------------------------------------------------

    // Структура маршрута

    public struct Way
    {
        public double X_m;
        public double Y_m;

        public double L_m;

    }
    // ---------------------------------------------------------------------
    // Структура Самолета

    public struct AirPlane
    {
        public double Lat;
        public double Long;
        public double X_m;
        public double Y_m;

        public double Angle;

        public int Num;
        public String sNum;

        public int sh;
        public int fl_sh;


    }
    // ---------------------------------------------------------------------
    // Для ЗПВ

    // .....................................................................
    [Serializable]
    public struct KoordThree
    {
        public KoordThree(double x, double y, double h)
        {
            this.x = x;
            this.y = y;
            this.h = h;
        }

        public double x;
        public double y;
        public double h;

        public static bool operator ==(KoordThree a, KoordThree b)
        {
            return    Math.Abs(a.x - b.x) < 1e-5
                   && Math.Abs(a.y - b.y) < 1e-5
                   && Math.Abs(a.h - b.h) < 1e-5;
        }

        public static bool operator !=(KoordThree a, KoordThree b)
        {
            return !(a == b);
        }

    } // struct KoordThree
    // .....................................................................
    public struct KoordTwo
    {
        public double x;
        public double y;

    } // struct KoordTwo
    // .....................................................................

    // ---------------------------------------------------------------------


    // SPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSP
    // Структура SP

    // ....................................................................
    public struct SP
    {
        // m на местности
        public double X_m;
        public double Y_m;
        public double H_m;

        public int NIP;

        public String sNum;
        public String sType;

        public int indzn;
    }
    // ....................................................................
    // CLASS JammerStation

    [Serializable]
    public class JammerStation
    {
        // См. выше для ЗПВ
        public KoordThree CurrentPosition
        {
            get { return _currentPosition; }
            set
            {
                if (_currentPosition == value && HasCurrentPosition)
                {
                    return;
                }
                _currentPosition = value;
                HasCurrentPosition = true;
                GlobalVarLn.NotifyListJSChanged();
            }
        }

        public KoordThree PlannedPosition
        {
            get { return _plannedPosition; }
            set
            {
                if (_plannedPosition == value && HasPlannedPosition)
                {
                    return;
                }
                _plannedPosition = value;
                HasPlannedPosition = true;
                GlobalVarLn.NotifyListJSChanged();
            }
        }

        public bool HasCurrentPosition
        {
            get { return _hasCurrentPosition; }
            set
            {
                if (_hasCurrentPosition == value)
                {
                    return;
                }

                _hasCurrentPosition = value;
                GlobalVarLn.NotifyListJSChanged();
            }
        }

        public bool HasPlannedPosition
        {
            get { return _hasPlannedPosition; }
            set
            {
                if (_hasPlannedPosition == value)
                {
                    return;
                }

                _hasPlannedPosition = value;
                GlobalVarLn.NotifyListJSChanged();
            }
        }

        public int IP;

        public string Name
        {
            get { return _name; }
            set
            {
                if (_name == value)
                {
                    return;
                }
                _name = value; 
                GlobalVarLn.NotifyListJSChanged();
            }
        }

        public string Type;

        public int indzn;
        private KoordThree _currentPosition;
        private KoordThree _plannedPosition;
        private string _name;
        private bool _hasPlannedPosition;
        private bool _hasCurrentPosition;

    } // Class JammerStation
    // ....................................................................

    // SPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSPSP

    // ---------------------------------------------------------------------
    // Структура LF

    public struct LF
    {
        // Merkator
        public double X_m;
        public double Y_m;
        public double H_m;


    }
    // ---------------------------------------------------------------------
    // Структура LF1

    [Serializable]
    public struct LF1
    {
        // grad WGS84
        public double Lat;
        public double Long;

        // Меркатор
        public double X_m;
        public double Y_m;
        public double H_m;
        public String sType;
        public int indzn;
        public double FrequencyMhz;

        // Индекс строки при ручном вводе OB1
        public int index;
        // =1 -> нажата enter при ручном вводе,=2-> ввод мышью на карте
        public int ent;

    }
    // ---------------------------------------------------------------------
    // Структура 

    public struct FB
    {

        // m на местности
        public double Fmin;
        public double Fmax;
        public double Bmin;
        public double Bmax;

    }
    // ---------------------------------------------------------------------
    // Структура 

    public struct FBSP
    {
        public String NumbSP;
        public int NumbDiap;
        public FB [] mfb;

    }
    // ---------------------------------------------------------------------


    // ************************************************************ Структуры

    class GlobalVarLn
    {
        // MAP *********************************************************************************
        //-----------------------------------------------------------------------------------------
        public static bool MapOpened = false;
        public static bool MatrixOpened = false;
        //-----------------------------------------------------------------------------------------
        // !!! Сюда заносятся координаты клика мыши на карте 

        // Mercator
        public static double X_Rastr = 0;
        public static double Y_Rastr = 0;

        public static double LAT_Rastr = 0;
        public static double LONG_Rastr = 0;
        public static double H_Rastr = 0;
        //-----------------------------------------------------------------------------------------
        public static bool MousePress = false;
        //-----------------------------------------------------------------------------------------
        // 555
        //1 - geographic 0- Mercator

        public static int TypeMap = 0;
        //-----------------------------------------------------------------------------------------

        // ********************************************************************************* MAP

        // Form ********************************************************************************
        // Объекты форм для использования в других формах (Инициализируются в Main)

        public static FormSP objFormSPG;
        public static FormLF objFormLFG;
        public static FormLF2 objFormLF2G;
        public static FormZO objFormZOG;
        public static FormOB1 objFormOB1G;
        public static FormOB2 objFormOB2G;
        public static FormSuppression objFormSuppressionG;
        public static FormSPFB objFormSPFBG;
        public static FormSPFB1 objFormSPFB1G;
        public static FormSPFB2 objFormSPFB2G;
        public static FormAz1 objFormAz1G;
        public static FormLineSightRange objFormLineSightRangeG;
        public static FormWay objFormWayG;
        public static FormSupSpuf objFormSupSpufG;
        public static FormSpuf objFormSpufG;
        public static FormScreen objFormScreenG;
        public static FormTRO objFormTROG;
        public static FormTab objFormTabG;
        // ******************************************************************************** Form

        // Events ******************************************************************************
        public static PC_DLL.ClientPC clientPCG;
        public static int AdressOwn = 0;
        public static int AdressLinked = 0;
        // ****************************************************************************** Events

        // Const *******************************************************************************

        // ????????????????? -> не используется
        // Глубина подавления
        public static int iSupDepth=1500;

        public static int RADIUS_EARTH = 8500000;
        // ******************************************************************************* Const

        // Peleng ******************************************************************************
        // Пеленг

        public static double[] mas_Pel = new double[10000];     // R,Широта,долгота
        public static double[] mas_Pel_XYZ = new double[10000]; // XYZ в опорной геоцентрической СК
        public static uint IndFikt_Pel_stat=0;
        public static uint NumbFikt_Pel_stat=0;
        public static int fl_Peleng_stat = 0;
        public static double LatP_Pel_stat=0;
        public static double LongP_Pel_stat=0;
        // ****************************************************************************** Peleng

        // Way *********************************************************************************
        // Маршрут

        // Флаг активации маршрута
        public static bool blWay_stat = false;
        public static int fFWay_stat = 0;
        public static int fl_Open_objFormWay = 0;

        // Размер dataGridView
        public static int sizeDatWay_stat = 1000;

        public static string flname_W_stat="";
        public static string flname_R_stat="";

        public static double dchisloW_stat=0;
        public static long ichisloW_stat=0;
        public static double X_Coordl5_stat=0;
        public static double Y_Coordl5_stat=0;

        public static double DXX_W_stat=0;
        public static double DYY_W_stat=0;

        // !!! Флаг окончаня маршрута (устанавливается по кнопке ЗАГРУЗИТЬ)
        public static uint flEndWay_stat = 0;

        public static uint iW_stat = 0;
        public static double LW_stat = 0;
        public static double X_StartW_stat = 0;
        public static double Y_StartW_stat = 0;
        public static double X1_W_stat = 0;
        public static double Y1_W_stat = 0;
        public static double X2_W_stat = 0;
        public static double Y2_W_stat = 0;

        public static List<Way> list_way = new List<Way>();
        // ********************************************************************************* Way

        // Azimuth *****************************************************************************
        // Азимут

        public static double XXXaz;
        public static double YYYaz;

        public static int fFAz1 = 0;
        public static int fl_Open_objFormAz1 = 0;

        // Флаг активации 
        public static bool blOB_az1 = false;
        // !!! 
        public static uint flEndOB_az1 = 0;
        public static uint flCoordOB_az1 = 0;


        // Размер dataGridView
        public static int sizeDatOB_az1 = 5;

        public static double XCenter_az1 = 0;
        public static double YCenter_az1 = 0;
        public static double HCenter_az1 = 0;

        public static int width_az1 = 24;
        public static int height_az1 = 24;


        public static double Lat84_az1 = 0;
        public static double Long84_az1 = 0;

        // ***************************************************************************** Azimuth

        // Aeroplane ***************************************************************************
        // Самолеты

        public static int fl_AirPlane = 0;
        public static int fl_AirPlaneVisible = 0;

        public static double Lat_air;
        public static double Long_air;
        public static double Angle_air;
        public static int Num_air = 0;
        public static String sNum_air = "";
        public static int Number_air = 0;
        public static int width_air = 24;
        public static int height_air = 24;

        public static int ndraw_air = 1;

        public static AirPlane[] mass_stAirPlane = new AirPlane[1000];
        public static List<AirPlane> list_air = new List<AirPlane>();
        // *************************************************************************** Aeroplane

        // ЗПВ *********************************************************************************
        // ЗПВ

        public static int fFLineSightRange = 0;
        public static int fl_Open_objFormLineSightRange = 0;

        public static int fl_LineSightRange = 0;
        public static uint flCoordZPV = 0; // =1-> Выбрали центр ЗПВ
        public static double XCenter_ZPV = 0;
        public static double YCenter_ZPV = 0;
        public static double LatCenter_ZPV_84 = 0;
        public static double LongCenter_ZPV_84 = 0;

        public static Point tpOwnCoordRect_ZPV;

        public static int iStepAngleInput_ZPV=1;
        public static int iStepLengthInput_ZPV=100;

        public static List<Point> listPointDSR = new List<Point>();

        public static Point [] listPointPictDSR;

        public static String NumbSP_lsr;
        // ********************************************************************************* ЗПВ

        //  Зона обнаружения СП ****************************************************************

        public static int fl_ZOSP = 0;
        public static double XCenter_ZOSP = 0;
        public static double YCenter_ZOSP = 0;
        public static int iR_ZOSP=0;
        public static List<Point> listDetectionZone = new List<Point>();
        //  **************************************************************** Зона обнаружения СП

        // ZoneSuppression *********************************************************************
        // Зона подавления радиолиний управления (зона подавления+зона неподавления)

        public static int fFSuppr = 0;
        public static int fl_Suppression = 0;
        public static int fl_Open_objFormSuppression = 0;

        public static uint flCoordSP_sup = 0; // =1-> Выбрали СП
        public static uint flCoordOP = 0;     // =1-> Выбрали OП

        public static Point tpOwnCoordRect_sup;
        public static Point tpPoint1Rect_sup;
        public static Point tpPointCentre1_sup;
        public static Point tpPointCentre2_sup;
        public static Point tpPointMiddle_sup;
        public static double XCenter_sup = 0;
        public static double YCenter_sup = 0;

        public static double LatCenter_sup_84 = 0;
        public static double LongCenter_sup_84 = 0;
        public static double LatCenter_sup_OP_84 = 0;
        public static double LongCenter_sup_OP_84 = 0;


        public static double XPoint1_sup = 0;
        public static double YPoint1_sup = 0;
        public static double RZ_sup = 0;
        public static List<Point> listControlJammingZone = new List<Point>();
        public static double RZ1_sup = 0;
        public static double RZ2_sup = 0;

        public static String NumbSP_sup = "";

        public static int flA_sup = 0;
        public static double s_sup = 30000;
        public static Point Luch_sup3;
        public static Point Luch_sup4;

        public static double H_sup = 0;

        public static List<Point> listPointDSR_sup = new List<Point>();

        // ********************************************************************* ZoneSuppression

        // Зона подавления навигации ***********************************************************

        public static int fFSupSpuf=0;
        public static int fl_Open_objFormSupSpuf = 0;

        public static int fl_supnav = 0;
        public static uint flCoord_supnav = 0; // =1-> Выбрали центр зоны
        public static double XCenter_supnav = 0;
        public static double YCenter_supnav = 0;

        public static double LatCenter_supnav_84 = 0;
        public static double LongCenter_supnav_84 = 0;

        public static String NumbSP_supnav;
        public static int iR_supnav = 0;
        public static List<Point> listNavigationJammingZone = new List<Point>();
        public static Point tpOwnCoordRect_supnav;
        // *********************************************************** Зона подавления навигации

        // Зона спуфинга ***********************************************************************
        // Зона спуфинга+зона подавления навигации для зоны спуфинга

        public static int fFSpuf = 0;
        public static int fl_Open_objFormSpuf = 0;

        public static int fl_spf = 0;
        public static int flS_spf = 0;
        public static int flJ_spf = 0;

        public static uint flCoord_spf = 0; // =1-> Выбрали центр зоны
        public static double XCenter_spf = 0;
        public static double YCenter_spf = 0;

        public static double LatCenter_spf_84 = 0;
        public static double LongCenter_spf_84 = 0;

        public static String NumbSP_spf;
        public static int iR1_spf = 0;
        public static int iR2_spf = 0;
        public static List<Point> listSpoofingZone = new List<Point>();
        public static List<Point> listSpoofingJamZone = new List<Point>();
        public static Point tpOwnCoordRect_spf;
        // *********************************************************************** Зона спуфинга

        // SP **********************************************************************************
        // Станции помех

        // -------------------------------------------------------------------------------------
        public static int fFSP = 0;
        public static int fl_Open_objFormSP = 0;

        // Флаг активации 
        public static bool blSP_stat = false;

        // Размер dataGridView
        // ?????? ->  No use
        public static int sizeDatSP_stat = 4;

        // Для перерисовки SP
        public static uint flEndSP_stat = 0;

        public static double XCenter_SP = 0;
        public static double YCenter_SP = 0;
        public static double HCenter_SP = 0;


        public static int width_SP = 24;
        public static int height_SP = 24;

        // -------------------------------------------------------------------------------------
        // Clear SP

        public static void ClearListJS()
        {
            foreach (var station in listJS)
            {
                station.HasCurrentPosition = false;
                station.HasPlannedPosition = false;
            }
        }
        // -------------------------------------------------------------------------------------
        // LoadListJS

        public static void LoadListJS()
        {
            var formatter = new BinaryFormatter();
            try
            {
                //using (var fs = new FileStream("SP.bin", FileMode.OpenOrCreate))
                using (var fs = new FileStream(Application.StartupPath + "\\SaveInFiles\\JS.bin", FileMode.OpenOrCreate))
                {
                    _listJS = (List<JammerStation>)formatter.Deserialize(fs);
                    NotifyListJSChanged();
                }
            }
            catch (Exception exception)
            {
                // ignored
            }

        }// P/P
        // -------------------------------------------------------------------------------------

        public static void NotifyListJSChanged()
        {
            if (ListJSChangedEvent != null)
            {
                ListJSChangedEvent(listJS, EventArgs.Empty);
            }
        }
        // -------------------------------------------------------------------------------------
        public static event EventHandler ListJSChangedEvent;

        public static IReadOnlyList<JammerStation> listJS { get { return _listJS; } }

        public static List<JammerStation> GetListJSWithCurrentPosition()
        {
            return listJS.Where(s => s.HasCurrentPosition).ToList();
        }

        private static List<JammerStation> _listJS = new List<JammerStation>
        {
            new JammerStation
            {
                HasCurrentPosition = false,
                HasPlannedPosition = false,
                indzn = 0,
                IP = 33,
                Name = "111",
                Type = "Groza-S",
            },
            new JammerStation
            {
                HasCurrentPosition = false,
                HasPlannedPosition = false,
                indzn = 0,
                IP = 35,
                Name = "112",
                Type = "Groza-S",
            }
        };
        // -------------------------------------------------------------------------------------
        public static uint flCoord_SP2 = 0; // =1-> Выбрали 

        public static int NumbSP = 5;

        // Значки
        public static int NumbZSP = 10;
        public static int iZSP = 0;


        public static double lt1sp = 0;
        public static double lt2sp = 0;
        public static double ln1sp = 0;
        public static double ln2sp = 0;
        public static int flagGPS_SP = 0;
        public static double hhsp = 0;

        public static int flllsp = 0;

        public static int fclSP = 0;
        // -------------------------------------------------------------------------------------
        // -------------------------------------------------------------------------------------

        // ********************************************************************************** SP

        // OB1 *********************************************************************************

        public static int fFOB1 = 0;
        public static int fl_Open_objFormOB1 = 0;

        // Флаг активации 
        public static bool blOB1_stat = false;

        // Размер dataGridView
        public static int sizeDatOB1_stat = 100;  // 10_10_2018

        // !!! Флаг окончаня маршрута (устанавливается по кнопке ЗАГРУЗИТЬ)
        public static uint flEndOB1_stat = 0;

        public static uint iOB1_stat = 0;

        public static double X_OB1 = 0;
        public static double Y_OB1 = 0;
        public static double H_OB1 = 0;

        public static int width_OB1 = 24;
        public static int height_OB1 = 24;

        public static List<LF> list_OB1 = new List<LF>();
        public static List<LF1> list1_OB1 = new List<LF1>();
        public static List<LF1> list1_OB1_dubl = new List<LF1>();


        // Значки
        public static int NumbZOB1 = 7;
        public static int iZOB1 = 0;

        public static string flname_OB1 = "";

        // ********************************************************************************* OB1

        // OB2 *********************************************************************************

        // -------------------------------------------------------------------------------------
        // LoadListOB2
        // Чтение из bin файла в List
        // public static List<LF1> list1_OB2 = new List<LF1>();

        public static void LoadListOB2()
        {
            var formatter = new BinaryFormatter();
            try
            {
                //using (var fs = new FileStream("OB2.bin", FileMode.OpenOrCreate))
                using (var fs = new FileStream(Application.StartupPath + "\\SaveInFiles\\ObjectsEnemy.bin", FileMode.OpenOrCreate))
                {
                    list1_OB2 = (List<LF1>) formatter.Deserialize(fs);
                }
            }
            catch (Exception exception)
            {
                // ignored
            }
        } // LoadListOB2
        // -------------------------------------------------------------------------------------
        // SaveListOB2
        // Сохранение List в bin файл
        // public static List<LF1> list1_OB2 = new List<LF1>();

        public static void SaveListOB2()
        {
            if (list1_OB2.Count == 0)
            {
                //return;
            }

            var formatter = new BinaryFormatter();
            try
            {
                using (var fs = new FileStream(Application.StartupPath + "\\SaveInFiles\\ObjectsEnemy.bin", FileMode.OpenOrCreate))
                {
                    formatter.Serialize(fs, list1_OB2);
                }
            }
            catch (Exception exception)
            {
                // ignored
            }

        } // SaveListOB2
        // -------------------------------------------------------------------------------------
        public static int fFOB2 = 0;
        public static int fl_Open_objFormOB2 = 0;

        // Флаг активации 
        public static bool blOB2_stat = false;

        // Размер dataGridView
        public static int sizeDatOB2_stat = 100;  // 10_10_2018

        // !!! Флаг окончаня маршрута (устанавливается по кнопке ЗАГРУЗИТЬ)
        public static uint flEndOB2_stat = 0;

        public static uint iOB2_stat { get { return (uint)list1_OB2.Count; } }

        public static double X_OB2 = 0;
        public static double Y_OB2 = 0;
        public static double H_OB2 = 0;

        public static int width_OB2 = 24;
        public static int height_OB2 = 24;

        public static List<LF> list_OB2 = new List<LF>();

        public static List<LF1> list1_OB2 = new List<LF1>();
        public static List<LF1> list1_OB2_dubl = new List<LF1>();

        // Значки
        public static int NumbZOB2 = 9;
        public static int iZOB2 = 0;

        // -------------------------------------------------------------------------------------

        // ********************************************************************************* OB2

        // LF1 *********************************************************************************
        // LF1(линия фронта1)

        public static int fFLF1 = 0;
        public static int fl_Open_objFormLF = 0;

        // Флаг активации 
        public static bool blLF1_stat = false;

        // Размер dataGridView
        public static int sizeDatLF1_stat = 100;

        // !!! Флаг окончаня маршрута (устанавливается по кнопке ЗАГРУЗИТЬ)
        public static uint flEndLF1_stat = 0;

        public static uint iLF1_stat = 0;

        public static double X_LF1 = 0;
        public static double Y_LF1 = 0;
        public static double H_LF1 = 0;

        public static int width_LF1 = 24;
        public static int height_LF1 = 24;

        public static List<LF> list_LF1 = new List<LF>();
        // ********************************************************************************* LF1

        // LF2 *********************************************************************************
        // LF2(линия фронта2)

        public static int fFLF2 = 0;
        public static int fl_Open_objFormLF2 = 0;

        // Флаг активации 
        public static bool blLF2_stat = false;

        // Размер dataGridView
        public static int sizeDatLF2_stat = 100;

        // !!! Флаг окончаня маршрута (устанавливается по кнопке ЗАГРУЗИТЬ)
        public static uint flEndLF2_stat = 0;

        public static uint iLF2_stat = 0;

        public static double X_LF2 = 0;
        public static double Y_LF2 = 0;
        public static double H_LF2 = 0;

        public static int width_LF2 = 24;
        public static int height_LF2 = 24;

        public static List<LF> list_LF2 = new List<LF>();
        // ********************************************************************************* LF2

        // ZO *********************************************************************************
        // ZO(зона ответственности)

        public static int fFZO = 0;
        public static int fl_Open_objFormZO = 0;

        // Флаг активации 
        public static bool blZO_stat = false;

        // Размер dataGridView
        public static int sizeDatZO_stat = 200;

        // !!! Флаг окончаня маршрута (устанавливается по кнопке ЗАГРУЗИТЬ)
        public static uint flEndZO_stat = 0;

        public static uint iZO_stat = 0;

        public static double X_ZO = 0;
        public static double Y_ZO = 0;
        public static double H_ZO = 0;

        public static int width_ZO = 24;
        public static int height_ZO = 24;

        public static List<LF> list_ZO = new List<LF>();
        // ********************************************************************************* ZO

        // TRO ********************************************************************************
        // Тактическая и радиоэлектронная обстановка

        // Флаг активации 
        public static bool blTRO_stat = false;
        public static uint flEndTRO_stat = 0;
        public static int fl_Open_objFormTRO = 0;


        public static int fFTRO = 0;
        public static int fFZagrTRO = 0;
        // ******************************************************************************** TRO

        // FRR ********************************************************************************
        // Диапазоны частот и сектора радиоразведки (РР)

        public static int fFSPFB = 0;
        public static int fl_Open_objFormSPFB = 0;

        // Флаг активации 
        public static uint flF_f1 = 0;

        // Размер dataGridView
        public static int sizeDat_f1 = 200;
        public static int sizeDiap_f1 = 9; // For 10

        public static uint iSP_f1 = 0;

        public static List<FBSP> list1_f1 = new List<FBSP>(); // SP1

        public static String NumbSP_f1;

        public static uint indmas_f1 = 0;
        public static int flrd_f1 = 200;

        public static double Az1_f1=0;
        public static double Az2_f1=0;
        public static double Az_f1=0;
        public static int flAz1_f1=0;
        public static int flAz2_f1=0;
        // ******************************************************************************** FRR

        // FRP ********************************************************************************
        // Диапазоны частот и сектора радиоподавления РП

        public static int fFSPFB1 = 0;
        public static int fl_Open_objFormSPFB1 = 0;

        // Флаг активации 
        public static uint flF_f2 = 0;

        // Размер dataGridView
        public static int sizeDat_f2 = 200;
        public static int sizeDiap_f2 = 9; // For 10

        public static uint iSP_f2 = 0;

        public static List<FBSP> list1_f2 = new List<FBSP>(); 

        public static String NumbSP_f2;

        public static uint indmas_f2 = 0;
        public static int flrd_f2 = 200;

        public static double Az1_f2 = 0;
        public static double Az2_f2 = 0;
        public static double Az_f2 = 0;
        public static int flAz1_f2 = 0;
        public static int flAz2_f2 = 0;
        // ******************************************************************************** FRP

        // FZapr *****************************************************************************
        // Диапазоны запрещенных частот 

        // Флаг активации 
        public static uint flF_f3 = 0;
        public static uint flF1_f3 = 0;
        public static int fl_Open_objFormSPFB2 = 0;

        // Размер dataGridView
        public static int sizeDat_f3 = 21;
        public static int sizeDiap_f3 = 19; // For 20

        public static uint iSP_f3 = 0;

        public static List<FBSP> list1_f3 = new List<FBSP>();
        public static List<FBSP> list2_f3 = new List<FBSP>();


        public static String NumbSP_f3;

        public static uint indmas_f3 = 0;
        public static int flrd_f3 = 200;
        public static uint indmas1_f3 = 0;
        public static int flrd1_f3 = 200;
        // ***************************************************************************** FZapr

        // Tab *******************************************************************************
        // Получение таблицы от оператора

        public static int sizeTab = 100;
        public static int fllTab = 0;
        public static List<AirPlane> list_air_tab = new List<AirPlane>();

        public static int fl_Open_objFormTab = 0;

        // ******************************************************************************* Tab

        // Screen ****************************************************************************
        // Screen

        public static int flScr = 0;
        public static int flScrM1 = 0;
        public static int flScrM2 = 0;
        public static int flScrM3 = 0;
        public static int fl_Open_objFormScreen = 0;

        public static double X1Scr = 0;
        public static double Y1Scr = 0;
        public static double X2Scr = 0;
        public static double Y2Scr = 0;

        public static Bitmap bmpScr;
        public static Point location1;
        public static Point location2;

        public static string flname_W_Scr = "";
        public static string flname_R_Scr = "";
        // **************************************************************************** Screen

    } // Class
} // NameSpace
