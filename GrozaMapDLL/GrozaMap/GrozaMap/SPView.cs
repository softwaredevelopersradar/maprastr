﻿using System;
using System.Windows.Forms;

using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;

namespace GrozaMap
{
    public partial class SPView : UserControl
    {
        // -------------------------------------------------------
        // Конструктор
        public SPView()
        {
            InitializeComponent();
        }
        // -------------------------------------------------------
        public event EventHandler CurrentLabelClickEvent;
        public event EventHandler PlannedlabelClickEvent;

        public event EventHandler<string> NameChangedEvent;
        public event EventHandler<string> TypeChangedEvent;
        public event EventHandler<int> IpChangedEvent;
        // -------------------------------------------------------

        // NameSP ************************************************
        // Получить/Установить имя СП

        private string _nameSP;

        // !!! Это доступное всем свойство, которое позволяет управлять внутренней
        //     переменной _nameSP
        public string NameSP
        {
            // Блок, котороый позволяет получить значение _nameSP
            // !!! В main: 
            // SPView ppp=new SPView();
            // ppp.NameSP="ggg"; -> _nameSP="ggg"
            get { return _nameSP; }

            // Установить имя
            set 
            {
                if (_nameSP == value)
                {
                    return;
                }
                _nameSP = value;
                NameTextBox.Text = value;

                if (NameChangedEvent != null)
                {
                    NameChangedEvent(this, value);
                }
            } // set
        }
        // ************************************************ NameSP

        // TypeSP ************************************************

        private string _typeSP;

        public string TypeSP
        {
            get { return _typeSP; }
            set
            {
                if (_typeSP == value)
                {
                    return;
                }
                _typeSP = value;
                TypeTextBox.Text = value;

                if (TypeChangedEvent != null)
                {
                    TypeChangedEvent(this, value);
                }
            }
        }
        // ************************************************ TypeSP

        // IP_SP *************************************************

        private int _ip;

        public int IP
        {
            get { return _ip; }
            set
            {
                if (_ip == value)
                {
                    return;
                }
                _ip = value;
                IpTextBox.Text = _ip.ToString();

                if (IpChangedEvent != null)
                {
                    IpChangedEvent(this, value);
                }
            }
        }
        // ************************************************* IP_SP

        // LatSP *************************************************

        private double _currentLatitude;

        public double CurrentLatitude
        {
            get { return _currentLatitude; }
            set
            {
                _currentLatitude = value;
                CurrentLatTextBox.Text = value.ToString("F3");
            }
        }
        // ************************************************* LatSP

        // LongSP ************************************************

        private double _currentLongitude;

        public double CurrentLongitude
        {
            get { return _currentLongitude; }
            set
            {
                _currentLongitude = value;
                CurrentLongTextBox.Text = value.ToString("F3");
            }
        }
        // ************************************************ LongSP

        // H_SP **************************************************

        private double _currentHeight;

        public double CurrentHeight
        {
            get { return _currentHeight; }
            set
            {
                _currentHeight = value;
                CurrentHTextBox.Text = ((int) value).ToString();
            }
        }
        // ************************************************** H_SP

        // LatSP2 ************************************************

        private double _plannedLatitude;

        public double PlannedLatitude
        {
            get { return _plannedLatitude; }
            set
            {
                _plannedLatitude = value;
                PlannedLatTextBox.Text = value.ToString("F3");
            }
        }
        // ************************************************ LatSP2

        // LongSP2 ***********************************************

        private double _plannedLongitude;

        public double PlannedLongitude
        {
            get { return _plannedLongitude; }
            set
            {
                _plannedLongitude = value;
                PlannedLongTextBox.Text = value.ToString("F3");
            }
        }
        // *********************************************** LongSP2

        // H_SP2 *************************************************

        private double _plannedHeight;

        public double PlannedHeight
        {
            get { return _plannedHeight; }
            set
            {
                _plannedHeight = value;
                PlannedHTextBox.Text = ((int)value).ToString();
            }
        }
        // ************************************************* H_SP2

        // Обновить Lat,Long,H_SP,SP2 ****************************

        public void UpdateView(SP sp, bool isCurrent)
        {
            if (isCurrent)
            {
                CurrentLatitude = sp.X_m;
                CurrentLongitude = sp.Y_m;
                CurrentHeight = sp.H_m;
            }
            else
            {
                PlannedLatitude = sp.X_m;
                PlannedLongitude = sp.Y_m;
                PlannedHeight = sp.H_m;
            }

            IP = sp.NIP;
            TypeSP = sp.sType;
            NameSP = sp.sNum;
        }
        // **************************** Обновить Lat,Long,H_SP,SP2

        // Обновить таблицу **************************************
        // Обновить таблицу

        public void UpdateView(JammerStation station)
        {
            if (station.HasCurrentPosition)
            {
                //0206
                //var pos = Map.MapPlaneToRealGeo(station.CurrentPosition.x, station.CurrentPosition.y);
                //CurrentLatitude = pos.X;
                //CurrentLongitude = pos.Y;
                //CurrentHeight = station.CurrentPosition.h;

                //0206*
                // преобразование из меркатора в wgs84
                var p2 = Mercator.ToLonLat(station.CurrentPosition.x, station.CurrentPosition.y);
                CurrentLatitude = p2.Y;
                CurrentLongitude = p2.X;
                CurrentHeight = station.CurrentPosition.h;

            }
            else
            {
                CurrentLatitude = 0;
                CurrentLongitude = 0;
                CurrentHeight = 0;
            }
            if (station.HasPlannedPosition)
            {
                //0206
                //var pos = Map.MapPlaneToRealGeo(station.PlannedPosition.x, station.PlannedPosition.y);
                //PlannedLatitude = pos.X;
                //PlannedLongitude = pos.Y;
                //PlannedHeight = station.PlannedPosition.h;

                //0206*
                // преобразование из меркатора в wgs84
                var p3 = Mercator.ToLonLat(station.PlannedPosition.x, station.PlannedPosition.y);
                PlannedLatitude = p3.Y;
                PlannedLongitude = p3.X;
                PlannedHeight = station.PlannedPosition.h;

            }
            else
            {
                PlannedLatitude = 0;
                PlannedLongitude = 0;
                PlannedHeight = 0;
            }

            IP = station.IP;
            TypeSP = station.Type;
            NameSP = station.Name;
        }

        // ************************************** Обновить таблицу

        // GetSP *************************************************
        // Заполнить структуру SP текущими данными SP

        public SP GetCurrentSP()
        {
            return new SP
            {
                X_m = CurrentLatitude,
                Y_m = CurrentLongitude,
                H_m = CurrentHeight,
                sNum = NameSP,
                sType = TypeSP,
                NIP = IP,
                indzn = GlobalVarLn.iZSP
            };
        }
        // ************************************************* GetSP

        // GetSP2 ************************************************
        // Заполнить структуру SP текущими данными SP2

        public SP GetPlannedSP()
        {
            return new SP
            {
                X_m = PlannedLatitude,
                Y_m = PlannedLongitude,
                H_m = PlannedHeight,
                sNum = NameSP,
                sType = TypeSP,
                NIP = IP,
                indzn = GlobalVarLn.iZSP
            };
        }
        // ************************************************ GetSP2

        private void CurrentLabel_Click(object sender, EventArgs e)
        {
            if (CurrentLabelClickEvent != null)
            {
                CurrentLabelClickEvent(this, EventArgs.Empty);
            }
        }
        // -------------------------------------------------------

        private void PlannedLabel_Click(object sender, EventArgs e)
        {
            if (PlannedlabelClickEvent != null)
            {
                PlannedlabelClickEvent(this, EventArgs.Empty);
            }
        }
        // -------------------------------------------------------

        private void TextBoxOnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ActiveControl = LatitudeLabel;
            }
        }
        // -------------------------------------------------------

        // Уйти с окна Name ***************************************

        private void NameTextBox_Leave(object sender, EventArgs e)
        {
            if (NameTextBox.Text != "")
            {
                NameSP = NameTextBox.Text;
            }
        }
        // *************************************** Уйти с окна Name

    } // Class
} // NameSpace
