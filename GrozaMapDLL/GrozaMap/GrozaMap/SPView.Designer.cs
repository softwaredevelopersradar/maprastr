﻿namespace GrozaMap
{
    partial class SPView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NameTextBox = new System.Windows.Forms.TextBox();
            this.NameLabel = new System.Windows.Forms.Label();
            this.Iplabel = new System.Windows.Forms.Label();
            this.IpTextBox = new System.Windows.Forms.TextBox();
            this.TypeLabel = new System.Windows.Forms.Label();
            this.TypeTextBox = new System.Windows.Forms.TextBox();
            this.CurrentLabel = new System.Windows.Forms.Label();
            this.PlannedLabel = new System.Windows.Forms.Label();
            this.BottomTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.LatitudeLabel = new System.Windows.Forms.Label();
            this.LongitudeLabel = new System.Windows.Forms.Label();
            this.HeightLabel = new System.Windows.Forms.Label();
            this.CurrentLatTextBox = new System.Windows.Forms.TextBox();
            this.CurrentLongTextBox = new System.Windows.Forms.TextBox();
            this.CurrentHTextBox = new System.Windows.Forms.TextBox();
            this.PlannedLongTextBox = new System.Windows.Forms.TextBox();
            this.PlannedHTextBox = new System.Windows.Forms.TextBox();
            this.PlannedLatTextBox = new System.Windows.Forms.TextBox();
            this.BottomTableLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // NameTextBox
            // 
            this.NameTextBox.Location = new System.Drawing.Point(47, 3);
            this.NameTextBox.Name = "NameTextBox";
            this.NameTextBox.Size = new System.Drawing.Size(70, 20);
            this.NameTextBox.TabIndex = 0;
            this.NameTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextBoxOnKeyDown);
            this.NameTextBox.Leave += new System.EventHandler(this.NameTextBox_Leave);
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Location = new System.Drawing.Point(3, 6);
            this.NameLabel.Margin = new System.Windows.Forms.Padding(3);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(38, 13);
            this.NameLabel.TabIndex = 1;
            this.NameLabel.Text = "Name:";
            // 
            // Iplabel
            // 
            this.Iplabel.AutoSize = true;
            this.Iplabel.Location = new System.Drawing.Point(242, 6);
            this.Iplabel.Margin = new System.Windows.Forms.Padding(3);
            this.Iplabel.Name = "Iplabel";
            this.Iplabel.Size = new System.Drawing.Size(20, 13);
            this.Iplabel.TabIndex = 3;
            this.Iplabel.Text = "IP:";
            // 
            // IpTextBox
            // 
            this.IpTextBox.Location = new System.Drawing.Point(268, 3);
            this.IpTextBox.Name = "IpTextBox";
            this.IpTextBox.Size = new System.Drawing.Size(45, 20);
            this.IpTextBox.TabIndex = 2;
            // 
            // TypeLabel
            // 
            this.TypeLabel.AutoSize = true;
            this.TypeLabel.Location = new System.Drawing.Point(122, 6);
            this.TypeLabel.Margin = new System.Windows.Forms.Padding(3);
            this.TypeLabel.Name = "TypeLabel";
            this.TypeLabel.Size = new System.Drawing.Size(34, 13);
            this.TypeLabel.TabIndex = 5;
            this.TypeLabel.Text = "Type:";
            // 
            // TypeTextBox
            // 
            this.TypeTextBox.Location = new System.Drawing.Point(162, 3);
            this.TypeTextBox.Name = "TypeTextBox";
            this.TypeTextBox.Size = new System.Drawing.Size(70, 20);
            this.TypeTextBox.TabIndex = 4;
            // 
            // CurrentLabel
            // 
            this.CurrentLabel.AutoSize = true;
            this.CurrentLabel.Location = new System.Drawing.Point(4, 24);
            this.CurrentLabel.Margin = new System.Windows.Forms.Padding(3);
            this.CurrentLabel.Name = "CurrentLabel";
            this.CurrentLabel.Size = new System.Drawing.Size(81, 13);
            this.CurrentLabel.TabIndex = 4;
            this.CurrentLabel.Text = "Current location";
            this.CurrentLabel.Click += new System.EventHandler(this.CurrentLabel_Click);
            // 
            // PlannedLabel
            // 
            this.PlannedLabel.AutoSize = true;
            this.PlannedLabel.Location = new System.Drawing.Point(4, 52);
            this.PlannedLabel.Margin = new System.Windows.Forms.Padding(3);
            this.PlannedLabel.Name = "PlannedLabel";
            this.PlannedLabel.Size = new System.Drawing.Size(86, 13);
            this.PlannedLabel.TabIndex = 5;
            this.PlannedLabel.Text = "Planned location";
            this.PlannedLabel.Click += new System.EventHandler(this.PlannedLabel_Click);
            // 
            // BottomTableLayout
            // 
            this.BottomTableLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BottomTableLayout.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.BottomTableLayout.ColumnCount = 4;
            this.BottomTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.BottomTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.BottomTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.BottomTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.BottomTableLayout.Controls.Add(this.PlannedLabel, 0, 2);
            this.BottomTableLayout.Controls.Add(this.CurrentLabel, 0, 1);
            this.BottomTableLayout.Controls.Add(this.LatitudeLabel, 1, 0);
            this.BottomTableLayout.Controls.Add(this.LongitudeLabel, 2, 0);
            this.BottomTableLayout.Controls.Add(this.HeightLabel, 3, 0);
            this.BottomTableLayout.Controls.Add(this.CurrentLatTextBox, 1, 1);
            this.BottomTableLayout.Controls.Add(this.CurrentLongTextBox, 2, 1);
            this.BottomTableLayout.Controls.Add(this.CurrentHTextBox, 3, 1);
            this.BottomTableLayout.Controls.Add(this.PlannedLongTextBox, 2, 2);
            this.BottomTableLayout.Controls.Add(this.PlannedHTextBox, 3, 2);
            this.BottomTableLayout.Controls.Add(this.PlannedLatTextBox, 1, 2);
            this.BottomTableLayout.Location = new System.Drawing.Point(-1, 26);
            this.BottomTableLayout.Margin = new System.Windows.Forms.Padding(0);
            this.BottomTableLayout.Name = "BottomTableLayout";
            this.BottomTableLayout.RowCount = 3;
            this.BottomTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.BottomTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.BottomTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.BottomTableLayout.Size = new System.Drawing.Size(318, 77);
            this.BottomTableLayout.TabIndex = 7;
            // 
            // LatitudeLabel
            // 
            this.LatitudeLabel.AutoSize = true;
            this.LatitudeLabel.Location = new System.Drawing.Point(97, 4);
            this.LatitudeLabel.Margin = new System.Windows.Forms.Padding(3);
            this.LatitudeLabel.Name = "LatitudeLabel";
            this.LatitudeLabel.Size = new System.Drawing.Size(46, 13);
            this.LatitudeLabel.TabIndex = 6;
            this.LatitudeLabel.Text = "Lat, deg";
            // 
            // LongitudeLabel
            // 
            this.LongitudeLabel.AutoSize = true;
            this.LongitudeLabel.Location = new System.Drawing.Point(172, 4);
            this.LongitudeLabel.Margin = new System.Windows.Forms.Padding(3);
            this.LongitudeLabel.Name = "LongitudeLabel";
            this.LongitudeLabel.Size = new System.Drawing.Size(55, 13);
            this.LongitudeLabel.TabIndex = 7;
            this.LongitudeLabel.Text = "Long, deg";
            // 
            // HeightLabel
            // 
            this.HeightLabel.AutoSize = true;
            this.HeightLabel.Location = new System.Drawing.Point(247, 4);
            this.HeightLabel.Margin = new System.Windows.Forms.Padding(3);
            this.HeightLabel.Name = "HeightLabel";
            this.HeightLabel.Size = new System.Drawing.Size(29, 13);
            this.HeightLabel.TabIndex = 8;
            this.HeightLabel.Text = "H, m";
            // 
            // CurrentLatTextBox
            // 
            this.CurrentLatTextBox.Location = new System.Drawing.Point(97, 24);
            this.CurrentLatTextBox.Name = "CurrentLatTextBox";
            this.CurrentLatTextBox.Size = new System.Drawing.Size(68, 20);
            this.CurrentLatTextBox.TabIndex = 9;
            // 
            // CurrentLongTextBox
            // 
            this.CurrentLongTextBox.Location = new System.Drawing.Point(172, 24);
            this.CurrentLongTextBox.Name = "CurrentLongTextBox";
            this.CurrentLongTextBox.Size = new System.Drawing.Size(68, 20);
            this.CurrentLongTextBox.TabIndex = 10;
            // 
            // CurrentHTextBox
            // 
            this.CurrentHTextBox.Location = new System.Drawing.Point(247, 24);
            this.CurrentHTextBox.Name = "CurrentHTextBox";
            this.CurrentHTextBox.Size = new System.Drawing.Size(68, 20);
            this.CurrentHTextBox.TabIndex = 11;
            // 
            // PlannedLongTextBox
            // 
            this.PlannedLongTextBox.BackColor = System.Drawing.Color.LightCyan;
            this.PlannedLongTextBox.Location = new System.Drawing.Point(172, 52);
            this.PlannedLongTextBox.Name = "PlannedLongTextBox";
            this.PlannedLongTextBox.Size = new System.Drawing.Size(68, 20);
            this.PlannedLongTextBox.TabIndex = 12;
            // 
            // PlannedHTextBox
            // 
            this.PlannedHTextBox.BackColor = System.Drawing.Color.LightCyan;
            this.PlannedHTextBox.Location = new System.Drawing.Point(247, 52);
            this.PlannedHTextBox.Name = "PlannedHTextBox";
            this.PlannedHTextBox.Size = new System.Drawing.Size(68, 20);
            this.PlannedHTextBox.TabIndex = 13;
            // 
            // PlannedLatTextBox
            // 
            this.PlannedLatTextBox.BackColor = System.Drawing.Color.LightCyan;
            this.PlannedLatTextBox.Location = new System.Drawing.Point(97, 52);
            this.PlannedLatTextBox.Name = "PlannedLatTextBox";
            this.PlannedLatTextBox.Size = new System.Drawing.Size(68, 20);
            this.PlannedLatTextBox.TabIndex = 14;
            // 
            // SPView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.BottomTableLayout);
            this.Controls.Add(this.TypeLabel);
            this.Controls.Add(this.TypeTextBox);
            this.Controls.Add(this.Iplabel);
            this.Controls.Add(this.IpTextBox);
            this.Controls.Add(this.NameLabel);
            this.Controls.Add(this.NameTextBox);
            this.Name = "SPView";
            this.Size = new System.Drawing.Size(316, 102);
            this.BottomTableLayout.ResumeLayout(false);
            this.BottomTableLayout.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.Label Iplabel;
        private System.Windows.Forms.Label TypeLabel;
        private System.Windows.Forms.Label PlannedLabel;
        private System.Windows.Forms.Label CurrentLabel;
        private System.Windows.Forms.TableLayoutPanel BottomTableLayout;
        private System.Windows.Forms.Label LatitudeLabel;
        private System.Windows.Forms.Label LongitudeLabel;
        private System.Windows.Forms.Label HeightLabel;
        public System.Windows.Forms.TextBox NameTextBox;
        public System.Windows.Forms.TextBox IpTextBox;
        public System.Windows.Forms.TextBox TypeTextBox;
        public System.Windows.Forms.TextBox CurrentLatTextBox;
        public System.Windows.Forms.TextBox CurrentLongTextBox;
        public System.Windows.Forms.TextBox CurrentHTextBox;
        public System.Windows.Forms.TextBox PlannedLongTextBox;
        public System.Windows.Forms.TextBox PlannedHTextBox;
        public System.Windows.Forms.TextBox PlannedLatTextBox;
    }
}
