﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
using System.Windows.Input;

using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;
using Bitmap = System.Drawing.Bitmap;
using Point = System.Drawing.Point;

namespace GrozaMap
{
    class ClassMapRastrDopFunctions
    {
        // Update_DataGridView_FromList<LF1> *************************************************
        // Обновление DataGridView из листа структур типа LF1
        // Используется: OB2

        public static void f_Update_datgrid_LF1(
                                           DataGridView datgrid,
                                           List<LF1> lst,
                                           // Количество строк в таблице
                                           int str
                                           )
        {

            // ...............................................................................
            // Очистка dataGridView + Установка str строк

            // Очистка dataGridView
            while (datgrid.Rows.Count != 0)
                datgrid.Rows.Remove(datgrid.Rows[datgrid.Rows.Count - 1]);

            for (int i = 0; i < str; i++)
            {
                datgrid.Rows.Add("", "", "", "", "");
            }
            // ...............................................................................
            // Заполнение

            for (var i = 0; i < lst.Count; ++i)
            {
                var ob = lst[i];
                var row = datgrid.Rows[i];

                row.Cells[0].Value = ob.Lat.ToString("F3");
                row.Cells[1].Value = ob.Long.ToString("F3");
                row.Cells[2].Value = ob.H_m;
                row.Cells[3].Value = ob.sType;
                row.Cells[4].Value = ob.FrequencyMhz.ToString("F3");
            }
        }
        // ************************************************* Update_DataGridView_FromList<LF1>

        // Updata_List<LF1>_FromDataGridView *************************************************
        // Обновление листа структур типа LF1 из DataGridView
        // Используется: OB2 -> ???????????  пока не используется

        public static void f_Update_ListLF1_datgrid(
                                           DataGridView datgrid,
                                           ref List<LF1> lst
                                           // Количество строк в таблице
                                           //int str
                                           )
        {
            double xx = 0;
            double yy = 0;
            int i = 0;
            String s1 = "";

            for (i = 0; i < datgrid.Rows.Count; ++i)
            {
                var ob = lst[i];
                var row = datgrid.Rows[i];

                var x = Convert.ToDouble((string)row.Cells[0].Value); // Lat
                var y = Convert.ToDouble((string)row.Cells[1].Value); //Long

                // преобразование В меркатор
                var p = Mercator.FromLonLat(y, x);
                xx = p.X;
                yy = p.Y;

                var h = (double)row.Cells[2].Value;     // H

                var name = (string)row.Cells[3].Value; // Name

                s1 = Convert.ToString(row.Cells[4].Value); // F

                double frequency = 0;
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    frequency = Convert.ToDouble(s1);
                }
                catch (SystemException)
                {
                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        frequency = Convert.ToDouble(s1);
                    }
                    catch
                    {
                        MessageBox.Show("Incorrect data");
                        return;
                    }
                }

                // Значок
                var iconIndex = ob.indzn;

                lst[i] = new LF1
                {
                    FrequencyMhz = frequency,
                    indzn = iconIndex,
                    sType = name,

                    X_m = xx,
                    Y_m = yy,

                    Lat = x,
                    Long = y,

                    H_m = h
                };

            } // FOR

        }
        // ************************************************* Updata_List<LF1>_FromDataGridView

        // Update_DataGridView_From_GlobalVarLn.listJS ***************************************
        // Обновление DataGridView из листа GlobalVarLn.listJS
        // В 1-ые три колонки заносится номер СП, ее широта, долгота
        // Используется: Azimuth

        public static void f_Update_datgrid_listJS(
                                           DataGridView datgrid
                                           )
        {

            // ...............................................................................
            var stations = GlobalVarLn.listJS.ToList();
            // ...............................................................................

            for (var i = 0; i < stations.Count && i < datgrid.RowCount; ++i)
            {
                var row = datgrid.Rows[i];

                var pos = Mercator.ToLonLat(stations[i].CurrentPosition.x, stations[i].CurrentPosition.y);
                row.Cells[0].Value = stations[i].Name;
                row.Cells[1].Value = pos.Y.ToString("F3");
                row.Cells[2].Value = pos.X.ToString("F3");

            } // FOR
            // ...............................................................................

        }
        // *************************************** Update_DataGridView_From_GlobalVarLn.listJS

        // F_Close_Form **********************************************************************
        // Функция для автоматического закрытия формы при открытии другой формы

        public static void F_Close_Form(int fl)
        {
            // SP
            if ((fl != 1) && (GlobalVarLn.fl_Open_objFormSP == 1))
            {
                GlobalVarLn.objFormSPG.Hide();
                GlobalVarLn.fl_Open_objFormSP = 0;
                GlobalVarLn.blSP_stat = false;
            }

            // OB1
            if ((fl != 2) && (GlobalVarLn.fl_Open_objFormOB1 == 1))
            {
                GlobalVarLn.objFormOB1G.Hide();
                GlobalVarLn.fl_Open_objFormOB1 = 0;
                GlobalVarLn.blOB1_stat = false;
            }

            // OB2
            if ((fl != 3) && (GlobalVarLn.fl_Open_objFormOB2 == 1))
            {
                GlobalVarLn.objFormOB2G.Hide();
                GlobalVarLn.fl_Open_objFormOB2 = 0;
                GlobalVarLn.blOB2_stat = false;
            }

            // LF
            if ((fl != 4) && (GlobalVarLn.fl_Open_objFormLF == 1))
            {
                GlobalVarLn.objFormLFG.Hide();
                GlobalVarLn.fl_Open_objFormLF = 0;
                GlobalVarLn.blLF1_stat = false;
            }

            // LF2
            if ((fl != 5) && (GlobalVarLn.fl_Open_objFormLF2 == 1))
            {
                GlobalVarLn.objFormLF2G.Hide();
                GlobalVarLn.fl_Open_objFormLF2 = 0;
                GlobalVarLn.blLF2_stat = false;
            }

            // ZO
            if ((fl != 6) && (GlobalVarLn.fl_Open_objFormZO == 1))
            {
                GlobalVarLn.objFormZOG.Hide();
                GlobalVarLn.fl_Open_objFormZO = 0;
                GlobalVarLn.blZO_stat = false;
            }

            // TRO
            if ((fl != 7) && (GlobalVarLn.fl_Open_objFormTRO == 1))
            {
                GlobalVarLn.objFormTROG.Hide();
                GlobalVarLn.fl_Open_objFormTRO = 0;
                GlobalVarLn.blTRO_stat = false;
                GlobalVarLn.blSP_stat = false;
                GlobalVarLn.blLF1_stat = false;
                GlobalVarLn.blLF2_stat = false;
                GlobalVarLn.blZO_stat = false;
                GlobalVarLn.blOB1_stat = false;
                GlobalVarLn.blOB2_stat = false;

            }

            // Way
            if ((fl != 8) && (GlobalVarLn.fl_Open_objFormWay == 1))
            {
                GlobalVarLn.objFormWayG.Hide();
                GlobalVarLn.fl_Open_objFormWay = 0;
            }

            // Az1
            if ((fl != 9) && (GlobalVarLn.fl_Open_objFormAz1 == 1))
            {
                GlobalVarLn.objFormAz1G.Hide();
                GlobalVarLn.fl_Open_objFormAz1 = 0;
                GlobalVarLn.blOB_az1 = false;
            }

            // LineSightRange
            if ((fl != 10) && (GlobalVarLn.fl_Open_objFormLineSightRange == 1))
            {
                GlobalVarLn.objFormLineSightRangeG.Hide();
                GlobalVarLn.fl_Open_objFormLineSightRange = 0;
            }

            // Suppression
            if ((fl != 11) && (GlobalVarLn.fl_Open_objFormSuppression == 1))
            {
                GlobalVarLn.objFormSuppressionG.Hide();
                GlobalVarLn.fl_Open_objFormSuppression = 0;
            }

            // SupSpuf
            if ((fl != 12) && (GlobalVarLn.fl_Open_objFormSupSpuf == 1))
            {
                GlobalVarLn.objFormSupSpufG.Hide();
                GlobalVarLn.fl_Open_objFormSupSpuf = 0;
            }

            // Spuf
            if ((fl != 13) && (GlobalVarLn.fl_Open_objFormSpuf == 1))
            {
                GlobalVarLn.objFormSpufG.Hide();
                GlobalVarLn.fl_Open_objFormSpuf = 0;
            }

            // SPFB
            if ((fl != 14) && (GlobalVarLn.fl_Open_objFormSPFB == 1))
            {
                GlobalVarLn.objFormSPFBG.Hide();
                GlobalVarLn.fl_Open_objFormSPFB = 0;
                GlobalVarLn.flF_f1 = 0;
            }

            // SPFB1
            if ((fl != 15) && (GlobalVarLn.fl_Open_objFormSPFB1 == 1))
            {
                GlobalVarLn.objFormSPFB1G.Hide();
                GlobalVarLn.fl_Open_objFormSPFB1 = 0;
                GlobalVarLn.flF_f2 = 0;

            }

            // SPFB2
            if ((fl != 16) && (GlobalVarLn.fl_Open_objFormSPFB2 == 1))
            {
                GlobalVarLn.objFormSPFB2G.Hide();
                GlobalVarLn.fl_Open_objFormSPFB2 = 0;
                GlobalVarLn.flF_f3 = 0;
                GlobalVarLn.flF1_f3 = 0;
            }

            // Tab
            if ((fl != 17) && (GlobalVarLn.fl_Open_objFormTab == 1))
            {
                GlobalVarLn.objFormTabG.Hide();
                GlobalVarLn.fl_Open_objFormTab = 0;
            }

            // Screen
            if ((fl != 18) && (GlobalVarLn.fl_Open_objFormScreen == 1))
            {
                /*
                        GlobalVarLn.objFormScreenG.Hide();
                        GlobalVarLn.fl_Open_objFormScreen = 0;

                        // 0310
                        GlobalVarLn.objFormScreenG.pbScreen.Image = null;
                        GlobalVarLn.flScrM1 = 0;
                        GlobalVarLn.flScrM2 = 0;
                        GlobalVarLn.flScrM3 = 0;
                        GlobalVarLn.bmpScr = null;
                */
            }

        }
        // ********************************************************************** F_Close_Form



    } // Class
} // NameSpace
