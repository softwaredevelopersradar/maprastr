﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
using System.Windows.Input;

using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;
using Bitmap = System.Drawing.Bitmap;
using Point = System.Drawing.Point;

namespace GrozaMap
{
    class ClassMapRastrLoadForm
    {

        // Load_FormLightSightRange ********************************************************************
        // Загрузка ЗПВ 

        public static void f_Load_FormLightSightRange()
        {
            // -----------------------------------------------------------------------------------------
            GlobalVarLn.objFormLineSightRangeG.gbRect.Visible = true;

            GlobalVarLn.objFormLineSightRangeG.gbRect42.Visible = false;
            GlobalVarLn.objFormLineSightRangeG.gbRad.Visible = false;
            GlobalVarLn.objFormLineSightRangeG.gbDegMin.Visible = false;
            GlobalVarLn.objFormLineSightRangeG.gbDegMinSec.Visible = false;
            // -----------------------------------------------------------------------------------------
            GlobalVarLn.objFormLineSightRangeG.cbChooseSC.SelectedIndex = 0;
            GlobalVarLn.objFormLineSightRangeG.cbHeightOwnObject.SelectedIndex = 0;

            // Объект РП
            GlobalVarLn.objFormLineSightRangeG.cbHeightOpponObject.SelectedIndex = 0;

            GlobalVarLn.objFormLineSightRangeG.cbCenterLSR.SelectedIndex = 0;
            // -----------------------------------------------------------------------------------------
            GlobalVarLn.objFormLineSightRangeG.chbXY.Checked = false;
            // -----------------------------------------------------------------------------------------
            // Переменные

            GlobalVarLn.fl_LineSightRange = 0;
            GlobalVarLn.flCoordZPV = 0; // =1-> Выбрали центр ЗПВ
            // -----------------------------------------------------------------------------------------
            if (GlobalVarLn.listPointDSR.Count != 0)
                GlobalVarLn.listPointDSR.Clear();
            // -----------------------------------------------------------------------------------------
            if (GlobalVarLn.listDetectionZone.Count != 0)
                GlobalVarLn.listDetectionZone.Clear();
            // -----------------------------------------------------------------------------------------
            GlobalVarLn.NumbSP_lsr = "";
            // -----------------------------------------------------------------------------------------
            // Обновление SP

            //UpdateDropDownList();
            MapForm.UpdateDropDownList(GlobalVarLn.objFormLineSightRangeG.cbCenterLSR);
            // -----------------------------------------------------------------------------------------
            //GlobalVarLn.ListJSChangedEvent += OnListJSChanged;
            // -----------------------------------------------------------------------------------------

        }
        // ******************************************************************** Load_FormLightSightRange

        // Load_FormSupression *************************************************************************
        // Загрузка зоны подавления радиолиний управления (зона подавл.+зона неподавл.)

        public static void f_Load_FormSuppression()
        {
            // -----------------------------------------------------------------------------------------
            GlobalVarLn.objFormSuppressionG.cbOwnObject.SelectedIndex = 0;
            GlobalVarLn.objFormSuppressionG.cbChooseSC.SelectedIndex = 0;
            GlobalVarLn.objFormSuppressionG.cbCommChooseSC.SelectedIndex = 0;
            // Средство РП
            GlobalVarLn.objFormSuppressionG.cbHeightOwnObject.SelectedIndex = 0;
            // Object РП
            GlobalVarLn.objFormSuppressionG.cbPt1HeightOwnObject.SelectedIndex = 0;
            // Поляризация сигнала
            GlobalVarLn.objFormSuppressionG.cbPolarOpponent.SelectedIndex = 0;
            // -----------------------------------------------------------------------------------------
            GlobalVarLn.NumbSP_sup = "";
            // -----------------------------------------------------------------------------------------
            GlobalVarLn.objFormSuppressionG.gbOwnRect.Visible = true;
            GlobalVarLn.objFormSuppressionG.gbOwnRect.Location = new Point(8, 26);

            GlobalVarLn.objFormSuppressionG.gbOwnRect42.Visible = false;
            GlobalVarLn.objFormSuppressionG.gbOwnRad.Visible = false;
            GlobalVarLn.objFormSuppressionG.gbOwnDegMin.Visible = false;
            GlobalVarLn.objFormSuppressionG.gbOwnDegMinSec.Visible = false;
            // -----------------------------------------------------------------------------------------
            GlobalVarLn.objFormSuppressionG.gbPt1Rect.Visible = true;
            GlobalVarLn.objFormSuppressionG.gbPt1Rect.Location = new Point(6, 11);

            GlobalVarLn.objFormSuppressionG.gbPt1Rect42.Visible = false;
            GlobalVarLn.objFormSuppressionG.gbPt1Rad.Visible = false;
            GlobalVarLn.objFormSuppressionG.gbPt1DegMin.Visible = false;
            GlobalVarLn.objFormSuppressionG.gbPt1DegMinSec.Visible = false;
            // -----------------------------------------------------------------------------------------
            GlobalVarLn.objFormSuppressionG.chbXY.Checked = false;
            GlobalVarLn.objFormSuppressionG.chbXY1.Checked = false;
            // -----------------------------------------------------------------------------------------
            // Переменные

            GlobalVarLn.fl_Suppression = 0; // Отрисовка зоны
            GlobalVarLn.flCoordSP_sup = 0; // =1-> Выбрали СП
            GlobalVarLn.flCoordOP = 0;

            GlobalVarLn.flA_sup = 0;
            // -----------------------------------------------------------------------------------------
            if (GlobalVarLn.listControlJammingZone.Count != 0)
                GlobalVarLn.listControlJammingZone.Clear();
            // -----------------------------------------------------------------------------------------
            // Обновление SP

            MapForm.UpdateDropDownList(GlobalVarLn.objFormSuppressionG.cbOwnObject);
            // -----------------------------------------------------------------------------------------
            //GlobalVarLn.ListJSChangedEvent += OnListJSChanged;
            // -----------------------------------------------------------------------------------------

        }
        // ************************************************************************* Load_FormSupression

        // Load_FormSupSpuf ****************************************************************************
        // Загрузка зоны подавления навигации

        public static void f_Load_FormSupSpuf()
        {
            // -----------------------------------------------------------------------------------------
            GlobalVarLn.objFormSupSpufG.gbRect.Visible = true;
            GlobalVarLn.objFormSupSpufG.gbRect.Location = new Point(7, 30);
            // -----------------------------------------------------------------------------------------
            GlobalVarLn.objFormSupSpufG.cbCenterLSR.SelectedIndex = 0;
            GlobalVarLn.objFormSupSpufG.comboBox2.SelectedIndex = 1;
            // -----------------------------------------------------------------------------------------
            // Переменные

            GlobalVarLn.fl_supnav = 0;
            GlobalVarLn.flCoord_supnav = 0; // =1-> Выбрали центр ЗПВ
            // -----------------------------------------------------------------------------------------
            // ?????????????
            //P_supnav = 15;

            GlobalVarLn.objFormSupSpufG.tbOwnHeight.Text = "";
            GlobalVarLn.objFormSupSpufG.tbDistSightRange.Text = "";

            GlobalVarLn.NumbSP_supnav = "";
            // -----------------------------------------------------------------------------------------
            if (GlobalVarLn.listNavigationJammingZone.Count != 0)
                GlobalVarLn.listNavigationJammingZone.Clear();
            // -----------------------------------------------------------------------------------------
            // Обновление SP

            MapForm.UpdateDropDownList(GlobalVarLn.objFormSupSpufG.cbCenterLSR);
            // -----------------------------------------------------------------------------------------
            //GlobalVarLn.ListJSChangedEvent += OnListJSChanged;
            // -----------------------------------------------------------------------------------------

        }
        // **************************************************************************** Load_FormSupSpuf

        // Load_FormSpuf *******************************************************************************
        // Загрузка зоны спуфинга (+зона подавления навигации для зоны спуфинга)

        public static void f_Load_FormSpuf()
        {
            // -----------------------------------------------------------------------------------------
            GlobalVarLn.objFormSpufG.cbCenterLSR.SelectedIndex = 0;
            GlobalVarLn.objFormSpufG.comboBox2.SelectedIndex = 1;
            // -----------------------------------------------------------------------------------------
            GlobalVarLn.objFormSpufG.tbOwnHeight.Text = "";
            GlobalVarLn.objFormSpufG.tbR1.Text = "";
            GlobalVarLn.objFormSpufG.tbR2.Text = "";

            GlobalVarLn.NumbSP_spf = "";
            // -----------------------------------------------------------------------------------------
            GlobalVarLn.objFormSpufG.gbRect.Visible = true;
            GlobalVarLn.objFormSpufG.gbRect.Location = new Point(7, 30);

            GlobalVarLn.objFormSpufG.chbS.Checked = false;
            GlobalVarLn.objFormSpufG.chbJ.Checked = false;
            // -----------------------------------------------------------------------------------------
            // Переменные

            GlobalVarLn.fl_spf = 0;
            GlobalVarLn.flCoord_spf = 0; // =1-> Выбрали центр ЗПВ

            GlobalVarLn.flS_spf = 0;
            GlobalVarLn.flJ_spf = 0;
            // -----------------------------------------------------------------------------------------
            if (GlobalVarLn.listSpoofingZone.Count != 0)
                GlobalVarLn.listSpoofingZone.Clear();
            if (GlobalVarLn.listSpoofingJamZone.Count != 0)
                GlobalVarLn.listSpoofingJamZone.Clear();
            // -----------------------------------------------------------------------------------------
            // Обновление SP

            MapForm.UpdateDropDownList(GlobalVarLn.objFormSpufG.cbCenterLSR);
            // -----------------------------------------------------------------------------------------
            //GlobalVarLn.ListJSChangedEvent += OnListJSChanged;
            // -----------------------------------------------------------------------------------------

        }
        // ******************************************************************************* Load_FormSpuf

        // Load_FormSP *********************************************************************************
        // Загрузка окна ввода СП

        public static void f_Load_FormSP()
        {
            if (GlobalVarLn.flEndTRO_stat != 1)
            {
                // -----------------------------------------------------------------------------------------
                GlobalVarLn.objFormSPG.cbChooseSC.SelectedIndex = 0;
                // -----------------------------------------------------------------------------------------
                GlobalVarLn.objFormSPG.chbXY.Checked = false;
                // -----------------------------------------------------------------------------------------
                GlobalVarLn.blSP_stat = true;
                GlobalVarLn.flEndSP_stat = 1;

                GlobalVarLn.XCenter_SP = 0;
                GlobalVarLn.YCenter_SP = 0;
                GlobalVarLn.HCenter_SP = 0;
                GlobalVarLn.flCoord_SP2 = 0;

                GlobalVarLn.iZSP = 0;
                // -----------------------------------------------------------------------------------------
                GlobalVarLn.objFormSPG.pbSP.BackgroundImage = GlobalVarLn.objFormSPG.imageList1.Images[0];
                // -----------------------------------------------------------------------------------------
            }

        }
        // ********************************************************************************* Load_FormSP

        // Load_FormOB1 ********************************************************************************
        // Загрузка окна ввода объектов своих войск

        public static void f_Load_FormOB1()
        {
            if (GlobalVarLn.flEndTRO_stat != 1)
            {
            // -----------------------------------------------------------------------------------------
            // Очистка dataGridView1+ Установка 100 строк

                while (GlobalVarLn.objFormOB1G.dataGridView1.Rows.Count != 0)
                    GlobalVarLn.objFormOB1G.dataGridView1.Rows.Remove(GlobalVarLn.objFormOB1G.dataGridView1.Rows[GlobalVarLn.objFormOB1G.dataGridView1.Rows.Count - 1]);

                for (int iyu = 0; iyu < GlobalVarLn.sizeDatOB1_stat; iyu++)
                {
                    GlobalVarLn.objFormOB1G.dataGridView1.Rows.Add("", "", "", "");
                }
                // -------------------------------------------------------------------------------------
                // Флаги

                GlobalVarLn.blOB1_stat = true;
                GlobalVarLn.flEndOB1_stat = 1;
                // -------------------------------------------------------------------------------------
                GlobalVarLn.iOB1_stat = 0;
                GlobalVarLn.X_OB1 = 0;
                GlobalVarLn.Y_OB1 = 0;
                GlobalVarLn.H_OB1 = 0;

                GlobalVarLn.list_OB1.Clear();
                GlobalVarLn.list1_OB1.Clear();

                GlobalVarLn.iZOB1 = 0;
                // -------------------------------------------------------------------------------------
                GlobalVarLn.iZOB1 = 0;
                GlobalVarLn.objFormOB1G.pbOB1.BackgroundImage = GlobalVarLn.objFormOB1G.imageList1.Images[0];
                // -------------------------------------------------------------------------------------

            }

        }
        // ******************************************************************************** Load_FormOB1

        // Load_FormOB2 ********************************************************************************
        // Загрузка окна ввода объектов войск противника

        public static void f_Load_FormOB2()
        {
            if (GlobalVarLn.flEndTRO_stat != 1)
            {
                // .....................................................................................
                // Очистка dataGridView

                // Очистка dataGridView1
                while (GlobalVarLn.objFormOB2G.dataGridView1.Rows.Count != 0)
                    GlobalVarLn.objFormOB2G.dataGridView1.Rows.Remove(GlobalVarLn.objFormOB2G.dataGridView1.Rows[GlobalVarLn.objFormOB2G.dataGridView1.Rows.Count - 1]);

                GlobalVarLn.objFormOB2G.dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDatOB2_stat; i++)
                {
                    GlobalVarLn.objFormOB2G.dataGridView1.Rows.Add("", "", "", "", "");
                }
                // .....................................................................................
                // Флаги

                GlobalVarLn.blOB2_stat = true;
                GlobalVarLn.flEndOB2_stat = 1;
                // .....................................................................................
                GlobalVarLn.X_OB2 = 0;
                GlobalVarLn.Y_OB2 = 0;
                GlobalVarLn.H_OB2 = 0;

                GlobalVarLn.list_OB2.Clear();
                GlobalVarLn.list1_OB2.Clear();
                // .....................................................................................
                GlobalVarLn.iZOB2 = 0;

                GlobalVarLn.objFormOB2G.pbOB2.BackgroundImage = GlobalVarLn.objFormOB2G.imageList1.Images[0];

            }

            // Обновление DataGridView из листа структур типа LF1
            ClassMapRastrDopFunctions.f_Update_datgrid_LF1(
                                                           GlobalVarLn.objFormOB2G.dataGridView1,
                                                           GlobalVarLn.list1_OB2,
                                                           GlobalVarLn.sizeDatOB2_stat
                                                           );

        }
        // ******************************************************************************** Load_FormOB2

        // Load_FormLF1 ********************************************************************************
        // Загрузка окна ввода линии фронта своих войск

        public static void f_Load_FormLF1()
        {
            if (GlobalVarLn.flEndTRO_stat != 1)
            {
                // .....................................................................................
                // Очистка dataGridView

                GlobalVarLn.objFormLFG.dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDatLF1_stat; i++)
                {
                    GlobalVarLn.objFormLFG.dataGridView1.Rows.Add("", "", "");
                }

                // .....................................................................................
                // Флаги

                GlobalVarLn.blLF1_stat = true;
                GlobalVarLn.flEndLF1_stat = 1;
                // .....................................................................................
                GlobalVarLn.iLF1_stat = 0;
                GlobalVarLn.X_LF1 = 0;
                GlobalVarLn.Y_LF1 = 0;
                GlobalVarLn.H_LF1 = 0;
                // .....................................................................................
                GlobalVarLn.list_LF1.Clear();
                // .....................................................................................
            }

        }
        // ******************************************************************************** Load_FormLF1

        // Load_FormLF2 ********************************************************************************
        // Загрузка окна ввода линии фронта войск противника

        public static void f_Load_FormLF2()
        {
            if (GlobalVarLn.flEndTRO_stat != 1)
            {
                // .....................................................................................
                // Очистка dataGridView

                GlobalVarLn.objFormLF2G.dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDatLF2_stat; i++)
                {
                    GlobalVarLn.objFormLF2G.dataGridView1.Rows.Add("", "", "");
                }
                // .....................................................................................
                // Флаги

                GlobalVarLn.blLF2_stat = true;
                GlobalVarLn.flEndLF2_stat = 1;
                // .....................................................................................
                GlobalVarLn.iLF2_stat = 0;
                GlobalVarLn.X_LF2 = 0;
                GlobalVarLn.Y_LF2 = 0;
                GlobalVarLn.H_LF2 = 0;
                // .....................................................................................
                GlobalVarLn.list_LF2.Clear();
                // .....................................................................................
            }

        }
        // ******************************************************************************** Load_FormLF2

        // Load_FormZO *********************************************************************************
        // Загрузка окна ввода зоны ответственности

        public static void f_Load_FormZO()
        {
            if (GlobalVarLn.flEndTRO_stat != 1)
            {
                // .....................................................................................
                // Очистка dataGridView

                GlobalVarLn.objFormZOG.dataGridView1.ClearSelection();
                for (int i = 0; i < GlobalVarLn.sizeDatZO_stat; i++)
                {
                    GlobalVarLn.objFormZOG.dataGridView1.Rows.Add("", "", "");
                }
                // .....................................................................................
                // Флаги

                GlobalVarLn.blZO_stat = true;
                GlobalVarLn.flEndZO_stat = 1;
                // .....................................................................................
                GlobalVarLn.iZO_stat = 0;
                GlobalVarLn.X_ZO = 0;
                GlobalVarLn.Y_ZO = 0;
                GlobalVarLn.H_ZO = 0;
                // .....................................................................................
                GlobalVarLn.list_ZO.Clear();
                // .....................................................................................
            }

        }
        // ********************************************************************************* Load_FormZO

        // Load_FormTRO ********************************************************************************
        // Загрузка окна ввода тактической и радиоэлектронной обстановки

        public static void f_Load_FormTRO()
        {
            // .........................................................................................
            // TRO
            GlobalVarLn.blTRO_stat = true;
            GlobalVarLn.flEndTRO_stat = 1;
            // .........................................................................................
            // SP

            GlobalVarLn.objFormSPG.gbRect42.Visible = false;
            GlobalVarLn.objFormSPG.gbRad.Visible = false;
            GlobalVarLn.objFormSPG.gbDegMin.Visible = false;
            GlobalVarLn.objFormSPG.gbDegMinSec.Visible = false;
            GlobalVarLn.objFormSPG.cbChooseSC.SelectedIndex = 0;
            GlobalVarLn.objFormSPG.chbXY.Checked = false;

            GlobalVarLn.blSP_stat = true;
            GlobalVarLn.flEndSP_stat = 1;
            GlobalVarLn.XCenter_SP = 0;
            GlobalVarLn.YCenter_SP = 0;
            GlobalVarLn.HCenter_SP = 0;

            GlobalVarLn.ClearListJS();
            // .........................................................................................
            // LF1

            GlobalVarLn.objFormLFG.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatLF1_stat; i++)
            {
                GlobalVarLn.objFormLFG.dataGridView1.Rows.Add("", "", "");
            }
            GlobalVarLn.blLF1_stat = true;
            GlobalVarLn.flEndLF1_stat = 1;
            GlobalVarLn.iLF1_stat = 0;
            GlobalVarLn.X_LF1 = 0;
            GlobalVarLn.Y_LF1 = 0;
            GlobalVarLn.H_LF1 = 0;

            GlobalVarLn.list_LF1.Clear();
            // .........................................................................................
            // LF2

            GlobalVarLn.objFormLF2G.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatLF2_stat; i++)
            {
                GlobalVarLn.objFormLF2G.dataGridView1.Rows.Add("", "", "");
            }
            GlobalVarLn.blLF2_stat = true;
            GlobalVarLn.flEndLF2_stat = 1;
            GlobalVarLn.iLF2_stat = 0;
            GlobalVarLn.X_LF2 = 0;
            GlobalVarLn.Y_LF2 = 0;
            GlobalVarLn.H_LF2 = 0;

            GlobalVarLn.list_LF2.Clear();
            // .........................................................................................
            // ZO

            GlobalVarLn.objFormZOG.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatZO_stat; i++)
            {
                GlobalVarLn.objFormZOG.dataGridView1.Rows.Add("", "", "");
            }
            GlobalVarLn.blZO_stat = true;
            GlobalVarLn.flEndZO_stat = 1;
            GlobalVarLn.iZO_stat = 0;
            GlobalVarLn.X_ZO = 0;
            GlobalVarLn.Y_ZO = 0;
            GlobalVarLn.H_ZO = 0;

            GlobalVarLn.list_ZO.Clear();
            // .........................................................................................
            // OB1

            GlobalVarLn.objFormOB1G.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatOB1_stat; i++)
            {
                GlobalVarLn.objFormOB1G.dataGridView1.Rows.Add("", "", "", "");
            }
            GlobalVarLn.blOB1_stat = true;
            GlobalVarLn.flEndOB1_stat = 1;
            GlobalVarLn.iOB1_stat = 0;
            GlobalVarLn.X_OB1 = 0;
            GlobalVarLn.Y_OB1 = 0;
            GlobalVarLn.H_OB1 = 0;

            GlobalVarLn.list_OB1.Clear();
            GlobalVarLn.list1_OB1.Clear();
            // .........................................................................................
            // OB2

            GlobalVarLn.objFormOB2G.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatOB2_stat; i++)
            {
                GlobalVarLn.objFormOB2G.dataGridView1.Rows.Add("", "", "", "","");
            }
            GlobalVarLn.blOB2_stat = true;
            GlobalVarLn.flEndOB2_stat = 1;
            GlobalVarLn.X_OB2 = 0;
            GlobalVarLn.Y_OB2 = 0;
            GlobalVarLn.H_OB2 = 0;

            GlobalVarLn.list_OB2.Clear();
            GlobalVarLn.list1_OB2.Clear();
            // .........................................................................................

        }
        // ******************************************************************************** Load_FormTRO

        // Load_FormWay ********************************************************************************
        // Загрузка окна ввода маршрута

        public static void f_Load_FormWay()
        {
            // .........................................................................................
            // Очистка dataGridView

            GlobalVarLn.objFormWayG.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatWay_stat; i++)
            {
                GlobalVarLn.objFormWayG.dataGridView1.Rows.Add("", "", "");
            }
            // .........................................................................................
            GlobalVarLn.objFormWayG.chbWay.Checked = true;
            if (GlobalVarLn.objFormWayG.chbWay.Checked == true)
            {
                GlobalVarLn.blWay_stat = true;
                GlobalVarLn.flEndWay_stat = 1;
            }

            GlobalVarLn.list_way.Clear();

            GlobalVarLn.iW_stat = 0;
            GlobalVarLn.X_StartW_stat = 0;
            GlobalVarLn.Y_StartW_stat = 0;
            GlobalVarLn.LW_stat = 0;
            // .........................................................................................

        }
        // ******************************************************************************** Load_FormWay

        // Load_FormAzimuth ****************************************************************************
        // Загрузка окна ввода маршрута

        public static void f_Load_FormAzimuth()
        {
            // .........................................................................................
            GlobalVarLn.objFormAz1G.gbRect.Visible = true;
            // .........................................................................................
            // Очистка dataGridView

            GlobalVarLn.objFormAz1G.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatOB_az1; i++)
            {
                GlobalVarLn.objFormAz1G.dataGridView1.Rows.Add("", "", "", "");
            }
            // .........................................................................................
            GlobalVarLn.blOB_az1 = true;
            GlobalVarLn.flEndOB_az1 = 1;
            GlobalVarLn.flCoordOB_az1 = 0;
            GlobalVarLn.XCenter_az1 = 0;
            GlobalVarLn.YCenter_az1 = 0;
            GlobalVarLn.HCenter_az1 = 0;
            // .........................................................................................
            ClassMapRastrDopFunctions.f_Update_datgrid_listJS
                                                             (
                                                             GlobalVarLn.objFormAz1G.dataGridView1
                                                             );
            // .........................................................................................

        }
        // **************************************************************************** Load_FormAzimuth

        // Load_FormSPFB *******************************************************************************
        // Загрузка окна ввода диапазонов частот и секторов радиоразведки

        public static void f_Load_FormSPFB()
        {
            // .........................................................................................
            GlobalVarLn.iSP_f1 = 0;
            // .........................................................................................
            GlobalVarLn.objFormSPFBG.cbChooseSC.SelectedIndex = 0;
            GlobalVarLn.NumbSP_f1 = Convert.ToString(GlobalVarLn.objFormSPFBG.cbChooseSC.Items[GlobalVarLn.objFormSPFBG.cbChooseSC.SelectedIndex]);
            // .........................................................................................
            // Очистка dataGridView

            GlobalVarLn.objFormSPFBG.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDat_f1; i++)
            {
                GlobalVarLn.objFormSPFBG.dataGridView1.Rows.Add("", "", "", "");
            }
            // .........................................................................................
            GlobalVarLn.flF_f1 = 1;
            GlobalVarLn.Az1_f1 = 0;
            GlobalVarLn.Az2_f1 = 0;
            GlobalVarLn.Az_f1 = 0;
            GlobalVarLn.flAz1_f1 = 0;
            GlobalVarLn.flAz2_f1 = 0;
            // .........................................................................................
            GlobalVarLn.list1_f1.Clear();
            // .........................................................................................

        }
        // ******************************************************************************* Load_FormSPFB

        // Load_FormSPFB1 ******************************************************************************
        // Загрузка окна ввода диапазонов частот и секторов радиоподавления

        public static void f_Load_FormSPFB1()
        {
            // .........................................................................................
            GlobalVarLn.iSP_f2 = 0;
            // .........................................................................................
            GlobalVarLn.objFormSPFB1G.cbChooseSC.SelectedIndex = 0;
            GlobalVarLn.NumbSP_f2 = Convert.ToString(GlobalVarLn.objFormSPFB1G.cbChooseSC.Items[GlobalVarLn.objFormSPFB1G.cbChooseSC.SelectedIndex]);
            // .........................................................................................
            // Очистка dataGridView

            GlobalVarLn.objFormSPFB1G.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDat_f2; i++)
            {
                GlobalVarLn.objFormSPFB1G.dataGridView1.Rows.Add("", "", "", "");
            }
            // .........................................................................................
            GlobalVarLn.flF_f2 = 1;
            GlobalVarLn.Az1_f2 = 0;
            GlobalVarLn.Az2_f2 = 0;
            GlobalVarLn.Az_f2 = 0;
            GlobalVarLn.flAz1_f2 = 0;
            GlobalVarLn.flAz2_f2 = 0;
            // .........................................................................................
            GlobalVarLn.list1_f2.Clear();
            // .........................................................................................

        }
        // ****************************************************************************** Load_FormSPFB1

        // Load_FormSPFB2 ******************************************************************************
        // Загрузка окна ввода запрещенных для подавления частот и частот особого внимания

        public static void f_Load_FormSPFB2()
        {
            // .........................................................................................
            GlobalVarLn.iSP_f3 = 0;
            // .........................................................................................
            GlobalVarLn.objFormSPFB2G.cbChooseSC.SelectedIndex = 0;
            GlobalVarLn.NumbSP_f3 = Convert.ToString(GlobalVarLn.objFormSPFB2G.cbChooseSC.Items[GlobalVarLn.objFormSPFB2G.cbChooseSC.SelectedIndex]);
            // .........................................................................................
            // Очистка dataGridView

            GlobalVarLn.objFormSPFB2G.dgvFreqForbid.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDat_f3; i++)
            {
                GlobalVarLn.objFormSPFB2G.dgvFreqForbid.Rows.Add("", "");
            }

            GlobalVarLn.objFormSPFB2G.dgvFreqSpec.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDat_f3; i++)
            {
                GlobalVarLn.objFormSPFB2G.dgvFreqSpec.Rows.Add("", "");
            }
            // .........................................................................................
            GlobalVarLn.flF_f3 = 1;
            GlobalVarLn.flF1_f3 = 1;
            // .........................................................................................
            GlobalVarLn.list1_f3.Clear();
            GlobalVarLn.list2_f3.Clear();
            // .........................................................................................

        }
        // ****************************************************************************** Load_FormSPFB2

        // Load_FormTab ********************************************************************************
        // Принять таблицу от оператора

        public static void f_Load_FormTab()
        {
            GlobalVarLn.fllTab = 0;
            GlobalVarLn.list_air_tab.Clear();
        }
        // ******************************************************************************** Load_FormTab



    } // Class
} // NameSpace
