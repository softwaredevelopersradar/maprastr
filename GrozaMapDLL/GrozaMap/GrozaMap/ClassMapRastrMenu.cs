﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
using System.Windows.Input;

using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;
using Bitmap = System.Drawing.Bitmap;
using Point = System.Drawing.Point;

namespace GrozaMap
{
    class ClassMapRastrMenu
    {
        // OpenMap ***************************************************************************
        // Открыть карту при загрузке карты (путь - в Setting.ini)

        public static string f_OpenMap(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     RasterMapControl objRasterMapControl,
                                     ref bool MapOpened
                                    )
        {
            string pathMap = "";

            if (System.IO.File.Exists(Application.StartupPath + "\\Setting.ini"))
            {
                //pathMap = Application.StartupPath + iniRW.get_map_path();
                // Полный путь
                pathMap = iniRW.get_map_path();

                try
                {
                    objRasterMapControl.OpenMap(pathMap);
                    MapOpened = true;
                    return "";
                }
                catch (Exception ex)
                {
                    MapOpened = false;
                    return "Can't load map";
                }

            }
            else
            {
                MapOpened = false;
                return "Can’t open map (no file INI)";
            }
        }
        // *************************************************************************** OpenMap

        // OpenMatrix ************************************************************************
        // Открыть матрицу высот при загрузке карты (путь - в Setting.ini)

        public static string f_OpenMatrix(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     RasterMapControl objRasterMapControl,
                                     ref bool MatrixOpened
                                       )
        {

            string pathMatrix = "";

            if (System.IO.File.Exists(Application.StartupPath + "\\Setting.ini"))
            {
                //pathMatrix = Application.StartupPath + iniRW.get_matrix_path();
                // Полный путь
                pathMatrix = iniRW.get_matrix_path();

                try
                {
                    objRasterMapControl.OpenDted(pathMatrix);
                    MatrixOpened = true;
                    return "";
                }
                catch (Exception ex)
                {
                    MatrixOpened = false;
                    return "Can't load the height matrix";
                }

            }
            else
            {
                MatrixOpened = false;
                return "Can’t open the height matrix (no file INI)";
            }

        }
        //************************************************************************  OpenMatrix

        // GetResolution *********************************************************************
        // Установить текущий масштаб с Setting ini при загрузке карты

        public static string f_GetResolution(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     RasterMapControl objRasterMapControl
                                            )
        {
            MapForm.RasterMapControl.Resolution = (double)iniRW.get_scl();

            if (System.IO.File.Exists(Application.StartupPath + "\\Setting.ini"))
            {
                try
                {
                    objRasterMapControl.Resolution = (double)iniRW.get_scl();
                    return "";
                }
                catch (Exception ex)
                {
                    return "Can't get scale";
                }

            }
            else
            {
                return "Can’t open file INI";
            }

        }
        // ********************************************************************* GetResolution

        // CloseMapMenu **********************************************************************
        // Закрыть карту и матрицу высот -> пункт меню

        public static string f_CloseMapMenu(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     RasterMapControl objRasterMapControl,
                                     ref bool MapOpened,
                                     ref bool MatrixOpened
                                         )
        {
            // ...............................................................................
            if (MapOpened == true)
            {
                objRasterMapControl.CloseMap();
                MapOpened = false;
            }
            // ...............................................................................
            if (MatrixOpened == true)
            {
                objRasterMapControl.CloseDted();
                MatrixOpened = false;
            }
            // ...............................................................................
            // Сохранить текущий масштаб в Setting.ini

            if (System.IO.File.Exists(Application.StartupPath + "\\Setting.ini"))
            {
                try
                {
                    // Сохранить текущий масштаб в Setting.ini
                    iniRW.write_map_scl((int)objRasterMapControl.Resolution);
                    return "";
                }
                catch (Exception ex)
                {
                    return "Can't save scale";
                }

            }
            else
            {
                return "Can't save scale (no file INI)";
            }
            // ...............................................................................

        }
        // ********************************************************************** CloseMapMenu

        // OpenMapMenu ***********************************************************************
        // Открыть карту -> пункт меню

        public static string f_OpenMapMenu(
                                         // Путь к карте
                                         string path,
                                         // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                         RasterMapControl objRasterMapControl,
                                         ref bool MapOpened
                                          )
        {

            // -----------------------------------------------------------------
            int length = 0;
            int length1 = 0;
            int indStart = 0;
            string s = "";
            string s1 = "";

            s = path;
            length = path.Length;
            if (length<5)
                return "Invalid map format";

            length1 = length-4;
            s1 = s.Remove(indStart, length1);

            if ((s1 != ".tif") && (s1 != ".IMG") && (s1 != ".JP2"))
                return "Invalid map format";
            // -----------------------------------------------------------------
            if (MapOpened == false)
            {
               try
               {
                    objRasterMapControl.OpenMap(path);

                    // ...............................................................................
                    // Сохранить путь

                    if (System.IO.File.Exists(Application.StartupPath + "\\Setting.ini"))
                    {
                        try
                        {
                            iniRW.set_map_path(path);
                            MapOpened = true;
                            return "";
                        }
                        catch (Exception ex)
                        {
                            return "Can't save path";
                        }
                    }
                    else
                    {
                        return "Can't save path (no file INI)";
                    }
                    // ...............................................................................

               }
               catch (Exception ex)
               {
                return "Can't load map";
               }

            } // MapOpened=false
              // -----------------------------------------------------------------
            else
            {
                return "The map is already open";

            } // MapOpened=true
            // -----------------------------------------------------------------

        }
        // *********************************************************************** OpenMapMenu

        // OpenMatrixMenu ********************************************************************
        // Открыть матрицу высот -> пункт меню

        public static string f_OpenMatrixMenu(
                                         // Путь к матрице высот 
                                         string path,
                                         // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                         RasterMapControl objRasterMapControl,
                                         ref bool MatrixOpened
                                             )
        {

            // -----------------------------------------------------------------
            int length = 0;
            int length1 = 0;
            int indStart = 0;
            string s = "";
            string s1 = "";

            s = path;
            length = path.Length;
            if (length < 5)
                return "Invalid Height Matrix format";

            length1 = length - 4;
            s1 = s.Remove(indStart, length1);

            if ((s1 != ".dt0") && (s1 != ".dt1") && (s1 != ".dt2"))
                return "Invalid Height Matrix format";
            // -----------------------------------------------------------------
            if (MatrixOpened == false)
            {
                try
                {
                    objRasterMapControl.OpenDted(path);

                    // ...............................................................................
                    // Сохранить путь

                    if (System.IO.File.Exists(Application.StartupPath + "\\Setting.ini"))
                    {
                        try
                        {
                            iniRW.set_matrix_path(path);
                            MatrixOpened = true;
                            return "";
                        }
                        catch (Exception ex)
                        {
                            return "Can't save path";
                        }
                    }
                    else
                    {
                        return "Can't save path (no file INI)";
                    }
                    // ...............................................................................

                }
                catch (Exception ex)
                {
                    return "Can't load the height matrix";
                }

            } // MatrixOpened=false
              // -----------------------------------------------------------------

            else
            {
                return "The height matrix is already open";

            } // MatrixOpened=true
            // -----------------------------------------------------------------

        }
        // ******************************************************************** OpenMatrixMenu

        // ResolutionBase ********************************************************************
        // Исходный масштаб

        public static void f_ResolutionBase(
                                            // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                            RasterMapControl objRasterMapControl
                                           )
        {
            objRasterMapControl.Resolution = 43942;

        }
        // ******************************************************************** ResolutionBase

        // ResolutionIncrease ****************************************************************
        // Увеличить масштаб

        public static void f_ResolutionIncrease(
                                            // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                            RasterMapControl objRasterMapControl
                                               )
        {

            objRasterMapControl.Resolution = objRasterMapControl.Resolution - 10000;

            if (objRasterMapControl.Resolution <= objRasterMapControl.MinResolution)
                objRasterMapControl.Resolution = objRasterMapControl.MinResolution;
            else if (objRasterMapControl.Resolution == objRasterMapControl.MaxResolution)
                objRasterMapControl.Resolution = 73942;

        }
        // **************************************************************** ResolutionIncrease

        // ResolutionDecrease ****************************************************************
        // Уменьшить масштаб

        public static void f_ResolutionDecrease(
                                            // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                            RasterMapControl objRasterMapControl
                                               )
        {
            objRasterMapControl.Resolution = objRasterMapControl.Resolution + 10000;

            if (objRasterMapControl.Resolution >= objRasterMapControl.MaxResolution)
                objRasterMapControl.Resolution = objRasterMapControl.MaxResolution;
            else if (objRasterMapControl.Resolution == objRasterMapControl.MinResolution)
                objRasterMapControl.Resolution = 13942;

        }
        // **************************************************************** ResolutionDecrease

        // CenterMapTo ***********************************************************************
        // Сцентрировать карту на определенную позицию

        public static string f_CenterMapTo(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     RasterMapControl objRasterMapControl,
                                     // Меркатор
                                     double x,
                                     double y
                                         )
        {
            try
            {
                double lat;
                double lon;
                Mapsui.Geometries.Point g = new Mapsui.Geometries.Point();

                //var p = Mercator.ToLonLat(GlobalVarLn.listJS[0].CurrentPosition.x, GlobalVarLn.listJS[0].CurrentPosition.y);
                var p = Mercator.ToLonLat(x, y);
                lat = p.Y;
                lon = p.X;

                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // 555
                // Если карта - Меркатор

                if (GlobalVarLn.TypeMap == 0)
                {
                    var pp = Mercator.FromLonLat(lon, lat);
                    lon = pp.X;
                    lat = pp.Y;
                }
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


                g.X = lon;
                g.Y = lat;
                objRasterMapControl.NavigateTo(g);
                return "";
            }
            catch
            {
                return "Can't go to this position";
            }

        }
        // *********************************************************************** CenterMapTo


    } // Class
} // Namespace
