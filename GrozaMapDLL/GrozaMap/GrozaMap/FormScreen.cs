﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Collections.Generic;

using System.IO;
using System.Collections.Generic;

using System.Windows.Input;
namespace GrozaMap
{
    public partial class FormScreen : Form
    {
        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();


        private MapForm mapForm;

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        private double dchislo;
        private long ichislo;
        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // Конструктор *********************************************************** 

        //public FormScreen(ref AxaxcMapScreen axaxcMapScreen1)
        public FormScreen(MapForm mapF)

        {
            InitializeComponent();

            mapForm = mapF;

            dchislo = 0;
            ichislo = 0;
        } // Конструктор
        // ***********************************************************  Конструктор

        // ************************************************************************
        // Загрузка формы
        // ************************************************************************
        private void FormScreen_Load(object sender, EventArgs e)
        {
/*
            GlobalVarLn.flScr = 0;
            GlobalVarLn.flScrM1 = 0;
            GlobalVarLn.flScrM2 = 0;
            GlobalVarLn.flScrM3 = 0;
*/
        } // Load
        // ************************************************************************


        private void bScreenShort_Click(object sender, EventArgs e)
        {

            //GlobalVarLn.flScrM1 = 1;

        } // 

        // **************************************************************************
        // Save JPEG
        // **************************************************************************
        private void button2_Click(object sender, EventArgs e)
        {
/*
          mapForm.saveFileDialog1.Filter = "JPEG image|*.jpeg|All files|*.*";
          mapForm.saveFileDialog1.Title = "Save captured screen as jpeg";
          if (mapForm.saveFileDialog1.ShowDialog() == DialogResult.OK)
          {

              using (MemoryStream memory = new MemoryStream())
              {
                  using (FileStream fs = new FileStream(mapForm.saveFileDialog1.FileName, FileMode.Create, FileAccess.ReadWrite))
                  {
                      pbScreen.Image.Save(memory, ImageFormat.Jpeg);
                      byte[] bytes = memory.ToArray();
                      fs.Write(bytes, 0, bytes.Length);
                      fs.Close();

                  }
              }

          } // IF(Dialog==OK)
          else
          {
              return;
          }
*/
        } // Save JPEG

        // **************************************************************************
        // Save BMP
        // **************************************************************************
        private void button4_Click(object sender, EventArgs e)
        {
/*
            mapForm.saveFileDialog1.Filter = "BMP image|*.bmp|All files|*.*";
            mapForm.saveFileDialog1.Title = "Save captured screen as bmp";
            if (mapForm.saveFileDialog1.ShowDialog() == DialogResult.OK)
            {

                using (MemoryStream memory = new MemoryStream())
                {
                    using (FileStream fs = new FileStream(mapForm.saveFileDialog1.FileName, FileMode.Create, FileAccess.ReadWrite))
                    {
                        pbScreen.Image.Save(memory, ImageFormat.Bmp);
                        byte[] bytes = memory.ToArray();
                        fs.Write(bytes, 0, bytes.Length);
                        fs.Close();

                    }
                }

            } // IF(Dialog==OK)
            else
            {
               // MessageBox.Show("Can't save file");
                return;
            }
*/
        } // Save BMP
        // **************************************************************************

        // **************************************************************************
        // Save PNG
        // **************************************************************************
        private void button6_Click(object sender, EventArgs e)
        {
/*
            mapForm.saveFileDialog1.Filter = "PNG image|*.png|All files|*.*";
            mapForm.saveFileDialog1.Title = "Save captured screen as png";
            if (mapForm.saveFileDialog1.ShowDialog() == DialogResult.OK)
            {

                using (MemoryStream memory = new MemoryStream())
                {
                    using (FileStream fs = new FileStream(mapForm.saveFileDialog1.FileName, FileMode.Create, FileAccess.ReadWrite))
                    {
                        pbScreen.Image.Save(memory, ImageFormat.Png);
                        byte[] bytes = memory.ToArray();
                        fs.Write(bytes, 0, bytes.Length);
                        fs.Close();

                    }
                }

            } // IF(Dialog==OK)
            else
            {
                return;
            }
*/
        } // Save PNG
        // **************************************************************************


        // **************************************************************************
        // Read JPEG
        // **************************************************************************
        private void button3_Click(object sender, EventArgs e)
        {
/*
            mapForm.openFileDialog1.Filter = "JPEG image|*.jpeg|All files|*.*";
            mapForm.openFileDialog1.Title = "Read captured screen as jpeg";
            if (mapForm.openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                    pbScreen.Image = Image.FromFile(mapForm.openFileDialog1.FileName);

            } // IF(Dialog==OK)

            else
            {
                //MessageBox.Show("Can't read file");
                return;
            }

*/
        } // Read JPEG
        // ************************************************************************
        // Read BMP
        // **************************************************************************

        private void button5_Click(object sender, EventArgs e)
        {
/*
            mapForm.openFileDialog1.Filter = "BMP image|*.bmp|All files|*.*";
            mapForm.openFileDialog1.Title = "Read captured screen as bmp";
            if (mapForm.openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pbScreen.Image = Image.FromFile(mapForm.openFileDialog1.FileName);

            } // IF(Dialog==OK)

            else
            {
                //MessageBox.Show("Can't read file");
                return;
            }
*/
        } // Read BMP
        // **************************************************************************

        // ************************************************************************
        // Read PNG
        // **************************************************************************
        private void button7_Click(object sender, EventArgs e)
        {
/*  
            mapForm.openFileDialog1.Filter = "PNG image|*.png|BMP image|*.bmp|JPEG image|*.jpeg|All files|*.*";
  
            if (mapForm.openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    pbScreen.Image = Image.FromFile(mapForm.openFileDialog1.FileName);
                }
                catch
                {
                    MessageBox.Show("Can't read file");
                    return;
                }

            } // IF(Dialog==OK)

            else
            {
                return;
            }
*/
        } // Read PNG
        // **************************************************************************

        // ************************************************************************
//0217
/*
        /// <summary>
        /// Сделать снимок основного экрана
        /// </summary>
        /// <returns>Возвращает снимок с основного экрана размером с текущее разрешение экрана</returns>
        public Bitmap ImageFromScreen(AxaxcMapScreen ppp)
        {
            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            double x1 = 0;
            double y1 = 0;
            double x2 = 0;
            double y2 = 0;
            double wd = 0;
            double ht = 0;
            double lft = 0;
            double tp = 0;

            x1 = GlobalVarLn.X1Scr;
            y1 = GlobalVarLn.Y1Scr;
            x2 = GlobalVarLn.X2Scr;
            y2 = GlobalVarLn.Y2Scr;

            // Расстояние в м на карте -> пикселы на изображении
            mapPlaneToPicture(GlobalVarLn.hmapl, ref x1, ref y1);
            mapPlaneToPicture(GlobalVarLn.hmapl, ref x2, ref y2);

            if (x2 >= x1) wd = x2 - x1;
            else ht = x1 - x2;
            if (y2 >= y1) ht = y2 - y1;
            else wd = y1 - y2;

            Point point1 = new Point((int)x1 - GlobalVarLn.axMapScreenGlobal.MapLeft,
                                     (int)y1 - GlobalVarLn.axMapScreenGlobal.MapTop 
                                    );

            Bitmap bmp = new Bitmap(
                                    (int)wd, 
                                    (int)ht,
                                    //250,
                                    //200,
                                    PixelFormat.Format32bppRgb

                                    );

            using (Graphics gr = Graphics.FromImage(bmp))
            {

                gr.CopyFromScreen(
                                  GlobalVarLn.location1.X,
                                  GlobalVarLn.location1.Y,

                                  0,
                                  0,
                                  bmp.Size,
                                  CopyPixelOperation.SourceCopy
                                  );

            }

            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

            return bmp;
        }
 */ 
        // ************************************************************************

        // ****************************************************************************************
        // Закрыть форму
        // ****************************************************************************************
        private void FormScreen_FormClosing(object sender, FormClosingEventArgs e)
        {
/*
            e.Cancel = true;
            Hide();

            GlobalVarLn.fl_Open_objFormScreen = 0;

            pbScreen.Image = null;
            GlobalVarLn.flScrM1 = 0;
            GlobalVarLn.flScrM2 = 0;
            GlobalVarLn.flScrM3 = 0;
            GlobalVarLn.bmpScr = null;
*/

        } //  Closing

        private void FormScreen_MaximumSizeChanged(object sender, EventArgs e)
        {
            ;
        }

        private void FormScreen_MinimumSizeChanged(object sender, EventArgs e)
        {
            ;
        }

        private void FormScreen_Resize(object sender, EventArgs e)
        {
            if (GlobalVarLn.flScr == 0)
            {
                //GlobalVarLn.flScr = 1;
                //pbScreen.Width *= 5;
                //pbScreen.Height *= 5;

            }

            else
            {
                //GlobalVarLn.flScr = 0;
                //pbScreen.Width /= 5;
                //pbScreen.Height /= 5;

            }


        }

        private void FormScreen_ResizeBegin(object sender, EventArgs e)
        {
            ;
        }

        private void FormScreen_ResizeEnd(object sender, EventArgs e)
        {
            ;
        }

        // Clear
        private void button1_Click(object sender, EventArgs e)
        {
/*
            pbScreen.Image = null;
            GlobalVarLn.flScrM1 = 0;
            GlobalVarLn.flScrM2 = 0;
            GlobalVarLn.flScrM3 = 0;
            GlobalVarLn.bmpScr = null;
*/
        }

        private void FormScreen_Activated(object sender, EventArgs e)
        {
            //GlobalVarLn.fl_Open_objFormScreen = 1;

        }

        // ****************************************************************************************



    } // Class
} // Namespace
