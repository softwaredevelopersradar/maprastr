﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.IO;

using System.Windows.Input;
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;
using Bitmap = System.Drawing.Bitmap;

namespace GrozaMap
{
    public partial class FormOB2 : Form
    {
        // Конструктор ************************************************************
        public FormOB2()
        {
            InitializeComponent();

        } // Конструктор
        // ***********************************************************  Конструктор

        // Загрузка формы *********************************************************

        private void FormOB2_Load(object sender, EventArgs e)
        {

            ClassMapRastrLoadForm.f_Load_FormOB2();

        }
        // ********************************************************* Загрузка формы

        // Активизировать форму ***************************************************

        private void FormOB2_Activated(object sender, EventArgs e)
        {
            GlobalVarLn.blOB2_stat = true;
            GlobalVarLn.flEndOB2_stat = 1;

            ClassMapRastrDopFunctions.f_Update_datgrid_LF1(
                                                           GlobalVarLn.objFormOB2G.dataGridView1,
                                                           GlobalVarLn.list1_OB2,
                                                           GlobalVarLn.sizeDatOB2_stat
                                                           );

            GlobalVarLn.fl_Open_objFormOB2 = 1;

        }
        // *************************************************** Активизировать форму

        // Закрыть форму **********************************************************

        private void FormOB2_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

            // приостановить обработку, если идет
            GlobalVarLn.blOB2_stat = false;

            GlobalVarLn.fl_Open_objFormOB2 = 0;

        }
        // ********************************************************** Закрыть форму

        // Очистка ****************************************************************

        private void bClear_Click(object sender, EventArgs e)
        {
            ClassMapRastrClear.f_Clear_FormOB2();
            // --------------------------------------------------------------------
            // Убрать с карты

            // CLASS
            ClassMapRastrReDrawAll.REDRAW_MAP_CLASS();
            // --------------------------------------------------------------------

        }
        // **************************************************************** Очистка

        // ************************************************************************
        // Обработчик кнопки : сохранить
        // ************************************************************************

        private void bAccept_Click(object sender, EventArgs e)
        {
            // OLD ***************************************************************
            // 10_10_2018
            /*
            UpdateListOB2();
            GlobalVarLn.SaveListOB2();

            // WORK
            //MapForm.REDRAW_MAP();
            // CLASS
            ClassMapRastrReDrawAll.REDRAW_MAP_CLASS();

             */
            // *************************************************************** OLD

            // NEW ***************************************************************
            // 10_10_2018

            // ---------------------------------------------------------------------
            GlobalVarLn.blOB2_stat = true;
            GlobalVarLn.flEndOB2_stat = 1;
            GlobalVarLn.list1_OB2.Clear();
            // ---------------------------------------------------------------------
            int ir = 0;
            int irf = 0;

            LF1 objLF = new LF1();
            double lt = 0;
            double lng = 0;
            double freq = 0;
            String s = "";
            // ---------------------------------------------------------------------

            // FOR >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            int jjj = 0;
            for (ir = 0; ir < dataGridView1.Rows.Count; ir++)
            {
                // IF**
                if (
                    ((dataGridView1.Rows[ir].Cells[0].Value != null) && (dataGridView1.Rows[ir].Cells[0].Value != "")) &&
                    ((dataGridView1.Rows[ir].Cells[1].Value != null) && (dataGridView1.Rows[ir].Cells[1].Value != ""))
                   )
                {


                    int index1 = 0;
                    int index2 = 0;
                    for (int jp = 0; jp < GlobalVarLn.list1_OB2_dubl.Count; jp++)
                    {
                        if (GlobalVarLn.list1_OB2_dubl[jp].index == ir)
                        {
                            index1 = 1;
                            index2 = jp;
                        }
                    }


                    if (index1 == 1)
                    {

                        // -----------------------------------------------------------------
                        //  Latitude из таблицы (WGS84)

                        // IF1
                        if ((dataGridView1.Rows[ir].Cells[0].Value != "") &&
                        (dataGridView1.Rows[ir].Cells[0].Value != null))
                        {
                            s = Convert.ToString(dataGridView1.Rows[ir].Cells[0].Value);

                            try
                            {
                                lt = Convert.ToDouble(s);
                            }
                            catch (SystemException)
                            {
                                try
                                {
                                    if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                                    lt = Convert.ToDouble(s);
                                }
                                catch
                                {
                                    MessageBox.Show("Incorrect data");
                                    return;
                                }

                            } // catch

                        } // IF1
                          // -----------------------------------------------------------------
                          //  Longitude из таблицы (WGS84)

                        // IF2
                        if ((dataGridView1.Rows[ir].Cells[1].Value != "") &&
                            (dataGridView1.Rows[ir].Cells[1].Value != null))
                        {
                            s = Convert.ToString(dataGridView1.Rows[ir].Cells[1].Value);

                            try
                            {
                                lng = Convert.ToDouble(s);
                            }
                            catch (SystemException)
                            {
                                try
                                {
                                    if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                                    lng = Convert.ToDouble(s);
                                }
                                catch
                                {
                                    MessageBox.Show("Incorrect data");
                                    return;
                                }

                            } // catch

                        } // IF2
                          // -----------------------------------------------------------------
                          // Имя

                        s = Convert.ToString(dataGridView1.Rows[ir].Cells[3].Value);

                        // -----------------------------------------------------------------
                        // Получить координаты на карте

                        // grad->rad
                        //lt = (lt * Math.PI) / 180;
                        //lng = (lng * Math.PI) / 180;
                        // Подаем rad, получаем там же расстояние на карте в м
                        //mapGeoToPlane(GlobalVarLn.hmapl, ref lt, ref lng);

                        var x = lt; // Lat
                        var y = lng; //Long
                                     // преобразование В меркатор
                        var p = Mercator.FromLonLat(y, x);
                        double xx = p.X;
                        double yy = p.Y;

                        double hhh = 0;
                        try
                        {
                            hhh = (double)MapForm.RasterMapControl.Dted.GetElevation(lng, lt);
                        }
                        catch
                        {
                            hhh = 0;
                        }

                        GlobalVarLn.X_OB2 = xx;
                        GlobalVarLn.Y_OB2 = yy;
                        // -----------------------------------------------------------------
                        // Заполнение структуры (X,Y,Name,Znak)

                        objLF.X_m = GlobalVarLn.X_OB2;
                        objLF.Y_m = GlobalVarLn.Y_OB2;
                        objLF.sType = s;

                        //objLF.indzn = GlobalVarLn.iZOB2;
/*
                        if (jjj < GlobalVarLn.list1_OB2_dubl.Count)
                            objLF.indzn = GlobalVarLn.list1_OB2_dubl[jjj].indzn;
                        else
                            objLF.indzn = GlobalVarLn.iZOB2;
*/
                        objLF.indzn = GlobalVarLn.list1_OB2_dubl[index2].indzn;


                        objLF.Lat = lt;
                        objLF.Long = lng;
                        // -----------------------------------------------------------------
                        // H

                        //GlobalVarLn.axMapPointGlobalAdd.SetPoint(GlobalVarLn.X_OB2, GlobalVarLn.Y_OB2);
                        //GlobalVarLn.H_OB2 = GlobalVarLn.axMapScreenGlobal.PointHeight_get(GlobalVarLn.axMapPointGlobalAdd);
                        GlobalVarLn.H_OB2 = hhh;

                        objLF.H_m = GlobalVarLn.H_OB2;

                        dataGridView1.Rows[ir].Cells[2].Value = GlobalVarLn.H_OB2;
                        // -----------------------------------------------------------------
                        // F из таблицы

                        // IF3
                        if ((dataGridView1.Rows[ir].Cells[4].Value != "") &&
                            (dataGridView1.Rows[ir].Cells[4].Value != null))
                        {
                            s = Convert.ToString(dataGridView1.Rows[ir].Cells[4].Value);

                            try
                            {
                                freq = Convert.ToDouble(s);
                            }
                            catch (SystemException)
                            {
                                try
                                {
                                    if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                                    freq = Convert.ToDouble(s);
                                }
                                catch
                                {
                                    MessageBox.Show("Incorrect data");
                                    return;
                                }

                            } // catch

                        } // IF3

                        objLF.FrequencyMhz = freq;
                        // -----------------------------------------------------------------
                        // Добавить строку

                        GlobalVarLn.list1_OB2.Add(objLF);
                        jjj += 1;
                        // -----------------------------------------------------------------

                        irf += 1;

                    } // index1=1

                } // IF**

            } // FOR
            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> FOR

            // -----------------------------------------------------------------
            if (irf != 0)
            {
                GlobalVarLn.blOB2_stat = true;
                GlobalVarLn.flEndOB2_stat = 1;
            }
            // -----------------------------------------------------------------
            // Сохранение List в bin файл
            //UpdateListOB2();
            GlobalVarLn.SaveListOB2();

            // WORK
            //MapForm.REDRAW_MAP();
            // CLASS
            ClassMapRastrReDrawAll.REDRAW_MAP_CLASS();

            // -----------------------------------------------------------------

            // *************************************************************** NEW



            //""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

            //0209 NO USE
            /*
                        int i_tmp = 0;
                        // -----------------------------------------------------------------------------------------
                        String strFileName;
                        strFileName = "OB2.txt";
                        StreamWriter srFile;

                        //StreamWriter srFile = new StreamWriter(strFileName);
                        try
                        {
                            srFile = new StreamWriter(strFileName);
                        }
                        catch
                        {
                            MessageBox.Show("Can’t save file");
                            return;
                        }


                        // -----------------------------------------------------------------------------------------
                        srFile.WriteLine("N =" + Convert.ToString(GlobalVarLn.iOB2_stat));

                        for (i_tmp = 0; i_tmp < GlobalVarLn.iOB2_stat; i_tmp++)
                        {

                            srFile.WriteLine("X =" + Convert.ToString((int)GlobalVarLn.list1_OB2[i_tmp].X_m));
                            srFile.WriteLine("Y =" + Convert.ToString((int)GlobalVarLn.list1_OB2[i_tmp].Y_m));
                            srFile.WriteLine("H =" + Convert.ToString((int)GlobalVarLn.list1_OB2[i_tmp].H_m));
                            srFile.WriteLine("Type =" + Convert.ToString(GlobalVarLn.list1_OB2[i_tmp].sType));
                            srFile.WriteLine("indzn =" + Convert.ToString(GlobalVarLn.list1_OB2[i_tmp].indzn));

                        }
                        // -------------------------------------------------------------------------------------

                        srFile.Close();
                        // ------------------------------------------------------------------------------------

            */

            //""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

        } // Save in file
        // ************************************************************************



        // Обработчик кнопки : read from file
        // ************************************************************************
        private void button1_Click(object sender, EventArgs e)
        {
            // Чтение из bin файла в List
            GlobalVarLn.LoadListOB2();
            // Обновление таблицы
            ClassMapRastrDopFunctions.f_Update_datgrid_LF1(
                                                           GlobalVarLn.objFormOB2G.dataGridView1,
                                                           GlobalVarLn.list1_OB2,
                                                           GlobalVarLn.sizeDatOB2_stat
                                                           );

            GlobalVarLn.flEndOB2_stat = 1;

            // CLASS
            ClassMapRastrReDrawAll.REDRAW_MAP_CLASS();
        }

        // ************************************************************************
        // Удалить объект
        // ************************************************************************
        //0209

        private void button3_Click(object sender, EventArgs e)
        {
            // -----------------------------------------------------------------------------------------
            String strLine2 = "";
            String strLine3 = "";

            int it = 0;
            int index = 0;
            // -----------------------------------------------------------------------------------------
            // Если открыта таблица

            if (GlobalVarLn.iOB2_stat != 0)
            {
                index = dataGridView1.CurrentRow.Index;
                if (index >= GlobalVarLn.list1_OB2.Count)
                {
                    return;
                }

                // Убрать с таблицы
                dataGridView1.Rows.Remove(dataGridView1.Rows[index]);
                // Del from list
                GlobalVarLn.list1_OB2.Remove(GlobalVarLn.list1_OB2[index]);


            } // IF(GlobalVarLn.iSP_stat != 0)
            // -------------------------------------------------------------------------------------


            // CLASS
            ClassMapRastrReDrawAll.REDRAW_MAP_CLASS();

        } // Delete
        // ************************************************************************

        // ФУНКЦИИ ********************************************************************************

        // ****************************************************************************************
        // Обработка нажатия левой кнопки мыши при отрисовке OB1
        //
        // Входные параметры:
        // X - X, m на местности
        // Y - Y, m
        // ****************************************************************************************
        public void f_OB2(
                          double X,
                          double Y
                         )
        {

            // ......................................................................

            LF1 objLF = new LF1();
            // ......................................................................
            // !!! Merkator в м 

            GlobalVarLn.X_OB2 = GlobalVarLn.X_Rastr;
            GlobalVarLn.Y_OB2 = GlobalVarLn.Y_Rastr;
            objLF.X_m = GlobalVarLn.X_OB2;
            objLF.Y_m = GlobalVarLn.Y_OB2;

            GlobalVarLn.H_OB2 = GlobalVarLn.H_Rastr;
            objLF.H_m = GlobalVarLn.H_OB2;

            objLF.Lat = GlobalVarLn.LAT_Rastr;   //grad WGS84
            objLF.Long = GlobalVarLn.LONG_Rastr; //grad WGS84

            objLF.indzn = GlobalVarLn.iZOB2;
            objLF.sType = "enemy";

            // ????????????????????
            objLF.index = (int)(GlobalVarLn.list1_OB2_dubl.Count);
            objLF.ent = 2;
            // ......................................................................
            // ......................................................................
            GlobalVarLn.blOB2_stat = true;
            GlobalVarLn.flEndOB2_stat = 1;
            // ......................................................................
            //GlobalVarLn.iOB2_stat += 1;

            // Добавить строку

            GlobalVarLn.list1_OB2.Add(objLF);
            GlobalVarLn.list1_OB2_dubl.Add(objLF);

            ClassMapRastrDopFunctions.f_Update_datgrid_LF1(
                                                           GlobalVarLn.objFormOB2G.dataGridView1,
                                                           GlobalVarLn.list1_OB2,
                                                           GlobalVarLn.sizeDatOB2_stat
                                                           );

            // ---------------------------------------------------------------------------------
            // Перерисовать

            // WORK
            //MapForm.REDRAW_MAP();
            // CLASS
            ClassMapRastrReDrawAll.REDRAW_MAP_CLASS();
            // ---------------------------------------------------------------------------------

        } // f_OB2

        // ***********************************************************************************

        // ****************************************************************************************
        // Ручной ввод OB2
        //
        // Входные параметры: Меркатор в м
        // ****************************************************************************************
        public void f_OB2_1(
                          double X,
                          double Y,
                          double lt,
                          double lng,
                          double hhh,
                          int index
                         )
        {
            // ......................................................................

            LF1 objLF = new LF1();
            // ......................................................................
            // !!! Merkator в м 

            GlobalVarLn.X_OB2 = X;
            GlobalVarLn.Y_OB2 = Y;
            objLF.X_m = GlobalVarLn.X_OB2;
            objLF.Y_m = GlobalVarLn.Y_OB2;

            GlobalVarLn.H_OB2 = hhh;
            objLF.H_m = GlobalVarLn.H_OB2;

            objLF.Lat = lt;   //grad WGS84
            objLF.Long = lng; //grad WGS84

            objLF.indzn = GlobalVarLn.iZOB2;
            objLF.sType = "enemy";

            // ручной ввод
            objLF.index = index;
            objLF.ent = 1;
            // ......................................................................
            // ......................................................................
            GlobalVarLn.blOB2_stat = true;
            GlobalVarLn.flEndOB2_stat = 1;
            // ......................................................................
            // Добавить в List

            GlobalVarLn.list1_OB2.Add(objLF);
            GlobalVarLn.list1_OB2_dubl.Add(objLF);

            ClassMapRastrDopFunctions.f_Update_datgrid_LF1(
                                                           GlobalVarLn.objFormOB2G.dataGridView1,
                                                           GlobalVarLn.list1_OB2,
                                                           GlobalVarLn.sizeDatOB2_stat
                                                           );

            // ---------------------------------------------------------------------------------
            // Перерисовать

            // WORK
            //MapForm.REDRAW_MAP();
            // CLASS
            ClassMapRastrReDrawAll.REDRAW_MAP_CLASS();
            // ---------------------------------------------------------------------------------

        } // P/P f_OB2_1
        // *************************************************************************************



        // ******************************************************************************** ФУНКЦИИ



        // ****************************************************************************************
        // Значок
        // ****************************************************************************************
        private void buttonZOB1_Click(object sender, EventArgs e)
        {
            GlobalVarLn.iZOB2 += 1;
            if (GlobalVarLn.iZOB2 == imageList1.Images.Count)
                GlobalVarLn.iZOB2 = 0;
            pbOB2.BackgroundImage = imageList1.Images[GlobalVarLn.iZOB2];


        }

        private void button2_Click(object sender, EventArgs e)
        {
            GlobalVarLn.iZOB2 -= 1;
            if (GlobalVarLn.iZOB2 < 0)
                GlobalVarLn.iZOB2 = imageList1.Images.Count - 1;
            pbOB2.BackgroundImage = imageList1.Images[GlobalVarLn.iZOB2];

        } // Значок

        private void button4_Click(object sender, EventArgs e)
        {
            int index = 0;
            string s = "";
            double lt = 0;
            double lng = 0;
            // -----------------------------------------------------------------------------------------
            // Индекс строки

            index = dataGridView1.CurrentRow.Index;
            if (index < GlobalVarLn.list1_OB2.Count)
            {
                return;
            }
            // -------------------------------------------------------------------------------------
            //  Latitude из таблицы (WGS84)

            // IF1
            if (
                (dataGridView1.Rows[index].Cells[0].Value != "") &&
                (dataGridView1.Rows[index].Cells[0].Value != null)
                )
            {
                s = Convert.ToString(dataGridView1.Rows[index].Cells[0].Value);

                try
                {
                    lt = Convert.ToDouble(s);
                }
                catch (SystemException)
                {
                    try
                    {
                        if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                        lt = Convert.ToDouble(s);
                    }
                    catch
                    {
                        MessageBox.Show("Incorrect data");
                        return;
                    }

                } // catch

            } // IF1

            else
            {
                MessageBox.Show("Incorrect data");
                return;
            }
            // -------------------------------------------------------------------------------------
            //  Longitude из таблицы (WGS84)

            // IF2
            if ((dataGridView1.Rows[index].Cells[1].Value != "") &&
                (dataGridView1.Rows[index].Cells[1].Value != null))
            {
                s = Convert.ToString(dataGridView1.Rows[index].Cells[1].Value);

                try
                {
                    lng = Convert.ToDouble(s);
                }
                catch (SystemException)
                {
                    try
                    {
                        if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                        lng = Convert.ToDouble(s);
                    }
                    catch
                    {
                        MessageBox.Show("Incorrect data");
                        return;
                    }

                } // catch

            } // IF2

            else
            {
                MessageBox.Show("Incorrect data");
                return;
            }
            // -------------------------------------------------------------------------------------
            var x = lt; // Lat
            var y = lng; //Long
                         // преобразование В меркатор
            var p = Mercator.FromLonLat(y, x);
            double xx = p.X;
            double yy = p.Y;

            double hhh = 0;
            try
            {
                hhh = (double)MapForm.RasterMapControl.Dted.GetElevation(lng, lt);
            }
            catch
            {
                hhh = 0;
            }
            // -------------------------------------------------------------------------------------
            f_OB2_1(
                    xx,
                    yy,
                    lt,
                    lng,
                    hhh,
                    index
                    );

        }
    } // Class
} // Namespace
