﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.IO;
using System.Collections.Generic;

using System.Windows.Input;
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;
using Bitmap = System.Drawing.Bitmap;
using Point = System.Drawing.Point;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.ServiceModel;
using System.Diagnostics;
using System.Threading;
using System.Globalization;

using MapRastr;  // DLL

namespace GrozaMap
{
    public partial class FormSpuf : Form
    {
        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        //private double LAMBDA;

        // .....................................................................
        private double OwnHeight_comm;
        // Высота средства подавления
        private double HeightAntennOwn_comm;
        private double HeightTotalOwn_comm;

        // объект подавления
        private double HeightOpponent_comm;
        // Высота антенны противника
        private int iOpponAnten_comm;

        private int iMiddleHeight_comm;
        // ......................................................................
        private double P_supnav;
        private double K_supnav;
        private double iKP_supnav;
        // ......................................................................

        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // Конструктор *********************************************************** 
        public FormSpuf()
        {
            InitializeComponent();

            //LAMBDA = 300000;
            // .....................................................................
            OwnHeight_comm = 0;
            // Высота средства подавления
            HeightAntennOwn_comm = 0; // антенна
            HeightTotalOwn_comm = 0;

            // объект подавления
            HeightOpponent_comm = 0;
            // Высота антенны противника
            iOpponAnten_comm = 0;

            iMiddleHeight_comm = 0;
            // ......................................................................
            P_supnav = 0;
            K_supnav = 0;
            iKP_supnav = 0;
            iKP_supnav = 0;
            // ......................................................................

        } // Конструктор
        // ***********************************************************  Конструктор

        // Загрузка формы *********************************************************

        private void FormSpuf_Load(object sender, EventArgs e)
        {
            // ----------------------------------------------------------------------
            ClassMapRastrLoadForm.f_Load_FormSpuf();
            // ----------------------------------------------------------------------
            // Изменение списка СП

            GlobalVarLn.ListJSChangedEvent += OnListJSChanged;
            // ----------------------------------------------------------------------

        }
        // ********************************************************* Загрузка формы

        // Изменение списка СП ****************************************************
        private void OnListJSChanged(object sender, EventArgs e)
        {
            MapForm.UpdateDropDownList(cbCenterLSR);
        }
        // **************************************************** Изменение списка СП

        // Activated **************************************************************

        private void FormSpuf_Activated(object sender, EventArgs e)
        {
            GlobalVarLn.fl_Open_objFormSpuf = 1;

        }
        // ************************************************************** Activated

        // Closing ****************************************************************

        private void FormSpuf_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

            GlobalVarLn.fl_Open_objFormSpuf = 0;

        }
        // **************************************************************** Closing

        // Очистка ****************************************************************

        private void bClear_Click(object sender, System.EventArgs e)
        {
            // --------------------------------------------------------------------
            ClassMapRastrClear.f_Clear_FormSpuf();
            // --------------------------------------------------------------------
            // Убрать с карты

            // CLASS
            ClassMapRastrReDrawAll.REDRAW_MAP_CLASS();
            // -------------------------------------------------------------------

        }
        // **************************************************************** Очистка

        // ************************************************************************
        // Обработчик ComboBox "cbCenterLSR": Выбор SP
        // ************************************************************************
        private void cbCenterLSR_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (cbCenterLSR.SelectedIndex == 0)
                GlobalVarLn.NumbSP_spf = "";
            else
                GlobalVarLn.NumbSP_spf = Convert.ToString(cbCenterLSR.Items[cbCenterLSR.SelectedIndex]);

        } // SP
          // ************************************************************************

        // ---------------------------------------------------------------------
        private void comboBox2_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            K_supnav = Convert.ToDouble(comboBox2.Items[comboBox2.SelectedIndex]);

        }
        // ---------------------------------------------------------------------

        // SP *********************************************************************
        // Обработчик Button1 "Центр Зоны"

        private void button1_Click(object sender, System.EventArgs e)
        {
            // ......................................................................
            ClassMap objClassMap1_ed = new ClassMap();
            ClassMap objClassMap2_ed = new ClassMap();
            ClassMap objClassMap3_ed = new ClassMap();
            // ......................................................................

            // Выбор координат COORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOOR

            // ----------------------------------------------------------------------
            // Мышь на карте

            if (cbCenterLSR.SelectedIndex == 0)
            {
                GlobalVarLn.NumbSP_spf = "";

                GlobalVarLn.XCenter_spf = GlobalVarLn.X_Rastr;
                GlobalVarLn.YCenter_spf = GlobalVarLn.Y_Rastr;
                OwnHeight_comm = GlobalVarLn.H_Rastr;

            }
            // ----------------------------------------------------------------------
            // Выбор из списка СП

            else
            {
                GlobalVarLn.NumbSP_spf = Convert.ToString(cbCenterLSR.Items[cbCenterLSR.SelectedIndex]);

                var stations = GlobalVarLn.listJS;
                var station = stations[cbCenterLSR.SelectedIndex - 1];

                GlobalVarLn.XCenter_spf = station.CurrentPosition.x;
                GlobalVarLn.YCenter_spf = station.CurrentPosition.y;
                OwnHeight_comm = station.CurrentPosition.h;

            }

            tbOwnHeight.Text = Convert.ToString(OwnHeight_comm);

            // COORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOOR Выбор координат

            // ......................................................................
            GlobalVarLn.tpOwnCoordRect_spf.X = (int)GlobalVarLn.XCenter_spf;
            GlobalVarLn.tpOwnCoordRect_spf.Y = (int)GlobalVarLn.YCenter_spf;
            // ......................................................................

            // ......................................................................
            // SP на карте

            // CLASS
            ClassMapRastrDraw.f_DrawImage(
                          GlobalVarLn.XCenter_spf,  // Mercator
                          GlobalVarLn.YCenter_spf,
                          -1,
                          -1,
                          Application.StartupPath + "\\ImagesHelp\\" + "1.png",
                          "",
                          // Масштаб изображения
                          2
                         );

            // ......................................................................
            GlobalVarLn.fl_supnav = 1;
            GlobalVarLn.flCoord_supnav = 1; // Центр ЗПВ выбран

            var p = Mercator.ToLonLat(GlobalVarLn.XCenter_spf, GlobalVarLn.YCenter_spf);
            GlobalVarLn.LatCenter_spf_84 = p.Y;
            GlobalVarLn.LongCenter_spf_84 = p.X;
            // ......................................................................
            // Отображение СП в выбранной СК

            OtobrSP_spf();
            // .......................................................................


        } //Центр Зоны
        // ********************************************************************* SP

        // ZONE *******************************************************************
        // Кнопка Принять

        private void bAccept_Click(object sender, System.EventArgs e)
        {
            String s1 = "";

            // ........................................................................
            if ((GlobalVarLn.XCenter_spf == 0) || (GlobalVarLn.YCenter_spf == 0))
            {
                MessageBox.Show("The center of the zone is not selected");
                return;
            }
            // ........................................................................
            if (GlobalVarLn.listSpoofingZone.Count != 0)
                GlobalVarLn.listSpoofingZone.Clear();
            if (GlobalVarLn.listSpoofingJamZone.Count != 0)
                GlobalVarLn.listSpoofingJamZone.Clear();
            // ----------------------------------------------------------------------

            // H *********************************************************************

            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
            // Средняя высота местности

            // DLL
            ClassMapRastr objClassMap1 = new ClassMapRastr();
            MyPoint objMyPoint1 = new MyPoint();
            objMyPoint1.X = (int)GlobalVarLn.tpOwnCoordRect_spf.X;
            objMyPoint1.Y = (int)GlobalVarLn.tpOwnCoordRect_spf.Y;
            iMiddleHeight_comm = objClassMap1.DefineMiddleHeight_Comm(
                                                                 objMyPoint1,
                                                                 MapForm.RasterMapControl,
                                                                 30000, // R
                                                                 100   // Step
                                                                     );
            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

            // Высота антенны
            s1 = tbHAnt.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                HeightAntennOwn_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    HeightAntennOwn_comm = Convert.ToDouble(s1);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }

            }

            HeightTotalOwn_comm = OwnHeight_comm + HeightAntennOwn_comm;

            // OP
            s1 = tbOpponentAntenna.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                iOpponAnten_comm = (int)Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    iOpponAnten_comm = (int)Convert.ToDouble(s1);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }

            }

            HeightOpponent_comm = (double)(iMiddleHeight_comm + iOpponAnten_comm);
            // ********************************************************************* H

            // Ввод параметров ********************************************************
            // !!! Координаты центра ЗПВ уже расчитаны и введены по кнопке 'Центр ЗПВ'

            // ........................................................................
            // Мощность 
            s1 = Convert.ToString(textBox2.Text);
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                P_supnav = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    P_supnav = Convert.ToDouble(s1);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }

            }
            if ((P_supnav < 0.1) || (P_supnav > 15))
            {
                MessageBox.Show("Parameter 'Power' out of range  0.1W - 15W");
                return;
            }

            // ........................................................................
            // Коэффициент усиления

            s1 = Convert.ToString(comboBox2.Items[comboBox2.SelectedIndex]);
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                K_supnav = (int)Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    K_supnav = (int)Convert.ToDouble(s1);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }

            }
            // ........................................................................
            // Коэффициент спуфинга

            s1 = textBox1.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                iKP_supnav = (int)Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    iKP_supnav = (int)Convert.ToDouble(s1);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }

            }

            if ((iKP_supnav < 7) || (iKP_supnav > 15))
            {
                MessageBox.Show("Parameter 'Spoofing coefficient' out of range 7dB - 15dB");
                return;
            }

            // ........................................................................

            // ******************************************************** Ввод параметров

            // Расчет зоны ************************************************************
            // DLL

            GlobalVarLn.fl_spf = 1;
            GlobalVarLn.flS_spf = 1;
            GlobalVarLn.flJ_spf = 1;

            ClassMapRastr objClassMap2 = new ClassMapRastr();
            MyPoint objMyPointSP = new MyPoint();

            objMyPointSP.X = GlobalVarLn.tpOwnCoordRect_spf.X;
            objMyPointSP.Y = GlobalVarLn.tpOwnCoordRect_spf.Y;

            List<MyPoint> PolSpuf = new List<MyPoint>();
            List<MyPoint> PolJam = new List<MyPoint>();

            objClassMap2.f_ZoneSpoofingJam(

                      // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                      MapForm.RasterMapControl,

                      // Координаты СП
                      objMyPointSP,

                      // Hantsp
                      (int)HeightAntennOwn_comm,
                      // Hrelsp
                      (int)OwnHeight_comm,
                      // Hsredn
                      iMiddleHeight_comm,
                      // Hantop
                      (int)iOpponAnten_comm,

                      // P, Wt, SP
                      P_supnav,
                      // К-т усиления, СП
                      K_supnav,
                      // К-т спуфинга
                      iKP_supnav,

                      // шаг изменения угла в град при расчете полигона
                      GlobalVarLn.iStepAngleInput_ZPV,
                      // шаг в м при расчете ЗПВ
                      GlobalVarLn.iStepLengthInput_ZPV,

                      // R zone spoofing (int)
                      ref GlobalVarLn.iR1_spf,
                      // R зоны подавления навигации(для зоны спуфинга) (int)
                      ref GlobalVarLn.iR2_spf,

                      ref PolSpuf,
                      ref PolJam

                                            );


            Point objpol = new Point();
            int ipol = 0;
            for (ipol = 0; ipol < PolSpuf.Count; ipol++)
            {
                objpol.X = PolSpuf[ipol].X;
                objpol.Y = PolSpuf[ipol].Y;
                GlobalVarLn.listSpoofingZone.Add(objpol);
            }
            for (ipol = 0; ipol < PolJam.Count; ipol++)
            {
                objpol.X = PolJam[ipol].X;
                objpol.Y = PolJam[ipol].Y;
                GlobalVarLn.listSpoofingJamZone.Add(objpol);
            }

            // .........................................................................
            // Убрать с карты и отрисовать зону спуфинга (Green)+
            // зону подавления навигации(для зоны спуфинга)(Blue)

            // CLASS
            ClassMapRastrReDrawAll.REDRAW_MAP_CLASS();
            // .........................................................................

            // ************************************************************ Расчет зоны

            // ----------------------------------------------------------------------
            tbR1.Text = Convert.ToString(GlobalVarLn.iR1_spf);
            tbR2.Text = Convert.ToString(GlobalVarLn.iR2_spf);
            // ----------------------------------------------------------------------


        } // Accept
        // ******************************************************************* ZONE

        // -------------------------------------------------------------------------
        // No

        private void chbS_CheckedChanged(object sender, EventArgs e)
        {
/*
            // ---------------------------------------------------------------------
            if (GlobalVarLn.objFormSpufG.chbS.Checked == false)
            {
                GlobalVarLn.flS_spf = 0;
            }
            // ---------------------------------------------------------------------
            else
            {
                GlobalVarLn.flS_spf = 1;
            }
            // ---------------------------------------------------------------------
*/
        }
        // -------------------------------------------------------------------------
        // No

        private void chbJ_CheckedChanged(object sender, EventArgs e)
        {
/*
            // ---------------------------------------------------------------------
            if (GlobalVarLn.objFormSpufG.chbJ.Checked == false)
            {
                GlobalVarLn.flJ_spf = 0;
                
            }
            // ---------------------------------------------------------------------
            else
            {
                GlobalVarLn.flJ_spf = 1;
            }
            // ---------------------------------------------------------------------
*/
        }
        // -------------------------------------------------------------------------

        // ************************************************************************
        // функция отображения координат центра зоны
        // ************************************************************************
        private void OtobrSP_spf()
        {
            //var pos = axaxcMapScreen.MapPlaneToRealGeo(GlobalVarLn.XCenter_spf, GlobalVarLn.YCenter_spf);
            //tbXRect.Text = pos.X.ToString("F3");
            //tbYRect.Text = pos.Y.ToString("F3");

            tbXRect.Text = GlobalVarLn.LatCenter_spf_84.ToString("F3");
            tbYRect.Text = GlobalVarLn.LongCenter_spf_84.ToString("F3");


        } // OtobrSP_spf
        // ************************************************************************

        // -------------------------------------------------------------------------

        private void tbR2_TextChanged(object sender, EventArgs e)
        {

        }
        // -------------------------------------------------------------------------

    } 
}
