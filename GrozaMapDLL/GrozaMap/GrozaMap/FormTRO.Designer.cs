﻿namespace GrozaMap
{
    partial class FormTRO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.bClear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(78, 23);
            this.button1.TabIndex = 222;
            this.button1.Text = "Load";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // bClear
            // 
            this.bClear.Location = new System.Drawing.Point(118, 12);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(85, 23);
            this.bClear.TabIndex = 220;
            this.bClear.Text = "Clear";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // FormTRO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(231, 45);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.bClear);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(1000, 100);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormTRO";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Tactical and radio environment";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.FormTRO_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormTRO_FormClosing);
            this.Load += new System.EventHandler(this.FormTRO_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button bClear;
    }
}