﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.IO;

using System.Windows.Input;
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;
using Bitmap = System.Drawing.Bitmap;
using Point = System.Drawing.Point;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.ServiceModel;
using System.Diagnostics;
using System.Threading;
using System.Globalization;

using MapRastr;  // DLL

namespace GrozaMap
{
    public partial class FormLineSightRange : Form
    {
        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        private double dchislo;
        private long ichislo;
        //private double LAMBDA;
        // .....................................................................

        private double OwnHeight_comm;

        public static FormLineSightRange Instance { get; private set; }

        // Высота средства подавления
        private double HeightAntennOwn_comm;
        private double HeightTotalOwn_comm;

        // объект подавления
        private int i_HeightOpponent_comm;
        private int i_HeightOwnObject_comm;
        private double HeightOpponent_comm;
        // Высота антенны противника
        private int iOpponAnten_comm;


        private int iMiddleHeight_comm;

        // ДПВ
        private int iDSR;

        // ......................................................................
        // ZOSP
        private double F_ZOSP;
        private double P_ZOSP;
        private double K_ZOSP;
        private int iKP_ZOSP;

        // ......................................................................

        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // Конструктор *********************************************************** 
        public FormLineSightRange()
        {
            Instance = this;

            InitializeComponent();

            dchislo = 0;
            ichislo = 0;
            //LAMBDA = 300000;

            // .....................................................................
            OwnHeight_comm = 0;

            i_HeightOwnObject_comm = 0;
            HeightAntennOwn_comm = 0; // антенна
            HeightTotalOwn_comm = 0;

            // объект подавления
            i_HeightOpponent_comm = 0;
            HeightOpponent_comm = 0;
            // Высота антенны противника
            iOpponAnten_comm = 0;

            iMiddleHeight_comm = 0;

            // ДПВ
            iDSR = 0;

            // ......................................................................
            // ZOSP
            F_ZOSP = 0;
            P_ZOSP = 0;
            K_ZOSP = 0;
            iKP_ZOSP = 0;
            // ......................................................................


        } // Конструктор
        // ***********************************************************  Конструктор

        // Загрузка формы *********************************************************
        private void FormLineSightRange_Load(object sender, EventArgs e)
        {

            // ----------------------------------------------------------------------
            ClassMapRastrLoadForm.f_Load_FormLightSightRange();
            // ----------------------------------------------------------------------
            // Изменение списка СП

            GlobalVarLn.ListJSChangedEvent += OnListJSChanged;
            // ----------------------------------------------------------------------

        } 
        // ********************************************************* Загрузка формы

        // Изменение списка СП ****************************************************

        // ------------------------------------------------------------------------
        private void OnListJSChanged(object sender, EventArgs e)
        {
            //UpdateDropDownList();
            MapForm.UpdateDropDownList(cbCenterLSR);

        }
        // ------------------------------------------------------------------------

        // -------------------------------------------------------------------------
        /*
                public void UpdateDropDownList()
                {
                    MapForm.UpdateDropDownList(cbCenterLSR);
                }
        */
        // -------------------------------------------------------------------------

        // ***************************************************** Изменение списка СП

        // Activated ***************************************************************

        private void FormLineSightRange_Activated(object sender, EventArgs e)
        {
            GlobalVarLn.fl_Open_objFormLineSightRange = 1;

        }
        // *************************************************************** Activated

        // Closing *****************************************************************

        private void FormLineSightRange_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

            GlobalVarLn.fl_Open_objFormLineSightRange = 0;

        }
        // ***************************************************************** Closing

        // Очистка1 ***************************************************************
        // ZPV

        private void bClear_Click_1(object sender, EventArgs e)
        {
            // ----------------------------------------------------------------------
            ClassMapRastrClear.f_Clear_FormLineSightRange1();
            // ----------------------------------------------------------------------
            // Убрать с карты

            // CLASS
            ClassMapRastrReDrawAll.REDRAW_MAP_CLASS();
            // ---------------------------------------------------------------------

        }
        // *************************************************************** Очистка1

        // Очистка2 ***************************************************************
        // Зона обнаружения СП

        private void button3_Click(object sender, EventArgs e)
        {
            // ----------------------------------------------------------------------
            ClassMapRastrClear.f_Clear_FormLineSightRange2();
            // ----------------------------------------------------------------------
            // Убрать с карты

            // CLASS
            ClassMapRastrReDrawAll.REDRAW_MAP_CLASS();
            // ----------------------------------------------------------------------

        }
        // *************************************************************** Очистка2

        // SP *********************************************************************

        private void button1_Click_1(object sender, EventArgs e)
        {

            double xtmp_ed, ytmp_ed;

/*
            double xtmp1_ed, ytmp1_ed;
            int it = 0;
            String strLine3 = "";
            String strLine2 = "";
*/

            ClassMap objClassMap3_ed = new ClassMap();

            // ......................................................................

            // Выбор координат COORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOOR

            // ----------------------------------------------------------------------
            // Мышь на карте

            if (cbCenterLSR.SelectedIndex == 0)
            {
                GlobalVarLn.NumbSP_lsr = "";

                GlobalVarLn.XCenter_ZPV = GlobalVarLn.X_Rastr; // Mercator
                GlobalVarLn.YCenter_ZPV = GlobalVarLn.Y_Rastr;
                OwnHeight_comm = GlobalVarLn.H_Rastr;
            }
            // ----------------------------------------------------------------------
            // Выбор из списка СП

            else
            {
                GlobalVarLn.NumbSP_lsr = Convert.ToString(cbCenterLSR.Items[cbCenterLSR.SelectedIndex]);
                var stations = GlobalVarLn.listJS;
                var station = stations[cbCenterLSR.SelectedIndex - 1];

                GlobalVarLn.XCenter_ZPV = station.CurrentPosition.x; // Mercator
                GlobalVarLn.YCenter_ZPV = station.CurrentPosition.y;
                OwnHeight_comm = station.CurrentPosition.h;

            }
            // ----------------------------------------------------------------------

            // ----------------------------------------------------------------------
            // Ручной ввод

            if (checkBox1.Checked == true)
            {
                if ((tbXRect.Text == "") || (tbYRect.Text == ""))
                {
                    MessageBox.Show("Invalid coordinates of the LOS center");
                    return;
                }

                double h = 0;
                double Lt = 0;
                double Ln = 0;
                String s = "";

                //Lat
                s = tbXRect.Text;
                try
                {
                    Lt = Convert.ToDouble(s);
                }
                catch (SystemException)
                {
                    try
                    {
                        if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                        Lt = Convert.ToDouble(s);
                    }
                    catch
                    {
                        MessageBox.Show("Incorrect data");
                        return;
                    }
                }

                //Long
                s = tbYRect.Text;
                try
                {
                    //if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                    Ln = Convert.ToDouble(s);
                }
                catch (SystemException)
                {
                    try
                    {
                        if (s.IndexOf(",") > -1) s = s.Replace(',', '.');
                        Ln = Convert.ToDouble(s);
                    }
                    catch
                    {
                        MessageBox.Show("Incorrect data");
                        return;
                    }
                }
                // ......................................................................
                // grad->rad
                //Lt = (Lt * Math.PI) / 180;
                //Ln = (Ln * Math.PI) / 180;
                // Подаем rad, получаем там же расстояние на карте в м
                //mapGeoToPlane(GlobalVarLn.hmapl, ref Lt, ref Ln);

                var x = Lt; // Lat
                var y = Ln; //Long
                // преобразование В меркатор
                var p1 = Mercator.FromLonLat(y, x);
                double xx = p1.X;
                double yy = p1.Y;

                double hhh = 0;
                try
                {
                    hhh = (double)MapForm.RasterMapControl.Dted.GetElevation(Ln, Lt);
                }
                catch
                {
                    hhh = 0;
                }

                GlobalVarLn.XCenter_ZPV = xx;
                GlobalVarLn.YCenter_ZPV = yy;
                OwnHeight_comm = hhh;

            } // chbXY.Checked == true
            // ---------------------------------------------------------------------

            // Для зоны обнаружения СП
            GlobalVarLn.XCenter_ZOSP = GlobalVarLn.XCenter_ZPV;
            GlobalVarLn.YCenter_ZOSP = GlobalVarLn.YCenter_ZPV;
            // ---------------------------------------------------------------------
            // Center ZPV (H)

            tbOwnHeight.Text = Convert.ToString(OwnHeight_comm);

            // COORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOOR Выбор координат

            // ......................................................................
            // SP на карте


            // CLASS
            ClassMapRastrDraw.f_DrawImage(
                          GlobalVarLn.XCenter_ZPV,  // Mercator
                          GlobalVarLn.YCenter_ZPV,
                          -1,
                          -1,
                          Application.StartupPath + "\\ImagesHelp\\" + "1.png",
                          "",
                          2
                         );


/*
            // !!!Для отладки самолета
            ClassMapRastrDraw.f_DrawImage(
                          -1,  // Mercator
                          -1,
                          23.181,
                          52.908,
                          Application.StartupPath + "\\ImagesHelp\\" + "Source.ico",
                          "www",
                          0.6
                         );
*/


            // ......................................................................
            GlobalVarLn.fl_LineSightRange = 1;
            GlobalVarLn.flCoordZPV = 1; // Центр ЗПВ выбран
            // ......................................................................
            // Реальные координаты карты в м -> в долготу и широту WGS84 с помощью функций Панорамы
            // !!! Выход функции(rad) идет на место входных переменных

            xtmp_ed = GlobalVarLn.XCenter_ZPV;
            ytmp_ed = GlobalVarLn.YCenter_ZPV;

            //mapPlaneToGeoWGS84(GlobalVarLn.hmapl, ref xtmp_ed, ref ytmp_ed);
            var p = Mercator.ToLonLat(GlobalVarLn.XCenter_ZPV, GlobalVarLn.YCenter_ZPV);
            GlobalVarLn.LatCenter_ZPV_84 = p.Y;
            GlobalVarLn.LongCenter_ZPV_84 = p.X;

            //0209
/*
            // rad(WGS84)->grad(WGS84)
            xtmp1_ed = (xtmp_ed * 180) / Math.PI;
            ytmp1_ed = (ytmp_ed * 180) / Math.PI;
            // .......................................................................
            // CDel

            // WGS84,grad
            LatKrG_comm = xtmp1_ed;
            LongKrG_comm = ytmp1_ed;
            // .......................................................................

            LatKrR_comm = (LatKrG_comm * Math.PI) / 180;
            LongKrR_comm = (LongKrG_comm * Math.PI) / 180;
            // .......................................................................
            // Эллипсоид Красовского, grad,min,sec
            // dd.ddddd -> DD MM SS
            // CDel
            // !!! Здесь это WGS84

            // Широта
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LatKrG_comm,

                // Выходные параметры 
                ref Lat_Grad_comm,
                ref Lat_Min_comm,
                ref Lat_Sec_comm

              );

            // Долгота
            objClassMap3_ed.f_Grad_GMS
              (
                // Входные параметры (grad)
                LongKrG_comm,

                // Выходные параметры 
                ref Long_Grad_comm,
                ref Long_Min_comm,
                ref Long_Sec_comm

              );
 */
            OtobrSP_Comm();
            // .......................................................................


        } // Button1: SP
        // ********************************************************************* SP

        //  Зона обнаружения СП ***************************************************
        // Кнопка Принять Зона обнаружения СП

        private void button2_Click(object sender, EventArgs e)
        {
            String s1 = "";

            // ........................................................................
            if ((GlobalVarLn.XCenter_ZOSP == 0) || (GlobalVarLn.YCenter_ZOSP == 0))
            {
                MessageBox.Show("The center of the zone is not selected");
                return;
            }
            // ........................................................................
            if (GlobalVarLn.listDetectionZone.Count != 0)
                GlobalVarLn.listDetectionZone.Clear();
            // ---------------------------------------------------------------------

            // Ввод параметров ********************************************************
            // !!! Координаты центра ЗПВ уже расчитаны и введены по кнопке 'Центр ЗПВ'

            // ........................................................................
            // Мощность передатчика связи 

            s1 = tbPowerOwn.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                P_ZOSP = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    P_ZOSP = Convert.ToDouble(s1);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }
            }

            if ((P_ZOSP < 0.1) || (P_ZOSP > 100))
            {
                MessageBox.Show("Parameter 'Power' out of range 0.1W - 100W");
                return;
            }
            // ........................................................................
            // Частота передатчика связи 

            s1 = tbFreq.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                F_ZOSP = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    F_ZOSP = Convert.ToDouble(s1);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }
            }

            if ((F_ZOSP < 100) || (F_ZOSP > 6000))
            {
                MessageBox.Show("Parameter 'frequency' out of range 100MHz - 6000MHz");
                return;
            }

            F_ZOSP = F_ZOSP * 1000000;
            // ........................................................................
            // Коэффициент усиления пеленгатора 

            s1 = tbCoeffTransmitOpponent.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                K_ZOSP = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    K_ZOSP = Convert.ToDouble(s1);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }
            }

            if ((K_ZOSP < 1) || (K_ZOSP > 6))
            {
                MessageBox.Show("Parameter 'gain' out of range  1 - 6");
                return;
            }
            // ........................................................................
            // Порог чувствительности пеленгатора

            s1 = textBox1.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                iKP_ZOSP = Convert.ToInt32(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    iKP_ZOSP = Convert.ToInt32(s1);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }
            }

            if ((iKP_ZOSP < -140) || (iKP_ZOSP > -100))
            {
                MessageBox.Show("Parameter 'Direction finder threshold sensitivity' out of range -140dBW - -100dBW");
                return;
            }
            // ........................................................................


            // ******************************************************** Ввод параметров

            // !!! Высоты *************************************************************
            // ***
            // !!! OwnHeight_comm введена по кнопке ЦентрЗоны

            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
            // Средняя высота местности

            // DLL
            ClassMapRastr objClassMap1 = new ClassMapRastr();
            MyPoint objMyPoint1 = new MyPoint();
            objMyPoint1.X = (int)GlobalVarLn.tpOwnCoordRect_ZPV.X;
            objMyPoint1.Y = (int)GlobalVarLn.tpOwnCoordRect_ZPV.Y;
            iMiddleHeight_comm = objClassMap1.DefineMiddleHeight_Comm(
                                                                 objMyPoint1,
                                                                 MapForm.RasterMapControl,
                                                                 30000, // R
                                                                 100   // Step
                                                                );


            tbMiddleHeight.Text = iMiddleHeight_comm.ToString();
            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

            // Общая высота СП
            HeightTotalOwn_comm = HeightAntennOwn_comm + OwnHeight_comm;
            // отобразить значение высоты
            ichislo = (long)(HeightTotalOwn_comm);
            tbHeightOwnObject.Text = Convert.ToString(ichislo);

            // Антенна ОП
            //iOpponAnten_comm = Convert.ToInt32(tbOpponentAntenna.Text);
            s1 = tbOpponentAntenna.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                iOpponAnten_comm = Convert.ToInt32(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    iOpponAnten_comm = Convert.ToInt32(s1);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }
            }

            // Общая высота ОП
            HeightOpponent_comm = iMiddleHeight_comm + iOpponAnten_comm;
            // отобразить значение высоты
            ichislo = (long)(HeightOpponent_comm);
            tbHeightOpponObject.Text = Convert.ToString(ichislo);
            // ************************************************************* !!! Высоты

            // Расчет зоны ************************************************************

            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

            // DLL
            Point pnt = new Point((int)GlobalVarLn.XCenter_ZOSP, (int)GlobalVarLn.YCenter_ZOSP);
            ClassMapRastr objClassMap5 = new ClassMapRastr();
            MyPoint objMyPoint5 = new MyPoint();
            objMyPoint5.X = pnt.X;
            objMyPoint5.Y = pnt.Y;

            var LSRpolOSP = objClassMap5.DetectionSP_Zone(
                                                  // Частота передатчика связи,Hz
                                                  F_ZOSP,
                                                  // Мощность передатчика связи, Wt
                                                  P_ZOSP,
                                                  // К-т усиления пеленгатора
                                                  K_ZOSP,
                                                  // Порог чувствительности пеленгатора,dBW
                                                  // !!! <0
                                                  iKP_ZOSP,

                                                  // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                                  MapForm.RasterMapControl,
                                                  // Структура координат центра зоны
                                                  objMyPoint5,
                                                  // HrelSP+Hantsp
                                                  (int)HeightTotalOwn_comm,
                                                  // Hantsp
                                                  (int)HeightAntennOwn_comm,
                                                  // Hrelsp
                                                  (int)OwnHeight_comm,
                                                  // Hsredn
                                                  iMiddleHeight_comm,
                                                  // Hantop
                                                  iOpponAnten_comm,
                                                  // шаг изменения угла в град при расчете ЗПВ
                                                  GlobalVarLn.iStepAngleInput_ZPV,
                                                  // шаг в м при расчете ЗПВ
                                                  GlobalVarLn.iStepLengthInput_ZPV,

                                                  ref GlobalVarLn.iR_ZOSP
                                                  );



            Point objpol = new Point();
            for (int ipol = 0; ipol < LSRpolOSP.Count; ipol++)
            {
                objpol.X = LSRpolOSP[ipol].X;
                objpol.Y = LSRpolOSP[ipol].Y;
                GlobalVarLn.listDetectionZone.Add(objpol);

            }

            textBox2.Text = Convert.ToString(GlobalVarLn.iR_ZOSP);
            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

            // ************************************************************ Расчет зоны

            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
            // DRAW

            // CLASS
            // Pink
            ClassMapRastrDraw.DrawPolygon_Rastr(GlobalVarLn.listDetectionZone, 100, 255, 0, 255);

            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

        } // Зона обнаружения СП (Кнопка Принять)
        //  *************************************************** Зона обнаружения СП

        // Расчет зоны ************************************************************
        // Button "Принять" ЗПВ

        private void bAccept_Click(object sender, EventArgs e)
        {
            String s3 = "";
            
            if (GlobalVarLn.flCoordZPV == 0)
            {
                MessageBox.Show("The center of the zone is not selected");
                return;
            }
            // ----------------------------------------------------------------------

            if (GlobalVarLn.listPointDSR.Count != 0)
                GlobalVarLn.listPointDSR.Clear();
            // ----------------------------------------------------------------------

            // Ввод параметров ********************************************************
            // !!! Координаты центра ЗПВ уже расчитаны и введены по кнопке 'Центр ЗПВ'

            // Высота антенны
            // ??????????????????????
            //HeightAntennOwn_comm = Convert.ToDouble(tbHAnt.Text);
            s3 = tbHAnt.Text;
            try
            {
                if (s3.IndexOf(",") > -1) s3 = s3.Replace(',', '.');
                HeightAntennOwn_comm = Convert.ToDouble(s3);
            }
            catch (SystemException)
            {
                try
                {
                    if (s3.IndexOf(",") > -1) s3 = s3.Replace(',', '.');
                    HeightAntennOwn_comm = Convert.ToDouble(s3);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }
            }

            // ComboBox (Индексы)
            i_HeightOwnObject_comm = cbHeightOwnObject.SelectedIndex; // Средство РП
            i_HeightOpponent_comm = cbHeightOpponObject.SelectedIndex; // Объект РП

            // ******************************************************** Ввод параметров

            // Центр ЗПВ **************************************************************
            // Координаты на местности в м

            //Mewrkator
            GlobalVarLn.tpOwnCoordRect_ZPV.X = (int)GlobalVarLn.XCenter_ZPV;
            GlobalVarLn.tpOwnCoordRect_ZPV.Y = (int)GlobalVarLn.YCenter_ZPV;

            if ((tbXRect.Text == "") || (tbYRect.Text == ""))
            {
                MessageBox.Show("Incorrect data");
                return;
            }

            // ************************************************************** Центр ЗПВ

            // !!! Высоты *************************************************************
            // ***
            // !!! OwnHeight_comm введена по кнопке ЦентрЗоны
            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
            // Средняя высота местности

            // DLL
            ClassMapRastr objClassMap1 = new ClassMapRastr();
            MyPoint objMyPoint1 = new MyPoint();
            objMyPoint1.X = (int)GlobalVarLn.tpOwnCoordRect_ZPV.X;
            objMyPoint1.Y = (int)GlobalVarLn.tpOwnCoordRect_ZPV.Y;
            iMiddleHeight_comm = objClassMap1.DefineMiddleHeight_Comm(
                                                                 objMyPoint1,
                                                                 MapForm.RasterMapControl,
                                                                 30000, // R
                                                                 100   // Step
                                                                );
            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

            tbMiddleHeight.Text = iMiddleHeight_comm.ToString();

            // Общая высота СП
            HeightTotalOwn_comm = HeightAntennOwn_comm + OwnHeight_comm;
            // отобразить значение высоты
            ichislo = (long)(HeightTotalOwn_comm);
            tbHeightOwnObject.Text = Convert.ToString(ichislo);

            // Антенна ОП
            s3 = tbOpponentAntenna.Text;
            try
            {
                if (s3.IndexOf(",") > -1) s3 = s3.Replace(',', '.');
                iOpponAnten_comm = Convert.ToInt32(s3);
            }
            catch (SystemException)
            {
                try
                {
                    if (s3.IndexOf(",") > -1) s3 = s3.Replace(',', '.');
                    iOpponAnten_comm = Convert.ToInt32(s3);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }
            }

            // Общая высота ОП
            HeightOpponent_comm = iMiddleHeight_comm + iOpponAnten_comm;
            // отобразить значение высоты
            ichislo = (long)(HeightOpponent_comm);
            tbHeightOpponObject.Text = Convert.ToString(ichislo);
            // ************************************************************* !!! Высоты

            // *************************************************** H объекта подавления

            // Расчет зоны ************************************************************

            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
            // ДПВ

            // DLL
            ClassMapRastr objClassMap2 = new ClassMapRastr();
            iDSR = objClassMap2.CountDSR(
                                 (int)HeightAntennOwn_comm,    // Hantsp
                                 (int)OwnHeight_comm,          // Hrelsp
                                 iMiddleHeight_comm,           // Hsredn
                                 iOpponAnten_comm              // Hantop
                                         );


            tbDistSightRange.Text = iDSR.ToString();
            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

            // отобразить глубину подавления-> нигде не используется
            // ?????????????????
            tbDepth.Text = GlobalVarLn.iSupDepth.ToString();

            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
            // рассчитать точки полигона

            // DLL
            ClassMapRastr objClassMap3 = new ClassMapRastr();
            MyPoint objMyPoint3 = new MyPoint();
            objMyPoint3.X = (int)GlobalVarLn.tpOwnCoordRect_ZPV.X;
            objMyPoint3.Y = (int)GlobalVarLn.tpOwnCoordRect_ZPV.Y;

            // List<MyPoint>
            var LSRpol= objClassMap3.CreateLineSightPolygon_Circle(
                                                              // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                                              MapForm.RasterMapControl,
                                                              // Структура координат центра зоны
                                                              objMyPoint3,
                                                              // HrelSP+Hantsp
                                                              (int)HeightTotalOwn_comm,
                                                              // Hantsp
                                                              (int)HeightAntennOwn_comm,
                                                              // Hrelsp
                                                              (int)OwnHeight_comm,
                                                              // Hsredn
                                                              iMiddleHeight_comm,
                                                              // Hantop
                                                              iOpponAnten_comm,  
                                                              // шаг изменения угла в град при расчете ЗПВ
                                                              GlobalVarLn.iStepAngleInput_ZPV,
                                                              // шаг в м при расчете ЗПВ
                                                              GlobalVarLn.iStepLengthInput_ZPV
                                                              );

                        //PelIRI[] mass = GlobalVarLn.list_PelIRI.ToArray();
                        //mass[iii].Pel1 = -1;
                        //mass[iii].Pel2 = -1;
                        //GlobalVarLn.list_PelIRI = mass.ToList();

            Point objpol = new Point();
            for(int ipol=0;ipol< LSRpol.Count;ipol++)
            {
                objpol.X = LSRpol[ipol].X;
                objpol.Y = LSRpol[ipol].Y;
                GlobalVarLn.listPointDSR.Add(objpol);

            }


            // отрисовать ЗПВ
            // CLASS
            // Yellow
            ClassMapRastrDraw.DrawPolygon_Rastr(GlobalVarLn.listPointDSR, 100, 255, 255, (byte)0.25 * 255);

            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

            // ************************************************************ Расчет зоны


        } // Button "Принять"
        // ************************************************************ Расчет зоны


        // Отображение в разных СК *************************************************
        private void OtobrSP_Comm()
        {
            //var pos = axaxcMapScreen.MapPlaneToRealGeo(GlobalVarLn.XCenter_ZPV, GlobalVarLn.YCenter_ZPV);

            //tbXRect.Text = pos.X.ToString("F3");
            //tbYRect.Text = pos.Y.ToString("F3");
            tbXRect.Text = GlobalVarLn.LatCenter_ZPV_84.ToString("F3");
            tbYRect.Text = GlobalVarLn.LongCenter_ZPV_84.ToString("F3");

/*
            ichislo = (long)(LatKrR_comm * 1000000);
            dchislo = ((double)ichislo) / 1000000;
            tbBRad.Text = Convert.ToString(dchislo);   // X, карта

            ichislo = (long)(LongKrR_comm * 1000000);
            dchislo = ((double)ichislo) / 1000000;
            tbLRad.Text = Convert.ToString(dchislo);   // X, карта

            //} // IF


            // Градусы (Красовский)
            // CDel
            // !!!Здесь это WGS84
            //else if (gbOwnDegMin.Visible == true)
            //{

            ichislo = (long)(LatKrG_comm * 1000000);
            dchislo = ((double)ichislo) / 1000000;
            tbBMin1.Text = Convert.ToString(dchislo);

            ichislo = (long)(LongKrG_comm * 1000000);
            dchislo = ((double)ichislo) / 1000000;
            tbLMin1.Text = Convert.ToString(dchislo);

            //} // IF


            // Градусы,мин,сек (Красовский)
            // CDel
            // !!!Здесь это WGS84
            //else if (gbOwnDegMinSec.Visible == true)
            // {

            tbBDeg2.Text = Convert.ToString(Lat_Grad_comm);
            tbBMin2.Text = Convert.ToString(Lat_Min_comm);
            ichislo = (long)(Lat_Sec_comm);
            tbBSec.Text = Convert.ToString(ichislo);

            tbLDeg2.Text = Convert.ToString(Long_Grad_comm);
            tbLMin2.Text = Convert.ToString(Long_Min_comm);
            ichislo = (long)(Long_Sec_comm);
            tbLSec.Text = Convert.ToString(ichislo);

            //} // IF
*/

        } // OtobrSP
          // ************************************************* Отображение в разных СК


        // ----------------------------------------------------------------------
        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void cbCenterLSR_SelectedIndexChanged(object sender, EventArgs e)
        {
            ;
        }
        // ----------------------------------------------------------------------


    } // Class
} // Namespace
