﻿using InitTable;
using PC_DLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GrozaMap
{
    public partial class FormTab : Form
    {
        private MapForm mapForm;

        initTable initialTable;
        PropertyDGV propertyDGV;
        TableColumn[] tableColumn;

        // Конструктор *********************************************************** 

        public FormTab(MapForm mapF)
        {
            InitializeComponent();

            mapForm = mapF;

            mapForm.UpdateTableReconFWS += new MapForm.UpdateTableReconFWSEventHandler(mapForm_UpdateTableReconFWS);
        }
        // ***********************************************************  Конструктор

        // Загрузка формы *********************************************************

        private void FormTab_Load(object sender, EventArgs e)
        {
            ClassMapRastrLoadForm.f_Load_FormTab();

        }
        // ********************************************************* Загрузка формы

        // Activated **************************************************************

        private void FormTab_Activated(object sender, EventArgs e)
        {
            GlobalVarLn.fl_Open_objFormTab = 1;

        }
        // ************************************************************** Activated

        // Shown ******************************************************************

        private void FormTab_Shown(object sender, EventArgs e)
        {
            InitTableSource(dgvTab);
        }
        // ****************************************************************** Shown

        // Closing ****************************************************************

        private void FormTab_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

            GlobalVarLn.fl_Open_objFormTab = 0;

        }
        // **************************************************************** Closing

        // Очистка ******************************************************************

        private void bClear_Click(object sender, EventArgs e)
        {
            //dgvTab.Rows.Clear();
            //initialTable.SetCountRows(dgvTab, tableColumn, NameTable.DEFAULT);
            //GlobalVarLn.fllTab = 0;
            //GlobalVarLn.list_air_tab.Clear();

            // --------------------------------------------------------------------
            ClassMapRastrClear.f_Clear_FormTab(initialTable, tableColumn);
            // --------------------------------------------------------------------
            // CLASS
            ClassMapRastrReDrawAll.REDRAW_MAP_CLASS();
            // --------------------------------------------------------------------

        }
        // ****************************************************************** Очистка






        /// <summary>
        /// Add records to datagridview
        /// </summary>
        void mapForm_UpdateTableReconFWS(PC_DLL.TReconFWS[] tReconFWS)
        {
            AirPlane objAirPlane = new AirPlane();


            for (var i = 0; i < tReconFWS.Length; i++)
            {
                //добавить строку в таблицу
                if (dgvTab.InvokeRequired)
                {
                    dgvTab.Invoke((MethodInvoker)(delegate()
                    {
                        if (i >= dgvTab.RowCount)
                            dgvTab.Rows.Add();
                    }));
                }
                else
                {
                    if (i >= dgvTab.RowCount)
                        dgvTab.Rows.Add();
                }
                //if (i >= dgvTab.RowCount)
                //    dgvTab.Rows.Add();

                var reconFWS = tReconFWS[i];
                var row = dgvTab.Rows[i];
                //row.Cells[0].Value = i + 1;
                row.Cells[0].Value = tReconFWS[i].iID;

                row.Cells[1].Value = tReconFWS[i].strName;
                row.Cells[2].Value = (Convert.ToDouble(tReconFWS[i].iFreq) / 1000).ToString("0.0");
                row.Cells[3].Value = (Convert.ToDouble(tReconFWS[i].iWidth) / 1000).ToString("0.0");
                row.Cells[4].Value = tReconFWS[i].sBearingOwn;
                row.Cells[5].Value = tReconFWS[i].sBearingLinked;
                row.Cells[6].Value = tReconFWS[i].tCoord.dLatitude.ToString("0.0000") + "      " + tReconFWS[i].tCoord.dLongitude.ToString("0.0000");
                row.Cells[7].Value = tReconFWS[i].iDistanceOwn;
                row.Cells[8].Value = string.Format("{0}:{1}:{2}", tReconFWS[i].tTime.bHour, tReconFWS[i].tTime.bMinute, tReconFWS[i].tTime.bSecond);
                row.Cells[9].Value = tReconFWS[i].strNote;


// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 

              double lat=0;
              double lng=0;
              String s1="";
              String s2="";
              
              s1=tReconFWS[i].tCoord.dLatitude.ToString("0.0000");
              s2=tReconFWS[i].tCoord.dLongitude.ToString("0.0000");

              lat = Convert.ToDouble(s1);
              lng = Convert.ToDouble(s2);

              objAirPlane.Lat = lat;
              objAirPlane.Long = lng;
              objAirPlane.Num = tReconFWS[i].iID;
              GlobalVarLn.list_air_tab.Add(objAirPlane);


              //ClassMap.f_DrawAirPlane_Tab_Rastr(lat, lng, 0, tReconFWS[i].iID);

                // CLASS
                ClassMapRastrDraw.f_DrawImage(
                              -1,  // Mercator
                              -1,
                              lat,
                              lng,
                              Application.StartupPath + "\\ImagesHelp\\" + "Source.ico",
                              Convert.ToString(tReconFWS[i].iID),
                              0.6
                             );

                GlobalVarLn.fllTab = 1;

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 




            } // FOR
        }


        private void InitTableSource(DataGridView dgv)
        {
            try
            {
                initialTable = new initTable();
                propertyDGV = new PropertyDGV();
                tableColumn = new TableColumn[10];

                double dTotalWidth = (double)pTableSource.Width;
                dgv.Width = pTableSource.Width;

                tableColumn[0].bVisible = true;
                tableColumn[0].fields = "№";
                tableColumn[0].widthField = Convert.ToInt32(0.05 * dTotalWidth);
                tableColumn[0].typeColumn = TypeColumn.TextBoxColumn;

                tableColumn[1].bVisible = true;
                tableColumn[1].fields = "Name";
                tableColumn[1].widthField = Convert.ToInt32(0.1 * dTotalWidth);
                tableColumn[1].typeColumn = TypeColumn.TextBoxColumn;

                tableColumn[2].bVisible = true;
                tableColumn[2].fields = "F [MHz]";
                tableColumn[2].widthField = Convert.ToInt32(0.1 * dTotalWidth);
                tableColumn[2].typeColumn = TypeColumn.TextBoxColumn;

                tableColumn[3].bVisible = true;
                tableColumn[3].fields = "\u0394" + "f [MHz]";
                tableColumn[3].widthField = Convert.ToInt32(0.1 * dTotalWidth);
                tableColumn[3].typeColumn = TypeColumn.TextBoxColumn;

                tableColumn[4].bVisible = true;
                tableColumn[4].fields = "Q1 [deg]";
                tableColumn[4].widthField = Convert.ToInt32(0.08 * dTotalWidth);
                tableColumn[4].typeColumn = TypeColumn.TextBoxColumn;

                tableColumn[5].bVisible = true;
                tableColumn[5].fields = "Q2 [deg]";
                tableColumn[5].widthField = Convert.ToInt32(0.08 * dTotalWidth);
                tableColumn[5].typeColumn = TypeColumn.TextBoxColumn;

                tableColumn[6].bVisible = true;
                tableColumn[6].fields = "Coord [Lat, Long]";
                tableColumn[6].widthField = Convert.ToInt32(0.2 * dTotalWidth);
                tableColumn[6].typeColumn = TypeColumn.TextBoxColumn;

                tableColumn[7].bVisible = true;
                tableColumn[7].fields = "Distance [m]";
                tableColumn[7].widthField = Convert.ToInt32(0.14 * dTotalWidth);
                tableColumn[7].typeColumn = TypeColumn.TextBoxColumn;

                tableColumn[8].bVisible = true;
                tableColumn[8].fields = "Time";
                tableColumn[8].widthField = Convert.ToInt32(0.08 * dTotalWidth);
                tableColumn[8].typeColumn = TypeColumn.TextBoxColumn;

                tableColumn[9].bVisible = true;
                tableColumn[9].fields = "Note";
                tableColumn[9].widthField = Convert.ToInt32(0.21 * dTotalWidth);
                tableColumn[9].typeColumn = TypeColumn.TextBoxColumn;

                propertyDGV.SetPropertyDGV(dgv);

                initialTable.AddColumnToTable(dgv, tableColumn);

                initialTable.SetCountRows(dgv, tableColumn, NameTable.DEFAULT);
            }

            catch
            {

            }
        }
        



        private void bGetFreq_Click(object sender, EventArgs e)
        {
            if (mapForm.clientPC != null)
            {
                try
                {
                    //mapForm.clientPC.SendReconFWS(3);
                    mapForm.clientPC.SendReconFWS((byte)GlobalVarLn.AdressOwn);


                }
                catch
                {

                }
            }
        }


        
    } // Class
} // Namespace
