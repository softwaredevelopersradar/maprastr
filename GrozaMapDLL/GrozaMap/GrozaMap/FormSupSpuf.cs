﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using System.IO;
using System.Runtime.CompilerServices;
using System.Collections.Generic;

using System.Windows.Input;
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;
using Bitmap = System.Drawing.Bitmap;
using Point = System.Drawing.Point;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.ServiceModel;
using System.Diagnostics;
using System.Threading;
using System.Globalization;

using MapRastr;  // DLL

namespace GrozaMap
{
    public partial class FormSupSpuf : Form
    {

        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        //private double LAMBDA;

        // .....................................................................
        // Координаты центра ЗПВ

        // Координаты центра ЗПВ на местности в м
        private double XSP_comm;
        private double YSP_comm;

        private double OwnHeight_comm;
        private double HeightAntennOwn_comm;
        private double HeightTotalOwn_comm;

        // объект подавления
        private double HeightOpponent_comm;
        // Высота антенны противника
        private int iOpponAnten_comm;
        private int iMiddleHeight_comm;

        // ......................................................................
        private double P_supnav;
        private double K_supnav;
        private double iKP_supnav;
        // ......................................................................

        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // Конструктор *********************************************************** 

        public FormSupSpuf()
        {
            InitializeComponent();

            //LAMBDA = 300000;
            // .....................................................................

            OwnHeight_comm = 0;

            HeightAntennOwn_comm = 0; // антенна
            HeightTotalOwn_comm = 0;

            HeightOpponent_comm = 0;
            // Высота антенны противника
            iOpponAnten_comm = 0;
            iMiddleHeight_comm = 0;
            // ......................................................................
            P_supnav = 0;
            K_supnav = 0;
            iKP_supnav = 0;
            // ......................................................................
            //GlobalVarLn.ListJSChangedEvent += OnListJSChanged;
            //MapForm.UpdateDropDownList(cbCenterLSR);
            // ......................................................................

        } // Конструктор
          // ***********************************************************  Конструктор

        //private void OnListJSChanged(object sender, EventArgs e)
        //{
        //    MapForm.UpdateDropDownList(cbCenterLSR);
        //}

        // Загрузка формы *********************************************************

        private void FormSupSpuf_Load(object sender, EventArgs e)
        {
            // ----------------------------------------------------------------------
            ClassMapRastrLoadForm.f_Load_FormSupSpuf();
            // ----------------------------------------------------------------------
            // Изменение списка СП

            GlobalVarLn.ListJSChangedEvent += OnListJSChanged;
            // ----------------------------------------------------------------------

        }
        // ********************************************************* Загрузка формы

        // Изменение списка СП ****************************************************

        private void OnListJSChanged(object sender, EventArgs e)
        {
            MapForm.UpdateDropDownList(GlobalVarLn.objFormSupSpufG.cbCenterLSR);
        }
        // **************************************************** Изменение списка СП

        // Activated **************************************************************

        private void FormSupSpuf_Activated(object sender, EventArgs e)
        {
            GlobalVarLn.fl_Open_objFormSupSpuf = 1;

        }
        // ************************************************************** Activated

        // FormClosing ************************************************************

        private void FormSupSpuf_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

            GlobalVarLn.fl_Open_objFormSupSpuf = 0;

        }
        // ************************************************************ FormClosing

        // Очистка ****************************************************************

        private void bClear_Click(object sender, EventArgs e)
        {
            // --------------------------------------------------------------------
            ClassMapRastrClear.f_Clear_FormSupSpuf();
            // --------------------------------------------------------------------
            // Убрать с карты

            // CLASS
            ClassMapRastrReDrawAll.REDRAW_MAP_CLASS();
            // --------------------------------------------------------------------

        }
        // **************************************************************** Очистка

        // ************************************************************************
        // Обработчик ComboBox "cbChooseSC": Выбор СК
        // ************************************************************************
        private void cbChooseSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            ;
        } // Closing
        // ************************************************************************

        // ************************************************************************
        // Обработчик ComboBox "cbCenterLSR": Выбор SP
        // ************************************************************************
        private void cbCenterLSR_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbCenterLSR.SelectedIndex == 0)
                GlobalVarLn.NumbSP_supnav = "";
            else
                GlobalVarLn.NumbSP_supnav = Convert.ToString(cbCenterLSR.Items[cbCenterLSR.SelectedIndex]);

        } // SP
        // ************************************************************************

        // ------------------------------------------------------------------------
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            K_supnav = Convert.ToDouble(comboBox2.Items[comboBox2.SelectedIndex]);
        }
        // ------------------------------------------------------------------------

        // SP *********************************************************************
        // Обработчик Button1 "Центр ЗПВ": Выбор центра ЗПВ

        private void button1_Click(object sender, EventArgs e)
        {
            // ......................................................................
            ClassMap objClassMap1_ed = new ClassMap();
            ClassMap objClassMap2_ed = new ClassMap();
            ClassMap objClassMap3_ed = new ClassMap();
            // ......................................................................

            // Выбор координат COORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOOR

            // ----------------------------------------------------------------------
            // Мышь на карте

            if (cbCenterLSR.SelectedIndex == 0)
            {
                GlobalVarLn.NumbSP_supnav = "";
                // !!! реальные координаты на местности карты в м (Plane)
                GlobalVarLn.XCenter_supnav = GlobalVarLn.X_Rastr;
                GlobalVarLn.YCenter_supnav = GlobalVarLn.Y_Rastr;

                XSP_comm = GlobalVarLn.X_Rastr;
                YSP_comm = GlobalVarLn.Y_Rastr;
                OwnHeight_comm = GlobalVarLn.H_Rastr;

            }
            // ----------------------------------------------------------------------
            // Выбор из списка СП

            else
            {
                GlobalVarLn.NumbSP_supnav = Convert.ToString(cbCenterLSR.Items[cbCenterLSR.SelectedIndex]);
                var stations = GlobalVarLn.listJS;
                var station = stations[cbCenterLSR.SelectedIndex - 1];

                GlobalVarLn.XCenter_supnav = station.CurrentPosition.x;
                GlobalVarLn.YCenter_supnav = station.CurrentPosition.y;
                XSP_comm = GlobalVarLn.XCenter_supnav;
                YSP_comm = GlobalVarLn.YCenter_supnav;
                OwnHeight_comm = station.CurrentPosition.h;

            }

            tbOwnHeight.Text = Convert.ToString(OwnHeight_comm);

            // COORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOORDCOOR Выбор координат

            // ......................................................................
            GlobalVarLn.tpOwnCoordRect_supnav.X = (int)GlobalVarLn.XCenter_supnav;
            GlobalVarLn.tpOwnCoordRect_supnav.Y = (int)GlobalVarLn.YCenter_supnav;
            // ......................................................................

            // ......................................................................
            // SP на карте

            // CLASS
            ClassMapRastrDraw.f_DrawImage(
                          GlobalVarLn.XCenter_supnav,  // Mercator
                          GlobalVarLn.YCenter_supnav,
                          -1,
                          -1,
                          Application.StartupPath + "\\ImagesHelp\\" + "1.png",
                          "",
                          // Масштаб изображения
                          2
                         );
            // ......................................................................
            GlobalVarLn.fl_supnav = 1;
            GlobalVarLn.flCoord_supnav = 1; // Центр ЗПВ выбран
            // ......................................................................
            // Отображение СП в выбранной СК

            var p = Mercator.ToLonLat(GlobalVarLn.XCenter_supnav, GlobalVarLn.YCenter_supnav);
            GlobalVarLn.LatCenter_supnav_84 = p.Y;
            GlobalVarLn.LongCenter_supnav_84 = p.X;

            OtobrSP_supnav();
            // .......................................................................

        } // Центр зоны
        // ********************************************************************* SP

        // Zone *******************************************************************
        // Кнопка Принять

        private void bAccept_Click(object sender, EventArgs e)
        {
            String s1 = "";

            // ........................................................................
            if (GlobalVarLn.XCenter_supnav == 0 || GlobalVarLn.YCenter_supnav == 0)
            {
                MessageBox.Show("The center of the zone is not selected");
                return;
            }
            // ........................................................................
            if (GlobalVarLn.listNavigationJammingZone.Count != 0)
                GlobalVarLn.listNavigationJammingZone.Clear();
            // ----------------------------------------------------------------------

            // H *********************************************************************

            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
            // Средняя высота местности

            // DLL
            ClassMapRastr objClassMap1 = new ClassMapRastr();
            MyPoint objMyPoint1 = new MyPoint();
            objMyPoint1.X = (int)GlobalVarLn.tpOwnCoordRect_supnav.X;
            objMyPoint1.Y = (int)GlobalVarLn.tpOwnCoordRect_supnav.Y;
            iMiddleHeight_comm = objClassMap1.DefineMiddleHeight_Comm(
                                                                 objMyPoint1,
                                                                 MapForm.RasterMapControl,
                                                                 30000, // R
                                                                 100   // Step
                                                                     );

            // ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

            // Высота антенны
            s1 = tbHAnt.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                HeightAntennOwn_comm = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    HeightAntennOwn_comm = Convert.ToDouble(s1);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }

            }

            HeightTotalOwn_comm = OwnHeight_comm + HeightAntennOwn_comm;

            // OP
            s1 = tbOpponentAntenna.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                iOpponAnten_comm = (int)Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    iOpponAnten_comm = (int)Convert.ToDouble(s1);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }

            }

            HeightOpponent_comm = (double)(iMiddleHeight_comm + iOpponAnten_comm);
            // ********************************************************************* H

            // Ввод параметров ********************************************************
            // !!! Координаты центра ЗПВ уже расчитаны и введены по кнопке 'Центр ЗПВ'

            // ........................................................................
            // Мощность 

            s1 = powerTextbox.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                P_supnav = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    P_supnav = Convert.ToDouble(s1);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }

            }
            if (P_supnav < 10 || P_supnav > 100)
            {
                MessageBox.Show("Parameter 'Power' out of range 10W - 100W");
                return;
            }

            // Коэффициент усиления
            s1 = Convert.ToString(comboBox2.Items[comboBox2.SelectedIndex]);
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                K_supnav = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    K_supnav = Convert.ToDouble(s1);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }

            }
            // ........................................................................
            // Коэффициент подавления

            s1 = textBox1.Text;
            try
            {
                if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                iKP_supnav = Convert.ToDouble(s1);
            }
            catch (SystemException)
            {
                try
                {
                    if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                    iKP_supnav = Convert.ToDouble(s1);
                }
                catch
                {
                    MessageBox.Show("Incorrect data");
                    return;
                }

            }

            if (iKP_supnav < 30 || iKP_supnav > 50)
            {
                MessageBox.Show("Parameter 'Jamming-to-Signal Ratio (J/Sratio)' out of range 30dB - 50dB");
                return;
            }
            // ........................................................................

            // ******************************************************** Ввод параметров

            // Расчет зоны DLL ********************************************************
            // DLL

            GlobalVarLn.fl_supnav = 1;

            ClassMapRastr objClassMap2 = new ClassMapRastr();
            MyPoint objMyPointSP = new MyPoint();

            objMyPointSP.X = GlobalVarLn.tpOwnCoordRect_supnav.X;
            objMyPointSP.Y = GlobalVarLn.tpOwnCoordRect_supnav.Y;

            var PolZoneSuppression = objClassMap2.f_ZoneSuppressionNavigation(

                                                  // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                                  MapForm.RasterMapControl,

                                                  // Координаты СП
                                                  objMyPointSP,

                                                  // Hantsp
                                                  (int)HeightAntennOwn_comm,
                                                  // Hrelsp
                                                  (int)OwnHeight_comm,
                                                  // Hsredn
                                                  iMiddleHeight_comm,
                                                  // Hantop
                                                  (int)iOpponAnten_comm,

                                                  // P, Wt, SP
                                                  P_supnav,
                                                  // К-т усиления, СП
                                                  K_supnav,
                                                  // К-т подавления
                                                  iKP_supnav,

                                                  // шаг изменения угла в град при расчете полигона
                                                  GlobalVarLn.iStepAngleInput_ZPV,
                                                  // шаг в м при расчете ЗПВ
                                                  GlobalVarLn.iStepLengthInput_ZPV,

                                                  // R zone suppression (int)
                                                  ref GlobalVarLn.iR_supnav
                                                                           );


            Point objpol = new Point();
            for (int ipol = 0; ipol < PolZoneSuppression.Count; ipol++)
            {
                objpol.X = PolZoneSuppression[ipol].X;
                objpol.Y = PolZoneSuppression[ipol].Y;
                GlobalVarLn.listNavigationJammingZone.Add(objpol);

            }

            // .........................................................................
            tbDistSightRange.Text = Convert.ToString(GlobalVarLn.iR_supnav);

            // CLASS
            ClassMapRastrReDrawAll.REDRAW_MAP_CLASS();
            // .........................................................................

            // ******************************************************** Расчет зоны DLL

        } // Принять
        // ******************************************************************* Zone

        // ************************************************************************
        // функция отображения координат центра зоны
        // ************************************************************************
        private void OtobrSP_supnav()
        {

            tbXRect.Text = GlobalVarLn.LatCenter_supnav_84.ToString("F3");
            tbYRect.Text = GlobalVarLn.LongCenter_supnav_84.ToString("F3");

        } // OtobrSP_supnav

        // NEW NEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWNEWN

        // ------------------------------------------------------------------------

        private void tbHAnt_TextChanged(object sender, EventArgs e)
        {

        }
        // ------------------------------------------------------------------------


    } // Class
} // Namespace
