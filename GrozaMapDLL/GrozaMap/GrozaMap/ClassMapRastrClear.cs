﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
using System.Windows.Input;

using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;
using Bitmap = System.Drawing.Bitmap;
using Point = System.Drawing.Point;

using InitTable;

namespace GrozaMap
{
    class ClassMapRastrClear
    {

        // Clear_FormSP **********************************************************************
        // Кнопка Clear в FormSP

        public static void f_Clear_FormSP()
        {
            // -------------------------------------------------------------------------------
/*
            tbXRect.Text = "";
            tbYRect.Text = "";
            tbXRect42.Text = "";
            tbYRect42.Text = "";
            tbBRad.Text = "";
            tbLRad.Text = "";
            tbBMin1.Text = "";
            tbLMin1.Text = "";
            tbBDeg2.Text = "";
            tbBMin2.Text = "";
            tbBSec.Text = "";
            tbLDeg2.Text = "";
            tbLMin2.Text = "";
            tbLSec.Text = "";

            tbOwnHeight.Text = "";
            tbNumSP.Text = "";
*/
            // -------------------------------------------------------------------------------
            GlobalVarLn.objFormSPG.chbXY.Checked = false;
            // -------------------------------------------------------------------------------
            // переменные

            GlobalVarLn.blSP_stat = true;
            GlobalVarLn.flEndSP_stat = 1;
            GlobalVarLn.XCenter_SP = 0;
            GlobalVarLn.YCenter_SP = 0;
            GlobalVarLn.HCenter_SP = 0;
            GlobalVarLn.flCoord_SP2 = 0;
            // -------------------------------------------------------------------------------

        }
        // ********************************************************************** Clear_FormSP

        // Clear_FormOB1 *********************************************************************
        // Кнопка Clear в FormOB1

        public static void f_Clear_FormOB1()
        {
            // -------------------------------------------------------------------------------
            GlobalVarLn.blOB1_stat = true;
            GlobalVarLn.flEndOB1_stat = 1;
            // -------------------------------------------------------------------------------
            // переменные

            GlobalVarLn.iOB1_stat = 0;
            GlobalVarLn.X_OB1 = 0;
            GlobalVarLn.Y_OB1 = 0;
            GlobalVarLn.H_OB1 = 0;
            // -------------------------------------------------------------------------------
            GlobalVarLn.list_OB1.Clear();
            GlobalVarLn.list1_OB1.Clear();
            GlobalVarLn.list1_OB1_dubl.Clear();

            // -------------------------------------------------------------------------------
            // Очистка dataGridView1+ Установка 100 строк

            // Очистка dataGridView1
            while (GlobalVarLn.objFormOB1G.dataGridView1.Rows.Count != 0)
                GlobalVarLn.objFormOB1G.dataGridView1.Rows.Remove(GlobalVarLn.objFormOB1G.dataGridView1.Rows[GlobalVarLn.objFormOB1G.dataGridView1.Rows.Count - 1]);

            for (int iyu = 0; iyu < GlobalVarLn.sizeDatOB1_stat; iyu++)
            {
                GlobalVarLn.objFormOB1G.dataGridView1.Rows.Add("", "", "", "");
            }
            // -------------------------------------------------------------------------------

        }
        // ********************************************************************* Clear_FormOB1

        // Clear_FormOB2 *********************************************************************
        // Кнопка Clear в FormOB2

        public static void f_Clear_FormOB2()
        {
            // -------------------------------------------------------------------------------
            GlobalVarLn.blOB2_stat = true;
            GlobalVarLn.flEndOB2_stat = 1;
            // -------------------------------------------------------------------------------
            // переменные

            GlobalVarLn.X_OB2 = 0;
            GlobalVarLn.Y_OB2 = 0;
            GlobalVarLn.H_OB2 = 0;
            // -------------------------------------------------------------------------------
            GlobalVarLn.list_OB2.Clear();
            GlobalVarLn.list1_OB2.Clear();
            GlobalVarLn.list1_OB2_dubl.Clear();

            // -------------------------------------------------------------------------------
            // Очистка dataGridView1+ Установка 100 строк

            // Очистка dataGridView1
            while (GlobalVarLn.objFormOB2G.dataGridView1.Rows.Count != 0)
                GlobalVarLn.objFormOB2G.dataGridView1.Rows.Remove(GlobalVarLn.objFormOB2G.dataGridView1.Rows[GlobalVarLn.objFormOB2G.dataGridView1.Rows.Count - 1]);

            for (int i = 0; i < GlobalVarLn.sizeDatOB2_stat; i++)
            {
                GlobalVarLn.objFormOB2G.dataGridView1.Rows.Add("", "", "", "", "");
            }
            // -------------------------------------------------------------------------------
        }
        // ********************************************************************* Clear_FormOB2

        // Clear_FormLF **********************************************************************
        // Кнопка Clear в FormLF

        public static void f_Clear_FormLF()
        {
            // -------------------------------------------------------------------------------
            GlobalVarLn.blLF1_stat = true;
            GlobalVarLn.flEndLF1_stat = 1;
            // -------------------------------------------------------------------------------
            // переменные

            GlobalVarLn.iLF1_stat = 0;
            GlobalVarLn.X_LF1 = 0;
            GlobalVarLn.Y_LF1 = 0;
            GlobalVarLn.H_LF1 = 0;
            // -------------------------------------------------------------------------------
            GlobalVarLn.list_LF1.Clear();
            // -------------------------------------------------------------------------------
            // Очистка dataGridView1

            while (GlobalVarLn.objFormLFG.dataGridView1.Rows.Count != 0)
                GlobalVarLn.objFormLFG.dataGridView1.Rows.Remove(GlobalVarLn.objFormLFG.dataGridView1.Rows[GlobalVarLn.objFormLFG.dataGridView1.Rows.Count - 1]);

            GlobalVarLn.objFormLFG.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatLF1_stat; i++)
            {
                GlobalVarLn.objFormLFG.dataGridView1.Rows.Add("", "", "");
            }
            // -------------------------------------------------------------------------------

        }
        // ********************************************************************** Clear_FormLF

        // Clear_FormLF2 *********************************************************************
        // Кнопка Clear в FormLF2

        public static void f_Clear_FormLF2()
        {
            // -------------------------------------------------------------------------------
            GlobalVarLn.blLF2_stat = true;
            GlobalVarLn.flEndLF2_stat = 1;
            // -------------------------------------------------------------------------------
            // переменные

            GlobalVarLn.iLF2_stat = 0;
            GlobalVarLn.X_LF2 = 0;
            GlobalVarLn.Y_LF2 = 0;
            GlobalVarLn.H_LF2 = 0;
            // -------------------------------------------------------------------------------
            GlobalVarLn.list_LF2.Clear();
            // -------------------------------------------------------------------------------
            // Очистка dataGridView1

            while (GlobalVarLn.objFormLF2G.dataGridView1.Rows.Count != 0)
                GlobalVarLn.objFormLF2G.dataGridView1.Rows.Remove(GlobalVarLn.objFormLF2G.dataGridView1.Rows[GlobalVarLn.objFormLF2G.dataGridView1.Rows.Count - 1]);

            GlobalVarLn.objFormLF2G.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatLF2_stat; i++)
            {
                GlobalVarLn.objFormLF2G.dataGridView1.Rows.Add("", "", "");
            }
            // -------------------------------------------------------------------------------

        }
        // ********************************************************************* Clear_FormLF2

        // Clear_FormZO **********************************************************************
        // Кнопка Clear в FormZO

        public static void f_Clear_FormZO()
        {
            // -------------------------------------------------------------------------------
            GlobalVarLn.blZO_stat = true;
            GlobalVarLn.flEndZO_stat = 1;
            // -------------------------------------------------------------------------------
            // переменные

            GlobalVarLn.iZO_stat = 0;
            GlobalVarLn.X_ZO = 0;
            GlobalVarLn.Y_ZO = 0;
            GlobalVarLn.H_ZO = 0;
            // -------------------------------------------------------------------------------
            GlobalVarLn.list_ZO.Clear();
            // -------------------------------------------------------------------------------
            // Очистка dataGridView1

            while (GlobalVarLn.objFormZOG.dataGridView1.Rows.Count != 0)
                GlobalVarLn.objFormZOG.dataGridView1.Rows.Remove(GlobalVarLn.objFormZOG.dataGridView1.Rows[GlobalVarLn.objFormZOG.dataGridView1.Rows.Count - 1]);

            GlobalVarLn.objFormZOG.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatZO_stat; i++)
            {
                GlobalVarLn.objFormZOG.dataGridView1.Rows.Add("", "", "");
            }
            // -------------------------------------------------------------------------------

        }
        // ********************************************************************** Clear_FormZO

        // Clear_FormTRO *********************************************************************
        // Кнопка Clear в FormTRO

        public static void f_Clear_FormTRO()
        {
            // -------------------------------------------------------------------------------
            GlobalVarLn.fFZagrTRO = 0;

            GlobalVarLn.blTRO_stat = false;
            GlobalVarLn.flEndTRO_stat = 0;
            // -------------------------------------------------------------------------------
            // SP

            /*
                        GlobalVarLn.objFormSPG.tbXRect.Text = "";
                        GlobalVarLn.objFormSPG.tbYRect.Text = "";
                        GlobalVarLn.objFormSPG.tbXRect42.Text = "";
                        GlobalVarLn.objFormSPG.tbYRect42.Text = "";
                        GlobalVarLn.objFormSPG.tbBRad.Text = "";
                        GlobalVarLn.objFormSPG.tbLRad.Text = "";
                        GlobalVarLn.objFormSPG.tbBMin1.Text = "";
                        GlobalVarLn.objFormSPG.tbLMin1.Text = "";
                        GlobalVarLn.objFormSPG.tbBDeg2.Text = "";
                        GlobalVarLn.objFormSPG.tbBMin2.Text = "";
                        GlobalVarLn.objFormSPG.tbBSec.Text = "";
                        GlobalVarLn.objFormSPG.tbLDeg2.Text = "";
                        GlobalVarLn.objFormSPG.tbLMin2.Text = "";
                        GlobalVarLn.objFormSPG.tbLSec.Text = "";
                        GlobalVarLn.objFormSPG.tbOwnHeight.Text = "";
                        GlobalVarLn.objFormSPG.tbNumSP.Text = "";
                        GlobalVarLn.objFormSPG.chbXY.Checked = false;
                        GlobalVarLn.objFormSPG.gbRect42.Visible = false;
                        GlobalVarLn.objFormSPG.gbRad.Visible = false;
                        GlobalVarLn.objFormSPG.gbDegMin.Visible = false;
                        GlobalVarLn.objFormSPG.gbDegMinSec.Visible = false;

                        GlobalVarLn.objFormSPG.cbChooseSC.SelectedIndex = 0;

            */

            ClassMapRastrClear.f_Clear_FormSP();

            GlobalVarLn.ClearListJS();
            // -------------------------------------------------------------------------------
            // LF

            ClassMapRastrClear.f_Clear_FormLF();
            // -------------------------------------------------------------------------------
            // LF2

            ClassMapRastrClear.f_Clear_FormLF2();
            // -------------------------------------------------------------------------------
            // ZO

            ClassMapRastrClear.f_Clear_FormZO();
            // -------------------------------------------------------------------------------
            // OB1

            ClassMapRastrClear.f_Clear_FormOB1();
            // -------------------------------------------------------------------------------
            // OB2

            ClassMapRastrClear.f_Clear_FormOB2();
            // -------------------------------------------------------------------------------

        }
        // ********************************************************************** Clear_FormTRO

        // Clear_FormWay *********************************************************************
        // Кнопка Clear в FormWay

        public static void f_Clear_FormWay()
        {
            // -------------------------------------------------------------------------------
            // Очистка dataGridView

            while (GlobalVarLn.objFormWayG.dataGridView1.Rows.Count != 0)
                GlobalVarLn.objFormWayG.dataGridView1.Rows.Remove(GlobalVarLn.objFormWayG.dataGridView1.Rows[GlobalVarLn.objFormWayG.dataGridView1.Rows.Count - 1]);

            GlobalVarLn.objFormWayG.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDatWay_stat; i++)
            {
                GlobalVarLn.objFormWayG.dataGridView1.Rows.Add("", "", "");
            }
            // -------------------------------------------------------------------------------
            GlobalVarLn.objFormWayG.chbWay.Checked = true;

            if (GlobalVarLn.objFormWayG.chbWay.Checked == false)
            {
                GlobalVarLn.blWay_stat = false;
                GlobalVarLn.flEndWay_stat = 0;
            }
            else
            {
                GlobalVarLn.blWay_stat = true;
                GlobalVarLn.flEndWay_stat = 1;
            }

            GlobalVarLn.list_way.Clear();

            GlobalVarLn.iW_stat = 0;
            GlobalVarLn.X_StartW_stat = 0;
            GlobalVarLn.Y_StartW_stat = 0;
            GlobalVarLn.LW_stat = 0;
            // -------------------------------------------------------------------------------
            GlobalVarLn.objFormWayG.textBox2.Text = "";   // L
            GlobalVarLn.objFormWayG.textBox1.Text = "";   // N
            // -------------------------------------------------------------------------------

        }
        // ********************************************************************** Clear_FormWay

        // Clear_FormAz1 *********************************************************************
        // Кнопка Clear в FormAz1

        public static void f_Clear_FormAz1()
        {
            // -------------------------------------------------------------------------------
            GlobalVarLn.objFormAz1G.tbXRect.Text = "";
            GlobalVarLn.objFormAz1G.tbYRect.Text = "";
            GlobalVarLn.objFormAz1G.tbOwnHeight.Text = "";
            // -------------------------------------------------------------------------------
            GlobalVarLn.objFormAz1G.chbXY.Checked = false;

            GlobalVarLn.blOB_az1 = true;
            GlobalVarLn.flEndOB_az1 = 1;
            GlobalVarLn.flCoordOB_az1 = 0;
            GlobalVarLn.XCenter_az1 = 0;
            GlobalVarLn.YCenter_az1 = 0;
            GlobalVarLn.HCenter_az1 = 0;

            GlobalVarLn.objFormAz1G.dataGridView1.Rows[0].Cells[3].Value = "";
            GlobalVarLn.objFormAz1G.dataGridView1.Rows[1].Cells[3].Value = "";
            // -------------------------------------------------------------------------------

        }
        // ********************************************************************** Clear_FormAz1

        // Clear_FormLineSightRange1 **********************************************************
        // Кнопка Clear в ЗПВ

        public static void f_Clear_FormLineSightRange1()
        {
            // -------------------------------------------------------------------------------
            GlobalVarLn.objFormLineSightRangeG.tbXRect.Text = "";
            GlobalVarLn.objFormLineSightRangeG.tbYRect.Text = "";
            GlobalVarLn.objFormLineSightRangeG.tbXRect42.Text = "";
            GlobalVarLn.objFormLineSightRangeG.tbYRect42.Text = "";
            GlobalVarLn.objFormLineSightRangeG.tbBRad.Text = "";
            GlobalVarLn.objFormLineSightRangeG.tbLRad.Text = "";
            GlobalVarLn.objFormLineSightRangeG.tbBMin1.Text = "";
            GlobalVarLn.objFormLineSightRangeG.tbLMin1.Text = "";
            GlobalVarLn.objFormLineSightRangeG.tbBDeg2.Text = "";
            GlobalVarLn.objFormLineSightRangeG.tbBMin2.Text = "";
            GlobalVarLn.objFormLineSightRangeG.tbBSec.Text = "";
            GlobalVarLn.objFormLineSightRangeG.tbLDeg2.Text = "";
            GlobalVarLn.objFormLineSightRangeG.tbLMin2.Text = "";
            GlobalVarLn.objFormLineSightRangeG.tbLSec.Text = "";

            GlobalVarLn.objFormLineSightRangeG.tbHeightOwnObject.Text = "";
            GlobalVarLn.objFormLineSightRangeG.tbHeightOpponObject.Text = "";

            GlobalVarLn.objFormLineSightRangeG.tbMiddleHeight.Text = "";
            GlobalVarLn.objFormLineSightRangeG.tbOwnHeight.Text = "";

            GlobalVarLn.objFormLineSightRangeG.textBox2.Text = "";

            // -------------------------------------------------------------------------------
            // переменные

            GlobalVarLn.fl_LineSightRange = 0; // =1-> Выбрали центр ЗПВ
            GlobalVarLn.flCoordZPV = 0; // Отрисовка 
            GlobalVarLn.XCenter_ZPV = 0;
            GlobalVarLn.YCenter_ZPV = 0;
            // -------------------------------------------------------------------------------
            if (GlobalVarLn.listPointDSR.Count != 0)
                GlobalVarLn.listPointDSR.Clear();
            // -------------------------------------------------------------------------------

        }
        // ********************************************************** Clear_FormLineSightRange1

        // Clear_FormLineSightRange2 **********************************************************
        // Кнопка Clear в Зоне обнаружения СП

        public static void f_Clear_FormLineSightRange2()
        {
            // -------------------------------------------------------------------------------
            GlobalVarLn.objFormLineSightRangeG.textBox2.Text = "";
            // -------------------------------------------------------------------------------
            // переменные

            GlobalVarLn.fl_ZOSP = 0;

            if (GlobalVarLn.fl_LineSightRange == 0)
            {
                GlobalVarLn.XCenter_ZOSP = 0;
                GlobalVarLn.YCenter_ZOSP = 0;
            }
            // -------------------------------------------------------------------------------
            if (GlobalVarLn.listDetectionZone.Count != 0)
                GlobalVarLn.listDetectionZone.Clear();
            // -------------------------------------------------------------------------------

        }
        // ********************************************************** Clear_FormLineSightRange2

        // Clear_FormSuppression **************************************************************
        // Кнопка Clear в Зоне подавления радиолиний управления

        public static void f_Clear_FormSuppression()
        {
            // -------------------------------------------------------------------------------
            GlobalVarLn.objFormSuppressionG.cbOwnObject.SelectedIndex = 0;
            // -------------------------------------------------------------------------------

            // SP
            GlobalVarLn.objFormSuppressionG.tbXRect.Text = "";
            GlobalVarLn.objFormSuppressionG.tbYRect.Text = "";
            GlobalVarLn.objFormSuppressionG.tbXRect42.Text = "";
            GlobalVarLn.objFormSuppressionG.tbYRect42.Text = "";
            GlobalVarLn.objFormSuppressionG.tbBRad.Text = "";
            GlobalVarLn.objFormSuppressionG.tbLRad.Text = "";
            GlobalVarLn.objFormSuppressionG.tbBMin1.Text = "";
            GlobalVarLn.objFormSuppressionG.tbLMin1.Text = "";
            GlobalVarLn.objFormSuppressionG.tbBDeg2.Text = "";
            GlobalVarLn.objFormSuppressionG.tbBMin2.Text = "";
            GlobalVarLn.objFormSuppressionG.tbBSec.Text = "";
            GlobalVarLn.objFormSuppressionG.tbLDeg2.Text = "";
            GlobalVarLn.objFormSuppressionG.tbLMin2.Text = "";
            GlobalVarLn.objFormSuppressionG.tbLSec.Text = "";

            // OP
            GlobalVarLn.objFormSuppressionG.tbPt1XRect.Text = "";
            GlobalVarLn.objFormSuppressionG.tbPt1YRect.Text = "";
            GlobalVarLn.objFormSuppressionG.tbPt1XRect42.Text = "";
            GlobalVarLn.objFormSuppressionG.tbPt1YRect42.Text = "";
            GlobalVarLn.objFormSuppressionG.tbPt1BRad.Text = "";
            GlobalVarLn.objFormSuppressionG.tbPt1LRad.Text = "";
            GlobalVarLn.objFormSuppressionG.tbPt1BMin1.Text = "";
            GlobalVarLn.objFormSuppressionG.tbPt1LMin1.Text = "";
            GlobalVarLn.objFormSuppressionG.tbPt1BDeg2.Text = "";
            GlobalVarLn.objFormSuppressionG.tbPt1BMin2.Text = "";
            GlobalVarLn.objFormSuppressionG.tbPt1BSec.Text = "";
            GlobalVarLn.objFormSuppressionG.tbPt1LDeg2.Text = "";
            GlobalVarLn.objFormSuppressionG.tbPt1LMin2.Text = "";
            GlobalVarLn.objFormSuppressionG.tbPt1LSec.Text = "";

            GlobalVarLn.objFormSuppressionG.tbOwnHeight.Text = "";
            GlobalVarLn.objFormSuppressionG.tbHeightOwnObject.Text = "";

            GlobalVarLn.objFormSuppressionG.tbPt1Height.Text = "";

            GlobalVarLn.objFormSuppressionG.tbRadiusZone.Text = "";
            GlobalVarLn.objFormSuppressionG.tbRadiusZone1.Text = "";
            // -------------------------------------------------------------------------------
            GlobalVarLn.objFormSuppressionG.chbXY.Checked = false;
            GlobalVarLn.objFormSuppressionG.chbXY1.Checked = false;
            // -------------------------------------------------------------------------------
            // Переменные

            GlobalVarLn.NumbSP_sup = "";

            GlobalVarLn.fl_Suppression = 0; // Отрисовка зоны
            GlobalVarLn.flCoordSP_sup = 0; // =1-> Выбрали СП
            GlobalVarLn.flCoordOP = 0;
            GlobalVarLn.flA_sup = 0;
            // -------------------------------------------------------------------------------
            if (GlobalVarLn.listControlJammingZone.Count != 0)
                GlobalVarLn.listControlJammingZone.Clear();
            // -------------------------------------------------------------------------------

        }
        // ************************************************************** Clear_FormSuppression

        // Clear_FormSupSpuf ******************************************************************
        // Кнопка Clear в Зоне подавления навигации

        public static void f_Clear_FormSupSpuf()
        {
            // -------------------------------------------------------------------------------
            GlobalVarLn.objFormSupSpufG.tbXRect.Text = "";
            GlobalVarLn.objFormSupSpufG.tbYRect.Text = "";
            GlobalVarLn.objFormSupSpufG.tbOwnHeight.Text = "";
            GlobalVarLn.objFormSupSpufG.tbDistSightRange.Text = "";
            // -------------------------------------------------------------------------------
            GlobalVarLn.NumbSP_supnav = "";
            // -------------------------------------------------------------------------------
            // переменные

            GlobalVarLn.fl_supnav = 0; // =1-> Выбрали центр ЗПВ
            GlobalVarLn.flCoord_supnav = 0; // Отрисовка 
            GlobalVarLn.XCenter_supnav = 0;
            GlobalVarLn.YCenter_supnav = 0;
            // -------------------------------------------------------------------------------
            if (GlobalVarLn.listNavigationJammingZone.Count != 0)
                GlobalVarLn.listNavigationJammingZone.Clear();
            // -------------------------------------------------------------------------------

        }
        // ****************************************************************** Clear_FormSupSpuf

        // Clear_FormSpuf *********************************************************************
        // Кнопка Clear в Зоне спуфинга

        public static void f_Clear_FormSpuf()
        {
            // -------------------------------------------------------------------------------
            GlobalVarLn.objFormSpufG.tbXRect.Text = "";
            GlobalVarLn.objFormSpufG.tbYRect.Text = "";
            GlobalVarLn.objFormSpufG.tbOwnHeight.Text = "";
            GlobalVarLn.objFormSpufG.tbR1.Text = "";
            GlobalVarLn.objFormSpufG.tbR2.Text = "";
            // -------------------------------------------------------------------------------
            GlobalVarLn.NumbSP_spf = "";
            // -------------------------------------------------------------------------------
            // переменные

            GlobalVarLn.fl_spf = 0; // =1-> Выбрали центр ЗПВ
            GlobalVarLn.flCoord_spf = 0; // Отрисовка 
            GlobalVarLn.XCenter_spf = 0;
            GlobalVarLn.YCenter_spf = 0;

            GlobalVarLn.flS_spf = 0;
            GlobalVarLn.flJ_spf = 0;
            // -------------------------------------------------------------------------------
            if (GlobalVarLn.listSpoofingZone.Count != 0)
                GlobalVarLn.listSpoofingZone.Clear();
            if (GlobalVarLn.listSpoofingJamZone.Count != 0)
                GlobalVarLn.listSpoofingJamZone.Clear();
            // -------------------------------------------------------------------------------
            GlobalVarLn.objFormSpufG.chbS.Checked = false;
            GlobalVarLn.objFormSpufG.chbJ.Checked = false;
            // -------------------------------------------------------------------------------

        }
        // ********************************************************************* Clear_FormSpuf

        // Clear_FormSPFB *********************************************************************
        // Кнопка Clear в окне ввода частот и секторов радиоразведки

        public static void f_Clear_FormSPFB()
        {
            // -------------------------------------------------------------------------------
            GlobalVarLn.objFormSPFBG.cbChooseSC.SelectedIndex = 0;
            GlobalVarLn.NumbSP_f1 = Convert.ToString(GlobalVarLn.objFormSPFBG.cbChooseSC.Items[GlobalVarLn.objFormSPFBG.cbChooseSC.SelectedIndex]);
            // -------------------------------------------------------------------------------
            // Очистка dataGridView

            while (GlobalVarLn.objFormSPFBG.dataGridView1.Rows.Count != 0)
                GlobalVarLn.objFormSPFBG.dataGridView1.Rows.Remove(GlobalVarLn.objFormSPFBG.dataGridView1.Rows[GlobalVarLn.objFormSPFBG.dataGridView1.Rows.Count - 1]);

            GlobalVarLn.objFormSPFBG.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDat_f1; i++)
            {
                GlobalVarLn.objFormSPFBG.dataGridView1.Rows.Add("", "", "", "");
            }
            // -------------------------------------------------------------------------------
            GlobalVarLn.flF_f1 = 1;
            GlobalVarLn.Az1_f1 = 0;
            GlobalVarLn.Az2_f1 = 0;
            GlobalVarLn.Az_f1 = 0;
            GlobalVarLn.flAz1_f1 = 0;
            GlobalVarLn.flAz2_f1 = 0;
            // -------------------------------------------------------------------------------
            GlobalVarLn.list1_f1.Clear();
            // -------------------------------------------------------------------------------

        }
        // ********************************************************************* Clear_FormSPFB

        // Clear_FormSPFB1 ********************************************************************
        // Кнопка Clear в окне ввода частот и секторов радиоподавления

        public static void f_Clear_FormSPFB1()
        {
            // -------------------------------------------------------------------------------
            GlobalVarLn.objFormSPFB1G.cbChooseSC.SelectedIndex = 0;
            GlobalVarLn.NumbSP_f2 = Convert.ToString(GlobalVarLn.objFormSPFB1G.cbChooseSC.Items[GlobalVarLn.objFormSPFB1G.cbChooseSC.SelectedIndex]);
            // -------------------------------------------------------------------------------
            // Очистка dataGridView

            while (GlobalVarLn.objFormSPFB1G.dataGridView1.Rows.Count != 0)
                GlobalVarLn.objFormSPFB1G.dataGridView1.Rows.Remove(GlobalVarLn.objFormSPFB1G.dataGridView1.Rows[GlobalVarLn.objFormSPFB1G.dataGridView1.Rows.Count - 1]);

            GlobalVarLn.objFormSPFB1G.dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDat_f2; i++)
            {
                GlobalVarLn.objFormSPFB1G.dataGridView1.Rows.Add("", "", "", "");
            }
            // -------------------------------------------------------------------------------
            GlobalVarLn.flF_f2 = 1;
            GlobalVarLn.Az1_f2 = 0;
            GlobalVarLn.Az2_f2 = 0;
            GlobalVarLn.Az_f2 = 0;
            GlobalVarLn.flAz1_f2 = 0;
            GlobalVarLn.flAz2_f2 = 0;
            // -------------------------------------------------------------------------------
            GlobalVarLn.list1_f2.Clear();
            // -------------------------------------------------------------------------------

        }
        // ******************************************************************** Clear_FormSPFB1

        // Clear_FormSPFB2_1 ******************************************************************
        // Кнопка Clear в окне ввода запрещенных частот для радиоподавления

        public static void f_Clear_FormSPFB2_1()
        {
            // -------------------------------------------------------------------------------
            //cbChooseSC.SelectedIndex = 0;
            //GlobalVarLn.NumbSP_f3 = Convert.ToString(cbChooseSC.Items[cbChooseSC.SelectedIndex]);
            // -------------------------------------------------------------------------------
            // Очистка dataGridView

            while (GlobalVarLn.objFormSPFB2G.dgvFreqForbid.Rows.Count != 0)
                GlobalVarLn.objFormSPFB2G.dgvFreqForbid.Rows.Remove(GlobalVarLn.objFormSPFB2G.dgvFreqForbid.Rows[GlobalVarLn.objFormSPFB2G.dgvFreqForbid.Rows.Count - 1]);
            GlobalVarLn.objFormSPFB2G.dgvFreqForbid.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDat_f3; i++)
            {
                GlobalVarLn.objFormSPFB2G.dgvFreqForbid.Rows.Add("", "");
            }
            // -------------------------------------------------------------------------------
            GlobalVarLn.flF_f3 = 1;
            // -------------------------------------------------------------------------------
            GlobalVarLn.list1_f3.Clear();
            // -------------------------------------------------------------------------------

        }
        // ****************************************************************** Clear_FormSPFB2_1

        // Clear_FormSPFB2_2 ******************************************************************
        // Кнопка Clear в окне ввода частот особого внимания

        public static void f_Clear_FormSPFB2_2()
        {
            // -------------------------------------------------------------------------------
            // Очистка dataGridView

            while (GlobalVarLn.objFormSPFB2G.dgvFreqSpec.Rows.Count != 0)
                GlobalVarLn.objFormSPFB2G.dgvFreqSpec.Rows.Remove(GlobalVarLn.objFormSPFB2G.dgvFreqSpec.Rows[GlobalVarLn.objFormSPFB2G.dgvFreqSpec.Rows.Count - 1]);
            GlobalVarLn.objFormSPFB2G.dgvFreqSpec.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDat_f3; i++)
            {
                GlobalVarLn.objFormSPFB2G.dgvFreqSpec.Rows.Add("", "");
            }
            // -------------------------------------------------------------------------------
            GlobalVarLn.flF1_f3 = 1;
            // -------------------------------------------------------------------------------
            GlobalVarLn.list2_f3.Clear();
            // -------------------------------------------------------------------------------

        }
        // ****************************************************************** Clear_FormSPFB2_2

        // Clear_FormTab **********************************************************************
        // Кнопка Clear в окне принятия таблицы

        public static void f_Clear_FormTab(
                                           initTable initialTable,
                                           TableColumn[] tableColumn
                                          )
        {

            // -------------------------------------------------------------------------------
            GlobalVarLn.objFormTabG.dgvTab.Rows.Clear();

            initialTable.SetCountRows(GlobalVarLn.objFormTabG.dgvTab, tableColumn, NameTable.DEFAULT);

            GlobalVarLn.fllTab = 0;
            GlobalVarLn.list_air_tab.Clear();
            // -------------------------------------------------------------------------------
        }
        // ********************************************************************** Clear_FormTab





    } // Class
} // Namespace
