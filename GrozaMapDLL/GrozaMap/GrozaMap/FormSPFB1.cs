﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;

using System.Windows.Input;
using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;
using Bitmap = System.Drawing.Bitmap;

namespace GrozaMap
{
    public partial class FormSPFB1 : Form
    {
        // Переменные VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR 
        private double dchislo;
        private long ichislo;
        //private double LAMBDA;

        // VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR VAR Переменные

        // Конструктор *********************************************************** 

        public FormSPFB1()
        {
            InitializeComponent();

            dchislo = 0;
            ichislo = 0;
            //LAMBDA = 300000;

        } // Конструктор
        // ***********************************************************  Конструктор

        // Загрузка формы *********************************************************

        private void FormSPFB1_Load(object sender, EventArgs e)
        {

            ClassMapRastrLoadForm.f_Load_FormSPFB1();

            GlobalVarLn.ListJSChangedEvent += OnListJSChanged;

            MapForm.UpdateDropDownList1(cbChooseSC);

        }
        // ********************************************************* Загрузка формы

        // ChangeSP *****************************************************************
        // Обработчик события при изменении списка СП

        private void OnListJSChanged(object sender, EventArgs e)
        {
            // Обновление списка СП (без X,Y в списке)
            MapForm.UpdateDropDownList1(cbChooseSC);
        }
        // ***************************************************************** ChangeSP

        // Активизировать форму *****************************************************

        private void FormSPFB1_Activated(object sender, EventArgs e)
        {
            GlobalVarLn.flF_f2 = 1;

            GlobalVarLn.fl_Open_objFormSPFB1 = 1;

        }
        // ***************************************************** Активизировать форму

        // Закрыть форму ************************************************************

        private void FormSPFB1_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Hide();

            // приостановить обработку, если идет
            GlobalVarLn.flF_f2 = 0;

            GlobalVarLn.fl_Open_objFormSPFB1 = 0;

        }
        // ************************************************************ Закрыть форму

        // Очистка ******************************************************************

        private void bClear_Click(object sender, EventArgs e)
        {
            // --------------------------------------------------------------------
            ClassMapRastrClear.f_Clear_FormSPFB1();
            // --------------------------------------------------------------------

        }
        // ****************************************************************** Очистка

        // ************************************************************************
        // Обработчик ComboBox "cbChooseSC": Выбор SP
        // ************************************************************************
        private void cbChooseSC_SelectedIndexChanged(object sender, EventArgs e)
        {
            GlobalVarLn.NumbSP_f2 = Convert.ToString(cbChooseSC.Items[cbChooseSC.SelectedIndex]);

        } //Select_SP
        // ************************************************************************

        // ************************************************************************
        // Save in file
        // ************************************************************************
        private void bAccept_Click(object sender, EventArgs e)
        {
            int i_tmp = 0;
            int i_tmp1 = 0;
            String s1 = "";
            // -----------------------------------------------------------------------------------------
            String strFileName;

            //strFileName = "FBSPRP.txt";
            //0219
            strFileName = Application.StartupPath + "\\SaveInFiles\\BandsSectJamm.txt";

            StreamReader srFile1;
            GlobalVarLn.NumbSP_f2 = cbChooseSC.Text;

            FBSP objFBSP = new FBSP();
            objFBSP.mfb = new FB[GlobalVarLn.sizeDiap_f2 + 10];

            GlobalVarLn.list1_f2.Clear();
            GlobalVarLn.iSP_f2 = 0;
            // ***********************************************************************************************
            // файл был

            try
            {
                // ----------------------------------------------------------------------------------------
                srFile1 = new StreamReader(strFileName);
                srFile1.Close();
                ReadFileFBSP_f2(0); // Читаем старый файл
                GlobalVarLn.iSP_f2 = (uint)GlobalVarLn.list1_f2.Count;

                for (i_tmp = 0; i_tmp < GlobalVarLn.list1_f2.Count; i_tmp++)
                {
                    // Убираем СП с таким же номером в старом файле
                    if (String.Compare(GlobalVarLn.list1_f2[i_tmp].NumbSP, GlobalVarLn.NumbSP_f2) == 0)
                    {
                        GlobalVarLn.list1_f2.RemoveAt(i_tmp);
                        GlobalVarLn.iSP_f2 -= 1;
                    }
                }
                // ----------------------------------------------------------------------------------------
                objFBSP.NumbSP = GlobalVarLn.NumbSP_f2;
                i_tmp = 0;

                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILE
                while (
                        (dataGridView1.Rows[(int)(i_tmp)].Cells[0].Value != "") &&
                        (dataGridView1.Rows[(int)(i_tmp)].Cells[0].Value != null)
                      )
                {
                    // ...........................................................................................
                    // Fmin

                    //objFBSP.mfb[i_tmp].Fmin = Convert.ToDouble(dataGridView1.Rows[(int)(i_tmp)].Cells[0].Value);

                    s1 = Convert.ToString(dataGridView1.Rows[(int)(i_tmp)].Cells[0].Value);
                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        objFBSP.mfb[i_tmp].Fmin = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            objFBSP.mfb[i_tmp].Fmin = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            MessageBox.Show("Incorrect data");
                            return;
                        }

                    }

                    // seg
                    ichislo = (long)(objFBSP.mfb[i_tmp].Fmin * 10);
                    dchislo = ((double)ichislo) / 10;
                    objFBSP.mfb[i_tmp].Fmin = dchislo;

                    // ...........................................................................................
                    // Fmax

                    s1 = Convert.ToString(dataGridView1.Rows[(int)(i_tmp)].Cells[1].Value);

                    if ((s1 == "") || (s1 == null))
                    {
                        MessageBox.Show("Incorrect data");
                        srFile1.Close();
                        return;
                    }

                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        objFBSP.mfb[i_tmp].Fmax = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            objFBSP.mfb[i_tmp].Fmax = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            MessageBox.Show("Incorrect data");
                            return;
                        }

                    }

                    // seg
                    ichislo = (long)(objFBSP.mfb[i_tmp].Fmax * 10);
                    dchislo = ((double)ichislo) / 10;
                    objFBSP.mfb[i_tmp].Fmax = dchislo;

                    // ...........................................................................................
                    // Bmin

                    s1 = Convert.ToString(dataGridView1.Rows[(int)(i_tmp)].Cells[2].Value);

                    if ((s1 == "") || (s1 == null))
                    {
                        MessageBox.Show("Incorrect data");
                        srFile1.Close();
                        return;
                    }

                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        objFBSP.mfb[i_tmp].Bmin = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            objFBSP.mfb[i_tmp].Bmin = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            MessageBox.Show("Incorrect data");
                            return;
                        }

                    }

                    // seg
                    ichislo = (long)(objFBSP.mfb[i_tmp].Bmin * 10);
                    dchislo = ((double)ichislo) / 10;
                    objFBSP.mfb[i_tmp].Bmin = dchislo;

                    // ...........................................................................................
                    // Bmax

                    s1 = Convert.ToString(dataGridView1.Rows[(int)(i_tmp)].Cells[3].Value);

                    if ((s1 == "") || (s1 == null))
                    {
                        MessageBox.Show("Incorrect data");
                        srFile1.Close();
                        return;
                    }

                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        objFBSP.mfb[i_tmp].Bmax = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            objFBSP.mfb[i_tmp].Bmax = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            MessageBox.Show("Incorrect data");
                            return;
                        }

                    }

                    // seg
                    ichislo = (long)(objFBSP.mfb[i_tmp].Bmax * 10);
                    dchislo = ((double)ichislo) / 10;
                    objFBSP.mfb[i_tmp].Bmax = dchislo;

                    // ...........................................................................................
                    // seg

                    if (
                       (objFBSP.mfb[i_tmp].Fmin > objFBSP.mfb[i_tmp].Fmax) 
                        //(objFBSP.mfb[i_tmp].Bmin > objFBSP.mfb[i_tmp].Bmax)
                       )
                    {
                        MessageBox.Show("Incorrect data");
                        srFile1.Close();
                        return;
                    }

                    if (
                       (objFBSP.mfb[i_tmp].Fmin < 100) ||
                        (objFBSP.mfb[i_tmp].Fmin > 6000) ||
                        (objFBSP.mfb[i_tmp].Fmax < 100) ||
                        (objFBSP.mfb[i_tmp].Fmax > 6000)
                       )
                    {
                        MessageBox.Show("Parameter 'frequency' out of range 100MHz-6000 MHz");
                        srFile1.Close();
                        return;
                    }

                    if (
                       (objFBSP.mfb[i_tmp].Bmin < 0) ||
                        (objFBSP.mfb[i_tmp].Bmin > 360) ||
                        (objFBSP.mfb[i_tmp].Bmax < 0) ||
                        (objFBSP.mfb[i_tmp].Bmax > 360)
                       )
                    {
                        MessageBox.Show("Parameter 'azimuth' out of range 0deg-360deg");
                        srFile1.Close();
                        return;
                    }
                    // ...........................................................................................

                    i_tmp += 1;
                    if (i_tmp > GlobalVarLn.sizeDiap_f2)
                    {
                        MessageBox.Show("Invalid number of frequency bands");
                        break;
                    }
                } // WHILE
                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILE

                objFBSP.NumbDiap = i_tmp;
                // ----------------------------------------------------------------------------------------
                GlobalVarLn.list1_f2.Add(objFBSP);
                GlobalVarLn.iSP_f2 += 1;
                // ----------------------------------------------------------------------------------------
                StreamWriter srFile = new StreamWriter(strFileName);
                // ----------------------------------------------------------------------------------------
                // Запись в файл

                for (i_tmp = 0; i_tmp < GlobalVarLn.list1_f2.Count; i_tmp++)
                {
                    // SPi
                    srFile.WriteLine("Numb =" + Convert.ToString(GlobalVarLn.list1_f2[i_tmp].NumbSP));

                    for (i_tmp1 = 0; i_tmp1 < GlobalVarLn.list1_f2[i_tmp].NumbDiap; i_tmp1++)
                    {
                        srFile.WriteLine("Fmin =" + Convert.ToString(GlobalVarLn.list1_f2[i_tmp].mfb[i_tmp1].Fmin));
                        srFile.WriteLine("Fmax =" + Convert.ToString(GlobalVarLn.list1_f2[i_tmp].mfb[i_tmp1].Fmax));
                        srFile.WriteLine("Bmin =" + Convert.ToString(GlobalVarLn.list1_f2[i_tmp].mfb[i_tmp1].Bmin));
                        srFile.WriteLine("Bmax =" + Convert.ToString(GlobalVarLn.list1_f2[i_tmp].mfb[i_tmp1].Bmax));
                    }

                }

                // ----------------------------------------------------------------------------------------
                srFile.Close();
            }
            // ***********************************************************************************************
            // файла не было

            catch
            {
                StreamWriter srFile;
                // ...........................................................................................
                //StreamWriter srFile = new StreamWriter(strFileName);
                try
                {
                    srFile = new StreamWriter(strFileName);
                }
                catch
                {
                    MessageBox.Show("Can’t save file");
                    return;
                }


                objFBSP.NumbSP = GlobalVarLn.NumbSP_f2;
                i_tmp = 0;
                // ...........................................................................................

                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILE
                while (
                       (dataGridView1.Rows[(int)(i_tmp)].Cells[0].Value != "") &&
                       (dataGridView1.Rows[(int)(i_tmp)].Cells[0].Value != null)
                      )
                {
                    // ...........................................................................................
                    // Fmin

                    s1 = Convert.ToString(dataGridView1.Rows[(int)(i_tmp)].Cells[0].Value);

                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        objFBSP.mfb[i_tmp].Fmin = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            objFBSP.mfb[i_tmp].Fmin = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            MessageBox.Show("Incorrect data");
                            return;
                        }

                    }

                    // seg
                    ichislo = (long)(objFBSP.mfb[i_tmp].Fmin * 10);
                    dchislo = ((double)ichislo) / 10;
                    objFBSP.mfb[i_tmp].Fmin = dchislo;

                    // ...........................................................................................
                    // Fmax

                    s1 = Convert.ToString(dataGridView1.Rows[(int)(i_tmp)].Cells[1].Value);

                    if ((s1 == "") || (s1 == null))
                    {
                        MessageBox.Show("Incorrect data");
                        srFile.Close();
                        return;
                    }

                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        objFBSP.mfb[i_tmp].Fmax = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            objFBSP.mfb[i_tmp].Fmax = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            MessageBox.Show("Incorrect data");
                            return;
                        }

                    }

                    // seg
                    ichislo = (long)(objFBSP.mfb[i_tmp].Fmax * 10);
                    dchislo = ((double)ichislo) / 10;
                    objFBSP.mfb[i_tmp].Fmax = dchislo;

                    // ...........................................................................................
                    // Bmin

                    s1 = Convert.ToString(dataGridView1.Rows[(int)(i_tmp)].Cells[2].Value);

                    if ((s1 == "") || (s1 == null))
                    {
                        MessageBox.Show("Incorrect data");
                        srFile.Close();
                        return;
                    }

                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        objFBSP.mfb[i_tmp].Bmin = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            objFBSP.mfb[i_tmp].Bmin = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            MessageBox.Show("Incorrect data");
                            return;
                        }

                    }

                    // seg
                    ichislo = (long)(objFBSP.mfb[i_tmp].Bmin * 10);
                    dchislo = ((double)ichislo) / 10;
                    objFBSP.mfb[i_tmp].Bmin = dchislo;

                    // ...........................................................................................
                    // Bmax

                    //objFBSP.mfb[i_tmp].Bmax = Convert.ToDouble(dataGridView1.Rows[(int)(i_tmp)].Cells[3].Value);

                    s1 = Convert.ToString(dataGridView1.Rows[(int)(i_tmp)].Cells[3].Value);

                    if ((s1 == "") || (s1 == null))
                    {
                        MessageBox.Show("Incorrect data");
                        srFile.Close();
                        return;
                    }

                    try
                    {
                        if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                        objFBSP.mfb[i_tmp].Bmax = Convert.ToDouble(s1);
                    }
                    catch (SystemException)
                    {
                        try
                        {
                            if (s1.IndexOf(",") > -1) s1 = s1.Replace(',', '.');
                            objFBSP.mfb[i_tmp].Bmax = Convert.ToDouble(s1);
                        }
                        catch
                        {
                            MessageBox.Show("Incorrect data");
                            return;
                        }

                    }

                    // seg
                    ichislo = (long)(objFBSP.mfb[i_tmp].Bmax * 10);
                    dchislo = ((double)ichislo) / 10;
                    objFBSP.mfb[i_tmp].Bmax = dchislo;

                    // ...........................................................................................
                    // seg

                    if (
                       (objFBSP.mfb[i_tmp].Fmin > objFBSP.mfb[i_tmp].Fmax) 
                       // (objFBSP.mfb[i_tmp].Bmin > objFBSP.mfb[i_tmp].Bmax)
                       )
                    {
                        MessageBox.Show("Incorrect data");
                        srFile.Close();
                        return;
                    }

                    if (
                       (objFBSP.mfb[i_tmp].Fmin < 100) ||
                        (objFBSP.mfb[i_tmp].Fmin > 6000) ||
                        (objFBSP.mfb[i_tmp].Fmax < 100) ||
                        (objFBSP.mfb[i_tmp].Fmax > 6000)
                       )
                    {
                        MessageBox.Show("Parameter 'frequency' out of range 100MHz-6000 MHz");
                        srFile.Close();
                        return;
                    }

                    if (
                       (objFBSP.mfb[i_tmp].Bmin < 0) ||
                        (objFBSP.mfb[i_tmp].Bmin > 360) ||
                        (objFBSP.mfb[i_tmp].Bmax < 0) ||
                        (objFBSP.mfb[i_tmp].Bmax > 360)
                       )
                    {
                        MessageBox.Show("Parameter 'azimuth' out of range 0deg-360deg");
                        srFile.Close();
                        return;
                    }
                    // ...........................................................................................

                    i_tmp += 1;
                    if (i_tmp > GlobalVarLn.sizeDiap_f2)
                    {
                        MessageBox.Show("Invalid number of frequency bands");
                        break;
                    }
                } // WHILE
                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILE

                objFBSP.NumbDiap = i_tmp;

                // SPi
                srFile.WriteLine("Numb =" + Convert.ToString(objFBSP.NumbSP));

                for (i_tmp = 0; i_tmp < objFBSP.NumbDiap; i_tmp++)
                {

                    srFile.WriteLine("Fmin =" + Convert.ToString(objFBSP.mfb[i_tmp].Fmin));
                    srFile.WriteLine("Fmax =" + Convert.ToString(objFBSP.mfb[i_tmp].Fmax));
                    srFile.WriteLine("Bmin =" + Convert.ToString(objFBSP.mfb[i_tmp].Bmin));
                    srFile.WriteLine("Bmax =" + Convert.ToString(objFBSP.mfb[i_tmp].Bmax));

                }

                srFile.Close();
            } // catch
            // ***********************************************************************************************

        } // Save in file
        // ************************************************************************

        // ************************************************************************
        // Read Tek
        // ************************************************************************
        private void button2_Click(object sender, EventArgs e)
        {
            // ----------------------------------------------------------------------
            // Очистка dataGridView

            while (dataGridView1.Rows.Count != 0)
                dataGridView1.Rows.Remove(dataGridView1.Rows[dataGridView1.Rows.Count - 1]);

            dataGridView1.ClearSelection();
            for (int i = 0; i < GlobalVarLn.sizeDat_f2; i++)
            {
                dataGridView1.Rows.Add("", "", "", "");
            }
            // .......................................................................

            GlobalVarLn.flrd_f2 = 0;
            ReadFileFBSP_f2(1); // Читаем старый файл
            GlobalVarLn.list1_f2.Clear();

            if (GlobalVarLn.flrd_f2 == 0)
            {
                MessageBox.Show("No information");
                return;
            }

        } // Read tek
        // ************************************************************************

        // ФУНКЦИИ ****************************************************************

        // ****************************************************************************************
        // Обработка нажатия левой кнопки мыши при отрисовке OB1
        //
        // Входные параметры:
        // X - X, m на местности
        // Y - Y, m
        // ****************************************************************************************
        public void f_SPFB_f2()
        {
            if ((GlobalVarLn.flAz1_f2 == 0) && (chbXY.Checked == true))

            {
                GlobalVarLn.flAz1_f2 = 1;
                GlobalVarLn.Az1_f2 = GlobalVarLn.Az_f2;

                ichislo = (long)(GlobalVarLn.Az1_f2 * 100);
                dchislo = ((double)ichislo) / 100;
                dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[2].Value = (int)GlobalVarLn.Az1_f2; // seg

            }
            else if ((GlobalVarLn.flAz1_f2 == 1) && (GlobalVarLn.flAz2_f2==0) && (chbXY.Checked == true))

            {
                GlobalVarLn.flAz2_f2 = 1;
                GlobalVarLn.Az2_f2 = GlobalVarLn.Az_f2;

                ichislo = (long)(GlobalVarLn.Az2_f2 * 100);
                dchislo = ((double)ichislo) / 100;
                dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[3].Value = (int)GlobalVarLn.Az2_f2;// seg

                GlobalVarLn.flAz1_f2 = 0;
                GlobalVarLn.flAz2_f2 = 0;
                GlobalVarLn.Az1_f2 = 0;
                GlobalVarLn.Az2_f2 = 0;

                // Убрать с карты
                // WORK
                //MapForm.REDRAW_MAP();
                // CLASS
                ClassMapRastrReDrawAll.REDRAW_MAP_CLASS();

                chbXY.Checked = false;

            }
        }
        // ************************************************************************
        // Change Azimuth_enter from map
        // ************************************************************************

        private void chbXY_CheckedChanged(object sender, EventArgs e)
        {
            //0221
            // !!! После ввода 1-го азимута отжали птичку
            if ((GlobalVarLn.flAz1_f2 == 1) && (chbXY.Checked == false))
            {
                GlobalVarLn.flAz1_f2 = 0;
                GlobalVarLn.flAz2_f2 = 0;
                GlobalVarLn.Az1_f2 = 0;
                GlobalVarLn.Az2_f2 = 0;

                // Убрать с карты !!! Заменить в растровой
                // WORK
                //MapForm.REDRAW_MAP();
                // CLASS
                ClassMapRastrReDrawAll.REDRAW_MAP_CLASS();
            }

        } // Change Azimuth_enter from map
        // ************************************************************************

        // ************************************************************************
        // функция чтения файла по FBСП 
        // ************************************************************************
        public void ReadFileFBSP_f2(int fl)
        {
            // ------------------------------------------------------------------------------------
            String strLine = "";
            String strLine1 = "";
            double number1 = 0;
            int number2 = 0;
            char symb1 = 'N';
            char symb2 = 'X';
            char symb3 = 'Y';
            char symb4 = '=';
            char symb5 = 'H';
            char symb6 = 'R';
            char symb7 = 'A';
            char symb8 = 'T';
            char symb9 = '{';
            char symb10 = '}';
            char symb11 = 'F';
            char symb12 = 'B';

            int indStart = 0;
            int indStop = 0;
            int iLength = 0;
            int IndZap = 0;
            int TekPoz = 0;
            int fi = 0;
            int if1 = 0;
            int indmas = 0;

            // Чтение файла ---------------------------------------------------------
            String strFileName;

            //strFileName = "FBSPRP.txt";
            //0219
            strFileName = Application.StartupPath + "\\SaveInFiles\\BandsSectJamm.txt";

            StreamReader srFile;
            try
            {
                srFile = new StreamReader(strFileName);
            }
            catch
            {
                MessageBox.Show("Can't open file");
                return;

            }
            // -------------------------------------------------------------------------------------
            // Чтение
            // Numb =...

            // Fmin =...
            // Fmax =...
            // Bmin =...
            // Bmax =...
            // ...

            indmas = 0;

            try
            {
                FBSP objSP = new FBSP();
                objSP.mfb = new FB[GlobalVarLn.sizeDiap_f2 + 10];

                TekPoz = 0;
                IndZap = 0;
                // .......................................................
                // Numb =...
                // 1-я строка

                strLine = srFile.ReadLine();
               int  А;
                if ((strLine == null) && (fl == 0))
                {
                    srFile.Close();
                    return;
                }

                if (strLine == null)
                {
                    MessageBox.Show("No information");
                    srFile.Close();
                    return;
                }

                indStart = strLine.IndexOf(symb1, TekPoz); // N

                if (indStart == -1)
                {
                    MessageBox.Show("No information");
                    srFile.Close();
                    return;
                }

                indStop = strLine.IndexOf(symb4, TekPoz);  //=

                if ((indStop == -1) || (indStop < indStart))
                {
                    MessageBox.Show("No information");
                    srFile.Close();
                    return;
                }

                iLength = indStop - indStart + 1;
                // Убираем 'Numb ='
                strLine1 = strLine.Remove(indStart, iLength);

                if (strLine1 == "")
                {
                    MessageBox.Show("No information");
                    srFile.Close();
                    return;
                }

                objSP.NumbSP = strLine1;
                // .......................................................

                fi = 0;
                strLine = srFile.ReadLine(); // читаем далее (Fmin1=)
                if ((strLine == "") || (strLine == null))
                {
                    fi = 1;
                }
                // .......................................................

                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH
                while ((strLine != "") && (strLine != null))
                {

                    // .......................................................
                    // Fmin =...

                    indStart = strLine.IndexOf(symb11, TekPoz); // F
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    number1 = Convert.ToDouble(strLine1);

                    objSP.mfb[indmas].Fmin = number1;
                    // .......................................................
                    // Fmax =...

                    strLine = srFile.ReadLine();

                    indStart = strLine.IndexOf(symb11, TekPoz); // F
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    number1 = Convert.ToDouble(strLine1);

                    objSP.mfb[indmas].Fmax = number1;
                    // .......................................................
                    // Bmin =...

                    strLine = srFile.ReadLine();

                    indStart = strLine.IndexOf(symb12, TekPoz); // B
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    number1 = Convert.ToDouble(strLine1);

                    objSP.mfb[indmas].Bmin = number1;
                    // .......................................................
                    // Bmax =...

                    strLine = srFile.ReadLine();

                    indStart = strLine.IndexOf(symb12, TekPoz); // B
                    indStop = strLine.IndexOf(symb4, TekPoz);  // =
                    iLength = indStop - indStart + 1;
                    strLine1 = strLine.Remove(indStart, iLength);
                    number1 = Convert.ToDouble(strLine1);

                    objSP.mfb[indmas].Bmax = number1;
                    // .......................................................
                    if ((fl == 1) &&
                        (String.Compare(objSP.NumbSP, GlobalVarLn.NumbSP_f2) == 0)
                       )
                    {
                        GlobalVarLn.flrd_f2 = 1;

                        dataGridView1.Rows[(int)(IndZap)].Cells[0].Value = objSP.mfb[indmas].Fmin;
                        dataGridView1.Rows[(int)(IndZap)].Cells[1].Value = objSP.mfb[indmas].Fmax;
                        dataGridView1.Rows[(int)(IndZap)].Cells[2].Value = objSP.mfb[indmas].Bmin;
                        dataGridView1.Rows[(int)(IndZap)].Cells[3].Value = objSP.mfb[indmas].Bmax;
                        IndZap += 1;

                    }
                    // .......................................................

                    fi = 0;
                    strLine = srFile.ReadLine(); // читаем далее (Numb/Fmin)
                    if ((strLine == "") || (strLine == null))
                        fi = 1;
                    // ...................................................................
                    if (strLine == null)
                    {
                        indmas += 1;
                        objSP.NumbDiap = indmas;
                        // Занести в List
                        GlobalVarLn.list1_f2.Add(objSP);
                        srFile.Close();
                        return;
                    }
                    // ...............................................
                    indStart = strLine.IndexOf(symb1, TekPoz); // N

                    if (indStart != -1)
                    {
                        indStop = strLine.IndexOf(symb4, TekPoz);  //=
                        iLength = indStop - indStart + 1;
                        iLength = indStop - indStart + 1;
                        // Убираем 'Numb ='
                        strLine1 = strLine.Remove(indStart, iLength);
                        indmas += 1;
                        objSP.NumbDiap = indmas;
                        // Занести в List
                        GlobalVarLn.list1_f2.Add(objSP); // предыдущий

                        objSP.NumbSP = strLine1;// новый номер
                        indmas = 0;
                        objSP.NumbDiap = 0;

                        fi = 0;
                        strLine = srFile.ReadLine(); // читаем далее (Fmini=)
                        if ((strLine == "") || (strLine == null))
                        {
                            fi = 1;
                        }

                    }
                    else
                    {
                        indmas += 1;
                    }


                } // WHILE
                // WHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWHILEWH

                // Занести в List
                GlobalVarLn.list1_f2.Add(objSP);

            }
            // -----------------------------------------------------------------------------------
            catch
            {
                srFile.Close();
                return;
            }
            // -------------------------------------------------------------------------------------
            srFile.Close();
            // -------------------------------------------------------------------------------------

        } // ReadfileFBSP_f2
        // ************************************************************************

        // **************************************************************** ФУНКЦИИ


    } // Class
} // Spacenew
