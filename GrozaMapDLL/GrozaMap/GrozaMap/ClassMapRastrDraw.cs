﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
using System.Windows.Input;

using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;
using Bitmap = System.Drawing.Bitmap;
using Point = System.Drawing.Point;

namespace GrozaMap
{
    class ClassMapRastrDraw
    {

     // Draw Polygon ********************************************************************
     // Отрисовка полигона на растровых картах для расчетных зон

        public static void DrawPolygon_Rastr(List<Point> points, byte Color1, byte Color2, byte Color3, byte Color4)

        {
          
            if (points.Count == 0)
            {
                return;
            }
            // --------------------------------------------------------------------------
            List<Mapsui.Geometries.Point> pointPel = new List<Mapsui.Geometries.Point>();

            double lat = 0;
            double lon = 0;
            // --------------------------------------------------------------------------
            for (int i = 0; i < points.Count; i++)
            {
                // !!! 1-й идет долгота, 2-й широта
                var p = Mercator.ToLonLat(points[i].X, points[i].Y);
                lat = p.Y;
                lon = p.X;

                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // 555
                // Если карта - Меркатор

                if (GlobalVarLn.TypeMap == 0)
                {
                    var pp = Mercator.FromLonLat(lon, lat);
                    lon = pp.X;
                    lat = pp.Y;
                }
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                pointPel.Add(new Mapsui.Geometries.Point(lon, lat));

            } // FOR
            // --------------------------------------------------------------------------
            try
            {
                // Yellow            -> 100, 255, 255, (byte)0.25 * 255
                // Pink(розовый)     -> 100, 255, 0, 255
                // Red               -> 100, 255, 0, 0
                // Purple(сиреневый) -> 100, 0, 0, 255
                // Green             -> 100, 0, 255, 0
                MapForm.RasterMapControl.AddPolygon(pointPel, Mapsui.Styles.Color.FromArgb(Color1, Color2, Color3, Color4));

            }
            catch
            {
                //MessageBox.Show(e.Message);
            }
            // --------------------------------------------------------------------------


        } // DrawPolygon_Rastr

        // ******************************************************************** Draw Polygon

        // DrawImage ***********************************************************************
        // Отрисовка изображения s1 (путь) по заданным координатам (Меркатор) -> тогда в
        // широте и долготе ставить -1
        // или по широте и долготе (тогда в координатах ставить -1)
        // с надписью s
        // !!! Желательно все изображения для дополнительного отображения кидать в каталог
        // ImagesHelp


        public static void f_DrawImage(
                                         // Mercator/-1
                                         double X,
                                         double Y,
                                         // grad/-1
                                         double Lat,
                                         double Long,
                                         // Путь к изображению
                                         String s1,
                                         // Надпись
                                         String s,
                                         // Масштаб изображения
                                         double scl
                                         )
        {

            IMapObject objectGrozaS1;
            MapObjectStyle _placeObjectStyleOwn;
            Mapsui.Geometries.Point pointOwn = new Mapsui.Geometries.Point();
            // ------------------------------------------------------------------------------

            try
            {
                // ..........................................................................
                // !!! В результате здесь получаем долготу(pointOwn.X) и широту(pointOwn.Y)

                if ((Lat == -1) && (Long == -1))
                {
                    var p3 = Mercator.ToLonLat(X, Y);
                    pointOwn.X = p3.X;
                    pointOwn.Y = p3.Y;
                }

                else
                {
                    pointOwn.X = Long;
                    pointOwn.Y = Lat;
                }
                // ..........................................................................

                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // 555
                // Если карта - Меркатор

                if (GlobalVarLn.TypeMap == 0)
                {
                    var pp = Mercator.FromLonLat(pointOwn.X, pointOwn.Y);
                    pointOwn.X = pp.X;
                    pointOwn.Y = pp.Y;
                }
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


                _placeObjectStyleOwn = MapForm.RasterMapControl.LoadObjectStyle(
                    //"1.png",
                    s1,
                    //(Bitmap)imageList1.Images[station.indzn],
                    //scale: 2,
                    scale: scl,
                    objectOffset: new Offset(0, 0),
                    textOffset: new Offset(0, 15)
                    );
                objectGrozaS1 = MapForm.RasterMapControl.AddMapObject(_placeObjectStyleOwn, s, pointOwn);

            }
            catch (Exception ex)
            {
                // ignored
            }


        } // Функция f_DrawImage
        // *********************************************************************** DrawImage

        // DrawImage_WithoutPath *************************************************************
        // Отрисовка изображения img по заданным координатам (Меркатор) -> тогда в
        // широте и долготе ставить -1
        // или по широте и долготе (тогда в координатах ставить -1)
        // с надписью s

        public static void f_DrawImage_WithoutPath(
                                         // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                         RasterMapControl objRasterMapControl,
                                         // Mercator/-1
                                         double X,
                                         double Y,
                                         // grad/-1
                                         double Lat,
                                         double Long,
                                         // изображение
                                         Bitmap img,
                                         // Надпись
                                         String s,
                                         // Масштаб изображения
                                         double scl
                                         )
        {
            // ------------------------------------------------------------------------------
            IMapObject objectGrozaS1;
            MapObjectStyle _placeObjectStyleOwn;
            Mapsui.Geometries.Point pointOwn = new Mapsui.Geometries.Point();
            // ------------------------------------------------------------------------------
            try
            {
                // ..........................................................................
                // !!! В результате здесь получаем долготу(pointOwn.X) и широту(pointOwn.Y)

                if ((Lat == -1) && (Long == -1))
                {
                    var p3 = Mercator.ToLonLat(X, Y);
                    pointOwn.X = p3.X;
                    pointOwn.Y = p3.Y;
                }

                else
                {
                    pointOwn.X = Long;
                    pointOwn.Y = Lat;
                }
                // ..........................................................................

                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // 555
                // Если карта - Меркатор

                if (GlobalVarLn.TypeMap == 0)
                {
                    var pp = Mercator.FromLonLat(pointOwn.X, pointOwn.Y);
                    pointOwn.X = pp.X;
                    pointOwn.Y = pp.Y;
                }
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                _placeObjectStyleOwn = objRasterMapControl.LoadObjectStyle(

                    img,
                    scale: scl,
                    objectOffset: new Offset(0, 0),
                    textOffset: new Offset(0, 15)
                    );
                objectGrozaS1 = objRasterMapControl.AddMapObject(_placeObjectStyleOwn, s, pointOwn);
                // ..........................................................................

            }
            // ------------------------------------------------------------------------------
            catch (Exception ex)
            {
                // ignored
            }
            // ------------------------------------------------------------------------------
        }
        // ************************************************************* DrawImage_WithoutPath

        // DrawCircle ************************************************************************
        // Заштрихованный круг

        public static void f_MapRastr_Circle(
                                         Point tpCenterPoint,
                                         byte clr1,
                                         byte clr2,
                                         byte clr3,
                                         byte clr4,
                                         long iRadiusZone
                                        )
        {
            // ------------------------------------------------------------------------------

            try
            {
                // ..........................................................................
                double xx = 0;
                double yy = 0;
                double lt = 0;
                double lng = 0;
                // ..........................................................................
                xx = tpCenterPoint.X;
                yy = tpCenterPoint.Y;
                // ..........................................................................
                var p = Mercator.ToLonLat(xx, yy);
                lt = p.Y;
                lng = p.X;

                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // 555
                // Если карта - Меркатор

                if (GlobalVarLn.TypeMap == 0)
                {
                    var pp = Mercator.FromLonLat(lng, lt);
                    lng = pp.X;
                    lt = pp.Y;
                }
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


                // ..........................................................................
                Mapsui.Geometries.Point point = new Mapsui.Geometries.Point();
                point.Y = lt;
                point.X = lng;

                float fSectorLeft = 0;
                float fSectorRight = 360;
                // ..........................................................................

                var points = MapForm.RasterMapControl.CreateSectorPoints(
                    point,
                    fSectorLeft,
                    fSectorRight,
                    (float)iRadiusZone);
                // ..........................................................................
                //MapForm.RasterMapControl.AddPolygon(points, Mapsui.Styles.Color.FromArgb(100, 255, 255, 255));

                // 100,255,255,255 -> белый
                MapForm.RasterMapControl.AddPolygon(points, Mapsui.Styles.Color.FromArgb(clr1, clr2, clr3, clr4));

                // ..........................................................................

            }
            // ------------------------------------------------------------------------------

            catch (Exception e)
            {
                //MessageBox.Show(e.Message);
            }
            // ------------------------------------------------------------------------------

        }
        // ************************************************************************ DrawCircle

        // DrawPoligonLines ******************************************************************
        // Отрисовка соединенных отрезков по заданным координатам точек (Меркатор) -> тогда в
        // широте и долготе ставить -1
        // или по широте и долготе (тогда в координатах ставить -1)

        public static void f_Draw_PoligonLines(
                                         // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                         RasterMapControl objRasterMapControl,
                                         List<Point_XY_LatLong> objPoint_XY_LatLong,
                                         Mapsui.Styles.Color clr
                                         )
        {

            List<Mapsui.Geometries.Point> pointPel = new List<Mapsui.Geometries.Point>();
            double lat = 0;
            double lon = 0;

            for (int i = 0; i < objPoint_XY_LatLong.Count; i++)
            {

                if ((objPoint_XY_LatLong[i].Lat == -1) && (objPoint_XY_LatLong[i].Long == -1))
                {
                    var p = Mercator.ToLonLat(objPoint_XY_LatLong[i].X, objPoint_XY_LatLong[i].Y);
                    lat = p.Y;
                    lon = p.X;
                }
                else
                {
                    lat = objPoint_XY_LatLong[i].Lat;
                    lon = objPoint_XY_LatLong[i].Long;
                }

                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                // 555
                // Если карта - Меркатор

                if (GlobalVarLn.TypeMap == 0)
                {
                    var pp = Mercator.FromLonLat(lon, lat);
                    lon = pp.X;
                    lat = pp.Y;
                }
                // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                pointPel.Add(new Mapsui.Geometries.Point(lon, lat));

            } // FOR


            try
            {
                //objRasterMapControl.AddPolyline(pointPel, Mapsui.Styles.Color.Blue, 2);
                objRasterMapControl.AddPolyline(pointPel, clr, 2);

            }
            catch
            {
                //MessageBox.Show(e.Message);
            }

        }
        // ****************************************************************** DrawPoligonLines






    } // Class
} // NameSpace
