﻿namespace GrozaMap
{
    partial class FormSuppression
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbPoint1 = new System.Windows.Forms.GroupBox();
            this.tbPt1Height = new System.Windows.Forms.TextBox();
            this.lPt1Height = new System.Windows.Forms.Label();
            this.gbPt1Rad = new System.Windows.Forms.GroupBox();
            this.tbPt1LRad = new System.Windows.Forms.TextBox();
            this.lPt1LRad = new System.Windows.Forms.Label();
            this.tbPt1BRad = new System.Windows.Forms.TextBox();
            this.lPt1BRad = new System.Windows.Forms.Label();
            this.gbPt1Rect = new System.Windows.Forms.GroupBox();
            this.tbPt1YRect = new System.Windows.Forms.TextBox();
            this.lPt1YRect = new System.Windows.Forms.Label();
            this.tbPt1XRect = new System.Windows.Forms.TextBox();
            this.lPt1XRect = new System.Windows.Forms.Label();
            this.gbPt1DegMin = new System.Windows.Forms.GroupBox();
            this.tbPt1LMin1 = new System.Windows.Forms.TextBox();
            this.tbPt1BMin1 = new System.Windows.Forms.TextBox();
            this.lPt1LDegMin = new System.Windows.Forms.Label();
            this.lPt1BDegMin = new System.Windows.Forms.Label();
            this.gbPt1Rect42 = new System.Windows.Forms.GroupBox();
            this.tbPt1YRect42 = new System.Windows.Forms.TextBox();
            this.lPt1YRect42 = new System.Windows.Forms.Label();
            this.tbPt1XRect42 = new System.Windows.Forms.TextBox();
            this.lPt1XRect42 = new System.Windows.Forms.Label();
            this.gbPt1DegMinSec = new System.Windows.Forms.GroupBox();
            this.tbPt1LSec = new System.Windows.Forms.TextBox();
            this.tbPt1BSec = new System.Windows.Forms.TextBox();
            this.lPt1Min4 = new System.Windows.Forms.Label();
            this.lPt1Min3 = new System.Windows.Forms.Label();
            this.tbPt1LMin2 = new System.Windows.Forms.TextBox();
            this.tbPt1BMin2 = new System.Windows.Forms.TextBox();
            this.lPt1Sec2 = new System.Windows.Forms.Label();
            this.lCommChooseSC = new System.Windows.Forms.Label();
            this.lPt1Sec1 = new System.Windows.Forms.Label();
            this.lPt1Deg4 = new System.Windows.Forms.Label();
            this.lPt1Deg3 = new System.Windows.Forms.Label();
            this.tbPt1LDeg2 = new System.Windows.Forms.TextBox();
            this.lPt1LDegMinSec = new System.Windows.Forms.Label();
            this.tbPt1BDeg2 = new System.Windows.Forms.TextBox();
            this.lPt1BDegMinSec = new System.Windows.Forms.Label();
            this.tbResultHeightOwn = new System.Windows.Forms.TextBox();
            this.grbDetail = new System.Windows.Forms.GroupBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.lChooseSC = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.cbChooseSC = new System.Windows.Forms.ComboBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.tbCorrectHeightOpponent = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tbGamma = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbResultHeightOpponent = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbMinHeight = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbMiddleHeight = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tbMaxDistance = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tbCorrectHeightOwn = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbCoeffHE = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbCoeffQ = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.tbRadiusZone1 = new System.Windows.Forms.TextBox();
            this.lRadiusZone = new System.Windows.Forms.Label();
            this.tbRadiusZone = new System.Windows.Forms.TextBox();
            this.bAccept = new System.Windows.Forms.Button();
            this.lLDegMin = new System.Windows.Forms.Label();
            this.cbOwnObject = new System.Windows.Forms.ComboBox();
            this.lCenterLSR = new System.Windows.Forms.Label();
            this.tbOwnHeight = new System.Windows.Forms.TextBox();
            this.lBDegMin = new System.Windows.Forms.Label();
            this.gbOwnDegMinSec = new System.Windows.Forms.GroupBox();
            this.tbLSec = new System.Windows.Forms.TextBox();
            this.tbBSec = new System.Windows.Forms.TextBox();
            this.lMin4 = new System.Windows.Forms.Label();
            this.lMin3 = new System.Windows.Forms.Label();
            this.tbLMin2 = new System.Windows.Forms.TextBox();
            this.tbBMin2 = new System.Windows.Forms.TextBox();
            this.lSec2 = new System.Windows.Forms.Label();
            this.lSec1 = new System.Windows.Forms.Label();
            this.lDeg4 = new System.Windows.Forms.Label();
            this.lDeg3 = new System.Windows.Forms.Label();
            this.tbLDeg2 = new System.Windows.Forms.TextBox();
            this.lLDegMinSec = new System.Windows.Forms.Label();
            this.tbBDeg2 = new System.Windows.Forms.TextBox();
            this.lBDegMinSec = new System.Windows.Forms.Label();
            this.gbOwnRect = new System.Windows.Forms.GroupBox();
            this.tbYRect = new System.Windows.Forms.TextBox();
            this.lYRect = new System.Windows.Forms.Label();
            this.tbXRect = new System.Windows.Forms.TextBox();
            this.lXRect = new System.Windows.Forms.Label();
            this.gbOwnRect42 = new System.Windows.Forms.GroupBox();
            this.tbYRect42 = new System.Windows.Forms.TextBox();
            this.lYRect42 = new System.Windows.Forms.Label();
            this.tbXRect42 = new System.Windows.Forms.TextBox();
            this.lXRect42 = new System.Windows.Forms.Label();
            this.gbOwnDegMin = new System.Windows.Forms.GroupBox();
            this.tbLMin1 = new System.Windows.Forms.TextBox();
            this.tbBMin1 = new System.Windows.Forms.TextBox();
            this.lOwnHeight = new System.Windows.Forms.Label();
            this.gbOwnRad = new System.Windows.Forms.GroupBox();
            this.tbLRad = new System.Windows.Forms.TextBox();
            this.lLRad = new System.Windows.Forms.Label();
            this.tbBRad = new System.Windows.Forms.TextBox();
            this.lBRad = new System.Windows.Forms.Label();
            this.tcParam = new System.Windows.Forms.TabControl();
            this.tpOwnObject = new System.Windows.Forms.TabPage();
            this.label17 = new System.Windows.Forms.Label();
            this.tbHAnt = new System.Windows.Forms.TextBox();
            this.lPowerOwn = new System.Windows.Forms.Label();
            this.tbCoeffOwn = new System.Windows.Forms.TextBox();
            this.lCoeffOwn = new System.Windows.Forms.Label();
            this.tbPowerOwn = new System.Windows.Forms.TextBox();
            this.pCoordPoint = new System.Windows.Forms.Panel();
            this.grbOwnObject = new System.Windows.Forms.GroupBox();
            this.tbHeightOwnObject = new System.Windows.Forms.TextBox();
            this.lHeightOwnObject = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label27 = new System.Windows.Forms.Label();
            this.tbOpponentAntenna1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tbOpponentAntenna = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbCoeffSupOwn = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.chbXY = new System.Windows.Forms.CheckBox();
            this.cbHeightOwnObject = new System.Windows.Forms.ComboBox();
            this.cbPt1HeightOwnObject = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.cbPolarOpponent = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.chbXY1 = new System.Windows.Forms.CheckBox();
            this.cbCommChooseSC = new System.Windows.Forms.ComboBox();
            this.gbPoint1.SuspendLayout();
            this.gbPt1Rad.SuspendLayout();
            this.gbPt1Rect.SuspendLayout();
            this.gbPt1DegMin.SuspendLayout();
            this.gbPt1Rect42.SuspendLayout();
            this.gbPt1DegMinSec.SuspendLayout();
            this.grbDetail.SuspendLayout();
            this.gbOwnDegMinSec.SuspendLayout();
            this.gbOwnRect.SuspendLayout();
            this.gbOwnRect42.SuspendLayout();
            this.gbOwnDegMin.SuspendLayout();
            this.gbOwnRad.SuspendLayout();
            this.tcParam.SuspendLayout();
            this.tpOwnObject.SuspendLayout();
            this.pCoordPoint.SuspendLayout();
            this.grbOwnObject.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbPoint1
            // 
            this.gbPoint1.Controls.Add(this.tbPt1Height);
            this.gbPoint1.Controls.Add(this.lPt1Height);
            this.gbPoint1.Controls.Add(this.gbPt1Rad);
            this.gbPoint1.Controls.Add(this.gbPt1Rect);
            this.gbPoint1.Controls.Add(this.gbPt1DegMin);
            this.gbPoint1.Controls.Add(this.gbPt1Rect42);
            this.gbPoint1.Location = new System.Drawing.Point(5, 4);
            this.gbPoint1.Name = "gbPoint1";
            this.gbPoint1.Size = new System.Drawing.Size(172, 103);
            this.gbPoint1.TabIndex = 85;
            this.gbPoint1.TabStop = false;
            this.gbPoint1.Text = "Object of jam";
            // 
            // tbPt1Height
            // 
            this.tbPt1Height.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbPt1Height.Location = new System.Drawing.Point(83, 75);
            this.tbPt1Height.Name = "tbPt1Height";
            this.tbPt1Height.ReadOnly = true;
            this.tbPt1Height.Size = new System.Drawing.Size(76, 20);
            this.tbPt1Height.TabIndex = 37;
            // 
            // lPt1Height
            // 
            this.lPt1Height.AutoSize = true;
            this.lPt1Height.Location = new System.Drawing.Point(10, 81);
            this.lPt1Height.Name = "lPt1Height";
            this.lPt1Height.Size = new System.Drawing.Size(68, 13);
            this.lPt1Height.TabIndex = 36;
            this.lPt1Height.Text = "H, m.............";
            // 
            // gbPt1Rad
            // 
            this.gbPt1Rad.Controls.Add(this.tbPt1LRad);
            this.gbPt1Rad.Controls.Add(this.lPt1LRad);
            this.gbPt1Rad.Controls.Add(this.tbPt1BRad);
            this.gbPt1Rad.Controls.Add(this.lPt1BRad);
            this.gbPt1Rad.Location = new System.Drawing.Point(172, 75);
            this.gbPt1Rad.Name = "gbPt1Rad";
            this.gbPt1Rad.Size = new System.Drawing.Size(160, 63);
            this.gbPt1Rad.TabIndex = 31;
            this.gbPt1Rad.TabStop = false;
            this.gbPt1Rad.Visible = false;
            // 
            // tbPt1LRad
            // 
            this.tbPt1LRad.Location = new System.Drawing.Point(78, 36);
            this.tbPt1LRad.MaxLength = 10;
            this.tbPt1LRad.Name = "tbPt1LRad";
            this.tbPt1LRad.Size = new System.Drawing.Size(75, 20);
            this.tbPt1LRad.TabIndex = 5;
            // 
            // lPt1LRad
            // 
            this.lPt1LRad.AutoSize = true;
            this.lPt1LRad.Location = new System.Drawing.Point(4, 41);
            this.lPt1LRad.Name = "lPt1LRad";
            this.lPt1LRad.Size = new System.Drawing.Size(94, 13);
            this.lPt1LRad.TabIndex = 6;
            this.lPt1LRad.Text = "L, рад...................";
            // 
            // tbPt1BRad
            // 
            this.tbPt1BRad.Location = new System.Drawing.Point(78, 13);
            this.tbPt1BRad.MaxLength = 10;
            this.tbPt1BRad.Name = "tbPt1BRad";
            this.tbPt1BRad.Size = new System.Drawing.Size(75, 20);
            this.tbPt1BRad.TabIndex = 4;
            // 
            // lPt1BRad
            // 
            this.lPt1BRad.AutoSize = true;
            this.lPt1BRad.Location = new System.Drawing.Point(4, 18);
            this.lPt1BRad.Name = "lPt1BRad";
            this.lPt1BRad.Size = new System.Drawing.Size(95, 13);
            this.lPt1BRad.TabIndex = 3;
            this.lPt1BRad.Text = "B, рад...................";
            // 
            // gbPt1Rect
            // 
            this.gbPt1Rect.Controls.Add(this.tbPt1YRect);
            this.gbPt1Rect.Controls.Add(this.lPt1YRect);
            this.gbPt1Rect.Controls.Add(this.tbPt1XRect);
            this.gbPt1Rect.Controls.Add(this.lPt1XRect);
            this.gbPt1Rect.Location = new System.Drawing.Point(6, 11);
            this.gbPt1Rect.Name = "gbPt1Rect";
            this.gbPt1Rect.Size = new System.Drawing.Size(160, 63);
            this.gbPt1Rect.TabIndex = 29;
            this.gbPt1Rect.TabStop = false;
            this.gbPt1Rect.Visible = false;
            // 
            // tbPt1YRect
            // 
            this.tbPt1YRect.BackColor = System.Drawing.SystemColors.Window;
            this.tbPt1YRect.Location = new System.Drawing.Point(78, 36);
            this.tbPt1YRect.MaxLength = 7;
            this.tbPt1YRect.Name = "tbPt1YRect";
            this.tbPt1YRect.ReadOnly = true;
            this.tbPt1YRect.Size = new System.Drawing.Size(75, 20);
            this.tbPt1YRect.TabIndex = 5;
            // 
            // lPt1YRect
            // 
            this.lPt1YRect.AutoSize = true;
            this.lPt1YRect.Location = new System.Drawing.Point(4, 42);
            this.lPt1YRect.Name = "lPt1YRect";
            this.lPt1YRect.Size = new System.Drawing.Size(67, 13);
            this.lPt1YRect.TabIndex = 6;
            this.lPt1YRect.Text = "Long, deg....";
            // 
            // tbPt1XRect
            // 
            this.tbPt1XRect.BackColor = System.Drawing.SystemColors.Window;
            this.tbPt1XRect.Location = new System.Drawing.Point(78, 13);
            this.tbPt1XRect.MaxLength = 7;
            this.tbPt1XRect.Name = "tbPt1XRect";
            this.tbPt1XRect.ReadOnly = true;
            this.tbPt1XRect.Size = new System.Drawing.Size(75, 20);
            this.tbPt1XRect.TabIndex = 4;
            // 
            // lPt1XRect
            // 
            this.lPt1XRect.AutoSize = true;
            this.lPt1XRect.Location = new System.Drawing.Point(4, 20);
            this.lPt1XRect.Name = "lPt1XRect";
            this.lPt1XRect.Size = new System.Drawing.Size(67, 13);
            this.lPt1XRect.TabIndex = 3;
            this.lPt1XRect.Text = "Lat, deg.......";
            // 
            // gbPt1DegMin
            // 
            this.gbPt1DegMin.Controls.Add(this.tbPt1LMin1);
            this.gbPt1DegMin.Controls.Add(this.tbPt1BMin1);
            this.gbPt1DegMin.Controls.Add(this.lPt1LDegMin);
            this.gbPt1DegMin.Controls.Add(this.lPt1BDegMin);
            this.gbPt1DegMin.Location = new System.Drawing.Point(5, 11);
            this.gbPt1DegMin.Name = "gbPt1DegMin";
            this.gbPt1DegMin.Size = new System.Drawing.Size(146, 63);
            this.gbPt1DegMin.TabIndex = 34;
            this.gbPt1DegMin.TabStop = false;
            this.gbPt1DegMin.Visible = false;
            // 
            // tbPt1LMin1
            // 
            this.tbPt1LMin1.Location = new System.Drawing.Point(62, 38);
            this.tbPt1LMin1.MaxLength = 8;
            this.tbPt1LMin1.Name = "tbPt1LMin1";
            this.tbPt1LMin1.Size = new System.Drawing.Size(50, 20);
            this.tbPt1LMin1.TabIndex = 15;
            // 
            // tbPt1BMin1
            // 
            this.tbPt1BMin1.Location = new System.Drawing.Point(62, 15);
            this.tbPt1BMin1.MaxLength = 8;
            this.tbPt1BMin1.Name = "tbPt1BMin1";
            this.tbPt1BMin1.Size = new System.Drawing.Size(50, 20);
            this.tbPt1BMin1.TabIndex = 13;
            // 
            // lPt1LDegMin
            // 
            this.lPt1LDegMin.AutoSize = true;
            this.lPt1LDegMin.Location = new System.Drawing.Point(4, 41);
            this.lPt1LDegMin.Name = "lPt1LDegMin";
            this.lPt1LDegMin.Size = new System.Drawing.Size(64, 13);
            this.lPt1LDegMin.TabIndex = 12;
            this.lPt1LDegMin.Text = "L.................";
            // 
            // lPt1BDegMin
            // 
            this.lPt1BDegMin.AutoSize = true;
            this.lPt1BDegMin.Location = new System.Drawing.Point(4, 18);
            this.lPt1BDegMin.Name = "lPt1BDegMin";
            this.lPt1BDegMin.Size = new System.Drawing.Size(59, 13);
            this.lPt1BDegMin.TabIndex = 10;
            this.lPt1BDegMin.Text = "B...............";
            // 
            // gbPt1Rect42
            // 
            this.gbPt1Rect42.Controls.Add(this.tbPt1YRect42);
            this.gbPt1Rect42.Controls.Add(this.lPt1YRect42);
            this.gbPt1Rect42.Controls.Add(this.tbPt1XRect42);
            this.gbPt1Rect42.Controls.Add(this.lPt1XRect42);
            this.gbPt1Rect42.Location = new System.Drawing.Point(5, 11);
            this.gbPt1Rect42.Name = "gbPt1Rect42";
            this.gbPt1Rect42.Size = new System.Drawing.Size(153, 63);
            this.gbPt1Rect42.TabIndex = 30;
            this.gbPt1Rect42.TabStop = false;
            this.gbPt1Rect42.Visible = false;
            // 
            // tbPt1YRect42
            // 
            this.tbPt1YRect42.Location = new System.Drawing.Point(78, 36);
            this.tbPt1YRect42.MaxLength = 7;
            this.tbPt1YRect42.Name = "tbPt1YRect42";
            this.tbPt1YRect42.Size = new System.Drawing.Size(75, 20);
            this.tbPt1YRect42.TabIndex = 5;
            // 
            // lPt1YRect42
            // 
            this.lPt1YRect42.AutoSize = true;
            this.lPt1YRect42.Location = new System.Drawing.Point(13, 43);
            this.lPt1YRect42.Name = "lPt1YRect42";
            this.lPt1YRect42.Size = new System.Drawing.Size(85, 13);
            this.lPt1YRect42.TabIndex = 6;
            this.lPt1YRect42.Text = "Y, м...................";
            // 
            // tbPt1XRect42
            // 
            this.tbPt1XRect42.Location = new System.Drawing.Point(78, 13);
            this.tbPt1XRect42.MaxLength = 7;
            this.tbPt1XRect42.Name = "tbPt1XRect42";
            this.tbPt1XRect42.Size = new System.Drawing.Size(75, 20);
            this.tbPt1XRect42.TabIndex = 4;
            // 
            // lPt1XRect42
            // 
            this.lPt1XRect42.AutoSize = true;
            this.lPt1XRect42.Location = new System.Drawing.Point(13, 16);
            this.lPt1XRect42.Name = "lPt1XRect42";
            this.lPt1XRect42.Size = new System.Drawing.Size(85, 13);
            this.lPt1XRect42.TabIndex = 3;
            this.lPt1XRect42.Text = "X, м...................";
            // 
            // gbPt1DegMinSec
            // 
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1LSec);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1BSec);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Min4);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Min3);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1LMin2);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1BMin2);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Sec2);
            this.gbPt1DegMinSec.Controls.Add(this.lCommChooseSC);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Sec1);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Deg4);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1Deg3);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1LDeg2);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1LDegMinSec);
            this.gbPt1DegMinSec.Controls.Add(this.tbPt1BDeg2);
            this.gbPt1DegMinSec.Controls.Add(this.lPt1BDegMinSec);
            this.gbPt1DegMinSec.Location = new System.Drawing.Point(30, 549);
            this.gbPt1DegMinSec.Name = "gbPt1DegMinSec";
            this.gbPt1DegMinSec.Size = new System.Drawing.Size(142, 63);
            this.gbPt1DegMinSec.TabIndex = 35;
            this.gbPt1DegMinSec.TabStop = false;
            this.gbPt1DegMinSec.Visible = false;
            // 
            // tbPt1LSec
            // 
            this.tbPt1LSec.Location = new System.Drawing.Point(93, 38);
            this.tbPt1LSec.MaxLength = 3;
            this.tbPt1LSec.Name = "tbPt1LSec";
            this.tbPt1LSec.Size = new System.Drawing.Size(25, 20);
            this.tbPt1LSec.TabIndex = 23;
            // 
            // tbPt1BSec
            // 
            this.tbPt1BSec.Location = new System.Drawing.Point(93, 12);
            this.tbPt1BSec.MaxLength = 3;
            this.tbPt1BSec.Name = "tbPt1BSec";
            this.tbPt1BSec.Size = new System.Drawing.Size(25, 20);
            this.tbPt1BSec.TabIndex = 18;
            // 
            // lPt1Min4
            // 
            this.lPt1Min4.AutoSize = true;
            this.lPt1Min4.Location = new System.Drawing.Point(84, 41);
            this.lPt1Min4.Name = "lPt1Min4";
            this.lPt1Min4.Size = new System.Drawing.Size(9, 13);
            this.lPt1Min4.TabIndex = 27;
            this.lPt1Min4.Text = "\'";
            // 
            // lPt1Min3
            // 
            this.lPt1Min3.AutoSize = true;
            this.lPt1Min3.Location = new System.Drawing.Point(84, 15);
            this.lPt1Min3.Name = "lPt1Min3";
            this.lPt1Min3.Size = new System.Drawing.Size(9, 13);
            this.lPt1Min3.TabIndex = 26;
            this.lPt1Min3.Text = "\'";
            // 
            // tbPt1LMin2
            // 
            this.tbPt1LMin2.Location = new System.Drawing.Point(59, 38);
            this.tbPt1LMin2.MaxLength = 2;
            this.tbPt1LMin2.Name = "tbPt1LMin2";
            this.tbPt1LMin2.Size = new System.Drawing.Size(25, 20);
            this.tbPt1LMin2.TabIndex = 22;
            // 
            // tbPt1BMin2
            // 
            this.tbPt1BMin2.Location = new System.Drawing.Point(59, 14);
            this.tbPt1BMin2.MaxLength = 2;
            this.tbPt1BMin2.Name = "tbPt1BMin2";
            this.tbPt1BMin2.Size = new System.Drawing.Size(25, 20);
            this.tbPt1BMin2.TabIndex = 16;
            // 
            // lPt1Sec2
            // 
            this.lPt1Sec2.AutoSize = true;
            this.lPt1Sec2.Location = new System.Drawing.Point(120, 37);
            this.lPt1Sec2.Name = "lPt1Sec2";
            this.lPt1Sec2.Size = new System.Drawing.Size(9, 13);
            this.lPt1Sec2.TabIndex = 25;
            this.lPt1Sec2.Text = "\'";
            // 
            // lCommChooseSC
            // 
            this.lCommChooseSC.AutoSize = true;
            this.lCommChooseSC.Location = new System.Drawing.Point(37, 2);
            this.lCommChooseSC.Name = "lCommChooseSC";
            this.lCommChooseSC.Size = new System.Drawing.Size(129, 13);
            this.lCommChooseSC.TabIndex = 92;
            this.lCommChooseSC.Text = "СК....................................";
            this.lCommChooseSC.Visible = false;
            // 
            // lPt1Sec1
            // 
            this.lPt1Sec1.AutoSize = true;
            this.lPt1Sec1.Location = new System.Drawing.Point(117, 13);
            this.lPt1Sec1.Name = "lPt1Sec1";
            this.lPt1Sec1.Size = new System.Drawing.Size(9, 13);
            this.lPt1Sec1.TabIndex = 24;
            this.lPt1Sec1.Text = "\'";
            // 
            // lPt1Deg4
            // 
            this.lPt1Deg4.AutoSize = true;
            this.lPt1Deg4.Location = new System.Drawing.Point(46, 36);
            this.lPt1Deg4.Name = "lPt1Deg4";
            this.lPt1Deg4.Size = new System.Drawing.Size(13, 13);
            this.lPt1Deg4.TabIndex = 21;
            this.lPt1Deg4.Text = "^";
            // 
            // lPt1Deg3
            // 
            this.lPt1Deg3.AutoSize = true;
            this.lPt1Deg3.Location = new System.Drawing.Point(46, 13);
            this.lPt1Deg3.Name = "lPt1Deg3";
            this.lPt1Deg3.Size = new System.Drawing.Size(13, 13);
            this.lPt1Deg3.TabIndex = 20;
            this.lPt1Deg3.Text = "^";
            // 
            // tbPt1LDeg2
            // 
            this.tbPt1LDeg2.Location = new System.Drawing.Point(19, 37);
            this.tbPt1LDeg2.MaxLength = 2;
            this.tbPt1LDeg2.Name = "tbPt1LDeg2";
            this.tbPt1LDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbPt1LDeg2.TabIndex = 19;
            // 
            // lPt1LDegMinSec
            // 
            this.lPt1LDegMinSec.AutoSize = true;
            this.lPt1LDegMinSec.Location = new System.Drawing.Point(4, 41);
            this.lPt1LDegMinSec.Name = "lPt1LDegMinSec";
            this.lPt1LDegMinSec.Size = new System.Drawing.Size(64, 13);
            this.lPt1LDegMinSec.TabIndex = 17;
            this.lPt1LDegMinSec.Text = "L.................";
            // 
            // tbPt1BDeg2
            // 
            this.tbPt1BDeg2.Location = new System.Drawing.Point(19, 13);
            this.tbPt1BDeg2.MaxLength = 2;
            this.tbPt1BDeg2.Name = "tbPt1BDeg2";
            this.tbPt1BDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbPt1BDeg2.TabIndex = 15;
            // 
            // lPt1BDegMinSec
            // 
            this.lPt1BDegMinSec.AutoSize = true;
            this.lPt1BDegMinSec.Location = new System.Drawing.Point(4, 18);
            this.lPt1BDegMinSec.Name = "lPt1BDegMinSec";
            this.lPt1BDegMinSec.Size = new System.Drawing.Size(59, 13);
            this.lPt1BDegMinSec.TabIndex = 14;
            this.lPt1BDegMinSec.Text = "B...............";
            // 
            // tbResultHeightOwn
            // 
            this.tbResultHeightOwn.Location = new System.Drawing.Point(159, 77);
            this.tbResultHeightOwn.Name = "tbResultHeightOwn";
            this.tbResultHeightOwn.Size = new System.Drawing.Size(60, 20);
            this.tbResultHeightOwn.TabIndex = 98;
            this.tbResultHeightOwn.Text = "4";
            // 
            // grbDetail
            // 
            this.grbDetail.Controls.Add(this.label18);
            this.grbDetail.Controls.Add(this.label26);
            this.grbDetail.Controls.Add(this.label25);
            this.grbDetail.Controls.Add(this.textBox9);
            this.grbDetail.Controls.Add(this.textBox8);
            this.grbDetail.Controls.Add(this.label24);
            this.grbDetail.Controls.Add(this.lChooseSC);
            this.grbDetail.Controls.Add(this.textBox7);
            this.grbDetail.Controls.Add(this.label23);
            this.grbDetail.Controls.Add(this.cbChooseSC);
            this.grbDetail.Controls.Add(this.textBox6);
            this.grbDetail.Controls.Add(this.label22);
            this.grbDetail.Controls.Add(this.textBox5);
            this.grbDetail.Controls.Add(this.tbCorrectHeightOpponent);
            this.grbDetail.Controls.Add(this.label10);
            this.grbDetail.Controls.Add(this.tbGamma);
            this.grbDetail.Controls.Add(this.label9);
            this.grbDetail.Controls.Add(this.tbResultHeightOpponent);
            this.grbDetail.Controls.Add(this.label5);
            this.grbDetail.Controls.Add(this.tbMinHeight);
            this.grbDetail.Controls.Add(this.label6);
            this.grbDetail.Controls.Add(this.tbMiddleHeight);
            this.grbDetail.Controls.Add(this.label7);
            this.grbDetail.Controls.Add(this.tbMaxDistance);
            this.grbDetail.Controls.Add(this.label8);
            this.grbDetail.Controls.Add(this.tbCorrectHeightOwn);
            this.grbDetail.Controls.Add(this.label4);
            this.grbDetail.Controls.Add(this.tbCoeffHE);
            this.grbDetail.Controls.Add(this.label3);
            this.grbDetail.Controls.Add(this.tbCoeffQ);
            this.grbDetail.Controls.Add(this.label2);
            this.grbDetail.Controls.Add(this.tbResultHeightOwn);
            this.grbDetail.Controls.Add(this.label1);
            this.grbDetail.Location = new System.Drawing.Point(13, 349);
            this.grbDetail.Name = "grbDetail";
            this.grbDetail.Size = new System.Drawing.Size(408, 202);
            this.grbDetail.TabIndex = 144;
            this.grbDetail.TabStop = false;
            this.grbDetail.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(15, 205);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(179, 13);
            this.label18.TabIndex = 110;
            this.label18.Text = "Коэффициент подавления.............\r\n";
            this.label18.Visible = false;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(29, 162);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(129, 13);
            this.label26.TabIndex = 126;
            this.label26.Text = "Xmax................................";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(29, 129);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(126, 13);
            this.label25.TabIndex = 125;
            this.label25.Text = "Xmin................................";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(159, 155);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(60, 20);
            this.textBox9.TabIndex = 124;
            this.textBox9.Text = "4";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(159, 129);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(60, 20);
            this.textBox8.TabIndex = 123;
            this.textBox8.Text = "4";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(254, 178);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(116, 13);
            this.label24.TabIndex = 122;
            this.label24.Text = "Y2................................";
            // 
            // lChooseSC
            // 
            this.lChooseSC.AutoSize = true;
            this.lChooseSC.Location = new System.Drawing.Point(155, 233);
            this.lChooseSC.Name = "lChooseSC";
            this.lChooseSC.Size = new System.Drawing.Size(45, 13);
            this.lChooseSC.TabIndex = 78;
            this.lChooseSC.Text = "СК........";
            this.lChooseSC.Visible = false;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(410, 178);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(60, 20);
            this.textBox7.TabIndex = 121;
            this.textBox7.Text = "4";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(254, 152);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(116, 13);
            this.label23.TabIndex = 120;
            this.label23.Text = "X2................................";
            // 
            // cbChooseSC
            // 
            this.cbChooseSC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChooseSC.DropDownWidth = 150;
            this.cbChooseSC.FormattingEnabled = true;
            this.cbChooseSC.Items.AddRange(new object[] {
            "Метры на мест-ти",
            "Радианы",
            "Градусы",
            "Град, мин, сек"});
            this.cbChooseSC.Location = new System.Drawing.Point(237, 211);
            this.cbChooseSC.Name = "cbChooseSC";
            this.cbChooseSC.Size = new System.Drawing.Size(123, 21);
            this.cbChooseSC.TabIndex = 77;
            this.cbChooseSC.Visible = false;
            this.cbChooseSC.SelectedIndexChanged += new System.EventHandler(this.cbChooseSC_SelectedIndexChanged);
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(410, 152);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(60, 20);
            this.textBox6.TabIndex = 119;
            this.textBox6.Text = "4";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(254, 129);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(116, 13);
            this.label22.TabIndex = 118;
            this.label22.Text = "Y1................................";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(410, 126);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(60, 20);
            this.textBox5.TabIndex = 117;
            this.textBox5.Text = "4";
            // 
            // tbCorrectHeightOpponent
            // 
            this.tbCorrectHeightOpponent.Location = new System.Drawing.Point(411, 55);
            this.tbCorrectHeightOpponent.Name = "tbCorrectHeightOpponent";
            this.tbCorrectHeightOpponent.Size = new System.Drawing.Size(60, 20);
            this.tbCorrectHeightOpponent.TabIndex = 116;
            this.tbCorrectHeightOpponent.Text = "4";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(254, 62);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(107, 13);
            this.label10.TabIndex = 115;
            this.label10.Text = "Dlt1...........................";
            // 
            // tbGamma
            // 
            this.tbGamma.Location = new System.Drawing.Point(159, 100);
            this.tbGamma.Name = "tbGamma";
            this.tbGamma.Size = new System.Drawing.Size(60, 20);
            this.tbGamma.TabIndex = 114;
            this.tbGamma.Text = "4";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(2, 107);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(152, 13);
            this.label9.TabIndex = 113;
            this.label9.Text = "A2............................................";
            // 
            // tbResultHeightOpponent
            // 
            this.tbResultHeightOpponent.Location = new System.Drawing.Point(411, 77);
            this.tbResultHeightOpponent.Name = "tbResultHeightOpponent";
            this.tbResultHeightOpponent.Size = new System.Drawing.Size(60, 20);
            this.tbResultHeightOpponent.TabIndex = 112;
            this.tbResultHeightOpponent.Text = "4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(254, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 13);
            this.label5.TabIndex = 111;
            this.label5.Text = "Dlt2..........................";
            // 
            // tbMinHeight
            // 
            this.tbMinHeight.Location = new System.Drawing.Point(411, 33);
            this.tbMinHeight.Name = "tbMinHeight";
            this.tbMinHeight.Size = new System.Drawing.Size(60, 20);
            this.tbMinHeight.TabIndex = 110;
            this.tbMinHeight.Text = "4";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(254, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(138, 13);
            this.label6.TabIndex = 109;
            this.label6.Text = "R2.......................................";
            // 
            // tbMiddleHeight
            // 
            this.tbMiddleHeight.Location = new System.Drawing.Point(411, 12);
            this.tbMiddleHeight.Name = "tbMiddleHeight";
            this.tbMiddleHeight.Size = new System.Drawing.Size(60, 20);
            this.tbMiddleHeight.TabIndex = 108;
            this.tbMiddleHeight.Text = "4";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(254, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(138, 13);
            this.label7.TabIndex = 107;
            this.label7.Text = "R1.......................................";
            // 
            // tbMaxDistance
            // 
            this.tbMaxDistance.Location = new System.Drawing.Point(411, 100);
            this.tbMaxDistance.Name = "tbMaxDistance";
            this.tbMaxDistance.Size = new System.Drawing.Size(60, 20);
            this.tbMaxDistance.TabIndex = 106;
            this.tbMaxDistance.Text = "4";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(254, 107);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(116, 13);
            this.label8.TabIndex = 105;
            this.label8.Text = "X1................................";
            // 
            // tbCorrectHeightOwn
            // 
            this.tbCorrectHeightOwn.Location = new System.Drawing.Point(159, 55);
            this.tbCorrectHeightOwn.Name = "tbCorrectHeightOwn";
            this.tbCorrectHeightOwn.Size = new System.Drawing.Size(60, 20);
            this.tbCorrectHeightOwn.TabIndex = 104;
            this.tbCorrectHeightOwn.Text = "4";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(2, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 13);
            this.label4.TabIndex = 103;
            this.label4.Text = "RZP...........................";
            // 
            // tbCoeffHE
            // 
            this.tbCoeffHE.Location = new System.Drawing.Point(159, 33);
            this.tbCoeffHE.Name = "tbCoeffHE";
            this.tbCoeffHE.Size = new System.Drawing.Size(60, 20);
            this.tbCoeffHE.TabIndex = 102;
            this.tbCoeffHE.Text = "4";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(163, 13);
            this.label3.TabIndex = 101;
            this.label3.Text = "HMiddle.......................................";
            // 
            // tbCoeffQ
            // 
            this.tbCoeffQ.Location = new System.Drawing.Point(159, 12);
            this.tbCoeffQ.Name = "tbCoeffQ";
            this.tbCoeffQ.Size = new System.Drawing.Size(60, 20);
            this.tbCoeffQ.TabIndex = 100;
            this.tbCoeffQ.Text = "4";
            this.tbCoeffQ.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(187, 13);
            this.label2.TabIndex = 99;
            this.label2.Text = "dDistance............................................";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 97;
            this.label1.Text = "A1.............................";
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button1.Location = new System.Drawing.Point(272, 189);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(57, 23);
            this.button1.TabIndex = 141;
            this.button1.Text = "JS";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button2.Location = new System.Drawing.Point(342, 188);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(54, 23);
            this.button2.TabIndex = 142;
            this.button2.Text = "Object of jam";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // bClear
            // 
            this.bClear.Location = new System.Drawing.Point(335, 218);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(61, 23);
            this.bClear.TabIndex = 140;
            this.bClear.Text = "Clear";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(-2, 218);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(156, 13);
            this.label13.TabIndex = 136;
            this.label13.Text = "Radius of the depletion zone, m";
            // 
            // tbRadiusZone1
            // 
            this.tbRadiusZone1.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbRadiusZone1.Location = new System.Drawing.Point(156, 216);
            this.tbRadiusZone1.Name = "tbRadiusZone1";
            this.tbRadiusZone1.ReadOnly = true;
            this.tbRadiusZone1.Size = new System.Drawing.Size(75, 20);
            this.tbRadiusZone1.TabIndex = 135;
            this.tbRadiusZone1.TextChanged += new System.EventHandler(this.tbRadiusZone1_TextChanged);
            // 
            // lRadiusZone
            // 
            this.lRadiusZone.AutoSize = true;
            this.lRadiusZone.Location = new System.Drawing.Point(-2, 194);
            this.lRadiusZone.Name = "lRadiusZone";
            this.lRadiusZone.Size = new System.Drawing.Size(151, 13);
            this.lRadiusZone.TabIndex = 133;
            this.lRadiusZone.Text = "Radius of the jamming zone, m";
            // 
            // tbRadiusZone
            // 
            this.tbRadiusZone.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbRadiusZone.Location = new System.Drawing.Point(156, 191);
            this.tbRadiusZone.Name = "tbRadiusZone";
            this.tbRadiusZone.Size = new System.Drawing.Size(75, 20);
            this.tbRadiusZone.TabIndex = 132;
            // 
            // bAccept
            // 
            this.bAccept.Location = new System.Drawing.Point(270, 218);
            this.bAccept.Name = "bAccept";
            this.bAccept.Size = new System.Drawing.Size(59, 23);
            this.bAccept.TabIndex = 139;
            this.bAccept.Text = "Accept";
            this.bAccept.UseVisualStyleBackColor = true;
            this.bAccept.Click += new System.EventHandler(this.bAccept_Click);
            // 
            // lLDegMin
            // 
            this.lLDegMin.AutoSize = true;
            this.lLDegMin.Location = new System.Drawing.Point(20, 39);
            this.lLDegMin.Name = "lLDegMin";
            this.lLDegMin.Size = new System.Drawing.Size(64, 13);
            this.lLDegMin.TabIndex = 12;
            this.lLDegMin.Text = "L.................";
            // 
            // cbOwnObject
            // 
            this.cbOwnObject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOwnObject.FormattingEnabled = true;
            this.cbOwnObject.Items.AddRange(new object[] {
            "X,Y",
            "",
            "",
            "",
            "",
            ""});
            this.cbOwnObject.Location = new System.Drawing.Point(99, 5);
            this.cbOwnObject.Name = "cbOwnObject";
            this.cbOwnObject.Size = new System.Drawing.Size(65, 21);
            this.cbOwnObject.TabIndex = 65;
            this.cbOwnObject.SelectedIndexChanged += new System.EventHandler(this.cbOwnObject_SelectedIndexChanged);
            // 
            // lCenterLSR
            // 
            this.lCenterLSR.AutoSize = true;
            this.lCenterLSR.Location = new System.Drawing.Point(5, 10);
            this.lCenterLSR.Name = "lCenterLSR";
            this.lCenterLSR.Size = new System.Drawing.Size(94, 13);
            this.lCenterLSR.TabIndex = 66;
            this.lCenterLSR.Text = "JS ........................";
            // 
            // tbOwnHeight
            // 
            this.tbOwnHeight.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbOwnHeight.Location = new System.Drawing.Point(85, 89);
            this.tbOwnHeight.Name = "tbOwnHeight";
            this.tbOwnHeight.ReadOnly = true;
            this.tbOwnHeight.Size = new System.Drawing.Size(76, 20);
            this.tbOwnHeight.TabIndex = 64;
            // 
            // lBDegMin
            // 
            this.lBDegMin.AutoSize = true;
            this.lBDegMin.Location = new System.Drawing.Point(20, 16);
            this.lBDegMin.Name = "lBDegMin";
            this.lBDegMin.Size = new System.Drawing.Size(59, 13);
            this.lBDegMin.TabIndex = 10;
            this.lBDegMin.Text = "B...............";
            // 
            // gbOwnDegMinSec
            // 
            this.gbOwnDegMinSec.Controls.Add(this.tbLSec);
            this.gbOwnDegMinSec.Controls.Add(this.tbBSec);
            this.gbOwnDegMinSec.Controls.Add(this.lMin4);
            this.gbOwnDegMinSec.Controls.Add(this.lMin3);
            this.gbOwnDegMinSec.Controls.Add(this.tbLMin2);
            this.gbOwnDegMinSec.Controls.Add(this.tbBMin2);
            this.gbOwnDegMinSec.Controls.Add(this.lSec2);
            this.gbOwnDegMinSec.Controls.Add(this.lSec1);
            this.gbOwnDegMinSec.Controls.Add(this.lDeg4);
            this.gbOwnDegMinSec.Controls.Add(this.lDeg3);
            this.gbOwnDegMinSec.Controls.Add(this.tbLDeg2);
            this.gbOwnDegMinSec.Controls.Add(this.lLDegMinSec);
            this.gbOwnDegMinSec.Controls.Add(this.tbBDeg2);
            this.gbOwnDegMinSec.Controls.Add(this.lBDegMinSec);
            this.gbOwnDegMinSec.Location = new System.Drawing.Point(7, 32);
            this.gbOwnDegMinSec.Name = "gbOwnDegMinSec";
            this.gbOwnDegMinSec.Size = new System.Drawing.Size(143, 60);
            this.gbOwnDegMinSec.TabIndex = 30;
            this.gbOwnDegMinSec.TabStop = false;
            this.gbOwnDegMinSec.Visible = false;
            // 
            // tbLSec
            // 
            this.tbLSec.Location = new System.Drawing.Point(102, 36);
            this.tbLSec.MaxLength = 3;
            this.tbLSec.Name = "tbLSec";
            this.tbLSec.Size = new System.Drawing.Size(25, 20);
            this.tbLSec.TabIndex = 23;
            // 
            // tbBSec
            // 
            this.tbBSec.Location = new System.Drawing.Point(102, 12);
            this.tbBSec.MaxLength = 3;
            this.tbBSec.Name = "tbBSec";
            this.tbBSec.Size = new System.Drawing.Size(25, 20);
            this.tbBSec.TabIndex = 18;
            // 
            // lMin4
            // 
            this.lMin4.AutoSize = true;
            this.lMin4.Location = new System.Drawing.Point(91, 35);
            this.lMin4.Name = "lMin4";
            this.lMin4.Size = new System.Drawing.Size(9, 13);
            this.lMin4.TabIndex = 27;
            this.lMin4.Text = "\'";
            // 
            // lMin3
            // 
            this.lMin3.AutoSize = true;
            this.lMin3.Location = new System.Drawing.Point(92, 12);
            this.lMin3.Name = "lMin3";
            this.lMin3.Size = new System.Drawing.Size(9, 13);
            this.lMin3.TabIndex = 26;
            this.lMin3.Text = "\'";
            // 
            // tbLMin2
            // 
            this.tbLMin2.Location = new System.Drawing.Point(67, 35);
            this.tbLMin2.MaxLength = 2;
            this.tbLMin2.Name = "tbLMin2";
            this.tbLMin2.Size = new System.Drawing.Size(25, 20);
            this.tbLMin2.TabIndex = 22;
            // 
            // tbBMin2
            // 
            this.tbBMin2.Location = new System.Drawing.Point(67, 12);
            this.tbBMin2.MaxLength = 2;
            this.tbBMin2.Name = "tbBMin2";
            this.tbBMin2.Size = new System.Drawing.Size(25, 20);
            this.tbBMin2.TabIndex = 16;
            // 
            // lSec2
            // 
            this.lSec2.AutoSize = true;
            this.lSec2.Location = new System.Drawing.Point(132, 36);
            this.lSec2.Name = "lSec2";
            this.lSec2.Size = new System.Drawing.Size(9, 13);
            this.lSec2.TabIndex = 25;
            this.lSec2.Text = "\'";
            // 
            // lSec1
            // 
            this.lSec1.AutoSize = true;
            this.lSec1.Location = new System.Drawing.Point(132, 12);
            this.lSec1.Name = "lSec1";
            this.lSec1.Size = new System.Drawing.Size(9, 13);
            this.lSec1.TabIndex = 24;
            this.lSec1.Text = "\'";
            // 
            // lDeg4
            // 
            this.lDeg4.AutoSize = true;
            this.lDeg4.Location = new System.Drawing.Point(51, 38);
            this.lDeg4.Name = "lDeg4";
            this.lDeg4.Size = new System.Drawing.Size(13, 13);
            this.lDeg4.TabIndex = 21;
            this.lDeg4.Text = "^";
            // 
            // lDeg3
            // 
            this.lDeg3.AutoSize = true;
            this.lDeg3.Location = new System.Drawing.Point(51, 13);
            this.lDeg3.Name = "lDeg3";
            this.lDeg3.Size = new System.Drawing.Size(13, 13);
            this.lDeg3.TabIndex = 20;
            this.lDeg3.Text = "^";
            // 
            // tbLDeg2
            // 
            this.tbLDeg2.Location = new System.Drawing.Point(23, 36);
            this.tbLDeg2.MaxLength = 2;
            this.tbLDeg2.Name = "tbLDeg2";
            this.tbLDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbLDeg2.TabIndex = 19;
            // 
            // lLDegMinSec
            // 
            this.lLDegMinSec.AutoSize = true;
            this.lLDegMinSec.Location = new System.Drawing.Point(6, 36);
            this.lLDegMinSec.Name = "lLDegMinSec";
            this.lLDegMinSec.Size = new System.Drawing.Size(64, 13);
            this.lLDegMinSec.TabIndex = 17;
            this.lLDegMinSec.Text = "L.................";
            // 
            // tbBDeg2
            // 
            this.tbBDeg2.Location = new System.Drawing.Point(23, 13);
            this.tbBDeg2.MaxLength = 2;
            this.tbBDeg2.Name = "tbBDeg2";
            this.tbBDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbBDeg2.TabIndex = 15;
            // 
            // lBDegMinSec
            // 
            this.lBDegMinSec.AutoSize = true;
            this.lBDegMinSec.Location = new System.Drawing.Point(5, 14);
            this.lBDegMinSec.Name = "lBDegMinSec";
            this.lBDegMinSec.Size = new System.Drawing.Size(59, 13);
            this.lBDegMinSec.TabIndex = 14;
            this.lBDegMinSec.Text = "B...............";
            // 
            // gbOwnRect
            // 
            this.gbOwnRect.Controls.Add(this.tbYRect);
            this.gbOwnRect.Controls.Add(this.lYRect);
            this.gbOwnRect.Controls.Add(this.tbXRect);
            this.gbOwnRect.Controls.Add(this.lXRect);
            this.gbOwnRect.Location = new System.Drawing.Point(8, 26);
            this.gbOwnRect.Name = "gbOwnRect";
            this.gbOwnRect.Size = new System.Drawing.Size(158, 63);
            this.gbOwnRect.TabIndex = 67;
            this.gbOwnRect.TabStop = false;
            this.gbOwnRect.Visible = false;
            // 
            // tbYRect
            // 
            this.tbYRect.BackColor = System.Drawing.SystemColors.Window;
            this.tbYRect.Location = new System.Drawing.Point(78, 36);
            this.tbYRect.MaxLength = 7;
            this.tbYRect.Name = "tbYRect";
            this.tbYRect.ReadOnly = true;
            this.tbYRect.Size = new System.Drawing.Size(75, 20);
            this.tbYRect.TabIndex = 5;
            // 
            // lYRect
            // 
            this.lYRect.AutoSize = true;
            this.lYRect.Location = new System.Drawing.Point(5, 39);
            this.lYRect.Name = "lYRect";
            this.lYRect.Size = new System.Drawing.Size(67, 13);
            this.lYRect.TabIndex = 6;
            this.lYRect.Text = "Long, deg....";
            // 
            // tbXRect
            // 
            this.tbXRect.BackColor = System.Drawing.SystemColors.Window;
            this.tbXRect.Location = new System.Drawing.Point(78, 13);
            this.tbXRect.MaxLength = 7;
            this.tbXRect.Name = "tbXRect";
            this.tbXRect.ReadOnly = true;
            this.tbXRect.Size = new System.Drawing.Size(75, 20);
            this.tbXRect.TabIndex = 4;
            // 
            // lXRect
            // 
            this.lXRect.AutoSize = true;
            this.lXRect.Location = new System.Drawing.Point(4, 20);
            this.lXRect.Name = "lXRect";
            this.lXRect.Size = new System.Drawing.Size(70, 13);
            this.lXRect.TabIndex = 3;
            this.lXRect.Text = "Lat, deg........";
            // 
            // gbOwnRect42
            // 
            this.gbOwnRect42.Controls.Add(this.tbYRect42);
            this.gbOwnRect42.Controls.Add(this.lYRect42);
            this.gbOwnRect42.Controls.Add(this.tbXRect42);
            this.gbOwnRect42.Controls.Add(this.lXRect42);
            this.gbOwnRect42.Location = new System.Drawing.Point(7, 32);
            this.gbOwnRect42.Name = "gbOwnRect42";
            this.gbOwnRect42.Size = new System.Drawing.Size(160, 63);
            this.gbOwnRect42.TabIndex = 27;
            this.gbOwnRect42.TabStop = false;
            this.gbOwnRect42.Visible = false;
            // 
            // tbYRect42
            // 
            this.tbYRect42.Location = new System.Drawing.Point(78, 36);
            this.tbYRect42.MaxLength = 7;
            this.tbYRect42.Name = "tbYRect42";
            this.tbYRect42.Size = new System.Drawing.Size(75, 20);
            this.tbYRect42.TabIndex = 5;
            // 
            // lYRect42
            // 
            this.lYRect42.AutoSize = true;
            this.lYRect42.Location = new System.Drawing.Point(4, 39);
            this.lYRect42.Name = "lYRect42";
            this.lYRect42.Size = new System.Drawing.Size(85, 13);
            this.lYRect42.TabIndex = 6;
            this.lYRect42.Text = "Y, м...................";
            // 
            // tbXRect42
            // 
            this.tbXRect42.Location = new System.Drawing.Point(78, 13);
            this.tbXRect42.MaxLength = 7;
            this.tbXRect42.Name = "tbXRect42";
            this.tbXRect42.Size = new System.Drawing.Size(75, 20);
            this.tbXRect42.TabIndex = 4;
            // 
            // lXRect42
            // 
            this.lXRect42.AutoSize = true;
            this.lXRect42.Location = new System.Drawing.Point(4, 20);
            this.lXRect42.Name = "lXRect42";
            this.lXRect42.Size = new System.Drawing.Size(85, 13);
            this.lXRect42.TabIndex = 3;
            this.lXRect42.Text = "X, м...................";
            // 
            // gbOwnDegMin
            // 
            this.gbOwnDegMin.Controls.Add(this.tbLMin1);
            this.gbOwnDegMin.Controls.Add(this.tbBMin1);
            this.gbOwnDegMin.Controls.Add(this.lLDegMin);
            this.gbOwnDegMin.Controls.Add(this.lBDegMin);
            this.gbOwnDegMin.Location = new System.Drawing.Point(6, 30);
            this.gbOwnDegMin.Name = "gbOwnDegMin";
            this.gbOwnDegMin.Size = new System.Drawing.Size(142, 63);
            this.gbOwnDegMin.TabIndex = 29;
            this.gbOwnDegMin.TabStop = false;
            this.gbOwnDegMin.Visible = false;
            // 
            // tbLMin1
            // 
            this.tbLMin1.Location = new System.Drawing.Point(75, 36);
            this.tbLMin1.MaxLength = 8;
            this.tbLMin1.Name = "tbLMin1";
            this.tbLMin1.Size = new System.Drawing.Size(50, 20);
            this.tbLMin1.TabIndex = 15;
            // 
            // tbBMin1
            // 
            this.tbBMin1.Location = new System.Drawing.Point(75, 13);
            this.tbBMin1.MaxLength = 8;
            this.tbBMin1.Name = "tbBMin1";
            this.tbBMin1.Size = new System.Drawing.Size(50, 20);
            this.tbBMin1.TabIndex = 13;
            // 
            // lOwnHeight
            // 
            this.lOwnHeight.AutoSize = true;
            this.lOwnHeight.Location = new System.Drawing.Point(9, 96);
            this.lOwnHeight.Name = "lOwnHeight";
            this.lOwnHeight.Size = new System.Drawing.Size(74, 13);
            this.lOwnHeight.TabIndex = 63;
            this.lOwnHeight.Text = "H, m...............";
            // 
            // gbOwnRad
            // 
            this.gbOwnRad.Controls.Add(this.tbLRad);
            this.gbOwnRad.Controls.Add(this.lLRad);
            this.gbOwnRad.Controls.Add(this.tbBRad);
            this.gbOwnRad.Controls.Add(this.lBRad);
            this.gbOwnRad.Location = new System.Drawing.Point(180, 69);
            this.gbOwnRad.Name = "gbOwnRad";
            this.gbOwnRad.Size = new System.Drawing.Size(160, 63);
            this.gbOwnRad.TabIndex = 28;
            this.gbOwnRad.TabStop = false;
            this.gbOwnRad.Visible = false;
            // 
            // tbLRad
            // 
            this.tbLRad.Location = new System.Drawing.Point(78, 36);
            this.tbLRad.MaxLength = 10;
            this.tbLRad.Name = "tbLRad";
            this.tbLRad.Size = new System.Drawing.Size(75, 20);
            this.tbLRad.TabIndex = 5;
            // 
            // lLRad
            // 
            this.lLRad.AutoSize = true;
            this.lLRad.Location = new System.Drawing.Point(4, 41);
            this.lLRad.Name = "lLRad";
            this.lLRad.Size = new System.Drawing.Size(94, 13);
            this.lLRad.TabIndex = 6;
            this.lLRad.Text = "L, рад...................";
            // 
            // tbBRad
            // 
            this.tbBRad.Location = new System.Drawing.Point(78, 13);
            this.tbBRad.MaxLength = 10;
            this.tbBRad.Name = "tbBRad";
            this.tbBRad.Size = new System.Drawing.Size(75, 20);
            this.tbBRad.TabIndex = 4;
            // 
            // lBRad
            // 
            this.lBRad.AutoSize = true;
            this.lBRad.Location = new System.Drawing.Point(4, 18);
            this.lBRad.Name = "lBRad";
            this.lBRad.Size = new System.Drawing.Size(95, 13);
            this.lBRad.TabIndex = 3;
            this.lBRad.Text = "B, рад...................";
            // 
            // tcParam
            // 
            this.tcParam.Controls.Add(this.tpOwnObject);
            this.tcParam.Controls.Add(this.tabPage1);
            this.tcParam.Location = new System.Drawing.Point(4, 4);
            this.tcParam.Name = "tcParam";
            this.tcParam.SelectedIndex = 0;
            this.tcParam.Size = new System.Drawing.Size(392, 182);
            this.tcParam.TabIndex = 131;
            // 
            // tpOwnObject
            // 
            this.tpOwnObject.BackColor = System.Drawing.SystemColors.Control;
            this.tpOwnObject.Controls.Add(this.label17);
            this.tpOwnObject.Controls.Add(this.tbHAnt);
            this.tpOwnObject.Controls.Add(this.lPowerOwn);
            this.tpOwnObject.Controls.Add(this.tbCoeffOwn);
            this.tpOwnObject.Controls.Add(this.lCoeffOwn);
            this.tpOwnObject.Controls.Add(this.tbPowerOwn);
            this.tpOwnObject.Controls.Add(this.pCoordPoint);
            this.tpOwnObject.Controls.Add(this.grbOwnObject);
            this.tpOwnObject.Location = new System.Drawing.Point(4, 22);
            this.tpOwnObject.Name = "tpOwnObject";
            this.tpOwnObject.Padding = new System.Windows.Forms.Padding(3);
            this.tpOwnObject.Size = new System.Drawing.Size(384, 156);
            this.tpOwnObject.TabIndex = 0;
            this.tpOwnObject.Text = "Jammer station (JS)";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(176, 6);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(123, 13);
            this.label17.TabIndex = 111;
            this.label17.Text = "Antenna height, m..........";
            // 
            // tbHAnt
            // 
            this.tbHAnt.Location = new System.Drawing.Point(311, 4);
            this.tbHAnt.Name = "tbHAnt";
            this.tbHAnt.Size = new System.Drawing.Size(60, 20);
            this.tbHAnt.TabIndex = 110;
            this.tbHAnt.Text = "8";
            // 
            // lPowerOwn
            // 
            this.lPowerOwn.AutoSize = true;
            this.lPowerOwn.Location = new System.Drawing.Point(176, 32);
            this.lPowerOwn.Name = "lPowerOwn";
            this.lPowerOwn.Size = new System.Drawing.Size(123, 13);
            this.lPowerOwn.TabIndex = 89;
            this.lPowerOwn.Text = "Power, W (10-300) ........";
            // 
            // tbCoeffOwn
            // 
            this.tbCoeffOwn.Location = new System.Drawing.Point(312, 51);
            this.tbCoeffOwn.Name = "tbCoeffOwn";
            this.tbCoeffOwn.Size = new System.Drawing.Size(59, 20);
            this.tbCoeffOwn.TabIndex = 88;
            this.tbCoeffOwn.Text = "4";
            // 
            // lCoeffOwn
            // 
            this.lCoeffOwn.AutoSize = true;
            this.lCoeffOwn.Location = new System.Drawing.Point(177, 53);
            this.lCoeffOwn.Name = "lCoeffOwn";
            this.lCoeffOwn.Size = new System.Drawing.Size(122, 13);
            this.lCoeffOwn.TabIndex = 87;
            this.lCoeffOwn.Text = "Gain (4-8) ......................";
            // 
            // tbPowerOwn
            // 
            this.tbPowerOwn.Location = new System.Drawing.Point(311, 27);
            this.tbPowerOwn.Name = "tbPowerOwn";
            this.tbPowerOwn.Size = new System.Drawing.Size(60, 20);
            this.tbPowerOwn.TabIndex = 86;
            this.tbPowerOwn.Text = "125";
            // 
            // pCoordPoint
            // 
            this.pCoordPoint.Controls.Add(this.gbOwnRect);
            this.pCoordPoint.Controls.Add(this.cbOwnObject);
            this.pCoordPoint.Controls.Add(this.lCenterLSR);
            this.pCoordPoint.Controls.Add(this.tbOwnHeight);
            this.pCoordPoint.Controls.Add(this.gbOwnDegMin);
            this.pCoordPoint.Controls.Add(this.gbOwnDegMinSec);
            this.pCoordPoint.Controls.Add(this.gbOwnRect42);
            this.pCoordPoint.Controls.Add(this.lOwnHeight);
            this.pCoordPoint.Controls.Add(this.gbOwnRad);
            this.pCoordPoint.Location = new System.Drawing.Point(2, 4);
            this.pCoordPoint.Name = "pCoordPoint";
            this.pCoordPoint.Size = new System.Drawing.Size(172, 113);
            this.pCoordPoint.TabIndex = 79;
            // 
            // grbOwnObject
            // 
            this.grbOwnObject.Controls.Add(this.tbHeightOwnObject);
            this.grbOwnObject.Controls.Add(this.lHeightOwnObject);
            this.grbOwnObject.Location = new System.Drawing.Point(177, 77);
            this.grbOwnObject.Name = "grbOwnObject";
            this.grbOwnObject.Size = new System.Drawing.Size(194, 41);
            this.grbOwnObject.TabIndex = 76;
            this.grbOwnObject.TabStop = false;
            // 
            // tbHeightOwnObject
            // 
            this.tbHeightOwnObject.Location = new System.Drawing.Point(131, 15);
            this.tbHeightOwnObject.MaxLength = 3;
            this.tbHeightOwnObject.Name = "tbHeightOwnObject";
            this.tbHeightOwnObject.Size = new System.Drawing.Size(60, 20);
            this.tbHeightOwnObject.TabIndex = 26;
            // 
            // lHeightOwnObject
            // 
            this.lHeightOwnObject.AutoSize = true;
            this.lHeightOwnObject.Location = new System.Drawing.Point(-2, 18);
            this.lHeightOwnObject.Name = "lHeightOwnObject";
            this.lHeightOwnObject.Size = new System.Drawing.Size(125, 13);
            this.lHeightOwnObject.TabIndex = 21;
            this.lHeightOwnObject.Text = "Total height , m...............";
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.label27);
            this.tabPage1.Controls.Add(this.tbOpponentAntenna1);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.tbOpponentAntenna);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.textBox3);
            this.tabPage1.Controls.Add(this.textBox2);
            this.tabPage1.Controls.Add(this.gbPoint1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(384, 156);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Object of jam";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(179, 122);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(143, 13);
            this.label27.TabIndex = 137;
            this.label27.Text = "Antenna height (receiver), m ";
            // 
            // tbOpponentAntenna1
            // 
            this.tbOpponentAntenna1.Location = new System.Drawing.Point(324, 119);
            this.tbOpponentAntenna1.Name = "tbOpponentAntenna1";
            this.tbOpponentAntenna1.Size = new System.Drawing.Size(60, 20);
            this.tbOpponentAntenna1.TabIndex = 136;
            this.tbOpponentAntenna1.Text = "2";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(181, 98);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(138, 13);
            this.label11.TabIndex = 135;
            this.label11.Text = "Antenna height, m ..............";
            // 
            // tbOpponentAntenna
            // 
            this.tbOpponentAntenna.Location = new System.Drawing.Point(324, 96);
            this.tbOpponentAntenna.Name = "tbOpponentAntenna";
            this.tbOpponentAntenna.Size = new System.Drawing.Size(60, 20);
            this.tbOpponentAntenna.TabIndex = 134;
            this.tbOpponentAntenna.Text = "2";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbCoeffSupOwn);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Location = new System.Drawing.Point(181, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(207, 41);
            this.groupBox1.TabIndex = 125;
            this.groupBox1.TabStop = false;
            // 
            // tbCoeffSupOwn
            // 
            this.tbCoeffSupOwn.Location = new System.Drawing.Point(144, 13);
            this.tbCoeffSupOwn.Name = "tbCoeffSupOwn";
            this.tbCoeffSupOwn.Size = new System.Drawing.Size(59, 20);
            this.tbCoeffSupOwn.TabIndex = 109;
            this.tbCoeffSupOwn.Text = "1";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(2, 16);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(141, 13);
            this.label21.TabIndex = 21;
            this.label21.Text = "JSR (1-100) .........................";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(182, 75);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(140, 13);
            this.label14.TabIndex = 121;
            this.label14.Text = "Gain (1-10) ..........................";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(181, 50);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(138, 13);
            this.label12.TabIndex = 120;
            this.label12.Text = "Power, W (1-100) ...............";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(324, 73);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(60, 20);
            this.textBox3.TabIndex = 118;
            this.textBox3.Text = "1";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(324, 49);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(60, 20);
            this.textBox2.TabIndex = 117;
            this.textBox2.Text = "25";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(116, 641);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(133, 13);
            this.label15.TabIndex = 106;
            this.label15.Text = "Ручной выбор координат";
            this.label15.Visible = false;
            // 
            // chbXY
            // 
            this.chbXY.AutoSize = true;
            this.chbXY.Location = new System.Drawing.Point(70, 628);
            this.chbXY.Name = "chbXY";
            this.chbXY.Size = new System.Drawing.Size(15, 14);
            this.chbXY.TabIndex = 105;
            this.chbXY.UseVisualStyleBackColor = true;
            this.chbXY.Visible = false;
            // 
            // cbHeightOwnObject
            // 
            this.cbHeightOwnObject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbHeightOwnObject.FormattingEnabled = true;
            this.cbHeightOwnObject.Items.AddRange(new object[] {
            "Antenna height+relief",
            "Antenna height",
            "Enter by yourself"});
            this.cbHeightOwnObject.Location = new System.Drawing.Point(464, 525);
            this.cbHeightOwnObject.Name = "cbHeightOwnObject";
            this.cbHeightOwnObject.Size = new System.Drawing.Size(152, 21);
            this.cbHeightOwnObject.TabIndex = 27;
            this.cbHeightOwnObject.Visible = false;
            // 
            // cbPt1HeightOwnObject
            // 
            this.cbPt1HeightOwnObject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPt1HeightOwnObject.FormattingEnabled = true;
            this.cbPt1HeightOwnObject.Items.AddRange(new object[] {
            "Antenna height+relief",
            "Antenna height",
            "Enter by yourself"});
            this.cbPt1HeightOwnObject.Location = new System.Drawing.Point(473, 559);
            this.cbPt1HeightOwnObject.Name = "cbPt1HeightOwnObject";
            this.cbPt1HeightOwnObject.Size = new System.Drawing.Size(143, 21);
            this.cbPt1HeightOwnObject.TabIndex = 27;
            this.cbPt1HeightOwnObject.Visible = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(172, 599);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(119, 13);
            this.label20.TabIndex = 124;
            this.label20.Text = "Поляризация сигнала";
            this.label20.Visible = false;
            this.label20.Click += new System.EventHandler(this.label20_Click);
            // 
            // cbPolarOpponent
            // 
            this.cbPolarOpponent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPolarOpponent.FormattingEnabled = true;
            this.cbPolarOpponent.Items.AddRange(new object[] {
            "Линейная вертикальная",
            "Линейная горизонтальная",
            "Круговая правая",
            "Круговая левая"});
            this.cbPolarOpponent.Location = new System.Drawing.Point(310, 606);
            this.cbPolarOpponent.Name = "cbPolarOpponent";
            this.cbPolarOpponent.Size = new System.Drawing.Size(110, 21);
            this.cbPolarOpponent.TabIndex = 123;
            this.cbPolarOpponent.Visible = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(190, 564);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(152, 13);
            this.label19.TabIndex = 122;
            this.label19.Text = "Коэффициент подавления....";
            this.label19.Visible = false;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(344, 557);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(60, 20);
            this.textBox4.TabIndex = 119;
            this.textBox4.Text = "1";
            this.textBox4.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(150, 628);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(133, 13);
            this.label16.TabIndex = 107;
            this.label16.Text = "Ручной выбор координат";
            this.label16.Visible = false;
            // 
            // chbXY1
            // 
            this.chbXY1.AutoSize = true;
            this.chbXY1.Location = new System.Drawing.Point(34, 618);
            this.chbXY1.Name = "chbXY1";
            this.chbXY1.Size = new System.Drawing.Size(15, 14);
            this.chbXY1.TabIndex = 106;
            this.chbXY1.UseVisualStyleBackColor = true;
            this.chbXY1.Visible = false;
            // 
            // cbCommChooseSC
            // 
            this.cbCommChooseSC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCommChooseSC.DropDownWidth = 150;
            this.cbCommChooseSC.FormattingEnabled = true;
            this.cbCommChooseSC.Items.AddRange(new object[] {
            "Метры на мест-ти",
            "Радианы",
            "Градусы",
            "Град, мин, сек"});
            this.cbCommChooseSC.Location = new System.Drawing.Point(159, 549);
            this.cbCommChooseSC.Name = "cbCommChooseSC";
            this.cbCommChooseSC.Size = new System.Drawing.Size(132, 21);
            this.cbCommChooseSC.TabIndex = 91;
            this.cbCommChooseSC.Visible = false;
            this.cbCommChooseSC.SelectedIndexChanged += new System.EventHandler(this.cbCommChooseSC_SelectedIndexChanged);
            // 
            // FormSuppression
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(399, 243);
            this.Controls.Add(this.cbHeightOwnObject);
            this.Controls.Add(this.cbPt1HeightOwnObject);
            this.Controls.Add(this.grbDetail);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cbPolarOpponent);
            this.Controls.Add(this.chbXY);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.cbCommChooseSC);
            this.Controls.Add(this.gbPt1DegMinSec);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.chbXY1);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.tbRadiusZone1);
            this.Controls.Add(this.lRadiusZone);
            this.Controls.Add(this.tbRadiusZone);
            this.Controls.Add(this.bAccept);
            this.Controls.Add(this.tcParam);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(800, 100);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSuppression";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Control jamming zone";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.FormSuppression_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSuppression_FormClosing);
            this.Load += new System.EventHandler(this.FormSuppression_Load);
            this.gbPoint1.ResumeLayout(false);
            this.gbPoint1.PerformLayout();
            this.gbPt1Rad.ResumeLayout(false);
            this.gbPt1Rad.PerformLayout();
            this.gbPt1Rect.ResumeLayout(false);
            this.gbPt1Rect.PerformLayout();
            this.gbPt1DegMin.ResumeLayout(false);
            this.gbPt1DegMin.PerformLayout();
            this.gbPt1Rect42.ResumeLayout(false);
            this.gbPt1Rect42.PerformLayout();
            this.gbPt1DegMinSec.ResumeLayout(false);
            this.gbPt1DegMinSec.PerformLayout();
            this.grbDetail.ResumeLayout(false);
            this.grbDetail.PerformLayout();
            this.gbOwnDegMinSec.ResumeLayout(false);
            this.gbOwnDegMinSec.PerformLayout();
            this.gbOwnRect.ResumeLayout(false);
            this.gbOwnRect.PerformLayout();
            this.gbOwnRect42.ResumeLayout(false);
            this.gbOwnRect42.PerformLayout();
            this.gbOwnDegMin.ResumeLayout(false);
            this.gbOwnDegMin.PerformLayout();
            this.gbOwnRad.ResumeLayout(false);
            this.gbOwnRad.PerformLayout();
            this.tcParam.ResumeLayout(false);
            this.tpOwnObject.ResumeLayout(false);
            this.tpOwnObject.PerformLayout();
            this.pCoordPoint.ResumeLayout(false);
            this.pCoordPoint.PerformLayout();
            this.grbOwnObject.ResumeLayout(false);
            this.grbOwnObject.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbPoint1;
        private System.Windows.Forms.Label lPt1Height;
        public System.Windows.Forms.TextBox tbPt1LSec;
        public System.Windows.Forms.TextBox tbPt1BSec;
        private System.Windows.Forms.Label lPt1Min4;
        private System.Windows.Forms.Label lPt1Min3;
        public System.Windows.Forms.TextBox tbPt1LMin2;
        public System.Windows.Forms.TextBox tbPt1BMin2;
        private System.Windows.Forms.Label lPt1Sec2;
        private System.Windows.Forms.Label lPt1Sec1;
        private System.Windows.Forms.Label lPt1Deg4;
        private System.Windows.Forms.Label lPt1Deg3;
        public System.Windows.Forms.TextBox tbPt1LDeg2;
        private System.Windows.Forms.Label lPt1LDegMinSec;
        public System.Windows.Forms.TextBox tbPt1BDeg2;
        private System.Windows.Forms.Label lPt1BDegMinSec;
        public System.Windows.Forms.TextBox tbPt1LRad;
        private System.Windows.Forms.Label lPt1LRad;
        public System.Windows.Forms.TextBox tbPt1BRad;
        private System.Windows.Forms.Label lPt1BRad;
        public System.Windows.Forms.TextBox tbPt1YRect;
        private System.Windows.Forms.Label lPt1YRect;
        public System.Windows.Forms.TextBox tbPt1XRect;
        private System.Windows.Forms.Label lPt1XRect;
        public System.Windows.Forms.TextBox tbPt1LMin1;
        public System.Windows.Forms.TextBox tbPt1BMin1;
        private System.Windows.Forms.Label lPt1LDegMin;
        private System.Windows.Forms.Label lPt1BDegMin;
        public System.Windows.Forms.TextBox tbPt1YRect42;
        private System.Windows.Forms.Label lPt1YRect42;
        public System.Windows.Forms.TextBox tbPt1XRect42;
        private System.Windows.Forms.Label lPt1XRect42;
        private System.Windows.Forms.TextBox tbResultHeightOwn;
        private System.Windows.Forms.GroupBox grbDetail;
        private System.Windows.Forms.TextBox tbCorrectHeightOpponent;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbGamma;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbResultHeightOpponent;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbMinHeight;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbMiddleHeight;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbMaxDistance;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbCorrectHeightOwn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbCoeffHE;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button bClear;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lRadiusZone;
        private System.Windows.Forms.Button bAccept;
        private System.Windows.Forms.Label lLDegMin;
        private System.Windows.Forms.Label lCenterLSR;
        private System.Windows.Forms.Label lBDegMin;
        public System.Windows.Forms.TextBox tbLSec;
        public System.Windows.Forms.TextBox tbBSec;
        private System.Windows.Forms.Label lMin4;
        private System.Windows.Forms.Label lMin3;
        public System.Windows.Forms.TextBox tbLMin2;
        public System.Windows.Forms.TextBox tbBMin2;
        private System.Windows.Forms.Label lSec2;
        private System.Windows.Forms.Label lSec1;
        private System.Windows.Forms.Label lDeg4;
        private System.Windows.Forms.Label lDeg3;
        public System.Windows.Forms.TextBox tbLDeg2;
        private System.Windows.Forms.Label lLDegMinSec;
        public System.Windows.Forms.TextBox tbBDeg2;
        private System.Windows.Forms.Label lBDegMinSec;
        public System.Windows.Forms.TextBox tbYRect;
        private System.Windows.Forms.Label lYRect;
        public System.Windows.Forms.TextBox tbXRect;
        private System.Windows.Forms.Label lXRect;
        public System.Windows.Forms.TextBox tbYRect42;
        private System.Windows.Forms.Label lYRect42;
        public System.Windows.Forms.TextBox tbXRect42;
        private System.Windows.Forms.Label lXRect42;
        public System.Windows.Forms.TextBox tbLMin1;
        public System.Windows.Forms.TextBox tbBMin1;
        private System.Windows.Forms.Label lOwnHeight;
        public System.Windows.Forms.TextBox tbLRad;
        private System.Windows.Forms.Label lLRad;
        public System.Windows.Forms.TextBox tbBRad;
        private System.Windows.Forms.Label lBRad;
        public System.Windows.Forms.TabControl tcParam;
        private System.Windows.Forms.TabPage tpOwnObject;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lPowerOwn;
        private System.Windows.Forms.TextBox tbCoeffOwn;
        private System.Windows.Forms.Label lCoeffOwn;
        private System.Windows.Forms.TextBox tbPowerOwn;
        private System.Windows.Forms.Label lChooseSC;
        private System.Windows.Forms.Panel pCoordPoint;
        private System.Windows.Forms.GroupBox grbOwnObject;
        private System.Windows.Forms.Label lHeightOwnObject;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lCommChooseSC;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tbCoeffSupOwn;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox8;
        public System.Windows.Forms.TextBox tbRadiusZone1;
        public System.Windows.Forms.TextBox tbRadiusZone;
        public System.Windows.Forms.ComboBox cbOwnObject;
        public System.Windows.Forms.TextBox tbOwnHeight;
        public System.Windows.Forms.GroupBox gbOwnDegMinSec;
        public System.Windows.Forms.GroupBox gbOwnRect;
        public System.Windows.Forms.GroupBox gbOwnRect42;
        public System.Windows.Forms.GroupBox gbOwnDegMin;
        public System.Windows.Forms.GroupBox gbOwnRad;
        public System.Windows.Forms.CheckBox chbXY;
        public System.Windows.Forms.ComboBox cbChooseSC;
        public System.Windows.Forms.ComboBox cbHeightOwnObject;
        public System.Windows.Forms.TextBox tbHeightOwnObject;
        public System.Windows.Forms.ComboBox cbCommChooseSC;
        public System.Windows.Forms.ComboBox cbPt1HeightOwnObject;
        public System.Windows.Forms.TextBox tbPt1Height;
        public System.Windows.Forms.GroupBox gbPt1DegMinSec;
        public System.Windows.Forms.GroupBox gbPt1Rad;
        public System.Windows.Forms.GroupBox gbPt1Rect;
        public System.Windows.Forms.GroupBox gbPt1DegMin;
        public System.Windows.Forms.GroupBox gbPt1Rect42;
        public System.Windows.Forms.CheckBox chbXY1;
        public System.Windows.Forms.ComboBox cbPolarOpponent;
        public System.Windows.Forms.TextBox tbHAnt;
        private System.Windows.Forms.Label label17;
        public System.Windows.Forms.TextBox tbOpponentAntenna;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label27;
        public System.Windows.Forms.TextBox tbOpponentAntenna1;
        public System.Windows.Forms.TextBox tbCoeffQ;

    }
}