﻿namespace GrozaMap
{
    partial class FormLineSightRange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.lCoeffTransmitOpponent = new System.Windows.Forms.Label();
            this.tbCoeffTransmitOpponent = new System.Windows.Forms.TextBox();
            this.lFreq = new System.Windows.Forms.Label();
            this.tbFreq = new System.Windows.Forms.TextBox();
            this.lPowerOwn = new System.Windows.Forms.Label();
            this.tbPowerOwn = new System.Windows.Forms.TextBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.gbRad = new System.Windows.Forms.GroupBox();
            this.tbLRad = new System.Windows.Forms.TextBox();
            this.lLRad = new System.Windows.Forms.Label();
            this.tbBRad = new System.Windows.Forms.TextBox();
            this.lBRad = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.bAccept = new System.Windows.Forms.Button();
            this.grbOpponObject = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbOpponentAntenna = new System.Windows.Forms.TextBox();
            this.lHeightOpponObject = new System.Windows.Forms.Label();
            this.tbHeightOpponObject = new System.Windows.Forms.TextBox();
            this.grbOwnObject = new System.Windows.Forms.GroupBox();
            this.tbHeightOwnObject = new System.Windows.Forms.TextBox();
            this.lHeightOwnObject = new System.Windows.Forms.Label();
            this.tbHAnt = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.lMiddleHeight = new System.Windows.Forms.Label();
            this.lDistSightRange = new System.Windows.Forms.Label();
            this.tbMiddleHeight = new System.Windows.Forms.TextBox();
            this.tbDistSightRange = new System.Windows.Forms.TextBox();
            this.lOwnHeight = new System.Windows.Forms.Label();
            this.tbOwnHeight = new System.Windows.Forms.TextBox();
            this.pCoordPoint = new System.Windows.Forms.Panel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.gbRect = new System.Windows.Forms.GroupBox();
            this.tbYRect = new System.Windows.Forms.TextBox();
            this.lYRect = new System.Windows.Forms.Label();
            this.tbXRect = new System.Windows.Forms.TextBox();
            this.lXRect = new System.Windows.Forms.Label();
            this.lCenterLSR = new System.Windows.Forms.Label();
            this.cbCenterLSR = new System.Windows.Forms.ComboBox();
            this.cbChooseSC = new System.Windows.Forms.ComboBox();
            this.lChooseSC = new System.Windows.Forms.Label();
            this.lDepth = new System.Windows.Forms.Label();
            this.tbStepDist = new System.Windows.Forms.TextBox();
            this.gbDegMinSec = new System.Windows.Forms.GroupBox();
            this.tbLSec = new System.Windows.Forms.TextBox();
            this.tbBSec = new System.Windows.Forms.TextBox();
            this.lMin4 = new System.Windows.Forms.Label();
            this.lMin3 = new System.Windows.Forms.Label();
            this.tbLMin2 = new System.Windows.Forms.TextBox();
            this.tbBMin2 = new System.Windows.Forms.TextBox();
            this.lSec2 = new System.Windows.Forms.Label();
            this.lSec1 = new System.Windows.Forms.Label();
            this.lDeg4 = new System.Windows.Forms.Label();
            this.lDeg3 = new System.Windows.Forms.Label();
            this.tbLDeg2 = new System.Windows.Forms.TextBox();
            this.lLDegMinSec = new System.Windows.Forms.Label();
            this.tbBDeg2 = new System.Windows.Forms.TextBox();
            this.lBDegMinSec = new System.Windows.Forms.Label();
            this.gbRect42 = new System.Windows.Forms.GroupBox();
            this.tbYRect42 = new System.Windows.Forms.TextBox();
            this.lYRect42 = new System.Windows.Forms.Label();
            this.tbXRect42 = new System.Windows.Forms.TextBox();
            this.lXRect42 = new System.Windows.Forms.Label();
            this.gbDegMin = new System.Windows.Forms.GroupBox();
            this.tbLMin1 = new System.Windows.Forms.TextBox();
            this.tbBMin1 = new System.Windows.Forms.TextBox();
            this.lLDegMin = new System.Windows.Forms.Label();
            this.lBDegMin = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.cbHeightOwnObject = new System.Windows.Forms.ComboBox();
            this.tbStepAngle = new System.Windows.Forms.TextBox();
            this.cbHeightOpponObject = new System.Windows.Forms.ComboBox();
            this.chbXY = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tbDepth = new System.Windows.Forms.TextBox();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.gbRad.SuspendLayout();
            this.grbOpponObject.SuspendLayout();
            this.grbOwnObject.SuspendLayout();
            this.pCoordPoint.SuspendLayout();
            this.gbRect.SuspendLayout();
            this.gbDegMinSec.SuspendLayout();
            this.gbRect42.SuspendLayout();
            this.gbDegMin.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.textBox2);
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.textBox1);
            this.tabPage2.Controls.Add(this.lCoeffTransmitOpponent);
            this.tabPage2.Controls.Add(this.tbCoeffTransmitOpponent);
            this.tabPage2.Controls.Add(this.lFreq);
            this.tabPage2.Controls.Add(this.tbFreq);
            this.tabPage2.Controls.Add(this.lPowerOwn);
            this.tabPage2.Controls.Add(this.tbPowerOwn);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(354, 199);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Detection zone";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 13);
            this.label3.TabIndex = 167;
            this.label3.Text = "Radius of the zone, m";
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.HighlightText;
            this.textBox2.Location = new System.Drawing.Point(126, 138);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(95, 20);
            this.textBox2.TabIndex = 166;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(207, 167);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 165;
            this.button3.Text = "Clear";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(71, 166);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 164;
            this.button2.Text = "Accept";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(265, 13);
            this.label2.TabIndex = 117;
            this.label2.Text = "Direction finder threshold sensitivity, dBW (-140 - -100) ";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(281, 110);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(60, 20);
            this.textBox1.TabIndex = 116;
            this.textBox1.Text = "-120";
            // 
            // lCoeffTransmitOpponent
            // 
            this.lCoeffTransmitOpponent.AutoSize = true;
            this.lCoeffTransmitOpponent.Location = new System.Drawing.Point(6, 80);
            this.lCoeffTransmitOpponent.Name = "lCoeffTransmitOpponent";
            this.lCoeffTransmitOpponent.Size = new System.Drawing.Size(263, 13);
            this.lCoeffTransmitOpponent.TabIndex = 115;
            this.lCoeffTransmitOpponent.Text = "Directionfinder antenna gain (1-6) ................................";
            // 
            // tbCoeffTransmitOpponent
            // 
            this.tbCoeffTransmitOpponent.Location = new System.Drawing.Point(280, 77);
            this.tbCoeffTransmitOpponent.Name = "tbCoeffTransmitOpponent";
            this.tbCoeffTransmitOpponent.Size = new System.Drawing.Size(60, 20);
            this.tbCoeffTransmitOpponent.TabIndex = 114;
            this.tbCoeffTransmitOpponent.Text = "2";
            // 
            // lFreq
            // 
            this.lFreq.AutoSize = true;
            this.lFreq.Location = new System.Drawing.Point(8, 54);
            this.lFreq.Name = "lFreq";
            this.lFreq.Size = new System.Drawing.Size(260, 13);
            this.lFreq.TabIndex = 111;
            this.lFreq.Text = "Radio transmitter frequency, MHz (100-6000) .............";
            // 
            // tbFreq
            // 
            this.tbFreq.Location = new System.Drawing.Point(281, 50);
            this.tbFreq.Name = "tbFreq";
            this.tbFreq.Size = new System.Drawing.Size(60, 20);
            this.tbFreq.TabIndex = 110;
            this.tbFreq.Text = "2400";
            // 
            // lPowerOwn
            // 
            this.lPowerOwn.AutoSize = true;
            this.lPowerOwn.Location = new System.Drawing.Point(5, 23);
            this.lPowerOwn.Name = "lPowerOwn";
            this.lPowerOwn.Size = new System.Drawing.Size(264, 13);
            this.lPowerOwn.TabIndex = 91;
            this.lPowerOwn.Text = "Radio transmitter power, W (0.1-100) ...........................";
            // 
            // tbPowerOwn
            // 
            this.tbPowerOwn.Location = new System.Drawing.Point(280, 20);
            this.tbPowerOwn.Name = "tbPowerOwn";
            this.tbPowerOwn.Size = new System.Drawing.Size(60, 20);
            this.tbPowerOwn.TabIndex = 88;
            this.tbPowerOwn.Text = "1";
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tabPage1.Controls.Add(this.gbRad);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.bClear);
            this.tabPage1.Controls.Add(this.bAccept);
            this.tabPage1.Controls.Add(this.grbOpponObject);
            this.tabPage1.Controls.Add(this.grbOwnObject);
            this.tabPage1.Controls.Add(this.lMiddleHeight);
            this.tabPage1.Controls.Add(this.lDistSightRange);
            this.tabPage1.Controls.Add(this.tbMiddleHeight);
            this.tabPage1.Controls.Add(this.tbDistSightRange);
            this.tabPage1.Controls.Add(this.lOwnHeight);
            this.tabPage1.Controls.Add(this.tbOwnHeight);
            this.tabPage1.Controls.Add(this.pCoordPoint);
            this.tabPage1.Controls.Add(this.lCenterLSR);
            this.tabPage1.Controls.Add(this.cbCenterLSR);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(354, 199);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "LOS";
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // gbRad
            // 
            this.gbRad.Controls.Add(this.tbLRad);
            this.gbRad.Controls.Add(this.lLRad);
            this.gbRad.Controls.Add(this.tbBRad);
            this.gbRad.Controls.Add(this.lBRad);
            this.gbRad.Location = new System.Drawing.Point(45, 244);
            this.gbRad.Name = "gbRad";
            this.gbRad.Size = new System.Drawing.Size(160, 63);
            this.gbRad.TabIndex = 28;
            this.gbRad.TabStop = false;
            this.gbRad.Visible = false;
            // 
            // tbLRad
            // 
            this.tbLRad.Location = new System.Drawing.Point(78, 36);
            this.tbLRad.MaxLength = 10;
            this.tbLRad.Name = "tbLRad";
            this.tbLRad.Size = new System.Drawing.Size(75, 20);
            this.tbLRad.TabIndex = 5;
            // 
            // lLRad
            // 
            this.lLRad.AutoSize = true;
            this.lLRad.Location = new System.Drawing.Point(4, 41);
            this.lLRad.Name = "lLRad";
            this.lLRad.Size = new System.Drawing.Size(94, 13);
            this.lLRad.TabIndex = 6;
            this.lLRad.Text = "L, рад...................";
            // 
            // tbBRad
            // 
            this.tbBRad.Location = new System.Drawing.Point(78, 13);
            this.tbBRad.MaxLength = 10;
            this.tbBRad.Name = "tbBRad";
            this.tbBRad.Size = new System.Drawing.Size(75, 20);
            this.tbBRad.TabIndex = 4;
            // 
            // lBRad
            // 
            this.lBRad.AutoSize = true;
            this.lBRad.Location = new System.Drawing.Point(4, 18);
            this.lBRad.Name = "lBRad";
            this.lBRad.Size = new System.Drawing.Size(95, 13);
            this.lBRad.TabIndex = 3;
            this.lBRad.Text = "B, рад...................";
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.button1.Location = new System.Drawing.Point(185, 145);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(73, 23);
            this.button1.TabIndex = 165;
            this.button1.Text = "Center LOS";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // bClear
            // 
            this.bClear.Location = new System.Drawing.Point(271, 172);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(75, 23);
            this.bClear.TabIndex = 164;
            this.bClear.Text = "Clear";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click_1);
            // 
            // bAccept
            // 
            this.bAccept.Location = new System.Drawing.Point(271, 146);
            this.bAccept.Name = "bAccept";
            this.bAccept.Size = new System.Drawing.Size(75, 23);
            this.bAccept.TabIndex = 163;
            this.bAccept.Text = "Accept";
            this.bAccept.UseVisualStyleBackColor = true;
            this.bAccept.Click += new System.EventHandler(this.bAccept_Click);
            // 
            // grbOpponObject
            // 
            this.grbOpponObject.Controls.Add(this.label1);
            this.grbOpponObject.Controls.Add(this.tbOpponentAntenna);
            this.grbOpponObject.Controls.Add(this.lHeightOpponObject);
            this.grbOpponObject.Controls.Add(this.tbHeightOpponObject);
            this.grbOpponObject.Location = new System.Drawing.Point(190, 76);
            this.grbOpponObject.Name = "grbOpponObject";
            this.grbOpponObject.Size = new System.Drawing.Size(158, 62);
            this.grbOpponObject.TabIndex = 162;
            this.grbOpponObject.TabStop = false;
            this.grbOpponObject.Text = "Object of jam";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 134;
            this.label1.Text = "Antenna height, m ";
            // 
            // tbOpponentAntenna
            // 
            this.tbOpponentAntenna.Location = new System.Drawing.Point(103, 14);
            this.tbOpponentAntenna.Name = "tbOpponentAntenna";
            this.tbOpponentAntenna.Size = new System.Drawing.Size(51, 20);
            this.tbOpponentAntenna.TabIndex = 133;
            this.tbOpponentAntenna.Text = "2";
            // 
            // lHeightOpponObject
            // 
            this.lHeightOpponObject.AutoSize = true;
            this.lHeightOpponObject.Location = new System.Drawing.Point(3, 40);
            this.lHeightOpponObject.Name = "lHeightOpponObject";
            this.lHeightOpponObject.Size = new System.Drawing.Size(95, 13);
            this.lHeightOpponObject.TabIndex = 21;
            this.lHeightOpponObject.Text = "Total height, m......";
            // 
            // tbHeightOpponObject
            // 
            this.tbHeightOpponObject.Location = new System.Drawing.Point(102, 37);
            this.tbHeightOpponObject.MaxLength = 3;
            this.tbHeightOpponObject.Name = "tbHeightOpponObject";
            this.tbHeightOpponObject.Size = new System.Drawing.Size(52, 20);
            this.tbHeightOpponObject.TabIndex = 26;
            // 
            // grbOwnObject
            // 
            this.grbOwnObject.Controls.Add(this.tbHeightOwnObject);
            this.grbOwnObject.Controls.Add(this.lHeightOwnObject);
            this.grbOwnObject.Controls.Add(this.tbHAnt);
            this.grbOwnObject.Controls.Add(this.label17);
            this.grbOwnObject.Location = new System.Drawing.Point(187, 3);
            this.grbOwnObject.Name = "grbOwnObject";
            this.grbOwnObject.Size = new System.Drawing.Size(161, 68);
            this.grbOwnObject.TabIndex = 161;
            this.grbOwnObject.TabStop = false;
            this.grbOwnObject.Text = "Jammer";
            // 
            // tbHeightOwnObject
            // 
            this.tbHeightOwnObject.Location = new System.Drawing.Point(102, 42);
            this.tbHeightOwnObject.MaxLength = 3;
            this.tbHeightOwnObject.Name = "tbHeightOwnObject";
            this.tbHeightOwnObject.Size = new System.Drawing.Size(52, 20);
            this.tbHeightOwnObject.TabIndex = 26;
            // 
            // lHeightOwnObject
            // 
            this.lHeightOwnObject.AutoSize = true;
            this.lHeightOwnObject.Location = new System.Drawing.Point(4, 44);
            this.lHeightOwnObject.Name = "lHeightOwnObject";
            this.lHeightOwnObject.Size = new System.Drawing.Size(86, 13);
            this.lHeightOwnObject.TabIndex = 21;
            this.lHeightOwnObject.Text = "Total height, m...";
            // 
            // tbHAnt
            // 
            this.tbHAnt.Location = new System.Drawing.Point(102, 19);
            this.tbHAnt.Name = "tbHAnt";
            this.tbHAnt.Size = new System.Drawing.Size(51, 20);
            this.tbHAnt.TabIndex = 109;
            this.tbHAnt.Text = "8";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(4, 23);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(96, 13);
            this.label17.TabIndex = 110;
            this.label17.Text = "Antenna height, m ";
            // 
            // lMiddleHeight
            // 
            this.lMiddleHeight.AutoSize = true;
            this.lMiddleHeight.Location = new System.Drawing.Point(3, 173);
            this.lMiddleHeight.Name = "lMiddleHeight";
            this.lMiddleHeight.Size = new System.Drawing.Size(93, 13);
            this.lMiddleHeight.TabIndex = 158;
            this.lMiddleHeight.Text = "Average height, m";
            // 
            // lDistSightRange
            // 
            this.lDistSightRange.AutoSize = true;
            this.lDistSightRange.Location = new System.Drawing.Point(3, 149);
            this.lDistSightRange.Name = "lDistSightRange";
            this.lDistSightRange.Size = new System.Drawing.Size(92, 13);
            this.lDistSightRange.TabIndex = 157;
            this.lDistSightRange.Text = "Range, m.............";
            // 
            // tbMiddleHeight
            // 
            this.tbMiddleHeight.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbMiddleHeight.Location = new System.Drawing.Point(107, 169);
            this.tbMiddleHeight.Name = "tbMiddleHeight";
            this.tbMiddleHeight.ReadOnly = true;
            this.tbMiddleHeight.Size = new System.Drawing.Size(67, 20);
            this.tbMiddleHeight.TabIndex = 156;
            // 
            // tbDistSightRange
            // 
            this.tbDistSightRange.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbDistSightRange.Location = new System.Drawing.Point(107, 146);
            this.tbDistSightRange.Name = "tbDistSightRange";
            this.tbDistSightRange.Size = new System.Drawing.Size(67, 20);
            this.tbDistSightRange.TabIndex = 155;
            // 
            // lOwnHeight
            // 
            this.lOwnHeight.AutoSize = true;
            this.lOwnHeight.Location = new System.Drawing.Point(4, 127);
            this.lOwnHeight.Name = "lOwnHeight";
            this.lOwnHeight.Size = new System.Drawing.Size(92, 13);
            this.lOwnHeight.TabIndex = 153;
            this.lOwnHeight.Text = "H, m.....................";
            // 
            // tbOwnHeight
            // 
            this.tbOwnHeight.BackColor = System.Drawing.SystemColors.HighlightText;
            this.tbOwnHeight.Location = new System.Drawing.Point(108, 124);
            this.tbOwnHeight.Name = "tbOwnHeight";
            this.tbOwnHeight.ReadOnly = true;
            this.tbOwnHeight.Size = new System.Drawing.Size(67, 20);
            this.tbOwnHeight.TabIndex = 152;
            // 
            // pCoordPoint
            // 
            this.pCoordPoint.Controls.Add(this.checkBox1);
            this.pCoordPoint.Controls.Add(this.label4);
            this.pCoordPoint.Controls.Add(this.gbRect);
            this.pCoordPoint.Location = new System.Drawing.Point(7, 28);
            this.pCoordPoint.Name = "pCoordPoint";
            this.pCoordPoint.Size = new System.Drawing.Size(173, 82);
            this.pCoordPoint.TabIndex = 138;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(144, 3);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 159;
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 2);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 158;
            this.label4.Text = "Enter coordinates";
            // 
            // gbRect
            // 
            this.gbRect.Controls.Add(this.tbYRect);
            this.gbRect.Controls.Add(this.lYRect);
            this.gbRect.Controls.Add(this.tbXRect);
            this.gbRect.Controls.Add(this.lXRect);
            this.gbRect.Location = new System.Drawing.Point(6, 16);
            this.gbRect.Name = "gbRect";
            this.gbRect.Size = new System.Drawing.Size(160, 63);
            this.gbRect.TabIndex = 17;
            this.gbRect.TabStop = false;
            this.gbRect.Visible = false;
            // 
            // tbYRect
            // 
            this.tbYRect.BackColor = System.Drawing.SystemColors.Window;
            this.tbYRect.Location = new System.Drawing.Point(78, 36);
            this.tbYRect.MaxLength = 7;
            this.tbYRect.Name = "tbYRect";
            this.tbYRect.Size = new System.Drawing.Size(75, 20);
            this.tbYRect.TabIndex = 5;
            // 
            // lYRect
            // 
            this.lYRect.AutoSize = true;
            this.lYRect.Location = new System.Drawing.Point(4, 42);
            this.lYRect.Name = "lYRect";
            this.lYRect.Size = new System.Drawing.Size(73, 13);
            this.lYRect.TabIndex = 6;
            this.lYRect.Text = "Long, deg......";
            // 
            // tbXRect
            // 
            this.tbXRect.BackColor = System.Drawing.SystemColors.Window;
            this.tbXRect.Location = new System.Drawing.Point(78, 13);
            this.tbXRect.MaxLength = 7;
            this.tbXRect.Name = "tbXRect";
            this.tbXRect.Size = new System.Drawing.Size(75, 20);
            this.tbXRect.TabIndex = 4;
            // 
            // lXRect
            // 
            this.lXRect.AutoSize = true;
            this.lXRect.Location = new System.Drawing.Point(4, 20);
            this.lXRect.Name = "lXRect";
            this.lXRect.Size = new System.Drawing.Size(73, 13);
            this.lXRect.TabIndex = 3;
            this.lXRect.Text = "Lat, deg.........";
            // 
            // lCenterLSR
            // 
            this.lCenterLSR.AutoSize = true;
            this.lCenterLSR.Location = new System.Drawing.Point(6, 7);
            this.lCenterLSR.Name = "lCenterLSR";
            this.lCenterLSR.Size = new System.Drawing.Size(94, 13);
            this.lCenterLSR.TabIndex = 137;
            this.lCenterLSR.Text = "Center of the zone";
            // 
            // cbCenterLSR
            // 
            this.cbCenterLSR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCenterLSR.FormattingEnabled = true;
            this.cbCenterLSR.Items.AddRange(new object[] {
            "X,Y",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""});
            this.cbCenterLSR.Location = new System.Drawing.Point(107, 4);
            this.cbCenterLSR.Name = "cbCenterLSR";
            this.cbCenterLSR.Size = new System.Drawing.Size(65, 21);
            this.cbCenterLSR.TabIndex = 134;
            this.cbCenterLSR.SelectedIndexChanged += new System.EventHandler(this.cbCenterLSR_SelectedIndexChanged);
            // 
            // cbChooseSC
            // 
            this.cbChooseSC.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChooseSC.DropDownWidth = 150;
            this.cbChooseSC.FormattingEnabled = true;
            this.cbChooseSC.Items.AddRange(new object[] {
            "Метры на мест-ти",
            "Радианы",
            "Градусы",
            "Град, мин, сек"});
            this.cbChooseSC.Location = new System.Drawing.Point(191, 586);
            this.cbChooseSC.Name = "cbChooseSC";
            this.cbChooseSC.Size = new System.Drawing.Size(123, 21);
            this.cbChooseSC.TabIndex = 11;
            this.cbChooseSC.Visible = false;
            // 
            // lChooseSC
            // 
            this.lChooseSC.AutoSize = true;
            this.lChooseSC.Location = new System.Drawing.Point(65, 605);
            this.lChooseSC.Name = "lChooseSC";
            this.lChooseSC.Size = new System.Drawing.Size(36, 13);
            this.lChooseSC.TabIndex = 12;
            this.lChooseSC.Text = "СК.....";
            this.lChooseSC.Visible = false;
            // 
            // lDepth
            // 
            this.lDepth.AutoSize = true;
            this.lDepth.Location = new System.Drawing.Point(501, 145);
            this.lDepth.Name = "lDepth";
            this.lDepth.Size = new System.Drawing.Size(106, 13);
            this.lDepth.TabIndex = 159;
            this.lDepth.Text = "Step, m.....................";
            this.lDepth.Visible = false;
            // 
            // tbStepDist
            // 
            this.tbStepDist.Location = new System.Drawing.Point(555, 93);
            this.tbStepDist.Name = "tbStepDist";
            this.tbStepDist.Size = new System.Drawing.Size(67, 20);
            this.tbStepDist.TabIndex = 160;
            this.tbStepDist.Text = "100";
            this.tbStepDist.Visible = false;
            // 
            // gbDegMinSec
            // 
            this.gbDegMinSec.Controls.Add(this.tbLSec);
            this.gbDegMinSec.Controls.Add(this.tbBSec);
            this.gbDegMinSec.Controls.Add(this.lMin4);
            this.gbDegMinSec.Controls.Add(this.lMin3);
            this.gbDegMinSec.Controls.Add(this.tbLMin2);
            this.gbDegMinSec.Controls.Add(this.tbBMin2);
            this.gbDegMinSec.Controls.Add(this.lSec2);
            this.gbDegMinSec.Controls.Add(this.lSec1);
            this.gbDegMinSec.Controls.Add(this.lDeg4);
            this.gbDegMinSec.Controls.Add(this.lDeg3);
            this.gbDegMinSec.Controls.Add(this.tbLDeg2);
            this.gbDegMinSec.Controls.Add(this.lLDegMinSec);
            this.gbDegMinSec.Controls.Add(this.tbBDeg2);
            this.gbDegMinSec.Controls.Add(this.lBDegMinSec);
            this.gbDegMinSec.Location = new System.Drawing.Point(53, 339);
            this.gbDegMinSec.Name = "gbDegMinSec";
            this.gbDegMinSec.Size = new System.Drawing.Size(158, 59);
            this.gbDegMinSec.TabIndex = 30;
            this.gbDegMinSec.TabStop = false;
            this.gbDegMinSec.Visible = false;
            // 
            // tbLSec
            // 
            this.tbLSec.Location = new System.Drawing.Point(119, 34);
            this.tbLSec.MaxLength = 3;
            this.tbLSec.Name = "tbLSec";
            this.tbLSec.Size = new System.Drawing.Size(25, 20);
            this.tbLSec.TabIndex = 23;
            // 
            // tbBSec
            // 
            this.tbBSec.Location = new System.Drawing.Point(119, 13);
            this.tbBSec.MaxLength = 3;
            this.tbBSec.Name = "tbBSec";
            this.tbBSec.Size = new System.Drawing.Size(25, 20);
            this.tbBSec.TabIndex = 18;
            // 
            // lMin4
            // 
            this.lMin4.AutoSize = true;
            this.lMin4.Location = new System.Drawing.Point(110, 36);
            this.lMin4.Name = "lMin4";
            this.lMin4.Size = new System.Drawing.Size(9, 13);
            this.lMin4.TabIndex = 27;
            this.lMin4.Text = "\'";
            // 
            // lMin3
            // 
            this.lMin3.AutoSize = true;
            this.lMin3.Location = new System.Drawing.Point(110, 14);
            this.lMin3.Name = "lMin3";
            this.lMin3.Size = new System.Drawing.Size(9, 13);
            this.lMin3.TabIndex = 26;
            this.lMin3.Text = "\'";
            // 
            // tbLMin2
            // 
            this.tbLMin2.Location = new System.Drawing.Point(84, 34);
            this.tbLMin2.MaxLength = 2;
            this.tbLMin2.Name = "tbLMin2";
            this.tbLMin2.Size = new System.Drawing.Size(25, 20);
            this.tbLMin2.TabIndex = 22;
            // 
            // tbBMin2
            // 
            this.tbBMin2.Location = new System.Drawing.Point(84, 13);
            this.tbBMin2.MaxLength = 2;
            this.tbBMin2.Name = "tbBMin2";
            this.tbBMin2.Size = new System.Drawing.Size(25, 20);
            this.tbBMin2.TabIndex = 16;
            // 
            // lSec2
            // 
            this.lSec2.AutoSize = true;
            this.lSec2.Location = new System.Drawing.Point(145, 35);
            this.lSec2.Name = "lSec2";
            this.lSec2.Size = new System.Drawing.Size(9, 13);
            this.lSec2.TabIndex = 25;
            this.lSec2.Text = "\'";
            // 
            // lSec1
            // 
            this.lSec1.AutoSize = true;
            this.lSec1.Location = new System.Drawing.Point(145, 15);
            this.lSec1.Name = "lSec1";
            this.lSec1.Size = new System.Drawing.Size(9, 13);
            this.lSec1.TabIndex = 24;
            this.lSec1.Text = "\'";
            // 
            // lDeg4
            // 
            this.lDeg4.AutoSize = true;
            this.lDeg4.Location = new System.Drawing.Point(69, 36);
            this.lDeg4.Name = "lDeg4";
            this.lDeg4.Size = new System.Drawing.Size(13, 13);
            this.lDeg4.TabIndex = 21;
            this.lDeg4.Text = "^";
            // 
            // lDeg3
            // 
            this.lDeg3.AutoSize = true;
            this.lDeg3.Location = new System.Drawing.Point(69, 14);
            this.lDeg3.Name = "lDeg3";
            this.lDeg3.Size = new System.Drawing.Size(13, 13);
            this.lDeg3.TabIndex = 20;
            this.lDeg3.Text = "^";
            // 
            // tbLDeg2
            // 
            this.tbLDeg2.Location = new System.Drawing.Point(43, 32);
            this.tbLDeg2.MaxLength = 2;
            this.tbLDeg2.Name = "tbLDeg2";
            this.tbLDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbLDeg2.TabIndex = 19;
            // 
            // lLDegMinSec
            // 
            this.lLDegMinSec.AutoSize = true;
            this.lLDegMinSec.Location = new System.Drawing.Point(4, 41);
            this.lLDegMinSec.Name = "lLDegMinSec";
            this.lLDegMinSec.Size = new System.Drawing.Size(64, 13);
            this.lLDegMinSec.TabIndex = 17;
            this.lLDegMinSec.Text = "L.................";
            // 
            // tbBDeg2
            // 
            this.tbBDeg2.Location = new System.Drawing.Point(43, 15);
            this.tbBDeg2.MaxLength = 2;
            this.tbBDeg2.Name = "tbBDeg2";
            this.tbBDeg2.Size = new System.Drawing.Size(25, 20);
            this.tbBDeg2.TabIndex = 15;
            // 
            // lBDegMinSec
            // 
            this.lBDegMinSec.AutoSize = true;
            this.lBDegMinSec.Location = new System.Drawing.Point(4, 18);
            this.lBDegMinSec.Name = "lBDegMinSec";
            this.lBDegMinSec.Size = new System.Drawing.Size(59, 13);
            this.lBDegMinSec.TabIndex = 14;
            this.lBDegMinSec.Text = "B...............";
            // 
            // gbRect42
            // 
            this.gbRect42.Controls.Add(this.tbYRect42);
            this.gbRect42.Controls.Add(this.lYRect42);
            this.gbRect42.Controls.Add(this.tbXRect42);
            this.gbRect42.Controls.Add(this.lXRect42);
            this.gbRect42.Location = new System.Drawing.Point(529, 354);
            this.gbRect42.Name = "gbRect42";
            this.gbRect42.Size = new System.Drawing.Size(160, 63);
            this.gbRect42.TabIndex = 27;
            this.gbRect42.TabStop = false;
            this.gbRect42.Visible = false;
            // 
            // tbYRect42
            // 
            this.tbYRect42.Location = new System.Drawing.Point(78, 36);
            this.tbYRect42.MaxLength = 7;
            this.tbYRect42.Name = "tbYRect42";
            this.tbYRect42.Size = new System.Drawing.Size(75, 20);
            this.tbYRect42.TabIndex = 5;
            this.tbYRect42.Visible = false;
            // 
            // lYRect42
            // 
            this.lYRect42.AutoSize = true;
            this.lYRect42.Location = new System.Drawing.Point(4, 42);
            this.lYRect42.Name = "lYRect42";
            this.lYRect42.Size = new System.Drawing.Size(85, 13);
            this.lYRect42.TabIndex = 6;
            this.lYRect42.Text = "Y, м...................";
            // 
            // tbXRect42
            // 
            this.tbXRect42.Location = new System.Drawing.Point(78, 13);
            this.tbXRect42.MaxLength = 7;
            this.tbXRect42.Name = "tbXRect42";
            this.tbXRect42.Size = new System.Drawing.Size(75, 20);
            this.tbXRect42.TabIndex = 4;
            this.tbXRect42.Visible = false;
            // 
            // lXRect42
            // 
            this.lXRect42.AutoSize = true;
            this.lXRect42.Location = new System.Drawing.Point(4, 20);
            this.lXRect42.Name = "lXRect42";
            this.lXRect42.Size = new System.Drawing.Size(85, 13);
            this.lXRect42.TabIndex = 3;
            this.lXRect42.Text = "X, м...................";
            // 
            // gbDegMin
            // 
            this.gbDegMin.Controls.Add(this.tbLMin1);
            this.gbDegMin.Controls.Add(this.tbBMin1);
            this.gbDegMin.Controls.Add(this.lLDegMin);
            this.gbDegMin.Controls.Add(this.lBDegMin);
            this.gbDegMin.Location = new System.Drawing.Point(504, 261);
            this.gbDegMin.Name = "gbDegMin";
            this.gbDegMin.Size = new System.Drawing.Size(150, 63);
            this.gbDegMin.TabIndex = 29;
            this.gbDegMin.TabStop = false;
            this.gbDegMin.Visible = false;
            // 
            // tbLMin1
            // 
            this.tbLMin1.Location = new System.Drawing.Point(65, 38);
            this.tbLMin1.MaxLength = 8;
            this.tbLMin1.Name = "tbLMin1";
            this.tbLMin1.Size = new System.Drawing.Size(74, 20);
            this.tbLMin1.TabIndex = 15;
            // 
            // tbBMin1
            // 
            this.tbBMin1.Location = new System.Drawing.Point(65, 15);
            this.tbBMin1.MaxLength = 8;
            this.tbBMin1.Name = "tbBMin1";
            this.tbBMin1.Size = new System.Drawing.Size(74, 20);
            this.tbBMin1.TabIndex = 13;
            // 
            // lLDegMin
            // 
            this.lLDegMin.AutoSize = true;
            this.lLDegMin.Location = new System.Drawing.Point(4, 41);
            this.lLDegMin.Name = "lLDegMin";
            this.lLDegMin.Size = new System.Drawing.Size(64, 13);
            this.lLDegMin.TabIndex = 12;
            this.lLDegMin.Text = "L.................";
            // 
            // lBDegMin
            // 
            this.lBDegMin.AutoSize = true;
            this.lBDegMin.Location = new System.Drawing.Point(4, 18);
            this.lBDegMin.Name = "lBDegMin";
            this.lBDegMin.Size = new System.Drawing.Size(59, 13);
            this.lBDegMin.TabIndex = 10;
            this.lBDegMin.Text = "B...............";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(4, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(362, 225);
            this.tabControl1.TabIndex = 1;
            // 
            // cbHeightOwnObject
            // 
            this.cbHeightOwnObject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbHeightOwnObject.FormattingEnabled = true;
            this.cbHeightOwnObject.Items.AddRange(new object[] {
            "Высота антенны+рельеф",
            "Высота антенны",
            "Задать самостоятельно"});
            this.cbHeightOwnObject.Location = new System.Drawing.Point(504, 443);
            this.cbHeightOwnObject.Name = "cbHeightOwnObject";
            this.cbHeightOwnObject.Size = new System.Drawing.Size(158, 21);
            this.cbHeightOwnObject.TabIndex = 154;
            this.cbHeightOwnObject.Visible = false;
            // 
            // tbStepAngle
            // 
            this.tbStepAngle.Location = new System.Drawing.Point(16, 417);
            this.tbStepAngle.Name = "tbStepAngle";
            this.tbStepAngle.Size = new System.Drawing.Size(56, 20);
            this.tbStepAngle.TabIndex = 159;
            this.tbStepAngle.Text = "1";
            this.tbStepAngle.Visible = false;
            // 
            // cbHeightOpponObject
            // 
            this.cbHeightOpponObject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbHeightOpponObject.FormattingEnabled = true;
            this.cbHeightOpponObject.Items.AddRange(new object[] {
            "Средняя высота местности",
            "Задать самостоятельно"});
            this.cbHeightOpponObject.Location = new System.Drawing.Point(529, 486);
            this.cbHeightOpponObject.Name = "cbHeightOpponObject";
            this.cbHeightOpponObject.Size = new System.Drawing.Size(165, 21);
            this.cbHeightOpponObject.TabIndex = 155;
            this.cbHeightOpponObject.Visible = false;
            // 
            // chbXY
            // 
            this.chbXY.AutoSize = true;
            this.chbXY.Location = new System.Drawing.Point(300, 397);
            this.chbXY.Name = "chbXY";
            this.chbXY.Size = new System.Drawing.Size(15, 14);
            this.chbXY.TabIndex = 158;
            this.chbXY.UseVisualStyleBackColor = true;
            this.chbXY.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(59, 401);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(133, 13);
            this.label15.TabIndex = 157;
            this.label15.Text = "Ручной выбор координат";
            this.label15.Visible = false;
            // 
            // tbDepth
            // 
            this.tbDepth.BackColor = System.Drawing.SystemColors.Info;
            this.tbDepth.Location = new System.Drawing.Point(114, 443);
            this.tbDepth.Name = "tbDepth";
            this.tbDepth.ReadOnly = true;
            this.tbDepth.Size = new System.Drawing.Size(67, 20);
            this.tbDepth.TabIndex = 156;
            this.tbDepth.Visible = false;
            // 
            // FormLineSightRange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(369, 231);
            this.Controls.Add(this.gbDegMin);
            this.Controls.Add(this.lChooseSC);
            this.Controls.Add(this.cbChooseSC);
            this.Controls.Add(this.gbRect42);
            this.Controls.Add(this.gbDegMinSec);
            this.Controls.Add(this.cbHeightOwnObject);
            this.Controls.Add(this.tbStepAngle);
            this.Controls.Add(this.lDepth);
            this.Controls.Add(this.cbHeightOpponObject);
            this.Controls.Add(this.chbXY);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.tbDepth);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.tbStepDist);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(850, 100);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormLineSightRange";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Line of sight zone (LOS)";
            this.TopMost = true;
            this.Activated += new System.EventHandler(this.FormLineSightRange_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormLineSightRange_FormClosing);
            this.Load += new System.EventHandler(this.FormLineSightRange_Load);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.gbRad.ResumeLayout(false);
            this.gbRad.PerformLayout();
            this.grbOpponObject.ResumeLayout(false);
            this.grbOpponObject.PerformLayout();
            this.grbOwnObject.ResumeLayout(false);
            this.grbOwnObject.PerformLayout();
            this.pCoordPoint.ResumeLayout(false);
            this.pCoordPoint.PerformLayout();
            this.gbRect.ResumeLayout(false);
            this.gbRect.PerformLayout();
            this.gbDegMinSec.ResumeLayout(false);
            this.gbDegMinSec.PerformLayout();
            this.gbRect42.ResumeLayout(false);
            this.gbRect42.PerformLayout();
            this.gbDegMin.ResumeLayout(false);
            this.gbDegMin.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabControl tabControl1;
        public System.Windows.Forms.ComboBox cbCenterLSR;
        private System.Windows.Forms.Label lCenterLSR;
        private System.Windows.Forms.Panel pCoordPoint;
        public System.Windows.Forms.GroupBox gbDegMinSec;
        public System.Windows.Forms.TextBox tbLSec;
        public System.Windows.Forms.TextBox tbBSec;
        private System.Windows.Forms.Label lMin4;
        private System.Windows.Forms.Label lMin3;
        public System.Windows.Forms.TextBox tbLMin2;
        public System.Windows.Forms.TextBox tbBMin2;
        private System.Windows.Forms.Label lSec2;
        private System.Windows.Forms.Label lSec1;
        private System.Windows.Forms.Label lDeg4;
        private System.Windows.Forms.Label lDeg3;
        public System.Windows.Forms.TextBox tbLDeg2;
        private System.Windows.Forms.Label lLDegMinSec;
        public System.Windows.Forms.TextBox tbBDeg2;
        private System.Windows.Forms.Label lBDegMinSec;
        public System.Windows.Forms.GroupBox gbRect42;
        public System.Windows.Forms.TextBox tbYRect42;
        private System.Windows.Forms.Label lYRect42;
        public System.Windows.Forms.TextBox tbXRect42;
        private System.Windows.Forms.Label lXRect42;
        public System.Windows.Forms.GroupBox gbDegMin;
        public System.Windows.Forms.TextBox tbLMin1;
        public System.Windows.Forms.TextBox tbBMin1;
        private System.Windows.Forms.Label lLDegMin;
        private System.Windows.Forms.Label lBDegMin;
        public System.Windows.Forms.GroupBox gbRect;
        public System.Windows.Forms.TextBox tbYRect;
        private System.Windows.Forms.Label lYRect;
        public System.Windows.Forms.TextBox tbXRect;
        private System.Windows.Forms.Label lXRect;
        public System.Windows.Forms.ComboBox cbChooseSC;
        private System.Windows.Forms.Label lChooseSC;
        public System.Windows.Forms.GroupBox gbRad;
        public System.Windows.Forms.TextBox tbLRad;
        private System.Windows.Forms.Label lLRad;
        public System.Windows.Forms.TextBox tbBRad;
        private System.Windows.Forms.Label lBRad;
        private System.Windows.Forms.Label lOwnHeight;
        private System.Windows.Forms.Label lDepth;
        private System.Windows.Forms.Label lMiddleHeight;
        private System.Windows.Forms.Label lDistSightRange;
        private System.Windows.Forms.TextBox tbDistSightRange;
        private System.Windows.Forms.TextBox tbStepDist;
        private System.Windows.Forms.GroupBox grbOwnObject;
        private System.Windows.Forms.Label lHeightOwnObject;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox grbOpponObject;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lHeightOpponObject;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button bClear;
        private System.Windows.Forms.Button bAccept;
        public System.Windows.Forms.ComboBox cbHeightOwnObject;
        private System.Windows.Forms.TextBox tbStepAngle;
        public System.Windows.Forms.ComboBox cbHeightOpponObject;
        public System.Windows.Forms.CheckBox chbXY;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox tbDepth;
        private System.Windows.Forms.TextBox tbPowerOwn;
        private System.Windows.Forms.Label lPowerOwn;
        private System.Windows.Forms.TextBox tbFreq;
        private System.Windows.Forms.Label lFreq;
        private System.Windows.Forms.TextBox tbCoeffTransmitOpponent;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label lCoeffTransmitOpponent;
        public System.Windows.Forms.TextBox tbHAnt;
        public System.Windows.Forms.TextBox tbOpponentAntenna;
        public System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox tbMiddleHeight;
        public System.Windows.Forms.TextBox tbHeightOwnObject;
        public System.Windows.Forms.TextBox tbHeightOpponObject;
        public System.Windows.Forms.TextBox tbOwnHeight;
        public System.Windows.Forms.TextBox textBox2;
    }
}