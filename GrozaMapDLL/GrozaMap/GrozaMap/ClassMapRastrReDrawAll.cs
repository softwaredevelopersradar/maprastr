﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
using System.Windows.Input;

using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;
using Bitmap = System.Drawing.Bitmap;
using Point = System.Drawing.Point;


namespace GrozaMap
{
    class ClassMapRastrReDrawAll
    {

        // REDRAW_MAP_CLASS ********************************************************************

        public static void REDRAW_MAP_CLASS()
        {
            // -------------------------------------------------------------------------------
            // Удалить все

            MapForm.RasterMapControl.RemoveAllObjects();
            // -------------------------------------------------------------------------------
            // SP

            if (GlobalVarLn.flEndSP_stat == 1)
            {
                ClassMapRastrReDraw.f_ReDraw_SP(
                                         // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                         MapForm.RasterMapControl
                                               );

            }
            // ---------------------------------------------------------------------------------
            // OB1

            if (
                (GlobalVarLn.flEndOB1_stat == 1)
                )
            {
                ClassMapRastrReDraw.f_ReDraw_OB1(
                                         // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                         MapForm.RasterMapControl
                                               );
            }
            // ---------------------------------------------------------------------------------
            // OB2

            if (
                (GlobalVarLn.flEndOB2_stat == 1)
                )
            {
                ClassMapRastrReDraw.f_ReDraw_OB2(
                                         // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                         MapForm.RasterMapControl
                                               );
            }
            // -------------------------------------------------------------------------------
            // LF1

            if (
                (GlobalVarLn.flEndLF1_stat == 1)
                )
            {
                ClassMapRastrReDraw.f_ReDraw_LF(
                                         // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                         MapForm.RasterMapControl,
                                         GlobalVarLn.list_LF1,
                                         Mapsui.Styles.Color.Blue
                                               );
            }
            // -----------------------------------------------------------------------------------
            // LF2

            if (
                (GlobalVarLn.flEndLF2_stat == 1)
                )
            {
                //GlobalVarLn.objFormLF2G.f_LF2ReDraw();
                ClassMapRastrReDraw.f_ReDraw_LF(
                                         // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                         MapForm.RasterMapControl,
                                         GlobalVarLn.list_LF2,
                                         Mapsui.Styles.Color.Red
                                               );

            }
            // -----------------------------------------------------------------------------------
            // ZO

            if (
                (GlobalVarLn.flEndZO_stat == 1)
                )
            {
                GlobalVarLn.objFormZOG.f_ZOReDraw();
            }
            // -------------------------------------------------------------------------------------
            // TRO

            if (
                (GlobalVarLn.flEndTRO_stat == 1)
                )
            {
                ClassMapRastrReDraw.f_ReDraw_SP(
                                         // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                         MapForm.RasterMapControl
                                               );
                // LF1
                ClassMapRastrReDraw.f_ReDraw_LF(
                                         // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                         MapForm.RasterMapControl,
                                         GlobalVarLn.list_LF1,
                                         Mapsui.Styles.Color.Blue
                                               );

                //GlobalVarLn.objFormLF2G.f_LF2ReDraw();
                ClassMapRastrReDraw.f_ReDraw_LF(
                                         // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                         MapForm.RasterMapControl,
                                         GlobalVarLn.list_LF2,
                                         Mapsui.Styles.Color.Red
                                               );

                GlobalVarLn.objFormZOG.f_ZOReDraw();

                ClassMapRastrReDraw.f_ReDraw_OB1(
                                         // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                         MapForm.RasterMapControl
                                               );

                ClassMapRastrReDraw.f_ReDraw_OB2(
                                         // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                         MapForm.RasterMapControl
                                               );

            }
            // -------------------------------------------------------------------------------------
            // Маршрут

            if (GlobalVarLn.flEndWay_stat == 1)
            {
                GlobalVarLn.objFormWayG.f_WayReDraw();

            }
            // -------------------------------------------------------------------------------------
            // Az1

            if (GlobalVarLn.flEndOB_az1 == 1)
            {
                //GlobalVarLn.objFormAz1G.f_OBReDraw_Az1();
                ClassMapRastrReDraw.f_ReDraw_Azimuth();
            }
            // -------------------------------------------------------------------------------------

            // -------------------------------------------------------------------------------------
            //0210
            //Tab

            if (GlobalVarLn.fllTab == 1)
            {
                for (int iitab = 0; iitab < GlobalVarLn.list_air_tab.Count; iitab++)
                {
                    //ClassMap.f_DrawAirPlane_Tab_Rastr(GlobalVarLn.list_air_tab[iitab].Lat, GlobalVarLn.list_air_tab[iitab].Long, 0, GlobalVarLn.list_air_tab[iitab].Num);

                    // CLASS
                    ClassMapRastrDraw.f_DrawImage(
                                  -1,  // Mercator
                                  -1,
                                  GlobalVarLn.list_air_tab[iitab].Lat,
                                  GlobalVarLn.list_air_tab[iitab].Long,
                                  Application.StartupPath + "\\ImagesHelp\\" + "Source.ico",
                                  Convert.ToString(GlobalVarLn.list_air_tab[iitab].Num),
                                  0.6
                                 );

                }
            }

            // -------------------------------------------------------------------------------------

            // ZONES >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

            // -------------------------------------------------------------------------------------
            // ЗПВ

            if (
                (GlobalVarLn.fl_LineSightRange == 1)
                )
            {
                // SP
                ClassMapRastrDraw.f_DrawImage(
                              GlobalVarLn.XCenter_ZPV,  // Mercator
                              GlobalVarLn.YCenter_ZPV,
                              -1,
                              -1,
                              Application.StartupPath + "\\ImagesHelp\\" + "1.png",
                              "",
                              2
                             );

                // Yellow
                // Полигон -> ЗПВ
                ClassMapRastrDraw.DrawPolygon_Rastr(GlobalVarLn.listPointDSR, 100, 255, 255, (byte)0.25 * 255);

                // Pink
                // Полигон -> зона обнаружения СП
                ClassMapRastrDraw.DrawPolygon_Rastr(GlobalVarLn.listDetectionZone, 100, 255, 0, 255);

            }
            // -------------------------------------------------------------------------------------
            // Зона подавления радиолиний управления

            if (
                (GlobalVarLn.flCoordSP_sup == 1)
               )
            {
                // СП+ОП+зона подавления(полигон)+зона неподавления(круг)
                ClassMapRastrReDraw.f_ReDraw_ZoneSuppression();
            }
            // -------------------------------------------------------------------------------------
            // Зона подавления навигации

            // СП+зона подавления(полигон)
            ClassMapRastrReDraw.f_ReDraw_ZoneSuppressionNavigation();

            // -----------------------------------------------------------------------------------
            // Зона спуфинга

            // СП + зона спуфинга (полигон) + зона подавления навигации для зоны спуфинга (полигон)
            ClassMapRastrReDraw.f_ReDraw_ZoneSpoofingJam();
            // ...................................................................................

            // >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ZONES


        }  // REDRAW_MAP_CLASS
        // ******************************************************************** REDRAW_MAP_CLASS


    } // Class
} // Namespace
