﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.IO;
using System.Windows.Input;

using Mapsui.Geometries;
using Mapsui.Styles;
using Mapsui.Projection;
using WpfMapControl;
using Bitmap = System.Drawing.Bitmap;
using Point = System.Drawing.Point;

namespace GrozaMap
{
    class ClassMapRastrReDraw
    {

        // ZoneSuppression ********************************************************************
        // Перерисовка зоны подавления радиолиний управления
        // СП+ОП+зона подавления(полигон)+зона неподавления(круг)

        public static void f_ReDraw_ZoneSuppression()
        {

            // --------------------------------------------------------------------------------
            // SP

            if (GlobalVarLn.flCoordSP_sup == 1)
            {
                // CLASS
                ClassMapRastrDraw.f_DrawImage(
                              GlobalVarLn.XCenter_sup,  // Mercator
                              GlobalVarLn.YCenter_sup,
                              -1,
                              -1,
                              Application.StartupPath + "\\ImagesHelp\\" + "1.png",
                              "",
                              2
                              );

            }
            // --------------------------------------------------------------------------------
            // OP (красный треугольник)

            if (GlobalVarLn.flCoordOP == 1)
            {
                /*
                        // WORK
                        // OP
                        ClassMap.f_Ob_Rastr(
                                      GlobalVarLn.XPoint1_sup,  // m
                                      GlobalVarLn.YPoint1_sup
                                     );
                */

                // CLASS
                ClassMapRastrDraw.f_DrawImage(
                            GlobalVarLn.XPoint1_sup,  // Mercator
                            GlobalVarLn.YPoint1_sup,
                            -1,
                            -1,
                            Application.StartupPath + "\\ImagesHelp\\" + "TriangleRed.png",
                           //"TriangleRed.png",
                           "",
                           0.6
                                             );

            }
            // --------------------------------------------------------------------------------
            // Зона подавления (полигон) и зона неподавления (заштрихованный белый круг)

            if (GlobalVarLn.fl_Suppression == 1)
            {
            // ................................................................................ 
            // Poligon (ZoneSuppression)

                // WORK
                //DrawPolygon_Rastr2(GlobalVarLn.listControlJammingZone);

                // CLASS
                // Red
                ClassMapRastrDraw.DrawPolygon_Rastr(GlobalVarLn.listControlJammingZone, 100, 255, 0, 0);

            // ................................................................................ 
             // ZoneNOSuppression

                // Центр вокруг ОП
                if (GlobalVarLn.flA_sup == 2)
                {
/*
                    // WORK
                    ClassMap.f_Map_Zon_Suppression_Rastr(
                                          GlobalVarLn.tpPointCentre1_sup,
                                          2,
                                          (long)GlobalVarLn.RZ1_sup
                                                        );
*/
                    // CLASS
                    ClassMapRastrDraw.f_MapRastr_Circle(
                                                     GlobalVarLn.tpPointCentre1_sup,
                                                     100,
                                                     255,
                                                     255,
                                                     255,
                                                     (long)GlobalVarLn.RZ1_sup
                                                    );

                }

                // Центр вокруг СП
                else if (GlobalVarLn.flA_sup == 3) 
                {
                    /*
                                        // WORK
                                        ClassMap.f_Map_Zon_Suppression_Rastr(
                                                          GlobalVarLn.tpPointCentre1_sup,
                                                          2,
                                                          (long)GlobalVarLn.RZ1_sup
                                                                             );
                    */
                    // CLASS
                    ClassMapRastrDraw.f_MapRastr_Circle(
                                                     GlobalVarLn.tpPointCentre1_sup,
                                                     100,
                                                     255,
                                                     255,
                                                     255,
                                                     (long)GlobalVarLn.RZ1_sup
                                                    );

                }
            }
            // --------------------------------------------------------------------------------

        }
        // ******************************************************************** ZoneSuppression

        // ZoneSuppressionNavigation **********************************************************
        // Перерисовка зоны подавления навигации
        // СП+зона подавления(полигон)

        public static void f_ReDraw_ZoneSuppressionNavigation()
        {

            // --------------------------------------------------------------------------------

            if (
                (GlobalVarLn.fl_supnav == 1)
               )
            {
            // ................................................................................
            // SP

                ClassMapRastrDraw.f_DrawImage(
                              GlobalVarLn.XCenter_supnav,  // Mercator
                              GlobalVarLn.YCenter_supnav,
                              -1,
                              -1,
                              Application.StartupPath + "\\ImagesHelp\\" + "1.png",
                              "",
                              // Масштаб изображения
                              2
                             );
             // ................................................................................
             // Poligon (ZoneSuppressionNavigation)

                // WORK
                //ClassMap.DrawPolygon_Rastr5(GlobalVarLn.listNavigationJammingZone);

                // CLASS
                // Сиреневый
                ClassMapRastrDraw.DrawPolygon_Rastr(GlobalVarLn.listNavigationJammingZone, 100, 0, 0, 255);

             // ...............................................................................

            }
            // --------------------------------------------------------------------------------

        }
        // ********************************************************** ZoneSuppressionNavigation

        // ZoneSpoofingJam ********************************************************************
        // Перерисовка зоны спуфинга 
        // СП + зона спуфинга (полигон) + зона подавления навигации(полигон)

        public static void f_ReDraw_ZoneSpoofingJam()
        {

            // --------------------------------------------------------------------------------

            if (
                (GlobalVarLn.fl_spf == 1)
               )
            {
                // ................................................................................
                // SP

                ClassMapRastrDraw.f_DrawImage(
                              GlobalVarLn.XCenter_spf,  // Mercator
                              GlobalVarLn.YCenter_spf,
                              -1,
                              -1,
                              Application.StartupPath + "\\ImagesHelp\\" + "1.png",
                              "",
                              // Масштаб изображения
                              2
                             );
                // ................................................................................
                // Poligon (ZoneSpoofing)

                // Green
                if (GlobalVarLn.flS_spf == 1)
                    ClassMapRastrDraw.DrawPolygon_Rastr(GlobalVarLn.listSpoofingZone, 100, 0, 255, 0);
                // ................................................................................
                // Poligon (Зона подавления навигации для зоны спуфинга)

                // 
                if (GlobalVarLn.flJ_spf == 1)
                    ClassMapRastrDraw.DrawPolygon_Rastr(GlobalVarLn.listSpoofingJamZone, 100, 0, 0, 255);
                // ...............................................................................

            } // GlobalVarLn.fl_spf == 1
            // --------------------------------------------------------------------------------

        }
        // ******************************************************************** ZoneSpoofingJam

        // Azimuth ****************************************************************************
        // Перерисовка азимута (пока это только объект)

        public static void f_ReDraw_Azimuth()
        {

            // --------------------------------------------------------------------------------
            // CLASS
            ClassMapRastrDraw.f_DrawImage(
                          GlobalVarLn.XCenter_az1,  // Mercator
                          GlobalVarLn.YCenter_az1,
                          -1,
                          -1,
                          Application.StartupPath + "\\ImagesHelp\\" + "SquareRed.png",
                          "",
                          // Scale
                          0.6
                          );
            // --------------------------------------------------------------------------------

        }
        // **************************************************************************** Azimuth

        // SP *********************************************************************************

        public static void f_ReDraw_SP(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     RasterMapControl objRasterMapControl
                                      )
        {
            foreach (var station in GlobalVarLn.listJS)
            {

                //IMapObject objectGrozaS1;
                //MapObjectStyle _placeObjectStyleOwn;
                //MapObjectStyle _placeObjectStyleLinked;
                //Mapsui.Geometries.Point pointOwn = new Mapsui.Geometries.Point();
                // ----------------------------------------------------------------------------
                // SP_main

                if (station.HasCurrentPosition)
                {
                    try
                    {

                        // CLASS
                        ClassMapRastrDraw.f_DrawImage_WithoutPath(
                                      // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                      objRasterMapControl,
                                      station.CurrentPosition.x,  // Mercator
                                      station.CurrentPosition.y,
                                      -1,
                                      -1,
                                      (Bitmap)GlobalVarLn.objFormSPG.imageList1.Images[station.indzn],
                                      Convert.ToString(station.Name),
                                      0.6
                                      );

                    }
                    catch (Exception ex)
                    {
                        // ignored
                    }


                } // SP_main
                  // ----------------------------------------------------------------------------
                  // SP_sopr

                if (station.HasPlannedPosition)
                {
                    try
                    {
                        // CLASS
                        ClassMapRastrDraw.f_DrawImage_WithoutPath(
                                      // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                      objRasterMapControl,
                                      station.PlannedPosition.x,  // Mercator
                                      station.PlannedPosition.y,
                                      -1,
                                      -1,
                                      (Bitmap)GlobalVarLn.objFormSPG.imageList1V.Images[station.indzn],
                                      Convert.ToString(station.Name),
                                      0.6
                                      );

                    }
                    catch (Exception ex)
                    {
                        // ignored
                    }

                }
                // ----------------------------------------------------------------------------
            } //Foreach

        }
        // ********************************************************************************* SP

        // OB1 ********************************************************************************

        public static void f_ReDraw_OB1(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     RasterMapControl objRasterMapControl
                                      )
        {
            int i = 0;

            for (i = 0; i < GlobalVarLn.list1_OB1.Count; i++)
            {
                try
                {
                    // CLASS
                    ClassMapRastrDraw.f_DrawImage_WithoutPath(
                                  // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                  objRasterMapControl,
                                  -1,  // Mercator
                                  -1,
                                  GlobalVarLn.list1_OB1[i].Lat,
                                  GlobalVarLn.list1_OB1[i].Long,
                                  (Bitmap)GlobalVarLn.objFormOB1G.imageList1.Images[GlobalVarLn.list1_OB1[i].indzn],
                                  Convert.ToString(GlobalVarLn.list1_OB1[i].sType),
                                  0.6
                                  );

                }
                catch (Exception ex)
                {
                    // ignored
                }

            } // FOR

        }
        // ******************************************************************************** OB1

        // OB2 ********************************************************************************

        public static void f_ReDraw_OB2(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     RasterMapControl objRasterMapControl
                                      )
        {
            int i = 0;

            for (i = 0; i < GlobalVarLn.list1_OB2.Count; i++)
            {
                try
                {
                    // CLASS
                    ClassMapRastrDraw.f_DrawImage_WithoutPath(
                                  // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                  objRasterMapControl,
                                  -1,  // Mercator
                                  -1,
                                  GlobalVarLn.list1_OB2[i].Lat,
                                  GlobalVarLn.list1_OB2[i].Long,
                                  (Bitmap)GlobalVarLn.objFormOB2G.imageList1.Images[GlobalVarLn.list1_OB2[i].indzn],
                                  Convert.ToString(GlobalVarLn.list1_OB2[i].sType),
                                  0.6
                                  );

                }
                catch (Exception ex)
                {
                    // ignored
                }

            } // FOR

        }
        // ******************************************************************************** OB2

        // LF1/2 ******************************************************************************

        public static void f_ReDraw_LF(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     RasterMapControl objRasterMapControl,
                                     List<LF> listLF,
                                     Mapsui.Styles.Color clr
                                      )
        {

            Point_XY_LatLong objPoint_XY_LatLong = new Point_XY_LatLong();
            List<Point_XY_LatLong> list_Point_XY_LatLong = new List<Point_XY_LatLong>();
            int j = 0;

            for(j=0;j< listLF.Count;j++)
            {
                objPoint_XY_LatLong.X = listLF[j].X_m;
                objPoint_XY_LatLong.Y = listLF[j].Y_m;
                objPoint_XY_LatLong.Lat = -1;
                objPoint_XY_LatLong.Long = -1;

                list_Point_XY_LatLong.Add(objPoint_XY_LatLong);

            } // FOR

            ClassMapRastrDraw.f_Draw_PoligonLines(
                                     // Дочерний элемент MapElementHost -> Элемент, где ложим карту
                                     objRasterMapControl,
                                     list_Point_XY_LatLong,
                                     clr
                                                 );

        }
        // ****************************************************************************** LF1/2



    } // Class
} // NameSpace
